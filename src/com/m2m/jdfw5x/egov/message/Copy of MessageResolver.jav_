package com.m2m.jdfw5x.egov.message;

import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.support.MessageSourceAccessor;

import com.m2m.jdfw5x.util.CharSetUtil;

public class MessageResolver {

	protected static Log log = LogFactory.getLog(MessageResolver.class);
	
	private static MessageSourceAccessor msgAssr;

	private static Locale locale = Locale.JAPANESE;
	
	private static String fromCharset;
	private static String toCharset;
	private static final String DEFAULT_KEY = "common.default";

	private static class MessageCharSet {

		private static Object[] assignArgs(Object args[]) {
			for (int i = 0; i < args.length; i++)
				if (args[i] instanceof String)
					args[i] = CharSetUtil.transfer((String) args[i],
							MessageResolver.toCharset,
							MessageResolver.fromCharset);
			return args;
		}

		private MessageCharSet() {
		}
	}

	public MessageResolver() {
	}

	public void setLocale(Locale locale) {
		MessageResolver.locale = locale;
	}

	public void setFromCharset(String fromCharset) {
		MessageResolver.fromCharset = fromCharset;
	}

	public void setToCharset(String toCharset) {
		MessageResolver.toCharset = toCharset;
	}

	public void setMessageSourceAccessor(MessageSourceAccessor msgAssr) {
		MessageResolver.msgAssr = msgAssr;
	}

	public static String getMessage(MessageSourceResolvable msgReslv) {
		return MessageResolver.msgAssr.getMessage(msgReslv, MessageResolver.locale);
	}

	public static String getMessage(String code, Object args[], Locale locale) {
		return MessageResolver.msgAssr.getMessage(code, args, locale);
	}
	
	public static String getText(String code) {
		return MessageResolver.msgAssr.getMessage(code, MessageResolver.locale);
	}

	public static String getMessage(String code, Object args[], String msgStr) {
		return MessageResolver.msgAssr.getMessage(code, args, msgStr);
	}

	public static String getMessage(String code, Object args[], String msgStr,
			Locale locale) {
		return MessageResolver.msgAssr.getMessage(code, args, msgStr, locale);
	}

	public static String getMessage(String code) {
		String message = "";
		try {
			log.info("[ messageSourceAccessor ] :" + MessageResolver.msgAssr + "[ locale ] :" + MessageResolver.locale.getCountry() + "[ locale ] :" + MessageResolver.locale.getDisplayLanguage());
			message = MessageResolver.msgAssr.getMessage(code, Locale.JAPANESE);
			
		} catch (Exception e) {
			message = MessageResolver.msgAssr.getMessage(DEFAULT_KEY, MessageResolver.locale);
		}
		
		return CharSetUtil.transfer(message, MessageResolver.fromCharset, MessageResolver.toCharset);
	}

	public static String getMessage(String code, Object args[]) {
		String message = "";
		try {
			log.info("[ locale ] :" + MessageResolver.locale.getCountry());
			if (args == null)
				message = MessageResolver.msgAssr.getMessage(code, MessageResolver.locale);
			else
				message = MessageResolver.msgAssr.getMessage(code, MessageCharSet.assignArgs(args), MessageResolver.locale);
		} catch (Exception e) {
			message = MessageResolver.msgAssr.getMessage(DEFAULT_KEY, MessageResolver.locale);
		}
		return CharSetUtil.transfer(message, MessageResolver.fromCharset, MessageResolver.toCharset);
	}

}
