package com.m2m.jdfw5x.egov.message;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;

public class MessageResolver {

	private static MessageSourceAccessor msgAssr;
	private static Locale locale;
	
	// protected final static Log log = LogFactory.getLog("MessageResolver");

	public MessageResolver() {
	}

	public synchronized void setMessageSourceAccessor(MessageSourceAccessor msgAssr) {
		MessageResolver.msgAssr = msgAssr;
	}

	public synchronized void setLocale(Locale locale) {
		MessageResolver.locale = locale;
	}

	public static String getMessage(MessageSourceResolvable msgReslv) {
	    HttpServletRequest httpRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
	    Locale locale = MessageResolver.getLocale(httpRequest);
	    return msgAssr.getMessage(msgReslv,  locale);
	}

	public static String getMessage(String code) {
	    /* 2015.06.12 에러시 값 반환안해서 일단 getText를 쓰게 함 */
	    /*
    	    HttpServletRequest httpRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
    	    Locale locale = MessageResolver.getLocale(httpRequest);
    		return msgAssr.getMessage(code,  locale);
		*/
	    return getText(code);
	}
	
	public static String getText(String code) {    
        String rtn = null;
        String key = CommonUtil.getTrimString(code);    // 코드에서 공백제거
        HttpServletRequest httpRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        try {
            Locale locale = MessageResolver.getLocale(httpRequest);
            // log.info("Locale :" + locale);
            rtn = msgAssr.getMessage(key,  locale);
            if (StringUtils.isEmpty(rtn)) {
                rtn = code;
            }
        } catch ( Exception e) {
            rtn =  code;      // 에러 발생했을 경우 키값으로 넘어온 문자열을 그대로 반환
        }
        return rtn;
        
    }

	public static String getText(String code, int languageType) {
	    String rtn = null;
	    String key = CommonUtil.getTrimString(code);    // 코드에서 공백제거
	    try {
	        HttpServletRequest httpRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            Locale locale = MessageResolver.getLocale(httpRequest);
            rtn = msgAssr.getMessage(key,  locale);
            if (StringUtils.isEmpty(rtn)) {
                rtn = code;
            }
		} catch ( Exception e) {
		    rtn =  code;      // 에러 발생했을 경우 키값으로 넘어온 문자열을 그대로 반환
		}
	    return rtn;
		
	}

	public static String getMessage(String code, Object args[]) {
	     HttpServletRequest httpRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
         Locale locale = MessageResolver.getLocale(httpRequest);
		if (args == null)
			return msgAssr.getMessage(code, locale);
		else		    
			return msgAssr.getMessage(code, args, locale);
	}
	
	public static String getMessage(String code, Object args[], int languageType) {
        if (args == null)
            return getText(code, languageType);
        
        else {
            String rtn = null;
            String key = CommonUtil.getTrimString(code);    // 코드에서 공백제거
  
            HttpServletRequest httpRequest = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
            try {
                Locale locale = MessageResolver.getLocale(httpRequest);
                rtn = msgAssr.getMessage(key, args, locale);
                
            } catch ( Exception e) {
               rtn = code; 
            }
            return rtn;
        }
    }
	   

	public static String getMessage(String code, Object args[], Locale locale) {
		return msgAssr.getMessage(code, args, locale);
	}

	public static String getMessage(String code, Object args[], String msgStr) {
		return msgAssr.getMessage(code, args, msgStr);
	}

	public static String getMessage(String code, Object args[], String msgStr,
			Locale locale) {
		return msgAssr.getMessage(code, args, msgStr, locale);
	}
	
	private static Locale getLocale(HttpServletRequest httpRequest) {    
        HttpSession session = httpRequest.getSession();
        Locale locale = null;
        // log.info("getLocale [ session ] :" + session.getAttribute(ConstantIF.SS_LANG_TYPE));
        
        if ( session == null || session.getAttribute(ConstantIF.SS_LANG_TYPE) == null) {
            locale =   httpRequest.getLocale();
            // log.info("getLocale [ locale ] :" + locale);
            
        } else {
            
            int langType = Integer.parseInt(session.getAttribute(ConstantIF.SS_LANG_TYPE).toString());
            // log.info("getLocale [ langType ] :" + langType);
            if ( ConstantIF.LANGUAGE_TYPE_JAPANESE == langType ) { 
                locale =  Locale.JAPANESE;
                
            } else if( ConstantIF.LANGUAGE_TYPE_CHINESE == langType ) {
                locale =  Locale.CHINESE;
                
            } else if ( ConstantIF.LANGUAGE_TYPE_ENGLISH == langType) {
                locale =  Locale.ENGLISH;
                
            } else if ( ConstantIF.LANGUAGE_TYPE_VIETNAM == langType) {
                locale =  Locale.ITALY;
                
            } else {
                locale =  Locale.KOREAN;
            }

        }
        return locale;
	    
	}
}
