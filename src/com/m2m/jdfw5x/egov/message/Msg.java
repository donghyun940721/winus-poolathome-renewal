package com.m2m.jdfw5x.egov.message;

@SuppressWarnings("PMD")
public class Msg {

	private String message;
	private boolean alert;
	private String returl_url;
	private String url_target;

	public String getMessage() {
		return message;
	}

	public boolean isAlert() {
		return alert;
	}

	public void setAlert(boolean alert) {
		this.alert = alert;
	}

	public String getReturl_url() {
		return returl_url;
	}

	public void setReturl_url(String returlUrl) {
		returl_url = returlUrl;
	}

	public String getUrl_target() {
		return url_target;
	}

	public void setUrl_target(String url_target) {
		this.url_target = url_target;
	}

	public Msg(String message) {
		this.message = message;
	}

	public Msg() {
		message = "";
	}
}