package com.m2m.jdfw5x.util.code.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.CommonUtil;
/**
 * 시스템명 : [FTA원산지관리시스템] 엠투엠글로벌
 * 패키지명 : com.m2m.jdfw5x.util.code.service
 * 클래스명 : CodeParser.java
 * 설명 : 어쩌구 ~하는 클래스(인터페이스)
 * @author  : indigo(HONG SIK)
 * @since   : 2010. 9. 6. 오후 4:47:04
 * @version : 1.0
 * @see
 *
 * == 개정이력(Modification Information) ==
 *   수정일        수정자      수정내용
 *  ---------   ---------   -------------------------------
 *  2010. 9. 6.    indigo(HONG SIK)    최초생성
 *
 *  -------------------------------------------------------
 * Copyright (C) by SP-IDC All right reserved.
 */
@Component("CodeParser")
public class CodeParser {
	
	protected static final Log LOGGER = LogFactory.getLog("CodeParser");
	
	/**
	 * Method ID   : getCodeName
	 * Method 설명 : 공통코드에 대한 코드값
	 * 작성자      : indigo(HONG SIK)
	 * @param groupId
	 * @param code
	 * @return
	 */
	public static String getCodeName(String groupId, String code) {
		//System.out.println("groupId========"+groupId);
		//System.out.println("code========"+code);
		return getCodeName("COMM", groupId, code);
	}

    /**
     * Method ID   : getCodeName
     * Method 설명 : 코드타입에 따른 코드값
     * 작성자      : indigo(HONG SIK)
     * @param groupType
     * @param groupId
     * @param code
     * @return
     */
	public static String getCodeName(String groupType, String groupId, String code) {
		try{
			CodeMngr codeMgr = CodeMngr.getInstance();
			String codeString = codeMgr.getCodeNm(groupType, groupId.trim(),code.trim());
			return codeString;
		}catch (Exception e){ return "";}
	}	
	
	/**
	 * 코드그룹 아이디와 select 필드 이름으로 value 값이 선택되고 event가 지정된 콤보박스 생성(전체 없음)
	 * @param groupId	코드 그룹 아이디
	 * @param fieldId	콤보 아이디
	 * @param fieldName	콤보 이름
	 * @param event		이벤트
	 * @param value		콤보 초기 세팅값
	 * @return
	 */
	public static String getComboStrWithoutAddedOption(String groupType, String groupId, String fieldId, String fieldName, String event, String function, String value, String sClass, ArrayList<String> list, String tabIndex, String etc){
		try{
			CodeCombo comboMgr = new CodeCombo(groupType.trim(), groupId.trim(),fieldId.trim(), fieldName.trim(), list);
			if(list.size() > 0) {
				return comboMgr.genComboBox2(event.trim(), function.trim(), value.trim(), 0, sClass, tabIndex, etc);//이벤트&호출함수(콤보박스선택)
			} else {
				return comboMgr.genComboBox(event.trim(), function.trim(), value.trim(), 0, sClass, tabIndex, etc);//이벤트&호출함수(콤보박스선택)
			}
		}catch (Exception e){
            if (LOGGER.isWarnEnabled()) {
            	LOGGER.warn("fail create combo option...", e);
            }
			return "";
		}
	}

	/**
	 * 코드그룹 아이디와 select 필드 이름으로 value 값이 선택되고 event가 지정된 콤보박스 생성(전체 대신 option값으로)
	 * @param groupId	코드 그룹 아이디
	 * @param fieldId	콤보 아이디
	 * @param fieldName	콤보 이름
	 * @param event		이벤트
	 * @param value		콤보 초기 세팅값
	 * @param option	전체 타이틀 명(value가 없는)
	 * @return
	 */
	public static String getComboStrWithAddedOption(String groupType, String groupId, String fieldId, String fieldName, String event, String function, String value, String option, String sClass, ArrayList<String> list, String tabIndex, String etc){
		try{
			CodeCombo comboMgr = new CodeCombo(groupType.trim(), groupId.trim(),fieldId.trim(),fieldName.trim(), list);
			comboMgr.insertOption(option, "");
			if(list.size() > 0) {
				return comboMgr.genComboBox2(event.trim(), function.trim(), value.trim(), 0, sClass, tabIndex, etc);//이벤트&호출함수(콤보박스선택)
			} else {
				return comboMgr.genComboBox(event.trim(), function.trim(), value.trim(), 0, sClass, tabIndex, etc);//이벤트&호출함수(콤보박스선택)
			}
		}catch (Exception e){ 
            if (LOGGER.isWarnEnabled()) {
            	LOGGER.warn("fail create combo option...", e);
            }			
			return "";
		}
	}
	
    /**
     * 공통코드 전체도 추가하고 값도 세팅, 이벤트에 따른 함수도 세팅
     * @param group
     * @param id
     * @param value
     * @param all
     * @return
     */
    public String getComboComCode(String group,String id, String value, boolean all) {
    	return getComboComCode(group,id,value,all,"전체","","","");
    }
    
    /**
     * 공통코드 전체도 추가하고 값도 세팅, 이벤트에 따른 함수도 세팅
     * @param group
     * @param id
     * @param value
     * @param all
     * @param allTxt /전체선택 true시 옵션명칭/
     * @return
     */
    public String getComboComCode(String group,String id, String value, boolean all,String allTxt) {
    	return getComboComCode(group,id,value,all,allTxt,"","","");
    }	   
    
    /**
     * 공통코드 전체도 추가하고 값도 세팅, 이벤트에 따른 함수도 세팅
     * @param group
     * @param id
     * @param value
     * @param all
     * @param allTxt /전체선택 true시 옵션명칭/
     * @param event /이벤트/
     * @param  function /이벤트 호출시 함수명
     * @return
     */
    public String getComboComCode(String group,String id, String value, boolean all,String allTxt, String event, String function) {
    	return getComboComCode(group,id,value,all,allTxt,event,function,"");
    }	
   
    /**
     * 공통코드 전체도 추가하고 값도 세팅, 이벤트에 따른 함수도 세팅
     * @param group /공통코드 그룹/
     * @param id   select id와 name
     * @param value /코드벨류값/
     * @param all  /전체여부/
     * @param allTxt
     * @param event /이벤트/
     * @param  function /이벤트 호출시 함수명
     * @param  className /콤보박스클래스명
     * @return 
     */
    public String getComboComCode(String group, String id, String value, boolean all,String allTxt, String event, String function , String className) {
    	try {
    		
    		List list = CodeMngr.getInstance().getValues(group); 
    		//System.out.println("size========"+list.size());
    	
    		StringBuffer sb = new StringBuffer();
    		String c = null;
    		Map<String, String> m = null;
    		CodeVO code = null;
    		//이벤트에 대해서
    		String eventCode = "";
    		if(!CommonUtil.isNull(event)) {
    			eventCode = (new StringBuilder().append(event.trim())
    							.append("='").append(function.trim()).append("'")
    			).toString();
    		}
    		
    		sb.append("<select id='").append(id).append("' name='").append(id).append("' ");
            //스타일시트적용
    		/*
    		if(CommonUtil.isNull(className)){
    			sb.append(" style='").append("width:200px;").append("'");
    		}else{
    			sb.append(" class='").append(className).append("'");
    		}*/
    		if(CommonUtil.isNull(className)){
    			sb.append(" style='").append("width:200px;").append("'");
    		}else{
    			sb.append(className);
    		}
    		if(eventCode != null && eventCode.trim().length() > 0) {
    			sb.append(" ").append(eventCode);
    		}
    		sb.append(">");
    		if(all) {
    			sb.append("<option value=''>"+allTxt+"</option>");
    		}
    		for (Iterator<CodeVO> i = list.iterator(); i.hasNext();) {
    			m = new HashMap<String, String>();
    			code = i.next();
    			//m.put("code", code.getCode());
    			//m.put("name", code.getCodeNm());
    			//m.put("level", "1");
    			//System.out.println("ccc::::"+code.getCode());
    			//System.out.println("cccN::::"+code.getCodeNm());
    			
    			sb.append("<option value='").append((String)code.getCode()).append("'");
    			if(((String)code.getCode()).equals(value)) {
    				sb.append(" selected");
    			}
    			sb.append(">").append(MessageResolver.getMessage((String)code.getCodeNm())).append("</option>");
    		}
    		sb.append("</select>");
            
    		return sb.toString();
    	} catch(Exception e) {
    		return "";
    	}
    }
  
   
   
    /**
     * 공통코드 전체도 추가하고 값도 세팅(그룹별), 이벤트에 따른 함수도 세팅
     * @param group
     * @param id
     * @param name
     * @param value
     * @param all
     * @param event
     * @return
     */
    public String getGridComboComCode(String group, boolean yn, String alltxt) {
    	try {
    		
    		List list = CodeMngr.getInstance().getValues(group); 
    		//System.out.println("getGridComboComCode========"+list.size());
    		StringBuffer sb = new StringBuffer();
    		String c = null;
    		Map<String, String> m = null;
    		CodeVO code = null;
    		sb.append("value:");		
    		int cnt =0;
    		for (Iterator<CodeVO> i = list.iterator(); i.hasNext();) {
    			m = new HashMap<String, String>();
    			code = i.next();
    			if(cnt > 0){
    			sb.append(";").append((String)code.getCode()).append(":").append(MessageResolver.getMessage((String)code.getCodeNm()));	
    			}else{
    				if(yn == true){
    					sb.append("'").append("").append(":").append(alltxt).append(";");
    					
    					sb.append((String)code.getCode()).append(":").append(MessageResolver.getMessage((String)code.getCodeNm()));		
    				}else{        				
	    				sb.append("'");
	    				sb.append((String)code.getCode()).append(":").append(MessageResolver.getMessage((String)code.getCodeNm()));		
    				}
    			}
    			cnt++;
    		}
    		sb.append("'");
    		//System.out.println("getGridComboComCode========"+sb.toString());
    		return sb.toString();
    	} catch(Exception e) {
    		return "";
    	}
    } 
 
    /**
     * 공통코드 전체도 추가하고 값도 세팅(그룹별), 이벤트에 따른 함수도 세팅
     * @param group
     * @param id
     * @param name
     * @param value
     * @param all
     * @param event
     * @return
     */
    public String getGridComboComCode(String group) {
    	return getGridComboComCode(group,false,""); 
    }  
}
