
package com.m2m.jdfw5x.util.code.service;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class CodeDB {

    protected static final Log LOGGER = LogFactory.getLog("CodeDB");

    private CodeDB() {
    }

    public static DataSource getDataSource() throws NamingException {
        DataSource ds = null;
        String dataSource = null;
        try {
            InitialContext context = new InitialContext();
            
            // WAS Server 기동시 -Djdbc.pool=WmsPool 로 설정
            String systemDatasource = System.getProperties().getProperty("jdbc.pool");
            dataSource = (StringUtils.isEmpty(systemDatasource))?"WmsPool":systemDatasource;
            if (checkTomcat())
                ds = (DataSource)context.lookup("java:comp/env/" + dataSource);
            
            else
                ds = (DataSource)context.lookup(dataSource);
            
            return ds;
            
        } catch (NamingException exception) {
        	if (LOGGER.isWarnEnabled()) {
        		LOGGER.warn("[ CodeDB ] Invalid DataSource...  dataSource : " + dataSource);
        	}
        } catch (Exception exception1) {
        }
        return null;
    }

    public static boolean checkTomcat() throws NamingException {
        Context tomcatCheck;
        InitialContext context = new InitialContext();
        tomcatCheck = (Context)context.lookup("java:/comp/env");
        return tomcatCheck != null;
    }

}
