package com.m2m.jdfw5x.util.code.service;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class CodeInit extends HttpServlet {
    
    private static final long serialVersionUID = 4957541016447929568L;
    protected Log log;

	public CodeInit() {
		log = LogFactory.getLog(getClass());
	}

	public void init() throws ServletException {
		super.init();
		try {
			CodeMngr.init();
		} catch (Exception e) {
			log("[ERROR] CodeInit: System initialize fail!", e);
			throw new ServletException(
					"[ERROR] CodeInit: System initialize fail!");
		}
	}

	public void service(ServletRequest servletrequest,
			ServletResponse servletresponse) {
	}
}