package com.m2m.jdfw5x.util.excel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.util.SAXHelper;
import org.apache.poi.xssf.binary.XSSFBSheetHandler.SheetContentsHandler;
import org.apache.poi.xssf.eventusermodel.ReadOnlySharedStringsTable;
import org.apache.poi.xssf.eventusermodel.XSSFReader;
import org.apache.poi.xssf.eventusermodel.XSSFSheetXMLHandler;
import org.apache.poi.xssf.model.StylesTable;
import org.apache.poi.xssf.usermodel.XSSFComment;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import com.logisall.winus.frm.common.util.DateUtil;


public class ExcelReader {
	
	protected static final Log logger = LogFactory.getLog(ExcelReader.class);
	
	public static List<Map> excelRead(File file, String[] cellName)
			throws Exception {
		return excelRead(file, cellName, 0, 0, 0, 0);
	}

	public static List<Map> excelRead(File file, String[] cellName,
			int startSheet) throws Exception {
		return excelRead(file, cellName, startSheet, 0, 0, 0);
	}

	public static List<Map> excelRead(File file, String[] cellName,
			int startSheet, int startRow) throws Exception {
		return excelRead(file, cellName, startSheet, startRow, 0, 0);
	}

	public static List<Map> excelRead(File file, String[] cellName,
			int startSheet, int startRow, int startCell) throws Exception {
		return excelRead(file, cellName, startSheet, startRow, startCell, 0);
	}

	public static List<Map> excelLimitRead(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell, int endCell)
			throws Exception {
		return excelLimitRead(file, cellName, startSheet, startRow, endRow,
				startCell, endCell, 0);
	}

	public static List<Map> excelRead(File file, String[] cellName,
			int startSheet, int startRow, int startCell, int mergeGubun)
			throws Exception {
		List list = null;

		BufferedInputStream bis = null;
//		int maxRow = 10000;
		int maxRow = 100000;
		try {
			if (!file.exists()) {
				throw new Exception("FIle Not EXIXT...");
			}
			bis = new BufferedInputStream(new FileInputStream(file));

			Workbook workbook = WorkbookFactory.create(bis);

			list = rRead(workbook, cellName, maxRow, startSheet, startRow,
					startCell, mergeGubun);

			bis.close();
			file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bis != null) {
				bis.close();
				file.delete();
			}
		}
		return list;
	}

	private static List<Map> rRead(Workbook workbook, String[] cellName,
			int maxRow, int startSheet, int startRow, int startCell,
			int mergeGubun) throws Exception {
		List list = new ArrayList();
		Map rowMap = null;

		int sheets = workbook.getNumberOfSheets();

		for (int sheetIdx = startSheet; sheetIdx < sheets; sheetIdx++) {
			Sheet sheet = workbook.getSheetAt(sheetIdx);

			int firstRow = sheet.getFirstRowNum() + startRow;
			int lastRow = sheet.getLastRowNum();
			int lcnt = 0;
			int modRow = maxRow;

			if (lastRow > maxRow) {
				lcnt = lastRow / maxRow;
			}

			for (int z = 0; z <= lcnt; z++) {
				if ((z == lcnt) && (firstRow != lastRow) && (z != 0)) {
					modRow = lastRow % (lcnt * z);
				}
				if (z > 0) {
					firstRow = z * maxRow;
					modRow = firstRow + maxRow;
				}

				for (int rowIdx = firstRow; rowIdx < modRow; rowIdx++) {
					int cIdx = 0;
					rowMap = new LinkedHashMap();

					Row row = sheet.getRow(rowIdx);

					if (row == null) {
						continue;
					}

					short firstCell = (short) (sheet.getRow(firstRow)
							.getFirstCellNum() + startCell);
					short lastCell = row.getLastCellNum();

					for (short cellIdx = firstCell; cellIdx < lastCell; cellIdx = (short) (cellIdx + 1)) {
						Object data = null;

						Cell cell = row.getCell(cellIdx);

						if (cell == null) {
							cell = row.createCell(cellIdx);
						}

						int type = cell.getCellType();

						if (type == 3) {
							if (mergeGubun == 1){
								cell = getRowCellMerge(row, cell, cellIdx);
							} else if (mergeGubun == 2) {
								cell = getCellMerge(cell, cellIdx);
							}
							type = cell.getCellType();
						}

						data = getData(type, cell);
						rowMap.put(cellName[cIdx], data);
						cIdx++;
					}

					list.add(rowMap);
				}
			}
		}

		return list;

	}

	public static List<Map> excelLimitRead(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell,
			int endCell, int mergeGubun) throws Exception {
		List list = null;

		BufferedInputStream bis = null;
		int maxRow = 10000;
		try {
			if (!file.exists()) {
				throw new Exception("FIle Not EXIXT...");
			}
			bis = new BufferedInputStream(new FileInputStream(file));

			Workbook workbook = WorkbookFactory.create(bis);

			list = rReadLimit(workbook, cellName, maxRow, startSheet, startRow,
					endRow, startCell, endCell, mergeGubun);
			// bis.close();
			// file.delete();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bis != null) {
				bis.close();
			}
			if (file != null) {
				file.delete();
			}
		}
		return list;
	}

	private static List<Map> rReadLimit(Workbook workbook, String[] cellName,
			int maxRow, int startSheet, int startRow, int endRow,
			int startCell, int endCell, int mergeGubun) throws Exception {
		List list = new ArrayList();
		Map rowMap = null;
		int sheets = workbook.getNumberOfSheets();

		for (int sheetIdx = startSheet; sheetIdx < sheets; sheetIdx++) {
			Sheet sheet = workbook.getSheetAt(sheetIdx);

			int firstRow = sheet.getFirstRowNum() + startRow;
			int lastRow = endRow + startRow;
			int lcnt = 0;
			// int modRow = maxRow;

			if (lastRow > maxRow) {
				lcnt = lastRow / maxRow;
			}

			for (int z = 0; z <= lcnt; z++) {
				if ((z == lcnt) && (firstRow != lastRow) && (z != 0)) {
					// modRow = lastRow % (lcnt * z);
				}
				if (z > 0) {
					firstRow = z * maxRow;
					// modRow = firstRow + maxRow;
				}

				for (int rowIdx = firstRow; rowIdx < lastRow; rowIdx++) {
					int cIdx = 0;
					rowMap = new LinkedHashMap();

					Row row = sheet.getRow(rowIdx);

					if (row == null) {
						continue;
					}

					int firstCell = sheet.getRow(firstRow).getFirstCellNum()
							+ startCell;

					int lastCell = endCell + startCell;

					for (int cellIdx = firstCell; cellIdx < lastCell; cellIdx++) {
						Object data = null;

						Cell cell = row.getCell(cellIdx);

						if (cell == null) {
							cell = row.createCell(cellIdx);
						}

						int type = cell.getCellType();

						if (type == 3) {
							if (mergeGubun == 1) {
								cell = getRowCellMerge(row, cell, cellIdx);
							} else if (mergeGubun == 2) {
								cell = getCellMerge(cell, cellIdx);
							}
							type = cell.getCellType();
						}

						data = getData(type, cell);
						rowMap.put(cellName[cIdx], data);
						cIdx++;
					}

					list.add(rowMap);
				}
			}
		}

		return list;
	}

	public static List<Map> excelLimitRowRead(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell)
			throws Exception {
		return excelLimitRowRead(file, cellName, startSheet, startRow, endRow,
				startCell, 0);
	}
	
	public static List<Map> excelLimitRowReadByHandler(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell)
			throws Exception {
		return excelLimitRowReadByHandler(file, cellName, startRow);
	}
	/***
	 * 
	 * @param file
	 * @param cellName
	 * @param startSheet
	 * @param startRow
	 * @param endRow
	 * @param startCell
	 * @return
	 * @throws Exception
	 * @author ykim
	 * @date   21.11.08
	 * @description 미리 정의된 템플릿 형태 대로 엑셀 파싱을 위한 메소드 분리(대화물류 로직)
	 */
	public static List<Map<String, Object>> excelReadRowByTemplate(List<Map<String, Object>> excelTemplate, File file) throws Exception {
		List<Map<String, Object>> list = null;

		BufferedInputStream bis = null;
		int maxRow = 100000;
		try {
			if (!file.exists()) {
				throw new Exception("File is not exists...");
			}
			
			String filename = file.getPath();
			
			//FSUtil.fileDecrypt(filename, filename);
			
			bis = new BufferedInputStream(new FileInputStream(file));
			Workbook workbook = WorkbookFactory.create(bis);

			list = rReadLimitRow(workbook, excelTemplate);
			bis.close();
			// file.delete();
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("fail to read :", e);
			}
		} finally {
			if (bis != null) {
				bis.close();
				// file.delete();
			}
		}
		return list;
	}


	// Method overloading
	// 대화물류 로직에 사용중. 21.11.08
	private static List<Map<String, Object>> rReadLimitRow(Workbook workbook, List<Map<String, Object>> excelTemplate) throws Exception {
		Map<Integer, String> templateDict = new HashMap<Integer, String>();
		int primaryCol = 0;
		int[] maxSeq = new int[excelTemplate.size()];//사용 컬럼중 마지막 seq 컬럼을 찾기위한 Integer Array 생성.
		int maxSeq_idx = 0;
		for (Map<String, Object> item : excelTemplate) {
			if(String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("null") || String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("")){
				break;
			}
			int viewSeq = Integer.parseInt(String.valueOf(item.get("FORMAT_VIEW_SEQ"))) - 1;  // cell 검증시 index가 0부터 시작이므로 -1 해서 사용.
			templateDict.put(viewSeq, String.valueOf(item.get("FORMAT_BIND_COL")));	//viewseq : colNm 형태로 담음.
			
			if(String.valueOf(item.get("FORMAT_BIND_COL")).equals("ITEM_CD")){
				primaryCol = viewSeq; // 필수 컬럼 -> 해당 컬럼에 값이 없을경우 루프 종료.
			}
			maxSeq[maxSeq_idx++] = Integer.valueOf(String.valueOf(item.get("FORMAT_VIEW_SEQ")));
		}
		
		int startRow = Integer.parseInt(String.valueOf(excelTemplate.get(0).get("FORMAT_START_ROW"))) - 1;
		Arrays.sort(maxSeq);
		int lastCol = maxSeq[maxSeq.length - 1]; //사용컬럼중 가장 마지막 Seq
		List<Map<String, Object>> parseResult = new ArrayList();
		Map rowMap = null;
		int sheets = workbook.getNumberOfSheets();	// 전체 Sheet 갯수.

		//STEP 1. Sheet 갯수만큼 루핑.
		int sheetIdx = 0; //FIX. 첫번째 Sheet 만 읽도록.
//		for (int sheetIdx = 0; sheetIdx < sheets; sheetIdx++) {
			
			Sheet sheet = workbook.getSheetAt(sheetIdx);

			
//			int lastRow = endRow + startRow;
			
			for (int rowIdx = 0; rowIdx < 20000; rowIdx++) {
				if(rowIdx < startRow){//시작행전은 스킵.
//					rowIdx++;
					continue;
				}
				
				Row row = sheet.getRow(rowIdx);
				Map<String, Object> rMap = new HashMap<String, Object>();	//각 Row를 담을 Map ,, 매 Loop마다 초기화.
				
				if(row==null){//마지막행에 도달했을 경우 또는 중간에 빈 Row가 있을경우 탐색 중단. row == null
					break;
				}
				
				Cell pkCell = row.getCell(primaryCol, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
				if(String.valueOf(getData2(pkCell)).equals(null) || (String.valueOf(getData2(pkCell)).trim()).equals("")  || (String.valueOf(getData2(pkCell)).trim()).equals("합계")){
					break;
				}
				
				
//				public static final Row.MissingCellPolicy CREATE_NULL_AS_BLANK
				for (int cellIdx = 0; cellIdx <= lastCol; cellIdx++) {
					Cell cell = row.getCell(cellIdx, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
					if(templateDict.containsKey(cellIdx)){
						if(templateDict.get(cellIdx).equals("ORD_QTY")){// 수량컬럼에 숫자 빼고 제거.
							String strCellVal = String.valueOf(getData2(cell));
							cell.setCellValue(strCellVal.replaceAll("[^\\d]", ""));
						}
						rMap.put(templateDict.get(cellIdx), getData2(cell));
						
					}
				}
				parseResult.add(rMap);
//				rowIdx++;
			}


		return parseResult;
	}
	
	/***
	 * @author : KSJ
	 * @date    : 22.01.26
	 * @description 임가공 주문 엑셀 업로드 
	 */
	public static List<Map<String, Object>> excelReadRowOM(List<Map<String, Object>> excelTemplate, File file) throws Exception {
		List<Map<String, Object>> list = null;

		BufferedInputStream bis = null;
		int maxRow = 100000;
		try {
			if (!file.exists()) {
				throw new Exception("File is not exists...");
			}
			
			String filename = file.getPath();
			
			bis = new BufferedInputStream(new FileInputStream(file));
			Workbook workbook = WorkbookFactory.create(bis);

			list = readLimitRowOm(workbook, excelTemplate);
			bis.close();
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("fail to read :", e);
			}
		} finally {
			if (bis != null) {
				bis.close();
				// file.delete();
			}
		}
		return list;
	}


	/***
	 * @author : KSJ
	 * @date    : 22.01.26
	 * @description 임가공 주문 엑셀 업로드 
	 */
	private static List<Map<String, Object>> readLimitRowOm(Workbook workbook, List<Map<String, Object>> excelTemplate) throws Exception {
		Map<Integer, String> templateDict = new HashMap<Integer, String>();
		int primaryCol = 0;
		int[] maxSeq = new int[excelTemplate.size()];//사용 컬럼중 마지막 seq 컬럼을 찾기위한 Integer Array 생성.
		int maxSeq_idx = 0;
		for (Map<String, Object> item : excelTemplate) {
			if(String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("null") || String.valueOf(item.get("FORMAT_VIEW_SEQ")).equals("")){
				break;
			}
			int viewSeq = Integer.parseInt(String.valueOf(item.get("FORMAT_VIEW_SEQ"))) - 1;  // cell 검증시 index가 0부터 시작이므로 -1 해서 사용.
			templateDict.put(viewSeq, String.valueOf(item.get("FORMAT_BIND_COL")));	//viewseq : colNm 형태로 담음.
			
			if(String.valueOf(item.get("FORMAT_BIND_COL")).equals("ITEM_CD")){
				primaryCol = viewSeq; // 필수 컬럼 -> 해당 컬럼에 값이 없을경우 루프 종료.
			}
			maxSeq[maxSeq_idx++] = Integer.valueOf(String.valueOf(item.get("FORMAT_VIEW_SEQ")));
		}
		
		int startRow = Integer.parseInt(String.valueOf(excelTemplate.get(0).get("FORMAT_START_ROW"))) - 1;
		Arrays.sort(maxSeq);
		int lastCol = maxSeq[maxSeq.length - 1]; //사용컬럼중 가장 마지막 Seq
		List<Map<String, Object>> parseResult = new ArrayList();
		Map rowMap = null;
		int sheets = workbook.getNumberOfSheets();	// 전체 Sheet 갯수.

		//STEP 1. Sheet 갯수만큼 루핑.
		int sheetIdx = 0; //FIX. 첫번째 Sheet 만 읽도록.
			
		Sheet sheet = workbook.getSheetAt(sheetIdx);
		for (Row row : sheet) {
			if(row.getRowNum() < startRow){//시작행전은 스킵.
				continue;
			}
			Map<String, Object> rMap = new HashMap<String, Object>();	//각 Row를 담을 Map ,, 매 Loop마다 초기화.
			
			Cell pkCell = row.getCell(primaryCol);
			if(String.valueOf(getData2(pkCell)).equals(null) || (String.valueOf(getData2(pkCell)).trim()).equals("")  || (String.valueOf(getData2(pkCell)).trim()).equals("합계")){
				break;
			}
			
			for (int cellIdx = 0; cellIdx <= lastCol; cellIdx++) {
				Cell cell = row.getCell(cellIdx, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
				if(templateDict.containsKey(cellIdx)){
					if(templateDict.get(cellIdx).equals("ORD_QTY")){// 수량컬럼에 숫자 빼고 제거.
						String strCellVal = String.valueOf(getData2(cell));
						cell.setCellValue(strCellVal.replaceAll("[^\\d]", ""));
					}
					rMap.put(templateDict.get(cellIdx), getData2(cell));
					
				}
			}
			parseResult.add(rMap);
		}

		return parseResult;
	}

	
	
	public static List<Map> excelLimitRowRead(File file, String[] cellName,
			int startSheet, int startRow, int endRow, int startCell,
			int mergeGubun) throws Exception {
		List<Map> list = null;

		BufferedInputStream bis = null;
		int maxRow = 100000;
		try {
			if (!file.exists()) {
				throw new Exception("FIle Not EXIXT...");
			}
			
			String filename = file.getPath();
			
			//FSUtil.fileDecrypt(filename, filename);
			
			bis = new BufferedInputStream(new FileInputStream(file));
			Workbook workbook = WorkbookFactory.create(bis);
			list = rReadLimitRow(workbook, cellName, maxRow, startSheet,
					startRow, endRow, startCell, mergeGubun);
			bis.close();
			// file.delete();
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("fail to read :", e);
			}
		} finally {
			if (bis != null) {
				bis.close();
				// file.delete();
			}
		}
		return list;
	}
	
	public static List<Map> excelLimitRowReadByHandler(File file, String[] cellName, int startRow) throws Exception {
		List<Map> list = null;

		BufferedInputStream bis = null;
		try {
			if (!file.exists()) {
				throw new Exception("FIle Not EXIXT...");
			}
			
			String filename = file.getPath();
			
			//FSUtil.fileDecrypt(filename, filename);
			
			bis = new BufferedInputStream(new FileInputStream(file));
			
			SheetHandler sheetHandler = new SheetHandler(cellName,startRow); 
			
			//SAX 방식 xml package
			OPCPackage pkg = OPCPackage.open(bis);
			//poi excel reader
			XSSFReader xssfReader = new XSSFReader(pkg);
			//get data in excel
			ReadOnlySharedStringsTable data = new ReadOnlySharedStringsTable(pkg);
			//get style in poi excel reader
			StylesTable styles = xssfReader.getStylesTable();
			//get first sheet
			InputStream sheetStream = xssfReader.getSheetsData().next();
			
			InputSource sheetSource = new InputSource(sheetStream);
			
			ContentHandler handler = new XSSFSheetXMLHandler(styles,data,sheetHandler,false);
			
			XMLReader sheetParser = SAXHelper.newXMLReader();
			sheetParser.setContentHandler(handler);
			sheetParser.parse(sheetSource);
			
			sheetStream.close();
			
			list = sheetHandler.getRows();
			
			
			System.out.println("listSize : " + list.size());
			
						
			bis.close();
			// file.delete();
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("fail to read :", e);
			}
		} finally {
			if (bis != null) {
				bis.close();
				// file.delete();
			}
		}
		return list;
	}
	
	

	private static List<Map> rReadLimitRow(Workbook workbook,
			String[] cellName, int maxRow, int startSheet, int startRow,
			int endRow, int startCell, int mergeGubun) throws Exception {
		List list = new ArrayList();
		Map rowMap = null;
		int sheets = workbook.getNumberOfSheets();
		int sheetIdx = startSheet;
//		for (int sheetIdx = startSheet; sheetIdx < sheets; sheetIdx++) { //엑셀 sheet 수만큼  for-loop
			Sheet sheet = workbook.getSheetAt(sheetIdx);

			int firstRow = sheet.getFirstRowNum() + startRow;
			int lastRow = endRow + startRow;

			int lcnt = 0;
			// int modRow = maxRow;

			if (lastRow > maxRow) {
				lcnt = lastRow / maxRow;
			}

			for (int z = 0; z <= lcnt; z++) {
				
				if (z > 0) {
					firstRow = z * maxRow;
				}

				for (int rowIdx = firstRow; rowIdx < lastRow; rowIdx++) {// 첫로우부터 마지막 로우까지 for-loop
					int cIdx = 0;
					rowMap = new LinkedHashMap();

					Row row = sheet.getRow(rowIdx);
					if (row == null) {
						continue;
					}

					int firstCell = sheet.getRow(firstRow).getFirstCellNum()
							+ startCell;

					int lastCell = cellName.length + startCell;
					
					boolean isClearedRow = true; 
					
					for (int cellIdx = firstCell; cellIdx < lastCell; cellIdx++) { // 첫컬럼부터 마지막 컬럼까지 for-loop
						Object data = null;

						Cell cell = row.getCell(cellIdx);
						if (cell == null) {
							cell = row.createCell(cellIdx);
						}
						int type = cell.getCellType();
						if (type == 3) {
							if (mergeGubun == 1) {
								cell = getRowCellMerge(row, cell, cellIdx);
							} else if (mergeGubun == 2) {
								cell = getCellMerge(cell, cellIdx);
							}
							type = cell.getCellType();
						}
						
						data = getData(type, cell);
						if(isClearedCell(type,data)==false){
							isClearedRow = false;
						}
						rowMap.put(cellName[cIdx], data);
						cIdx++;
					}
					if(!isClearedRow){
						list.add(rowMap);
					}
				}
			}
//		}

		return list;
	}

	
	public static List<Map> excelLimitCellRead(File file, String[] cellName,
			int startSheet, int startRow, int startCell, int endCell)
			throws Exception {
		return excelLimitCellRead(file, cellName, startSheet, startRow,
				startCell, endCell, 0);
	}

	public static List<Map> excelLimitCellRead(File file, String[] cellName,
			int startSheet, int startRow, int startCell, int endCell,
			int mergeGubun) throws Exception {
		List list = null;

		BufferedInputStream bis = null;
		int maxRow = 10000;
		try {
			if (!file.exists()) {
				throw new Exception("FIle Not EXIXT...");
			}
			bis = new BufferedInputStream(new FileInputStream(file));

			Workbook workbook = WorkbookFactory.create(bis);

			list = rReadLimitCell(workbook, cellName, maxRow, startSheet,
					startRow, startCell, endCell, mergeGubun);
			bis.close();
			file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bis != null) {
				bis.close();
				file.delete();
			}
		}
		return list;
	}

	private static List<Map> rReadLimitCell(Workbook workbook,
			String[] cellName, int maxRow, int startSheet, int startRow,
			int startCell, int endCell, int mergeGubun) throws Exception {
		List list = new ArrayList();
		Map rowMap = null;
		int sheets = workbook.getNumberOfSheets();

		for (int sheetIdx = startSheet; sheetIdx < sheets; sheetIdx++) {
			Sheet sheet = workbook.getSheetAt(sheetIdx);

			int firstRow = sheet.getFirstRowNum() + startRow;
			int lastRow = sheet.getLastRowNum();
			int lcnt = 0;
			int modRow = maxRow;

			if (lastRow > maxRow) {
				lcnt = lastRow / maxRow;
			}

			for (int z = 0; z <= lcnt; z++) {
				if ((z == lcnt) && (firstRow != lastRow) && (z != 0)) {
					modRow = lastRow % (lcnt * z);
				}
				if (z > 0) {
					firstRow = z * maxRow;
					modRow = firstRow + maxRow;
				}

				for (int rowIdx = firstRow; rowIdx < modRow; rowIdx++) {
					int cIdx = 0;
					rowMap = new LinkedHashMap();

					Row row = sheet.getRow(rowIdx);

					if (row == null) {
						continue;
					}

					int firstCell = sheet.getRow(firstRow).getFirstCellNum()
							+ startCell;

					int lastCell = endCell + startCell;

					for (int cellIdx = firstCell; cellIdx < lastCell; cellIdx++) {
						Object data = null;

						Cell cell = row.getCell(cellIdx);

						if (cell == null) {
							cell = row.createCell(cellIdx);
						}

						int type = cell.getCellType();

						if (type == 3) {
							if (mergeGubun == 1) {
								cell = getRowCellMerge(row, cell, cellIdx);
							} else if (mergeGubun == 2) {
								cell = getCellMerge(cell, cellIdx);
							}
							type = cell.getCellType();
						}

						data = getData(type, cell);
						rowMap.put(cellName[cIdx], data);
						cIdx++;
					}

					list.add(rowMap);
				}
			}
		}

		return list;

	}

	private static Cell getRowCellMerge(Row row, Cell cell, int cellIdx)
			throws Exception {
		Sheet sheet = row.getSheet();
		int rowIdx = row.getRowNum();

		if (rowIdx != sheet.getFirstRowNum()) {
			for (int i = rowIdx; i >= sheet.getFirstRowNum(); i--) {
				if (sheet.getRow(i).getCell(cellIdx).getCellType() != 3) {
					return sheet.getRow(i).getCell(cellIdx);
				}
			}
		}
		return cell;
	}

	private static Cell getCellMerge(Cell cell, int cellIdx) throws Exception {
		Row row = cell.getRow();
		if (cellIdx != row.getFirstCellNum()) {
			for (int i = cellIdx; i >= row.getFirstCellNum(); i--) {
				if (row.getCell(i).getCellType() != 3) {
					return row.getCell(i);
				}
			}
		}
		return cell;
	}

	/*
		private static int checkRow(Workbook workbook, int startSheet) {
			Sheet sheet = workbook.getSheetAt(startSheet);
			// int firstRow = sheet.getFirstRowNum();
			int lastRow = sheet.getLastRowNum();
			return lastRow;
		}
	*/

	private static Object getData(int type, Cell cell) throws Exception {
		Object data = null;
		NumberFormat nf = new DecimalFormat("#########.###");
		switch (type) {
			case 2:	//FORMULA
				data = String.valueOf(cell.getStringCellValue().trim());
				break;
			case 0:	//NUMERIC
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					data = DateUtil.getDateFormat(cell.getDateCellValue(), "yyyy-MM-dd");
				} else {
					data = nf.format(cell.getNumericCellValue());
				}
	
				break;
			case 1:	//STRING
				data = String.valueOf(cell.getStringCellValue().trim());
				break;
			case 3:	//BLANK
				data = "";
				break;
			case 4:	//BOOLEAN
				data = Boolean.valueOf(cell.getBooleanCellValue());
				break;
			case 5:	//ERROR
				data = Byte.valueOf(cell.getErrorCellValue());
				break;
			default:
				data = "";
				break;			
		}

		return data;
	}
	
	private static Object getData2(Cell cell) throws Exception {
		Object data = null;
		NumberFormat nf = new DecimalFormat("#########.###");
		if(cell == null){	//특이케이스 : 마지막행 이후 공백이 null 들어오는 템플릿 리턴처리.
			data = "";
			return data;
		}
		switch (cell.getCellTypeEnum()) {
			case FORMULA:	//FORMULA
				data = String.valueOf(cell.getStringCellValue().trim());
				break;
			case NUMERIC:	//NUMERIC
				if (HSSFDateUtil.isCellDateFormatted(cell)) {
					data = DateUtil.getDateFormat(cell.getDateCellValue(), "yyyyMMdd");
				} else {
					data = nf.format(cell.getNumericCellValue());
				}
	
				break;
			case STRING:	//STRING
				data = String.valueOf(cell.getStringCellValue().trim());
				break;
			case BLANK:	//BLANK
				data = "";
				break;
			case BOOLEAN:	//BOOLEAN
				data = Boolean.valueOf(cell.getBooleanCellValue());
				break;
			case ERROR:	//ERROR
				data = Byte.valueOf(cell.getErrorCellValue());
				break;
//			case 9: //강제처리
//				String str = String.valueOf(cell.getStringCellValue().trim());
//				str.replaceAll("[^\\d]", "");
//				data = str;
//				break;
			default:
				data = "";
				break;			
		}

		return data;
	}
	
	private static boolean isClearedCell(int type, Object data) throws Exception {
		boolean flag = false;
		switch (type) {
			case 2:	//FORMULA
			case 0:	//NUMERIC
			case 1:	//STRING
			case 3:	//BLANK
				if(data.toString().equals("")){
					flag = true;
				}
				else{
					flag = false;
				}
				break;
			case 4:	//BOOLEAN
			case 5:	//ERROR
				if(data==null){
					flag = true;
				}
				else{
					flag = false;
				}
				break;
			default:
				flag = true;
				break;			
		}
		return flag;
	}
}

class SheetHandler implements SheetContentsHandler{
	
	//header를 제외한 데이터부분
	private List rows = new ArrayList();
	//cell 호출시마다 쌓아놓을 1 row List
	private Map row = new LinkedHashMap();
	//Header 정보를 입력
	private String[] header;
	//빈 값을 체크하기 위해 사용할 셀번호
	private int currentCol = -1;
	//현재 읽고 있는 Cell의 Col
	private int currRowNum = 0;
	private int startRow = 0;
	
	
	SheetHandler(String[] cellName, int startRow){
		this.header = cellName;
		this.startRow = startRow;
		
	}
	
	@Override
	public void cell(String columnName, String value, XSSFComment arg2) {
		int iCol = (new CellReference(columnName)).getCol();
        int emptyCol = iCol - currentCol;
        //읽은 Cell의 번호를 이용하여 빈Cell 자리에 빈값을 강제로 저장시켜줌
        for(int i = 1 ; i < emptyCol ; i++) {
            row.put(header[currentCol+i],"");
        }
        currentCol = iCol;
        row.put(header[currentCol],value);		
	}

	@Override
	public void endRow(int rowNum) {
		if(rowNum < startRow) {
			return;
		} 
		else {
			if(row.size() == 0){
				return;
			}
            //헤더의 길이가 현재 로우보다 더 길다면 Cell의 뒷자리가 빈값임으로 해당값만큼 공백
            if(row.size() < header.length) {
                for (int i = row.size(); i < header.length; i++) {
                    row.put(header[i],"");
                }
            }
            rows.add(new LinkedHashMap(row));
        }
		if(rows.size() > 100000){
			System.out.println(row);
		}
       row.clear();
	}

	@Override
	public void headerFooter(String arg0, boolean arg1, String arg2) {
		
	}

	@Override
	public void startRow(int rowNum) {
		this.currentCol = -1;
		this.currRowNum = rowNum;
		
	}

	@Override
	public void hyperlinkCell(String arg0, String arg1, String arg2,
			String arg3, XSSFComment arg4) {
		
		
	}
	
	public List getRows(){
		return rows;
	}
	
}