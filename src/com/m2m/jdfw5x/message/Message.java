package com.m2m.jdfw5x.message;

import com.m2m.jdfw5x.egov.message.MessageResolver;

public final class Message {

	private Message() {
	}

	public static Msg getMsg(String key) {
		return new Msg(MessageResolver.getMessage(key));
	}

	@SuppressWarnings("PMD")
	public static Msg getMsg(String key, Object args[]) {
		return new Msg(MessageResolver.getMessage(key, args));
	}

	public static String getMessage(String key) {
		return getMessage(key, null);
	}

	@SuppressWarnings("PMD")
	public static String getMessage(String key, Object args[]) {
		return MessageResolver.getMessage(key, args);
	}
}
