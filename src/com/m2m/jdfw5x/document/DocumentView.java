package com.m2m.jdfw5x.document;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;

public class DocumentView {

	protected Log log;

	/**
	 * 
	 */
	public DocumentView() {
		log = LogFactory.getLog(getClass());
	}

	/*-
	 * Method ID : getPDFView
	 * Method 설명 : 물리적인 파일을 기본 브라우져의 PDF출력에 연결 
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param file
	 * @throws IOException
	 */
	public static void getPDFView(HttpServletRequest request, HttpServletResponse response, File file)
			throws IOException {
		InputStream inPdf = new FileInputStream(file);
		PdfReader pdfReader = new PdfReader(inPdf);
		response.setContentType("application/pdf");
		int page = pdfReader.getNumberOfPages();
		Document document = new Document(pdfReader.getPageSizeWithRotation(1));
		try {
			PdfCopy copy = new PdfCopy(document, response.getOutputStream());
			document.open();
			for (int i = 1; i < page + 1; i++)
				copy.addPage(copy.getImportedPage(pdfReader, i));

			document.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}
}
