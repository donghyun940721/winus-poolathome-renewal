package com.logisall.mobile.controller;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.ctc.wstx.util.StringUtil;
import com.logisall.winus.tmsys.service.TMSYS030Service;
import com.logisall.winus.wmsop.service.WMSOP035Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class Mobile010Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP035Service")
    private WMSOP035Service wmsop035Service;
    
    @Resource(name = "TMSYS030Service")
	private TMSYS030Service service;
    
    @Resource(name = "WMSIF000Service")
    private WMSIF000Service wmsif000Service;
    
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/tracking.mob")
    public ModelAndView wmsif202(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/mobile/index");
    }

    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/equip.mob")
    public ModelAndView equip(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF203A");
    }

    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/join.mob")
    public @ResponseBody Map<String, Object> join(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.insertediya(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		return m;
    }
    
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/test.mob")
    public @ResponseBody Map<String, Object> test(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Random rnd = new Random();
		m.put("ERROR", rnd.nextInt());
		return m;
    }
    
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/decryptOliveData.mob")
    public @ResponseBody Map<String, Object> decryptOliveData(Map<String, Object> model) throws Exception {
		Map<String, Object> m = wmsop035Service.emsAddress(model);
		return m;
    }

    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/mobile/sabangnetOrderHeader3.mob")
    public ResponseEntity<org.springframework.core.io.Resource> sabangnetOrderHeader3(Map<String, Object> model) throws Exception {
    	HttpHeaders headers = new HttpHeaders();
    	headers.setContentType(MediaType.valueOf("image/svg+xml"));
    	String xml = wmsif000Service.crossDomainSabangOrderHead(model);
    	org.springframework.core.io.Resource resource = new InputStreamResource(new ByteArrayInputStream(xml.getBytes("UTF-8")));
    	//System.out.println(xml);
    	ResponseEntity<org.springframework.core.io.Resource> svg = new ResponseEntity<>(resource, headers, HttpStatus.OK); 
    	return svg;
    	
    }
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetOrderHeader2.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetOrderHeader2(Map<String, Object> model, HttpServletResponse response) throws Exception {
    	String xml = wmsif000Service.crossDomainSabangOrderHead(model);
    	try(InputStream is = new ByteArrayInputStream(xml.getBytes("UTF-8"))) {
    		IOUtils.copy(is, response.getOutputStream());
    		response.flushBuffer();
    	}catch(Exception e){
    		log.error(e.getMessage());
    	}
    }
    /**
     * Method ID	: 
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping(value = "/mobile/sabangnetOrderHeader.mob", produces = MediaType.APPLICATION_XML_VALUE, consumes = MediaType.APPLICATION_XML_VALUE)
    public void sabangnetOrderHeader(Map<String, Object> model, HttpServletResponse response) throws Exception {
    	//String xml = wmsif000Service.crossDomainSabangOrderHead(model);
    	
    	Object orderDateObj = model.get("orderDate");
    	String orderDate;
    	if(orderDateObj != null){
    		orderDate = (String)orderDateObj;
    	}else{
    		String pattern = "yyyyMMdd";
    		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

    		String date = simpleDateFormat.format(new Date());
    		orderDate = date;
    	}
    	
    	Object sendCompanyIdObj = model.get("sendCompanyId");
    	String sendCompanyId;
    	if(sendCompanyIdObj != null){
    		sendCompanyId = (String)sendCompanyIdObj;
    	}else{
    		sendCompanyId = "muriapu";
    	}
    	
    	Object sendAuthKeyObj = model.get("sendAuthKey");
    	String sendAuthKey;
    	if(sendAuthKeyObj != null){
    		sendAuthKey = (String)sendAuthKeyObj;
    	}else{
    		sendAuthKey = "VPuH8AFCMdR7Ed0889ES4BTWX2rGAyM";
    	}
    	
    	StringBuilder sb = new StringBuilder();
    	
    	String[] fieldList = {
    			"IDX",
    			"ORDER_ID",
    			"MALL_ID",
    			"MALL_USER_ID",
    			"MALL_USER_ID2",
    			"ORDER_STATUS",
    			"USER_ID",
    			"USER_NAME",
    			"USER_TEL",
    			"USER_CEL",
    			"USER_EMAIL",
    			"RECEIVE_TEL",
    			"RECEIVE_CEL",
    			"RECEIVE_EMAIL",
    			"DELV_MSG",
    			"RECEIVE_NAME",
    			"RECEIVE_ZIPCODE",
    			"RECEIVE_ADDR",
    			"TOTAL_COST",
    			"PAY_COST",
    			"ORDER_DATE",
    			"PARTNER_ID",
    			"DPARTNER_ID",
    			"MALL_PRODUCT_ID",
    			"PRODUCT_ID",
    			"SKU_ID",
    			"P_PRODUCT_NAME",
    			"P_SKU_VALUE",
    			"PRODUCT_NAME",
    			"SALE_COST",
    			"MALL_WON_COST",
    			"WON_COST",
    			"SKU_VALUE",
    			"SALE_CNT",
    			"DELIVERY_METHOD_STR",
    			"DELV_COST",
    			"COMPAYNY_GOODS_CD",
    			"SKU_ALIAS",
    			"BOX_EA",
    			"JUNG_CHK_YN",
    			"MALL_ORDER_SEQ",
    			"MALL_ORDER_ID",
    			"ETC_FIELD3",
    			"ORDER_GUBUN",
    			"P_EA",
    			"REG_DATE",
    			"ORDER_ETC_1",
    			"ORDER_ETC_2",
    			"ORDER_ETC_3",
    			"ORDER_ETC_4",
    			"ORDER_ETC_5",
    			"ORDER_ETC_6",
    			"ORDER_ETC_7",
    			"ORDER_ETC_8",
    			"ORDER_ETC_9",
    			"ORDER_ETC_10",
    			"ORDER_ETC_11",
    			"ORDER_ETC_12",
    			"ORDER_ETC_13",
    			"ORDER_ETC_14",
    			"ord_field2",
    			"copy_idx",
    			"GOODS_NM_PR",
    			"GOODS_KEYWORD",
    			"ORD_CONFIRM_DATE",
    			"RTN_DT",
    			"CHNG_DT",
    			"DELIVERY_CONFIRM_DATE",
    			"CANCEL_DT",
    			"CLASS_CD1",
    			"CLASS_CD2",
    			"CLASS_CD3",
    			"CLASS_CD4",
    			"BRAND_NM",
    			"DELIVERY_ID",
    			"INVOICE_NO",
    			"HOPE_DELV_DATE",
    			"FLD_DSP",
    			"INV_SEND_MSG",
    			"MODEL_NO",
    			"SET_GUBUN",
    			"ETC_MSG",
    			"DELV_MSG1",
    			"MUL_DELV_MSG",
    			"BARCODE",
    			"INV_SEND_DM",
    			"DELIVERY_METHOD_STR2",
    			"FREE_GIFT"
    			};
    	String fieldListString = StringUtils.join(fieldList, "|");
    	
    	
    	
    	sb.append("<?xml version=\"1.0\" encoding=\"EUC-KR\"?>"
    			+ "<SABANG_ORDER_LIST>"
    			+ "<HEADER>"
    			+ "<SEND_COMPAYNY_ID>"
    			+ sendCompanyId
    			+ "</SEND_COMPAYNY_ID>"
    			+ "<SEND_AUTH_KEY>"
    			+ sendAuthKey
    			+ "</SEND_AUTH_KEY>"
    			+ "<SEND_DATE>"
    			+ orderDate
    			+ "</SEND_DATE>"
    			+ "</HEADER>"
    			+ "<DATA>"
    			+ "<ORD_ST_DATE>"
    			+ orderDate
    			+ "</ORD_ST_DATE>"
    			+ "<ORD_ED_DATE>"
    			+ orderDate
    			+ "</ORD_ED_DATE>"
    			+ "<ORD_FIELD>"
    			+ "<![CDATA[" + fieldListString + "]]>"
    			+ "</ORD_FIELD>"
    			+ "</DATA>"
    			+ "</SABANG_ORDER_LIST>");
    	
    	String xml = sb.toString();
    	BufferedOutputStream bos = null;
    	InputStream fin = null;
		
		try{
			
			byte[] xmlByte = xml.getBytes("UTF-8");
			fin = new ByteArrayInputStream(xmlByte);
			
			response.reset();
			//response.setHeader("Content-Disposition", "attachment;filename="
			//		+ URLEncoder.encode("ORDER_HEADER.xml", "UTF-8") + ";");
			response.setHeader("Content-Transfer-Encoding","binary;");
			response.setHeader("Pragma","no-cache;");
			response.setHeader("Expires","-1;");
			response.setContentLength(xmlByte.length);
			response.setContentType("application/xml;charset=UTF-8");
			
			
			bos = new BufferedOutputStream(response.getOutputStream());
			fin.read(xmlByte);
			bos.write(xmlByte,0,xmlByte.length);
			bos.flush();
			
		}catch(Exception e){
			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
				//e.printStackTrace();
				log.error("download fail");
			}
			
		}finally{
			if (bos != null) {
				try {
					bos.close();
				} catch (Exception ie) {
				}
			}
			if (fin != null) {
				try {
					fin.close();
				} catch (Exception ise) {
				}
			}	
			
		}
    }
    
	
    
}
