package com.logisall.websocket.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository
public class EdiyaDao extends SqlMapAbstractDAO{

    /**
     * Method ID  : listExtra
     * Method 설명   : 
     * 작성자                : KHKIM
     * @param   model
     * @return
     */
	public Object listExtra(Map<String, Object> model) {
		return executeQueryPageWq("wmsif202.listExtra", model);
	}
	/**
	 * Method ID  : listExtra
	 * Method 설명   : 
	 * 작성자                : KHKIM
	 * @param   model
	 * @return
	 */
	public List listExtra() {
		GenericResultSet genericResultSet = new GenericResultSet();
		List<Object> list = list("wmsif202.listExtra", null);
		return list;
	}
}


