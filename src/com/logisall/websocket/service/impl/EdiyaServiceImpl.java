package com.logisall.websocket.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.websocket.service.EdiyaDao;
import com.logisall.websocket.service.EdiyaService;

@Service
public class EdiyaServiceImpl implements EdiyaService{
    protected Log log = LogFactory.getLog(this.getClass());

    @Autowired
    private EdiyaDao ediyaDao;
	
	/**
     * 
     * 대체 Method ID	: listExtra
     * 대체 Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExtra(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", ediyaDao.listExtra(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listExtra
     * 대체 Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public List<Object> listExtra() throws Exception {
        return ediyaDao.listExtra();
    }
    
}
