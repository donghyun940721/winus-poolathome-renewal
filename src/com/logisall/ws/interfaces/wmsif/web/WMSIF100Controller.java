package com.logisall.ws.interfaces.wmsif.web;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF100Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Controller
public class WMSIF100Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF100Service")
	private WMSIF100Service service;
	
	/*-
	 * Method ID : getforkLiftQry 
	 * Method 설명 : 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_FORKLIFT.if")	
	public void getforkLiftQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			m = service.selectForkLiftQry(model);
			log.info(" ********** m ============m========m========m====m====: " + m);
			service.fcmSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getUwms1stReceivingProcessQry 
	 * Method 설명 : UWMS 1충 엘리베이터 탑승 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_UWMS_1ST_RECEIVING_PROCESS.if")	
	public void getUwms1stReceivingProcessQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_EVENT_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD)[0]);
			}
			
			m = service.selectUwms1stReceivingProcessQry(model);
			log.info(" ********** m ============m========m========m====m====: " + m);
			service.fcmSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getUwms3rdReceivingProcessQry 
	 * Method 설명 : UWMS 3충 엘리베이터 하차
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_UWMS_3RD_RECEIVING_PROCESS.if")	
	public void getUwms3rdReceivingProcessQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_EVENT_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD)[0]);
			}			
			
			m = service.selectUwms3rdReceivingProcessQry(model);
			log.info(" ********** m ============m========m========m====m====: " + m);
			service.fcmSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getUwmsReceivingProcessQry 
	 * Method 설명 : UWMS 입고  조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_UWMS_RECEIVING_PROCESS.if")	
	public void getUwmsReceivingProcessQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_EVENT_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD)[0]);
			}			
			
			m = service.selectUwmsReceivingProcessQry(model);
			log.info(" ********** m ============m========m========m====m====: " + m);
			service.fcmSender(m);
			service.fcmNaviSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getUwmsLocPositionQry 
	 * Method 설명 : UWMS 적용 물류센터 로케이션 좌표값 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LOC_POSITION_QRY.if")	
	public void getUwmsLocPositionQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_EVENT_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD)[0]);
			}			
			
			m = service.selectUwmsLocPositionQry(model);
			service.fcmMapSender(m);
			//service.fcmSendTest(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			log.info(" ********** mresponse, m, model=: " + model);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
		
	/*-
	 * Method ID : getUwmsReceivingPositionQry 
	 * Method 설명 : UWMS 입고 확정 수신 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_UWMS_RECEIVING_POSITION.if")	
	public void getUwmsReceivingPositionQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SX) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SX, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SX)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SY) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SY, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SY)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SZ) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SZ, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SZ)[0]);
			}
			
			m = service.selectUwmsReceivingPositionQry(model);
			log.info(" ********** m ============m========m========m====m====: " + m);
			service.fcmSender(m);
			service.fcmNaviSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : mappingInComplete
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_IN_FORKLIFT_COMPLETE.if")
	public void forkliftInComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_MAPPING_IN_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_MAPPING_IN_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.forkliftInComplete(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			log.info("InterfaceException start=======");
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			log.info("Exception e start=======");
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			log.info("try start=======");
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, m, model);

		} catch (Exception e) {
			log.info("try Exception e start=======");
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

		/* 2016.03.02 Security */
		/*
			if (log.isInfoEnabled()) {
				log.info("m :" + m);
			}
		*/
	}
	
	/*-
	 * Method ID : getDensoStockQry 
	 * Method 설명 : DENSO 현재고 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_DENSO_STOCK_QRY.if")	
	public void getDensoStockQry(HttpServletRequest request, HttpServletResponse response) {
		
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		log.info(" ********** request =============request=======================: " + request);
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_CUST_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectDensoStockQry(model);
		}
		
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getDensoStockDetailQry 
	 * Method 설명 : DENSO 현재고 조회(상세) 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_DENSO_STOCK_DETAIL_QRY.if")	
	public void getDensoStockDetailQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_CUST_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectDensoStockDetailQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getDensoStockDailyQry 
	 * Method 설명 : DENSO 현재고 조회(주차별) 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_DENSO_STOCK_DAILY_QRY.if")	
	public void getDensoStockDailyQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_CUST_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_TIME_FROM) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_TIME_FROM, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_TIME_FROM)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_TIME_TO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_TIME_TO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_TIME_TO)[0]);
			}
			
			m = service.selectDensoStockDailyQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	
	/*-
	 * Method ID : getEcoproBMStockDailyQry 
	 * Method 설명 : 에코프로BM 현재고 조회(일별) 
	 * Date	: 2019-01-11
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_ECOPROBM_STOCK_DAILY_QRY.if")	
	public void getEcoproBMStockDailyQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		//if (request.getParameterMap() != null) {
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {				
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, "ecoprobmmk01");
			//}

			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, "ecoprobmmk01");
			//}
			
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, "0000001700");
			//}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_CUST_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD)[0]);
			}
			
			m = service.selectEcoproBMStockDailyQry(model);
		//}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			//service.sendJSON(response, m, model);
			service.ifEcoproBmDailyStockRtnToMssql(response, m, model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : ifEcoproBmOutOrdComfRtn 
	 * Method 설명 : 에코프로BM 출고확정 리턴
	 * Date	: 2019-01-11
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_ECOPROBM_OUT_ORD_COMF_RTN.if")	
	public void ifEcoproBmOutOrdComfRtn(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		//if (request.getParameterMap() != null) {
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {				
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, "m208019");
			//}
	
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, "m1234");
			//}
			
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, "0000001700");
			//}
			
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_CUST_CD, "ECOPROBM");
			//}
			
			m = service.ifEcoproBmOutOrdComfRtnList(model);
		//}
			
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			//service.sendJSON(response, m, model);
			service.ifEcoproBmOutOrdComfRtnToMssql(response, m, model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : ifEcoproBmOutOrdReturnComfRtn 
	 * Method 설명 : 에코프로BM 반품출고확정 리턴
	 * Date	: 2019-01-11
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_ECOPROBM_OUT_ORD_RETURN_COMF_RTN.if")	
	public void ifEcoproBmOutOrdReturnComfRtn(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		//if (request.getParameterMap() != null) {
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {				
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, "m208019");
			//}
	
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, "m1234");
			//}
			
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, "0000001700");
			//}
			
			//if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_CUST_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_CUST_CD, "ECOPROBM");
			//}
			
			m = service.ifEcoproBmOutOrdReturnComfRtnList(model);
		//}
			
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			//service.sendJSON(response, m, model);
			service.ifEcoproBmOutOrdReturnComfRtnToMssql(response, m, model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : getStockQry 
	 * Method 설명 : 현재고 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_FCM_SEND_TEST.if")	
	public void fcmMapSender(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	
	
	String rstFcm = "";
	PrintWriter printWriter = null;
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			model.put(ConstantWSIF.IF_KEY_LOGIN_ID, "admin");
			model.put(ConstantWSIF.IF_KEY_PASSWORD, "jpr001");
			model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, "0000000001");
			
			m.put("param1", request.getParameterMap().get("param1")[0]);
			m.put("param2", request.getParameterMap().get("param2")[0]);
			m.put("param3", request.getParameterMap().get("param3")[0]);
			m.put("param4", request.getParameterMap().get("param4")[0]);
			m.put("param5", request.getParameterMap().get("param5")[0]);
			
			//log.info(" ********** m ============m========m========m====m====: " + m);
			rstFcm = service.fcmMapSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			String sendData = new StringBuffer().append(rstFcm).toString();
			response.setCharacterEncoding(ConstantWSIF.ENCODING_TYPE_JSON_FILE);
			printWriter = response.getWriter();
			printWriter.println("{\"DATA\":[" + sendData+"]}");
			printWriter.close();
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	

	/*-
	 * Method ID : getUwmsLocMapSetInfoQry 
	 * Method 설명 : UWMS 적용 물류센터 로케이션 좌표값 조회(기초정보)
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LOC_MAP_SET_INFO_QRY.if")	
	public void getUwmsLocMapSetInfoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_EVENT_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD)[0]);
			}			
			
			m = service.selectUwmsMapLocMapSetInfoQry(model);
			//service.fcmMapSender(m);
			service.fcmMapSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			log.info(" ********** mresponse, m, model=: " + model);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	

	/*-
	 * Method ID : getUwmsLocMapPositionQry 
	 * Method 설명 : UWMS 적용 물류센터 로케이션 좌표값 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LOC_MAP_POSITION_QRY.if")	
	public void getUwmsLocMapPositionQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_EVENT_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD)[0]);
			}			
			
			m = service.selectUwmsMapLocPositionQry(model);
			//service.fcmMapSender(m);
			service.fcmMapSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			log.info(" ********** mresponse, m, model=: " + model);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getUwmsAgvProcessQry 
	 * Method 설명 : AGV 입출고 정보  조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_UWMS_AGV_PROCESS.if")	
	public void getUwmsAgvProcessQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_EVENT_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD)[0]);
			}			
			
			m = service.selectUwmsAgvProcessQry(model);
			//log.info(" ********** m ============m========m========m====m====: " + m);
			service.agv_post_rest_api(m);
			service.fcmSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getUwmsAgvCompleteQry 
	 * Method 설명 : AGV 입출고 정보  조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_UWMS_AGV_COMPLETE.if")	
	public void getUwmsAgvCompleteQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_EVENT_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_EVENT_CD)[0]);
			}			
			
			m = service.selectUwmsAgvCompleteQry(model);
			//log.info(" ********** m ============m========m========m====m====: " + m);
			service.fcmSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	


	/*-
	 * Method ID : getAgvOrderQry 
	 * Method 설명 : agv 정보 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AGV_ORDER_QRY.if")	
	public void getAgvOrderQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
						
			m = service.selectAgvOrderQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getNaviLoginQry 
	 * Method 설명 : winus 로그인시 네비게이션 로그인 push 발생
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_NAVI_LOGIN.if")	
	public void getNaviLoginQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			m = service.selectNaviLoginQry(model);
			log.info(" ********** m ============m========m========m====m====: " + m);
			service.fcmNaviSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : getAnywareZoneInfoQry 
	 * Method 설명 : ANYWARE ZONE 위치 정보 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_ANYWARE_ZONE_INFO_QRY.if")	
	public void getAnywareZoneInfoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF100");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}				
			
			m = service.selectAnywareZoneInfoQry(model);
			//service.fcmMapSender(m);
			service.fcmMapSender(m);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			log.info(" ********** mresponse, m, model=: " + model);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : EdiyaMobileCsComplete
	 * Method 설명 : 이디야 모바일 CS입력 저장
	 * DATE : 2022-02-22
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_EDIYA_MOBILE_CS_COMPLETE.if")
	public void EdiyaMobileCsComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF100");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateEdiyaMobileCsComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : EdiyaMobileReturnComplete
	 * Method 설명 : 이디야 모바일 반품 요청 저장
	 * DATE : 2022-02-22
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_EDIYA_MOBILE_RETURN_COMPLETE.if")
	public void EdiyaMobileReturnComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF100");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateEdiyaMobileReturnComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
}
