package com.logisall.ws.interfaces.wmsif.web;

import java.util.HashMap;
import java.util.Map;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.util.Base64;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF091Service;
import com.lowagie.text.List;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

import winus.delivery.proxy.common.Constant;

@Controller
public class WMSIF091Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF091Service")
	private WMSIF091Service service;
	
	/*-
	 * Method ID : getdeliveryQry 
	 * Method 설명 : 배송정보조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_SEARCH.if")	
	public void getdeliveryAsQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			m = service.selectDeliveryAsQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getdeliveryAsPhoneQry 
	 * Method 설명 : 전화번호로 배송 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_PHONE_QRY.if")	
	public void getdeliveryAsPhoneQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			
			m = service.selectDeliveryAsPhoneQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getdeliveryAsPhoneShortQry 
	 * Method 설명 : 뒷 전화번호로 배송 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_PHONE_SHORT_QRY.if")	
	public void getdeliveryAsPhoneShortQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			m = service.selectDeliveryAsPhoneShortQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getdeliveryAsOrderQry 
	 * Method 설명 : 배송주문번호로 배송 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_ORDER_QRY.if")	
	public void getdeliveryAsOrderQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectDeliveryAsOrderQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getdeliveryAsOrderDetailQry 
	 * Method 설명 : 배송주문번호로 배송 상품 상세 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_ORDER_DETAIL_QRY.if")	
	public void getdeliveryAsOrderDetailQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectDeliveryAsOrderDetailQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getdeliveryAsOrderDetailQry 2
	 * Method 설명 : 배송주문번호로 배송 상품 상세 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_ORDER_DETAIL_QRY2.if")	
	public void getdeliveryAsOrderDetailQry2(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectDeliveryAsOrderDetailQry2(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : receiveJSONWithFiles
	 * Method 설명 : 파일을 포함한 Json request 의 저장함수
	 * 작성자 : kdalma
	 *
	 * @param model : mulitpart form request 객체
	 * @param inputJSON : file 이외의 json 데이터
	 * @param files : 첨부된 파일 array
	 * @throws Exception
	 */
	public void saveImageFromBase64(String key, String fileName, String base64Data, String ord_id) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model2 = new HashMap<String, Object>();
		
		try {
			if(base64Data == null) return;
			
			String path = ConstantWSIF.DELIVERY_IMG_PATH + DateUtil.getDateByPattern2() +"/"+ key;
			String file_id = key+fileName;
			InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));
			
			model2.put("ORD_ID", ord_id);
			model2.put("FILE_ID", file_id);
			model2.put("ATTACH_GB", "INTERFACE");
			model2.put("FILE_VALUE", "wmssp010");
			model2.put("FILE_PATH", path);
			model2.put("FILE_NAME", fileName);
			model2.put("FILE_EXT", ".jpg");
			
			try {
				m = service.DeliveryImgDataInsert(model2);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save confirm :", e);
				}
				m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliveryComplete
	 * Method 설명 : 파일을 포함한 Delivery 완료 처리 함수
	 * 작성자 : kdalma
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE.if")
	public void deliveryComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[6];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryComplete(model);
			
			// Save photo files
			for(int index=0;index<6;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	

	/*-
	 * Method ID : deliveryManageData
	 * Method 설명 : 배송관리 데이터 전송
	 * 작성자 : MonkeySeok
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_MANAGE_DATA.if")
	public void deliveryManageData(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.DeliveryManageComplete(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliveryCancelData
	 * Method 설명 : 배송취소요청 데이터 전송
	 * 작성자 : MonkeySeok
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_CANCEL_DATA.if")
	public void deliveryCancelData(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCancelComplete(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliveryStartData
	 * Method 설명 : 배송시작 데이터 전송
	 * 작성자 : MonkeySeok
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_START_DATA.if")
	public void deliveryStartData(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.DeliveryStartComplete(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliverySendMsgData
	 * Method 설명 : 배송문자 데이터 전송
	 * 작성자 : MonkeySeok
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_SEND_MSG_DATA.if")
	public void deliverySendMsgData(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.DeliverySendMsgComplete(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : getdeliveryAsDriverQry 
	 * Method 설명 : 배송기사전화번호로 배송 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_DRIVER_QRY.if")	
	public void getdeliveryAsDriverQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			m = service.selectDeliveryAsDriverQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : getdeliveryAsDriverListQry 
	 * Method 설명 : 물류센터별 기사 목록 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_DRIVER_LIST_QRY.if")	
	public void getdeliveryAsDriverListQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			m = service.selectDeliveryAsDriverListQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : deliveryCancelComplete
	 * Method 설명 : 파일을 포함한 Delivery 배송 취소 요청
	 * 작성자 : MonkeySeok
	 *
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_CANCEL_COMPLETE.if")
	public void deliveryCancelComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[6];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCancelComplete(model);
			
			// Save photo files
			for(int index=0;index<6;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliveryFirstBadData
	 * Method 설명 : 배송 초도불량요청 데이터 전송
	 * 작성자 : MonkeySeok
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_FIRST_BAD_DATA.if")
	public void deliveryFirstBadData(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[9];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryFirstBadComplete(model);
			
			// Save photo files
			for(int index=0;index<9;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}


	/*-
	 * Method ID : getdeliveryAsPartInfoQry 
	 * Method 설명 : AS부품정보조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_PART_INFO_QRY.if")	
	public void getdeliveryAsPartInfoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectDeliveryAsPartInfoQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	
	/*-
	 * Method ID : deliveryExclusiveAsComplete
	 * Method 설명 : 파일을 포함한 Delivery 전용기사 as 완료 처리 함수
	 * 작성자 : smics
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_EXCLUSIVE_AS_COMPLETE.if")
	public void deliveryExclusiveAsComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[6];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.deliveryExclusiveAsComplete(model);
			
			// Save photo files
			for(int index=0;index<6;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : getdeliveryAsAsErrorCdQry 
	 * Method 설명 : AS증상별 고장코드 정보조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_AS_ERROR_CD_QRY.if")	
	public void getdeliveryAsAsErrorCdQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectDeliveryAsAsErrorCdQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	

	/*-
	 * Method ID : deliveryHolding
	 * Method 설명 : 파일을 포함한 deliveryHolding 처리 함수
	 * 작성자 : MonkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_HOLDING.if")
	public void deliveryHolding(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[6];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryHolding(model);
			
			// Save photo files
			for(int index=0;index<6;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliveryNotConnect
	 * Method 설명 : 파일을 포함한 미배송 프로시져 호출
	 * 작성자 : MonkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_NOT_CONNECT.if")
	public void deliveryNotConnect(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[6];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryNotConnect(model);
			
			// Save photo files
			for(int index=0;index<6;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : getdeliveryAsPhoneQry 
	 * Method 설명 : 배송게시판
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_BOARD.if")	
	public void getdeliveryAsBoardQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			
			m = service.selectDeliveryAsboardQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getdeliveryAsBoardDetailQry 
	 * Method 설명 : 배송게시판 상세
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_BOARD_DETAIL_QRY.if")	
	public void getdeliveryAsBoardDetailQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			
			m = service.selectDeliveryAsBoardDetailQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getdeliveryAsContentsSearch 
	 * Method 설명 : 배송게시판 제목 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_CONTENTS_SEARCH.if")	
	public void getdeliveryAsContentsSearch(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectDeliveryAsContentsSearch(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : InDlvsetComplete
	 * Method 설명 : 몰테일 주문 등록상태에서 입고, 배송 거점 지정, 출고 주문등록 진행
	 * DATE : 2019-08-18
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_IN_DLVSET_COMPLETE.if")
	public void InDlvsetComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateInDlvsetComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : deliveryChangeReqDate
	 * Method 설명 : 배송 예정일 변경
	 * 작성자 : MonkeySeok
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_CHANGE_REQ_DATE.if")
	public void deliveryChangeReqDate(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF010");

			service.receiveJSON(model, inputJSON);

			service.DeliveryChangeReqDate(model);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliveryCompleteNew
	 * Method 설명 : 파일을 포함한 Delivery 완료 처리 함수
	 * 작성자 : kdalma
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE_NEW.if")
	public void deliveryCompleteNew(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[6];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew(model);
			
			// Save photo files
			for(int index=0;index<6;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
		
	/*-
	 * Method ID : deliveryCompleteMorePhoto
	 * Method 설명 : 배송완료(사진 9장 까지)
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE_MORE_PHOTO.if")
	public void deliveryCompleteMorePhoto(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[12];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew(model);
			
			// Save photo files
			for(int index=0;index<12;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : DeliverySetDriver
	 * Method 설명 : 배송 기사 지정 모바일
	 * DATE : 2019-11-28
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_SET_DRIVER.if")
	public void DeliverySetDriver(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateDeliverySetDriver(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getdeliveryAsPhoneQry 
	 * Method 설명 : 전화번호로 배송 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_PHONE_CHANGE_QRY.if")	
	public void getdeliveryAsPhoneChangeQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			
			m = service.selectDeliveryAsPhoneChangeQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getdeliveryAsOrderQry 
	 * Method 설명 : 배송주문번호로 배송 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_ORDER_CHANGE_QRY.if")	
	public void getdeliveryAsOrderChangeQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectDeliveryAsOrderChangeQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getdeliveryAsInteriorPhotoQry 
	 * Method 설명 : 인테리어 해피콜 전화 고객 사진 조회 쿼리
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_INTERIOR_PHOTO_QRY.if")	
	public void getdeliveryAsInteriorPhotoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			
			m = service.selectDeliveryAsInteriorPhotoQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : deliveryComplete20200101
	 * Method 설명 : 배송완료(사진 12장 까지)
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE_20200101.if")
	public void deliveryComplete20200101(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[12];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			//log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew20200101(model);

			// Save photo files
			for(int index=0;index<12;index++) {
				saveImageFromBase64(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
		
	/*-
	 * Method ID : getdeliveryAsTraceQry 
	 * Method 설명 : 배송이력정보 조회 쿼리
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_TRACE_QRY.if")	
	public void getdeliveryAsTraceQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			
			m = service.selectDeliveryAsTraceQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
		
	/*-
	 * Method ID : saveImageFromBase64Folder
	 * Method 설명 : 파일을 포함한 Json request 의 저장함수 있다. 기존에 비해 폴더를 상세적으로 구성
	 * 작성자 : MonkeySeok
	 *
	 * @param model : mulitpart form request 객체
	 * @param inputJSON : file 이외의 json 데이터
	 * @param files : 첨부된 파일 array
	 * @throws Exception
	 */
	public void saveImageFromBase64Folder(String key, String key2, String fileName, String base64Data, String ord_id) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model2 = new HashMap<String, Object>();
		
		try {
			if(base64Data == null) return;
			
			String month;
			 
	        Calendar cal = Calendar.getInstance(Locale.getDefault());
	 
	        StringBuffer buf = new StringBuffer();
	 
	        buf.append(Integer.toString(cal.get(Calendar.YEAR)));
	        month = Integer.toString(cal.get(Calendar.MONTH) + 1);
	        if (month.length() == 1) {
	            month = "0" + month;
	        }
	        
	        key = key.replaceAll("[*]", "-");
	        key2 = key2.replaceAll("[*]", "-");

			String path = ConstantWSIF.DELIVERY_IMG_PATH + "KGK" +"/"+ month +"/"+ DateUtil.getDateByPattern2() +"/"+ key2;
	    
			String file_id = key+fileName;
	        
			InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));

			model2.put("ORD_ID", ord_id);
			model2.put("FILE_ID", file_id);
			model2.put("ATTACH_GB", "INTERFACE");
			model2.put("FILE_VALUE", "wmssp010");
			model2.put("FILE_PATH", path);
			model2.put("FILE_NAME", fileName);
			model2.put("FILE_EXT", ".jpg");
			
			try { 
				m = service.DeliveryImgDataInsert(model2);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save confirm :", e);
				}
				m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
		}
	}

	/*-
	 * Method ID : saveImageFromBase64Folder2
	 * Method 설명 : 파일을 포함한 Json request 의 저장함수 있다. 기존에 비해 폴더를 상세적으로 구성
	 * 작성자 : MonkeySeok
	 *
	 * @param model : mulitpart form request 객체
	 * @param inputJSON : file 이외의 json 데이터
	 * @param files : 첨부된 파일 array
	 * @throws Exception
	 */
	public void saveImageFromBase64Folder2(String key, String key2, String key3, String fileName, String base64Data, String ord_id) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model2 = new HashMap<String, Object>();
		
		try {
			if(base64Data == null) return;
			
			String month;
			 
	        Calendar cal = Calendar.getInstance(Locale.getDefault());
	 
	        StringBuffer buf = new StringBuffer();
	 
	        buf.append(Integer.toString(cal.get(Calendar.YEAR)));
	        month = Integer.toString(cal.get(Calendar.MONTH) + 1);
	        if (month.length() == 1) {
	            month = "0" + month;
	        }
	        
	        key = key.replaceAll("[*]", "-");
	        key2 = key2.replaceAll("[*]", "-");
	        	 			
			String path = ConstantWSIF.DELIVERY_IMG_PATH + key3 +"/"+ month +"/"+ DateUtil.getDateByPattern2() +"/"+ key2;
			String file_id = key+fileName;
			InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));
			
			model2.put("ORD_ID", ord_id);
			model2.put("FILE_ID", file_id);
			model2.put("ATTACH_GB", "INTERFACE");
			model2.put("FILE_VALUE", "wmssp010");
			model2.put("FILE_PATH", path);
			model2.put("FILE_NAME", fileName);
			model2.put("FILE_EXT", ".jpg");
			
			try {
				m = service.DeliveryImgDataInsert(model2);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save confirm :", e);
				}
				m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliveryComplete20200403
	 * Method 설명 : 배송완료(사진 12장 까지)
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE_20200403.if")
	public void deliveryComplete20200403(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[12];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			
			String original_key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String original_key2 = request.getParameter("key2");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew20200101(model);

			// Save photo files
			for(int index=0;index<12;index++) {
				saveImageFromBase64Folder(key, key2, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64Folder(key, key2, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : deliveryComplete20200407
	 * Method 설명 : 배송완료(사진 12장 까지)
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE_20200407.if")
	public void deliveryComplete20200407(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[12];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			
			String original_key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String original_key2 = request.getParameter("key2");
			String original_key3 = request.getParameter("key3");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew20200101(model);
				
			// Save photo files
			for(int index=0;index<12;index++) {
				saveImageFromBase64Folder2(key, key2,  key3, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64Folder2(key, key2,  key3, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : deliveryComplete20200614
	 * Method 설명 : 배송완료(사진 12장 까지)_string2048
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE_20200614.if")
	public void deliveryComplete20200614(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	

		try {
			String[] files = new String[12];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			
			String original_key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String original_key2 = request.getParameter("key2");
			String original_key3 = request.getParameter("key3");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew20200614(model);
				
			// Save photo files
			for(int index=0;index<12;index++) {
				saveImageFromBase64Folder2(key, key2,  key3, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64Folder2(key, key2,  key3, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}

	}

		
	/*-
	 * Method ID : deliveryComplete12Rbf20230717
	 * Method 설명 : 배송완료(사진 12장 까지)_string2048 -> requestBody String 객체 변경 (아이로브)
	 * 작성자 : jicho
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping(value = "/IF_AS_DELIVERY_COMPLETE_12RBF_20230717.if", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
	public @ResponseBody void deliveryComplete12Rbf20230717(@RequestBody Map<String, String> requestBody, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[12];
			files[0] = (String) requestBody.get("file0");
			files[1] = (String) requestBody.get("file1");
			files[2] = (String) requestBody.get("file2");
			files[3] = (String) requestBody.get("file3");
			files[4] = (String) requestBody.get("file4");
			files[5] = (String) requestBody.get("file5");
			files[6] = (String) requestBody.get("file6");
			files[7] = (String) requestBody.get("file7");
			files[8] = (String) requestBody.get("file8");
			files[9] = (String) requestBody.get("file9");
			files[10] = (String) requestBody.get("file10");
			files[11] = (String) requestBody.get("file11");
			
			String original_key = (String) requestBody.get("key");
			String sign = (String) requestBody.get("sign");			
			String ord_id = (String) requestBody.get("img_ord_id");
			String original_key2 = (String) requestBody.get("key2");
			String original_key3 = (String) requestBody.get("key3");
			String inputJSON = new String(Base64.decodeBase64((String)requestBody.get("json_data")), "UTF-8");
			
			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew20200614(model);
				
			// Save photo files
			for(int index=0;index<12;index++) {
				saveImageFromBase64Folder2(key, key2,  key3, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64Folder2(key, key2,  key3, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : deliveryComplete20211231
	 * Method 설명 : 배송완료(사진 12장 까지)_string2048_Header Detail
	 * 날 짜 : 2021-12-30
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE_20211231.if")
	public void deliveryComplete20211231(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[21];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			files[12] = request.getParameter("file12");
			files[13] = request.getParameter("file13");
			files[14] = request.getParameter("file14");
			files[15] = request.getParameter("file15");
			files[16] = request.getParameter("file16");
			files[17] = request.getParameter("file17");
			files[18] = request.getParameter("file18");
			files[19] = request.getParameter("file19");
			files[20] = request.getParameter("file20");
			
			String original_key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String original_key2 = request.getParameter("key2");
			String original_key3 = request.getParameter("key3");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew20211231(model);
				
			// Save photo files
			for(int index=0;index<21;index++) {
				saveImageFromBase64Folder2(key, key2,  key3, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64Folder2(key, key2,  key3, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : deliveryComplete20220501
	 * Method 설명 : 배송완료(사진 20장 까지)_string2048_Header Detail
	 * 날 짜 : 2022-05-01
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_COMPLETE_20220501.if")
	public void deliveryComplete20220501(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[21];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			files[12] = request.getParameter("file12");
			files[13] = request.getParameter("file13");
			files[14] = request.getParameter("file14");
			files[15] = request.getParameter("file15");
			files[16] = request.getParameter("file16");
			files[17] = request.getParameter("file17");
			files[18] = request.getParameter("file18");
			files[19] = request.getParameter("file19");
			files[20] = request.getParameter("file20");
			
			String original_key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String original_key2 = request.getParameter("key2");
			String original_key3 = request.getParameter("key3");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryCompleteNew20220501(model);
				
			// Save photo files
			for(int index=0;index<21;index++) {
				saveImageFromBase64Folder2(key, key2,  key3, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64Folder2(key, key2,  key3, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : runSpeliveryDriverSetQry
	 * Method 설명 : 배송기사별 배송예정일정 조회 
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2020-06-18
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_DRIVER_SET_QRY.if")	
	public void getdeliveryAsDriverSetQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			
			m = service.selectDeliveryAsDriverSetQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		

	/*-
	 * Method ID : deliveryTimeSetComplete
	 * Method 설명 : 배송기사별 배송시간 설정
	 * 작성자 : SMICS
	 * 날 짜 : 2020.06.18
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_TIME_SET_COMPLETE.if")
	public void deliveryTimeSetComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			//String data = getInputStream(inputJSON);

			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF070");
			
			service.receiveJSON(model, inputJSON);

			service.updateDeliveryTimeSetComplete(model);
		
			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : getdeliveryAsExtQry 
	 * Method 설명 : 배송정보조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_EXT_SEARCH.if")	
	public void getdeliveryAsExtQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			m = service.selectDeliveryAsExtQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : getParcelOrdSearchQry 
	 * Method 설명 : 택배 주문 정보조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_PARCEL_ORD_SEARCH.if")	
	public void getParcelOrdSearchQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectParcelOrdSearchQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : deliveryParcelComplete
	 * Method 설명 : 택배배송완료
	 * 작성자 : monkeySeok
	 * 날짜 : 2021-02-15
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_PARCEL_COMPLETE.if")
	public void deliveryParcelComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[12];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			
			String original_key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String original_key2 = request.getParameter("key2");
			String original_key3 = request.getParameter("key3");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryParcelComplete(model);
				
			// Save photo files
			//for(int index=0;index<12;index++) {
			//	saveImageFromBase64Folder2(key, key2,  key3, String.format("photo%d.jpg", index), files[index], ord_id);
			//}
			// Save Signature file
			//saveImageFromBase64Folder2(key, key2,  key3, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : deliveryImgSender
	 * Method 설명 : 배송사진전송(사진 12장 까지)_string2048
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_IMG_SENDER.if")
	public void deliveryImgSender(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[12];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			
			String original_key = request.getParameter("key");
			String ord_id = request.getParameter("img_ord_id");
			String original_key2 = request.getParameter("key2");
			String original_key3 = request.getParameter("key3");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");

			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;

	        int img_pre = Integer.parseInt("20");

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.DeliveryImgSender(model);

			// Save photo files
			for(int index=0;index<9;index++) {
				saveImageFromBase64Folder2(key, key2,  key3, String.format("photo%d.jpg", img_pre+index), files[index], ord_id);
			}

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : deliveryFunk
	 * Method 설명 : 배송 펑크 완료
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_FUNK.if")
	public void deliveryFunk(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[12];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			
			String original_key = request.getParameter("key");
			String ord_id = request.getParameter("img_ord_id");
			String original_key2 = request.getParameter("key2");
			String original_key3 = request.getParameter("key3");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			String key = URLDecoder.decode(original_key, "UTF-8") ;
			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;

			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);
				
			// Save photo files
			for(int index=0;index<12;index++) {
				saveImageFromBase64Funk(key, key2,  key3, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// m.put("MSG", MessageResolver.getMessage("complete"));

			service.DeliveryCompleteNew20200614(model);

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : saveImageFromBase64Funk
	 * Method 설명 : 파일을 포함한 Json request 의 저장함수 있다. 기존에 비해 폴더를 상세적으로 구성
	 * 작성자 : MonkeySeok
	 *
	 * @param model : mulitpart form request 객체
	 * @param inputJSON : file 이외의 json 데이터
	 * @param files : 첨부된 파일 array
	 * @throws Exception
	 */
	public void saveImageFromBase64Funk(String key, String key2, String key3, String fileName, String base64Data, String ord_id) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model2 = new HashMap<String, Object>();
		
		try {
			if(base64Data == null) return;
			
			String month;
			 
	        Calendar cal = Calendar.getInstance(Locale.getDefault());
	 
	        StringBuffer buf = new StringBuffer();
	 
	        buf.append(Integer.toString(cal.get(Calendar.YEAR)));
	        month = Integer.toString(cal.get(Calendar.MONTH) + 1);
	        if (month.length() == 1) {
	            month = "0" + month;
	        }
	        
	        key = key.replaceAll("[*]", "-");
	        key2 = key2.replaceAll("[*]", "-");
	        	 			
			String path = ConstantWSIF.DELIVERY_IMG_PATH + key3 +"/"+ month +"/"+ DateUtil.getDateByPattern2() +"/"+ key2;
			String file_id = key+fileName;
			InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));
			
			model2.put("ORD_ID", ord_id);
			model2.put("FILE_ID", file_id);
			model2.put("ATTACH_GB", "DLV_FUNK");
			model2.put("FILE_VALUE", "wmssp010");
			model2.put("FILE_PATH", path);
			model2.put("FILE_NAME", fileName);
			model2.put("FILE_EXT", ".jpg");
			
			try {
				m = service.DeliveryImgDataInsert(model2);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save confirm :", e);
				}
				m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
		}
	}

	/*-
	 * Method ID : getdeliveryAsB2pSearchQry 
	 * Method 설명 : B2P 차량별 배차 정보 조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_AS_DELIVERY_B2P_SEARCH.if")	
	public void getdeliveryAsB2pSearchQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF091");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectDeliveryAsB2pSearchQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : receiveJSONWithFiles
	 * Method 설명 : 파일을 포함한 Json request 의 저장함수
	 * 작성자 : kdalma
	 *
	 * @param model : mulitpart form request 객체
	 * @param inputJSON : file 이외의 json 데이터
	 * @param files : 첨부된 파일 array
	 * @throws Exception
	 */
	public void saveImageFromBase64Thirty(String key, String fileName, String base64Data, String ord_id) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model2 = new HashMap<String, Object>();
		
		try {
			if(base64Data == null) return;
			
			String path = ConstantWSIF.WMS_IMG_PATH + DateUtil.getDateByPattern2() +"/"+ key;
			String file_id = key+fileName;
			InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));
			
			model2.put("ORD_ID", ord_id);
			model2.put("FILE_ID", file_id);
			model2.put("ATTACH_GB", "WMS");
			model2.put("FILE_VALUE", "WMSOP");
			model2.put("FILE_PATH", path);
			model2.put("FILE_NAME", fileName);
			model2.put("FILE_EXT", ".jpg");
			
			try {
				m = service.DeliveryImgDataInsertThirty(model2);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save confirm :", e);
				}
				m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
		}
	}
	
	/*-
	 * Method ID : deliveryb2pStart
	 * Method 설명 : b2p 배송출발
	 * DATE : 2021-08-05
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVE_B2P_START.if")
	public void deliveryb2pStart(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updatedeliveryb2pStart(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : deliveryb2pComplete
	 * Method 설명 : b2p배송완료(사진 30장 까지)_string2048
	 * 작성자 : monkeySeok
	 *
	 * @param request : mulitpart form request 객체
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_AS_DELIVERY_B2P_COMPLETE.if")
	public void deliveryb2pComplete(MultipartHttpServletRequest request, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();	
		
		try {
			String[] files = new String[30];
			files[0] = request.getParameter("file0");
			files[1] = request.getParameter("file1");
			files[2] = request.getParameter("file2");
			files[3] = request.getParameter("file3");
			files[4] = request.getParameter("file4");
			files[5] = request.getParameter("file5");
			files[6] = request.getParameter("file6");
			files[7] = request.getParameter("file7");
			files[8] = request.getParameter("file8");
			files[9] = request.getParameter("file9");
			files[10] = request.getParameter("file10");
			files[11] = request.getParameter("file11");
			files[12] = request.getParameter("file12");
			files[13] = request.getParameter("file13");
			files[14] = request.getParameter("file14");
			files[15] = request.getParameter("file15");
			files[16] = request.getParameter("file16");
			files[17] = request.getParameter("file17");
			files[18] = request.getParameter("file18");
			files[19] = request.getParameter("file19");
			files[20] = request.getParameter("file20");
			files[21] = request.getParameter("file21");
			files[22] = request.getParameter("file22");
			files[23] = request.getParameter("file23");
			files[24] = request.getParameter("file24");
			files[25] = request.getParameter("file25");
			files[26] = request.getParameter("file26");
			files[27] = request.getParameter("file27");
			files[28] = request.getParameter("file28");
			files[29] = request.getParameter("file29");
			
//			String original_key = request.getParameter("key");
//			String ord_id = request.getParameter("img_ord_id");
//			String original_key2 = request.getParameter("key2");
//			String original_key3 = request.getParameter("key3");
//			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
//			
//			String key = URLDecoder.decode(original_key, "UTF-8") ;
//			String key2 = URLDecoder.decode(original_key2, "UTF-8") ;
//			String key3 = URLDecoder.decode(original_key3, "UTF-8") ;
//
//			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
//
//			model.put("SAVE_JSON_FILE", "Y");
//			model.put("SAVE_DOC_INFO", "Y");
//
//			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
//			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
//			model.put("D_DOC_CODE", "WMSIF091");
//
//			service.receiveJSON(model, inputJSON);
//
//			service.Deliveryb2pComplete(model);
//				
//			// Save photo files
//			for(int index=0;index<30;index++) {
//				saveImageFromBase64Folder2(key, key2,  key3, String.format("photo%d.jpg", index), files[index], ord_id);
//			}
			
			String key = request.getParameter("key");
			String sign = request.getParameter("sign");			
			String ord_id = request.getParameter("img_ord_id");
			String inputJSON = new String(Base64.decodeBase64(request.getParameter("json_data")), "UTF-8");
			
			//System.out.println(inputJSON); // print log
			log.info(" ********** inputJSON ********** : " + inputJSON);
			
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);

			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");

			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_DELIVERY_COMPLETE_DATA);
			model.put("D_DOC_CODE", "WMSIF091");

			service.receiveJSON(model, inputJSON);

			service.Deliveryb2pComplete(model);
			
			// Save photo files
			for(int index=0;index<30;index++) {
				saveImageFromBase64_30(key, String.format("photo%d.jpg", index), files[index], ord_id);
			}
			// Save Signature file
			saveImageFromBase64_30(key, "sign.jpg", sign, ord_id);

			// m.put("MSG", MessageResolver.getMessage("complete"));

		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : saveImageFromBase64_30
	 * Method 설명 : 파일을 포함한 Json request 의 저장함수
	 * 작성자 : kdalma
	 *
	 * @param model : mulitpart form request 객체
	 * @param inputJSON : file 이외의 json 데이터
	 * @param files : 첨부된 파일 array
	 * @throws Exception
	 */
	public void saveImageFromBase64_30(String key, String fileName, String base64Data, String ord_id) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model2 = new HashMap<String, Object>();
		
		try {
			if(base64Data == null) return;
			
			String path = ConstantWSIF.WMS_IMG_PATH + DateUtil.getDateByPattern2() +"/"+ key;
			String file_id = key+fileName;
			InterfaceUtil.saveBytesToFile(path, fileName, Base64.decodeBase64(base64Data));
			
			model2.put("ORD_ID", ord_id);
			model2.put("FILE_ID", file_id);
			model2.put("ATTACH_GB", "WMS");
			model2.put("FILE_VALUE", "WMSOP");
			model2.put("FILE_PATH", path);
			model2.put("FILE_NAME", fileName);
			model2.put("FILE_EXT", ".jpg");
			
			try {
				m = service.DeliveryImgDataInsert(model2);
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to save confirm :", e);
				}
				m.put("MSG", MessageResolver.getMessage("save.error"));
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
		}
	}
	
}
