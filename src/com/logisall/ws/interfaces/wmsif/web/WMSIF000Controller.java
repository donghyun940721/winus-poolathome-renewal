package com.logisall.ws.interfaces.wmsif.web;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.lowagie.text.List;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF000Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF000Service")
	private WMSIF000Service service;

	/*-
	 * Method ID	: crossDomainHttpWsMobile
	 * Method 설명	: 웹서비스 전송 공통모듈
	 * 작성자			: W
	 */
	@RequestMapping("/crossDomainHttpWsMobile.if")
	public ModelAndView crossDomainHttpWs(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String cMethod		= request.getParameter("cMethod");
		String cUrl			= request.getParameter("cUrl");
		String pOrdId		= request.getParameter("pOrdId");
		model.put("cMethod"		, cMethod);
		model.put("cUrl"		, cUrl);
		model.put("pOrdId"		, pOrdId);
		
		try {
			m = service.crossDomainHttpWsMobile(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: crossDomainKakaoSms
	 * Method 설명	: 웹서비스 전송 공통모듈
	 * 작성자			: W
	 */
	@RequestMapping("/crossDomainKakaoSms.if")
	public ModelAndView crossDomainKakaoSms(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		String data		= request.getParameter("data");
		String flag		= request.getParameter("flag");
		model.put("data", data);
		model.put("flag", flag);
		if(flag == null) { flag = ""; }
		try {
			m = service.crossDomainKakaoSms(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		System.out.println(m.toString());
		mav.addAllObjects(m);
		return mav;
	}

	
	
	
	
	/*-
	 * Method ID	: crossDomain
	 * Method 설명	: 웹서비스 전송 공통모듈 (KCC)
	 * 작성자			: W
	 */
	
	
	
	@RequestMapping("/crossDomainHttpWsKcc.if")
	public ModelAndView crossDomainHttpWsKcc(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String cMethod		= request.getParameter("cMethod");
		String cUrl			= request.getParameter("cUrl");
		String jsonString	= request.getParameter("formData");
		String data		= request.getParameter("data");
		
		model.put("cMethod"		, cMethod);
		model.put("cUrl"		, cUrl);
		model.put("jsonString"	, jsonString);
		model.put("data", data);
		
		try {
			m = service.crossDomainHttpWsKcc(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		System.out.println(m.toString());
		mav.addAllObjects(m);
		return mav;
	}
}