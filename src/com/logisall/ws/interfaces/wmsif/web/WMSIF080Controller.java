package com.logisall.ws.interfaces.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF080Service;
import com.lowagie.text.List;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Controller
public class WMSIF080Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF080Service")
	private WMSIF080Service service;
	
	/*-
	 * Method ID : getStockQry 
	 * Method 설명 : 현재고 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_STOCK_QRY.if")	
	public void getStockQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			m = service.selectStockQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getStockInoutQry 
	 * Method 설명 : 현재고 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_STOCK_INOUT_QRY.if")	
	public void getStockInoutQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectStockInoutQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getStockLocationQry 
	 * Method 설명 : 로케이션별 현재고 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_STOCK_LOCATION_QRY.if")	
	public void getStockLocationQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LOC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LOC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LOC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			m = service.selectStockLocationQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getStockLotQry 
	 * Method 설명 : LOT별 현재고 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_STOCK_LOT_QRY.if")	
	public void getStockLotQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectStockLotQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getStockPriorityQry 
	 * Method 설명 : 재고 선입선출 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_STOCK_PRIORITY_QRY.if")	
	public void getStockPriorityQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_QTY) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_QTY, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_QTY)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectStockPriorityQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getRealtimeStockConfirmQry 
	 * Method 설명 : 이재매핑과 같이 작업 후 남은 재고 조회
	 * 작성자 : smics
	 * 작성일 : 2016.09.30
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_REALTIME_STOCK_CONFRIM_QRY.if")	
	public void getRealtimeStockConfirmQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			m = service.selectRealtimeStockConfirmQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getMappingStatConfirmQry 
	 * Method 설명 : 매핑 후 매핑 상태 조회
	 * 작성자 : smics
	 * 작성일 : 2016.09.30
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_MAPPING_STAT_CONFRIM_QRY.if")	
	public void getMappingStatConfirmQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			m = service.selectMappingStatConfirmQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getSearchLocationQry 
	 * Method 설명 : 로케이션에 적재되어 있는 정보 확인
	 * 작성자 : smics
	 * 작성일 : 2017.02.16
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_SEARCH_LOCATION_QRY.if")	
	public void getSearchLocationQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_FROM_LOC_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_FROM_LOC_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_FROM_LOC_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectSearchLocationQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		

	/*-
	 * Method ID : getSetPartConfirmQry 
	 * Method 설명 : 완성품의 부분품 구성 정보 조회
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_SET_PART_CONFIRM_QRY.if")	
	public void getSetPartConfirmQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectSetPartConfirmQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : getSetStatQry 
	 * Method 설명 : 임가공 상태 정보 확인
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_SET_STAT_QRY.if")	
	public void getSetStatQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectSetStatQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/*-
	 * Method ID : LocSearchIdQry 
	 * Method 설명 : 로케이션 정보 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LOC_SEARCH_ID_QRY.if")	
	public void getLocSearchIdQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectLocSearchIdQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getLotItemStockQry 
	 * Method 설명 : 현재고 정보 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LOT_ITEM_STOCK_QRY.if")	
	public void getLotItemStockQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
						
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectLotItemStockQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		
	
	/*-
	 * Method ID : getDlvInoutStatQry 
	 * Method 설명 : 일별 배송 정보 조회 
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_DLV_INOUT_STAT_QRY.if")	
	public void getDlvInoutStatQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
						
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectDlvInoutStatQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	@RequestMapping("/IF_ITEM_INFO_QRY.if")	
	public void getItemInfoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectItemInfoQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
		
	@RequestMapping("/IF_RTI_TRACE_QRY.if")	
	public void getRtiTraceQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD)[0]);
			}
			
			m = service.selectRtiTraceQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : getRitemSerialInfoQry 
	 * Method 설명 : cust_lot_no 상태 조회
	 * 작성자 : smics
	 * 날짜 : 2019-11-08
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_RITEM_SERIAL_INFO_QRY.if")	
	public void getRitemSerialInfoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectRitemSerialInfoQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : ReqHelpdesk
	 * Method 설명 : 헬프데스크 등록(모바일)
	 * DATE : 2019-12-02
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_REQ_HELPDESK.if")
	public void ReqHelpdesk(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateReqHelpdesk(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	

	/*-
	 * Method ID : ItemBarcodeSender
	 * Method 설명 : 상품바코드 등록
	 * DATE : 2019-12-04
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_ITEM_BARCODE_SENDER.if")
	public void ItemBarcodeSender(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateItemBarcodeSender(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}			
	
	/*-
	 * Method ID : Stocktaking_first
	 * Method 설명 : 재고실사 후 초기 재고 등록
	 * DATE : 2019-12-06
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_STOCKTAKING_FIRST.if")
	public void StocktakingFirst(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateStocktakingFirst(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : getRefurbishedSerialQry 
	 * Method 설명 : 리퍼브 시리얼을 통한 주문 정보 조회(리퍼스 사진에 사용)
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_REFURBISHED_SERIAL_QRY.if")	
	public void getRefurbishedSerialQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF060");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO)[0]);
			}
			
			m = service.selectRefurbishedSerialQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}

	/*-
	 * Method ID : getSubulQry 
	 * Method 설명 : 수불 조회 
	 * 날짜 : 2020-05-13
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_SUBUL_QRY.if")	
	public void getSubulQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			m = service.selectSubulQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getLabelPrintingInfoQry 
	 * Method 설명 : 레이블 발행 정보 조회 
	 * 날짜 : 2020-07-14
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LABEL_PRINTING_INFO_QRY.if")	
	public void getLabelPrintingInfoQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectLabelPrintingInfoQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getEdiyaNotDepartQry 
	 * Method 설명 : 이디야 미배송 데이터 조회
	 * 날짜 : 2020-09-10
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_EDIYA_NOT_DEPART_QRY.if")	
	public void getEdiyaNotDepartQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectEdiyaNotDepartQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	

	/*-
	 * Method ID : ItemUomRegComplete
	 * Method 설명 : 상품 UOM 환산 정보 등록
	 * DATE : 2020-10-07
	 * 작성자 : SMICS
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_ITEM_UOM_REG_COMPLETE.if")
	public void ItemUomRegComplete(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF060");
			
			log.info("inputJSON==============="+inputJSON);

			service.receiveJSON(model, inputJSON);

			service.updateItemUomRegComplete(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}

	/*-
	 * Method ID : DasCheckCompleteMobile
	 * Method 설명 : DAS 검수 확정 전송
	 * 작성자 : SMICS
	 * 날 짜 : 2020-10-12
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_DAS_CHECK_COMPLETE_MOBILE.if")
	public void DasCheckCompleteMobile(@RequestBody String inputJSON, HttpServletResponse response) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			model.put(ConstantWSIF.KEY_JSON_STRING, inputJSON);
			
			m.put("SAVE_JSON_FILE", "Y");
			m.put("SAVE_DOC_INFO", "Y");

			m.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			m.put("D_DOC_CODE", "WMSIF050");
			
			service.receiveJSON(model, inputJSON);

			service.updateDasCheckCompleteMobile(model);
		
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getEdiyaDeviceQry 
	 * Method 설명 : 이디야 설비데이터 조회
	 * 날짜 : 2020-11-03
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_EDIYA_DEVICE_QRY.if")	
	public void getEdiyaDeviceQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectEdiyaDeviceQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getSupplementListQry 
	 * Method 설명 : 보충 리스트 조회
	 * 날짜 : 2020-11-09
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_SUPPLEMENT_LIST_QRY.if")	
	public void getSupplementListQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_RITEM_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_RITEM_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectSupplementListQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getLocHeaderListQry 
	 * Method 설명 : 로케이션 헤더 정보 조회
	 * 날짜 : 2020-12-03
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LOC_HEADER_LIST_QRY.if")	
	public void getLocHeaderListQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectLocHeaderListQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getEdiyaFactoryLabelListQry 
	 * Method 설명 : 이디야 공장 레이블 발행 리스트 조회
	 * 날짜 : 2021-02-24
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_EDIYA_FACTORY_LABEL_LIST_QRY.if")	
	public void getEdiyaFactoryLabelListQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectEdiyaFactoryLabelListQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
	
	/*-
	 * Method ID : getLcBoxListQry 
	 * Method 설명 : 포장 박스 리스트 조회
	 * 날짜 : 2021-06-30
	 * 작성자 : MonkeySeok
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_LC_BOX_LIST_QRY.if")	
	public void getLcBoxListQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF080");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}

			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_LC_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_LC_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_ORD_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DATE, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DATE)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD)[0]);
			}
			
			m = service.selectLcBoxListQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.sendJSON(response, m, model);
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}	
}
