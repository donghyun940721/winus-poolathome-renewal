package com.logisall.ws.interfaces.wmsif.web;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.OrderWebService;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF101Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Controller
public class WMSIF101Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF101Service")
	private WMSIF101Service service;
	
	/*-
	 * Method ID : ifHowereDlvData
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 * 날짜 : 2019-07-11
	 * 내용 : 하우저 배송완료, 수정 데이터 인터페이스
	 * 
	 *
	 * @param inputJSON
	 * @param response
	 * @throws Exception
	 */
	@RequestMapping("/IF_HOWSER_DLV_DATA.if")
	
	public void ifHowereDlvData(@RequestBody String inputJSON, HttpServletResponse response, HttpServletRequest request) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();

		try {
			//UTF-8 인코딩
			response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			request.setCharacterEncoding("UTF-8");
			String jsonData = new String (request.getParameter("jsonData").getBytes ("iso-8859-1"), "UTF-8");
	        
			model.put(ConstantWSIF.KEY_JSON_STRING, jsonData);
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");
			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_IN_RECEIVE_COMPLETE);
			model.put("D_DOC_CODE", "WMSIF101");

			service.updateifHowereDlvData(model);
			
		} catch (InterfaceException ife) {
			m = new HashMap<String, Object>();
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());

			String sendData = ""; //sendData = "{\"SEND_DATA\":\"FAIL\"}";
			sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_S)
										 .append("\"")
										 .append(ConstantWSIF.KEY_JSON_RESPONSE_START)
										 .append("\"")
										 .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C)
										 .append("\"")
										 .append("FAIL")
										 .append("\"")
										 .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_E)
										 .append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString();
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			out.print(sendData);
			out.flush();   
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			
			m = new HashMap<String, Object>();
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
			
			String sendData = ""; //sendData = "{\"SEND_DATA\":\"FAIL\"}";
			sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_S)
										 .append("\"")
										 .append(ConstantWSIF.KEY_JSON_RESPONSE_START)
										 .append("\"")
										 .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C)
										 .append("\"")
										 .append("FAIL")
										 .append("\"")
										 .append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_E)
										 .append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString();
			PrintWriter out = response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			out.print(sendData);
			out.flush(); 
		}

		try {
			model.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
			service.sendResponse(response, model, m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to sendResponse :", e);
			}
		}
	}
	
	/*-
	 * Method ID : getLoginList 
	 * Method 설명 : 로그인
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/IF_SWEETTRACKER_DLV_QRY.if")	
	public void getSweettrackerDlvTraceQry(HttpServletRequest request, HttpServletResponse response) {
	Map<String, Object> m = new HashMap<String, Object>();
	Map<String, Object> model = new HashMap<String, Object>();
	try {
		model.put("SAVE_JSON_FILE", "Y");
		model.put("SAVE_DOC_INFO", "Y");

		model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
		model.put("D_DOC_CODE", "WMSIF101");
		
		if (request.getParameterMap() != null) {
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_LOGIN_ID)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD) != null) {
				model.put(ConstantWSIF.IF_KEY_PASSWORD, request.getParameterMap().get(ConstantWSIF.IF_KEY_PASSWORD)[0]);
			}
			
			if (request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ORD_ID) != null) {
				model.put(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ORD_ID, request.getParameterMap().get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ORD_ID)[0]);
			}
			
			m = service.selectSweettrackerDlvTraceQry(model);
		}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
	
		}
	
		try {
			//model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
			service.comSendResponse("SWEETTRACKER", response, m, model);
			service.sendJSON(response, m, model);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}		

	/*-
	 * Method ID : getTwelvecmInsertGoodsinfoQry 
	 * Method 설명 : 로그인
	 * 작성자 : smics
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/TWELVECM/TRACKING/SELECT.if")
	public void getTwelvecmGoodsinfoQry(HttpServletRequest request, HttpServletResponse response) {
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> model = new HashMap<String, Object>();
		
		try {
			model.put("SAVE_JSON_FILE", "Y");
			model.put("SAVE_DOC_INFO", "Y");
			model.put("FILE_PREFIX", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
			model.put("D_DOC_ID", ConstantWSIF.DOC_ID_IF_OUT_RECEIVING_LIST_QRY);
			model.put("D_DOC_CODE", "WMSIF101");
			
			if (request.getParameterMap() != null) {
				if ("m208019" != null){model.put(ConstantWSIF.IF_KEY_LOGIN_ID, "m208019");}
				if ("m1234" != null){model.put(ConstantWSIF.IF_KEY_PASSWORD, "m1234");}
											
				String body = null;
				StringBuilder stringBuilder = new StringBuilder();
				BufferedReader bufferedReader = null;
				InputStream inputStream = request.getInputStream();
				if(inputStream != null){
					bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
					char[] charBuffer = new char[128];
					int bytesRead = -1;
					while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
						stringBuilder.append(charBuffer, 0, bytesRead);
					}
				}else{
					stringBuilder.append("");
				}
		        body = stringBuilder.toString();

		        String referenceNo1 = null;
				String sendData		= new StringBuffer().append(body).toString();
				JsonParser Parser	= new JsonParser();
				JsonObject jsonObj	= (JsonObject) Parser.parse(sendData);
				JsonObject ObjTrace	= (JsonObject) jsonObj.get("trace");
				referenceNo1 = ObjTrace.get("referenceNo1").toString().replaceAll("\"", "");
				System.out.println("param ObjReferenceNo1 : " + referenceNo1);
				
				model.put(ConstantWSIF.IF_KEY_ROW_TWELVECM_TRACE_ORD_ID, referenceNo1);
				m = service.selectTwelvecmGoodsinfoQry(model);
				System.out.println("output select O_INVC_NO : " + m.get("O_INVC_NO"));
			}
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", ife);
			}
			m.put("ERRCODE", ife.getMsgCode());
			m.put("MSG", ife.getMessage());
		
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			m.put("ERRCODE", -1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		
		try {
			JaxWsProxyFactoryBean svr = new JaxWsProxyFactoryBean();
    		svr.setServiceClass(OrderWebService.class);	
    		svr.setAddress("http://osms.sf-express.com/osms/services/OrderWebService?wsdl");
    		OrderWebService orderWebService = svr.create(OrderWebService.class);
    		StringBuffer sb      =  new StringBuffer();
    		sb.append("<?xml version='1.0' encoding='utf-8'?>");
    		sb.append("<Request service='RouteService' lang='zh-CN'>");
    		sb.append("<Head>OSMS_8269</Head>");
    		sb.append("<Body>");
    		sb.append("<Route");
    		sb.append("	tracking_type='1'");
    		sb.append("	tracking_number='"+m.get("O_INVC_NO")+"'"); //mailno
    		sb.append("/>");
    		sb.append("</Body>");
    		sb.append("</Request>");
    		
    		//고정 시작
    		String xml = sb.toString();
    		System.out.println("xml>>*****************");
    		System.out.println("xml>> : " + xml);
    		System.out.println("xml>>*****************");
    		String checkWord ="561b32d1798e40cc";
    		String CustomerCode ="OSMS_8269"; 
    		String data = encodeBase64(xml);		
    		String md5 = DigestUtils.md5Hex(xml + checkWord);
    		String validateStr = encodeBase64(md5);
    		String result = orderWebService.sfexpressService(data, validateStr, CustomerCode);
    		//System.out.println("result>> : " + result);
    		//고정 끝
    		
    		JSONObject xmlJSONObj = XML.toJSONObject(result, true);
            String jsonString	  = xmlJSONObj.toString(4);
			JSONObject jsonData = new JSONObject(jsonString);
			JSONObject jsonRsResponse = jsonData.getJSONObject("Response");
			JSONObject jsonRsBody = jsonRsResponse.getJSONObject("Body");
    		try {
    			JSONObject jsonRsRouteResponse = jsonRsBody.getJSONObject("RouteResponse");
    			
    			Object myObj = jsonRsRouteResponse.opt("Route");
    			if( myObj instanceof JSONObject ){
    				JSONArray jsonArray = new JSONArray();
    				jsonArray.put(myObj);
    		
    				jsonRsRouteResponse.put("Route", jsonArray);
    				jsonRsBody.put("RouteResponse", jsonRsRouteResponse);
    				jsonRsResponse.put("Body", jsonRsBody);
    				jsonData.put("Response", jsonRsResponse);
    			}
    		} catch (Exception e) {
    			// sf 인터페이스 처리 오류 시 (송장번호가 없을 경우 등) 
    		} finally {
    			String sendData = new StringBuffer().append(jsonData.toString(4)).toString();
    			//System.out.println("sendData : "+sendData);
    			
    			response.setCharacterEncoding("UTF-8");
    			response.setContentType("text/html; charset=UTF-8");
    			response.setContentType("application/json; charset=UTF-8");
    			PrintWriter printWriter = null;
    			printWriter = response.getWriter();
    			printWriter.println(sendData);
    			printWriter.close();
    		}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to send JSON :", e);
			}
		}
	}
	
	/**
	 * Base64
	 * @param orderData
	 * @return
	 */
	private static String encodeBase64(String orderData) {
		String result = "";
		Base64 base64encoder = new Base64();
		try {
			result = new String(base64encoder.encode(orderData.getBytes("UTF-8")),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
}
