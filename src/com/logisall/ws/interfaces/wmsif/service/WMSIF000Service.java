package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF000Service extends AbstractInterfaceService {
	public Map<String, Object> crossDomainHttpWsMobile(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainKakaoSms(Map<String, Object> model) throws Exception;
	public String crossDomainSabangOrderHead(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWsKcc(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs5200(Map<String, Object> model) throws Exception;
}