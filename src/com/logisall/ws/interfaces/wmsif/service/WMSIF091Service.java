package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF091Service extends AbstractInterfaceService {

    public Map<String, Object> selectDeliveryAsQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsPhoneQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsOrderQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsOrderDetailQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsOrderDetailQry2(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsPhoneShortQry(Map<String, Object> model) throws Exception;
    public void DeliveryComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryManageComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> DeliveryImgDataInsert(Map<String, Object> model) throws Exception;
    public void DeliveryCancelComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryStartComplete(Map<String, Object> model) throws Exception; 
    public void DeliverySendMsgComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsDriverQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsDriverListQry(Map<String, Object> model) throws Exception;
    public void DeliveryFirstBadComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsPartInfoQry(Map<String, Object> model) throws Exception;
    public void deliveryExclusiveAsComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsAsErrorCdQry(Map<String, Object> model) throws Exception;
    public void DeliveryHolding(Map<String, Object> model) throws Exception; 
    public void DeliveryNotConnect(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsboardQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsBoardDetailQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsContentsSearch(Map<String, Object> model) throws Exception;
    public void updateInDlvsetComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryChangeReqDate(Map<String, Object> model) throws Exception; 
    public void DeliveryCompleteNew(Map<String, Object> model) throws Exception; 
    public void updateDeliverySetDriver(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsPhoneChangeQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsOrderChangeQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryAsInteriorPhotoQry(Map<String, Object> model) throws Exception;
    public void DeliveryCompleteNew20200101(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsTraceQry(Map<String, Object> model) throws Exception;
    public void DeliveryCompleteNew20200614(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsDriverSetQry(Map<String, Object> model) throws Exception;
    public void updateDeliveryTimeSetComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsExtQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectParcelOrdSearchQry(Map<String, Object> model) throws Exception;
    public void DeliveryParcelComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryImgSender(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsB2pSearchQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> DeliveryImgDataInsertThirty(Map<String, Object> model) throws Exception;
    public void Deliveryb2pComplete(Map<String, Object> model) throws Exception; 
    public void updatedeliveryb2pStart(Map<String, Object> model) throws Exception; 
    public void DeliveryCompleteNew20211231(Map<String, Object> model) throws Exception; 
    public void DeliveryCompleteNew20220501(Map<String, Object> model) throws Exception; 
}
