package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF102Service extends AbstractInterfaceService {

    public Map<String, Object> selectGodomallOrderInQry(Map<String, Object> model) throws Exception;
    public void updateGodomallOrderSearchComplete(Map<String, Object> model) throws Exception; 
    public void updateGodomallItemInfoComplete(Map<String, Object> model) throws Exception;     
    public void updateShopbuyOrderSearchComplete(Map<String, Object> model) throws Exception; 
    public void updateShopbuyItemSearchComplete(Map<String, Object> model) throws Exception; 
}
