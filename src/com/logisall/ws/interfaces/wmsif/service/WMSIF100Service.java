package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF100Service extends AbstractInterfaceService {

    public Map<String, Object> selectForkLiftQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwms1stReceivingProcessQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwms3rdReceivingProcessQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwmsReceivingProcessQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwmsReceivingPositionQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwmsLocPositionQry(Map<String, Object> model) throws Exception;
    public void forkliftInComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDensoStockQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDensoStockDetailQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDensoStockDailyQry(Map<String, Object> model) throws Exception;
    public String fcmSender(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectEcoproBMStockDailyQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> ifEcoproBmOutOrdComfRtnList(Map<String, Object> model) throws Exception;
    public Map<String, Object> ifEcoproBmOutOrdReturnComfRtnList(Map<String, Object> model) throws Exception;
    public void ifEcoproBmOutOrdComfRtnToMssql(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception;
    public void ifEcoproBmOutOrdReturnComfRtnToMssql(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception;
    public void ifEcoproBmDailyStockRtnToMssql(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception;
    public String fcmNaviSender(Map<String, Object> model) throws Exception;    
    public String fcmMapSender(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwmsMapLocPositionQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwmsMapLocMapSetInfoQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwmsAgvProcessQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectAgvOrderQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectUwmsAgvCompleteQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> agv_post_rest_api(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectNaviLoginQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectAnywareZoneInfoQry(Map<String, Object> model) throws Exception;
    public void updateEdiyaMobileCsComplete(Map<String, Object> model) throws Exception; 
    public void updateEdiyaMobileReturnComplete(Map<String, Object> model) throws Exception; 
}
