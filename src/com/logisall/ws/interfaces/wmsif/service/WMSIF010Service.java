package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF010Service extends AbstractInterfaceService {

	//public Map<String, Object> selectLoginList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectAcceptingList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectAcceptingQry(Map<String, Object> model) throws Exception;
    public void updateAcceptingComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectReceivingList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectInList(Map<String, Object> model) throws Exception;
    public void updateInComplete(Map<String, Object> model) throws Exception;
    public void mappingInComplete(Map<String, Object> model) throws Exception; 
    
}
