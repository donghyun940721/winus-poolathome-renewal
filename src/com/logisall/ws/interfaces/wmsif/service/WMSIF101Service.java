package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF101Service extends AbstractInterfaceService {

    public void updateifHowereDlvData(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectSweettrackerDlvTraceQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectTwelvecmGoodsinfoQry(Map<String, Object> model) throws Exception;
}
