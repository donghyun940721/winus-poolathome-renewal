package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF050Service extends AbstractInterfaceService {

	public Map<String, Object> selectLoginList(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectAuthQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectVersionQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInUsaOrderQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderMappingQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderDetailQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderRemainQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectPltConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderConfirmQry(Map<String, Object> model) throws Exception;
	public void updateReceiveInComplete(Map<String, Object> model) throws Exception;
	public void updateReceiveInPltComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectPltMappingConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLcRitemQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMapPltConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectEpcConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectEpcHexConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectCustIdQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectReceivingOrderStatQry(Map<String, Object> model) throws Exception;
	public void updateInAutoOrderInsert(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderLotDetailQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> save(Map<String, Object> model) throws Exception;
	public void updateSetPartComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectPartInfoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInLotStatInfoQry(Map<String, Object> model) throws Exception;
	public void updateFcmPhoneComplete(Map<String, Object> model) throws Exception;
	public void updateInBoxAutoOrderInsert(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderLotMultiQry(Map<String, Object> model) throws Exception;
	public void updateInAutoOrderSapInsert(Map<String, Object> model) throws Exception;
    public Map<String, Object> WmsImgDataInsert(Map<String, Object> model) throws Exception;
	public void updateInOrdLocCompleteMobile(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLcCarInfoQry(Map<String, Object> model) throws Exception;
	public void updateInTakingPicturesMobile(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInfoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderInsertQry(Map<String, Object> model) throws Exception;
	public void updateLocWorkingComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectTransCustInfoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectTransItemInfoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectInOrderDateQry(Map<String, Object> model) throws Exception;
	public void updateReceiveInOrdInsSimpleIn(Map<String, Object> model) throws Exception;
    public String fcmNaviSender(Map<String, Object> model) throws Exception;    
	public void updateReceiveAfterGrn(Map<String, Object> model) throws Exception;
	public void updatepackingPartInspection(Map<String, Object> model) throws Exception;
	public void updateReceiveOrderInsertConfirmGrn(Map<String, Object> model) throws Exception;
	public void updateReceiveReceivingCompleteGrn(Map<String, Object> model) throws Exception;
	public void updateReceivingOrdLocDayComplete(Map<String, Object> model) throws Exception;
	public void updateReceiveCancelGrnBadComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> ItemImgDataInsert(Map<String, Object> model) throws Exception;
	public void updateItemImgSenderMobile(Map<String, Object> model) throws Exception;
	public void updateLocDayInMobileComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectCustCalPltIdQry(Map<String, Object> model) throws Exception;
	public void updateInIssueLabel(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectIssueLabelInfoQry(Map<String, Object> model) throws Exception;
	public void updateReceiveInOrdInsDaySimpleIn(Map<String, Object> model) throws Exception;
	public void updateOrdDiviedCompleteMobile(Map<String, Object> model) throws Exception;
	public void updateOrdCngQtySimpleIn(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectFixLocConfirmQry(Map<String, Object> model) throws Exception;
	public void updateInIssueOrdLabel(Map<String, Object> model) throws Exception;
	public void updateOrdDiviedCompleteMobile2048(Map<String, Object> model) throws Exception;
	public void updateInIssueLbLabel(Map<String, Object> model) throws Exception;
	public void updateOrd2048LocCompleteMobile(Map<String, Object> model) throws Exception;
	public void updateInIssueLabelLbOrdComplete(Map<String, Object> model) throws Exception;

}
