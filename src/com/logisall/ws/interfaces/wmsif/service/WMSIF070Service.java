package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF070Service extends AbstractInterfaceService {

	public Map<String, Object> selectMappingDetailAsan(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMappingRemainQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMoveFromLocQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMoveToLocQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMappingLotConfirm(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLotRitemQtyQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMovePriorityQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMovePriorityEpcQry(Map<String, Object> model) throws Exception;
	public void updateTransferAllComplete(Map<String, Object> model) throws Exception;
	public void updateDemappingComplete(Map<String, Object> model) throws Exception;
	public void updateMappingCompleteAsan(Map<String, Object> model) throws Exception;
	public void updateLotMappingComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLotEpcQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectStocktakingIdQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLocIdQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectStocktakingTotalQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectStockInfoQry(Map<String, Object> model) throws Exception;
	public void updateStocktakingComplete(Map<String, Object> model) throws Exception;
	public void updateMappingCompleteMobile(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectIfMoveFromLocQry(Map<String, Object> model) throws Exception;
	public void updateRtiTransferComplete(Map<String, Object> model) throws Exception;
	public void updateKitGenMakeComplete(Map<String, Object> model) throws Exception;
	public void updateKitGenMakeCompleteInplt(Map<String, Object> model) throws Exception;
	public void updateKitDeleteComplete(Map<String, Object> model) throws Exception;  
	public Map<String, Object> selectDemappingDetailQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMoveTransQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMakeKitFifoQry(Map<String, Object> model) throws Exception;
	public void updateMovestockCmtComplete(Map<String, Object> model) throws Exception; 
	public Map<String, Object> selectItemZoneFifoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLotInfoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLotItemConfirmQry(Map<String, Object> model) throws Exception;
	public void updaterepackingComplete(Map<String, Object> model) throws Exception;  
	public Map<String, Object> selectMoveFromRefurbishedQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectSerialAsInfoQry(Map<String, Object> model) throws Exception;
	public void updaterefurbishedTransferComplete(Map<String, Object> model) throws Exception;  
	public void updaterefurbishedPartOutComplete(Map<String, Object> model) throws Exception;  
	public void updatekitMakeSerialComplete(Map<String, Object> model) throws Exception;  
	public Map<String, Object> selectMoveFromLocUnitQry(Map<String, Object> model) throws Exception;
	public void updatekitGenWorkseqComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectKitGenListInfoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLotDuplicationInfoQry(Map<String, Object> model) throws Exception;
	public void updateTransferAllMultiPart(Map<String, Object> model) throws Exception;
    public Map<String, Object> WmsImgDataInsert(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMoveItemFromDataQry(Map<String, Object> model) throws Exception;
	public void updateItemTransferAllComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLocInoutDataQry(Map<String, Object> model) throws Exception;
	public void updateInoutCheckComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMoveItemPriorityQry(Map<String, Object> model) throws Exception;
	public void updateLotLocResettingComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectTransferReadyInfoQry(Map<String, Object> model) throws Exception;
	public void updatekitMakeSerialWork(Map<String, Object> model) throws Exception;  
	public void updatemodifyStockComplete(Map<String, Object> model) throws Exception;  
	public void updateTransferQtyMulti(Map<String, Object> model) throws Exception;  
	public void updateKitGenMakeComplete2048(Map<String, Object> model) throws Exception;
	public void updateItemTransferAllMultiComplete(Map<String, Object> model) throws Exception;

}
