package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import org.springframework.web.bind.annotation.RequestBody;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF060Service extends AbstractInterfaceService {

	public Map<String, Object> selectOutOrderQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutOrderDetailQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutOrderMappingQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutOrderConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectShippingOrderStatQry(Map<String, Object> model) throws Exception;
	public void updateShippingInPltComplete(Map<String, Object> model) throws Exception;
	public void updateShippingInComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutAsnOrderQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutAsnOrderDetailQry(Map<String, Object> model) throws Exception;
	public void updateOutShippingCompleteAsn(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutLotOrderQry(Map<String, Object> model) throws Exception;
	public void updateOutShippingCompleteLot(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutAutoOrderStatQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutAutoOrderChangeQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutOrderInsertQry(Map<String, Object> model) throws Exception;
	public void updateOutAutoOrderInsert(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutOrderFeedingQry(Map<String, Object> model) throws Exception;
	public void updateOutFeedingComplete(Map<String, Object> model) throws Exception;
	public void updateOutFeedingCompleteForce(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutOrderFeedingFifoQry(Map<String, Object> model) throws Exception;
	public void updateOutFeedingCompleteFifo(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutEpcMapQry(Map<String, Object> model) throws Exception;
	public void updateOutCompleteSetForce(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutFroceLotOrdQry(Map<String, Object> model) throws Exception;
	public void updateoutInordComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutProcessQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutLocRemainQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutOrdRemainQry(Map<String, Object> model) throws Exception;
	public void updateoutInordProcessComplete(Map<String, Object> model) throws Exception;	
	public Map<String, Object> selectOutOrderLotQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLotOverlapConfirmQry(Map<String, Object> model) throws Exception;
	public void updateOutCompleteBoxForce(Map<String, Object> model) throws Exception;	
	public Map<String, Object> selectOutOrdIdOrderQry(Map<String, Object> model) throws Exception;
	public void updatepickingComplete(Map<String, Object> model) throws Exception;
	public void updateShippingPickedComplete(Map<String, Object> model) throws Exception;
	public void updatePickingCheckedComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> WmsImgDataInsert(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutLotOrderIdQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutLotUnitOrderIdQry(Map<String, Object> model) throws Exception;
	public void updateShippingPickedUnitComplete(Map<String, Object> model) throws Exception;
	public void updatepickingTransCustMobile(Map<String, Object> model) throws Exception;
	public void updateMultiShippingComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutLotConfirmQry(Map<String, Object> model) throws Exception;
    public String fcmNaviSender(Map<String, Object> model) throws Exception;   
	public void updateShippingCompleteMobile(Map<String, Object> model) throws Exception; 
	public void updateCancelPickingMobile(Map<String, Object> model) throws Exception; 
    public String deleteImgFolder(Map<String, Object> model) throws Exception;
	public void updateSetLocMappingOutMobile(Map<String, Object> model) throws Exception; 
	public void updatesimpleOutMobile(Map<String, Object> model) throws Exception;
	public void updateOrdersimpleOutMobile(Map<String, Object> model) throws Exception;
	public void updateLocMappingPickingComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutPickingListInfoQry(Map<String, Object> model) throws Exception;
	public void updateOrdInsSimpleOut(Map<String, Object> model) throws Exception;
	public void updateLotLocShippingComplete(Map<String, Object> model) throws Exception;
	public void updateshippingEdiyaNotDepart(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectOutTotalPickingQry(Map<String, Object> model) throws Exception;
	public void updatePickingTotalComplete(Map<String, Object> model) throws Exception;
	public void updateLocCancelAlloComplete(Map<String, Object> model) throws Exception;
	public void updateShippingSerialComplete(Map<String, Object> model) throws Exception;  
	public void updateCrossDomainHttpWsMobile(Map<String, Object> model) throws Exception;  
	public Map<String, Object> selectOutParcelInvcQry(Map<String, Object> model) throws Exception;
	public void updatePickingCheckComplete(Map<String, Object> model) throws Exception;
	public void updateTempCheckComplete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectParcelPickingCntQry(Map<String, Object> model) throws Exception;
	public void updateLocSetPickingCompleteMobile(Map<String, Object> model) throws Exception;
	public void updateLocSetPickingTotalCompleteMobile(Map<String, Object> model) throws Exception;

}
