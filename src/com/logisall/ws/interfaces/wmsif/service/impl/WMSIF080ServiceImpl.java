package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVO;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKR;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKRDate;
import com.logisall.ws.interfaces.wmsif.service.WMSIF080Service;

@Service("WMSIF080Service")
public class WMSIF080ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF080Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF080Dao")
	private WMSIF080Dao dao;

	@Autowired
	WMSIF080ServiceImpl(WMSIF080Dao dao) {
		super(dao);
	}
		
	public Map<String, Object> selectStockQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStockQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectStockInoutQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStockInoutQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectStockLocationQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStockLocationQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectStockLotQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStockLotQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectStockPriorityQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStockPriorityQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectRealtimeStockConfirmQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpRealtimeStockConfirmQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMappingStatConfirmQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMappingStatConfirmQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectSearchLocationQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpSearchLocationQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectSetPartConfirmQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpSetPartConfirmQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectSetStatQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpSetStatQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectLocSearchIdQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLocSearchIdQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectLotItemStockQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotItemStockQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectDlvInoutStatQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpDlvInoutStatQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectItemInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpItemInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectRtiTraceQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpRtiTraceQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public Map<String, Object> selectRitemSerialInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpRitemSerialInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public void updateReqHelpdesk(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVODlv data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONDlv(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}


			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVODlv(data);				
				if (log.isInfoEnabled()) {
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapDlv(data, workList);
					if (log.isInfoEnabled()) {
						log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						//log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpReqHelpdesk(modelIns);
					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF090", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}  
	
	public void updateItemBarcodeSender(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpItemBarcodeSender(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public void updateStocktakingFirst(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpStocktakingFirst(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public Map<String, Object> selectRefurbishedSerialQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpRefurbishedSerialQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectSubulQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpSubulQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectLabelPrintingInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLabelPrintingInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectEdiyaNotDepartQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpEdiyaNotDepartQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updateItemUomRegComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpItemUomRegComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		

	public void updateDasCheckCompleteMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpDasCheckCompleteMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	

	public Map<String, Object> selectEdiyaDeviceQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpEdiyaDeviceQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectSupplementListQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpSupplementListQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectLocHeaderListQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLocHeaderListQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectEdiyaFactoryLabelListQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpEdiyaFactoryLabelListQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectLcBoxListQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLcBoxListQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF080", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
//		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
//			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
//			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5013", "work_seq is NOT VALID");
//		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
//		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
//			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
//			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
//		}
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}

}
