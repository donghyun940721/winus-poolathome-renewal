package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF000Dao")
public class WMSIF000Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
}