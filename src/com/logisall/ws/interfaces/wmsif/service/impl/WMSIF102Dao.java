package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF102Dao")
public class WMSIF102Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpGodomallOrderInQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpGodomallOrderInQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif102.sp_godomall_order_in_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpGodomallOrderSearchComplete
	 * Method 설명 : 고도몰 주문 정보 수신
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpGodomallOrderSearchComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif102.sp_godomall_order_search_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpGodomallItemInfoComplete
	 * Method 설명 : 고도몰 상품 정보 수신
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpGodomallItemInfoComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif102.sp_godomall_item_info_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpShopbuyOrderSearchComplete
	 * Method 설명 : 샵바이 주문 정보 수신
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShopbuyOrderSearchComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif102.sp_shopbuy_order_search_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpShopbuyItemSearchComplete
	 * Method 설명 : 샵바이 상품 정보 수신
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShopbuyItemSearchComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif102.sp_shopbuy_item_search_complete", model);
		return model;
	}
}
