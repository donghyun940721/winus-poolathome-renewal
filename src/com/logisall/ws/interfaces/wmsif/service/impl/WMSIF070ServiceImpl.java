package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVO;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKR;
import com.logisall.ws.interfaces.wmsif.service.WMSIF070Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF070Service")
public class WMSIF070ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF070Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF070Dao")
	private WMSIF070Dao dao;

	@Autowired
	WMSIF070ServiceImpl(WMSIF070Dao dao) {
		super(dao);
	}
		
	
	public Map<String, Object> selectMappingDetailAsan(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMappingDetailAsan(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMappingRemainQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMappingRemainQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectMoveFromLocQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveFromLocQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectMoveToLocQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveToLocQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMappingLotConfirm(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMappingLotConfirm(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectLotRitemQtyQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotRitemQtyQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectMovePriorityQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMovePriorityQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMovePriorityEpcQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMovePriorityEpcQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectStocktakingIdQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStocktakingIdQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectLocIdQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLocIdQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectStocktakingTotalQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStocktakingTotalQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public void updateTransferAllComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTransferAllComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateDemappingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpDemappingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	} 

	public void updateMappingCompleteAsan(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpMappingCompleteAsan(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	
	public void updateLotMappingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpLotMappingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectLotEpcQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotEpcQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectStockInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStockInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public void updateStocktakingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpStocktakingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public void updateMappingCompleteMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpMappingCompleteMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		

	
	public Map<String, Object> selectIfMoveFromLocQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpIfMoveFromLocQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateRtiTransferComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpRtiTransferComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateKitGenMakeComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitGenMakeComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateKitGenMakeCompleteInplt(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitGenMakeCompleteInplt(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateKitDeleteComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitDeleteComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectDemappingDetailQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpDemappingDetailQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectMoveTransQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveTransQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectMakeKitFifoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMakeKitFifoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updateMovestockCmtComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpMovestockCmtComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	

	public Map<String, Object> selectLotInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectItemZoneFifoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpItemZoneFifoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
		
	public Map<String, Object> selectLotItemConfirmQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotItemConfirmQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updaterepackingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpRepackingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectMoveFromRefurbishedQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveFromRefurbishedQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}


	public Map<String, Object> selectSerialAsInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpSerialAsInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updaterefurbishedTransferComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSprefurbishedTransferComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updaterefurbishedPartOutComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSprefurbishedPartOutComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updatekitMakeSerialComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpkitMakeSerialComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectMoveFromLocUnitQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveFromLocUnitQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updatekitGenWorkseqComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpkitGenWorkseqComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectKitGenListInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpKitGenListInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}


	public Map<String, Object> selectLotDuplicationInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotDuplicationInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updateTransferAllMultiPart(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTransferAllMultiPart(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		

	  /**
	  * 
	  * Method ID   : WmsImgDataInsert
	  * Method �ㅻ�    : ��怨�����
	  * ���깆��                      : smics
	  * @param model
	  * @return
	  * @throws Exception
	  */
	 @Override
	 public Map<String, Object> WmsImgDataInsert(Map<String, Object> model) throws Exception {
	     Map<String, Object> m = new HashMap<String, Object>();
	     
	     try{

	         //log.info(" ********** model DeliveryImgDataInsert model: " + model);
	         int tmpCnt = 1;
	         
	         if(tmpCnt > 0){
	             String[] ordId  = new String[tmpCnt];                
	             String[] fileId = new String[tmpCnt];         
	             String[] attachGb  = new String[tmpCnt];
	             String[] fileValue = new String[tmpCnt];
	             String[] filePath = new String[tmpCnt];
	             String[] fileName = new String[tmpCnt];
	             String[] fileExt = new String[tmpCnt];
	             
	             for(int i = 0 ; i < tmpCnt ; i ++){
	                 ordId[i]    	= (String)model.get("ORD_ID");               
	                 fileId[i]   	= (String)model.get("FILE_ID");         
	                 attachGb[i]    	= (String)model.get("ATTACH_GB");
	                 fileValue[i]    = (String)model.get("FILE_VALUE");
	                 filePath[i]    	= (String)model.get("FILE_PATH");
	                 fileName[i]    	= (String)model.get("FILE_NAME");
	                 fileExt[i]    	= (String)model.get("FILE_EXT");		
	             }
	             
	             Map<String, Object> modelIns = new HashMap<String, Object>();
	             
	             modelIns.put("ordId", ordId);
	             modelIns.put("fileId", fileId);
	             modelIns.put("attachGb", attachGb);
	             modelIns.put("fileValue", fileValue);
	             modelIns.put("filePath", filePath);
	             modelIns.put("fileName", fileName);
	             modelIns.put("fileExt", fileExt);                
	             
	             //dao                
	             modelIns = (Map<String, Object>)dao.WmsImgDataInsert(modelIns);
	             
	             ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	             // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
	             // errMsg = modelIns.get("O_MSG_NAME").toString();
	             
	         }            
	         m.put("errCnt", 0);
	         m.put("MSG", MessageResolver.getMessage("save.success"));
	         
	     } catch(BizException be) {
	         m.put("errCnt", 1);
	         m.put("MSG", be.getMessage() );
	         
	     } catch(Exception e){
	         throw e;
	     }
	     return m;
	 }
	 
	 public Map<String, Object> selectMoveItemFromDataQry(Map<String, Object> model) throws Exception {

			Map<String, Object> map = new HashMap<String, Object>();
			try {

				checkParamMapValidationForList(model);

				checkUserInfoValidation(model);

				Map<String, Object> modelIns = createSPParamMapKR(model);
				
				//log.info(" ********** modelIns ====================================: " + modelIns);

				// //log.info(" ********** outputList 1 : " + modelIns);
				modelIns = (Map<String, Object>) dao.runSpMoveItemFromDataQry(modelIns);

				// //log.info(" ********** outputList 2 : " + modelIns);
				InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
				//log.info(" ********** outputList : " + outputList);

				map = createReturnMapKr(outputList, model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
				}
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to select list :", e);
				}
				throw e;
			}

			return map;

		}
	 
	public void updateItemTransferAllComplete(Map<String, Object> model) throws Exception {

			String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

			InterfaceBaseVOKR data = null;
			List<Map<String, Object>> workList = null;
			try {
				if (log.isInfoEnabled()) {
					//log.info("[ inputJSON ] :" + inputJSON);
				}

				data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

				if (log.isInfoEnabled()) {
					//log.info("[ data ] :" + data);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5005", "JSON Data Convert Error");

			}

			if (data != null) {
				try {

					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
					model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

					checkUserInfoValidation(model);

				} catch (InterfaceException ife) {
					if (log.isErrorEnabled()) {
						log.error(ife.getMsgCode());
					}
					throw ife;

				}

				try {
					//log.info("[ data########################### ] :" + data);
					workList = getDataRowFromInterfaceVOKR(data);				
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ workList ] :" + workList);
					}
				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw new InterfaceException("5006", "Data ROW Convert Error");

				}
				
				try {
					if (workList != null && !workList.isEmpty()) {
						Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
						if (log.isInfoEnabled()) {
							//log.info("===========================================================================");
							//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
						}

						//for(int i=0; i<modelIns.size(); i++){
							////log.info("iLcId:" + modelIns.get("iLcId"));
							//String[] test = modelIns.
							
						//}
						
						// //log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpItemTransferAllComplete(modelIns);

						model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
						model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
						InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
						
					}
				} catch (InterfaceException ife) {
					throw ife;

				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw e;

				}
			}

		}

	public Map<String, Object> selectLocInoutDataQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLocInoutDataQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	 
	public void updateInoutCheckComplete(Map<String, Object> model) throws Exception {

			String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

			InterfaceBaseVOKR data = null;
			List<Map<String, Object>> workList = null;
			try {
				if (log.isInfoEnabled()) {
					//log.info("[ inputJSON ] :" + inputJSON);
				}

				data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

				if (log.isInfoEnabled()) {
					//log.info("[ data ] :" + data);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5005", "JSON Data Convert Error");

			}

			if (data != null) {
				try {

					model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
					model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

					checkUserInfoValidation(model);

				} catch (InterfaceException ife) {
					if (log.isErrorEnabled()) {
						log.error(ife.getMsgCode());
					}
					throw ife;

				}

				try {
					//log.info("[ data########################### ] :" + data);
					workList = getDataRowFromInterfaceVOKR(data);				
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ workList ] :" + workList);
					}
				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw new InterfaceException("5006", "Data ROW Convert Error");

				}
				
				try {
					if (workList != null && !workList.isEmpty()) {
						Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
						if (log.isInfoEnabled()) {
							//log.info("===========================================================================");
							//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
						}

						//for(int i=0; i<modelIns.size(); i++){
							////log.info("iLcId:" + modelIns.get("iLcId"));
							//String[] test = modelIns.
							
						//}
						
						// //log.info(" ********** outputList 1 : " + modelIns);
						modelIns = (Map<String, Object>) dao.runSpInoutCheckComplete(modelIns);

						model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
						model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
						InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
						
					}
				} catch (InterfaceException ife) {
					throw ife;

				} catch (Exception e) {
					if (log.isErrorEnabled()) {
						log.error("Fail to excecute complete :", e);
					}
					throw e;

				}
			}

		}

	 
public void updateLotLocResettingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpLotLocResettingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

public Map<String, Object> selectTransferReadyInfoQry(Map<String, Object> model) throws Exception {

	Map<String, Object> map = new HashMap<String, Object>();
	try {

		checkParamMapValidationForList(model);

		checkUserInfoValidation(model);

		Map<String, Object> modelIns = createSPParamMapKR(model);
		
		//log.info(" ********** modelIns ====================================: " + modelIns);

		// //log.info(" ********** outputList 1 : " + modelIns);
		modelIns = (Map<String, Object>) dao.runSpTransferReadyInfoQry(modelIns);

		// //log.info(" ********** outputList 2 : " + modelIns);
		InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

		List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
		//log.info(" ********** outputList : " + outputList);

		map = createReturnMapKr(outputList, model);

	} catch (InterfaceException ife) {
		if (log.isErrorEnabled()) {
			log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
		}
		throw ife;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to select list :", e);
		}
		throw e;
	}

	return map;

}

	public Map<String, Object> selectMoveItemPriorityQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveItemPriorityQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF070", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updatekitMakeSerialWork(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpkitMakeSerialWork(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updatemodifyStockComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpmodifyStockComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateTransferQtyMulti(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTransferQtyMulti(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateKitGenMakeComplete2048(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpKitGenMakeComplete2048(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
//		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
//			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
//			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5013", "work_seq is NOT VALID");
//		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
//		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
//			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
//			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
//		}
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}

	public void updateItemTransferAllMultiComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpItemTransferAllMultiComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
}
