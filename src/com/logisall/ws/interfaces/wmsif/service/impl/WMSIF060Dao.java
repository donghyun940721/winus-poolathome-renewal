package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF060Dao")
public class WMSIF060Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpOutOrderQry
	 * Method 설명 : 출고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_order_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutOrderDetailQry
	 * Method 설명 : 출고주문상세조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrderDetailQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_order_detail_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutOrderMappingQry
	 * Method 설명 : 출고주문매핑정보조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrderMappingQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_order_mapping_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutOrderConfirmQry
	 * Method 설명 : 출고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrderConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_order_confirm_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpShippingInPltComplete
	 * Method 설명 : 물류용기출고완료
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShippingInPltComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_shipping_in_plt_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpShippingInComplete
	 * Method 설명 : 출고완료
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShippingInComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_shipping_in_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpShippingOrderStatQry
	 * Method 설명 : 출고주문상태조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShippingOrderStatQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_shipping_order_stat_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutAsnOrderQry
	 * Method 설명 : 출고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutAsnOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_asn_order_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutAsnOrderDetailQry
	 * Method 설명 : 출고주문상세조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutAsnOrderDetailQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_asn_order_detail_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutShippingCompleteAsn
	 * Method 설명 : 출고완료(asn)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutShippingCompleteAsn(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_shipping_complete_asn", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutLotOrderQry
	 * Method 설명 : 출고를 위한 lot재고 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutLotOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_lot_order_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutShippingCompleteLot
	 * Method 설명 : 출고완료(lot)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutShippingCompleteLot(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_shipping_complete_lot", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutAutoOrderStatQry
	 * Method 설명 : 자동출고주문상태조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutAutoOrderStatQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_auto_order_stat_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutAutoOrderChangeQry
	 * Method 설명 : 자동출고주문상태변경
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutAutoOrderChangeQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_auto_order_change_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutOrderInsertQry
	 * Method 설명 : 출고주문등록을 위한 상품코드 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrderInsertQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_order_insert_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutAutoOrderInsert
	 * Method 설명 : 출고주문자동입력
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutAutoOrderInsert(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_auto_order_insert", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutOrderFeedingQry
	 * Method 설명 : 출고 feeding 주문 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrderFeedingQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_order_feeding_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutFeedingComplete
	 * Method 설명 : 출고확정(FEEDING)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutFeedingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_feeding_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpOutFeedingCompleteForce
	 * Method 설명 : 주문등록부터 출고확정까지 강제 출고
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutFeedingCompleteForce(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_feeding_complete_force", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutOrderFeedingFifoQry
	 * Method 설명 : 출고 feeding LOT 선입선출 주문 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrderFeedingFifoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_order_feeding_fifo_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutFeedingCompleteFifo
	 * Method 설명 : 출고확정(FEEDING_FIFO)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutFeedingCompleteFifo(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_feeding_complete_fifo", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpOutEpcMapQry
	 * Method 설명 : 출고 feeding LOT 선입선출 주문 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutEpcMapQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_epc_map_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpOutCompleteSetForce
	 * Method 설명 : 주문등록부터 출고확정까지 강제 출고(임가공)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutCompleteSetForce(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_complete_set_force", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutFroceLotOrdQry
	 * Method 설명 : 출고주문기준 재고이동 정보 조회 
	 * 작성자 : smics 2018-06-14
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutFroceLotOrdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_force_lot_ord_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpOutInordComplete
	 * Method 설명 : 주문이 등록되어 있는 상태에서의 출고처리(로케이션지정, 피킹리스트발행, 출고확정)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutInordComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_inord_complete", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpOutProcessQry
	 * Method 설명 : 출고주문기준 로케이션지정 정보 조회
	 * 작성자 : smics 2018-06-26
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutProcessQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_inord_process_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutLocRemainQry
	 * Method 설명 : 출고주문기준 로케이션지정 정보 조회
	 * 작성자 : smics 2018-06-26
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutLocRemainQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_loc_remain_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutOrdRemainQry
	 * Method 설명 : 출고주문기준 츌고 미처리 정보 조회
	 * 작성자 : smics 2018-06-26
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrdRemainQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_ord_remain_qry", model);
		return model;
	}	
	

	/*-
	 * Method ID : runSpOutInordProcessComplete
	 * Method 설명 : 주문이 등록되어 있는 상태에서의 로케이션 지정처리(로케이션지정, 피킹리스트발행)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutInordProcessComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_inord_process_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutOrderLotQry
	 * Method 설명 : 매핑이 되어 있지 않는 주문에 대한 LOT 정보 조회
	 * 작성자 : smics 2018-06-27
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrderLotQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_order_lot_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpLotOverlapConfirmQry
	 * Method 설명 : LOT 중복 조회
	 * 작성자 : smics 2018-07-28
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotOverlapConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_lot_overlap_confirm_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutCompleteBoxForce
	 * Method 설명 : 주문등록부터 출고확정까지 강제 출고(BOX주문)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutCompleteBoxForce(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_complete_box_force", model);
		return model;
	}
	
	
	/*-
	 * Method ID : runSpOutOrdIdOrderQry
	 * Method 설명 : 출고주문조회(주문번호, seq)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutOrdIdOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_ord_id_order_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpOutPickingComplete
	 * Method 설명 : 피킹확정
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutPickingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_picking_complete", model);
		return model;
	}		

	/*-
	 * Method ID : runSpShippingPickedComplete
	 * Method 설명 : 피킹 확정 후 출고확정(이미지 처리)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShippingPickedComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_checked_complete", model);
		return model;
	}		

	/*-
	 * Method ID : runSpPickingCheckedComplete
	 * Method 설명 : 피킹 확정 (이미지 처리)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPickingCheckedComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_picking_checked_complete", model);
		return model;
	}		
	
    /**
     * Method ID    : WmsImgDataInsert
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object WmsImgDataInsert(Map<String, Object> model){
        executeUpdate("pk_wmsif090.sp_delivery_img_data_insert", model);
        return model;
    }
	
	/*-
	 * Method ID : runSpOutLotOrderIdQry
	 * Method 설명 : LOT번호로 출고 주문 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutLotOrderIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_lot_order_id_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutLotUnitOrderIdQry
	 * Method 설명 : LOT번호, Unit 번호로 출고 주문 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutLotUnitOrderIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_lot_unit_order_id_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpShippingPickedUnitComplete
	 * Method 설명 : 피킹 확정 후 출고확정(이미지 처리)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShippingPickedUnitComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_shipping_picked_unit_complete", model);
		return model;
	}		

	/*-
	 * Method ID : runSpPickingTransCustMobile
	 * Method 설명 : 피킹확정(거래처별)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPickingTransCustMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_picking_trans_cust_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpMultiShippingComplete
	 * Method 설명 : 출고확정(멀티 ORD, SEQ)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMultiShippingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_multi_shipping_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutLotConfirmQry
	 * Method 설명 : LOT일치 여부확인 및 로케이션 변경 로직
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutLotConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_lot_confirm_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpShippingCompleteMobile
	 * Method 설명 : 피킹 확정 후 출고확정(이미지 처리)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShippingCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_shipping_complete_mobile", model);
		return model;
	}	

	/*-
	 * Method ID : runSpCancelPickingMobile
	 * Method 설명 : 피킹 취소 모바일
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpCancelPickingMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_cancel_picking_mobile", model);
		return model;
	}		

	/*-
	 * Method ID : runSpSetLocMappingOutMobile
	 * Method 설명 : 출고주문이 있을 때 LOT정보로 로케이션 지정, 피킹리스트 발행, 매핑, 출고 처리
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSetLocMappingOutMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_set_loc_mapping_out_mobile", model);
		return model;
	}	

	/*-
	 * Method ID : runSpOutsimpleOutMobile
	 * Method 설명 : 모바일 간편 출고
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutsimpleOutMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_simple_out_mobile", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpOrdersimpleOutMobile
	 * Method 설명 : 주문 입력 후 모바일 간편 출고
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrdersimpleOutMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_order_simple_out_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutPickingListQry
	 * Method 설명 : 피킹리스트 조회 쿼리
	 * 작성자 : smics 2019-12-03
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutPickingListInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_picking_list_Info_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpLocMappingPickingComplete
	 * Method 설명 : 주문이 입력되어 있는 상태에서 로케이션 지정, 매핑, 피킹리스트 발행, 피킹확정 처리
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocMappingPickingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_loc_mapping_picking_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpOrdInsSimpleOut
	 * Method 설명 : 출고 주문 입력 후 간편 출고
	 * DATE : 2020-06-21
	 * 작성자 : SMICS
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrdInsSimpleOut(Map<String, Object> model) {
		executeUpdate("pk_wmsop030.sp_ord_ins_simple_out", model);
		return model;
	}	

	/*-
	 * Method ID : LotLocShippingComplete
	 * Method 설명 : 수신된 LOT번호로 로케이션 지정 후 출고확정
	 * DATE : 2020-06-30
	 * 작성자 : SMICS
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotLocShippingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_lot_loc_shipping_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpShippingEdiyaNotDepart
	 * Method 설명 : 이디야 미배송 차량 데이터 전송(이미지 포함)
	 * 작성자 :  MonkeySeok
	 * 날 짜 : 2020-09-09
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShippingEdiyaNotDepart(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_shipping_ediya_not_depart", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpOutTotalPickingQry
	 * Method 설명 : 토탈 피킹리스트 조회 쿼리
	 * 작성자 : smics 2020-10-03
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutTotalPickingQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_total_picking_list_qry", model);
		return model;
	}	

	/*-
	 * Method ID : pickingTotalComplete
	 * Method 설명 : 토탈피킹리스트 조회 후 피킹 확정
	 * DATE : 2020-10-05
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPickingTotalComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_picking_total_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpLocCancelAlloComplete
	 * Method 설명 : 토탈피킹리스트 발행 후 로케이션 삭제 및 할당
	 * DATE : 2020-11-03
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocCancelAlloComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_loc_cancel_allo_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpShippingSerialComplete
	 * Method 설명 : 시리얼 출고확정(모바일)
	 * 작성자 : SMICS
	 * 날   짜 : 202-12-22
	 *
	 * @param model
	 * @return
	 */
	public Object runSpShippingSerialComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_shipping_serial_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpCrossDomainHttpWsMobile
	 * Method 설명 : CJ올리브영 DHL 전송로직
	 * 작성자 : SMICS
	 * 날   짜 : 2021-04-25
	 *
	 * @param model
	 * @return
	 */
	public Object runSpCrossDomainHttpWsMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_ord_id_out_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutParcelInvcQry
	 * Method 설명 : 토탈 피킹리스트 조회 쿼리
	 * 작성자 : smics 2021-06-24
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOutParcelInvcQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_parcel_invc_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : pickingTotalComplete
	 * Method 설명 : 피킹작업완료 
	 * DATE : 2021-08-23
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPickingCheckComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_picking_check_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpTempCheckComplete
	 * Method 설명 : 임시 데이터 전송용
	 * DATE : 2021-08-24
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTempCheckComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_temp_check_complete", model);
		return model;
	}	
	
	/*-
	 * Method 설명 : 합포장 여부 확인
	 * 작성자 : smics
	 * date 2021-08-27
	 *
	 * @param model
	 * @return
	 */
	public Object runSpParcelPickingCntQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_out_parcel_cnt_list_qry", model);
		return model;
	}	

	/*-
	 * Method ID : locSetPickingCompleteMobile
	 * Method 설명 : 주문등록 상태에서 로케이션 추천 후 피킹확정
	 * DATE : 2022-06-21
	 * 작성자 : SMICS
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocSetPickingCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_loc_set_picking_complete_mobile", model);
		return model;
	}		
	/*-
	 * Method ID : runSpLocSetPickingTotalCompleteMobile
	 * Method 설명 : 주문등록 상태에서 로케이션 추천 후 피킹확정 및 피킹리스트 발행 상태 피킹확정 처리
	 * DATE : 2023-01-14
	 * 작성자 : sing09
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocSetPickingTotalCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif060.sp_loc_set_picking_total_complete_mobile", model);
		return model;
	}		
}
