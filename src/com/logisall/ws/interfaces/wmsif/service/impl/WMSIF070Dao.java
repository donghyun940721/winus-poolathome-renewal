package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF070Dao")
public class WMSIF070Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpMappingDetailAsan
	 * Method 설명 : 아산물류센터 매핑 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMappingDetailAsan(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_mapping_detail_asan", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpMappingRemainQry
	 * Method 설명 : 아산물류센터 매핑 잔업 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMappingRemainQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_mapping_remain_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpMoveFromLocQry
	 * Method 설명 : 현재 로케이션 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMoveFromLocQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_from_loc_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpMoveToLocQry
	 * Method 설명 : 이동할 로케이션 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMoveToLocQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_to_loc_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpMappingLotConfirm
	 * Method 설명 : 매핑할 LOT 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMappingLotConfirm(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_mapping_lot_confirm_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpLotRitemQtyQry
	 * Method 설명 : 매핑할 LOT 상품 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotRitemQtyQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_lot_ritem_qty_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpMovePriorityQry
	 * Method 설명 : 선입선출재고조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMovePriorityQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_priority_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpMovePriorityQry
	 * Method 설명 : 선입선출 개체 정보조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMovePriorityEpcQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_priority_epc_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpTransferAllComplete
	 * Method 설명 : 재고이동(제한 없음) 완료
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTransferAllComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_transfer_all_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpDemappingComplete
	 * Method 설명 : 매핑해제(모바일)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDemappingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_demapping_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpMappingCompleteAsan
	 * Method 설명 : 매핑아산(모바일)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMappingCompleteAsan(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_mapping_complete_asan", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpLotMappingComplete
	 * Method 설명 : 이재매핑usa(모바일)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotMappingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_lot_mapping_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpLotEpcQry
	 * Method 설명 : LOT번호로 EPC조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotEpcQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_lot_epc_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpStocktakingIdQry
	 * Method 설명 : 재고조사 ID 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStocktakingIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_stocktaking_id_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpLocIdQry
	 * Method 설명 : 로케이션 ID 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_loc_id_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpStocktakingTotalQry
	 * Method 설명 : 재고조사 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStocktakingTotalQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_stocktaking_total_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpStocktakingTotalQry
	 * Method 설명 : 재고조사 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStockInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_stock_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpStocktakingComplete
	 * Method 설명 : 재고조사결과전송로직
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStocktakingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_stocktaking_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpMappingCompleteMobile
	 * Method 설명 : 출고매핑(모바일)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMappingCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_mapping_complete_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpIfMoveFromLocQry
	 * Method 설명 : 현재 로케이션 정보 조회(rti_
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpIfMoveFromLocQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_rti_move_from_loc_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpRtiTransferComplete
	 * Method 설명 : 재고이동(RTI) 완료
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpRtiTransferComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_rti_transfer_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpKitGenMakeComplete
	 * Method 설명 : 임가공조립
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpKitGenMakeComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_kit_gen_make_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpKitGenMakeCompleteInplt
	 * Method 설명 : 임가공조립(물류용기epc포함)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpKitGenMakeCompleteInplt(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_kit_gen_make_complete_inplt", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpKitDeleteComplete  
	 * Method 설명 : 임가공해체
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpKitDeleteComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_kit_delete_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDemappingDetailQry
	 * Method 설명 : 매핑해제 매핑 데이터 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDemappingDetailQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_demapping_detail_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpMoveTransQry
	 * Method 설명 : 임가공 후 재고이동 데이터 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMoveTransQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_trans_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpMakeKitFifoQry
	 * Method 설명 : 임가공 부분품 재고이동을 위한 선입선출 재고 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMakeKitFifoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_make_kit_fifo_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpMovestockCmtComplete
	 * Method 설명 : 임가공 부부품 재고이동
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMovestockCmtComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_movestock_cmt_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpItemZoneFifoQry
	 * Method 설명 : 상품별 선입선출 재고 위치 및 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpItemZoneFifoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_item_zone_fifo_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpLotInfoQry
	 * Method 설명 : LOT번호로 로케이션조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_lot_info_qry", model);
		return model;
	}
		
	/*-
	 * Method ID : runSpLotItemConfirmQry
	 * Method 설명 : LOT번호로 상품기본정보 획득
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotItemConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_lot_item_confirm_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpRepackingComplete  
	 * Method 설명 : repacking 작업 후 데이터 전송
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpRepackingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_repacking_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpMoveFromRefurbishedQry
	 * Method 설명 : 리퍼 재고이동 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMoveFromRefurbishedQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_from_refurbished_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpSerialAsInfoQry
	 * Method 설명 : 시리얼번호로 as정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSerialAsInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_serial_as_info_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSprefurbishedTransferComplete  
	 * Method 설명 : 리퍼브 재고이동 완료
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSprefurbishedTransferComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_refurbished_transfer_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSprefurbishedPartOutComplete  
	 * Method 설명 : 리퍼브 부품 출고 완료
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSprefurbishedPartOutComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_refurbished_part_out_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpkitMakeSerialComplete  
	 * Method 설명 : 리퍼브 임가공 완료(시리얼 기준)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpkitMakeSerialComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_kit_make_seiral_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpMoveFromLocUnitQry
	 * Method 설명 : 현재 로케이션 정보 조회(unit)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMoveFromLocUnitQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_from_loc_unit_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpkitGenWorkseqComplete
	 * Method 설명 : 임가공조립
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpkitGenWorkseqComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_kit_gen_make_workseq_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpKitGenListInfoQry
	 * Method 설명 : 임가공 작업 리스트 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpKitGenListInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_kit_gen_list_info_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpLotDuplicationInfoQry
	 * Method 설명 : LOT번호 중복 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotDuplicationInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_lot_duplication_info_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpTransferAllMultiPart
	 * Method 설명 : 다중 부품 재고이동
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTransferAllMultiPart(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_transfer_all_multi_part", model);
		return model;
	}	
		
    /**
     * Method ID    : WmsImgDataInsert
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object WmsImgDataInsert(Map<String, Object> model){
        executeUpdate("pk_wmsif090.sp_delivery_img_data_insert", model);
        return model;
    }

	/*-
	 * Method ID : runSpMoveItemFromLocQry
	 * Method 설명 : 재고이동 from 정보 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMoveItemFromDataQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_item_from_data_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpItemTransferAllComplete
	 * Method 설명 : 상품 재고이동(제한 없음) 완료
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpItemTransferAllComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_item_transfer_all_complete", model);
		return model;
	}	

	/*-
	 * Method ID : IF_LOC_INOUT_DATA_QRY 
	 * Method 설명 : 로케이션 상품 입출고 이력 조회 조회
	 * 작성자 : smics
	 * 날 짜 : 2020.06.03
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocInoutDataQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_loc_inout_data_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInoutCheckComplete
	 * Method 설명 : 입출고내역확인전송
	 * 작성자 : SMICS
	 * 날 짜 : 2020.06.05
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInoutCheckComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_inout_check_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpMoveItemPriorityQry
	 * Method 설명 : 상품의 선입선출 정보 조회
	 * 작성자 : SMICS
	 * 날 짜 : 2020.11.09
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMoveItemPriorityQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_move_item_priority_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpLotLocResettingComplete
	 * Method 설명 : 로케이션정보에 lot정보 입력
	 * 작성자 : SMICS
	 * 날 짜 : 2020.11.12
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotLocResettingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_lot_loc_resetting_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpTransferReadyInfoQry 
	 * Method 설명 : 재고이동 대상 리스트 조회
	 * 작성자 : smics
	 * 날 짜 : 2020.11.25
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTransferReadyInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_transfer_ready_info_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpkitMakeSerialWork  
	 * Method 설명 : 리퍼브 임가공 작업 완료(시리얼 기준)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpkitMakeSerialWork(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_kit_make_seiral_work", model);
		return model;
	}

	/*-
	 * Method ID : runSpmodifyStockComplete
	 * Method 설명 : 재고조정완료(모바일)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpmodifyStockComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_modify_stock_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpTransferQtyMulti 
	 * Method 설명 : 다중 재고 이동
	 * 작성자 : SMICS
	 * 날짜 : 2021-04-27
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTransferQtyMulti(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_transfer_qty_multi", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpKitGenMakeComplete
	 * Method 설명 : 임가공조립
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpKitGenMakeComplete2048(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_kit_gen_make_complete_2048", model);
		return model;
	}
	
	/*-
	 * Method ID : receiveItemTransferAllMultiComplete
	 * Method 설명 : 로케이션 전체 재고이동
	 * 날짜 : 2022-07-07
	 * 작성자 : SMICS
	 *
	 * @param model
	 * @return
	 */
	public Object runSpItemTransferAllMultiComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif070.sp_item_transfer_all_multi_complete", model);
		return model;
	}	
}
