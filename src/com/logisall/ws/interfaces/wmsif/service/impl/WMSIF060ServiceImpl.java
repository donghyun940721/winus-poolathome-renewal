package com.logisall.ws.interfaces.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.io.File;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVO;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKR;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKRDate;
import com.logisall.ws.interfaces.wmsif.service.WMSIF060Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF060Service")
public class WMSIF060ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF060Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF060Dao")
	private WMSIF060Dao dao;

	@Autowired
	WMSIF060ServiceImpl(WMSIF060Dao dao) {
		super(dao);
	}
		
	
	public Map<String, Object> selectOutOrderQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrderQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectOutOrderDetailQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrderDetailQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectOutOrderMappingQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrderMappingQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public Map<String, Object> selectOutOrderConfirmQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrderConfirmQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectShippingOrderStatQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpShippingOrderStatQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updateShippingInPltComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpShippingInPltComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updateShippingInComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpShippingInComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectOutAsnOrderQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutAsnOrderQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectOutAsnOrderDetailQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutAsnOrderDetailQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public void updateOutShippingCompleteAsn(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutShippingCompleteAsn(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectOutLotOrderQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutLotOrderQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public void updateOutShippingCompleteLot(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutShippingCompleteLot(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectOutAutoOrderStatQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutAutoOrderStatQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectOutAutoOrderChangeQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutAutoOrderChangeQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectOutOrderInsertQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrderInsertQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public void updateOutAutoOrderInsert(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutAutoOrderInsert(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectOutOrderFeedingQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrderFeedingQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public void updateOutFeedingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutFeedingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateOutFeedingCompleteForce(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutFeedingCompleteForce(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectOutOrderFeedingFifoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrderFeedingFifoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public void updateOutFeedingCompleteFifo(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutFeedingCompleteFifo(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectOutEpcMapQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutEpcMapQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public void updateOutCompleteSetForce(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutCompleteSetForce(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectOutFroceLotOrdQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutFroceLotOrdQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public void updateoutInordComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutInordComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	

	public Map<String, Object> selectOutProcessQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutProcessQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectOutLocRemainQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutLocRemainQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectOutOrdRemainQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrdRemainQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public void updateoutInordProcessComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutInordProcessComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	

	public Map<String, Object> selectOutOrderLotQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrderLotQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public Map<String, Object> selectLotOverlapConfirmQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpLotOverlapConfirmQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	
	
	public void updateOutCompleteBoxForce(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutCompleteBoxForce(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public Map<String, Object> selectOutOrdIdOrderQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutOrdIdOrderQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updatepickingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutPickingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
  /**
  * 
  * Method ID   : WmsImgDataInsert
  * Method �ㅻ�    : ��怨�����
  * ���깆��                      : smics
  * @param model
  * @return
  * @throws Exception
  */
 @Override
 public Map<String, Object> WmsImgDataInsert(Map<String, Object> model) throws Exception {
     Map<String, Object> m = new HashMap<String, Object>();
     
     try{

         //log.info(" ********** model DeliveryImgDataInsert model: " + model);
         int tmpCnt = 1;
         
         if(tmpCnt > 0){
             String[] ordId  = new String[tmpCnt];                
             String[] fileId = new String[tmpCnt];         
             String[] attachGb  = new String[tmpCnt];
             String[] fileValue = new String[tmpCnt];
             String[] filePath = new String[tmpCnt];
             String[] fileName = new String[tmpCnt];
             String[] fileExt = new String[tmpCnt];
             
             for(int i = 0 ; i < tmpCnt ; i ++){
                 ordId[i]    	= (String)model.get("ORD_ID");               
                 fileId[i]   	= (String)model.get("FILE_ID");         
                 attachGb[i]    	= (String)model.get("ATTACH_GB");
                 fileValue[i]    = (String)model.get("FILE_VALUE");
                 filePath[i]    	= (String)model.get("FILE_PATH");
                 fileName[i]    	= (String)model.get("FILE_NAME");
                 fileExt[i]    	= (String)model.get("FILE_EXT");		
             }
             
             Map<String, Object> modelIns = new HashMap<String, Object>();
             
             modelIns.put("ordId", ordId);
             modelIns.put("fileId", fileId);
             modelIns.put("attachGb", attachGb);
             modelIns.put("fileValue", fileValue);
             modelIns.put("filePath", filePath);
             modelIns.put("fileName", fileName);
             modelIns.put("fileExt", fileExt);                
             
             //dao                
             modelIns = (Map<String, Object>)dao.WmsImgDataInsert(modelIns);
             
             ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
             // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
             // errMsg = modelIns.get("O_MSG_NAME").toString();
             
         }            
         m.put("errCnt", 0);
         m.put("MSG", MessageResolver.getMessage("save.success"));
         
     } catch(BizException be) {
         m.put("errCnt", 1);
         m.put("MSG", be.getMessage() );
         
     } catch(Exception e){
         throw e;
     }
     return m;
 }
 
	public void updateShippingPickedComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpShippingPickedComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	 
	public void updatePickingCheckedComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpPickingCheckedComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectOutLotOrderIdQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutLotOrderIdQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
		
	public Map<String, Object> selectOutLotUnitOrderIdQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutLotUnitOrderIdQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updateShippingPickedUnitComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpShippingPickedUnitComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updatepickingTransCustMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpPickingTransCustMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateMultiShippingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpMultiShippingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public Map<String, Object> selectOutLotConfirmQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutLotConfirmQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	@Override
	 public String fcmNaviSender(Map<String, Object> model) throws Exception {
    List<Map<String, Object>> outputList = (List<Map<String, Object>>) model.get("DATA");
	  //String token     = (String) outputList.get(0).get("prop_chk");
	  String dummy     = "";
//	  String pageGb    = (String) outputList.get(0).get("target_cd"); //페이지 이동 시 구분 값
//	  String param2    = (String) outputList.get(0).get("param2"); //페이지 이동 시 구분 값
//	  String param3    = (String) outputList.get(0).get("param3"); 
//	  String param4    = (String) outputList.get(0).get("param4"); 
//	  String param5    = (String) outputList.get(0).get("param5"); 
		String pageGb    = (String) outputList.get(0).get("before_stock_qty"); //페이지 이동 시 구분 값
		String param2    = String.valueOf(outputList.get(0).get("rti_epc_cd")); //페이지 이동 시 구분 값
		String param3    = String.valueOf(outputList.get(0).get("loc_cd")); 
		String param4    = String.valueOf(outputList.get(0).get("in_qty")); 
		String param5    = String.valueOf(outputList.get(0).get("out_qty")); 
		String param6    = String.valueOf(outputList.get(0).get("stock_qty")); 
		String param7    = String.valueOf(outputList.get(0).get("bad_qty")); 
		String param8    = String.valueOf(outputList.get(0).get("as_qty")); 
		String param9    = String.valueOf(outputList.get(0).get("param9")); 
		String param10    = String.valueOf(outputList.get(0).get("param10")); 
	     
	     final String deviceToken = (String) outputList.get(0).get("remark");
	     log.info(" ********** deviceToken ===============: " + deviceToken);
	     /** String apiKey googleApi고유값 고정 **/
	     final String apiKey = "AAAA3Z9ipPE:APA91bErJxnV9Yv36efN8mFI6hX35whZElAcYwKuHacVxBFiB5xfNtuDob4Jdb20lGTVthQlHgkqlf3ELhfLUm7wL690UmQg9-g6ZCNqqvjOAzHDznuVJSgLP7SajOHStQ1444HfHcYE";
	     URL url = new URL("https://fcm.googleapis.com/fcm/send");
	     HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	     conn.setDoOutput(true);
	     conn.setRequestMethod("POST");
	     conn.setRequestProperty("Content-Type", "application/json");
	     conn.setRequestProperty("Authorization", "key=" + apiKey);
	 
	     conn.setDoOutput(true);
	 
	     String input = "{"
	     		      + "   \"data\" : {                           "
	     		      + "       \"param1\" : \"" + pageGb + "\"    "
	     		      + "      ,\"param2\" : \"" + param2 + "\"  	"
	     		      + "      ,\"param3\" : \"" + param3 + "\"     "
	     		      + "      ,\"param4\" : \"" + param4 + "\"     "
	     		      + "      ,\"param5\" : \"" + param5 + "\"     "
	     		      + "      ,\"param6\" : \"" + param6 + "\"  	"
	     		      + "      ,\"param7\" : \"" + param7 + "\"     "
	     		      + "      ,\"param8\" : \"" + param8 + "\"     "
	     		      + "      ,\"param9\" : \"" + param9 + "\"     "
	     	    	  + "      ,\"param10\" : \"" + param10 + "\"   "
	     		      + "   },                                     "
	     		      + "   \"to\":\"" + deviceToken + "\"         "
	     		      + "}";
	     // 전체 발송 시 : , \"to\":\"/topics/ALL\"}
	     
	     OutputStream os = conn.getOutputStream();
	     
	     // 한글 깨짐 UTF-8로 인코딩
	     os.write(input.getBytes("UTF-8"));
	     os.flush();
	     os.close();
	 
	     int responseCode = conn.getResponseCode();
	     //System.out.println("\nSending 'POST' request to URL : " + url);
	     //System.out.println("Post parameters : " + input);
	     //System.out.println("Response Code : " + responseCode);
			//log.info(" **********Response Code ====: " + responseCode);
	     
	     BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	     String inputLine;
	     StringBuffer response = new StringBuffer();
	 
	     while ((inputLine = in.readLine()) != null) {
	         response.append(inputLine);
	     }
	     in.close();
	     
	     //System.out.println(response.toString());
			//log.info(" **********response====: " + response.toString());
	     
	     return "jsonView";
	 }

	public void updateShippingCompleteMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpShippingCompleteMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateCancelPickingMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpCancelPickingMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public String deleteImgFolder(Map<String, Object> model) throws Exception {
		log.info("[111111111 model&&&&&&&&&&&&&&&&& ] :" + model);

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					log.info("[ 222222 workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			String file_path = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_CUST_LOT_NO_M);
			//log.info("[file_path file_path&&&&&&&&&&&&&&&&& ] :" + file_path);
			
			File folder = new File(file_path);
			try {
			    while(folder.exists()) {
				File[] folder_list = folder.listFiles(); //파일리스트 얻어오기
						
				for (int j = 0; j < folder_list.length; j++) {
					folder_list[j].delete(); //파일 삭제 
					System.out.println("파일이 삭제되었습니다.");
							
				}
						
				if(folder_list.length == 0 && folder.isDirectory()){ 
					folder.delete(); //대상폴더 삭제
					System.out.println("폴더가 삭제되었습니다.");
				}
		            }
			 } catch (Exception e) {
				e.getStackTrace();
			}	

		}
		
		return "jsonView";
	}
	
	public void updateSetLocMappingOutMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpSetLocMappingOutMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}

	public void updatesimpleOutMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOutsimpleOutMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public void updateLocMappingPickingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpLocMappingPickingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public void updateOrdersimpleOutMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOrdersimpleOutMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public void updateOrdInsSimpleOut(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpOrdInsSimpleOut(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public Map<String, Object> selectOutPickingListInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutPickingListInfoQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public void updateLotLocShippingComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpLotLocShippingComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	

	public void updateshippingEdiyaNotDepart(Map<String, Object> model) throws Exception {
		
		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				workList = getDataRowFromInterfaceVOKR(data);
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {

					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						log.info("[ modelIns ] :" + modelIns);
					}

					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpShippingEdiyaNotDepart(modelIns);
					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectOutTotalPickingQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutTotalPickingQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updatePickingTotalComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpPickingTotalComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updateLocCancelAlloComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpLocCancelAlloComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updateShippingSerialComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpShippingSerialComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public void updateCrossDomainHttpWsMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpCrossDomainHttpWsMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectOutParcelInvcQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpOutParcelInvcQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updatePickingCheckComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpPickingCheckComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		

	
	public void updateTempCheckComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpTempCheckComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public Map<String, Object> selectParcelPickingCntQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpParcelPickingCntQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateLocSetPickingCompleteMobile(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKR(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// //log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpLocSetPickingCompleteMobile(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF050", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}	
	
	public void updateLocSetPickingTotalCompleteMobile(Map<String, Object> model) throws Exception {
		
		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);
		
		InterfaceBaseVOKR data = null;
		List<Map<String, Object>> workList = null;
		try {
			data = InterfaceUtil.convertInterfaceVOFromJSONKR(inputJSON);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");
			
		}
		
		if (data != null) {
			try {
				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());
				
				checkUserInfoValidation(model);
				
			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;
			}
			
			try {
				workList = getDataRowFromInterfaceVOKR(data);				
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");
			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKR(data, workList);
					modelIns = (Map<String, Object>) dao.runSpLocSetPickingTotalCompleteMobile(modelIns);
					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
				}
			} catch (InterfaceException ife) {
				throw ife;
				
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;
				
			}
		}
		
	}	
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
//		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
//			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
//			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5013", "work_seq is NOT VALID");
//		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
//		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
//			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
//			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
//		}
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}

}
