package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVO;
import com.logisall.ws.interfaces.wmsif.service.WMSIF030Service;

@Service("WMSIF030Service")
public class WMSIF030ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF030Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF030Dao")
	private WMSIF030Dao dao;

	@Autowired
	WMSIF030ServiceImpl(WMSIF030Dao dao) {
		super(dao);
	}

	public void updateRtiAlert(Map<String, Object> model) throws Exception {
		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVO data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSON(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("===========================================================================");
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute picking complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				workList = getDataRowFromInterfaceVO(data);
				if (log.isInfoEnabled()) {
					log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute picking complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			try {
				if (workList != null && !workList.isEmpty()) {

					Map<String, Object> modelIns = getSPCompleteParamMap(data, workList);

					modelIns = (Map<String, Object>) dao.runSpRtiAlertComplete(modelIns);
					InterfaceUtil.isValidReturnCode("WMSIF030", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute picking complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectIsolationList(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpIsolationList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF020", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select picking list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectIsolationQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidation(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			modelIns = (Map<String, Object>) dao.runSpIsolationQry(modelIns);

			InterfaceUtil.isValidReturnCode("WMSIF030", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select picking list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateIsolationComplete(Map<String, Object> model) throws Exception {
		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVO data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSON(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("===========================================================================");
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute picking complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				workList = getDataRowFromInterfaceVO(data);
				if (log.isInfoEnabled()) {
					log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute picking complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			try {
				if (workList != null && !workList.isEmpty()) {

					Map<String, Object> modelIns = getSPCompleteParamMap(data, workList);

					modelIns = (Map<String, Object>) dao.runSpIsolationComplete(modelIns);
					InterfaceUtil.isValidReturnCode("WMSIF030", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute picking complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectMoveList(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpMoveList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF020", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select picking list :", e);
			}
			throw e;
		}

		return map;

	}	

	public Map<String, Object> selectMoveQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidation(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			modelIns = (Map<String, Object>) dao.runSpMoveQry(modelIns);

			InterfaceUtil.isValidReturnCode("WMSIF030", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select picking list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateMoveComplete(Map<String, Object> model) throws Exception {
		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVO data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSON(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("===========================================================================");
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute picking complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				workList = getDataRowFromInterfaceVO(data);
				if (log.isInfoEnabled()) {
					log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute picking complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			try {
				if (workList != null && !workList.isEmpty()) {

					Map<String, Object> modelIns = getSPCompleteParamMap(data, workList);

					modelIns = (Map<String, Object>) dao.runSpMoveComplete(modelIns);
					InterfaceUtil.isValidReturnCode("WMSIF030", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute picking complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectStockList(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpStockList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF020", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select picking list :", e);
			}
			throw e;
		}

		return map;

	}		

	public Map<String, Object> selectStockQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidation(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMap(model);

			modelIns = (Map<String, Object>) dao.runSpStockQry(modelIns);

			InterfaceUtil.isValidReturnCode("WMSIF030", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMap(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select picking list :", e);
			}
			throw e;
		}

		return map;

	}

	public void updateStockComplete(Map<String, Object> model) throws Exception {
		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVO data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSON(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("===========================================================================");
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute picking complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				workList = getDataRowFromInterfaceVO(data);
				if (log.isInfoEnabled()) {
					log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute picking complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			try {
				if (workList != null && !workList.isEmpty()) {

					Map<String, Object> modelIns = getSPCompleteParamMap(data, workList);

					modelIns = (Map<String, Object>) dao.runSpStockComplete(modelIns);
					InterfaceUtil.isValidReturnCode("WMSIF030", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute picking complete :", e);
				}
				throw e;

			}
		}

	}

	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
		}
		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
		}
		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
			throw new InterfaceException("5013", "work_seq is NOT VALID");
		}

	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
		}
		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
		}
		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
		}
		/*
		 * if ( map.get(ConstantWSIF.IF_KEY_LOCAL_SLIP_NO) == null ||
		 * StringUtils.isEmpty((String)map.get(ConstantWSIF.IF_KEY_LOCAL_SLIP_NO
		 * ))) { throw new InterfaceException("5017",
		 * "work_list.local_slip_no is NOT VALID"); } if (
		 * map.get(ConstantWSIF.IF_KEY_LOCAL_LOC_CD) == null ||
		 * StringUtils.isEmpty((String)map.get(ConstantWSIF.IF_KEY_LOCAL_LOC_CD)
		 * )) { throw new InterfaceException("5018",
		 * "work_list.local_loc_cd is NOT VALID"); } if (
		 * map.get(ConstantWSIF.IF_KEY_OTHER_PARTY_LOC_CD) == null ||
		 * StringUtils.isEmpty((String)map.get(ConstantWSIF.
		 * IF_KEY_OTHER_PARTY_LOC_CD))) { throw new InterfaceException("5019",
		 * "work_list.other_party_loc_cd is NOT VALID"); } if (
		 * map.get(ConstantWSIF.IF_KEY_UNIT_WEIGHT) == null ||
		 * StringUtils.isEmpty((String)map.get(ConstantWSIF.IF_KEY_UNIT_WEIGHT))
		 * ) { throw new InterfaceException("5020",
		 * "work_list.unit_weight is NOT VALID"); } if (
		 * map.get(ConstantWSIF.IF_KEY_EVENT_DT) == null ||
		 * StringUtils.isEmpty((String)map.get(ConstantWSIF.
		 * IF_KEY_OTHER_PARTY_LOC_CD)) ||
		 * "null".equalsIgnoreCase((String)map.get(ConstantWSIF.
		 * IF_KEY_OTHER_PARTY_LOC_CD)) ) { throw new InterfaceException("5021",
		 * "work_list.event_dt is NOT VALID"); }
		 */
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}

}
