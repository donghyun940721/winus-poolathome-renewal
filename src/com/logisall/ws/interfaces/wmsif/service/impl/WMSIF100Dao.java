package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF100Dao")
public class WMSIF100Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpForkliftList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpForkliftList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_forklift_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpUwmsReceivingProcessList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwms1stReceivingProcessList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_1st_receiving_process_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpUwmsReceivingProcessList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwms3rdReceivingProcessList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_3rd_receiving_process_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpUwmsReceivingProcessList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwmsReceivingProcessList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_receiving_process_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpUwmsReceivingPosition
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwmsReceivingPositionList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_receiving_position_list", model);
		return model;
	}

	/*-
	 * Method ID : runSpUwmsLocPositionQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwmsLocPositionQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_location_position_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInComplete 
	 * Method 설명 : 입고완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpForkliftComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_forklift_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDensoStockList
	 * Method 설명 : DENSO 재고 조회(메인)
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDensoStockList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_denso_stock_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDensoStockDetailList
	 * Method 설명 : DENSO 재고 조회(상세)
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDensoStockDetailList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_denso_stock_detail_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDensoStockDailyList
	 * Method 설명 : DENSO 재고 조회(주차별)
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDensoStockDailyList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_denso_stock_daily_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpEcoproBMStockDailyList
	 * Method 설명 : 에코프로BM 재고 조회(주차별)
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEcoproBMStockDailyList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_ecoprobm_stock_daily_list", model);
		return model;
	}
	
	/*-
	 * Method ID : ifEcoproBmOutOrdComfRtnList
	 * Method 설명 : 에코프로BM 출고확정 리턴 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object ifEcoproBmOutOrdComfRtnList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_ecoprobm_out_ord_comp_list", model);
		return model;
	}
	
	/*-
	 * Method ID : ifEcoproBmOutOrdComfToOracle
	 * Method 설명 : 에코프로BM 출고확정 오라클 update
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object ifEcoproBmOutOrdComfToOracle(Map<String, Object> model) {
		executeUpdate("wmsop030.pk_wmsif100.sp_insert_order_rst_conf", model);
		return model;
	}
	
	/*-
	 * Method ID : ifEcoproBmOutOrdReturnComfRtnList
	 * Method 설명 : 에코프로BM 반품출고확정 리턴 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object ifEcoproBmOutOrdReturnComfRtnList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_ecoprobm_out_ord_return_comp_list", model);
		return model;
	}
	
	/*-
	 * Method ID : ifEcoproBmOutOrdComfToOracle
	 * Method 설명 : 에코프로BM 반품출고확정 오라클 update
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object ifEcoproBmOutOrdReturnComfToOracle(Map<String, Object> model) {
		executeUpdate("wmsop030.pk_wmsif100.sp_insert_order_return_rst_conf", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpUwmsMapLocMapSetInfoQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwmsMapLocMapSetInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_map_set_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpUwmsMapLocPositionQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwmsMapLocPositionQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_map_location_position_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpUwmsAgvProcessList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwmsAgvProcessList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_agv_process_list", model);
		return model;
	}

	/*-
	 * Method ID : runSpAgvOrderQry
	 * Method 설명 : AGV 주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpAgvOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_agv_order_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpUwmsAgvCompleteList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpUwmsAgvCompleteList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_uwms_agv_complete_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpNaviLoginQry
	 * Method 설명 : winus 로그인시 네비게이션 로그인 push 발생
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpNaviLoginQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_navi_login", model);
		return model;
	}

	/*-
	 * Method ID : runSpAnywareZoneInfoList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpAnywareZoneInfoList(Map<String, Object> model) {
		executeUpdate("pk_wmsif100.sp_anyware_zone_info_list", model);
		return model;
	}

	/*-
	 * Method ID : runSpEdiyaMobileCsComplete
	 * Method 설명 : 이디야 모바일 CS입력 저장
	 * DATE : 2022-02-22
	 * 작성자 : SMICS
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEdiyaMobileCsComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif103.sp_ediya_mobile_cs_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpEdiyaMobileReturnComplete
	 * Method 설명 : 이디야 모바일 반품 저장
	 * DATE : 2022-02-22
	 * 작성자 : SMICS
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEdiyaMobileReturnComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif103.sp_ediya_mobile_return_complete", model);
		return model;
	}
}
