package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF080Dao")
public class WMSIF080Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpStockQry
	 * Method 설명 : 현재고조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStockQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_stock_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpStockInoutQry
	 * Method 설명 : 재고 입출고 이력 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStockInoutQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_stock_inout_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpStockLocationQry
	 * Method 설명 : 로케이션별 재고조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStockLocationQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_stock_location_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpStockLotQry
	 * Method 설명 : LOT별 재고조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStockLotQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_stock_lot_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpStockPriorityQry
	 * Method 설명 : 선입선출 재고 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStockPriorityQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_stock_priority_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpRealtimeStockConfirmQry
	 * Method 설명 : 물류용기의 실시간 재고 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpRealtimeStockConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_realtime_stock_confrim_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpMappingStatConfirmQry
	 * Method 설명 : 매핑 상태 조회 
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMappingStatConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_mapping_stat_confrim_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpSearchLocationQry
	 * Method 설명 : 로케이션 적재 정보 조회 
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSearchLocationQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_search_location_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : SetPartConfirmQry
	 * Method 설명 : 완성품 부분품 정보 조회 
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSetPartConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_set_part_confirm_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpSetStatQry
	 * Method 설명 : 완성품 상태 정보 확인
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSetStatQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_set_stat_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpLocIdQry
	 * Method 설명 : 로케이션 정보 조회 
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocSearchIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_loc_search_id_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpLotItemStockQry
	 * Method 설명 : 현재고 정보 조회 
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLotItemStockQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_lot_item_stock_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDlvInoutStatQry
	 * Method 설명 : 일별 배송 정보 조회 
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDlvInoutStatQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_dlv_inout_stat_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpItemInfoQry
	 * Method 설명 : 상품정보조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpItemInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_item_info_qry", model);
		return model;
	}

	 /*-
	 * Method ID : runSpRtiTraceQry
	 * Method 설명 : 물류용기 이력추적
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpRtiTraceQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_rti_trace_qry", model);
		return model;
	}

	 /*-
	 * Method ID : getRitemSerialInfoQry 
	 * Method 설명 : cust_lot_no 상태 조회
	 * 작성자 : smics
	 * 날짜 : 2019-11-08
	 * @param model
	 * @return
	 */
	public Object runSpRitemSerialInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_item_serial_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReqHelpdesk
	 * Method 설명 : 헬프데스크 등록(모바일)
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReqHelpdesk(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_req_helpdesk", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOutItemBarcodeSender
	 * Method 설명 : 상품바코드 등록
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpItemBarcodeSender(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_item_barcode_sender", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpStocktakingFirst
	 * Method 설명 : 초기 재고 실사 후 재고 등록
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpStocktakingFirst(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_stocktaking_first", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpRefurbishedSerialQry
	 * Method 설명 : 리퍼브 시리얼을 통한 주문 정보 조회(리퍼스 사진에 사용)
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpRefurbishedSerialQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_refurbished_serial_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpSubulQry
	 * Method 설명 : 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSubulQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_subul_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpLabelPrintingInfoQry
	 * Method 설명 : 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLabelPrintingInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_label_printing_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpEdiyaNotDepartQry
	 * Method 설명 : 이디야 미배송 데이터 조회
	 * 날짜 : 2020-09-10
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEdiyaNotDepartQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_ediya_not_depart_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpItemUomRegComplete
	 * Method 설명 : 상품 UOM 환산 정보 등록
	 * DATE : 2020-10-07
	 * 작성자 : SMICS
	 *
	 * @param model
	 * @return
	 */
	public Object runSpItemUomRegComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_item_uom_reg_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpDasCheckCompleteMobile
	 * Method 설명 : DAS 검수 확정 전송
	 * 작성자 : SMICS
	 * 날 짜 : 2020-10-12
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDasCheckCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_das_check_complete_mobile", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpEdiyaDeviceQry
	 * Method 설명 : 이디야 설비데이터 조회
	 * 날짜 : 2020-11-03
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEdiyaDeviceQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_ediya_device_stat_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpSupplementListQry
	 * Method 설명 : 보충 리스트 조회
	 * 날짜 : 2020-11-09
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSupplementListQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_supplement_list_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpLocHeaderListQry
	 * Method 설명 : 로케이션 헤더 정보 조회
	 * 날짜 : 2020-12-03
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocHeaderListQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_loc_header_list_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpEdiyaFactoryLabelListQry
	 * Method 설명 : 이디야 공장 레이블 발행 리스트 조회
	 * 날짜 : 2021-02-24
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEdiyaFactoryLabelListQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_ediya_factory_label_list_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpLcBoxListQry
	 * Method 설명 : 포장 박스 리스트 조회
	 * 날짜 : 2021-06-30
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLcBoxListQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif080.sp_lc_box_list_qry", model);
		return model;
	}		
}
