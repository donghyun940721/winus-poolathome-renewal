package com.logisall.ws.interfaces.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.PreparedStatement;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.service.AbstractInterfaceServiceImpl;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOForklift;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKRDate;
import com.logisall.ws.interfaces.wmsif.service.WMSIF100Service;

@Service("WMSIF100Service")
public class WMSIF100ServiceImpl extends AbstractInterfaceServiceImpl implements WMSIF100Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIF100Dao")
	private WMSIF100Dao dao;

	@Autowired
	WMSIF100ServiceImpl(WMSIF100Dao dao) {
		super(dao);
	}
		
	
	public Map<String, Object> selectForkLiftQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpForkliftList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectUwms1stReceivingProcessQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwms1stReceivingProcessList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectUwms3rdReceivingProcessQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwms3rdReceivingProcessList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectUwmsReceivingProcessQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwmsReceivingProcessList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectUwmsReceivingPositionQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwmsReceivingPositionList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}	

	public Map<String, Object> selectUwmsLocPositionQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwmsLocPositionQry(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void forkliftInComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOForklift data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONForklift(inputJSON);

			if (log.isInfoEnabled()) {
				log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}


			try {
				log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOForkLift(data);				
				if (log.isInfoEnabled()) {
					log.info("===========================================================================");
					log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapForklift(data, workList);
					if (log.isInfoEnabled()) {
						log.info("===========================================================================");
						log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						//log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					// log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpForkliftComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}
	
	public Map<String, Object> selectDensoStockQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {
			log.info(" ********** model =============model=======================: " + model);
			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapCust(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpDensoStockList(modelIns);
			
			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMapStock(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectDensoStockDetailQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapCust(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpDensoStockDetailList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMapStock(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectDensoStockDailyQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapCust(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpDensoStockDailyList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMapStock(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	@Override
	 public String fcmSender(Map<String, Object> model) throws Exception {
      List<Map<String, Object>> outputList = (List<Map<String, Object>>) model.get("DATA");
	  //String token     = (String) outputList.get(0).get("prop_chk");
      /*
	  String dummy     = "";
	  String pageGb    = (String) outputList.get(0).get("target_cd"); //페이지 이동 시 구분 값
	  String param2    = (String) outputList.get(0).get("param2"); //페이지 이동 시 구분 값
	  String param3    = (String) outputList.get(0).get("param3"); 
	  String param4    = (String) outputList.get(0).get("param4"); 
	  String param5    = (String) outputList.get(0).get("param5"); 
	  */
      
      /*outputList.get(0).get() 으로 받을 경우 string cast를 맞춰서 받아야 한다.
      sql에서 number로 넘어 올 경우. valueOf함수 사용으로 string cast*/
      String dummy     = "";
	  String pageGb    = (String) outputList.get(0).get("target_cd"); //페이지 이동 시 구분 값
	  String param2    = String.valueOf(outputList.get(0).get("param2")); //페이지 이동 시 구분 값
	  String param3    = String.valueOf(outputList.get(0).get("param3")); 
	  String param4    = String.valueOf(outputList.get(0).get("param4")); 
	  String param5    = String.valueOf(outputList.get(0).get("param5")); 
	  String param6    = String.valueOf(outputList.get(0).get("param6")); 
	  String param7    = String.valueOf(outputList.get(0).get("param7")); 
	  String param8    = String.valueOf(outputList.get(0).get("param8")); 
	  String param9    = String.valueOf(outputList.get(0).get("param9")); 
	  String param10    = String.valueOf(outputList.get(0).get("param10")); 
	  
	     
	     final String deviceToken = (String) outputList.get(0).get("token_cd");
	     //final String deviceToken = "ft_yxl4HJ_8:APA91bEkRvolEMLRUDK2OVVb6Bl1YkHmNJ2VQyEd2lfbM7nVvJGHiF1o1xZooAWY8HYaLs24yeniXXO-Hzh5nIDIu5rBtldwaLKA1aRekEQp7mI6_GsMA30l4BmP3pJ9kcmIpktP9oEK";
	     /** String apiKey googleApi고유값 고정 **/
	     //final String apiKey = "AAAA8BpkhlE:APA91bEElguh6WZvTo0VBGpDPVppTP8iamdFtxlL4STxTCt1dO-BsfFsFqraTf-BpkSapaqqTuPZs5Td2Pyqe2qVkt19Y-XobodbD7_SjC_Pmg5v0V3DJVByn5iQ_SDluKTaJspz8f54";
	     //final String apiKey = "AAAA9NFsVZQ:APA91bGv3Zt6KsaHaadzWy_kfPMGBa8Sz3MX-H06Tj2mNv5DQzrASlECMd3-9Sa6EKhd_1VVtiRJmwOCMptbpFgae49-_MovONWwJQ21170LJ4uCETqqYxhA9DjRdRY9_Fe4rsIoR-0z0yVzpgxskeibw5NKUfqZTA";
	     final String apiKey = "AAAA97YglUE:APA91bHg75oqQU-NdzKE6ruByHRiiG4CcXrJqKeGEoEe-48uQOvbwym2GIXU_Ql_l5hQI_RGlOOqo7qy469xAB6BQd2y82q7Qc41W9Mzg3EJOJksPwc7zTPt_c2a7yBuhNAV9ZTp-IqY";
	     URL url = new URL("https://fcm.googleapis.com/fcm/send");
	     HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	     conn.setDoOutput(true);
	     conn.setRequestMethod("POST");
	     conn.setRequestProperty("Content-Type", "application/json");
	     conn.setRequestProperty("Authorization", "key=" + apiKey);
	 
	     conn.setDoOutput(true);
	 
	     String input = "{"
	     		      + "   \"notification\" : {                   "
	     		      + "       \"title\" : \"EPC_CD \"            "
	     		      + "      ,\"body\"  : \"EPC_CD \"            "
	     		      + "   },                                     "
	     		      + "   \"data\" : {                           "
	     		      + "       \"param1\" : \"" + pageGb + "\"    "
	     		      + "      ,\"param2\" : \"" + param2 + "\"  "
	     		      + "      ,\"param3\" : \"" + param3 + "\"     "
	     		      + "      ,\"param4\" : \"" + param4 + "\"     "
	     		      + "      ,\"param5\" : \"" + param5 + "\"     "
	    	     	  + "      ,\"param6\" : \"" + param6 + "\"     "
	    	    	  + "      ,\"param7\" : \"" + param7 + "\"     "
	    	     	  + "      ,\"param8\" : \"" + param8 + "\"     "
	    	    	  + "      ,\"param9\" : \"" + param9 + "\"     "
	    		      + "      ,\"param10\" : \"" + param10 + "\"   "
	     		      + "   },                                     "
	     		      + "   \"to\":\"" + deviceToken + "\"         "
	     		      + "}";
	     // 전체 발송 시 : , \"to\":\"/topics/ALL\"}
	     
	     OutputStream os = conn.getOutputStream();
	     
	     // 한글 깨짐 UTF-8로 인코딩
	     os.write(input.getBytes("UTF-8"));
	     os.flush();
	     os.close();
	 
	     int responseCode = conn.getResponseCode();
	     //System.out.println("\nSending 'POST' request to URL : " + url);
	     //System.out.println("Post parameters : " + input);
	     //System.out.println("Response Code : " + responseCode);
	     
	     BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	     String inputLine;
	     StringBuffer response = new StringBuffer();
	 
	     while ((inputLine = in.readLine()) != null) {
	         response.append(inputLine);
	     }
	     in.close();
	     
	     //System.out.println(response.toString());
	     
	     return "jsonView";
	 }

	@Override
	 public String fcmNaviSender(Map<String, Object> model) throws Exception {
     List<Map<String, Object>> outputList = (List<Map<String, Object>>) model.get("DATA");
	  //String token     = (String) outputList.get(0).get("prop_chk");
	  String dummy     = "";
//	  String pageGb    = (String) outputList.get(0).get("target_cd"); //페이지 이동 시 구분 값
//	  String param2    = (String) outputList.get(0).get("param2"); //페이지 이동 시 구분 값
//	  String param3    = (String) outputList.get(0).get("param3"); 
//	  String param4    = (String) outputList.get(0).get("param4"); 
//	  String param5    = (String) outputList.get(0).get("param5"); 
		String pageGb    = (String) outputList.get(0).get("dev2_target_cd"); //페이지 이동 시 구분 값
		String param2    = String.valueOf(outputList.get(0).get("param2")); //페이지 이동 시 구분 값
		String param3    = String.valueOf(outputList.get(0).get("param3")); 
		String param4    = String.valueOf(outputList.get(0).get("param4")); 
		String param5    = String.valueOf(outputList.get(0).get("param5")); 
		String param6    = String.valueOf(outputList.get(0).get("param6")); 
		String param7    = String.valueOf(outputList.get(0).get("param7")); 
		String param8    = String.valueOf(outputList.get(0).get("param8")); 
		String param9    = String.valueOf(outputList.get(0).get("param9")); 
		String param10    = String.valueOf(outputList.get(0).get("param10")); 
	     
	     final String deviceToken = (String) outputList.get(0).get("dev2_token_cd");
		 //final String deviceToken = "eKjNbzqFsHw:APA91bHOaDaCD0IfPp0IKpANByX1th9deWCXl6rG5iAONBfaIxSgHMYwxMd8SjK1f8-l9bKKbliYB6wmA4Ac3exvJgruPW7G20SBouEGsGlRkew_UgEVn8b8gd6LXlL1444io0DW2obZ";
		 //final String deviceToken = "cW32E2_QnmE:APA91bFneEo-smaHfFB1PVm4-MU5DqFhjWPTxU845lN5Gdx-OF7_11D2BeMngH_Dt3XwLqTGUqQSo0uEE9fb2oxKDeJOdnger5FsVQFEvJe4ahGsBpgn7ZbatcieD-Gi1BxkmYl26mFo";
	     //final String deviceToken = "ft_yxl4HJ_8:APA91bEkRvolEMLRUDK2OVVb6Bl1YkHmNJ2VQyEd2lfbM7nVvJGHiF1o1xZooAWY8HYaLs24yeniXXO-Hzh5nIDIu5rBtldwaLKA1aRekEQp7mI6_GsMA30l4BmP3pJ9kcmIpktP9oEK";
	     /** String apiKey googleApi고유값 고정 **/
	     //final String apiKey = "AAAA3Z9ipPE:APA91bErJxnV9Yv36efN8mFI6hX35whZElAcYwKuHacVxBFiB5xfNtuDob4Jdb20lGTVthQlHgkqlf3ELhfLUm7wL690UmQg9-g6ZCNqqvjOAzHDznuVJSgLP7SajOHStQ1444HfHcYE";
	     final String apiKey = "AAAA3Z9ipPE:APA91bErJxnV9Yv36efN8mFI6hX35whZElAcYwKuHacVxBFiB5xfNtuDob4Jdb20lGTVthQlHgkqlf3ELhfLUm7wL690UmQg9-g6ZCNqqvjOAzHDznuVJSgLP7SajOHStQ1444HfHcYE";
	     URL url = new URL("https://fcm.googleapis.com/fcm/send");
	     HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	     conn.setDoOutput(true);
	     conn.setRequestMethod("POST");
	     conn.setRequestProperty("Content-Type", "application/json");
	     conn.setRequestProperty("Authorization", "key=" + apiKey);
	 
	     conn.setDoOutput(true);
	 
	     
	     String input = "{"
	     		      + "   \"data\" : {                           "
	     		      + "       \"param1\" : \"" + pageGb + "\"    "
	     		      + "      ,\"param2\" : \"" + param2 + "\"  	"
	     		      + "      ,\"param3\" : \"" + param3 + "\"     "
	     		      + "      ,\"param4\" : \"" + param4 + "\"     "
	     		      + "      ,\"param5\" : \"" + param5 + "\"     "
	     		      + "      ,\"param6\" : \"" + param6 + "\"  	"
	     		      + "      ,\"param7\" : \"" + param7 + "\"     "
	     		      + "      ,\"param8\" : \"" + param8 + "\"     "
	     		      + "      ,\"param9\" : \"" + param9 + "\"     "
	     	    	  + "      ,\"param10\" : \"" + param10 + "\"   "
	     		      + "   },                                     "
	     		      + "   \"to\":\"" + deviceToken + "\"         "
	     		      + "}";
	     // 전체 발송 시 : , \"to\":\"/topics/ALL\"}
	     
	     OutputStream os = conn.getOutputStream();
	     
	     // 한글 깨짐 UTF-8로 인코딩
	     os.write(input.getBytes("UTF-8"));
	     os.flush();
	     os.close();
	 
	     int responseCode = conn.getResponseCode();
	     //System.out.println("\nSending 'POST' request to URL : " + url);
	     //System.out.println("Post parameters : " + input);
	     //System.out.println("Response Code : " + responseCode);
			//log.info(" **********Response Code ====: " + responseCode);
	     
	     BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	     String inputLine;
	     StringBuffer response = new StringBuffer();
	 
	     while ((inputLine = in.readLine()) != null) {
	         response.append(inputLine);
	     }
	     in.close();
	     
	     //System.out.println(response.toString());
			//log.info(" **********response====: " + response.toString());
	     
	     return "jsonView";
	 }

	public Map<String, Object> selectEcoproBMStockDailyQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapCust(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpEcoproBMStockDailyList(modelIns);

			log.info(" ********** outputList 2 : " + modelIns.size());
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList.size());

			map = createReturnMapStock(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	protected final void checkParamMapValidation(Map<String, Object> model) throws Exception {

		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
//		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
//			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
//			throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
//		}
//		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5013", "work_seq is NOT VALID");
//		}
	}

	protected void checkWorkInfoValidation(Map<String, Object> map) throws Exception {
//		if (map.get(ConstantWSIF.IF_KEY_EVENT_CD) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_EVENT_CD))) {
//			throw new InterfaceException("5021", "work_list.event_cd is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_REGIST_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_REGIST_SEQ))) {
//			throw new InterfaceException("5022", "work_list.regist_seq is NOT VALID");
//		}
//		if (map.get(ConstantWSIF.IF_KEY_WORK_SEQ) == null || StringUtils.isEmpty((String) map.get(ConstantWSIF.IF_KEY_WORK_SEQ))) {
//			throw new InterfaceException("5023", "work_list.work_seq is NOT VALID");
//		}
	}
	
	protected void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception {
	}
	
	public Map<String, Object> ifEcoproBmOutOrdComfRtnList(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapCust(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			//log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.ifEcoproBmOutOrdComfRtnList(modelIns);

			//log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapStock(outputList, model);
			
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}
		return map;
	}
	
	public Map<String, Object> ifEcoproBmOutOrdReturnComfRtnList(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapCust(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			//log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.ifEcoproBmOutOrdReturnComfRtnList(modelIns);

			//log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapStock(outputList, model);
			
		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}
		return map;
	}
	
	public void ifEcoproBmOutOrdComfRtnToMssql(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception {
		PrintWriter printWriter = null;
		
		// Declare the JDBC objects.
        String connectionUrl = "jdbc:sqlserver://211.171.50.156:1433;" + "databaseName=ECOPRO_IF_OUT;"; 
        Connection con       = null;
        Statement stmt       = null;

        if(model.size() > 0){
			try {
				Gson gson = new Gson();
				String jsonString = gson.toJson(model);
				String sendData   = new StringBuffer().append(jsonString).toString();
				
				JsonParser Parser    = new JsonParser();
				JsonObject jsonObj   = (JsonObject) Parser.parse(sendData);
				JsonArray dataSetArr = (JsonArray) jsonObj.get("DATA");
				
		        //db connect
	            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	            con = DriverManager.getConnection(connectionUrl,"IF_OUT_USER","1234qwer!");
	        	
	            //oracle 확정 파라미터 담기
	            String[] dnReqNo       = new String[dataSetArr.size()];
	            String[] dnReqSeq      = new String[dataSetArr.size()];
	            String[] materialLotId = new String[dataSetArr.size()];
	            
	            //실행시간 get
	    		//String getDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
	    		//String getDate     = getDateTime.substring(0,8);
	    		//String getTime     = getDateTime.substring(8,14);
	    		//String ifId        = "IF" + getDate + getTime;
	    		
				for (int i = 0; i < dataSetArr.size(); i++) {
					JsonObject object = (JsonObject) dataSetArr.get(i);
	
					//mssql this row update
					stmt = con.createStatement();
					stmt.executeUpdate(" INSERT INTO UT_MATERIAL_SHIP_RESULT_IF("
									  + "		  IF_ID"
									  + "		, IF_DT"
									  + "		, DN_REQ_NO"
									  + "		, DN_REQ_SEQ"
									  + "		, REQ_COUNT"
									  + "		, REQ_QTY"
									  + "		, MATERIALLOTID"
									  + "		, MATERIALLOTQTY"
									  + "		, FINAL_LOT_NO"
									  + "		, IN_BCR_NO"
									  + "		, OUT_BCR_NO"
									  + "		, SHIPNO"
									  + "		, LOADSTATE"
									  + "		, PALLETBREAKSTATE"
									  + "		, PACKPOLLUTIONSTATE"
									  + "		, LABELSTATE"
									  + "		, PACKSTATE"
									  + "		, BP_CD"
									  + "		, BP_NAME"
									  + "		, PLANT_CD"
									  + "		, SL_CD"
									  + "		, ITEM_CD"
									  + "		, CUD_FLG"
									  + "		, BIZ_REC_FLG"
									  //+ ",BIZ_REC_DT"
									  + "		, BIZ_REC_ERROR"
									  + "		, INSRT_USER_ID"
									  + "		, INSRT_DT"
								      + ")VALUES("
							  		  + "		  REPLACE('"+ object.get("IF_ID")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
							  		  + "		, CONVERT(DATETIME, REPLACE('"+ object.get("TRANS_IF_DATE")+"\"".toString().replaceAll("\"", "") +"','\"','') + ' ' + STUFF(STUFF(REPLACE('"+ object.get("TRANS_IF_TIME")+"\"".toString().replaceAll("\"", "") +"','\"',''), 3, 0, ':'), 6, 0, ':'), 120)"
									  + "		, REPLACE('"+ object.get("DN_REQ_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("DN_REQ_SEQ")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("REQ_COUNT")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("REQ_QTY")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("MATERIALLOTID")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("MATERIALLOTQTY")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("FINAL_LOT_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("IN_BCR_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("OUT_BCR_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("SHIPNO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("LOADSTATE")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("PALLETBREAKSTATE")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("PACKPOLLUTIONSTATE")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("LABELSTATE")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("PACKSTATE")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("BP_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ (object.get("BP_NAME")+"\"".toString().replaceAll("\"", "")).replaceAll("'", "''") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("PLANT_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("SL_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("ITEM_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, 'C'"
									  + "		, 'N'"
									  //+ ",CONVERT (DATETIME, '"+ object.get("BIZ_REC_DT") +"')"
									  + "		, 'N'"
									  + "		, REPLACE('"+ object.get("INSRT_USER_ID")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, GETDATE()"
									  + ");"
					);
					
					dnReqNo[i]       = object.get("DN_REQ_NO").toString().replaceAll("\"", "");
					dnReqSeq[i]      = object.get("DN_REQ_SEQ").toString().replaceAll("\"", "");
					materialLotId[i] = object.get("MATERIALLOTID").toString().replaceAll("\"", "");
					
					stmt.close();
				}//for End
				
				Map<String, Object> modelIns = new HashMap<String, Object>();
				modelIns.put("dnReqNo"        , dnReqNo);
				modelIns.put("dnReqSeq"       , dnReqSeq);
				modelIns.put("materialLotId"  , materialLotId);
				dao.ifEcoproBmOutOrdComfToOracle(modelIns);
				
				stmt.close();
	            con.close();
	            
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to create Return Map :", e);
					
					stmt.close();
		        	con.rollback();
		        	con.close();
				}
				throw e;
	
			} finally {
				if (printWriter != null) {
					try {
						printWriter.close();
					} catch (Exception e) {
					}
				}
			}
		}
	}
	
	public void ifEcoproBmOutOrdReturnComfRtnToMssql(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception {
		PrintWriter printWriter = null;
		
		// Declare the JDBC objects.
        String connectionUrl = "jdbc:sqlserver://211.171.50.156:1433;" + "databaseName=ECOPRO_IF_OUT;"; 
        Connection con       = null;
        Statement stmt       = null;

        if(model.size() > 0){
			try {
				Gson gson = new Gson();
				String jsonString = gson.toJson(model);
				String sendData   = new StringBuffer().append(jsonString).toString();
				
				JsonParser Parser    = new JsonParser();
				JsonObject jsonObj   = (JsonObject) Parser.parse(sendData);
				JsonArray dataSetArr = (JsonArray) jsonObj.get("DATA");
				
		        //db connect
	            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	            con = DriverManager.getConnection(connectionUrl,"IF_OUT_USER","1234qwer!");
	        	
	            //oracle 확정 파라미터 담기
	            String[] dnReqNo       = new String[dataSetArr.size()];
	            String[] dnReqSeq      = new String[dataSetArr.size()];
	            String[] materialLotId = new String[dataSetArr.size()];
	            
	            //실행시간 get
	    		//String getDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
	    		//String getDate     = getDateTime.substring(0,8);
	    		//String getTime     = getDateTime.substring(8,14);
	    		//String ifId        = "IF" + getDate + getTime;
	    		
				for (int i = 0; i < dataSetArr.size(); i++) {
					JsonObject object = (JsonObject) dataSetArr.get(i);
	
					//mssql this row update
					stmt = con.createStatement();
					stmt.executeUpdate(" INSERT INTO UT_MATERIAL_RETURN_RESULT_IF("
									  + "		  IF_ID"
									  + "		, IF_DT"
									  + "		, MOVE_NO"
									  + "		, MATERIALLOTID"
									  + "		, MATERIALLOTQTY"
									  + "		, FINAL_LOT_NO"
									  + "		, IN_BCR_NO"
									  + "		, OUT_BCR_NO"
									  + "		, PLANT_CD"
									  + "		, SL_CD"
									  + "		, ITEM_CD"
									  + "		, MOVE_COUNT"
									  + "		, MOVE_QTY"
									  + "		, CUD_FLG"
									  + "		, BIZ_REC_FLG"
								      + ")VALUES("
								      + "		  REPLACE('"+ object.get("IF_ID")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, CONVERT(DATETIME, REPLACE('"+ object.get("TRANS_IF_DATE")+"\"".toString().replaceAll("\"", "") +"','\"','') + ' ' + STUFF(STUFF(REPLACE('"+ object.get("TRANS_IF_TIME")+"\"".toString().replaceAll("\"", "") +"','\"',''), 3, 0, ':'), 6, 0, ':'), 120)"
								      + "		, REPLACE('"+ object.get("DN_REQ_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("MATERIALLOTID")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("MATERIALLOTQTY")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("FINAL_LOT_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("IN_BCR_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("OUT_BCR_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("PLANT_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("SL_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("ITEM_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("REQ_COUNT")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, REPLACE('"+ object.get("REQ_QTY")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
								      + "		, 'R'"
								      + "		, 'N'"
									  + ");"
					);
					
					dnReqNo[i]       = object.get("DN_REQ_NO").toString().replaceAll("\"", "");
					dnReqSeq[i]      = object.get("DN_REQ_SEQ").toString().replaceAll("\"", "");
					materialLotId[i] = object.get("MATERIALLOTID").toString().replaceAll("\"", "");
					
					stmt.close();
				}//for End
				
				Map<String, Object> modelIns = new HashMap<String, Object>();
				modelIns.put("dnReqNo"        , dnReqNo);
				modelIns.put("dnReqSeq"       , dnReqSeq);
				modelIns.put("materialLotId"  , materialLotId);
				dao.ifEcoproBmOutOrdReturnComfToOracle(modelIns);
				
				stmt.close();
	            con.close();
	            
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to create Return Map :", e);
					
					stmt.close();
		        	con.rollback();
		        	con.close();
				}
				throw e;
	
			} finally {
				if (printWriter != null) {
					try {
						printWriter.close();
					} catch (Exception e) {
					}
				}
			}
		}
	}
	
	public void ifEcoproBmDailyStockRtnToMssql(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception {
		PrintWriter printWriter = null;
		
		// Declare the JDBC objects.
        String connectionUrl = "jdbc:sqlserver://211.171.50.156:1433;" + "databaseName=ECOPRO_IF_OUT;"; 
        Connection con       = null;
        Statement stmt       = null;

        if(model.size() > 0){
			try {
				Gson gson = new Gson();
				String jsonString = gson.toJson(model);
				String sendData   = new StringBuffer().append(jsonString).toString();
				
				JsonParser Parser    = new JsonParser();
				JsonObject jsonObj   = (JsonObject) Parser.parse(sendData);
				JsonArray dataSetArr = (JsonArray) jsonObj.get("DATA");
				
		        //db connect
	            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	            con = DriverManager.getConnection(connectionUrl,"IF_OUT_USER","1234qwer!");
	            
	            //실행시간 get
	    		//String getDateTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
	    		//String getDate     = getDateTime.substring(0,8);
	    		//String getTime     = getDateTime.substring(8,14);
	    		//String ifId        = "IF" + getDate + getTime;
	    		
	    		System.out.println("dataSetArr.size() : " + dataSetArr.size());
				for (int i = 0; i < dataSetArr.size(); i++) {
					JsonObject object = (JsonObject) dataSetArr.get(i);
	
					//mssql this row update
					stmt = con.createStatement();
					stmt.executeUpdate(" INSERT INTO UT_MATERIAL_INVENTORY_IF("
									  + "		  IF_ID"
									  + "		, IF_DT"
									  + "		, REPORT_DT"
									  + "		, INVENTORY_COUNT"
									  + "		, MATERIALLOTID"
									  + "		, MATERIALLOTQTY"
									  + "		, FINAL_LOT_NO"
									  + "		, IN_BCR_NO"
									  + "		, OUT_BCR_NO"
									  + "		, ITEM_CD"
									  + "		, PLANT_CD"
									  + "		, SL_CD"
									  + "		, CUD_FLG"
									  + "		, BIZ_REC_FLG"
									  //+ "		, BIZ_REC_DT"
									  + "		, INSRT_USER_ID"
									  + "		, INSRT_DT"
								      + ")VALUES("
								      + "		  REPLACE('"+ object.get("IF_ID")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, CONVERT(DATETIME, REPLACE('"+ object.get("TRANS_IF_DATE")+"\"".toString().replaceAll("\"", "") +"','\"','') + ' ' + STUFF(STUFF(REPLACE('"+ object.get("TRANS_IF_TIME")+"\"".toString().replaceAll("\"", "") +"','\"',''), 3, 0, ':'), 6, 0, ':'), 120)"
									  + "		, CONVERT(DATETIME, REPLACE('"+ object.get("TRANS_IF_DATE")+"\"".toString().replaceAll("\"", "") +"','\"','') + ' ' + STUFF(STUFF(REPLACE('"+ object.get("TRANS_IF_TIME")+"\"".toString().replaceAll("\"", "") +"','\"',''), 3, 0, ':'), 6, 0, ':'), 120)"
									  + "		, REPLACE('"+ object.get("INVENTORY_COUNT")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("MATERIALLOTID")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("MATERIALLOTQTY")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("FINAL_LOT_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("IN_BCR_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("OUT_BCR_NO")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("ITEM_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("PLANT_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, REPLACE('"+ object.get("SL_CD")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, 'C'"
									  + "		, 'N'"
									  //+ ",CONVERT (DATETIME, '"+ object.get("BIZ_REC_DT") +"')"
									  + "		, REPLACE('"+ object.get("INSRT_USER_ID")+"\"".toString().replaceAll("\"", "") +"','\"','')	"
									  + "		, GETDATE()"
									  + ");"
					);
					
					stmt.close();
				}//for End

				stmt.close();
	            con.close();
	            
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to create Return Map :", e);
					
					stmt.close();
		        	con.rollback();
		        	con.close();
				}
				throw e;
	
			} finally {
				if (printWriter != null) {
					try {
						printWriter.close();
					} catch (Exception e) {
					}
				}
			}
		}
	}
	
	@Override
	public String fcmMapSender(Map<String, Object> model) throws Exception {
		List<Map<String, Object>> outputList = (List<Map<String, Object>>) model.get("DATA");
		
		String dummy     = "";
	    String pageGb    = (String) outputList.get(0).get("target_cd"); //페이지 이동 시 구분 값
	    String param2    = String.valueOf(outputList.get(0).get("param2")); //페이지 이동 시 구분 값
	    String param3    = String.valueOf(outputList.get(0).get("param3")); 
	    String param4    = String.valueOf(outputList.get(0).get("param4")); 
	    String param5    = String.valueOf(outputList.get(0).get("param5")); 
	    String param6    = String.valueOf(outputList.get(0).get("param6")); 
	    String param7    = String.valueOf(outputList.get(0).get("param7")); 
	    String param8    = String.valueOf(outputList.get(0).get("param8")); 
	    String param9    = String.valueOf(outputList.get(0).get("param9")); 
		String param10    = String.valueOf(outputList.get(0).get("param10")); 
		 
		final String deviceToken = (String) outputList.get(0).get("token_cd");
	    //final String deviceToken = "d5IBdyehPOc:APA91bHBjAEa7vy5wQTe34nD_e9y_WoJViEQ8rRufpCs-QHCWKR_ENAHpz0co1yeMVDHW5fe2BQRsQWjp5c_GNVjEsmx5i9C1_6gYgo3y-busJk0IO-jeyNWyNDFkqgKlNId0OEyLdRB";
	    /** String apiKey googleApi고유값 고정 **/
	    /*WINUS*/
	    //final String apiKey = "AAAA9NFsVZQ:APA91bGv3Zt6KsaHaadzWy_kfPMGBa8Sz3MX-H06Tj2mNv5DQzrASlECMd3-9Sa6EKhd_1VVtiRJmwOCMptbpFgae49-_MovONWwJQ21170LJ4uCETqqYxhA9DjRdRY9_Fe4rsIoR-0z0yVzpgxskeibw5NKUfqZTA";
	    /*WINUS_NAVI*/
	    //final String apiKey = "AAAA3Z9ipPE:APA91bErJxnV9Yv36efN8mFI6hX35whZElAcYwKuHacVxBFiB5xfNtuDob4Jdb20lGTVthQlHgkqlf3ELhfLUm7wL690UmQg9-g6ZCNqqvjOAzHDznuVJSgLP7SajOHStQ1444HfHcYE";
	    final String apiKey = "AAAA9NFsVZQ:APA91bGv3Zt6KsaHaadzWy_kfPMGBa8Sz3MX-H06Tj2mNv5DQzrASlECMd3-9Sa6EKhd_1VVtiRJmwOCMptbpFgae49-_MovONWwJQ21170LJ4uCETqqYxhA9DjRdRY9_Fe4rsIoR-0z0yVzpgxskeibw5NKUfqZTA";
	    URL url = new URL("https://fcm.googleapis.com/fcm/send");
	    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    conn.setDoOutput(true);
	    conn.setRequestMethod("POST");
	    conn.setRequestProperty("Content-Type", "application/json");
	    conn.setRequestProperty("Authorization", "key=" + apiKey);
	 
	    conn.setDoOutput(true);
	 
	    String input = "{"
	    		     + "   \"notification\" : {                   "
	     		     + "       \"title\" : \"SEND_TEST_TITLE \"   "
	     		     + "      ,\"body\"  : \"SEND_TEST_BODY  \"   "
	     		     + "   },                                     "
	     		     + "   \"data\" : {                           "
	     		     + "       \"param1\" : \"" + pageGb + "\"    "
	     		     + "      ,\"param2\" : \"" + param2 + "\"    "
	     		     + "      ,\"param3\" : \"" + param3 + "\"    "
	     		     + "      ,\"param4\" : \"" + param4 + "\"    "
	     		     + "      ,\"param5\" : \"" + param5 + "\"    "
	    	     	 + "      ,\"param6\" : \"" + param6 + "\"     "
	    	    	 + "      ,\"param7\" : \"" + param7 + "\"     "
	    	     	 + "      ,\"param8\" : \"" + param8 + "\"     "
	    	    	 + "      ,\"param9\" : \"" + param9 + "\"     "
	    	    	 + "      ,\"param10\" : \"" + param10 + "\"   "
	     		     + "   },                                     "
	     		     + "   \"to\":\"" + deviceToken + "\"         "
	     		     + "}";
	    // 전체 발송 시 : , \"to\":\"/topics/ALL\"}
	     
	    OutputStream os = conn.getOutputStream();
	     
	    // 한글 깨짐 UTF-8로 인코딩
	    os.write(input.getBytes("UTF-8"));
	    os.flush();
	    os.close();
	 
	    int responseCode = conn.getResponseCode();
	    //System.out.println("\nSending 'POST' request to URL : " + url);
	    //System.out.println("Post parameters : " + input);
	    //System.out.println("Response Code : " + responseCode);
	     
	    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
	    String inputLine;
	    StringBuffer response = new StringBuffer();
	    
	    while ((inputLine = in.readLine()) != null) {
	    	response.append(inputLine);
	    }
	    
	    in.close();

	    return response.toString();
	}
	
	public Map<String, Object> selectUwmsMapLocPositionQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwmsMapLocPositionQry(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}


	public Map<String, Object> selectUwmsMapLocMapSetInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwmsMapLocMapSetInfoQry(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectUwmsAgvProcessQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwmsAgvProcessList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

	public Map<String, Object> selectAgvOrderQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			//log.info(" ********** modelIns ====================================: " + modelIns);

			// //log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpAgvOrderQry(modelIns);

			// //log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF060", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			//log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public Map<String, Object> selectUwmsAgvCompleteQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpUwmsAgvCompleteList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}

    public Map<String, Object> agv_post_rest_api (Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try{

    	    List<Map<String, Object>> outputList = (List<Map<String, Object>>) model.get("DATA");
    		
        	String missionId   = (String) outputList.get(0).get("param4");
        			
			String body = "{"+ "   \"mission_id\":\"" + missionId + "\" "+ "}";
	     		      
        	URL url = new URL("http://192.168.0.20/api/v2.0.0/mission_queue");

        	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        	
        	connection.setRequestMethod("POST");
        	connection.setDoInput(true);
        	connection.setDoOutput(true);
        	connection.setRequestProperty("Content-Type", "application/json");
        	connection.setRequestProperty("Authorization", "Basic YWRtaW46OGM2OTc2ZTViNTQxMDQxNWJkZTkwOGJkNGRlZTE1ZGZiMTY3YTljODczZmM0YmI4YTgxZjZmMmFiNDQ4YTkxOA==");
        	connection.setRequestProperty("Accept-Language", "en_US"); 
        	OutputStream os = connection.getOutputStream();
        	os.write( body.getBytes("UTF-8") );
        	os.flush();
        	os.close();

        	System.out.println(">>> " + connection.getResponseCode() + " // " + HttpURLConnection.HTTP_OK);
        	
        	BufferedReader reader = new BufferedReader( new InputStreamReader( connection.getInputStream()));
        	StringBuffer buffer = new StringBuffer(); 

            int read = 0; 
            char[] cbuff = new char[1024]; 
            
            while ((read = reader.read(cbuff)) > 0){
            	buffer.append(cbuff, 0, read); 
            }

        	reader.close();
        	map.put("DS", buffer.toString());
        } catch(Exception e){
        	map.put("DS", "ERR");
        	System.out.println("Exception =========2===========");
        }
    	return map;
	}
    
    public Map<String, Object> selectNaviLoginQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKR(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpNaviLoginQry(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createReturnMapKr(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
    public Map<String, Object> selectAnywareZoneInfoQry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();
		try {

			checkParamMapValidationForList(model);

			checkUserInfoValidation(model);

			Map<String, Object> modelIns = createSPParamMapKrUWMS(model);
			
			log.info(" ********** modelIns ====================================: " + modelIns);

			// log.info(" ********** outputList 1 : " + modelIns);
			modelIns = (Map<String, Object>) dao.runSpAnywareZoneInfoList(modelIns);

			// log.info(" ********** outputList 2 : " + modelIns);
			InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));

			List<Map<String, Object>> outputList = (List<Map<String, Object>>) modelIns.get("O_CURSOR");
			log.info(" ********** outputList : " + outputList);

			map = createWorkInfoKrUwms(outputList, model);

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to select list :", e);
			}
			throw e;
		}

		return map;

	}
	
	public void updateEdiyaMobileCsComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpEdiyaMobileCsComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
	
	public void updateEdiyaMobileReturnComplete(Map<String, Object> model) throws Exception {

		String inputJSON = (String) model.get(ConstantWSIF.KEY_JSON_STRING);

		InterfaceBaseVOKRDate data = null;
		List<Map<String, Object>> workList = null;
		try {
			if (log.isInfoEnabled()) {
				//log.info("[ inputJSON ] :" + inputJSON);
			}

			data = InterfaceUtil.convertInterfaceVOFromJSONKRDate(inputJSON);

			if (log.isInfoEnabled()) {
				//log.info("[ data ] :" + data);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to excecute complete :", e);
			}
			throw new InterfaceException("5005", "JSON Data Convert Error");

		}

		if (data != null) {
			try {

				model.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
				model.put(ConstantWSIF.IF_KEY_PASSWORD, data.getPassword());

				checkUserInfoValidation(model);

			} catch (InterfaceException ife) {
				if (log.isErrorEnabled()) {
					log.error(ife.getMsgCode());
				}
				throw ife;

			}

			try {
				//log.info("[ data########################### ] :" + data);
				workList = getDataRowFromInterfaceVOKRDate(data);				
				if (log.isInfoEnabled()) {
					//log.info("===========================================================================");
					//log.info("[ workList ] :" + workList);
				}
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw new InterfaceException("5006", "Data ROW Convert Error");

			}
			
			try {
				if (workList != null && !workList.isEmpty()) {
					Map<String, Object> modelIns = getSPCompleteParamMapKRDate(data, workList);
					if (log.isInfoEnabled()) {
						//log.info("===========================================================================");
						//log.info("[ modelIns&&&&&&&&&&&&&&&&& ] :" + modelIns);
					}

					//for(int i=0; i<modelIns.size(); i++){
						////log.info("iLcId:" + modelIns.get("iLcId"));
						//String[] test = modelIns.
						
					//}
					
					log.info(" ********** outputList 1 : " + modelIns);
					modelIns = (Map<String, Object>) dao.runSpEdiyaMobileReturnComplete(modelIns);

					model.put("RETURN_CD", (String) modelIns.get("O_MSG_CODE"));
					model.put("RETURN_MSG", (String) modelIns.get("O_MSG_NAME"));
					InterfaceUtil.isValidReturnCode("WMSIF100", String.valueOf(modelIns.get("O_MSG_CODE")), (String) modelIns.get("O_MSG_NAME"));
					
				}
			} catch (InterfaceException ife) {
				throw ife;

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to excecute complete :", e);
				}
				throw e;

			}
		}

	}		
    
}

