package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF091Dao")
public class WMSIF091Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsPhoneShortList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsPhoneShortList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_phone_short_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsPhoneList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_phone_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsOrderList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_order_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsOrderDetailList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_order_detail_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList2
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsOrderDetailList2(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_order_detail_search2", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsComplete 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsManageComplete 
	 * Method 설명 : 배송관리 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsManageComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_manage_complete", model);
		return model;
	}
		
    /**
     * Method ID    : deliveryImgDataInsert
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object deliveryImgDataInsert(Map<String, Object> model){
        executeUpdate("pk_wmsif091.sp_delivery_as_img_data_insert", model);
        return model;
    }

	/*-
	 * Method ID : runSpDeliveryAsCancelComplete 
	 * Method 설명 : 배송취소요청관리 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsCancelComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_cancel_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsStartComplete 
	 * Method 설명 : 배송시작 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsStartComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_start_complete", model);
		return model;
	}	


	/*-
	 * Method ID : runSpDeliveryAsSendMsgComplete 
	 * Method 설명 : 배송 문자 전송 완료 정보 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsSendMsgComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_send_msg_complete", model);
		return model;
	}	


	/*-
	 * Method ID : runSpDeliveryAsDriverQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsDriverQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_driver_qry", model);
		return model;
	}


	/*-
	 * Method ID : runSpDeliveryAsDriverListQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsDriverListQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_driver_list_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsFirstBadComplete 
	 * Method 설명 : 배송 초도불량요청관리 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsFirstBadComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_first_bad_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsPartInfoQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsPartInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_part_info_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsExclusiveAsComplete 
	 * Method 설명 : 배송 초도불량요청관리 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsExclusiveAsComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_exclusive_as_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsAsErrorCdQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsAsErrorCdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_as_error_cd_qry", model);
		return model;
	}
	

	/*-
	 * Method ID : runSpDeliveryAsHolding 
	 * Method 설명 : 배송보류 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsHolding(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_holding", model);
		return model;
	}
	

	/*-
	 * Method ID : runSpDeliveryAsNotConnect 
	 * Method 설명 : 미배송 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsNotConnect(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_not_connect", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsBoardList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsBoardList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_board", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsBoardDetailList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsBoardDetailList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_board_detail", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsContentsSearch
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsContentsSearch(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_contents_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpInDlvsetComplete
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInDlvsetComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_in_dlvset_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsChangeReqDate 
	 * Method 설명 : 배송 예정일 변경 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsChangeReqDate(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_change_req_date", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpDeliveryAsCompleteNew 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsCompleteNew(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_complete_new", model);
		return model;
	}

	/*-
	 * Method ID : runSpInDeliverySetDriver
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInDeliverySetDriver(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_set_driver", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsPhoneChangeList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_phone_change_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsOrderChangeList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_order_change_search", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsInteriorPhotoList
	 * Method 설명 : 인테리어 해피콜 전화 고객 사진 조회 쿼리
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsInteriorPhotoList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_interior_photo_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsCompleteNew20200102 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsCompleteNew20200102(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_complete_new_20200101", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsTraceQry
	 * Method 설명 : 배송이력정보 조회 쿼리
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsTraceQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_trace_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsCompleteNew20200614 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsCompleteNew20200614(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_complete_new_20200614", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsCompleteNew20211231 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsCompleteNew20211231(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_complete_new_20211231", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpeliveryDriverSetQry
	 * Method 설명 : 배송기사별 배송예정일정 조회 
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2020-06-18
	 *
	 * @param model
	 * @return
	 */
	public Object runSpeliveryDriverSetQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_driver_set_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsTimeSetComplete 
	 * Method 설명 : 배송기사별 배송시간 설정
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2020-06-18
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsTimeSetComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_time_set_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsExtList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsExtList(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_ext_search", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpParcelOrdSearchQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 * 날짜 : 2021-02-10
	 * @param model
	 * @return
	 */
	public Object runSpParcelOrdSearchQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_parcel_ord_search", model);
		return model;
	}

	/*-
	 * Method ID : deliveryParcelComplete
	 * Method 설명 : 택배배송완료
	 * 작성자 : monkeySeok
	 * 날짜 : 2021-02-15
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsParcelComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_parcel_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsImgSender 
	 * Method 설명 : 배송사진 전송 
	 * 작성자 : MonkeySeok
	 * 날  짜  : 2021-05-28
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsImgSender(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_ing_sender", model);
		return model;
	}
	/*-
	 * Method ID : runSpDeliveryAsB2pSearchQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 * 날  짜  : 2021-07-01
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsB2pSearchQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_b2p_search_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsb2pComplete 
	 * Method 설명 : BP2 배송완료 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsb2pComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_b2p_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAsCompleteNew 
	 * Method 설명 : B2P 배송 출발
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsb2pStart(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_b2p_start", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsCompleteNew20220501 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsCompleteNew20220501(Map<String, Object> model) {
		executeUpdate("pk_wmsif091.sp_delivery_as_complete_new_20220501", model);
		return model;
	}
}
