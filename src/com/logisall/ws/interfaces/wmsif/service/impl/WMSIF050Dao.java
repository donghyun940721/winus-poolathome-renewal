package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF050Dao")
public class WMSIF050Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpLoginList
	 * Method 설명 : 로그인
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLoginList(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_login_list", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpAuthQry
	 * Method 설명 : 권한획득
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpAuthQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_auth_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpVersionQry
	 * Method 설명 : 권한획득
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpVersionQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_version_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInOrderQry
	 * Method 설명 : 입고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpInUsaOrderQry
	 * Method 설명 : 입고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInUsaOrderQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_usa_order_mapping_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpInUsaOrderQry
	 * Method 설명 : 입고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderMappingQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_mapping_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpInUsaOrderQry
	 * Method 설명 : 입고주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderDetailQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_detail_qry", model);
		return model;
	}
	
	
	/*-
	 * Method ID : runSpInOrderRemainQry
	 * Method 설명 : 입고잔여주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderRemainQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_remain_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpPltConfirmQry
	 * Method 설명 : 입고잔여주문조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPltConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_plt_confirm_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpReceiveInComplete 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_reveive_in_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceiveInComplete 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInPltComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_reveive_in_rti_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInOrderConfirmQry 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInOrderConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_confirm_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpPltMappingConfirmQry
	 * Method 설명 : 매핑시 매핑된 상태인지확인 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPltMappingConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_plt_mapping_confirm_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpPltMappingConfirmQry
	 * Method 설명 : 매핑시 매핑된 상태인지확인 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLcRitemQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_lc_ritem_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpMapPltConfirmQry
	 * Method 설명 : 물류용기입고시 매핑상태 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpMapPltConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_map_plt_confirm_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpEpcConfirmQry
	 * Method 설명 : 바코드 변환 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEpcConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_epc_confirm_qry", model);
		return model;
	}	

	/*-
	 * Method ID : runSpEpcHexConfirmQry
	 * Method 설명 : Hex 코드로 EPC코드 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpEpcHexConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_epc_hex_confirm_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpCustIdQry
	 * Method 설명 : 물류센터별 화주 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpCustIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_cust_id_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceivingOrderStatQry
	 * Method 설명 : 입고 주문 상태 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceivingOrderStatQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_receiving_order_stat_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInAutoOrderInsert
	 * Method 설명 : 출고주문자동입력
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInAutoOrderInsert(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_auto_order_insert", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceivingOrderStatQry
	 * Method 설명 : 롯트번호로 주문정보 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderLotDetailQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_lot_detail_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpSetPartComplete
	 * Method 설명 : 임가공 파트 입고 로직
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpSetPartComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_set_part_complete_mobile", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpPartInfoQry
	 * Method 설명 : 임가공 파트 식별표 바코드 정보 획득
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPartInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_set_part_info_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpInLotStatInfoQry
	 * Method 설명 : LOT번호로 정보 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInLotStatInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_lot_stat_info_qry", model);
		return model;
	}		
	
	/*-
	 * Method ID : runSpFcmPhoneComplete
	 * Method 설명 : 기기 FCM, 전화번호 전송 로직
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpFcmPhoneComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_fcm_phone_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInBoxAutoOrderInsert
	 * Method 설명 : 입고주문자동입력(box 수량 입력)
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInBoxAutoOrderInsert(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_box_auto_order_insert", model);
		return model;
	}
	
	
	/*-
	 * Method ID : runSpInOrderLotMultiQry
	 * Method 설명 : 입고주문조회시 동일 lot가 있을 경우 주문번호, seq 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderLotMultiQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_lot_multi_qry", model);
		return model;
	}			
	
	/*-
	 * Method ID : runSpReceiveInSapComplete 
	 * Method 설명 : 입하완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInSapComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_auto_order_sap_insert", model);
		return model;
	}	
		
    /**
     * Method ID    : WmsImgDataInsert
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object WmsImgDataInsert(Map<String, Object> model){
        executeUpdate("pk_wmsif090.sp_delivery_img_data_insert", model);
        return model;
    }
	
	/*-
	 * Method ID : runSpInOrdLocCompleteMobile 
	 * Method 설명 : 주문입력, 로케이션지정, 배차(옵션), 입고 완료 진행
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInOrdLocCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_ord_loc_complete_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInLcCarInfoQry 
	 * Method 설명 : 물류센터별 차량조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInLcCarInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_lc_car_info_qry", model);
		return model;
	}	

	
	/*-
	 * Method ID : runSpInTakingPicturesMobile 
	 * Method 설명 : 입출고사진저장
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInTakingPicturesMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_taking_pictures_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInInfoQry 
	 * Method 설명 : 물류센터별 기준 정보조회
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpInInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_info_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInOrderInsertQry
	 * Method 설명 : 입고주문등록을 위한 상품코드 조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderInsertQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_insert_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpLocWorkingComplete
	 * Method 설명 : 주문입력된 상태에서 모바일 로케이션 지정
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpLocWorkingComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_save_loc_mobile", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpTransCustInfoQry
	 * Method 설명 : 입출고거래처조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTransCustInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_trans_cust_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpTransItemInfoQry
	 * Method 설명 : 거래처별 상품조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpTransItemInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_trans_item_info_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpTransItemInfoQry
	 * Method 설명 : 거래처별 상품조회
	 * 작성자 : smics
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInOrderDateQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_date_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceiveInOrdInsSimpleIn
	 * Method 설명 : 입고 주문 입력 후 간편 입고 처리
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInOrdInsSimpleIn(Map<String, Object> model) {
		executeUpdate("pk_wmsop020.sp_ord_ins_simple_in", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceiveAfterGrn 
	 * Method 설명 : 입하완료 입고확정 프로세스 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveAfterGrn(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_reveive_after_grn", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpPackingPartInspection
	 * Method 설명 : 포장 부품 검수 결과 전송
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpPackingPartInspection(Map<String, Object> model) {
		executeUpdate("pk_wmsop020.sp_packing_part_inspection", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpReceiveOrderInsertConfirmGrn 
	 * Method 설명 : 입고 주문 입력 후 입하확정 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveOrderInsertConfirmGrn(Map<String, Object> model) {
		executeUpdate("pk_wmsop020.sp_order_insert_confirm_grn", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceiveReceivingCompleteGrn 
	 * Method 설명 : 입하확정 후 입고확정
	 * 작성자 : MonkeySEok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceiveReceivingCompleteGrn(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_receiving_complete_confirm_grn", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceivingOrdLocDayComplete 
	 * Method 설명 : 입고주문, 로케이션, 유통기한 입력 후 입고 확정
	 * 작성자 : MonkeySEok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpReceivingOrdLocDayComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_receiving_ord_loc_day_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpCancelGrnBadComplete 
	 * Method 설명 : 입하확정 후 입하 취소 및 불량 입고확정
	 * 작성자 : MonkeySEok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpCancelGrnBadComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_cancel_grn_bad_complete", model);
		return model;
	}	

    /**
     * Method ID    : ItemImgDataInsert
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object ItemImgDataInsert(Map<String, Object> model){
        executeUpdate("pk_wmsif050.sp_item_img_data_insert", model);
        return model;
    }
    
	/*-
	 * Method ID : runSpItemImgSenderMobile 
	 * Method 설명 : 모바일 상품 사진 등록
	 * 작성자 : MonkeySEok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpItemImgSenderMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_item_img_path_insert", model);
		return model;
	}	
	
	/*-
	 * Method ID : locDayInMobileComplete
	 * Method 설명 : 로케이션, 유통기한 입력 후 입고 확정
	 * 작성자 : SMICS
	 * 날 짜   : 2020.02.25
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpLocDayInMobileComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_loc_day_in_mobile_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpCustCalPltIdQry
	 * Method 설명 : 물류센터 화주의 정산용 PLT 조회 쿼리
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpCustCalPltIdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_cust_cal_plt_id_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpInIssueLabel
	 * Method 설명 : 레이블 발행 정보 입력
	 * 작성자 : SMICS
	 * 날 짜 : 2020.06.01
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLabel(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_label", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInIssueLabelInfoQry
	 * Method 설명 : 레이블 바코드 정보 조회
	 * 작성자 : smics
	 * 날 짜 : 2020.06.01
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLabelInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_issue_label_info_qry", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpReceiveInOrdInsDaySimpleIn
	 * Method 설명 : 자동 주문 입력 후 간편 입고(제조일자, 유통기한 추가)
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2020-06-18
	 *
	 * @param model
	 * @return
	 */
	public Object runSpReceiveInOrdInsDaySimpleIn(Map<String, Object> model) {
		executeUpdate("pk_wmsop020.sp_ord_ins_day_simple_in", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpOrdDiviedCompleteMobile
	 * Method 설명 : 주문을 파렛트 수량으로 나눠서 주문 입력
	 * 작성자 : SMICS
	 * 날 짜 : 2020-09-25
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrdDiviedCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_ord_divied_complete_mobile", model);
		return model;
	}
	
	/*-
	 * Method ID : OrdCngQtySimpleIn
	 * Method 설명 : 주문입력 후 SIMPLE IN(수량변경가능)
	 * 작성자 : SMICS
	 * 날 짜 : 2021-04-21
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrdCngQtySimpleIn(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_ord_cng_qty_simple_in", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpFixLocConfirmQry
	 * Method 설명 : 픽스 로케이션 추전 상품정보 조회
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpFixLocConfirmQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_fix_loc_confirm_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInIssueOrdLabel
	 * Method 설명 : 레이블 발행 정보 입력 후 주문정보에 lot 업데이트
	 * 작성자 : SMICS
	 * 날 짜 : 2021.05.16
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueOrdLabel(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_ord_label", model);
		return model;
	}
	
	/*-
	 * Method ID : inOrdDiviedCompleteMobile2048
	 * Method 설명 : 주문을 파렛트 수량으로 나눠서 주문 입력
	 * 작성자 : SMICS
	 * 날 짜 : 2021-07-06
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrdDiviedCompleteMobile2048(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_ord_divied_complete_mobile_2048", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInIssueLbLabel
	 * Method 설명 : 레이블 발행 정보 입력 후 주문정보에 lot 업데이트
	 * 작성자 : SMICS
	 * 날 짜 : 2021.05.16
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLbLabel(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_lb_label", model);
		return model;
	}
	
	/*-
	 * Method ID : inOrdDiviedCompleteMobile2048
	 * Method 설명 : 발행된 레이블 정보를 기반으로 주문입력, 로케이션 지정, 입고 확정
	 * 작성자 : SMICS
	 * 날 짜 : 2021-07-18
	 *
	 *
	 * @param model
	 * @return
	 */
	public Object runSpOrd2048LocCompleteMobile(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_order_2048_loc_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpInIssueLabelLbOrdComplete
	 * Method 설명 : 레이블 발행 정보 입력 후 입고 주문 생성
	 * 작성자 : SMICS
	 * 날 짜 : 2021.08.12
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInIssueLabelLbOrdComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif050.sp_in_issue_label_lb_ord_complete", model);
		return model;
	}
}
