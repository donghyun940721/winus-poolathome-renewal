package com.logisall.ws.interfaces.wmsif.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceDao;

@Repository("WMSIF090Dao")
public class WMSIF090Dao extends AbstractInterfaceDao {

	protected Log log = LogFactory.getLog(this.getClass());
		
	/*-
	 * Method ID : runSpDeliveryList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_search", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryHdNotcompleteList
	 * Method 설명 : 오늘 이전 미배송조회
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2022.08.26
	 * @param inputJSON
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryHdNotcompleteList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_hd_notcomplete_search", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryHdList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryHdList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_hd_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryPhoneShortList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryPhoneShortList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_phone_short_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryPhoneList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_phone_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryOrderList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_order_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryOrderDetailList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_order_detail_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList2
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryOrderDetailList2(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_order_detail_search2", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryComplete 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryManageComplete 
	 * Method 설명 : 배송관리 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryManageComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_manage_complete", model);
		return model;
	}
		
    /**
     * Method ID    : deliveryImgDataInsert
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object deliveryImgDataInsert(Map<String, Object> model){
        executeUpdate("pk_wmsif090.sp_delivery_img_data_insert", model);
        return model;
    }

	/*-
	 * Method ID : runSpDeliveryCancelComplete 
	 * Method 설명 : 배송취소요청관리 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryCancelComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_cancel_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryStartComplete 
	 * Method 설명 : 배송시작 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryStartComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_start_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpDeliverySendMsgComplete 
	 * Method 설명 : 배송 문자 전송 완료 정보 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliverySendMsgComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_send_msg_complete", model);
		return model;
	}	

	/*-
	 * Method ID : runSpDeliveryDriverQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryDriverHdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_driver_hd_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryDriverQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryDriverQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_driver_qry", model);
		return model;
	}


	/*-
	 * Method ID : runSpDeliveryDriverListQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryDriverListQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_driver_list_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryFirstBadComplete 
	 * Method 설명 : 배송 초도불량요청관리 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryFirstBadComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_first_bad_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryPartInfoQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryPartInfoQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_part_info_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpdeliveryExclusiveAsComplete 
	 * Method 설명 : 배송 초도불량요청관리 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpdeliveryExclusiveAsComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_exclusive_as_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryAsErrorCdQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAsErrorCdQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_as_error_cd_qry", model);
		return model;
	}
	

	/*-
	 * Method ID : runSpDeliveryHolding 
	 * Method 설명 : 배송보류 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryHolding(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_holding", model);
		return model;
	}
	

	/*-
	 * Method ID : runSpDeliveryNotConnect 
	 * Method 설명 : 미배송 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryNotConnect(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_not_connect", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryBoardList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryBoardList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_board", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryBoardDetailList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryBoardDetailList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_board_detail", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryContentsSearch
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryContentsSearch(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_contents_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpInDlvsetComplete
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInDlvsetComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_in_dlvset_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryChangeReqDate 
	 * Method 설명 : 배송 예정일 변경 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryChangeReqDate(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_change_req_date", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpDeliveryCompleteNew 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryCompleteNew(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_complete_new", model);
		return model;
	}

	/*-
	 * Method ID : runSpInDeliverySetDriver
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpInDeliverySetDriver(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_set_driver", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryPhoneChangeList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_phone_change_search", model);
		return model;
	}

	/*-
	 * Method ID : runSpAcceptingList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryOrderChangeList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_order_change_search", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryInteriorPhotoList
	 * Method 설명 : 인테리어 해피콜 전화 고객 사진 조회 쿼리
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryInteriorPhotoList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_interior_photo_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryCompleteNew20200102 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryCompleteNew20200102(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_complete_new_20200101", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryTraceQry
	 * Method 설명 : 배송이력정보 조회 쿼리
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryTraceQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_trace_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryCompleteNew20200614 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryCompleteNew20200614(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_complete_new_20200614", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryCompleteNew20211231 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryCompleteNew20211231(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_complete_new_20211231", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpeliveryDriverSetQry
	 * Method 설명 : 배송기사별 배송예정일정 조회 
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2020-06-18
	 *
	 * @param model
	 * @return
	 */
	public Object runSpeliveryDriverSetQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_driver_set_qry", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryTimeSetComplete 
	 * Method 설명 : 배송기사별 배송시간 설정
	 * 작성자 : MonkeySeok
	 * 날 짜 : 2020-06-18
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryTimeSetComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_time_set_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryExtList
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryExtList(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_ext_search", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpParcelOrdSearchQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 * 날짜 : 2021-02-10
	 * @param model
	 * @return
	 */
	public Object runSpParcelOrdSearchQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_parcel_ord_search", model);
		return model;
	}

	/*-
	 * Method ID : deliveryParcelComplete
	 * Method 설명 : 택배배송완료
	 * 작성자 : monkeySeok
	 * 날짜 : 2021-02-15
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryParcelComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_parcel_complete", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryImgSender 
	 * Method 설명 : 배송사진 전송 
	 * 작성자 : MonkeySeok
	 * 날  짜  : 2021-05-28
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryImgSender(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_ing_sender", model);
		return model;
	}
	/*-
	 * Method ID : runSpDeliveryB2pSearchQry
	 * Method 설명 : 
	 * 작성자 : MonkeySeok
	 * 날  짜  : 2021-07-01
	 *
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryB2pSearchQry(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_b2p_search_qry", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryb2pComplete 
	 * Method 설명 : BP2 배송완료 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryb2pComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_b2p_complete", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryCompleteNew 
	 * Method 설명 : B2P 배송 출발
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryb2pStart(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_b2p_start", model);
		return model;
	}

	/*-
	 * Method ID : runSpDeliveryCompleteNew20220501 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryCompleteNew20220501(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_complete_new_20220501", model);
		return model;
	}
	
	/*-
	 * Method ID : runSpDeliveryAllStartComplete 
	 * Method 설명 : 일괄 배송시작 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 날짜 : 2022-08-26
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryAllStartComplete(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_all_start_complete", model);
		return model;
	}	
	
	/*-
	 * Method ID : runSpDeliveryResetAfterStart 
	 * Method 설명 : 배송출발 후 배송 초기화
	 * 작성자 : MonkeySeok
	 * 날 자  : 2022-11-01
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryResetAfterStart(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_reset_after_start", model);
		return model;
	}	

	/*-
	 * Method ID : runSpDeliveryCompleteNew20220501 
	 * Method 설명 : 배송완료 프로시져 호출, 
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public Object runSpDeliveryCompleteNew20230410(Map<String, Object> model) {
		executeUpdate("pk_wmsif090.sp_delivery_complete_new_20230410", model);
		return model;
	}
}
