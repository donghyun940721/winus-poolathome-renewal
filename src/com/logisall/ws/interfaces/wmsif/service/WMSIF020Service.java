package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF020Service extends AbstractInterfaceService {

    public Map<String, Object> selectPickingList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectPickingQry(Map<String, Object> model) throws Exception;
    public void updatePickingOrderComplete(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> selectShippingingList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectSearchList(Map<String, Object> model) throws Exception;
    public void updateOutComplete(Map<String, Object> model) throws Exception;
    
    public void updateMappingComplete(Map<String, Object> model) throws Exception;
    public void updateMappingDeleteComplete(Map<String, Object> model) throws Exception;
    
}
