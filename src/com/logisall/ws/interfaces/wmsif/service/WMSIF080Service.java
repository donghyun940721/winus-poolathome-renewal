package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF080Service extends AbstractInterfaceService {

	public Map<String, Object> selectStockQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectStockInoutQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectStockLocationQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectStockLotQry(Map<String, Object> model) throws Exception;	
	public Map<String, Object> selectStockPriorityQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectRealtimeStockConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectMappingStatConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectSearchLocationQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectSetPartConfirmQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectSetStatQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLocSearchIdQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLotItemStockQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectDlvInoutStatQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectItemInfoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectRtiTraceQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectRitemSerialInfoQry(Map<String, Object> model) throws Exception;
    public void updateReqHelpdesk(Map<String, Object> model) throws Exception; 
	public void updateItemBarcodeSender(Map<String, Object> model) throws Exception;
	public void updateStocktakingFirst(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectRefurbishedSerialQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectSubulQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLabelPrintingInfoQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectEdiyaNotDepartQry(Map<String, Object> model) throws Exception;
	public void updateItemUomRegComplete(Map<String, Object> model) throws Exception;
	public void updateDasCheckCompleteMobile(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectEdiyaDeviceQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectSupplementListQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLocHeaderListQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectEdiyaFactoryLabelListQry(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLcBoxListQry(Map<String, Object> model) throws Exception;
}
