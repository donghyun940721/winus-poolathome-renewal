package com.logisall.ws.interfaces.wmsif.service;

import java.util.Map;

import com.logisall.ws.interfaces.common.service.AbstractInterfaceService;

public interface WMSIF090Service extends AbstractInterfaceService {

    public Map<String, Object> selectDeliveryQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryHdQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryHdNotcompleteQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryPhoneQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryOrderQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryOrderDetailQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryOrderDetailQry2(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryPhoneShortQry(Map<String, Object> model) throws Exception;
    public void DeliveryComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryManageComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> DeliveryImgDataInsert(Map<String, Object> model) throws Exception;
    public void DeliveryCancelComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryStartComplete(Map<String, Object> model) throws Exception; 
    public void DeliverySendMsgComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryDriverQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryDriverHdQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryDriverListQry(Map<String, Object> model) throws Exception;
    public void DeliveryFirstBadComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryPartInfoQry(Map<String, Object> model) throws Exception;
    public void deliveryExclusiveAsComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryAsErrorCdQry(Map<String, Object> model) throws Exception;
    public void DeliveryHolding(Map<String, Object> model) throws Exception; 
    public void DeliveryNotConnect(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryboardQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryBoardDetailQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryContentsSearch(Map<String, Object> model) throws Exception;
    public void updateInDlvsetComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryChangeReqDate(Map<String, Object> model) throws Exception; 
    public void DeliveryCompleteNew(Map<String, Object> model) throws Exception; 
    public void updateDeliverySetDriver(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryPhoneChangeQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryOrderChangeQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectDeliveryInteriorPhotoQry(Map<String, Object> model) throws Exception;
    public void DeliveryCompleteNew20200101(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryTraceQry(Map<String, Object> model) throws Exception;
    public void DeliveryCompleteNew20200614(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryDriverSetQry(Map<String, Object> model) throws Exception;
    public void updateDeliveryTimeSetComplete(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryExtQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectParcelOrdSearchQry(Map<String, Object> model) throws Exception;
    public void DeliveryParcelComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryImgSender(Map<String, Object> model) throws Exception; 
    public Map<String, Object> selectDeliveryB2pSearchQry(Map<String, Object> model) throws Exception;
    public Map<String, Object> DeliveryImgDataInsertThirty(Map<String, Object> model) throws Exception;
    public void Deliveryb2pComplete(Map<String, Object> model) throws Exception; 
    public void updatedeliveryb2pStart(Map<String, Object> model) throws Exception; 
    public void DeliveryCompleteNew20211231(Map<String, Object> model) throws Exception; 
    public void DeliveryCompleteNew20220501(Map<String, Object> model) throws Exception; 
    public void DeliveryAllStartComplete(Map<String, Object> model) throws Exception; 
    public void DeliveryResetAfterStart(Map<String, Object> model) throws Exception; 
    public void DeliveryCompleteNew20230410(Map<String, Object> model) throws Exception; 
}
