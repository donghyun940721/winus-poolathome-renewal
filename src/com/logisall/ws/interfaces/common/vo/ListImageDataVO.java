package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;

public class ListImageDataVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;

	private String idx;
	private String content;
	
	public String getIdx() {
		return idx;
	}
	public void setIdx(String idx) {
		this.idx = idx;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("ListImageDataVO [idx=");
		builder.append(idx);
		builder.append(", content=");
		builder.append(content);
		builder.append("]");
		return builder.toString();
	}
}
