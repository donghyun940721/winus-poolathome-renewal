package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVOGodomall implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String orderNo;
	private String memNo;
	private String orderStatus;
	private String orderIp;
	private String orderChannelFl;
	private String orderTypeFl;
	private String orderEmail;
	private String orderGoodsNm;
	private String orderGoodsCnt;
	private String settlePrice;
	private String taxSupplyPrice;
	private String taxVatPrice;
	private String taxFreePrice;
	private String realTaxSupplyPrice;
	private String realTaxvatPrice;
	private String realTaxFreePrice;
	private String useMileage;
	private String useDeposit;
	private String totalGoodsPrice;
	private String totalDeliveryCharge;
	private String totalGoodsDcPrice;
	private String totalMemberOverlapDcPrice;
	private String totalCouponGoodsDcPrice;
	private String totalCouponOrderDcPrice;
	private String totalCouponDeliveryDcPrice;
	private String totalMileage;
	private String totalGoodsMileage;
	private String totalMemberMileage;
	private String totalCouponGoodsMileage;
	private String totalCouponOrderMileage;
	private String firstSaleFl;
	private String settleKind;
	private String paymentDt;
	private String orderDate;
	private String apiOrderNo;
	private String addField;
	private String multiShippingFl;
	private String bankName;
	private String accountNumber;
	private String depositor;
	private String memId;
	private String memGroupNm;	
	private String mallSno;
	
//	private OrderDeliveryDataVO orderDeliveryData;	
//	private OrderInfoDataVO orderInfoData;	
//	private OrderGoodsDataVO orderGoodsData;
//	private AddGoodsDataVO addGoodsData;	
//	private GiftDataVO giftData;	
//	private ExchangeInfoDataVO exchangeInfoData;
//	private OrderConsultDataVO orderConsultData;
//	private AddGoodsSnoDataVO addGoodsSnoData;
	


	private List<OrderDeliveryDataVO> orderDeliveryData;
	private List<OrderInfoDataVO> orderInfoData;
	private List<OrderGoodsDataVO> orderGoodsData;
	
	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getApiOrderNo() {
		return apiOrderNo;
	}

	public void setApiOrderNo(String apiOrderNo) {
		this.apiOrderNo = apiOrderNo;
	}

	public String getMallSno() {
		return mallSno;
	}

	public void setMallSno(String mallSno) {
		this.mallSno = mallSno;
	}

	public String getMemNo() {
		return memNo;
	}

	public void setMemNo(String memNo) {
		this.memNo = memNo;
	}

	public String getOrderStatus() {
		return orderStatus;
	}

	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}

	public String getOrderIp() {
		return orderIp;
	}

	public void setOrderIp(String orderIp) {
		this.orderIp = orderIp;
	}

	public String getOrderChannelFl() {
		return orderChannelFl;
	}

	public void setOrderChannelFl(String orderChannelFl) {
		this.orderChannelFl = orderChannelFl;
	}

	public String getOrderTypeFl() {
		return orderTypeFl;
	}

	public void setOrderTypeFl(String orderTypeFl) {
		this.orderTypeFl = orderTypeFl;
	}
	
	public String getOrderEmail() {
		return orderEmail;
	}

	public void setOrderEmail(String orderEmail) {
		this.orderEmail = orderEmail;
	}
	
	public String getOrderGoodsNm() {
		return orderGoodsNm;
	}

	public void setOrderGoodsNm(String orderGoodsNm) {
		this.orderGoodsNm = orderGoodsNm;
	}
	
	public String getOrderGoodsCnt() {
		return orderGoodsCnt;
	}

	public void setOrderGoodsCnt(String orderGoodsCnt) {
		this.orderGoodsCnt = orderGoodsCnt;
	}

	public String getSettlePrice() {
		return settlePrice;
	}

	public void setSettlePrice(String settlePrice) {
		this.settlePrice = settlePrice;
	}

	public String getTaxSupplyPrice() {
		return taxSupplyPrice;
	}

	public void setTaxSupplyPrice(String taxSupplyPrice) {
		this.taxSupplyPrice = taxSupplyPrice;
	}

	public String getTaxVatPrice() {
		return taxVatPrice;
	}

	public void setTaxVatPrice(String taxVatPrice) {
		this.taxVatPrice = taxVatPrice;
	}

	public String getTaxFreePrice() {
		return taxFreePrice;
	}

	public void setTaxFreePrice(String taxFreePrice) {
		this.taxFreePrice = taxFreePrice;
	}

	public String getRealTaxSupplyPrice() {
		return realTaxSupplyPrice;
	}

	public void setRealTaxSupplyPrice(String realTaxSupplyPrice) {
		this.realTaxSupplyPrice = realTaxSupplyPrice;
	}

	public String getRealTaxvatPrice() {
		return realTaxvatPrice;
	}

	public void setRealTaxvatPrice(String realTaxvatPrice) {
		this.realTaxvatPrice = realTaxvatPrice;
	}

	public String getRealTaxFreePrice() {
		return realTaxFreePrice;
	}

	public void setRealTaxFreePrice(String realTaxFreePrice) {
		this.realTaxFreePrice = realTaxFreePrice;
	}

	public String getUseMileage() {
		return useMileage;
	}

	public void setUseMileage(String useMileage) {
		this.useMileage = useMileage;
	}

	public String getUseDeposit() {
		return useDeposit;
	}

	public void setUseDeposit(String useDeposit) {
		this.useDeposit = useDeposit;
	}

	public String getTotalGoodsPrice() {
		return totalGoodsPrice;
	}

	public void setTotalGoodsPrice(String totalGoodsPrice) {
		this.totalGoodsPrice = totalGoodsPrice;
	}

	public String getTotalDeliveryCharge() {
		return totalDeliveryCharge;
	}

	public void setTotalDeliveryCharge(String totalDeliveryCharge) {
		this.totalDeliveryCharge = totalDeliveryCharge;
	}

	public String getTotalGoodsDcPrice() {
		return totalGoodsDcPrice;
	}

	public void setTotalGoodsDcPrice(String totalGoodsDcPrice) {
		this.totalGoodsDcPrice = totalGoodsDcPrice;
	}

	public String getTotalMemberOverlapDcPrice() {
		return totalMemberOverlapDcPrice;
	}

	public void setTotalMemberOverlapDcPrice(String totalMemberOverlapDcPrice) {
		this.totalMemberOverlapDcPrice = totalMemberOverlapDcPrice;
	}

	public String getTotalCouponGoodsDcPrice() {
		return totalCouponGoodsDcPrice;
	}

	public void setTotalCouponGoodsDcPrice(String totalCouponGoodsDcPrice) {
		this.totalCouponGoodsDcPrice = totalCouponGoodsDcPrice;
	}

	public String getTotalCouponOrderDcPrice() {
		return totalCouponOrderDcPrice;
	}

	public void setTotalCouponOrderDcPrice(String totalCouponOrderDcPrice) {
		this.totalCouponOrderDcPrice = totalCouponOrderDcPrice;
	}

	public String getTotalCouponDeliveryDcPrice() {
		return totalCouponDeliveryDcPrice;
	}

	public void setTotalCouponDeliveryDcPrice(String totalCouponDeliveryDcPrice) {
		this.totalCouponDeliveryDcPrice = totalCouponDeliveryDcPrice;
	}

	public String getTotalMileage() {
		return totalMileage;
	}

	public void setTotalMileage(String totalMileage) {
		this.totalMileage = totalMileage;
	}

	public String getTotalGoodsMileage() {
		return totalGoodsMileage;
	}

	public void setTotalGoodsMileage(String totalGoodsMileage) {
		this.totalGoodsMileage = totalGoodsMileage;
	}

	public String getTotalMemberMileage() {
		return totalMemberMileage;
	}

	public void setTotalMemberMileage(String totalMemberMileage) {
		this.totalMemberMileage = totalMemberMileage;
	}

	public String getTotalCouponGoodsMileage() {
		return totalCouponGoodsMileage;
	}

	public void setTotalCouponGoodsMileage(String totalCouponGoodsMileage) {
		this.totalCouponGoodsMileage = totalCouponGoodsMileage;
	}

	public String getTotalCouponOrderMileage() {
		return totalCouponOrderMileage;
	}

	public void setTotalCouponOrderMileage(String totalCouponOrderMileage) {
		this.totalCouponOrderMileage = totalCouponOrderMileage;
	}

	public String getFirstSaleFl() {
		return firstSaleFl;
	}

	public void setFirstSaleFl(String firstSaleFl) {
		this.firstSaleFl = firstSaleFl;
	}

	public String getSettleKind() {
		return settleKind;
	}

	public void setSettleKind(String settleKind) {
		this.settleKind = settleKind;
	}

	public String getPaymentDt() {
		return paymentDt;
	}

	public void setPaymentDt(String paymentDt) {
		this.paymentDt = paymentDt;
	}

	public String getOrderDate() {
		return orderDate;
	}

	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}

	public String getAddField() {
		return addField;
	}

	public void setAddField(String addField) {
		this.addField = addField;
	}

	public String getMultiShippingFl() {
		return multiShippingFl;
	}

	public void setMultiShippingFl(String multiShippingFl) {
		this.multiShippingFl = multiShippingFl;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getDepositor() {
		return depositor;
	}

	public void setDepositor(String depositor) {
		this.depositor = depositor;
	}

	public String getMemId() {
		return memId;
	}

	public void setMemId(String memId) {
		this.memId = memId;
	}

	public String getMemGroupNm() {
		return memGroupNm;
	}

	public void setMemGroupNm(String memGroupNm) {
		this.memGroupNm = memGroupNm;
	}

//	public String getOrderDeliveryData() {
//		return orderDeliveryData;
//	}
//
//	public void setOrderDeliveryData(String orderDeliveryData) {
//		this.orderDeliveryData = orderDeliveryData;
//	}
//
//	public String getAddGoodsData() {
//		return addGoodsData;
//	}
//
//	public void setAddGoodsData(String addGoodsData) {
//		this.addGoodsData = addGoodsData;
//	}
//
//	public String getGiftData() {
//		return giftData;
//	}
//
//	public void setGiftData(String giftData) {
//		this.giftData = giftData;
//	}
//
//	public String getOrderGoodsData() {
//		return orderGoodsData;
//	}
//
//	public void setOrderGoodsData(String orderGoodsData) {
//		this.orderGoodsData = orderGoodsData;
//	}
//
//	public String getOrderConsultData() {
//		return orderConsultData;
//	}
//
//	public void setOrderConsultData(String orderConsultData) {
//		this.orderConsultData = orderConsultData;
//	}


//	public OrderGoodsDataVO getOrderGoodsData() {
//		return orderGoodsData;
//	}
//
//	public void setOrderGoodsData(OrderGoodsDataVO orderGoodsData) {
//		this.orderGoodsData = orderGoodsData;
//	}

//	public OrderInfoDataVO getOrderInfoData() {
//		return orderInfoData;
//	}
//
//	public void setOrderInfoData(OrderInfoDataVO orderInfoData) {
//		this.orderInfoData = orderInfoData;
//	}
//
//	public OrderDeliveryDataVO getOrderDeliveryData() {
//		return orderDeliveryData;
//	}
//
//	public void setOrderDeliveryData(OrderDeliveryDataVO orderDeliveryData) {
//		this.orderDeliveryData = orderDeliveryData;
//	}
	


	public List<OrderGoodsDataVO> getOrderGoodsData() {
		return orderGoodsData;
	}

	public void setOrderGoodsData(List<OrderGoodsDataVO> orderGoodsData) {
		this.orderGoodsData = orderGoodsData;
	}
	
	public List<OrderInfoDataVO> getOrderInfoData() {
		return orderInfoData;
	}

	public void setOrderInfoData(List<OrderInfoDataVO> orderInfoData) {
		this.orderInfoData = orderInfoData;
	}

	public List<OrderDeliveryDataVO> getOrderDeliveryData() {
		return orderDeliveryData;
	}

	public void setOrderDeliveryData(List<OrderDeliveryDataVO> orderDeliveryData) {
		this.orderDeliveryData = orderDeliveryData;
	}

//	public AddGoodsDataVO getAddGoodsData() {
//		return addGoodsData;
//	}
//
//	public void setAddGoodsData(AddGoodsDataVO addGoodsData) {
//		this.addGoodsData = addGoodsData;
//	}
//
//
//	public GiftDataVO getGiftData() {
//		return giftData;
//	}
//
//	public void setGiftData(GiftDataVO giftData) {
//		this.giftData = giftData;
//	}
//
//
//	public ExchangeInfoDataVO getExchangeInfoData() {
//		return exchangeInfoData;
//	}
//
//	public void setExchangeInfoData(ExchangeInfoDataVO exchangeInfoData) {
//		this.exchangeInfoData = exchangeInfoData;
//	}
//
//
//	public OrderConsultDataVO getOrderConsultData() {
//		return orderConsultData;
//	}
//
//	public void setOrderConsultData(OrderConsultDataVO orderConsultData) {
//		this.orderConsultData = orderConsultData;
//	}
//
//
//	public AddGoodsSnoDataVO getAddGoodsSnoData() {
//		return addGoodsSnoData;
//	}
//
//	public void setgetAddGoodsSnoData(AddGoodsSnoDataVO addGoodsSnoData) {
//		this.addGoodsSnoData = addGoodsSnoData;
//	}
	
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOGodomall [orderNo=");
		builder.append(orderNo);
		builder.append(", apiOrderNo=");
		builder.append(apiOrderNo);
		builder.append(", mallSno=");
		builder.append(mallSno);
		builder.append(", memNo=");
		builder.append(memNo);
		builder.append(", orderStatus=");
		builder.append(orderStatus);
		builder.append(", orderIp=");
		builder.append(orderIp);
		builder.append(", orderChannelFl=");
		builder.append(orderChannelFl);
		builder.append(", orderTypeFl=");
		builder.append(orderTypeFl);
		builder.append(", orderEmail=");
		builder.append(orderEmail);
		builder.append(", orderGoodsNm=");
		builder.append(orderGoodsNm);
		builder.append(", orderGoodsCnt=");
		builder.append(orderGoodsCnt);
		builder.append(", settlePrice=");
		builder.append(settlePrice);
		builder.append(", taxSupplyPrice=");
		builder.append(taxSupplyPrice);
		builder.append(", taxVatPrice=");
		builder.append(taxVatPrice);
		builder.append(", taxFreePrice=");
		builder.append(taxFreePrice);
		builder.append(", realTaxSupplyPrice=");
		builder.append(realTaxSupplyPrice);
		builder.append(", realTaxvatPrice=");
		builder.append(realTaxvatPrice);
		builder.append(", realTaxFreePrice=");
		builder.append(realTaxFreePrice);
		builder.append(", useMileage=");
		builder.append(useMileage);
		builder.append(", useDeposit=");
		builder.append(useDeposit);
		builder.append(", totalGoodsPrice=");
		builder.append(totalGoodsPrice);
		builder.append(", totalDeliveryCharge=");
		builder.append(totalDeliveryCharge);
		builder.append(", totalGoodsDcPrice=");
		builder.append(totalGoodsDcPrice);
		builder.append(", totalMemberOverlapDcPrice=");
		builder.append(totalMemberOverlapDcPrice);
		builder.append(", totalCouponGoodsDcPrice=");
		builder.append(totalCouponGoodsDcPrice);
		builder.append(", totalCouponOrderDcPrice=");
		builder.append(totalCouponOrderDcPrice);
		builder.append(", totalCouponDeliveryDcPrice=");
		builder.append(totalCouponDeliveryDcPrice);
		builder.append(", totalMileage=");
		builder.append(totalMileage);
		builder.append(", totalGoodsMileage=");
		builder.append(totalGoodsMileage);
		builder.append(", totalMemberMileage=");
		builder.append(totalMemberMileage);
		builder.append(", totalCouponGoodsMileage=");
		builder.append(totalCouponGoodsMileage);
		builder.append(", totalCouponOrderMileage=");
		builder.append(totalCouponOrderMileage);
		builder.append(", firstSaleFl=");
		builder.append(firstSaleFl);
		builder.append(", settleKind=");
		builder.append(settleKind);
		builder.append(", paymentDt=");
		builder.append(paymentDt);
		builder.append(", orderDate=");
		builder.append(orderDate);
		builder.append(", addField=");
		builder.append(addField);
		builder.append(", multiShippingFl=");
		builder.append(multiShippingFl);
		builder.append(", bankName=");
		builder.append(bankName);
		builder.append(", accountNumber=");
		builder.append(accountNumber);
		builder.append(", depositor=");
		builder.append(depositor);
		builder.append(", memId=");
		builder.append(memId);
		builder.append(", memGroupNm=");
		builder.append(memGroupNm);
		builder.append(", orderDeliveryData=");
		builder.append(orderDeliveryData);
		builder.append(", orderGoodsData=");
		builder.append(orderGoodsData);
		builder.append(", orderDeliveryData=");
		builder.append(orderDeliveryData);
		builder.append(", orderInfoData=");
		builder.append(orderInfoData);
//		builder.append(", addGoodsData=");
//		builder.append(addGoodsData);
//		builder.append(", giftData=");
//		builder.append(giftData);
//		builder.append(", orderConsultData=");
//		builder.append(orderConsultData);
//		builder.append(", addGoodsData=");
//		builder.append(addGoodsData);
//		builder.append(", giftData=");
//		builder.append(giftData);
//		builder.append(", exchangeInfoData=");
//		builder.append(exchangeInfoData);
//		builder.append(", orderConsultData=");
//		builder.append(orderConsultData);
//		builder.append(", addGoodsSnoData=");
//		builder.append(addGoodsSnoData);
		builder.append("]");
		return builder.toString();
	}

}