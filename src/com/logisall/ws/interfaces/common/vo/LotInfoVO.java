package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class LotInfoVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;
	
	private String orderSeq;
	private String qty;
	private String realQty;
	private String fromLocLocalCd;
	private String toLocLocalCd;
	private String fromLocGlobalCd;
	private String toLocGlobalCd;
	private Date expiryDt;
	private String expiryDtType;
	private Date makeDt;
	private String lotNo;
		
	private List<ItemSerialInfoVO> itemSerialList;
	

	public String getOrderSeq() {
		return orderSeq;
	}

	public void setOrderSeq(String orderSeq) {
		this.orderSeq = orderSeq;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getRealQty() {
		return realQty;
	}

	public void setRealQty(String realQty) {
		this.realQty = realQty;
	}
	
	public String getFromLocLocalCd() {
		return fromLocLocalCd;
	}
	
	public void setFromLocLocalCd(String fromLocLocalCd) {
		this.fromLocLocalCd = fromLocLocalCd;
	}
	
	public String getToLocLocalCd() {
		return toLocLocalCd;
	}
	
	public void setToLocLocalCd(String toLocLocalCd) {
		this.toLocLocalCd = toLocLocalCd;
	}
	
	public String getFromLocGlobalCd() {
		return fromLocGlobalCd;
	}
	
	public void setFromLocGlobalCd(String fromLocGlobalCd) {
		this.fromLocGlobalCd = fromLocGlobalCd;
	}
	
	public String getToLocGlobalCd() {
		return toLocGlobalCd;
	}
	
	public void setToLocGlobalCd(String toLocGlobalCd) {
		this.toLocGlobalCd = toLocGlobalCd;
	}

	public Date getExpiryDt() {
		return expiryDt;
	}

	public void setExpiryDt(Date expiryDt) {
		this.expiryDt = expiryDt;
	}

	public String getExpiryDtType() {
		return expiryDtType;
	}

	public void setExpiryDtType(String expiryDtType) {
		this.expiryDtType = expiryDtType;
	}

	public Date getMakeDt() {
		return makeDt;
	}

	public void setMakeDt(Date makeDt) {
		this.makeDt = makeDt;
	}

	public String getLotNo() {
		return lotNo;
	}

	public void setLotNo(String lotNo) {
		this.lotNo = lotNo;
	}

	public List<ItemSerialInfoVO> getItemSerialList() {
		return itemSerialList;
	}

	public void setItemSerialList(List<ItemSerialInfoVO> itemSerialList) {
		this.itemSerialList = itemSerialList;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("LotInfoVO [orderSeq=");
		builder.append(orderSeq);
		builder.append(", qty=");
		builder.append(qty);
		builder.append(", realQty=");
		builder.append(realQty);
		builder.append(", fromLocLocalCd=");
		builder.append(fromLocLocalCd);
		builder.append(", toLocLocalCd=");
		builder.append(toLocLocalCd);
		builder.append(", fromLocGlobalCd=");
		builder.append(fromLocGlobalCd);
		builder.append(", toLocGlobalCd=");
		builder.append(toLocGlobalCd);
		builder.append(", expiryDt=");
		builder.append(expiryDt);
		builder.append(", expiryDtType=");
		builder.append(expiryDtType);
		builder.append(", makeDt=");
		builder.append(makeDt);
		builder.append(", lotNo=");
		builder.append(lotNo);
		builder.append(", itemSerialList=");
		builder.append(itemSerialList);
		builder.append("]");
		return builder.toString();
	}


}
