package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class InterfaceBaseVOGodomall implements Serializable {

	private static final long serialVersionUID = -6445511060732512635L;
	
//	private WorkListBaseVOGodomall orderData;
//
//	public WorkListBaseVOGodomall getOrderData() {
//		return orderData;
//	}
//
//	public void setOrderData(WorkListBaseVOGodomall orderData) {
//		this.orderData = orderData;
//	}
//
//	@Override
//	public String toString() {
//		StringBuffer builder = new StringBuffer();
//		builder.append("WorkListBaseVOGodomall [orderData=");
//		builder.append(orderData);
//		builder.append("]");
//		return builder.toString();
//	}
	


	private List<WorkListBaseVOGodomall> orderData;

	public List<WorkListBaseVOGodomall> getOrderData() {
		return orderData;
	}

	public void setOrderData(List<WorkListBaseVOGodomall> orderData) {
		this.orderData = orderData;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOGodomall [orderData=");
		builder.append(orderData);
		builder.append("]");
		return builder.toString();
	}
	
	

}
