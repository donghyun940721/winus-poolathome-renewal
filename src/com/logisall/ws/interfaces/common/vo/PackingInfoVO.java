package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PackingInfoVO implements Serializable {

	private static final long serialVersionUID = -5209687646905390447L;
	
	private String packageNo;
	private String packageLevel;

	private String rtiCd;
	private String rtiBarcode;
	private String qty;
	private String realQty;
	private String individualGrossType;
	private String fromLocLocalCd;
	private String toLocLocalCd;
	private String fromLocGlobalCd;
	private String toLocGlobalCd;
	private String totalWgtMax;
	private String totalWgtMin;
	private String realTotalWgt;
	private String alertReasonCd;

	private CargoInfoVO cargoInfo;
	private List<RtiSerialInfoVO> rtiSerialList;

	public String getPackageNo() {
		return packageNo;
	}

	public void setPackageNo(String packageNo) {
		this.packageNo = packageNo;
	}

	public String getPackageLevel() {
		return packageLevel;
	}

	public void setPackageLevel(String packageLevel) {
		this.packageLevel = packageLevel;
	}
	
	public String getRtiCd() {
		return rtiCd;
	}

	public void setRtiCd(String rtiCd) {
		this.rtiCd = rtiCd;
	}

	public String getRtiBarcode() {
		return rtiBarcode;
	}

	public void setRtiBarcode(String rtiBarcode) {
		this.rtiBarcode = rtiBarcode;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getRealQty() {
		return realQty;
	}

	public void setRealQty(String realQty) {
		this.realQty = realQty;
	}

	public String getIndividualGrossType() {
		return individualGrossType;
	}

	public void setIndividualGrossType(String individualGrossType) {
		this.individualGrossType = individualGrossType;
	}

	public String getFromLocLocalCd() {
		return fromLocLocalCd;
	}

	public void setFromLocLocalCd(String fromLocLocalCd) {
		this.fromLocLocalCd = fromLocLocalCd;
	}

	public String getToLocLocalCd() {
		return toLocLocalCd;
	}

	public void setToLocLocalCd(String toLocLocalCd) {
		this.toLocLocalCd = toLocLocalCd;
	}

	public String getFromLocGlobalCd() {
		return fromLocGlobalCd;
	}

	public void setFromLocGlobalCd(String fromLocGlobalCd) {
		this.fromLocGlobalCd = fromLocGlobalCd;
	}

	public String getToLocGlobalCd() {
		return toLocGlobalCd;
	}

	public void setToLocGlobalCd(String toLocGlobalCd) {
		this.toLocGlobalCd = toLocGlobalCd;
	}

	public String getTotalWgtMax() {
		return totalWgtMax;
	}

	public void setTotalWgtMax(String totalWgtMax) {
		this.totalWgtMax = totalWgtMax;
	}

	public String getTotalWgtMin() {
		return totalWgtMin;
	}

	public void setTotalWgtMin(String totalWgtMin) {
		this.totalWgtMin = totalWgtMin;
	}

	public String getRealTotalWgt() {
		return realTotalWgt;
	}

	public void setRealTotalWgt(String realTotalWgt) {
		this.realTotalWgt = realTotalWgt;
	}

	public String getAlertReasonCd() {
		return alertReasonCd;
	}

	public void setAlertReasonCd(String alertReasonCd) {
		this.alertReasonCd = alertReasonCd;
	}

	public CargoInfoVO getCargoInfo() {
		return cargoInfo;
	}

	public void setCargoInfo(CargoInfoVO cargoInfo) {
		this.cargoInfo = cargoInfo;
	}

	public List<RtiSerialInfoVO> getRtiSerialList() {
		return rtiSerialList;
	}

	public void setRtiSerialList(List<RtiSerialInfoVO> rtiSerialList) {
		this.rtiSerialList = rtiSerialList;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("PackingInfoVO [packageNo=");
		builder.append(packageNo);
		builder.append(", packageLevel=");
		builder.append(packageLevel);
		builder.append(", rtiCd=");
		builder.append(rtiCd);
		builder.append(", rtiBarcode=");
		builder.append(rtiBarcode);
		builder.append(", qty=");
		builder.append(qty);
		builder.append(", realQty=");
		builder.append(realQty);
		builder.append(", individualGrossType=");
		builder.append(individualGrossType);
		builder.append(", fromLocLocalCd=");
		builder.append(fromLocLocalCd);
		builder.append(", toLocLocalCd=");
		builder.append(toLocLocalCd);
		builder.append(", fromLocGlobalCd=");
		builder.append(fromLocGlobalCd);
		builder.append(", toLocGlobalCd=");
		builder.append(toLocGlobalCd);
		builder.append(", totalWgtMax=");
		builder.append(totalWgtMax);
		builder.append(", totalWgtMin=");
		builder.append(totalWgtMin);
		builder.append(", realTotalWgt=");
		builder.append(realTotalWgt);
		builder.append(", alertReasonCd=");
		builder.append(alertReasonCd);
		builder.append(", cargoInfo=");
		builder.append(cargoInfo);
		builder.append(", rtiSerialList=");
		builder.append(rtiSerialList);
		builder.append("]");
		return builder.toString();
	}


}
