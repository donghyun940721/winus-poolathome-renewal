package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class RtiInfoVO implements Serializable {

	private static final long serialVersionUID = 3664930953694445366L;

	private String fromLocLocalCd;
	private String toLocLocalCd;
	private String fromLocGlobalCd;
	private String toLocGlobalCd;
	private String rtiSerial;
	private Date rfidReadDt;

	public String getFromLocLocalCd() {
		return fromLocLocalCd;
	}

	public void setFromLocLocalCd(String fromLocLocalCd) {
		this.fromLocLocalCd = fromLocLocalCd;
	}

	public String getToLocLocalCd() {
		return toLocLocalCd;
	}

	public void setToLocLocalCd(String toLocLocalCd) {
		this.toLocLocalCd = toLocLocalCd;
	}

	public String getFromLocGlobalCd() {
		return fromLocGlobalCd;
	}

	public void setFromLocGlobalCd(String fromLocGlobalCd) {
		this.fromLocGlobalCd = fromLocGlobalCd;
	}

	public String getToLocGlobalCd() {
		return toLocGlobalCd;
	}

	public void setToLocGlobalCd(String toLocGlobalCd) {
		this.toLocGlobalCd = toLocGlobalCd;
	}

	public String getRtiSerial() {
		return rtiSerial;
	}

	public void setRtiSerial(String rtiSerial) {
		this.rtiSerial = rtiSerial;
	}

	public Date getRfidReadDt() {
		return rfidReadDt;
	}

	public void setRfidReadDt(Date rfidReadDt) {
		this.rfidReadDt = rfidReadDt;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("RtiInfoVO [fromLocLocalCd=");
		builder.append(fromLocLocalCd);
		builder.append(", toLocLocalCd=");
		builder.append(toLocLocalCd);
		builder.append(", fromLocGlobalCd=");
		builder.append(fromLocGlobalCd);
		builder.append(", toLocGlobalCd=");
		builder.append(toLocGlobalCd);
		builder.append(", rtiSerial=");
		builder.append(rtiSerial);
		builder.append(", rfidReadDt=");
		builder.append(rfidReadDt);
		builder.append("]");
		return builder.toString();
	}

}
