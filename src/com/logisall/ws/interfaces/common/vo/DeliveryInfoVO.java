package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DeliveryInfoVO implements Serializable {

	private static final long serialVersionUID = -5981219636093139985L;

	private List<GoodsResultVO> goodsresult;
	private List<DeliveryManUsersVO> deliveryManUsers;
	//private List<OnsiteImagesVO> onsiteImages;

	public List<GoodsResultVO> getGoodsresult() {
		return goodsresult;
	}

	public void setGoodsresult(List<GoodsResultVO> goodsresult) {
		this.goodsresult = goodsresult;
	}

	public List<DeliveryManUsersVO> getDeliveryManUsers() {
		return deliveryManUsers;
	}

	public void setDeliveryManUsers(List<DeliveryManUsersVO> deliveryManUsers) {
		this.deliveryManUsers = deliveryManUsers;
	}

//	public List<OnsiteImagesVO> getOnsiteImages() {
//		return onsiteImages;
//	}
//
//	public void setOnsiteImages(List<OnsiteImagesVO> onsiteImages) {
//		this.onsiteImages = onsiteImages;
//	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DeliveryInfoVO [goodsresult=");
		builder.append(goodsresult);
		builder.append(", deliveryManUsers=");
		builder.append(deliveryManUsers);
//		builder.append(", onsiteImages=");
//		builder.append(onsiteImages);
		builder.append("]");
		return builder.toString();
	}

}
