package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;

public class TextOptionDataVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;

	private String goodsNo;
	private String inputLimit;
	private String mustFl;
	private String addPrice;
	private String optionName;
	private String regDt;
	private String modDt;
	
	public String getGoodsNo() {return goodsNo;}public void setGoodsNo(String goodsNo) {this.goodsNo = goodsNo;}
	public String getInputLimit() {return inputLimit;}public void setInputLimit(String inputLimit) {this.inputLimit = inputLimit;}
	public String getMustFl() {return mustFl;}public void setMustFl(String mustFl) {this.mustFl = mustFl;}
	public String getAddPrice() {return addPrice;}public void setAddPrice(String addPrice) {this.addPrice = addPrice;}
	public String getOptionName() {return optionName;}public void setOptionName(String optionName) {this.optionName = optionName;}
	public String getRegDt() {return regDt;}public void setRegDt(String regDt) {this.regDt = regDt;}
	public String getModDt() {return modDt;}public void setModDt(String modDt) {this.modDt = modDt;}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("TextOptionDataVO [goodsNo=");
		builder.append(goodsNo);
		builder.append(", inputLimit=");
		builder.append(inputLimit);
		builder.append(", goodsNo=");
		builder.append(goodsNo);
		builder.append(", mustFl=");
		builder.append(mustFl);
		builder.append(", addPrice=");
		builder.append(addPrice);
		builder.append(", optionName=");
		builder.append(optionName);
		builder.append(", regDt=");
		builder.append(regDt);
		builder.append(", modDt=");
		builder.append(modDt);
		builder.append("]");
		return builder.toString();
	}
}
