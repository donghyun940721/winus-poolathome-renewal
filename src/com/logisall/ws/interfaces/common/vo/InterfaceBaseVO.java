package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;

public class InterfaceBaseVO  implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6445511060732512635L;
	
	private String loginId;
	private String terminalId;
	private String password;
	
	private List<WorkListBaseVO> workList;

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getTerminalId() {
		return terminalId;
	}

	public void setTerminalId(String terminalId) {
		this.terminalId = terminalId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<WorkListBaseVO> getWorkList() {
		return workList;
	}

	public void setWorkList(List<WorkListBaseVO> workList) {
		this.workList = workList;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("InterfaceBaseVO [loginId=");
		builder.append(loginId);
		builder.append(", terminalId=");
		builder.append(terminalId);
		builder.append(", password=");
		builder.append(password);
		builder.append(", workList=");
		builder.append(workList);
		builder.append("]");
		return builder.toString();
	}


}
