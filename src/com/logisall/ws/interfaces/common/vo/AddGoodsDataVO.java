package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;

public class AddGoodsDataVO implements Serializable {

	private static final long serialVersionUID = -3844727177593095135L;

	private String rtiSerial;
	private Date rfidReadDt;

	public String getRtiSerial() {
		return rtiSerial;
	}

	public void setRtiSerial(String rtiSerial) {
		this.rtiSerial = rtiSerial;
	}

	public Date getRfidReadDt() {
		return rfidReadDt;
	}

	public void setRfidReadDt(Date rfidReadDt) {
		this.rfidReadDt = rfidReadDt;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("RtiSerialInfoVO [rtiSerial=");
		builder.append(rtiSerial);
		builder.append(", rfidReadDt=");
		builder.append(rfidReadDt);
		builder.append("]");
		return builder.toString();
	}
	
}
