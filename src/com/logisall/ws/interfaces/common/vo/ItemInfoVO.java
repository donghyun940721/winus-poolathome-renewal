package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;

public class ItemInfoVO implements Serializable {

	private static final long serialVersionUID = -8148450324486000127L;

	private String itemCd;
	private String customerItemCd;
	private String itfCd;
	private String janCd;
	private String itemNm;
	private String makerNm;
	private String colorNm;
	private String size;
	private String qty;
	private String realQty;
	private String unit;
	private String wgt;
	private String wgtArrowRate;

	private String finallyReceivingExpiryDt;
	private String finallyReceivingMakeDt;
	private String finallyReceivingLotNo;
	private String bestDateNum;
	private String bestDateUnit;
	private String individualGrossType;
	private String totalWgtMax;
	private String totalWgtMin;
	private String realTotalWgt;
	private String itemIdCd;

	private List<LotInfoVO> lotList;

	public String getItemCd() {
		return itemCd;
	}

	public void setItemCd(String itemCd) {
		this.itemCd = itemCd;
	}

	public String getCustomerItemCd() {
		return customerItemCd;
	}

	public void setCustomerItemCd(String customerItemCd) {
		this.customerItemCd = customerItemCd;
	}

	public String getItfCd() {
		return itfCd;
	}

	public void setItfCd(String itfCd) {
		this.itfCd = itfCd;
	}

	public String getJanCd() {
		return janCd;
	}

	public void setJanCd(String janCd) {
		this.janCd = janCd;
	}

	public String getItemNm() {
		return itemNm;
	}

	public void setItemNm(String itemNm) {
		this.itemNm = itemNm;
	}

	public String getMakerNm() {
		return makerNm;
	}

	public void setMakerNm(String makerNm) {
		this.makerNm = makerNm;
	}

	public String getColorNm() {
		return colorNm;
	}

	public void setColorNm(String colorNm) {
		this.colorNm = colorNm;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public String getRealQty() {
		return realQty;
	}

	public void setRealQty(String realQty) {
		this.realQty = realQty;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getWgt() {
		return wgt;
	}

	public void setWgt(String wgt) {
		this.wgt = wgt;
	}

	public String getWgtArrowRate() {
		return wgtArrowRate;
	}

	public void setWgtArrowRate(String wgtArrowRate) {
		this.wgtArrowRate = wgtArrowRate;
	}

	public String getFinallyReceivingExpiryDt() {
		return finallyReceivingExpiryDt;
	}

	public void setFinallyReceivingExpiryDt(String finallyReceivingExpiryDt) {
		this.finallyReceivingExpiryDt = finallyReceivingExpiryDt;
	}

	public String getFinallyReceivingMakeDt() {
		return finallyReceivingMakeDt;
	}

	public void setFinallyReceivingMakeDt(String finallyReceivingMakeDt) {
		this.finallyReceivingMakeDt = finallyReceivingMakeDt;
	}

	public String getFinallyReceivingLotNo() {
		return finallyReceivingLotNo;
	}

	public void setFinallyReceivingLotNo(String finallyReceivingLotNo) {
		this.finallyReceivingLotNo = finallyReceivingLotNo;
	}

	public String getBestDateNum() {
		return bestDateNum;
	}

	public void setBestDateNum(String bestDateNum) {
		this.bestDateNum = bestDateNum;
	}

	public String getBestDateUnit() {
		return bestDateUnit;
	}

	public void setBestDateUnit(String bestDateUnit) {
		this.bestDateUnit = bestDateUnit;
	}

	public String getIndividualGrossType() {
		return individualGrossType;
	}

	public void setIndividualGrossType(String individualGrossType) {
		this.individualGrossType = individualGrossType;
	}

	public String getTotalWgtMax() {
		return totalWgtMax;
	}

	public void setTotalWgtMax(String totalWgtMax) {
		this.totalWgtMax = totalWgtMax;
	}

	public String getTotalWgtMin() {
		return totalWgtMin;
	}

	public void setTotalWgtMin(String totalWgtMin) {
		this.totalWgtMin = totalWgtMin;
	}

	public String getRealTotalWgt() {
		return realTotalWgt;
	}

	public void setRealTotalWgt(String realTotalWgt) {
		this.realTotalWgt = realTotalWgt;
	}

	public String getItemIdCd() {
		return itemIdCd;
	}

	public void setItemIdCd(String itemIdCd) {
		this.itemIdCd = itemIdCd;
	}

	public List<LotInfoVO> getLotList() {
		return lotList;
	}

	public void setLotList(List<LotInfoVO> lotList) {
		this.lotList = lotList;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("ItemInfoVO [itemCd=");
		builder.append(itemCd);
		builder.append(", customerItemCd=");
		builder.append(customerItemCd);
		builder.append(", itfCd=");
		builder.append(itfCd);
		builder.append(", janCd=");
		builder.append(janCd);
		builder.append(", itemNm=");
		builder.append(itemNm);
		builder.append(", makerNm=");
		builder.append(makerNm);
		builder.append(", colorNm=");
		builder.append(colorNm);
		builder.append(", size=");
		builder.append(size);
		builder.append(", qty=");
		builder.append(qty);
		builder.append(", realQty=");
		builder.append(realQty);
		builder.append(", unit=");
		builder.append(unit);
		builder.append(", wgt=");
		builder.append(wgt);
		builder.append(", wgtArrowRate=");
		builder.append(wgtArrowRate);
		builder.append(", finallyReceivingExpiryDt=");
		builder.append(finallyReceivingExpiryDt);
		builder.append(", finallyReceivingMakeDt=");
		builder.append(finallyReceivingMakeDt);
		builder.append(", finallyReceivingLotNo=");
		builder.append(finallyReceivingLotNo);
		builder.append(", bestDateNum=");
		builder.append(bestDateNum);
		builder.append(", bestDateUnit=");
		builder.append(bestDateUnit);
		builder.append(", individualGrossType=");
		builder.append(individualGrossType);
		builder.append(", totalWgtMax=");
		builder.append(totalWgtMax);
		builder.append(", totalWgtMin=");
		builder.append(totalWgtMin);
		builder.append(", realTotalWgt=");
		builder.append(realTotalWgt);
		builder.append(", itemIdCd=");
		builder.append(itemIdCd);
		builder.append(", lotList=");
		builder.append(lotList);
		builder.append("]");
		return builder.toString();
	}


}
