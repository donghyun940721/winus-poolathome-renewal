package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class CargoInfoVO implements Serializable {

	private static final long serialVersionUID = -5981219636093139985L;

	private List<RtiInfoVO> rtiList;
	private List<ItemInfoVO> itemList;

	public List<RtiInfoVO> getRtiList() {
		return rtiList;
	}

	public void setRtiList(List<RtiInfoVO> rtiList) {
		this.rtiList = rtiList;
	}

	public List<ItemInfoVO> getItemList() {
		return itemList;
	}

	public void setItemList(List<ItemInfoVO> itemList) {
		this.itemList = itemList;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CargoInfoVO [rtiList=");
		builder.append(rtiList);
		builder.append(", itemList=");
		builder.append(itemList);
		builder.append("]");
		return builder.toString();
	}

}
