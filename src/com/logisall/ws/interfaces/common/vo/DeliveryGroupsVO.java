package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DeliveryGroupsVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String deliveryNo;
	private String invoiceNo;
	private String invoiceRegisterYmdt;
	private String deliveryTemplateGroupNo;
	private String deliveryTemplateNo;
	private String deliveryConditionNo;
	private String deliveryAmt;
	private String returnDeliveryAmt;
	private String remoteDeliveryAmt;
	private String adjustedAmt;
	private String deliveryCompanyType;
	private String receiverName;
	private String countryCd;
	private String receiverZipCd;
	private String receiverAddress;
	private String receiverJibunAddress;
	private String receiverDetailAddress;
	private String receiverContact1;
	private String receiverContact2;
	private String deliveryMemo;
	private String deliveryType;
	private String shippingMethodType;
	private String customsIdNumber;
	private String deliveryAmtInAdvanceYn;
	private String deliveryYn;
	private String combineDeliveryYn;
	private String divideDeliveryYn;
	private String deliveryCombinationYn;
	private String originalDeliveryNo;

	private List<OrderProductsVO> orderProducts;
	
	public String getDeliveryNo() {
		return deliveryNo;
	} 
	public void setDeliveryNo(String deliveryNo) {
		this.deliveryNo = deliveryNo;
	}
	
	public String getInvoiceNo() {
		return invoiceNo;
	} 
	public void setInvoiceNo(String invoiceNo) {
			this.invoiceNo = invoiceNo;
	}
	
	public String getInvoiceRegisterYmdt() {
		return invoiceRegisterYmdt;
	} 
	public void setInvoiceRegisterYmdt(String invoiceRegisterYmdt) {
		this.invoiceRegisterYmdt = invoiceRegisterYmdt;
	}
	
	public String getDeliveryTemplateGroupNo() {
		return deliveryTemplateGroupNo;
	} 
	public void setDeliveryTemplateGroupNo(String deliveryTemplateGroupNo) {
		this.deliveryTemplateGroupNo = deliveryTemplateGroupNo;
	}
	
	public String getDeliveryTemplateNo() {
		return deliveryTemplateNo;
	} 
	public void setDeliveryTemplateNo(String deliveryTemplateNo) {
		this.deliveryTemplateNo = deliveryTemplateNo;
	}
	
	public String getDeliveryConditionNo() {
		return deliveryConditionNo;
	} 
	public void setDeliveryConditionNo(String deliveryConditionNo) {
		this.deliveryConditionNo = deliveryConditionNo;
	}
	
	public String getDeliveryAmt() {
		return deliveryAmt;
	} 
	public void setDeliveryAmt(String deliveryAmt) {
		this.deliveryAmt = deliveryAmt;
	}
	
	public String getReturnDeliveryAmt() {
		return returnDeliveryAmt;
	} 
	public void setReturnDeliveryAmt(String returnDeliveryAmt) {
		this.returnDeliveryAmt = returnDeliveryAmt;
	}
	
	public String getRemoteDeliveryAmt() {
		return remoteDeliveryAmt;
	} 
	public void setRemoteDeliveryAmt(String remoteDeliveryAmt) {
		this.remoteDeliveryAmt = remoteDeliveryAmt;
	}
	
	public String getAdjustedAmt() {
		return adjustedAmt;
	} 
	public void setAdjustedAmt(String adjustedAmt) {
		this.adjustedAmt = adjustedAmt;
	}
	
	public String getDeliveryCompanyType() {
		return deliveryCompanyType;
	} 
	public void setDeliveryCompanyType(String deliveryCompanyType) {
		this.deliveryCompanyType = deliveryCompanyType;
	}
	
	public String getReceiverName() {
		return receiverName;
	} 
	public void setReceiverName(String receiverName) {
		this.receiverName = receiverName;
	}
	
	public String getCountryCd() {
		return countryCd;
	} 
	public void setCountryCd(String countryCd) {
		this.countryCd = countryCd;
	}
	
	public String getReceiverZipCd() {
		return receiverZipCd;
	} 
	public void setReceiverZipCd(String receiverZipCd) {
		this.receiverZipCd = receiverZipCd;
	}
	
	public String getReceiverAddress() {
		return receiverAddress;
	} 
	public void setReceiverAddress(String receiverAddress) {
		this.receiverAddress = receiverAddress;
	}
	
	public String getReceiverJibunAddress() {
		return receiverJibunAddress;
	} 
	public void setReceiverJibunAddress(String receiverJibunAddress) {
		this.receiverJibunAddress = receiverJibunAddress;
	}
	
	public String getReceiverDetailAddress() {
		return receiverDetailAddress;
	} 
	public void setReceiverDetailAddress(String receiverDetailAddress) {
		this.receiverDetailAddress = receiverDetailAddress;
	}
	
	public String getReceiverContact1() {
		return receiverContact1;
	} 
	public void setReceiverContact1(String receiverContact1) {
		this.receiverContact1 = receiverContact1;
	}
	
	public String getReceiverContact2() {
		return receiverContact2;
	} 
	public void setReceiverContact2(String receiverContact2) {
		this.receiverContact2 = receiverContact2;
	}
	
	public String getDeliveryMemo() {
		return deliveryMemo;
	} 
	public void setDeliveryMemo(String deliveryMemo) {
		this.deliveryMemo = deliveryMemo;
	}
	
	public String getDeliveryType() {
		return deliveryType;
	} 
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	
	public String getShippingMethodType() {
		return shippingMethodType;
	} 
	public void setShippingMethodType(String shippingMethodType) {
		this.shippingMethodType = shippingMethodType;
	}
	
	public String getCustomsIdNumber() {
		return customsIdNumber;
	} 
	public void setCustomsIdNumber(String customsIdNumber) {
		this.customsIdNumber = customsIdNumber;
	}
	
	public String getDeliveryAmtInAdvanceYn() {
		return deliveryAmtInAdvanceYn;
	} 
	public void setDeliveryAmtInAdvanceYn(String deliveryAmtInAdvanceYn) {
		this.deliveryAmtInAdvanceYn = deliveryAmtInAdvanceYn;
	}
	
	public String getDeliveryYn() {
		return deliveryYn;
	} 
	public void setDeliveryYn(String deliveryYn) {
		this.deliveryYn = deliveryYn;
	}
	
	public String getCombineDeliveryYn() {
		return combineDeliveryYn;
	} 
	public void setCombineDeliveryYn(String combineDeliveryYn) {
		this.combineDeliveryYn = combineDeliveryYn;
	}
	
	public String getDivideDeliveryYn() {
		return divideDeliveryYn;
	} 
	public void setDivideDeliveryYn(String divideDeliveryYn) {
		this.divideDeliveryYn = divideDeliveryYn;
	}
	
	public String getDeliveryCombinationYn() {
		return deliveryCombinationYn;
	} 
	public void setDeliveryCombinationYn(String deliveryCombinationYn) {
		this.deliveryCombinationYn = deliveryCombinationYn;
	}
	
	public String getOriginalDeliveryNo() {
		return originalDeliveryNo;
	} 
	public void setOriginalDeliveryNo(String originalDeliveryNo) {
		this.originalDeliveryNo = originalDeliveryNo;
	}

	public List<OrderProductsVO> getOrderProducts() {
		return orderProducts;
	}

	public void setOrderProducts(List<OrderProductsVO> orderProducts) {
		this.orderProducts = orderProducts;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("DeliveryGroupsVO [deliveryNo=");
		builder.append(deliveryNo);
		builder.append(", invoiceNo=");
		builder.append(invoiceNo);
		builder.append(", invoiceRegisterYmdt=");
		builder.append(invoiceRegisterYmdt);
		builder.append(", deliveryTemplateGroupNo=");
		builder.append(deliveryTemplateGroupNo);
		builder.append(", deliveryTemplateNo=");
		builder.append(deliveryTemplateNo);
		builder.append(", deliveryConditionNo=");
		builder.append(deliveryConditionNo);
		builder.append(", deliveryAmt=");
		builder.append(deliveryAmt);
		builder.append(", returnDeliveryAmt=");
		builder.append(returnDeliveryAmt);
		builder.append(", remoteDeliveryAmt=");
		builder.append(remoteDeliveryAmt);
		builder.append(", adjustedAmt=");
		builder.append(adjustedAmt);
		builder.append(", deliveryCompanyType=");
		builder.append(deliveryCompanyType);
		builder.append(", receiverName=");
		builder.append(receiverName);
		builder.append(", countryCd=");
		builder.append(countryCd);
		builder.append(", receiverZipCd=");
		builder.append(receiverZipCd);
		builder.append(", receiverAddress=");
		builder.append(receiverAddress);
		builder.append(", receiverJibunAddress=");
		builder.append(receiverJibunAddress);
		builder.append(", receiverDetailAddress=");
		builder.append(receiverDetailAddress);
		builder.append(", receiverContact1=");
		builder.append(receiverContact1);
		builder.append(", receiverContact2=");
		builder.append(receiverContact2);
		builder.append(", deliveryMemo=");
		builder.append(deliveryMemo);
		builder.append(", deliveryType=");
		builder.append(deliveryType);
		builder.append(", shippingMethodType=");
		builder.append(shippingMethodType);
		builder.append(", customsIdNumber=");
		builder.append(customsIdNumber);
		builder.append(", deliveryAmtInAdvanceYn=");
		builder.append(deliveryAmtInAdvanceYn);
		builder.append(", deliveryYn=");
		builder.append(deliveryYn);
		builder.append(", combineDeliveryYn=");
		builder.append(combineDeliveryYn);
		builder.append(", divideDeliveryYn=");
		builder.append(divideDeliveryYn);
		builder.append(", deliveryCombinationYn=");
		builder.append(deliveryCombinationYn);
		builder.append(", originalDeliveryNo=");
		builder.append(originalDeliveryNo);
		builder.append(", orderProducts=");
		builder.append(orderProducts);
		builder.append("]");
		return builder.toString();
	}

}