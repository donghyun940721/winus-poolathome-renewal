package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class OrderProductOptionsVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String orderProductOptionNo;
	private String deliveryTemplateNo;
	private String partnerNo;
	private String deliveryPartnerNo;
	private String categoryNo;
	private String brandNo;
	private String mallOptionNo;
	private String mallAdditionalProductNo;
	private String stockNo;
	private String releaseWarehouseNo;
	private String optionUseYn;
	private String optionName;
	private String optionValue;
	private String optionManagementCd;
	private String imageUrl;
	private String hsCode;
	private String orderCnt;
	private String originalOrderCnt;
	private String salePrice;
	private String immediateDiscountAmt;
	private String addPrice;
	private String additionalDiscountAmt;
	private String partnerChargeAmt;
	private String adjustedAmt;
	private String commissionRate;
	private String productType;
	private String orderOptionType;
	private String orderStatusType;
	private String claimStatusType;
	private String refundableYn;
	private String shippingAreaType;
	private String deliveryInternationalYn;
	private String holdYn;
	private String exchangeYn;
	private String userInputText;
	private String statusChangeYmdt;
	private String orderYmdt;
	private String payYmdt;
	private String orderAcceptYmdt;
	private String releaseReadyYmdt;
	private String releaseYmdt;
	private String deliveryCompleteYmdt;
	private String buyConfirmYmdt;
	private String registerYmdt;
	private String deliveryYn;
	private String updateYmdt;
	private String originOrderProductOptionNo;
	private String extraJson;
	private String sku;
	
	private List<SetOptionsVO> setOptions;
	
	public String getOrderProductOptionNo() {
		return orderProductOptionNo;
	} 
	public void setOrderProductOptionNo(String orderProductOptionNo) {
		this.orderProductOptionNo = orderProductOptionNo;
	}
	
	public String getDeliveryTemplateNo() {
		return deliveryTemplateNo;
	} 
	public void setDeliveryTemplateNo(String deliveryTemplateNo) {
		this.deliveryTemplateNo = deliveryTemplateNo;
	}
	
	public String getPartnerNo() {
		return partnerNo;
	} 
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	
	public String getDeliveryPartnerNo() {
		return deliveryPartnerNo;
	} 
	public void setDeliveryPartnerNo(String deliveryPartnerNo) {
		this.deliveryPartnerNo = deliveryPartnerNo;
	}
	
	public String getCategoryNo() {
		return categoryNo;
	} 
	public void setCategoryNo(String categoryNo) {
		this.categoryNo = categoryNo;
	}
	
	public String getBrandNo() {
		return brandNo;
	} 
	public void setBrandNo(String brandNo) {
		this.brandNo = brandNo;
	}
	
	public String getMallOptionNo() {
		return mallOptionNo;
	} 
	public void setMallOptionNo(String mallOptionNo) {
		this.mallOptionNo = mallOptionNo;
	}
	
	public String getMallAdditionalProductNo() {
		return mallAdditionalProductNo;
	} 
	public void setMallAdditionalProductNo(String mallAdditionalProductNo) {
		this.mallAdditionalProductNo = mallAdditionalProductNo;
	}
	
	public String getStockNo() {
		return stockNo;
	} 
	public void setStockNo(String stockNo) {
		this.stockNo = stockNo;
	}
	
	public String getReleaseWarehouseNo() {
		return releaseWarehouseNo;
	} 
	public void setReleaseWarehouseNo(String releaseWarehouseNo) {
		this.releaseWarehouseNo = releaseWarehouseNo;
	}
	
	public String getOptionUseYn() {
		return optionUseYn;
	} 
	public void setOptionUseYn(String optionUseYn) {
		this.optionUseYn = optionUseYn;
	}
	
	public String getOptionName() {
		return optionName;
	} 
	public void setOptionName(String optionName) {
		this.optionName = optionName;
	}
	
	public String getOptionValue() {
		return optionValue;
	} 
	public void setOptionValue(String optionValue) {
		this.optionValue = optionValue;
	}
	
	public String getOptionManagementCd() {
		return optionManagementCd;
	} 
	public void setOptionManagementCd(String optionManagementCd) {
		this.optionManagementCd = optionManagementCd;
	}
	
	public String getImageUrl() {
		return imageUrl;
	} 
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	
	public String getHsCode() {
		return hsCode;
	} 
	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}
	
	public String getOrderCnt() {
		return orderCnt;
	} 
	public void setOrderCnt(String orderCnt) {
		this.orderCnt = orderCnt;
	}
	
	public String getOriginalOrderCnt() {
		return originalOrderCnt;
	} 
	public void setOriginalOrderCnt(String originalOrderCnt) {
		this.originalOrderCnt = originalOrderCnt;
	}
	
	public String getSalePrice() {
		return salePrice;
	} 
	public void setSalePrice(String salePrice) {
		this.salePrice = salePrice;
	}
	
	public String getImmediateDiscountAmt() {
		return immediateDiscountAmt;
	} 
	public void setImmediateDiscountAmt(String immediateDiscountAmt) {
		this.immediateDiscountAmt = immediateDiscountAmt;
	}
	
	public String getAddPrice() {
		return addPrice;
	} 
	public void setAddPrice(String addPrice) {
		this.addPrice = addPrice;
	}
	
	public String getAdditionalDiscountAmt() {
		return additionalDiscountAmt;
	} 
	public void setAdditionalDiscountAmt(String additionalDiscountAmt) {
		this.additionalDiscountAmt = additionalDiscountAmt;
	}
	
	public String getPartnerChargeAmt() {
		return partnerChargeAmt;
	} 
	public void setPartnerChargeAmt(String partnerChargeAmt) {
		this.partnerChargeAmt = partnerChargeAmt;
	}
	
	public String getAdjustedAmt() {
		return adjustedAmt;
	} 
	public void setAdjustedAmt(String adjustedAmt) {
		this.adjustedAmt = adjustedAmt;
	}
	
	public String getCommissionRate() {
		return commissionRate;
	} 
	public void setCommissionRate(String commissionRate) {
		this.commissionRate = commissionRate;
	}
	
	public String getProductType() {
		return productType;
	} 
	public void setProductType(String productType) {
		this.productType = productType;
	}
	
	public String getOrderOptionType() {
		return orderOptionType;
	} 
	public void setOrderOptionType(String orderOptionType) {
		this.orderOptionType = orderOptionType;
	}
	
	public String getOrderStatusType() {
		return orderStatusType;
	} 
	public void setOrderStatusType(String orderStatusType) {
		this.orderStatusType = orderStatusType;
	}
	
	public String getClaimStatusType() {
		return claimStatusType;
	} 
	public void setClaimStatusType(String claimStatusType) {
		this.claimStatusType = claimStatusType;
	}
	
	public String getRefundableYn() {
		return refundableYn;
	} 
	public void setRefundableYn(String refundableYn) {
		this.refundableYn = refundableYn;
	}
	
	public String getShippingAreaType() {
		return shippingAreaType;
	} 
	public void setShippingAreaType(String shippingAreaType) {
		this.shippingAreaType = shippingAreaType;
	}
	
	public String getDeliveryInternationalYn() {
		return deliveryInternationalYn;
	} 
	public void setDeliveryInternationalYn(String deliveryInternationalYn) {
		this.deliveryInternationalYn = deliveryInternationalYn;
	}
	
	public String getHoldYn() {
		return holdYn;
	} 
	public void setHoldYn(String holdYn) {
		this.holdYn = holdYn;
	}
	
	public String getExchangeYn() {
		return exchangeYn;
	} 
	public void setExchangeYn(String exchangeYn) {
		this.exchangeYn = exchangeYn;
	}
	
	public String getUserInputText() {
		return userInputText;
	} 
	public void setUserInputText(String userInputText) {
		this.userInputText = userInputText;
	}
	
	public String getStatusChangeYmdt() {
		return statusChangeYmdt;
	} 
	public void setStatusChangeYmdt(String statusChangeYmdt) {
		this.statusChangeYmdt = statusChangeYmdt;
	}
	
	public String getOrderYmdt() {
		return orderYmdt;
	} 
	public void setOrderYmdt(String orderYmdt) {
		this.orderYmdt = orderYmdt;
	}
	
	public String getPayYmdt() {
		return payYmdt;
	} 
	public void setPayYmdt(String payYmdt) {
		this.payYmdt = payYmdt;
	}
	
	public String getOrderAcceptYmdt() {
		return orderAcceptYmdt;
	} 
	public void setOrderAcceptYmdt(String orderAcceptYmdt) {
		this.orderAcceptYmdt = orderAcceptYmdt;
	}
	
	public String getReleaseReadyYmdt() {
		return releaseReadyYmdt;
	} 
	public void setReleaseReadyYmdt(String releaseReadyYmdt) {
		this.releaseReadyYmdt = releaseReadyYmdt;
	}
	
	public String getReleaseYmdt() {
		return releaseYmdt;
	} 
	public void setReleaseYmdt(String releaseYmdt) {
		this.releaseYmdt = releaseYmdt;
	}
	
	public String getDeliveryCompleteYmdt() {
		return deliveryCompleteYmdt;
	} 
	public void setDeliveryCompleteYmdt(String deliveryCompleteYmdt) {
		this.deliveryCompleteYmdt = deliveryCompleteYmdt;
	}
	
	public String getBuyConfirmYmdt() {
		return buyConfirmYmdt;
	} 
	public void setBuyConfirmYmdt(String buyConfirmYmdt) {
		this.buyConfirmYmdt = buyConfirmYmdt;
	}
	
	public String getRegisterYmdt() {
		return registerYmdt;
	} 
	public void setRegisterYmdt(String registerYmdt) {
		this.registerYmdt = registerYmdt;
	}
	
	public String getDeliveryYn() {
		return deliveryYn;
	} 
	public void setDeliveryYn(String deliveryYn) {
		this.deliveryYn = deliveryYn;
	}
	
	public String getUpdateYmdt() {
		return updateYmdt;
	} 
	public void setUpdateYmdt(String updateYmdt) {
		this.updateYmdt = updateYmdt;
	}
	
	public String getOriginOrderProductOptionNo() {
		return originOrderProductOptionNo;
	} 
	public void setOriginOrderProductOptionNo(String originOrderProductOptionNo) {
		this.originOrderProductOptionNo = originOrderProductOptionNo;
	}
	
	public String getExtraJson() {
		return extraJson;
	} 
	public void setExtraJson(String extraJson) {
		this.extraJson = extraJson;
	}
	
	public String getSku() {
		return sku;
	} 
	public void setSku(String sku) {
		this.sku = sku;
	}
	
	public List<SetOptionsVO> getSetOptions() {
		return setOptions;
	}

	public void setSetOptions(List<SetOptionsVO> setOptions) {
		this.setOptions = setOptions;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("OrderProductsOptionVO [orderProductOptionNo=");
		builder.append(orderProductOptionNo);
		builder.append(", deliveryTemplateNo=");
		builder.append(deliveryTemplateNo);
		builder.append(", partnerNo=");
		builder.append(partnerNo);
		builder.append(", deliveryPartnerNo=");
		builder.append(deliveryPartnerNo);
		builder.append(", categoryNo=");
		builder.append(categoryNo);
		builder.append(", brandNo=");
		builder.append(brandNo);
		builder.append(", mallOptionNo=");
		builder.append(mallOptionNo);
		builder.append(", mallAdditionalProductNo=");
		builder.append(mallAdditionalProductNo);
		builder.append(", stockNo=");
		builder.append(stockNo);
		builder.append(", releaseWarehouseNo=");
		builder.append(releaseWarehouseNo);
		builder.append(", optionUseYn=");
		builder.append(optionUseYn);
		builder.append(", optionName=");
		builder.append(optionName);
		builder.append(", optionValue=");
		builder.append(optionValue);
		builder.append(", optionManagementCd=");
		builder.append(optionManagementCd);
		builder.append(", imageUrl=");
		builder.append(imageUrl);
		builder.append(", hsCode=");
		builder.append(hsCode);
		builder.append(", orderCnt=");
		builder.append(orderCnt);
		builder.append(", originalOrderCnt=");
		builder.append(originalOrderCnt);
		builder.append(", salePrice=");
		builder.append(salePrice);
		builder.append(", immediateDiscountAmt=");
		builder.append(immediateDiscountAmt);
		builder.append(", addPrice=");
		builder.append(addPrice);
		builder.append(", additionalDiscountAmt=");
		builder.append(additionalDiscountAmt);
		builder.append(", partnerChargeAmt=");
		builder.append(partnerChargeAmt);
		builder.append(", adjustedAmt=");
		builder.append(adjustedAmt);
		builder.append(", commissionRate=");
		builder.append(commissionRate);
		builder.append(", productType=");
		builder.append(productType);
		builder.append(", orderOptionType=");
		builder.append(orderOptionType);
		builder.append(", orderStatusType=");
		builder.append(orderStatusType);
		builder.append(", claimStatusType=");
		builder.append(claimStatusType);
		builder.append(", refundableYn=");
		builder.append(refundableYn);
		builder.append(", shippingAreaType=");
		builder.append(shippingAreaType);
		builder.append(", deliveryInternationalYn=");
		builder.append(deliveryInternationalYn);
		builder.append(", holdYn=");
		builder.append(holdYn);
		builder.append(", exchangeYn=");
		builder.append(exchangeYn);
		builder.append(", userInputText=");
		builder.append(userInputText);
		builder.append(", statusChangeYmdt=");
		builder.append(statusChangeYmdt);
		builder.append(", orderYmdt=");
		builder.append(orderYmdt);
		builder.append(", payYmdt=");
		builder.append(payYmdt);
		builder.append(", orderAcceptYmdt=");
		builder.append(orderAcceptYmdt);
		builder.append(", releaseReadyYmdt=");
		builder.append(releaseReadyYmdt);
		builder.append(", releaseYmdt=");
		builder.append(releaseYmdt);
		builder.append(", deliveryCompleteYmdt=");
		builder.append(deliveryCompleteYmdt);
		builder.append(", buyConfirmYmdt=");
		builder.append(buyConfirmYmdt);
		builder.append(", registerYmdt=");
		builder.append(registerYmdt);
		builder.append(", deliveryYn=");
		builder.append(deliveryYn);
		builder.append(", updateYmdt=");
		builder.append(updateYmdt);
		builder.append(", originOrderProductOptionNo=");
		builder.append(originOrderProductOptionNo);
		builder.append(", extraJson=");
		builder.append(extraJson);
		builder.append(", sku=");
		builder.append(sku);
		builder.append(", setOptions=");
		builder.append(setOptions);
		builder.append("]");
		return builder.toString();
	}

}