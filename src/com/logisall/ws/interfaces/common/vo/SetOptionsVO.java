package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SetOptionsVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String count;
	private String mallOptionNo;
	private String mallProductNo;
	private String option;
	private String optionManagementCd;
	private String optionPrice;
	private String productManagementCd;
	private String productName;
	private String sku;
	
	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public String getMallProductNo() {
		return mallProductNo;
	}

	public void setMallProductNo(String mallProductNo) {
		this.mallProductNo = mallProductNo;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getOptionManagementCd() {
		return optionManagementCd;
	}

	public void setOptionManagementCd(String optionManagementCd) {
		this.optionManagementCd = optionManagementCd;
	}

	public String getOptionPrice() {
		return optionPrice;
	}

	public void setOptionPrice(String optionPrice) {
		this.optionPrice = optionPrice;
	}

	public String getProductManagementCd() {
		return productManagementCd;
	}

	public void setProductManagementCd(String productManagementCd) {
		this.productManagementCd = productManagementCd;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("SetOptionsVO [count=");
		builder.append(count);
		builder.append(", mallOptionNo=");
		builder.append(mallOptionNo);
		builder.append(", mallProductNo=");
		builder.append(mallProductNo);
		builder.append(", option=");
		builder.append(option);
		builder.append(", optionManagementCd=");
		builder.append(optionManagementCd);
		builder.append(", optionPrice=");
		builder.append(optionPrice);
		builder.append(", productManagementCd=");
		builder.append(productManagementCd);
		builder.append(", productName=");
		builder.append(productName);
		builder.append(", sku=");
		builder.append(sku);
		builder.append("]");
		return builder.toString();
	}

}