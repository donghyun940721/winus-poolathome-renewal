package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;

public class GoodsResultVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;
	
	private String goodsSrl;
	private String amount;
	private String originCode;
	private String resultType;
	private String causeType;
	private String causeDescr;
	private String rentalSrl;
	private String rentalSrlImg;

	public String getGoodsSrl() {
		return goodsSrl;
	}

	public void setGoodsSrl(String goodsSrl) {
		this.goodsSrl = goodsSrl;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOriginCode() {
		return originCode;
	}

	public void setOriginCode(String originCode) {
		this.originCode = originCode;
	}
	
	public String getResultType() {
		return resultType;
	}
	
	public void setResultType(String resultType) {
		this.resultType = resultType;
	}
	
	public String getCauseType() {
		return causeType;
	}
	
	public void setCauseType(String causeType) {
		this.causeType = causeType;
	}
	
	public String getCauseDescr() {
		return causeDescr;
	}
	
	public void setCauseDescr(String causeDescr) {
		this.causeDescr = causeDescr;
	}
	
	public String getRentalSrl() {
		return rentalSrl;
	}
	
	public void setRentalSrl(String rentalSrl) {
		this.rentalSrl = rentalSrl;
	}

	public String getRentalSrlImg() {
		return rentalSrlImg;
	}

	public void setRentalSrlImg(String rentalSrlImg) {
		this.rentalSrlImg = rentalSrlImg;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("GoodsResultVO [goodsSrl=");
		builder.append(goodsSrl);
		builder.append(", amount=");
		builder.append(amount);
		builder.append(", originCode=");
		builder.append(originCode);
		builder.append(", resultType=");
		builder.append(resultType);
		builder.append(", causeType=");
		builder.append(causeType);
		builder.append(", causeDescr=");
		builder.append(causeDescr);
		builder.append(", rentalSrl=");
		builder.append(rentalSrl);
		builder.append(", rentalSrlImg=");
		builder.append(rentalSrlImg);
		builder.append("]");
		return builder.toString();
	}
}
