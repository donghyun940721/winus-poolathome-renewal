package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVOShopbuy implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String orderYmdt;
	private String orderNo;
	private String mallNo;
	private String ordererName;
	private String memberId;
	private String oauthIdNo;
	private String ordererContact1;
	private String ordererContact2;
	private String orderMemo;
	private String firstCartCouponDiscountAmt;
	private String firstPayAmt;
	private String firstMainPayAmt;
	private String firstSubPayAmt;
	private String lastCartCouponDiscountAmt;
	private String lastPayAmt;
	private String lastMainPayAmt;
	private String lastSubPayAmt;
	private String extraData;
	private String memberAdditionalInfoJson;

	private List<DeliveryGroupsVO> deliveryGroups;
	
	public String getOrderYmdt() {
		return orderYmdt;
	}

	public void setOrderYmdt(String orderYmdt) {
		this.orderYmdt = orderYmdt;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getMallNo() {
		return mallNo;
	}

	public void setMallNo(String mallNo) {
		this.mallNo = mallNo;
	}

	public String getOrdererName() {
		return ordererName;
	}

	public void setOrdererName(String ordererName) {
		this.ordererName = ordererName;
	}

	public String getMemberId() {
		return memberId;
	}

	public void setMemberId(String memberId) {
		this.memberId = memberId;
	}

	public String getOauthIdNo() {
		return oauthIdNo;
	}

	public void setOauthIdNo(String oauthIdNo) {
		this.oauthIdNo = oauthIdNo;
	}

	public String getOrdererContact1() {
		return ordererContact1;
	}

	public void setOrdererContact1(String ordererContact1) {
		this.ordererContact1 = ordererContact1;
	}

	public String getOrdererContact2() {
		return ordererContact2;
	}

	public void setOrdererContact2(String ordererContact2) {
		this.ordererContact2 = ordererContact2;
	}

	public String getOrderMemo() {
		return orderMemo;
	}

	public void setOrderMemo(String orderMemo) {
		this.orderMemo = orderMemo;
	}
	
	public String getFirstCartCouponDiscountAmt() {
		return firstCartCouponDiscountAmt;
	}

	public void setFirstCartCouponDiscountAmt(String firstCartCouponDiscountAmt) {
		this.firstCartCouponDiscountAmt = firstCartCouponDiscountAmt;
	}
	
	public String getFirstPayAmt() {
		return firstPayAmt;
	}

	public void setFirstPayAmt(String firstPayAmt) {
		this.firstPayAmt = firstPayAmt;
	}
	
	public String getFirstMainPayAmt() {
		return firstMainPayAmt;
	}

	public void setFirstMainPayAmt(String firstMainPayAmt) {
		this.firstMainPayAmt = firstMainPayAmt;
	}

	public String getFirstSubPayAmt() {
		return firstSubPayAmt;
	}

	public void setFirstSubPayAmt(String firstSubPayAmt) {
		this.firstSubPayAmt = firstSubPayAmt;
	}

	public String getLastCartCouponDiscountAmt() {
		return lastCartCouponDiscountAmt;
	}

	public void setLastCartCouponDiscountAmt(String lastCartCouponDiscountAmt) {
		this.lastCartCouponDiscountAmt = lastCartCouponDiscountAmt;
	}

	public String getLastPayAmt() {
		return lastPayAmt;
	}

	public void setLastPayAmt(String lastPayAmt) {
		this.lastPayAmt = lastPayAmt;
	}

	public String getLastMainPayAmt() {
		return lastMainPayAmt;
	}

	public void setLastMainPayAmt(String lastMainPayAmt) {
		this.lastMainPayAmt = lastMainPayAmt;
	}

	public String getLastSubPayAmt() {
		return lastSubPayAmt;
	}

	public void setLastSubPayAmt(String lastSubPayAmt) {
		this.lastSubPayAmt = lastSubPayAmt;
	}

	public String getExtraData() {
		return extraData;
	}

	public void setExtraData(String extraData) {
		this.extraData = extraData;
	}

	public String getMemberAdditionalInfoJson() {
		return memberAdditionalInfoJson;
	}

	public void setMemberAdditionalInfoJson(String memberAdditionalInfoJson) {
		this.memberAdditionalInfoJson = memberAdditionalInfoJson;
	}

	public List<DeliveryGroupsVO> getDeliveryGroups() {
		return deliveryGroups;
	}

	public void setDeliveryGroups(List<DeliveryGroupsVO> deliveryGroups) {
		this.deliveryGroups = deliveryGroups;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOShopbuy [orderYmdt=");
		builder.append(orderYmdt);
		builder.append(", orderNo=");
		builder.append(orderNo);
		builder.append(", mallNo=");
		builder.append(mallNo);
		builder.append(", ordererName=");
		builder.append(ordererName);
		builder.append(", memberId=");
		builder.append(memberId);
		builder.append(", oauthIdNo=");
		builder.append(oauthIdNo);
		builder.append(", ordererContact1=");
		builder.append(ordererContact1);
		builder.append(", ordererContact2=");
		builder.append(ordererContact2);
		builder.append(", orderMemo=");
		builder.append(orderMemo);
		builder.append(", firstCartCouponDiscountAmt=");
		builder.append(firstCartCouponDiscountAmt);
		builder.append(", firstPayAmt=");
		builder.append(firstPayAmt);
		builder.append(", firstMainPayAmt=");
		builder.append(firstMainPayAmt);
		builder.append(", firstSubPayAmt=");
		builder.append(firstSubPayAmt);
		builder.append(", lastCartCouponDiscountAmt=");
		builder.append(lastCartCouponDiscountAmt);
		builder.append(", lastPayAmt=");
		builder.append(lastPayAmt);
		builder.append(", lastMainPayAmt=");
		builder.append(lastMainPayAmt);
		builder.append(", lastSubPayAmt=");
		builder.append(lastSubPayAmt);
		builder.append(", extraData=");
		builder.append(extraData);
		builder.append(", memberAdditionalInfoJson=");
		builder.append(memberAdditionalInfoJson);
		builder.append(", deliveryGroups=");
		builder.append(deliveryGroups);
		builder.append("]");
		return builder.toString();
	}

}