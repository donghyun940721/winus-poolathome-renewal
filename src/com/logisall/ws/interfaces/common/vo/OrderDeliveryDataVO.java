package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;

public class OrderDeliveryDataVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;
	
	private String sno;
	private String scmNo;
	private String commission;
	private String scmAdjustNo;
	private String scmAdjustAfterNo;
	private String deliveryCharge;
	private String deliveryPolicyCharge;
	private String deliveryAreaCharge;
	private String divisionDeliveryUseDeposit;
	private String divisionDeliveryUseMileage;
	private String divisionDeliveryCharge;
	private String divisionMemberDeliveryDcPrice;
	private String deliveryInsuranceFee;
	private String deliveryFixFl;
	private String deliveryWeightInfo;
	private String overseasDeliveryPolicy;
	private String deliveryCollectFl;
	private String deliveryCollectPrice;
	private String deliveryWholeFreePrice;
	private String statisticsOrderFl;
	
	public String getSno() {
		return sno;
	}

	public void setSno(String sno) {
		this.sno = sno;
	}

	public String getScmNo() {
		return scmNo;
	}

	public void setScmNo(String scmNo) {
		this.scmNo = scmNo;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getScmAdjustNo() {
		return scmAdjustNo;
	}

	public void setScmAdjustNo(String scmAdjustNo) {
		this.scmAdjustNo = scmAdjustNo;
	}

	public String getScmAdjustAfterNo() {
		return scmAdjustAfterNo;
	}

	public void setScmAdjustAfterNo(String scmAdjustAfterNo) {
		this.scmAdjustAfterNo = scmAdjustAfterNo;
	}

	public String getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(String deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}

	public String getDeliveryPolicyCharge() {
		return deliveryPolicyCharge;
	}

	public void setDeliveryPolicyCharge(String deliveryPolicyCharge) {
		this.deliveryPolicyCharge = deliveryPolicyCharge;
	}

	public String getDeliveryAreaCharge() {
		return deliveryAreaCharge;
	}

	public void setDeliveryAreaCharge(String deliveryAreaCharge) {
		this.deliveryAreaCharge = deliveryAreaCharge;
	}

	public String getDivisionDeliveryUseDeposit() {
		return divisionDeliveryUseDeposit;
	}

	public void setDivisionDeliveryUseDeposit(String divisionDeliveryUseDeposit) {
		this.divisionDeliveryUseDeposit = divisionDeliveryUseDeposit;
	}

	public String getDivisionDeliveryUseMileage() {
		return divisionDeliveryUseMileage;
	}

	public void setDivisionDeliveryUseMileage(String divisionDeliveryUseMileage) {
		this.divisionDeliveryUseMileage = divisionDeliveryUseMileage;
	}

	public String getDivisionDeliveryCharge() {
		return divisionDeliveryCharge;
	}

	public void setDivisionDeliveryCharge(String divisionDeliveryCharge) {
		this.divisionDeliveryCharge = divisionDeliveryCharge;
	}

	public String getDivisionMemberDeliveryDcPrice() {
		return divisionMemberDeliveryDcPrice;
	}

	public void setDivisionMemberDeliveryDcPrice(String divisionMemberDeliveryDcPrice) {
		this.divisionMemberDeliveryDcPrice = divisionMemberDeliveryDcPrice;
	}

	public String getDeliveryInsuranceFee() {
		return deliveryInsuranceFee;
	}

	public void setDeliveryInsuranceFee(String deliveryInsuranceFee) {
		this.deliveryInsuranceFee = deliveryInsuranceFee;
	}

	public String getDeliveryFixFl() {
		return deliveryFixFl;
	}

	public void setDeliveryFixFl(String deliveryFixFl) {
		this.deliveryFixFl = deliveryFixFl;
	}

	public String getDeliveryWeightInfo() {
		return deliveryWeightInfo;
	}

	public void setDeliveryWeightInfo(String deliveryWeightInfo) {
		this.deliveryWeightInfo = deliveryWeightInfo;
	}

	public String getOverseasDeliveryPolicy() {
		return overseasDeliveryPolicy;
	}

	public void setOverseasDeliveryPolicy(String overseasDeliveryPolicy) {
		this.overseasDeliveryPolicy = overseasDeliveryPolicy;
	}

	public String getDeliveryCollectFl() {
		return deliveryCollectFl;
	}

	public void setDeliveryCollectFl(String deliveryCollectFl) {
		this.deliveryCollectFl = deliveryCollectFl;
	}

	public String getDeliveryCollectPrice() {
		return deliveryCollectPrice;
	}

	public void setDeliveryCollectPrice(String deliveryCollectPrice) {
		this.deliveryCollectPrice = deliveryCollectPrice;
	}

	public String getDeliveryWholeFreePrice() {
		return deliveryWholeFreePrice;
	}

	public void setDeliveryWholeFreePrice(String deliveryWholeFreePrice) {
		this.deliveryWholeFreePrice = deliveryWholeFreePrice;
	}

	public String getStatisticsOrderFl() {
		return statisticsOrderFl;
	}

	public void setStatisticsOrderFl(String statisticsOrderFl) {
		this.statisticsOrderFl = statisticsOrderFl;
	}

	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("OrderDeliveryDataVO [sno=");
		builder.append(sno);
		builder.append(", scmNo=");
		builder.append(scmNo);
		builder.append(", commission=");
		builder.append(commission);
		builder.append(", scmAdjustNo=");
		builder.append(scmAdjustNo);
		builder.append(", scmAdjustAfterNo=");
		builder.append(scmAdjustAfterNo);
		builder.append(", deliveryCharge=");
		builder.append(deliveryCharge);
		builder.append(", deliveryPolicyCharge=");
		builder.append(deliveryPolicyCharge);
		builder.append(", deliveryAreaCharge=");
		builder.append(deliveryAreaCharge);
		builder.append(", divisionDeliveryUseDeposit=");
		builder.append(divisionDeliveryUseDeposit);
		builder.append(", divisionDeliveryUseMileage=");
		builder.append(divisionDeliveryUseMileage);
		builder.append(", divisionDeliveryCharge=");
		builder.append(divisionDeliveryCharge);
		builder.append(", divisionMemberDeliveryDcPrice=");
		builder.append(divisionMemberDeliveryDcPrice);
		builder.append(", deliveryInsuranceFee=");
		builder.append(deliveryInsuranceFee);
		builder.append(", deliveryFixFl=");
		builder.append(deliveryFixFl);
		builder.append(", deliveryWeightInfo=");
		builder.append(deliveryWeightInfo);
		builder.append(", overseasDeliveryPolicy=");
		builder.append(overseasDeliveryPolicy);
		builder.append(", deliveryCollectFl=");
		builder.append(deliveryCollectFl);
		builder.append(", deliveryCollectPrice=");
		builder.append(deliveryCollectPrice);
		builder.append(", deliveryWholeFreePrice=");
		builder.append(deliveryWholeFreePrice);
		builder.append(", statisticsOrderFl=");
		builder.append(statisticsOrderFl);
		builder.append("]");
		return builder.toString();
	}
}
