package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class InterfaceBaseVOShopbuy implements Serializable {

	private static final long serialVersionUID = -6445511060732512635L;

	private List<WorkListBaseVOShopbuy> orderData;

	public List<WorkListBaseVOShopbuy> getOrderData() {
		return orderData;
	}

	public void setOrderData(List<WorkListBaseVOShopbuy> orderData) {
		this.orderData = orderData;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOShopbuy [orderData=");
		builder.append(orderData);
		builder.append("]");
		return builder.toString();
	}
	
	

}
