package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.List;

public class GoodsMustInfoDataVO  implements Serializable {
	
	private static final long serialVersionUID = 4075496818610100268L;
	
	private String idx;
	
	private List<StepDataVO> stepData;

	public String getIdx() {
		return idx;
	}

	public void setIdx(String idx) {
		this.idx = idx;
	}
	
	public List<StepDataVO> getStepData() {
		return stepData;
	}

	public void setStepData(List<StepDataVO> stepData) {
		this.stepData = stepData;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("GoodsMustInfoDataVO [idx=");
		builder.append(idx);
		builder.append(", stepData=");
		builder.append(stepData);
		builder.append("]");
		return builder.toString();
	}
}
