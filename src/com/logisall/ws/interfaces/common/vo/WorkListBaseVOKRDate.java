package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVOKRDate implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;

	private String lcId;
	private String transCustId;
	private String ordId;
	private String ordSeq;
	private String workQty;
	private String workWgt;
	private String refSubLotId;	
	private String sapBarcode;
	private String fromLocCd;
	private String toLocCd;
	private String userNo;
	private String workIp;
	private String custLotNo;
	private String ordType;
	private String custId;
	private String inCustId;
	private String ritemId;
	private String itemCode;
	private String workMemo;
	private String itemBestDateEnd;
	private String boxInQty;
	private String boxBarCd;
	private String itemBarCd;
	private String issueDate;
	private String makeDate;
	private String makeLineNo;
	
	private List<RtiSerialInfoVOKRDate> rtiSerialList;

	public String getLcId() {
		return lcId;
	}

	public void setLcId(String lcId) {
		this.lcId = lcId;
	}
	
	public String getTransCustId() {
		return transCustId;
	}

	public void setTransCustId(String transCustId) {
		this.transCustId = transCustId;
	}
	
	public String getOrdId() {
		return ordId;
	}

	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}
	
	public String getOrdSeq() {
		return ordSeq;
	}

	public void setOrdSeq(String ordSeq) {
		this.ordSeq = ordSeq;
	}
	
	public String getWorkQty() {
		return workQty;
	}

	public void setWorkQty(String workQty) {
		this.workQty = workQty;
	}
	
	public String getWorkWgt() {
		return workWgt;
	}

	public void setWorkWgt(String workWgt) {
		this.workWgt = workWgt;
	}
	
	public String getRefSubLotId() {
		return refSubLotId;
	}

	public void setRefSubLotId(String refSubLotId) {
		this.refSubLotId = refSubLotId;
	}
	
	public String getSapBarcode() {
		return sapBarcode;
	}

	public void setSapBarcode(String sapBarcode) {
		this.sapBarcode = sapBarcode;
	}
	
	public String getFromLocCd() {
		return fromLocCd;
	}

	public void setFromLocCd(String fromLocCd) {
		this.fromLocCd = fromLocCd;
	}
	
	public String getToLocCd() {
		return toLocCd;
	}

	public void setToLocCd(String toLocCd) {
		this.toLocCd = toLocCd;
	}	
	
	public String getCustLotNo() {
		return custLotNo;
	}

	public void setCustLotNo(String custLotNo) {
		this.custLotNo = custLotNo;
	}	
	
	public String getUserNo() {
		return userNo;
	}

	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	
	public String getWorkIp() {
		return workIp;
	}

	public void setWorkIp(String workIp) {
		this.workIp = workIp;
	}
	
	public String getOrdType() {
		return ordType;
	}

	public void setOrdType(String ordType) {
		this.ordType = ordType;;
	}
	
	public String getCustId() {
		return custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}
	
	public String getInCustId() {
		return inCustId;
	}

	public void setInCustId(String inCustId) {
		this.inCustId = inCustId;
	}
	
	public String getRitemId() {
		return ritemId;
	}

	public void setRitemId(String ritemId) {
		this.ritemId = ritemId;
	}
	
	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}
	
	public String getWorkMemo() {
		return workMemo;
	}

	public void setWorkMemo(String workMemo) {
		this.workMemo = workMemo;
	}
	
	public String getItemBestDateEnd() {
		return itemBestDateEnd;
	}

	public void setItemBestDateEnd(String itemBestDateEnd) {
		this.itemBestDateEnd = itemBestDateEnd;
	}
	
	public String getBoxInQty() {
		return boxInQty;
	}

	public void setBoxInQty(String boxInQty) {
		this.boxInQty = boxInQty;
	}
	
	public String getBoxBarCd() {
		return boxBarCd;
	}

	public void setBoxBarCd(String boxBarCd) {
		this.boxBarCd = boxBarCd;
	}
	
	public String getItemBarCd() {
		return itemBarCd;
	}

	public void setItemBarCd(String itemBarCd) {
		this.itemBarCd = itemBarCd;
	}
	
	public String getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	
	public String getMakeDate() {
		return makeDate;
	}

	public void setMakeDate(String makeDate) {
		this.makeDate = makeDate;
	}
	
	public String getMakeLineNo() {
		return makeLineNo;
	}

	public void setMakeLineNo(String makeLineNo) {
		this.makeLineNo = makeLineNo;
	}

	public List<RtiSerialInfoVOKRDate> getRtiSerialList() {
		return rtiSerialList;
	}

	public void setRtiSerialList(List<RtiSerialInfoVOKRDate> rtiSerialList) {
		this.rtiSerialList = rtiSerialList;
	}		
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVOKRDate [lcId=");
		builder.append(lcId);
		builder.append(", transCustId=");
		builder.append(transCustId);
		builder.append(", ordId=");
		builder.append(ordId);
		builder.append(", ordSeq=");
		builder.append(ordSeq);
		builder.append(", workQty=");
		builder.append(workQty);
		builder.append(", workWgt=");
		builder.append(workWgt);
		builder.append(", refSubLotId=");
		builder.append(refSubLotId);
		builder.append(", sapBarcode=");
		builder.append(sapBarcode);
		builder.append(", fromLocCd=");
		builder.append(fromLocCd);
		builder.append(", toLocCd=");
		builder.append(toLocCd);
		builder.append(", custLotNo=");
		builder.append(custLotNo);
		builder.append(", userNo=");
		builder.append(userNo);
		builder.append(", workIp=");
		builder.append(workIp);
		builder.append(", ordType=");
		builder.append(ordType);
		builder.append(", custId=");
		builder.append(custId);
		builder.append(", inCustId=");
		builder.append(inCustId);
		builder.append(", ritemId=");
		builder.append(ritemId);
		builder.append(", itemCode=");
		builder.append(itemCode);
		builder.append(", workMemo=");
		builder.append(workMemo);
		builder.append(", itemBestDateEnd=");
		builder.append(itemBestDateEnd);
		builder.append(", boxInQty=");
		builder.append(boxInQty);
		builder.append(", itemBarCd=");
		builder.append(itemBarCd);
		builder.append(", issueDate=");
		builder.append(issueDate);
		builder.append(", makeDate=");
		builder.append(makeDate);
		builder.append(", makeLineNo=");
		builder.append(makeLineNo);
		builder.append(", rtiSerialList=");
		builder.append(rtiSerialList);
		builder.append("]");
		return builder.toString();
	}
}
