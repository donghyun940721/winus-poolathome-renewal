package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVODlvNew implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String userId;
	private String ordId;
	private String phoneNo;
	private String serialNo;
	private String date;
	private String memo;
	private String timeArea;
	private String version;
	private String comCost;
	private String costType;
	private String setType;
	private String dlvOption1;
	private String dlvOption2;
	private String dlvOption3;
	private String dlvOption4;
	private String dlvOption5;
	
	//private List<WorkListBaseVOKR> workList;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrdId() {
		return ordId;
	}

	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	public String getTimeArea() {
		return timeArea;
	}

	public void setTimeArea(String timeArea) {
		this.timeArea = timeArea;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getComCost() {
		return comCost;
	}

	public void setComCost(String comCost) {
		this.comCost = comCost;
	}

	public String getCostType() {
		return costType;
	}

	public void setCostType(String costType) {
		this.costType = costType;
	}

	public String getSetType() {
		return setType;
	}

	public void setSetType(String setType) {
		this.setType = setType;
	}

	public String getDlvOption1() {
		return dlvOption1;
	}

	public void setDlvOption1(String dlvOption1) {
		this.dlvOption1 = dlvOption1;
	}

	public String getDlvOption2() {
		return dlvOption2;
	}

	public void setDlvOption2(String dlvOption2) {
		this.dlvOption2 = dlvOption2;
	}

	public String getDlvOption3() {
		return dlvOption3;
	}

	public void setDlvOption3(String dlvOption3) {
		this.dlvOption3 = dlvOption3;
	}

	public String getDlvOption4() {
		return dlvOption4;
	}

	public void setDlvOption4(String dlvOption4) {
		this.dlvOption4 = dlvOption4;
	}

	public String getDlvOption5() {
		return dlvOption5;
	}

	public void setDlvOption5(String dlvOption5) {
		this.dlvOption5 = dlvOption5;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVODlvNew [userId=");
		builder.append(userId);
		builder.append(", ordId=");
		builder.append(ordId);
		builder.append(", phoneNo=");
		builder.append(phoneNo);
		builder.append(", serialNo=");
		builder.append(serialNo);
		builder.append(", date=");
		builder.append(date);
		builder.append(", memo=");
		builder.append(memo);
		builder.append(", timeArea=");
		builder.append(timeArea);
		builder.append(", version=");
		builder.append(version);
		builder.append(", comCost=");
		builder.append(comCost);
		builder.append(", costType=");
		builder.append(costType);
		builder.append(", setType=");
		builder.append(setType);
		builder.append(", dlvOption1=");
		builder.append(dlvOption1);
		builder.append(", dlvOption2=");
		builder.append(dlvOption2);
		builder.append(", dlvOption3=");
		builder.append(dlvOption3);
		builder.append(", dlvOption4=");
		builder.append(dlvOption4);
		builder.append(", dlvOption5=");
		builder.append(dlvOption5);
		builder.append("]");
		return builder.toString();
	}

}