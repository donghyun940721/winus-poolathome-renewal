package com.logisall.ws.interfaces.common.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class WorkListBaseVODlv220501 implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8085791936054188255L;
	
	private String userId;
	private String ordId;
	private String phoneNo;
	private String serialNo;
	private String date;
	private String memo;
	private String timeArea;
	private String version;
	private String comCost;
	private String costType;
	private String setType;
	private String dlvOption1;
	private String dlvOption2;
	private String dlvOption3;
	private String dlvOption4;
	private String dlvOption5;
	private String dlvOption6;
	private String dlvOption7;
	private String dlvOption8;
	private String dlvOption9;
	private String dlvOption10;
	private String dlvOption11;
	private String dlvOption12;
	private String dlvOption13;
	private String dlvOption14;
	private String dlvOption15;
	private String dlvOption16;
	private String dlvOption17;
	private String dlvOption18;
	private String dlvOption19;
	private String dlvOption20;
	
	private List<RtiSerialInfoVOKR> rtiSerialList;
		
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrdId() {
		return ordId;
	}

	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}

	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}
	
	public String getTimeArea() {
		return timeArea;
	}

	public void setTimeArea(String timeArea) {
		this.timeArea = timeArea;
	}
	
	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getComCost() {
		return comCost;
	}

	public void setComCost(String comCost) {
		this.comCost = comCost;
	}

	public String getCostType() {
		return costType;
	}

	public void setCostType(String costType) {
		this.costType = costType;
	}

	public String getSetType() {
		return setType;
	}

	public void setSetType(String setType) {
		this.setType = setType;
	}

	public String getDlvOption1() {
		return dlvOption1;
	}

	public void setDlvOption1(String dlvOption1) {
		this.dlvOption1 = dlvOption1;
	}

	public String getDlvOption2() {
		return dlvOption2;
	}

	public void setDlvOption2(String dlvOption2) {
		this.dlvOption2 = dlvOption2;
	}

	public String getDlvOption3() {
		return dlvOption3;
	}

	public void setDlvOption3(String dlvOption3) {
		this.dlvOption3 = dlvOption3;
	}

	public String getDlvOption4() {
		return dlvOption4;
	}

	public void setDlvOption4(String dlvOption4) {
		this.dlvOption4 = dlvOption4;
	}

	public String getDlvOption5() {
		return dlvOption5;
	}

	public void setDlvOption5(String dlvOption5) {
		this.dlvOption5 = dlvOption5;
	}

	public String getDlvOption6() {
		return dlvOption6;
	}

	public void setDlvOption6(String dlvOption6) {
		this.dlvOption6 = dlvOption6;
	}

	public String getDlvOption7() {
		return dlvOption7;
	}

	public void setDlvOption7(String dlvOption7) {
		this.dlvOption7 = dlvOption7;
	}

	public String getDlvOption8() {
		return dlvOption8;
	}

	public void setDlvOption8(String dlvOption8) {
		this.dlvOption8 = dlvOption8;
	}

	public String getDlvOption9() {
		return dlvOption9;
	}

	public void setDlvOption9(String dlvOption9) {
		this.dlvOption9 = dlvOption9;
	}

	public String getDlvOption10() {
		return dlvOption10;
	}

	public void setDlvOption10(String dlvOption10) {
		this.dlvOption10 = dlvOption10;
	}

	public String getDlvOption11() {
		return dlvOption11;
	}

	public void setDlvOption11(String dlvOption11) {
		this.dlvOption11 = dlvOption11;
	}

	public String getDlvOption12() {
		return dlvOption12;
	}

	public void setDlvOption12(String dlvOption12) {
		this.dlvOption12 = dlvOption12;
	}

	public String getDlvOption13() {
		return dlvOption13;
	}

	public void setDlvOption13(String dlvOption13) {
		this.dlvOption13 = dlvOption13;
	}

	public String getDlvOption14() {
		return dlvOption14;
	}

	public void setDlvOption14(String dlvOption14) {
		this.dlvOption14 = dlvOption14;
	}

	public String getDlvOption15() {
		return dlvOption15;
	}

	public void setDlvOption15(String dlvOption15) {
		this.dlvOption15 = dlvOption15;
	}

	public String getDlvOption16() {
		return dlvOption16;
	}

	public void setDlvOption16(String dlvOption16) {
		this.dlvOption16 = dlvOption16;
	}

	public String getDlvOption17() {
		return dlvOption17;
	}

	public void setDlvOption17(String dlvOption17) {
		this.dlvOption17 = dlvOption17;
	}

	public String getDlvOption18() {
		return dlvOption18;
	}

	public void setDlvOption18(String dlvOption18) {
		this.dlvOption18 = dlvOption18;
	}

	public String getDlvOption19() {
		return dlvOption19;
	}

	public void setDlvOption19(String dlvOption19) {
		this.dlvOption19 = dlvOption19;
	}

	public String getDlvOption20() {
		return dlvOption20;
	}

	public void setDlvOption20(String dlvOption20) {
		this.dlvOption20 = dlvOption20;
	}

	public List<RtiSerialInfoVOKR> getRtiSerialList() {
		return rtiSerialList;
	}

	public void setRtiSerialList(List<RtiSerialInfoVOKR> rtiSerialList) {
		this.rtiSerialList = rtiSerialList;
	}
	
	@Override
	public String toString() {
		StringBuffer builder = new StringBuffer();
		builder.append("WorkListBaseVODlvNew [userId=");
		builder.append(userId);
		builder.append(", ordId=");
		builder.append(ordId);
		builder.append(", phoneNo=");
		builder.append(phoneNo);
		builder.append(", serialNo=");
		builder.append(serialNo);
		builder.append(", date=");
		builder.append(date);
		builder.append(", memo=");
		builder.append(memo);
		builder.append(", timeArea=");
		builder.append(timeArea);
		builder.append(", version=");
		builder.append(version);
		builder.append(", comCost=");
		builder.append(comCost);
		builder.append(", costType=");
		builder.append(costType);
		builder.append(", setType=");
		builder.append(setType);
		builder.append(", dlvOption1=");
		builder.append(dlvOption1);
		builder.append(", dlvOption2=");
		builder.append(dlvOption2);
		builder.append(", dlvOption3=");
		builder.append(dlvOption3);
		builder.append(", dlvOption4=");
		builder.append(dlvOption4);
		builder.append(", dlvOption5=");
		builder.append(dlvOption5);
		builder.append(", dlvOption6=");
		builder.append(dlvOption6);
		builder.append(", dlvOption7=");
		builder.append(dlvOption7);
		builder.append(", dlvOption8=");
		builder.append(dlvOption8);
		builder.append(", dlvOption9=");
		builder.append(dlvOption9);
		builder.append(", dlvOption10=");
		builder.append(dlvOption10);
		builder.append(", dlvOption11=");
		builder.append(dlvOption11);
		builder.append(", dlvOption12=");
		builder.append(dlvOption12);
		builder.append(", dlvOption13=");
		builder.append(dlvOption13);
		builder.append(", dlvOption14=");
		builder.append(dlvOption14);
		builder.append(", dlvOption15=");
		builder.append(dlvOption15);
		builder.append(", dlvOption16=");
		builder.append(dlvOption16);
		builder.append(", dlvOption17=");
		builder.append(dlvOption17);
		builder.append(", dlvOption18=");
		builder.append(dlvOption18);
		builder.append(", dlvOption19=");
		builder.append(dlvOption19);
		builder.append(", dlvOption20=");
		builder.append(dlvOption20);
		builder.append(", rtiSerialList=");
		builder.append(rtiSerialList);
		builder.append("]");
		return builder.toString();
	}

}