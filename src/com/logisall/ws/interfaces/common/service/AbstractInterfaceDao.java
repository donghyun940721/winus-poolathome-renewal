package com.logisall.ws.interfaces.common.service;

import java.util.Map;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

public abstract class AbstractInterfaceDao extends SqlMapAbstractDAO {

	/**
	 * Method ID : insert 
	 * Method 설명 : 송수신문서 insert 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmsif.insert", model);
	}

	/**
	 * Method ID : insert 
	 * Method 설명 : 유져정보 확인 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public Object getUserInfo(Map<String, Object> model) {
		return executeView("wmsif.getpswd", model);
	}

	public int updateDocState(Map<String, Object> model) {
		Object obj = executeUpdate("wmsif.update.state", model);
		int rtn = 1;
		if (obj != null) {
			rtn = Integer.parseInt(obj.toString());
		}
		return rtn;
	}
	
    public Object selectPackageTest(Map<String, Object> model){
        return executeQueryForList("wmsif.test_package", model);
    }

}
