package com.logisall.ws.interfaces.common.service;

import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;
import com.logisall.winus.frm.common.util.DESUtil;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.InterfaceUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.vo.CargoInfoVO;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVO;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKR;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOForklift;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlvNew;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOHOWSER;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOGodomall;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKRDate;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOGodomallItem;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOShopbuy;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOShopbuyItem;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv200614;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv211231;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv220501;
import com.logisall.ws.interfaces.common.vo.ItemInfoVO;
import com.logisall.ws.interfaces.common.vo.ItemSerialInfoVO;
import com.logisall.ws.interfaces.common.vo.ItemSerialInfoVOKR;
import com.logisall.ws.interfaces.common.vo.ItemSerialInfoVOKRDate;
import com.logisall.ws.interfaces.common.vo.LotInfoVO;
import com.logisall.ws.interfaces.common.vo.PackingInfoVO;
import com.logisall.ws.interfaces.common.vo.RtiInfoVO;
import com.logisall.ws.interfaces.common.vo.RtiSerialInfoVO;
import com.logisall.ws.interfaces.common.vo.RtiSerialInfoVOKR;
import com.logisall.ws.interfaces.common.vo.RtiSerialInfoVOKRDate;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVO;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVOKR;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVOForklift;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVODlv;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVODlvNew;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVOGodomall;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVOKRDate;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVOGodomallItem;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVOShopbuy;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVOShopbuyItem;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVODlv200614;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVODlv211231;
import com.logisall.ws.interfaces.common.vo.WorkListBaseVODlv220501;
import com.logisall.ws.interfaces.common.vo.GoodsResultVO;
import com.logisall.ws.interfaces.common.vo.OrderDeliveryDataVO;
import com.logisall.ws.interfaces.common.vo.DeliveryManUsersVO;
import com.logisall.ws.interfaces.common.vo.OnsiteImagesVO;
import com.logisall.ws.interfaces.common.vo.DeliveryInfoVO;
import com.logisall.ws.interfaces.common.vo.OrderInfoDataVO;
import com.logisall.ws.interfaces.common.vo.AddGoodsDataVO;
import com.logisall.ws.interfaces.common.vo.GiftDataVO;
import com.logisall.ws.interfaces.common.vo.OrderGoodsDataVO;
import com.logisall.ws.interfaces.common.vo.ClaimDataVO;
import com.logisall.ws.interfaces.common.vo.ExchangeInfoDataVO;
import com.logisall.ws.interfaces.common.vo.OrderConsultDataVO;
import com.logisall.ws.interfaces.common.vo.AddGoodsSnoDataVO;
import com.logisall.ws.interfaces.common.vo.OptionDataVO;
import com.logisall.ws.interfaces.common.vo.MagnifyImageDataVO;
import com.logisall.ws.interfaces.common.vo.MainImageDataVO;
import com.logisall.ws.interfaces.common.vo.ListImageDataVO;
import com.logisall.ws.interfaces.common.vo.DetailImageDataVO;
import com.logisall.ws.interfaces.common.vo.TextOptionDataVO;
import com.logisall.ws.interfaces.common.vo.GoodsMustInfoDataVO;
import com.logisall.ws.interfaces.common.vo.StepDataVO;
import com.logisall.ws.interfaces.common.vo.DeliveryGroupsVO;
import com.logisall.ws.interfaces.common.vo.OrderProductsVO;
import com.logisall.ws.interfaces.common.vo.OrderProductOptionsVO;
import com.logisall.ws.interfaces.common.vo.SetOptionsVO;

import com.m2m.jdfw5x.egov.message.MessageResolver;

import net.sf.sojo.core.Conversion;

public abstract class AbstractInterfaceServiceImpl implements AbstractInterfaceService {

	protected static Log log = LogFactory.getLog(AbstractInterfaceServiceImpl.class);

	private AbstractInterfaceDao dao;

	public AbstractInterfaceServiceImpl(AbstractInterfaceDao dao) {
		this.dao = dao;
	}

	public void sendJSON(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception {
		PrintWriter printWriter = null;
		try {
			Gson gson = new Gson();
			String jsonString = gson.toJson(model);
			//log.info(" ********** (1)paramMap ====================================: " + paramMap);
			//log.info(" ********** (2)model ====================================: " + model);
			
			if ("Y".equals((String) paramMap.get("SAVE_JSON_FILE"))) {
//				InterfaceUtil.saveJSONStringToFile(paramMap, jsonString);

				if ("Y".equals((String) paramMap.get("SAVE_DOC_INFO"))) {

					if (model.get("ERRCODE") == null) {
						model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_SUCCESS);

					} else {
						paramMap.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_ERROR);
						paramMap.put("D_ERR_CD", model.get("ERRCODE"));
						paramMap.put("D_MESSAGE", model.get("MSG"));
					}

//					saveSendDocInfo(paramMap);
				}
			}
			
			//log.info(" ********** (3)jsonString ====================================: " + jsonString);
			
//			String sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_S).append(ConstantWSIF.KEY_JSON_RESPONSE_START).append(jsonString).append(ConstantWSIF.KEY_JSON_RESPONSE_END).append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_E).toString();
			String sendData = new StringBuffer().append(jsonString).toString();
			System.out.println(sendData);
			response.setCharacterEncoding(ConstantWSIF.ENCODING_TYPE_JSON_FILE);
			// response.setContentType("text/html; charset=UTF-8");
			// response.setContentType("application/json; charset=UTF-8");

			printWriter = response.getWriter();
			printWriter.println(sendData);
			printWriter.close();

			// 별도 상태변경 필요 없을 듯
			/*
			 * if ( model.get("ERRCODE") == null && "Y".equals((String)
			 * paramMap.get("SAVE_JSON_FILE")) ) { paramMap.put("TR_STAT",
			 * ConstantWSIF.SEND_STATE_SEND_PROCESS_SUCCESS);
			 * updateDocState(paramMap); }
			 */

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
			throw e;

		} finally {
			if (printWriter != null) {
				try {
					printWriter.close();
				} catch (Exception e) {
				}
			}
		}
	}

	public void sendResponse(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception {
		PrintWriter printWriter = null;
		try {
//			Gson gson = new Gson();
//			String jsonString = gson.toJson(model);
//			if(Integer.parseInt((String)model.get("RETURN_CD")) < 0) {
//				String sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_START).append(jsonString).append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString();
//				sendData = "{\"SEND_DATA\":\"ERROR\"}";
//				response.setCharacterEncoding(ConstantWSIF.ENCODING_TYPE_JSON_FILE);
//				printWriter = response.getWriter();
//				printWriter.println(sendData);
//				printWriter.close();
//				
//			} else {
//				if ("Y".equals((String) paramMap.get("SAVE_JSON_FILE")) && "Y".equals((String) paramMap.get("SAVE_DOC_INFO"))) {
//	
//					if (model.get("ERRCODE") == null) {
//						paramMap.put("D_TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_SUCCESS);
//	
//					} else {
//						paramMap.put("D_TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_ERROR);
//						paramMap.put("D_ERR_CD", model.get("ERRCODE"));
//						paramMap.put("D_MESSAGE", model.get("MSG"));
//					}
//					updateDocState(paramMap);
//				}
//				String sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_START).append(jsonString).append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString();
//				sendData = "{\"SEND_DATA\":\"SUCCESS\"}";
//				response.setCharacterEncoding(ConstantWSIF.ENCODING_TYPE_JSON_FILE);
//				printWriter = response.getWriter();
//				printWriter.println(sendData);
//				printWriter.close();
//			}
			// response.setContentType("text/html; charset=UTF-8");
			//response.setContentType("application/json; charset=UTF-8");
			//printWriter = response.getWriter();
			//printWriter.println(sendData);
			//printWriter.close();
			
			Gson gson = new Gson();
			String jsonString = gson.toJson(model);

			String sendData = "";
			if(model.get("RETURN_CD") != null){
				if (Integer.parseInt(model.get("RETURN_CD").toString()) > -1) {
					sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_S).append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_START).append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C)
							.append("\"").append("SUCCESS").append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_E).append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString();					//sendData = "{\"SEND_DATA\":\"SUCCESS\"}";
				} else {

					paramMap.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_ERROR);
					paramMap.put("D_ERR_CD", paramMap.get("ERRCODE"));
					paramMap.put("D_MESSAGE", paramMap.get("MSG"));
					
					updateDocState(paramMap);
					
					String RETURN_MSG = model.get("RETURN_MSG").toString();
					sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_S).append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_START)
							.append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C).append("\"").append("ERROR").append("\"")
							.append(",").append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_RETURN_MSG).append("\"")
							.append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C).append("\"").append(RETURN_MSG).append("\"")
							.append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_E).append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString();
				}
			} else {			
				if (Integer.parseInt(paramMap.get("ERRCODE").toString()) < 0){

					paramMap.put("TR_STAT", ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_ERROR);
					paramMap.put("D_ERR_CD", paramMap.get("ERRCODE"));
					paramMap.put("D_MESSAGE", paramMap.get("MSG"));
					
					updateDocState(paramMap);
					
					sendData = new StringBuffer().append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_S).append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_START).append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_C)
							.append("\"").append("ERROR").append("\"").append(ConstantWSIF.KEY_JSON_RESPONSE_DEFAULT_E).append(ConstantWSIF.KEY_JSON_RESPONSE_END).toString();
				}
			}
			//log.info("sendData+sendData11============:"+sendData);

			response.setCharacterEncoding(ConstantWSIF.ENCODING_TYPE_JSON_FILE);

			printWriter = response.getWriter();
			printWriter.println(sendData);
			printWriter.close();
			

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
			throw e;

		} finally {
			if (printWriter != null) {
				try {
					printWriter.close();
				} catch (Exception e) {
				}
			}
		}
	}

	public void comSendResponse(String custGb, HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception {
		PrintWriter printWriter = null;
		
		try {
			Gson gson = new Gson();
			String jsonString = gson.toJson(model);
			
			//** SWEETTRACKER 일 경우
			if(custGb == "SWEETTRACKER"){
				JSONObject jsonObject = new JSONObject();
				JSONArray trackingDetails = new JSONArray();
				JSONObject trackingDetailsInfo = new JSONObject();
				
				int idx = 0;
				String result = null;
				String senderName = null;
				String receiverName = null;
				String itemName = null;
				String invoiceNo = null;
				String receiverAddr = null;
				String zipcode = null;
				String estimate = null;
				String level = null;
				String complete = null;
				String recipient = null;
				
				List<Map<String, Object>> outputList = (List<Map<String, Object>>) model.get("DATA");
		        for (Map<String, Object> outputInfo : outputList) {
		        	if(idx == 0){
		        		//** parameter json 변수 데이터 만들기
		        		result = getParameterValue(outputInfo.get("dlv_trace_result"));
		        		senderName = getParameterValue(outputInfo.get("dlv_trace_sender_name"));
		        		receiverName = getParameterValue(outputInfo.get("dlv_trace_receiver_name"));
		        		itemName = getParameterValue(outputInfo.get("dlv_trace_item_name"));
		        		invoiceNo = getParameterValue(outputInfo.get("dlv_trace_invoice_no"));
		        		receiverAddr = getParameterValue(outputInfo.get("dlv_trace_receiver_addr"));
		        		zipcode = getParameterValue(outputInfo.get("dlv_trace_zipcode"));
		        		estimate = getParameterValue(outputInfo.get("dlv_trace_estimate"));
		        		level = getParameterValue(outputInfo.get("dlv_trace_level"));
		        		complete = getParameterValue(outputInfo.get("dlv_trace_complete"));
		        		recipient = getParameterValue(outputInfo.get("dlv_trace_recipient"));
		        	}
		        	
		        	//** trackingDetails[] json 데이터 만들기
		        	trackingDetailsInfo = new JSONObject();
		        	trackingDetailsInfo.put("time", getParameterValue(outputInfo.get("dlv_trace_detail_time")));
		        	trackingDetailsInfo.put("timeString", getParameterValue(outputInfo.get("dlv_trace_detail_time_string")));
		        	trackingDetailsInfo.put("where", getParameterValue(outputInfo.get("dlv_trace_detail_where")));
		        	trackingDetailsInfo.put("kind", getParameterValue(outputInfo.get("dlv_trace_detail_kind")));
		        	trackingDetailsInfo.put("telno", getParameterValue(outputInfo.get("dlv_trace_detail_telno")));
		        	trackingDetailsInfo.put("telno2", getParameterValue(outputInfo.get("dlv_trace_detail_telno2")));
		        	trackingDetailsInfo.put("level", getParameterValue(outputInfo.get("dlv_trace_detail_level")));
		        	trackingDetailsInfo.put("manName", getParameterValue(outputInfo.get("dlv_trace_detail_man_name")));
		        	trackingDetailsInfo.put("manPic", getParameterValue(outputInfo.get("dlv_trace_detail_man_pic")));
		            trackingDetails.put(trackingDetailsInfo);
		        }
		        
		        //** parameter json 변수 jsonObject 넣기
		        jsonObject.put("result", result);
		        jsonObject.put("senderName", senderName);
		        jsonObject.put("receiverName", receiverName);
		        jsonObject.put("itemName", itemName);
		        jsonObject.put("invoiceNo", invoiceNo);
		        jsonObject.put("receiverAddr", receiverAddr);
		        jsonObject.put("zipcode", zipcode);
		        jsonObject.put("estimate", estimate);
		        jsonObject.put("level", level);
		        jsonObject.put("complete", complete);
		        jsonObject.put("recipient", recipient);
		        //** trackingDetails[] JSONArray 넣기
		        jsonObject.put("trackingDetails", trackingDetails);
		        
		        String jsonInfo = jsonObject.toString();
				//System.out.println(jsonInfo);
				response.setCharacterEncoding(ConstantWSIF.ENCODING_TYPE_JSON_FILE);
				response.setContentType("text/html; charset=UTF-8");
				response.setContentType("application/json; charset=UTF-8");

				printWriter = response.getWriter();
				printWriter.println(jsonInfo);
				printWriter.close();
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
			throw e;

		} finally {
			if (printWriter != null) {
				try {
					printWriter.close();
				} catch (Exception e) {
				}
			}
		}
	}
	
	public void updateDocState(Map<String, Object> model) throws Exception {

		try {
			Map<String, Object> modelDt = new HashMap<String, Object>();
			modelDt.put("SVC_NO", ConstantWSIF.WS_ADMIN_ID);
			modelDt.put("TR_ID", model.get("TR_ID"));
			modelDt.put("TR_STAT", model.get("TR_STAT"));

			dao.updateDocState(modelDt);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update Doc State :", e);
			}
		}

	}

	/**
	 * 
	 * Method ID : insertRetry Method 설명 : 송수신문서 재처리 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> insertRetry(Map<String, Object> model) throws Exception {

		Map<String, Object> map = new HashMap<String, Object>();

		try {
			model.put("D_TR_STAT", "R");
			model.put("D_ERR_CD", "");
			model.put("D_MESSAGE", "");
			model.put("INSERT_NO", "SYSTEM");
			dao.insert(model);

			/*
			 * Map<String, Object> modelDt = new HashMap<String, Object>();
			 * modelDt.put("SVC_NO", model.get(ConstantIF.SS_SVC_NO));
			 * modelDt.put("TR_ID", model.get("D_TR_ID"));
			 * modelDt.put("TR_STAT", "RS");
			 * 
			 * dao.updateTrStat(modelDt);
			 */

			map.put("MSG", MessageResolver.getMessage("complete"));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
			map.put("MSG", MessageResolver.getMessage("save.error", new String[]{}));
		}
		// //log.info(map);

		return map;

	}

	public void receiveJSON(Map<String, Object> model, String inputJSON) throws Exception {

		try {
			if ("Y".equals((String) model.get("SAVE_JSON_FILE"))) {
				InterfaceUtil.saveJSONStringToFile(model, inputJSON);

				if ("Y".equals((String) model.get("SAVE_DOC_INFO"))) {
					saveReceiveDocInfo(model);
				}
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
		}
	}

	protected void saveSendDocInfo(Map<String, Object> model) throws Exception {
		model.put("D_SVC_NO", ConstantWSIF.WS_ADMIN_ID);
		// model.put("D_TR_STAT", ConstantWSIF.SEND_STATE_SEND_PROCESS_INSERT);
		model.put("D_IN_OUT_CLS", "O");
		model.put("D_DOC_TYPE", "JSON");
		model.put("D_ERR_CD", model.get("D_ERR_CD"));
		model.put("D_MESSAGE", model.get("D_MESSAGE"));
		model.put("INSERT_NO", "SYSTEM");

		model.put("TR_ID", dao.insert(model));
	}

	protected void saveReceiveDocInfo(Map<String, Object> model) throws Exception {
		model.put("D_SVC_NO", ConstantWSIF.WS_ADMIN_ID);
		// model.put("D_TR_STAT",
		// ConstantWSIF.RECCEIVE_STATE_RECEIVE_PROCESS_INSERT);
		model.put("D_IN_OUT_CLS", "I");
		model.put("D_DOC_TYPE", "JSON");
		model.put("D_ERR_CD", model.get("D_ERR_CD"));
		model.put("D_MESSAGE", model.get("D_MESSAGE"));
		model.put("INSERT_NO", "SYSTEM");

		model.put("TR_ID", dao.insert(model));
	}

	protected void checkUserInfoValidation(Map<String, Object> model) throws Exception {
		try {
			String passwd = (String) model.get(ConstantWSIF.IF_KEY_PASSWORD);

			Map<String, Object> mapPswd = (Map) dao.getUserInfo(model);
			String passwd2 = (mapPswd != null) ? DESUtil.dataDecrypt((String) mapPswd.get("USER_PASSWORD")) : "";

			if (!passwd.equals(passwd2)) {
				throw new InterfaceException();
			}
		} catch (Exception e) {
			/*
			 * if (log.isErrorEnabled()) { log.error("Fail to get UserInfo :",
			 * e); }
			 */
			throw new InterfaceException("5002", "UserInfo is NOT VALID");
		}
	}

	protected Map<String, Object> createReturnMap(List<Map<String, Object>> outputList, Map<String, Object> model) throws Exception {
		Map<String, Object> returnMap = new HashMap<String, Object>();

		try {
			returnMap.put(ConstantWSIF.IF_KEY_LOGIN_ID, (String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID));
			returnMap.put(ConstantWSIF.IF_KEY_PASSWORD, (String) model.get(ConstantWSIF.IF_KEY_PASSWORD));
			returnMap.put(ConstantWSIF.IF_KEY_TERMINAL_ID, (String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID));

			if (outputList != null && outputList.size() > 0) {

				String currentWorkSeq = "";
				List<Map<String, Object>> workList = null;
				for (Map<String, Object> outputRow : outputList) {
					currentWorkSeq = getRowValue(outputRow, ConstantWSIF.IF_KEY_WORK_SEQ);

					workList = (List<Map<String, Object>>) returnMap.get(ConstantWSIF.IF_KEY_WORK_LIST);
					if (workList == null) {
						workList = new ArrayList<Map<String, Object>>();
					}
					createWorkInfo(outputRow, model, workList);

					if (!workList.isEmpty()) {
						returnMap.put(ConstantWSIF.IF_KEY_WORK_LIST, workList);
					}
				}
			}
		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
			throw new InterfaceException();
		}
		
		//log.info(" ********** createReturnMap:createReturnMap : " + returnMap);
		return returnMap;
	}
	
	protected Map<String, Object> createReturnMapKr(List<Map<String, Object>> outputList, Map<String, Object> model) throws Exception {
		Map<String, Object> returnMap = new HashMap<String, Object>();

		try {

			if (outputList != null && outputList.size() > 0) {

				List<Map<String, Object>> workList = null;
				for (Map<String, Object> outputRow : outputList) {

					workList = (List<Map<String, Object>>) returnMap.get(ConstantWSIF.IF_KEY_ROW_LC_ID);
					//log.info(" **********(317) workList : " + workList);
					if (workList == null) {
						workList = new ArrayList<Map<String, Object>>();
					}
					createWorkInfoKr(outputRow, model, workList);
					//log.info(" **********(325) workList:2222222 : " + workList);
					if (!workList.isEmpty()) {
						returnMap.put(ConstantWSIF.KEY_JSON_RESPONSE_START_NULL, outputList);
					}
				}
			}
		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
			throw new InterfaceException();
		}
		
		//log.info(" ********** createReturnMap : createReturnMapKr: " + returnMap);
		return returnMap;
	}

	protected Map<String, Object> createReturnMapKrDlv(List<Map<String, Object>> outputList, Map<String, Object> model) throws Exception {
		Map<String, Object> returnMap = new HashMap<String, Object>();

		try {

			if (outputList != null && outputList.size() > 0) {

				List<Map<String, Object>> workList = null;
				for (Map<String, Object> outputRow : outputList) {

					workList = (List<Map<String, Object>>) returnMap.get(ConstantWSIF.IF_KEY_ROW_ORD_ID);
					//log.info(" **********(317) workList : " + workList);
					if (workList == null) {
						workList = new ArrayList<Map<String, Object>>();
					}
					createWorkInfoKrDlv(outputRow, model, workList);
					//log.info(" **********(325) workList:2222222 : " + workList);
					if (!workList.isEmpty()) {
						returnMap.put(ConstantWSIF.KEY_JSON_RESPONSE_START_NULL, outputList);
					}
				}
			}
		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Return Map :", e);
			}
			throw new InterfaceException();
		}
		
		//log.info(" ********** createReturnMap : createReturnMapKr: " + returnMap);
		return returnMap;
	}

protected Map<String, Object> createWorkInfoKrUwms(List<Map<String, Object>> outputList, Map<String, Object> model) throws Exception {
	Map<String, Object> returnMap = new HashMap<String, Object>();

	try {

		if (outputList != null && outputList.size() > 0) {

			List<Map<String, Object>> workList = null;
			for (Map<String, Object> outputRow : outputList) {

				workList = (List<Map<String, Object>>) returnMap.get(ConstantWSIF.IF_KEY_ROW_LC_ID);
				//log.info(" **********(317) workList : " + workList);
				if (workList == null) {
					workList = new ArrayList<Map<String, Object>>();
				}
				createWorkInfoKrUwms(outputRow, model, workList);
				//log.info(" **********(325) workList:2222222 : " + workList);
				if (!workList.isEmpty()) {
					returnMap.put(ConstantWSIF.KEY_JSON_RESPONSE_START_NULL, outputList);
				}
			}
		}
	} catch (InterfaceException ie) {
		throw ie;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to create Return Map :", e);
		}
		throw new InterfaceException();
	}
	
	//log.info(" ********** createReturnMap : createReturnMapKr: " + returnMap);
	return returnMap;
}

protected Map<String, Object> createReturnMapStock(List<Map<String, Object>> outputList, Map<String, Object> model) throws Exception {
	Map<String, Object> returnMap = new HashMap<String, Object>();

	try {

		if (outputList != null && outputList.size() > 0) {

			List<Map<String, Object>> workList = null;
			for (Map<String, Object> outputRow : outputList) {

				workList = (List<Map<String, Object>>) returnMap.get(ConstantWSIF.IF_KEY_ROW_ORD_ID);
				//log.info(" **********(317) workList : " + workList);
				if (workList == null) {
					workList = new ArrayList<Map<String, Object>>();
				}
				createWorkInfoStock(outputRow, model, workList);
				//log.info(" **********(325) workList:2222222 : " + workList);
				if (!workList.isEmpty()) {
					returnMap.put(ConstantWSIF.KEY_JSON_RESPONSE_START_NULL, outputList);
				}
			}
		}
	} catch (InterfaceException ie) {
		throw ie;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to create Return Map :", e);
		}
		throw new InterfaceException();
	}
	
	//log.info(" ********** createReturnMap : createReturnMapKr: " + returnMap);
	return returnMap;
}	

protected Map<String, Object> createReturnMapDlvTrace(List<Map<String, Object>> outputList, Map<String, Object> model) throws Exception {
	Map<String, Object> returnMap = new HashMap<String, Object>();

	try {

		if (outputList != null && outputList.size() > 0) {

			List<Map<String, Object>> workList = null;
			for (Map<String, Object> outputRow : outputList) {

				workList = (List<Map<String, Object>>) returnMap.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_INVOICE_NO);
				//log.info(" **********(317) workList : " + workList);
				if (workList == null) {
					workList = new ArrayList<Map<String, Object>>();
				}
				createWorkInfoDlvTrace(outputRow, model, workList);
				//log.info(" **********(325) workList:2222222 : " + workList);
				if (!workList.isEmpty()) {
					returnMap.put(ConstantWSIF.KEY_JSON_RESPONSE_START_NULL, outputList);
				}
			}
		}
	} catch (InterfaceException ie) {
		throw ie;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to create Return Map :", e);
		}
		throw new InterfaceException();
	}
	
	//log.info(" ********** createReturnMap : createReturnMapKr: " + returnMap);
	return returnMap;
}
	
protected void createWorkInfoKr(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> workList) throws Exception {
		
		//log.info(" ********** createWorkInfo : S "+ workList);
		try {
			Map<String, Object> workInfoKr = null;
			String workSeq = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_LC_ID);
//			if (workSeq != null && !workSeq.isEmpty()) {
//				for (Map<String, Object> work : workList) {
//					String workSeqNo = convertString(work.get(ConstantWSIF.IF_KEY_LC_ID));
//					if (workSeq.equals(workSeqNo)) {
//						workInfoKr = work;
//						break;
//					}
//				}
			//log.info(" ********** workInfo111111111======== : S "+workInfoKr);
				if (workInfoKr == null) {
					workInfoKr = new HashMap<String, Object>();
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_LC_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_LC_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_USER_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_USER_NO));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_LANG, outputRow.get(ConstantWSIF.IF_KEY_ROW_LANG));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_PROGRAM_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_PROGRAM_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ORD_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_ORD_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ORD_SEQ, outputRow.get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ORD_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_ORD_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ORD_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_ORD_TYPE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ORD_SUBTYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_ORD_SUBTYPE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_RITEM_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_RITEM_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_RITEM_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_RITEM_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_RITEM_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_RITEM_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_CUST_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUST_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_CUST_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUST_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_CUST_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUST_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_CUST_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUST_TYPE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_TRANS_CUST_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_TRANS_CUST_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ORD_WEIGHT, outputRow.get(ConstantWSIF.IF_KEY_ROW_ORD_WEIGHT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_MIN_WGT, outputRow.get(ConstantWSIF.IF_KEY_ROW_MIN_WGT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_MAX_WGT, outputRow.get(ConstantWSIF.IF_KEY_ROW_MAX_WGT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_CUST_LOT_NO_M, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUST_LOT_NO_M));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_LOC_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_LOC_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_LOC_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_LOC_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_LOC_BARCODE_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_LOC_BARCODE_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_UOM_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_UOM_ID));					
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_MAKE_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_MAKE_DT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ITEM_BAR_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_BAR_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_EPC_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_EPC_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_BEFORE_STOCK_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_BEFORE_STOCK_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_IN_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_IN_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_OUT_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_OUT_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_STOCK_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_STOCK_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_BAD_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_BAD_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_AS_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_AS_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ITEM_GRP_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_GRP_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_UOM_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_UOM_NM));		
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_REMARK, outputRow.get(ConstantWSIF.IF_KEY_ROW_REMARK));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_EVENT_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_EVENT_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_PROP_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_PROP_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_RETURN_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_RETURN_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_PROP_CHK, outputRow.get(ConstantWSIF.IF_KEY_ROW_PROP_CHK));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_WH_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_WH_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_CROSS_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_CROSS_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_WORK_STAT, outputRow.get(ConstantWSIF.IF_KEY_ROW_WORK_STAT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_IN_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_IN_DT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_OUT_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_OUT_DT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_TOTAL_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_TOTAL_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_REMAIN_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_REMAIN_QTY));

					checkWorkInfoValidationKr(workInfoKr);
					
					//log.info(" ********** workInfo======== : S "+workInfoKr);

					// //log.info(" ********** ADD workInfo : " + workInfo);
					// //log.info(" ********** $$$ outputRow : " + outputRow);
					workList.add(workInfoKr);

					// //log.info("workList.add : " + workList.size());
				}
//			}

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("impl Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			throw new InterfaceException();
		}
	}

protected void createWorkInfoKrDlv(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> workList) throws Exception {
	
	//log.info(" ********** createWorkInfo : S "+ workList);
		try {
			Map<String, Object> workInfoKr = null;
			String workSeq = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_LC_ID);
			
			//log.info(" ********** workInfo111111111======== : S "+workInfoKr);
				if (workInfoKr == null) {
					workInfoKr = new HashMap<String, Object>();
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ORD_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ORD_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_CUSTOMER_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_CUSTOMER_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PRODUCT_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PRODUCT_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ADDR, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ADDR));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PHONE1, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PHONE1));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PHONE2, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PHONE2));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_WORK_STAT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_WORK_STAT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_REQ_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_REQ_DT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_HAPPY_CALL_YN, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ETC2));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ETC1, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ETC1));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ETC2, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ETC2));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_DRIVER_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_DRIVER_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_DRIVER_PHONE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_DRIVER_PHONE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ETC3, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ETC3));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_HAPPY_CALL_MEMO, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_HAPPY_CALL_MEMO));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_COMMENT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_COMMENT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_SERIAL_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_SERIAL_NO));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_TRUST_CUST_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRUST_CUST_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ORD_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ORD_TYPE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PART_PRICE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PART_PRICE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_WORK_COST, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_WORK_COST));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_VISIT_COST, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_VISIT_COST));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ERROR_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ERROR_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ERROR_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ERROR_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_SETUP_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_SETUP_DT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PAY_REQ_YN, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PAY_REQ_YN));
					
					checkWorkInfoValidationKr(workInfoKr);
					
					log.info(" ********** workInfo======== : S "+workInfoKr);

					// //log.info(" ********** ADD workInfo : " + workInfo);
					// //log.info(" ********** $$$ outputRow : " + outputRow);
					workList.add(workInfoKr);

					// //log.info("workList.add : " + workList.size());
				}
//			}

	} catch (InterfaceException ife) {
		if (log.isErrorEnabled()) {
			log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
		}
		throw ife;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to create Work Info :", e);
		}
		throw new InterfaceException();
	}
}

protected void createWorkInfoKrDlvHd(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> workList) throws Exception {
	
	//log.info(" ********** createWorkInfo : S "+ workList);
		try {
			Map<String, Object> workInfoKr = null;
			String workSeq = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_LC_ID);
			
			//log.info(" ********** workInfo111111111======== : S "+workInfoKr);
				if (workInfoKr == null) {
					workInfoKr = new HashMap<String, Object>();
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ORD_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ORD_ID));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ORD_SEQ, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ORD_SEQ));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_CUSTOMER_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_CUSTOMER_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PRODUCT_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PRODUCT_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_QTY));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ADDR, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ADDR));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PHONE1, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PHONE1));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PHONE2, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PHONE2));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_WORK_STAT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_WORK_STAT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_REQ_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_REQ_DT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_HAPPY_CALL_YN, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ETC2));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ETC1, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ETC1));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ETC2, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ETC2));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_DRIVER_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_DRIVER_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_DRIVER_PHONE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_DRIVER_PHONE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ETC3, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ETC3));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_HAPPY_CALL_MEMO, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_HAPPY_CALL_MEMO));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_COMMENT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_COMMENT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_SERIAL_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_SERIAL_NO));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_TRUST_CUST_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRUST_CUST_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ORD_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ORD_TYPE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PART_PRICE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PART_PRICE));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_WORK_COST, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_WORK_COST));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_VISIT_COST, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_VISIT_COST));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ERROR_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ERROR_CD));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_ERROR_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_ERROR_NM));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_SETUP_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_SETUP_DT));
					setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_DLV_PAY_REQ_YN, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_PAY_REQ_YN));
					
					checkWorkInfoValidationKr(workInfoKr);
					
					log.info(" ********** workInfo======== : S "+workInfoKr);

					// //log.info(" ********** ADD workInfo : " + workInfo);
					// //log.info(" ********** $$$ outputRow : " + outputRow);
					workList.add(workInfoKr);

					// //log.info("workList.add : " + workList.size());
				}
//			}

	} catch (InterfaceException ife) {
		if (log.isErrorEnabled()) {
			log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
		}
		throw ife;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to create Work Info :", e);
		}
		throw new InterfaceException();
	}
}

protected void createWorkInfoKrUwms(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> workList) throws Exception {
		
	//log.info(" ********** createWorkInfo : S "+ workList);
	try {
		Map<String, Object> workInfoKr = null;
		String workSeq = getRowValue(outputRow, ConstantWSIF.MAP_KEY_WORK_UWMS_PARM2);
		
		//log.info(" ********** workInfo111111111======== : S "+workInfoKr);
			if (workInfoKr == null) {
				workInfoKr = new HashMap<String, Object>();
				setJsonMap(workInfoKr, ConstantWSIF.MAP_KEY_WORK_UWMS_TARGET_CD, outputRow.get(ConstantWSIF.MAP_KEY_WORK_UWMS_TARGET_CD));
				setJsonMap(workInfoKr, ConstantWSIF.MAP_KEY_WORK_UWMS_TOKEN_CD, outputRow.get(ConstantWSIF.MAP_KEY_WORK_UWMS_TOKEN_CD));
				setJsonMap(workInfoKr, ConstantWSIF.MAP_KEY_WORK_UWMS_PARM2, outputRow.get(ConstantWSIF.MAP_KEY_WORK_UWMS_PARM2));
				setJsonMap(workInfoKr, ConstantWSIF.MAP_KEY_WORK_UWMS_PARM3, outputRow.get(ConstantWSIF.MAP_KEY_WORK_UWMS_PARM3));
				setJsonMap(workInfoKr, ConstantWSIF.MAP_KEY_WORK_UWMS_PARM4, outputRow.get(ConstantWSIF.MAP_KEY_WORK_UWMS_PARM4));
				setJsonMap(workInfoKr, ConstantWSIF.MAP_KEY_WORK_UWMS_PARM5, outputRow.get(ConstantWSIF.MAP_KEY_WORK_UWMS_PARM5));
				
				checkWorkInfoValidationKr(workInfoKr);
				
				log.info(" ********** workInfo======== : S "+workInfoKr);
				workList.add(workInfoKr);
			}

	} catch (InterfaceException ife) {
		if (log.isErrorEnabled()) {
			log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
		}
		throw ife;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to create Work Info :", e);
		}
		throw new InterfaceException();
	}
}

protected void createWorkInfoStock(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> workList) throws Exception {
	
	//log.info(" ********** createWorkInfo : S "+ workList);
	try {
		Map<String, Object> workInfoKr = null;
		String workSeq = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_LC_ID);
		
		//log.info(" ********** workInfo111111111======== : S "+workInfoKr);
			if (workInfoKr == null) {
				workInfoKr = new HashMap<String, Object>();
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_AS_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_AS_QTY));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_BEFORE_STOCK_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_BEFORE_STOCK_QTY));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_BAD_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_BAD_QTY));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_BL_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_BL_NO));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_CUST_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUST_CD));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_CUST_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUST_NM));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_IN_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_IN_QTY));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ITEM_BEST_DATE_END, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_BEST_DATE_END));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_ITEM_GRP_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_GRP_NM));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_LOC_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_LOC_CD));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_OUT_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_OUT_QTY));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_PROP_CHK, outputRow.get(ConstantWSIF.IF_KEY_ROW_PROP_CHK));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_PROP_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_PROP_QTY));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_REAL_PLT_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_REAL_PLT_QTY));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_RITEM_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_RITEM_ID));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_RITEM_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_RITEM_CD));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_RITEM_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_RITEM_NM));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_STOCK_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_STOCK_QTY));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_STOCK_WEIGHT, outputRow.get(ConstantWSIF.IF_KEY_ROW_STOCK_WEIGHT));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_UOM_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_UOM_NM));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_IN_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_IN_DT));
				setJsonMap(workInfoKr, ConstantWSIF.IF_KEY_ROW_WEEK_OF_YEAR_ISO, outputRow.get(ConstantWSIF.IF_KEY_ROW_WEEK_OF_YEAR_ISO));
				
				checkWorkInfoValidationKr(workInfoKr);
				
				//log.info(" ********** workInfo======== : S "+workInfoKr);

				// //log.info(" ********** ADD workInfo : " + workInfo);
				// //log.info(" ********** $$$ outputRow : " + outputRow);
				workList.add(workInfoKr);

				// //log.info("workList.add : " + workList.size());
			}
//		}

	} catch (InterfaceException ife) {
		if (log.isErrorEnabled()) {
			log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
		}
		throw ife;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to create Work Info :", e);
		}
		throw new InterfaceException();
	}
}

protected void createWorkInfoDlvTrace(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> workList) throws Exception {
	
	//log.info(" ********** createWorkInfo : S "+ workList);
	try {
		Map<String, Object> workInfoDlvTrace = null;
		String workSeq = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_INVOICE_NO);
		
		//log.info(" ********** workInfo111111111======== : S "+workInfoKr);
			if (workInfoDlvTrace == null) {
				workInfoDlvTrace = new HashMap<String, Object>();
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_RESULT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_RESULT));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_SENDER_NAME, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_SENDER_NAME));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_RECEIVER_NAME, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_RECEIVER_NAME));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ITEM_NAME, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ITEM_NAME));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_INVOICE_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_INVOICE_NO));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_RECEIVER_ADDR, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_RECEIVER_ADDR));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ZIPCODE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ZIPCODE));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ESTIMATE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ESTIMATE));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_LEVEL, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_LEVEL));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_COMPLETE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_COMPLETE));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_RECIPIENT, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_RECIPIENT));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_TIME, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_TIME));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_TIME_STRING, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_TIME_STRING));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_WHERE, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_WHERE));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_KIND, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_KIND));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_TELNO, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_TELNO));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_TELNO2, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_TELNO2));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_LEVEL, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_LEVEL));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_MAN_NAME, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_MAN_NAME));
				setJsonMap(workInfoDlvTrace, ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_MAN_PIC, outputRow.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_DETAIL_MAN_PIC));
				
				checkWorkInfoValidationKr(workInfoDlvTrace);
				
				//log.info(" ********** workInfo======== : S "+workInfoKr);

				// //log.info(" ********** ADD workInfo : " + workInfo);
				// //log.info(" ********** $$$ outputRow : " + outputRow);
				workList.add(workInfoDlvTrace);

				// //log.info("workList.add : " + workList.size());
			}
//		}

	} catch (InterfaceException ife) {
		if (log.isErrorEnabled()) {
			log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
		}
		throw ife;

	} catch (Exception e) {
		if (log.isErrorEnabled()) {
			log.error("Fail to create Work Info :", e);
		}
		throw new InterfaceException();
	}
}
	protected void createWorkInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> workList) throws Exception {
		
		//log.info(" ********** createWorkInfo : S ");
		try {
			Map<String, Object> workInfo = null;
			String workSeq = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_WORK_SEQ);
			if (workSeq != null && !workSeq.isEmpty()) {
				for (Map<String, Object> work : workList) {
					String workSeqNo = convertString(work.get(ConstantWSIF.IF_KEY_WORK_SEQ));
					if (workSeq.equals(workSeqNo)) {
						workInfo = work;
						break;
					}
				}
				if (workInfo == null) {
					workInfo = new HashMap<String, Object>();
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_EVENT_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_EVENT_CD));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_STATE_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_STATE_CD));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_REGIST_SEQ, outputRow.get(ConstantWSIF.IF_KEY_ROW_REGIST_SEQ));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_WORK_SEQ, outputRow.get(ConstantWSIF.IF_KEY_ROW_WORK_SEQ));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_BASE_LOC_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_BASE_LOC_CD));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_OTHER_PARTY_LOC_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_OTHER_PARTY_LOC_CD));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_CUSTOMER_LOC_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUSTOMER_LOC_CD));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_WAREHOUSE_ID, outputRow.get(ConstantWSIF.IF_KEY_ROW_WAREHOUSE_ID));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_WAREHOUSE_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_WAREHOUSE_NM));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_SHIPPING_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_SHIPPING_DT));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_RECEIVING_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_RECEIVING_DT));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_IN_OUT_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_IN_OUT_DT));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_UNIT_WEIGHT, outputRow.get(ConstantWSIF.IF_KEY_ROW_UNIT_WEIGHT));
					setJsonMap(workInfo, ConstantWSIF.IF_KEY_EVENT_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_EVENT_DT));
					
					checkWorkInfoValidation(workInfo);

					// //log.info(" ********** ADD workInfo : " + workInfo);
					// //log.info(" ********** $$$ outputRow : " + outputRow);
					workList.add(workInfo);

					// //log.info("workList.add : " + workList.size());
				}
			}

			if (workInfo != null) {
				
				// String ifType = (String)model.get(ConstantWSIF.IF_KEY_TYPE);
				// if(ConstantWSIF.IF_TYPE_TOTAL.equals(ifType)) {
				
				String rtiSerial = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_RTI_SERIAL);
				if(rtiSerial == null || StringUtils.isEmpty(rtiSerial)) {
					
					List<Map<String, Object>> itemList = (List<Map<String, Object>>) workInfo.get(ConstantWSIF.IF_KEY_ITEM_LIST);
					if (itemList == null) {
						itemList = new ArrayList<Map<String, Object>>();
					}
					int itemListSize = itemList.size();
	
					createItemInfo(outputRow, model, itemList);
	
					if (itemListSize != itemList.size()) {
						workInfo.put(ConstantWSIF.IF_KEY_ITEM_LIST, itemList);
					}
					
				} else {
				
					List<Map<String, Object>> packingList = (List<Map<String, Object>>) workInfo.get(ConstantWSIF.IF_KEY_PACKING_LIST);
					if (packingList == null) {
						packingList = new ArrayList<Map<String, Object>>();
					}
					int packingListSize = packingList.size();
	
					createPackingInfo(outputRow, model, packingList);
	
					if (packingListSize != packingList.size()) {
						workInfo.put(ConstantWSIF.IF_KEY_PACKING_LIST, packingList);
					}
					
					List<Map<String, Object>> itemList = (List<Map<String, Object>>) workInfo.get(ConstantWSIF.IF_KEY_ITEM_LIST);
					if (itemList == null) {
						itemList = new ArrayList<Map<String, Object>>();
					}
					int itemListSize = itemList.size();
	
					createWorkItemInfo(outputRow, model, itemList);
	
					if (itemListSize != itemList.size()) {
						workInfo.put(ConstantWSIF.IF_KEY_ITEM_LIST, itemList);
					}
				}
			}

		} catch (InterfaceException ife) {
			if (log.isErrorEnabled()) {
				log.error(new StringBuffer().append("Fail to create Work Info : [").append(ife.getMsgCode()).append("] : ").append(ife.getMessage()), ife);
			}
			throw ife;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Work Info :", e);
			}
			throw new InterfaceException();
		}
	}
	
	protected void createPackingInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> packingList) throws Exception {
		
		try {
			Map<String, Object> packingInfo = null;
			// String currentPackageNo =  (String) outputRow.get(ConstantWSIF.IF_KEY_PACKAGE_NO);
			// String currentPackageLevel =  (String) outputRow.get(ConstantWSIF.IF_KEY_PACKAGE_LEVEL);			
			
			String rtiSerial = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_RTI_SERIAL);
			if ( rtiSerial == null || StringUtils.isEmpty(rtiSerial) 
					|| (rtiSerial.equals(ConstantWSIF.DEFAULT_RTI_SERIAL_FOR_NO_PACKING))) {
				return;
			}		

			for (Map<String, Object> packing : packingList) {
				
				String rtiSerialNo = "";
				List<Map<String, Object>> rtiSerialList = (List<Map<String, Object>>) packing.get(ConstantWSIF.IF_KEY_RTI_SERIAL_LIST);
				if (rtiSerialList != null) {
					Map<String, Object> rtiSerialMap =  rtiSerialList.get(0);
					if (rtiSerialMap != null) { 
						rtiSerialNo = (String) rtiSerialMap.get(ConstantWSIF.IF_KEY_RTI_SERIAL); 
					}
				}
				
				if (rtiSerial.equals(rtiSerialNo)) {						
					packingInfo = packing;
					break;
				}
			}

			if (packingInfo == null) {
				packingInfo = new HashMap<String, Object>();
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_PACKAGE_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_PACKAGE_NO));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_PACKAGE_LEVEL, outputRow.get(ConstantWSIF.IF_KEY_ROW_PACKAGE_LEVEL));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_RTI_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_CD));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_QTY));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_REAL_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_REAL_QTY));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_INDIVIDUAL_GROSS_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_INDIVIDUAL_GROSS_TYPE));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_FROM_LOC_LOCAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_FROM_LOC_LOCAL_CD));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_TO_LOC_LOCAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_TO_LOC_LOCAL_CD));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_FROM_LOC_GLOBAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_FROM_LOC_GLOBAL_CD));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_TO_LOC_GLOBAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_TO_LOC_GLOBAL_CD));
				
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_TOTAL_WGT_MAX, outputRow.get(ConstantWSIF.IF_KEY_ROW_TOTAL_WGT_MAX));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_TOTAL_WGT_MIN, outputRow.get(ConstantWSIF.IF_KEY_ROW_TOTAL_WGT_MIN));
				setJsonMap(packingInfo, ConstantWSIF.IF_KEY_REAL_TOTAL_WGT, outputRow.get(ConstantWSIF.IF_KEY_ROW_REAL_TOTAL_WGT));
				
				packingList.add(packingInfo);

				// //log.info("packingList.add : " + packingList.size() + " ######## : " + packingInfo);
			}

			if (packingInfo != null) {
				List<Map<String, Object>> rtiSerialList = (List<Map<String, Object>>) packingInfo.get(ConstantWSIF.IF_KEY_RTI_SERIAL_LIST);
				if (rtiSerialList == null) {
					rtiSerialList = new ArrayList<Map<String, Object>>();
				}
				int rtiSerialListSize = rtiSerialList.size();

				createRtiSerialInfo(outputRow, model, rtiSerialList);

				if (rtiSerialListSize != rtiSerialList.size()) {
					packingInfo.put(ConstantWSIF.IF_KEY_RTI_SERIAL_LIST, rtiSerialList);
				}
				
				Map<String, Object> cargoInfo = (Map<String, Object>) packingInfo.get(ConstantWSIF.IF_KEY_CARGO_INFO);
				if (cargoInfo == null) {
					cargoInfo = new HashMap<String, Object>();
				}
				createCargoInfo(outputRow, model, cargoInfo);
				
				packingInfo.put(ConstantWSIF.IF_KEY_CARGO_INFO, cargoInfo);

			}
		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Packing Info :", e);
			}
			throw new InterfaceException();
		}
	}	
	
	protected void createCargoInfo(Map<String, Object> outputRow, Map<String, Object> model, Map<String, Object> cargoInfo) throws Exception {
		try {

			String currentRtichildSerial =  getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_RTI_CHILD_SERIAL);
			String currentitemSerial =  getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_ITEM_SERIAL);
			
			// //log.info("@@@@@@@@@@@@@@ currentRtichildSerial" + currentRtichildSerial + ", currentitemSerial :" + currentitemSerial);
			
			if (currentRtichildSerial != null && ! currentRtichildSerial.isEmpty() 
					&& (currentitemSerial == null || currentitemSerial.isEmpty())) {			
				List<Map<String, Object>> rtiList = (List<Map<String, Object>>) cargoInfo.get(ConstantWSIF.IF_KEY_RTI_LIST);
				if (rtiList == null) {
					rtiList = new ArrayList<Map<String, Object>>();
				}
				int rtiListSize = rtiList.size();
	
				createRtiInfo(outputRow, model, rtiList);
	
				if (rtiListSize != rtiList.size()) {
					cargoInfo.put(ConstantWSIF.IF_KEY_RTI_LIST, rtiList);
				}
			}
			
			if ((currentRtichildSerial == null || currentRtichildSerial.isEmpty()) 
					&& currentitemSerial != null && !currentitemSerial.isEmpty()) {				
				List<Map<String, Object>> itemList = (List<Map<String, Object>>) cargoInfo.get(ConstantWSIF.IF_KEY_ITEM_LIST);
				if (itemList == null) {
					itemList = new ArrayList<Map<String, Object>>();
				}
				int itemListSize = itemList.size();
	
				createCargoItemInfo(outputRow, model, itemList);
	
				if (itemListSize != itemList.size()) {
					cargoInfo.put(ConstantWSIF.IF_KEY_ITEM_LIST, itemList);
				}
			}
			
		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Item Info :", e);
			}
			throw new InterfaceException();
		}
	}
	
	protected void createRtiInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> rtiList) throws Exception {
		try {
			Map<String, Object> rtiInfo = null;
			String currentRtiKey = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_RTI_CHILD_SERIAL);
			if (currentRtiKey != null && !currentRtiKey.isEmpty()) {

				for (Map<String, Object> rti : rtiList) {
					String rtiNo = convertString(ConstantWSIF.IF_KEY_RTI_SERIAL);
					if (currentRtiKey.equals(rtiNo)) {
						rtiInfo = rti;
						break;
					}

				}

				if (rtiInfo == null) {
					rtiInfo = new HashMap<String, Object>();
					setJsonMap(rtiInfo, ConstantWSIF.IF_KEY_RTI_SERIAL, outputRow.get(ConstantWSIF.IF_KEY_ROW_RTI_CHILD_SERIAL));
					setJsonMap(rtiInfo, ConstantWSIF.IF_KEY_RFID_READ_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_RFID_CHILD_READ_DT));
					setJsonMap(rtiInfo, ConstantWSIF.IF_KEY_FROM_LOC_LOCAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_FROM_LOC_LOCAL_CD));
					setJsonMap(rtiInfo, ConstantWSIF.IF_KEY_TO_LOC_LOCAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_TO_LOC_LOCAL_CD));
					setJsonMap(rtiInfo, ConstantWSIF.IF_KEY_FROM_LOC_GLOBAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_FROM_LOC_GLOBAL_CD));
					setJsonMap(rtiInfo, ConstantWSIF.IF_KEY_TO_LOC_GLOBAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_TO_LOC_GLOBAL_CD));
					rtiList.add(rtiInfo);
				}
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Item Info :", e);
			}
			throw new InterfaceException();
		}
	}	
	
	protected void createRtiSerialInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> rtiSerialList) throws Exception {
		try {
			String currentRtiSerialNo = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_RTI_SERIAL);
			Map<String, Object> rtiSerialInfo = null;
			if (currentRtiSerialNo != null && !currentRtiSerialNo.isEmpty()) {

				for (Map<String, Object> rtiSerial : rtiSerialList) {
					String rtiSerialNo = convertString(rtiSerial.get(ConstantWSIF.IF_KEY_RTI_SERIAL));
					if (currentRtiSerialNo.equals(rtiSerialNo)) {
						rtiSerialInfo = rtiSerial;
						break;
					}

				}

				if (rtiSerialInfo == null) {
					rtiSerialInfo = new HashMap<String, Object>();
					setJsonMap(rtiSerialInfo, ConstantWSIF.IF_KEY_RTI_SERIAL, currentRtiSerialNo);
					setJsonMap(rtiSerialInfo, ConstantWSIF.IF_KEY_RFID_READ_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_RFID_READ_DT));
					
					rtiSerialList.add(rtiSerialInfo);
				}
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create RtiSerial Info :", e);
			}
			throw new InterfaceException();
		}
	}
	
	protected void createWorkItemInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> itemList) throws Exception {
		try {
			
			String currentRtichildSerial =  getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_RTI_CHILD_SERIAL);
			String currentitemSerial =  getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_ITEM_SERIAL);
			String currenrtiSerial =  getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_RTI_SERIAL);
			
			if ((currenrtiSerial == null || currenrtiSerial.isEmpty()) 
					&& (currentRtichildSerial != null && !currentRtichildSerial.isEmpty())
					&& (currentitemSerial != null && !currentitemSerial.isEmpty())) {
				
				Map<String, Object> itemInfo = null;
				String currentItemKey = getRowValue(outputRow, ConstantWSIF.IF_KEY_ITEM_CD);
				if (currentItemKey != null && !currentItemKey.isEmpty()) {
	
					for (Map<String, Object> item : itemList) {
						String itemNo = convertString(item.get(ConstantWSIF.IF_KEY_ITEM_CD));
						if (currentItemKey.equals(itemNo)) {
							itemInfo = item;
							break;
						}
	
					}
	
					if (itemInfo == null) {
						itemInfo = new HashMap<String, Object>();
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_CD));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_CUSTOMER_ITEM_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUSTOMER_ITEM_CD));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITF_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITF_CD));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_JAN_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_JAN_CD));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_NM));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_MAKER_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_MAKER_NM));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_COLOR_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_COLOR_NM));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_SIZE, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_SIZE));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_QTY));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_REAL_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_REAL_QTY));
						
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_UNIT, outputRow.get(ConstantWSIF.IF_KEY_ROW_UNIT));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_WGT, outputRow.get(ConstantWSIF.IF_KEY_ROW_WGT));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_WGT_ARROW_RATE, outputRow.get(ConstantWSIF.IF_KEY_ROW_WGT_ARROW_RATE));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_EXPIRY_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_EXPIRY_DT));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_MAKE_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_MAKE_DT));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_LOT_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_LOT_NO));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_BEST_DATE_NUM, outputRow.get(ConstantWSIF.IF_KEY_ROW_BEST_DATE_NUM));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_BEST_DATE_UNIT, outputRow.get(ConstantWSIF.IF_KEY_ROW_BEST_DATE_UNIT));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_INDIVIDUAL_GROSS_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_INDIVIDUAL_GROSS_TYPE));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_TOTAL_WGT_MAX, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_TOTAL_WGT_MAX));
						
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_TOTAL_WGT_MIN, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_TOTAL_WGT_MIN));
						setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_ID_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_ID_CD));
						
						itemList.add(itemInfo);
	
						//log.info("itemList.add : " + itemList.size());
					}
				}
	
				if (itemInfo != null) {
					List<Map<String, Object>> lotList = (List<Map<String, Object>>) itemInfo.get(ConstantWSIF.IF_KEY_LOT_LIST);
					if (lotList == null) {
						lotList = new ArrayList<Map<String, Object>>();
					}
					int lotListSize = lotList.size();
	
					createLotInfo(outputRow, model, lotList);
	
					if (lotListSize != lotList.size()) {
						itemInfo.put(ConstantWSIF.IF_KEY_LOT_LIST, lotList);
					}
	
				}
			}
		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Item Info :", e);
			}
			throw new InterfaceException();
		}
	}
	
	protected void createItemInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> itemList) throws Exception {
		try {
			Map<String, Object> itemInfo = null;
			String currentItemKey = (String) outputRow.get(ConstantWSIF.IF_KEY_ITEM_CD);
			if (currentItemKey != null && !currentItemKey.isEmpty()) {

				for (Map<String, Object> item : itemList) {
					String itemNo = (String) item.get(ConstantWSIF.IF_KEY_ITEM_CD);
					if (currentItemKey.equals(itemNo)) {
						itemInfo = item;
						break;
					}

				}

				if (itemInfo == null) {
					itemInfo = new HashMap<String, Object>();
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_CD));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_CUSTOMER_ITEM_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUSTOMER_ITEM_CD));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITF_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITF_CD));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_JAN_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_JAN_CD));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_NM));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_MAKER_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_MAKER_NM));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_COLOR_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_COLOR_NM));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_SIZE, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_SIZE));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_QTY));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_REAL_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_REAL_QTY));
					
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_UNIT, outputRow.get(ConstantWSIF.IF_KEY_ROW_UNIT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_WGT, outputRow.get(ConstantWSIF.IF_KEY_ROW_WGT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_WGT_ARROW_RATE, outputRow.get(ConstantWSIF.IF_KEY_ROW_WGT_ARROW_RATE));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_EXPIRY_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_EXPIRY_DT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_MAKE_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_MAKE_DT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_LOT_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_LOT_NO));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_BEST_DATE_NUM, outputRow.get(ConstantWSIF.IF_KEY_ROW_BEST_DATE_NUM));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_BEST_DATE_UNIT, outputRow.get(ConstantWSIF.IF_KEY_ROW_BEST_DATE_UNIT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_INDIVIDUAL_GROSS_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_INDIVIDUAL_GROSS_TYPE));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_TOTAL_WGT_MAX, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_TOTAL_WGT_MAX));
					
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_TOTAL_WGT_MIN, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_TOTAL_WGT_MIN));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_ID_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_ID_CD));
					itemList.add(itemInfo);
				}
			}

			if (itemInfo != null) {
				List<Map<String, Object>> lotList = (List<Map<String, Object>>) itemInfo.get(ConstantWSIF.IF_KEY_LOT_LIST);
				if (lotList == null) {
					lotList = new ArrayList<Map<String, Object>>();
				}
				int lotListSize = lotList.size();

				createLotInfo(outputRow, model, lotList);

				if (lotListSize != lotList.size()) {
					itemInfo.put(ConstantWSIF.IF_KEY_LOT_LIST, lotList);
				}

			}
		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Item Info :", e);
			}
			throw new InterfaceException();
		}
	}	

	protected void createCargoItemInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> itemList) throws Exception {
		try {				
			Map<String, Object> itemInfo = null;
			String currentItemKey = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_ITEM_ID_CD);
			if (currentItemKey != null && !currentItemKey.isEmpty()) {
				for (Map<String, Object> item : itemList) {
					String itemNo = convertString(item.get(ConstantWSIF.IF_KEY_ITEM_ID_CD));
					if (currentItemKey.equals(itemNo)) {
						itemInfo = item;
						break;
					}

				}

				if (itemInfo == null) {
					itemInfo = new HashMap<String, Object>();
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_CD));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_CUSTOMER_ITEM_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_CUSTOMER_ITEM_CD));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITF_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITF_CD));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_JAN_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_JAN_CD));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_NM));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_MAKER_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_MAKER_NM));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_COLOR_NM, outputRow.get(ConstantWSIF.IF_KEY_ROW_COLOR_NM));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_SIZE, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_SIZE));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_QTY));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_REAL_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_REAL_QTY));
					
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_UNIT, outputRow.get(ConstantWSIF.IF_KEY_ROW_UNIT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_WGT, outputRow.get(ConstantWSIF.IF_KEY_ROW_WGT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_WGT_ARROW_RATE, outputRow.get(ConstantWSIF.IF_KEY_ROW_WGT_ARROW_RATE));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_EXPIRY_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_EXPIRY_DT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_MAKE_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_MAKE_DT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_FINALLY_RECEIVING_LOT_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_FINALLY_RECEIVING_LOT_NO));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_BEST_DATE_NUM, outputRow.get(ConstantWSIF.IF_KEY_ROW_BEST_DATE_NUM));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_BEST_DATE_UNIT, outputRow.get(ConstantWSIF.IF_KEY_ROW_BEST_DATE_UNIT));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_INDIVIDUAL_GROSS_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_INDIVIDUAL_GROSS_TYPE));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_TOTAL_WGT_MAX, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_TOTAL_WGT_MAX));
					
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_TOTAL_WGT_MIN, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_TOTAL_WGT_MIN));
					setJsonMap(itemInfo, ConstantWSIF.IF_KEY_ITEM_ID_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_ID_CD));
					
					itemList.add(itemInfo);
					//log.info("itemList.add : " + itemList.size());
				}
			}

			if (itemInfo != null) {
				List<Map<String, Object>> lotList = (List<Map<String, Object>>) itemInfo.get(ConstantWSIF.IF_KEY_LOT_LIST);
				if (lotList == null) {
					lotList = new ArrayList<Map<String, Object>>();
				}
				int lotListSize = lotList.size();

				createLotInfo(outputRow, model, lotList);

				if (lotListSize != lotList.size()) {
					itemInfo.put(ConstantWSIF.IF_KEY_LOT_LIST, lotList);
				}

			}
		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Item Info :", e);
			}
			throw new InterfaceException();
		}
	}

	protected void createLotInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> lotList) throws Exception {
		try {
			String currentLotKey = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_ORD_SEQ);

			Map<String, Object> lotInfo = null;
			if (currentLotKey != null && !currentLotKey.isEmpty()) {

				for (Map<String, Object> lot : lotList) {
					String lotNo = convertString(lot.get(ConstantWSIF.IF_KEY_ORDER_SEQ));
					if (currentLotKey.equals(lotNo)) {
						lotInfo = lot;
						break;
					}

				}

				if (lotInfo == null) {
					lotInfo = new HashMap<String, Object>();
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_ORDER_SEQ, outputRow.get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_QTY));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_REAL_QTY, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_REAL_QTY));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_FROM_LOC_LOCAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_FROM_LOC_LOCAL_CD));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_TO_LOC_LOCAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_TO_LOC_LOCAL_CD));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_FROM_LOC_GLOBAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_FROM_LOC_GLOBAL_CD));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_TO_LOC_GLOBAL_CD, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_TO_LOC_GLOBAL_CD));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_EXPIRY_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_EXPIRY_DT));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_EXPIRY_DT_TYPE, outputRow.get(ConstantWSIF.IF_KEY_ROW_EXPIRY_DT_TYPE));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_MAKE_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_MAKE_DT));
					setJsonMap(lotInfo, ConstantWSIF.IF_KEY_LOT_NO, outputRow.get(ConstantWSIF.IF_KEY_ROW_LOT_NO));
					lotList.add(lotInfo);
				}
			}

			if (lotInfo != null) {
				List<Map<String, Object>> itemSerialList = (List<Map<String, Object>>) lotInfo.get(ConstantWSIF.IF_KEY_ITEM_SERIAL_LIST);
				if (itemSerialList == null) {
					itemSerialList = new ArrayList<Map<String, Object>>();
				}
				int itemSerialSize = itemSerialList.size();

				createIitemSerialInfo(outputRow, model, itemSerialList);

				if (itemSerialSize != itemSerialList.size()) {
					lotInfo.put(ConstantWSIF.IF_KEY_ITEM_SERIAL_LIST, itemSerialList);
				}
			}

		} catch (InterfaceException ie) {
			throw ie;

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create Lot Info :", e);
			}
			throw new InterfaceException();
		}
	}

	protected void createIitemSerialInfo(Map<String, Object> outputRow, Map<String, Object> model, List<Map<String, Object>> itemSerialList) throws Exception {
		try {
			String currentItemSerialNo = getRowValue(outputRow, ConstantWSIF.IF_KEY_ROW_ITEM_SERIAL);
			Map<String, Object> itemSerialInfo = null;
			if (currentItemSerialNo != null && !currentItemSerialNo.isEmpty()) {

				for (Map<String, Object> itemSerial : itemSerialList) {
					String itemSerialNo = convertString(itemSerial.get(ConstantWSIF.IF_KEY_ITEM_SERIAL));
					if (currentItemSerialNo.equals(itemSerialNo)) {
						itemSerialInfo = itemSerial;
						break;
					}

				}

				if (itemSerialInfo == null) {
					itemSerialInfo = new HashMap<String, Object>();
					setJsonMap(itemSerialInfo, ConstantWSIF.IF_KEY_ITEM_SERIAL, currentItemSerialNo);
					setJsonMap(itemSerialInfo, ConstantWSIF.IF_KEY_RFID_READ_DT, outputRow.get(ConstantWSIF.IF_KEY_ROW_ITEM_RFID_READ_DT));
					itemSerialList.add(itemSerialInfo);
				}
			}

			/* Make Dummy */
			/*
			if (itemSerialList.isEmpty()) {
				itemSerialInfo = new HashMap<String, Object>();
				itemSerialInfo.put(ConstantWSIF.IF_KEY_ITEM_SERIAL, null);
				itemSerialInfo.put(ConstantWSIF.IF_KEY_RFID_READ_DT, null);
				itemSerialList.add(itemSerialInfo);
			}
			*/

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to create ItemSerial Info :", e);
			}
			throw new InterfaceException();
		}
	}

	protected String getWorkType(InterfaceBaseVO data) {
		String type = "ITEM";
		if (data != null && data.getWorkList() != null && data.getWorkList().size() > 0) {
			WorkListBaseVO workInfo = (WorkListBaseVO) data.getWorkList().get(0);

			List<PackingInfoVO> packingList = (List<PackingInfoVO>) workInfo.getPackingList();
			if (packingList != null && packingList.size() > 0) {
				return "PACKING";
			}
		}
		return type;
	}

	protected void setWorkInfoMap(Map<String, Object> map, WorkListBaseVO workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_EVENT_CD, workInfo.getEventCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_STATE_CD, workInfo.getStateCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_REGIST_SEQ, workInfo.getRegistSeq());
		map.put(ConstantWSIF.MAP_KEY_WORK_WORK_SEQ, workInfo.getWorkSeq());
		map.put(ConstantWSIF.MAP_KEY_WORK_LOCAL_SLIP_NO, workInfo.getLocalSlipNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_CUSTOMER_SLIP_NO, workInfo.getCustomerSlipNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_OTHER_SLIP_NO, workInfo.getOtherSlipNo());
		
		// map.put(ConstantWSIF.MAP_KEY_WORK_LOCAL_LOC_CD, workInfo.getLocalLocCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_BASE_LOC_CD, workInfo.getBaseLocCd());
		
		map.put(ConstantWSIF.MAP_KEY_WORK_OTHER_PARTY_LOC_CD, workInfo.getOtherPartyLocCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_CUSTOMER_LOC_CD, workInfo.getCustomerLocCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_WAREHOUSE_ID, workInfo.getWarehouseId());
		map.put(ConstantWSIF.MAP_KEY_WORK_WAREHOUSE_NM, workInfo.getWarehouseNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_CUSTOMER_ID, workInfo.getCustomerId());
		map.put(ConstantWSIF.MAP_KEY_WORK_CUSTOMER_NM, workInfo.getCustomerNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHIPPING_DT, workInfo.getShippingDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_RECEIVING_DT, workInfo.getReceivingDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_IN_OUT_DT, workInfo.getInOutDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_EVENT_DT, workInfo.getEventDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_FORWARDER_CD, workInfo.getForwarderCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_FORWARDER_NM, workInfo.getForwarderNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_CAR_ID, workInfo.getCarId());
		map.put(ConstantWSIF.MAP_KEY_WORK_REASON_CD, workInfo.getReasonCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_UNIT_WEIGHT, workInfo.getUnitWeight());		
		// map.put(ConstantWSIF.MAP_KEY_WORK_LOCK_REASON_CD, workInfo.getLockReasonCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_LOCK_STATE_CD, workInfo.getLockStateCd());
	}

	protected void setWorkInfoMapKR(Map<String, Object> map, WorkListBaseVOKR workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_LC_ID, workInfo.getLcId());
		map.put(ConstantWSIF.MAP_KEY_WORK_TRANS_CUST_ID, workInfo.getTransCustId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_ID, workInfo.getOrdId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_SEQ_M, workInfo.getOrdSeq());
		map.put(ConstantWSIF.MAP_KEY_WORK_WORK_QTY, workInfo.getWorkQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_WORK_WGT, workInfo.getWorkWgt());
		map.put(ConstantWSIF.MAP_KEY_WORK_REF_SUB_LOT_ID, workInfo.getRefSubLotId());
		map.put(ConstantWSIF.MAP_KEY_WORK_SAP_BARCODE, workInfo.getSapBarcode());
		map.put(ConstantWSIF.MAP_KEY_WORK_FROM_LOC_BARCODE_CD, workInfo.getFromLocCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_TO_LOC_BARCODE_CD, workInfo.getToLocCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_CUST_LOT_NO_M, workInfo.getCustLotNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_RITEM_ID, workInfo.getRitemId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_CODE, workInfo.getItemCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_SET_TYPE, workInfo.getSetType());
		map.put(ConstantWSIF.MAP_KEY_WORK_MEMO, workInfo.getWorkMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO, workInfo.getSerialNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_TYPE_MEMO, workInfo.getWorkTypeMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_USER_NO, workInfo.getUserNo());	
		map.put(ConstantWSIF.MAP_KEY_WORK_IP, workInfo.getWorkIp());
	}

	protected void setWorkInfoMapKRDate(Map<String, Object> map, WorkListBaseVOKRDate workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_LC_ID, workInfo.getLcId());
		map.put(ConstantWSIF.MAP_KEY_WORK_TRANS_CUST_ID, workInfo.getTransCustId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_ID, workInfo.getOrdId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_SEQ_M, workInfo.getOrdSeq());
		map.put(ConstantWSIF.MAP_KEY_WORK_WORK_QTY, workInfo.getWorkQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_WORK_WGT, workInfo.getWorkWgt());
		map.put(ConstantWSIF.MAP_KEY_WORK_REF_SUB_LOT_ID, workInfo.getRefSubLotId());
		map.put(ConstantWSIF.MAP_KEY_WORK_SAP_BARCODE, workInfo.getSapBarcode());
		map.put(ConstantWSIF.MAP_KEY_WORK_FROM_LOC_BARCODE_CD, workInfo.getFromLocCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_TO_LOC_BARCODE_CD, workInfo.getToLocCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_CUST_LOT_NO_M, workInfo.getCustLotNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_TYPE, workInfo.getOrdType());
		map.put(ConstantWSIF.MAP_KEY_WORK_CUST_ID, workInfo.getCustId());
		map.put(ConstantWSIF.MAP_KEY_WORK_IN_CUST_ID, workInfo.getInCustId());
		map.put(ConstantWSIF.MAP_KEY_WORK_RITEM_ID, workInfo.getRitemId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_CODE, workInfo.getItemCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_BEST_DATE_END, workInfo.getItemBestDateEnd());
		map.put(ConstantWSIF.MAP_KEY_WORK_BOX_IN_QTY, workInfo.getBoxInQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_BOX_BAR_CD, workInfo.getBoxBarCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_BAR_CD, workInfo.getItemBarCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ISSUE_DATE, workInfo.getIssueDate());
		map.put(ConstantWSIF.MAP_KEY_WORK_MAKE_DATE, workInfo.getMakeDate());
		map.put(ConstantWSIF.MAP_KEY_WORK_MAKE_LINE_NO, workInfo.getMakeLineNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_USER_NO, workInfo.getUserNo());	
		map.put(ConstantWSIF.MAP_KEY_WORK_IP, workInfo.getWorkIp());
		map.put(ConstantWSIF.MAP_KEY_WORK_MEMO, workInfo.getWorkMemo());
	}
	
	protected void setWorkInfoMapForklift(Map<String, Object> map, WorkListBaseVOForklift workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_DEVICE_ID, workInfo.getDeviceId());
		map.put(ConstantWSIF.MAP_KEY_WORK_EPC_CD, workInfo.getEpcCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_POSITION_X, workInfo.getPositionX());
		map.put(ConstantWSIF.MAP_KEY_WORK_POSITION_Y, workInfo.getPositionY());
		map.put(ConstantWSIF.MAP_KEY_WORK_POSITION_Z, workInfo.getPositionZ());
		map.put(ConstantWSIF.MAP_KEY_WORK_WGT, workInfo.getWgt());
	}
	
	protected void setWorkInfoMapDlv(Map<String, Object> map, WorkListBaseVODlv workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_USER_ID, workInfo.getUserId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_ID, workInfo.getOrdId());
		map.put(ConstantWSIF.MAP_KEY_WORK_PHONE_NO, workInfo.getPhoneNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO, workInfo.getSerialNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_DATE, workInfo.getDate());
		map.put(ConstantWSIF.MAP_KEY_WORK_MEMO, workInfo.getMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_TIME_AREA, workInfo.getTimeArea());
		map.put(ConstantWSIF.MAP_KEY_WORK_VERSION, workInfo.getVersion());
		map.put(ConstantWSIF.MAP_KEY_WORK_COM_COST, workInfo.getComCost());
		map.put(ConstantWSIF.MAP_KEY_WORK_COST_TYPE, workInfo.getCostType());
	}
	
	protected void setWorkInfoMapDlvNew(Map<String, Object> map, WorkListBaseVODlvNew workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_USER_ID, workInfo.getUserId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_ID, workInfo.getOrdId());
		map.put(ConstantWSIF.MAP_KEY_WORK_PHONE_NO, workInfo.getPhoneNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO, workInfo.getSerialNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_DATE, workInfo.getDate());
		map.put(ConstantWSIF.MAP_KEY_WORK_MEMO, workInfo.getMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_TIME_AREA, workInfo.getTimeArea());
		map.put(ConstantWSIF.MAP_KEY_WORK_VERSION, workInfo.getVersion());
		map.put(ConstantWSIF.MAP_KEY_WORK_COM_COST, workInfo.getComCost());
		map.put(ConstantWSIF.MAP_KEY_WORK_COST_TYPE, workInfo.getCostType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SET_TYPE, workInfo.getSetType());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION1, workInfo.getDlvOption1());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION2, workInfo.getDlvOption2());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION3, workInfo.getDlvOption3());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION4, workInfo.getDlvOption4());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION5, workInfo.getDlvOption5());
	}
	
	protected void setWorkInfoMapDlv200614(Map<String, Object> map, WorkListBaseVODlv200614 workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_USER_ID, workInfo.getUserId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_ID, workInfo.getOrdId());
		map.put(ConstantWSIF.MAP_KEY_WORK_PHONE_NO, workInfo.getPhoneNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO, workInfo.getSerialNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_DATE, workInfo.getDate());
		map.put(ConstantWSIF.MAP_KEY_WORK_MEMO, workInfo.getMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_TIME_AREA, workInfo.getTimeArea());
		map.put(ConstantWSIF.MAP_KEY_WORK_VERSION, workInfo.getVersion());
		map.put(ConstantWSIF.MAP_KEY_WORK_COM_COST, workInfo.getComCost());
		map.put(ConstantWSIF.MAP_KEY_WORK_COST_TYPE, workInfo.getCostType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SET_TYPE, workInfo.getSetType());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION1, workInfo.getDlvOption1());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION2, workInfo.getDlvOption2());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION3, workInfo.getDlvOption3());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION4, workInfo.getDlvOption4());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION5, workInfo.getDlvOption5());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION6, workInfo.getDlvOption6());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION7, workInfo.getDlvOption7());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION8, workInfo.getDlvOption8());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION9, workInfo.getDlvOption9());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION10, workInfo.getDlvOption10());
	}
	
	protected void setWorkInfoMapDlv211231(Map<String, Object> map, WorkListBaseVODlv211231 workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_USER_ID, workInfo.getUserId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_ID, workInfo.getOrdId());
		map.put(ConstantWSIF.MAP_KEY_WORK_PHONE_NO, workInfo.getPhoneNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO, workInfo.getSerialNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_DATE, workInfo.getDate());
		map.put(ConstantWSIF.MAP_KEY_WORK_MEMO, workInfo.getMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_TIME_AREA, workInfo.getTimeArea());
		map.put(ConstantWSIF.MAP_KEY_WORK_VERSION, workInfo.getVersion());
		map.put(ConstantWSIF.MAP_KEY_WORK_COM_COST, workInfo.getComCost());
		map.put(ConstantWSIF.MAP_KEY_WORK_COST_TYPE, workInfo.getCostType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SET_TYPE, workInfo.getSetType());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION1, workInfo.getDlvOption1());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION2, workInfo.getDlvOption2());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION3, workInfo.getDlvOption3());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION4, workInfo.getDlvOption4());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION5, workInfo.getDlvOption5());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION6, workInfo.getDlvOption6());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION7, workInfo.getDlvOption7());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION8, workInfo.getDlvOption8());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION9, workInfo.getDlvOption9());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION10, workInfo.getDlvOption10());
	}
	
	protected void setWorkInfoMapDlv220501(Map<String, Object> map, WorkListBaseVODlv220501 workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_USER_ID, workInfo.getUserId());
		map.put(ConstantWSIF.MAP_KEY_WORK_ORD_ID, workInfo.getOrdId());
		map.put(ConstantWSIF.MAP_KEY_WORK_PHONE_NO, workInfo.getPhoneNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO, workInfo.getSerialNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_DATE, workInfo.getDate());
		map.put(ConstantWSIF.MAP_KEY_WORK_MEMO, workInfo.getMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_TIME_AREA, workInfo.getTimeArea());
		map.put(ConstantWSIF.MAP_KEY_WORK_VERSION, workInfo.getVersion());
		map.put(ConstantWSIF.MAP_KEY_WORK_COM_COST, workInfo.getComCost());
		map.put(ConstantWSIF.MAP_KEY_WORK_COST_TYPE, workInfo.getCostType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SET_TYPE, workInfo.getSetType());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION1, workInfo.getDlvOption1());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION2, workInfo.getDlvOption2());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION3, workInfo.getDlvOption3());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION4, workInfo.getDlvOption4());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION5, workInfo.getDlvOption5());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION6, workInfo.getDlvOption6());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION7, workInfo.getDlvOption7());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION8, workInfo.getDlvOption8());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION9, workInfo.getDlvOption9());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION10, workInfo.getDlvOption10());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION11, workInfo.getDlvOption11());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION12, workInfo.getDlvOption12());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION13, workInfo.getDlvOption13());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION14, workInfo.getDlvOption14());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION15, workInfo.getDlvOption15());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION16, workInfo.getDlvOption16());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION17, workInfo.getDlvOption17());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION18, workInfo.getDlvOption18());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION19, workInfo.getDlvOption19());
		map.put(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION20, workInfo.getDlvOption20());
	}
	
	protected void setItemInfoMapKR(Map<String, Object> map, RtiSerialInfoVOKR rtiSerialInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_EPC_CD, rtiSerialInfo.getEpcCd());
	}
	
	protected void setItemInfoMapKRDate(Map<String, Object> map, RtiSerialInfoVOKRDate rtiSerialInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_EPC_CD, rtiSerialInfo.getEpcCd());
	}
	
	protected void setItemInfoMap(Map<String, Object> map, ItemInfoVO itemInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_ITEM_CD, itemInfo.getItemCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_CUSTOMER_ITEM_CD, itemInfo.getCustomerItemCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_ITF_CD, itemInfo.getItfCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_JAN_CD, itemInfo.getJanCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_ITEM_NM, itemInfo.getItemNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_MAKER_NM, itemInfo.getMakerNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_COLOR_NM, itemInfo.getColorNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_SIZE, itemInfo.getSize());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_QTY, itemInfo.getQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_REAL_QTY, itemInfo.getRealQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_UNIT, itemInfo.getUnit());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_WGT, itemInfo.getWgt());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_WGT_ARROW_RATE, itemInfo.getWgtArrowRate());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_FINALLY_RECEIVING_EXPIRY_DT, itemInfo.getFinallyReceivingExpiryDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_FINALLY_RECEIVING_MAKE_DT, itemInfo.getFinallyReceivingMakeDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_FINALLY_RECEIVING_LOT_NO, itemInfo.getFinallyReceivingLotNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_BEST_DATE_NUM, itemInfo.getBestDateNum());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_BEST_DATE_UNIT, itemInfo.getBestDateUnit());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_INDIVIDUAL_GROSS_TYPE, itemInfo.getIndividualGrossType());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_TOTAL_WGT_MAX, itemInfo.getTotalWgtMax());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_TOTAL_WGT_MIN, itemInfo.getTotalWgtMin());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_REAL_TOTAL_WGT, itemInfo.getRealTotalWgt());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_ITEM_ID_CD, itemInfo.getItemIdCd());
	}

	protected void setLotInfoMap(Map<String, Object> map, LotInfoVO lotInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_ORDER_SEQ, lotInfo.getOrderSeq());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_QTY, lotInfo.getQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_REAL_QTY, lotInfo.getRealQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_FROM_LOC_LOCAL_CD, lotInfo.getFromLocLocalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_TO_LOC_LOCAL_CD, lotInfo.getToLocLocalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_FROM_LOC_GLOBAL_CD, lotInfo.getFromLocGlobalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_TO_LOC_GLOBAL_CD, lotInfo.getToLocGlobalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_EXPIRY_DT, lotInfo.getExpiryDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_EXPIRY_DT_TYPE, lotInfo.getExpiryDtType());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_MAKE_DT, lotInfo.getMakeDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_LOT_NO, lotInfo.getLotNo());
	}

	protected void setItemSerailInfoMap(Map<String, Object> map, ItemSerialInfoVO itemSerialInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_ITEM_SERIAL_ITEM_SERIAL, itemSerialInfo.getItemSerial());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_ITEM_SERIAL_ALERT_REASON_CD, itemSerialInfo.getAlertReasonCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_ITEM_SERIAL_RFID_READ_DT, itemSerialInfo.getRfidReadDt());
	}

	protected void setPackingInfoMap(Map<String, Object> map, PackingInfoVO packInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_PACKAGE_NO, packInfo.getPackageNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_PACKAGE_LEVEL, packInfo.getPackageLevel());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_CD, packInfo.getRtiCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_BARCODE, packInfo.getRtiBarcode());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_QTY, packInfo.getQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_REAL_QTY, packInfo.getRealQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_INDIVIDUAL_GROSS_TYPE, packInfo.getIndividualGrossType());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_FROM_LOC_LOCAL_CD, packInfo.getFromLocLocalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_TO_LOC_LOCAL_CD, packInfo.getToLocLocalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_FROM_LOC_GLOBAL_CD, packInfo.getFromLocGlobalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_TO_LOC_GLOBAL_CD, packInfo.getToLocGlobalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_TOTAL_WGT_MAX, packInfo.getTotalWgtMax());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_TOTAL_WGT_MIN, packInfo.getTotalWgtMin());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_REAL_TOTAL_WGT, packInfo.getRealTotalWgt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_ALERT_REASON_CD, packInfo.getAlertReasonCd());
	}

	protected void setRtiSerailInfoMap(Map<String, Object> map, RtiSerialInfoVO rtiSerialInfoVO) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RTI_SERIAL, rtiSerialInfoVO.getRtiSerial());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RFID_READ_DT, rtiSerialInfoVO.getRfidReadDt());
	}
	
	protected void setCargoRtiInfoMap(Map<String, Object> map, RtiInfoVO rtiInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_FROM_LOC_LOCAL_CD, rtiInfo.getFromLocLocalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_TO_LOC_LOCAL_CD, rtiInfo.getToLocLocalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_FROM_LOC_GLOBAL_CD, rtiInfo.getFromLocGlobalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_TO_LOC_GLOBAL_CD, rtiInfo.getToLocGlobalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_RTI_SERIAL, rtiInfo.getRtiSerial());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_RFID_READ_DT, rtiInfo.getRfidReadDt());
	}

	protected void setCargoItemInfoMap(Map<String, Object> map, ItemInfoVO itemInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_ITEM_CD, itemInfo.getItemCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_CUSTOMER_ITEM_CD, itemInfo.getCustomerItemCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_ITF_CD, itemInfo.getItfCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_JAN_CD, itemInfo.getJanCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_ITEM_NM, itemInfo.getItemNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_MAKER_NM, itemInfo.getMakerNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_COLOR_NM, itemInfo.getColorNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_SIZE, itemInfo.getSize());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_QTY, itemInfo.getQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_REAL_QTY, itemInfo.getRealQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_UNIT, itemInfo.getUnit());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_WGT, itemInfo.getWgt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_WGT_ARROW_RATE, itemInfo.getWgtArrowRate());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_FINALLY_RECEIVING_EXPIRY_DT, itemInfo.getFinallyReceivingExpiryDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_FINALLY_RECEIVING_MAKE_DT, itemInfo.getFinallyReceivingMakeDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_FINALLY_RECEIVING_LOT_NO, itemInfo.getFinallyReceivingLotNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_BEST_DATE_NUM, itemInfo.getBestDateNum());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_BEST_DATE_UNIT, itemInfo.getBestDateUnit());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_INDIVIDUAL_GROSS_TYPE, itemInfo.getIndividualGrossType());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_TOTAL_WGT_MAX, itemInfo.getTotalWgtMax());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_TOTAL_WGT_MIN, itemInfo.getTotalWgtMin());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_REAL_TOTAL_WGT, itemInfo.getRealTotalWgt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_ITEM_ID_CD, itemInfo.getItemIdCd());
	}

	protected void setCargoLotInfoMap(Map<String, Object> map, LotInfoVO lotInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_ORDER_SEQ, lotInfo.getOrderSeq());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_QTY, lotInfo.getQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_REAL_QTY, lotInfo.getRealQty());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_FROM_LOC_LOCAL_CD, lotInfo.getFromLocLocalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_TO_LOC_LOCAL_CD, lotInfo.getToLocLocalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_FROM_LOC_GLOBAL_CD, lotInfo.getFromLocGlobalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_TO_LOC_GLOBAL_CD, lotInfo.getToLocGlobalCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_EXPIRY_DT, lotInfo.getExpiryDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_EXPIRY_DT_TYPE, lotInfo.getExpiryDtType());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_MAKE_DT, lotInfo.getMakeDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_NO, lotInfo.getLotNo());
	}

	protected void setCargoItemSerailInfoMap(Map<String, Object> map, ItemSerialInfoVO itemSerialInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_ITEM_SERIAL, itemSerialInfo.getItemSerial());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_ALERT_REASON_CD, itemSerialInfo.getAlertReasonCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_RFID_READ_DT, itemSerialInfo.getRfidReadDt());
	}
	
	protected void setCargoItemSerailInfoMapKR(Map<String, Object> map, ItemSerialInfoVOKR itemSerialInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_ITEM_SERIAL, itemSerialInfo.getItemSerial());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_RFID_READ_DT, itemSerialInfo.getRfidReadDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_CHILD_CUST_LOT_NO, itemSerialInfo.getChildCustLotNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_BAR_CD, itemSerialInfo.getMapItemBarCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_QTY, itemSerialInfo.getMapItemQty());
	}
	
	protected void setCargoItemSerailInfoMapKRDate(Map<String, Object> map, ItemSerialInfoVOKRDate itemSerialInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_ITEM_SERIAL, itemSerialInfo.getItemSerial());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_RFID_READ_DT, itemSerialInfo.getRfidReadDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_CHILD_CUST_LOT_NO, itemSerialInfo.getChildCustLotNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_BAR_CD, itemSerialInfo.getMapItemBarCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_QTY, itemSerialInfo.getMapItemQty());
	}

	protected void setGoodsResultInfoMapHOWSER(Map<String, Object> map, GoodsResultVO howserGoodsInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_GOODS_SRL, howserGoodsInfo.getGoodsSrl());
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_AMOUNT, howserGoodsInfo.getAmount());
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_ORIGIN_CODE, howserGoodsInfo.getOriginCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_RESULT_TYPE, howserGoodsInfo.getResultType());
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_CAUSE_TYPE, howserGoodsInfo.getCauseType());
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_CAUSE_DESCR, howserGoodsInfo.getCauseDescr());
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_RENTAL_SRL, howserGoodsInfo.getRentalSrl());
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_RENTAL_SRL_IMG, howserGoodsInfo.getRentalSrlImg());
	}

	protected void setDeliveryManUsersInfoMapHOWSER(Map<String, Object> map, DeliveryManUsersVO howserUserInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_USERS_ID, howserUserInfo.getId());
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_USERS_NAME, howserUserInfo.getName());
	}

	protected void setOnsiteImagesInfoMapHOWSER(Map<String, Object> map, String OnsiteImages) {
		map.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_ONSITE_IMAGES, OnsiteImages);
	}
	
	protected void setWorkInfoMapGodomall(Map<String, Object> map, WorkListBaseVOGodomall workInfo) {
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_NO, workInfo.getOrderNo());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MEM_NO, workInfo.getMemNo());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_STATUS, workInfo.getOrderStatus());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_IP, workInfo.getOrderIp());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_CHANNEL_FL, workInfo.getOrderChannelFl());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_TYPE_FL, workInfo.getOrderTypeFl());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_EMAIL, workInfo.getOrderEmail());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_NM, workInfo.getOrderGoodsNm());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_CNT, workInfo.getOrderGoodsCnt());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_SETTLE_PRICE, workInfo.getSettlePrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TAX_SUPPLY_PRICE, workInfo.getTaxSupplyPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TAX_VAT_PRICE, workInfo.getTaxVatPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TAX_FREE_PRICE, workInfo.getTaxFreePrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REAL_TAX_SUPPLY_PRICE, workInfo.getRealTaxSupplyPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REAL_TAX_FREE_PRICE, workInfo.getRealTaxFreePrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_USE_MILEAGE, workInfo.getUseMileage());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_USE_DEPOSIT, workInfo.getUseDeposit());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_GOODS_PRICE, workInfo.getTotalGoodsPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_DELIVERY_CHARGE, workInfo.getTotalDeliveryCharge());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_GOODS_DC_PRICE, workInfo.getTotalGoodsDcPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_MEMBER_OVERLAP_DC_PRICE, workInfo.getTotalMemberOverlapDcPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_GOODS_DC_PRICE, workInfo.getTotalCouponGoodsDcPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_ORDER_DC_PRICE, workInfo.getTotalCouponOrderDcPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_DELIVERY_DC_PRICE, workInfo.getTotalCouponDeliveryDcPrice());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_MILEAGE, workInfo.getTotalMileage());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_GOODS_MILEAGE, workInfo.getTotalGoodsMileage());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_MEMBER_MILEAGE, workInfo.getTotalMemberMileage());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_GOODS_MILEAGE, workInfo.getTotalCouponGoodsMileage());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_ORDER_MILEAGE, workInfo.getTotalCouponOrderMileage());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_FIRST_SALE_FL, workInfo.getFirstSaleFl());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_SETTLE_KIND, workInfo.getSettleKind());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_PAYMENT_DT, workInfo.getPaymentDt());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_DATE, workInfo.getOrderDate());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_API_ORDER_NO, workInfo.getApiOrderNo());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ADD_FIELD, workInfo.getAddField());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MULTI_SHIPPING_FL, workInfo.getMultiShippingFl());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_BANK_NAME, workInfo.getBankName());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ACCOUNT_NUMBER, workInfo.getAccountNumber());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DEPOSITOR, workInfo.getDepositor());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MEM_ID, workInfo.getMemId());
	    map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MEM_GROUP_NM, workInfo.getMemGroupNm());
	}
	
	protected void setOrderDeliveryDataInfoMap(Map<String, Object> map, OrderDeliveryDataVO orderDeliveryDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_SNO, orderDeliveryDataInfo.getSno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_SCM_NO, orderDeliveryDataInfo.getScmNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_COMMISSION, orderDeliveryDataInfo.getCommission());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_SCM_ADJUST_NO, orderDeliveryDataInfo.getScmAdjustNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_SCM_ADJUST_AFTER_NO, orderDeliveryDataInfo.getScmAdjustAfterNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_CHARGE, orderDeliveryDataInfo.getDeliveryCharge());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_POLICY_CHARGE, orderDeliveryDataInfo.getDeliveryPolicyCharge());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_AREA_CHARGE, orderDeliveryDataInfo.getDeliveryAreaCharge());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DIVISION_DELIVERY_USE_DEPOSIT, orderDeliveryDataInfo.getDivisionDeliveryUseDeposit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DIVISION_DELIVERY_CHARGE, orderDeliveryDataInfo.getDivisionDeliveryCharge());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DIVISION_MEMBER_DELIVERY_DC_PRICE, orderDeliveryDataInfo.getDivisionMemberDeliveryDcPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_INSURANCE_FEE, orderDeliveryDataInfo.getDeliveryInsuranceFee());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_WEIGHT_INFO, orderDeliveryDataInfo.getDeliveryWeightInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_OVERSEAS_DELIVERY_POLICY, orderDeliveryDataInfo.getOverseasDeliveryPolicy());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_COLLECT_PRICE, orderDeliveryDataInfo.getDeliveryCollectPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_WHOLE_FREE_PRICE, orderDeliveryDataInfo.getDeliveryWholeFreePrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_STATISTICS_ORDER_FL, orderDeliveryDataInfo.getStatisticsOrderFl());
	}
	
	protected void setOrderInfoDataInfoMap(Map<String, Object> map, OrderInfoDataVO orderInfoDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_NAME, orderInfoDataInfo.getOrderName());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_EMAIL, orderInfoDataInfo.getOrderEmail());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_PHONE_PREFIX_CODE, orderInfoDataInfo.getOrderPhonePrefixCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_PHONE_PREFIX, orderInfoDataInfo.getOrderPhonePrefix());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_PHONE, orderInfoDataInfo.getOrderPhone());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_CELL_PHONE_PREFIX_CODE, orderInfoDataInfo.getOrderCellPhonePrefixCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_CELL_PHONE_PREFIX, orderInfoDataInfo.getOrderCellPhonePrefix());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_CELL_PHONE, orderInfoDataInfo.getOrderCellPhone());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_STATE, orderInfoDataInfo.getOrderState());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_CITY, orderInfoDataInfo.getOrderCity());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_ADDRESS, orderInfoDataInfo.getOrderAddress());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_ADDRESS_SUB, orderInfoDataInfo.getOrderAddressSub());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_NAME, orderInfoDataInfo.getReceiverName());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_COUNTRY_CODE, orderInfoDataInfo.getReceiverCountryCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_PHONE_PREFIX_CODE, orderInfoDataInfo.getReceiverPhonePrefixCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_PHONE_PREFIX, orderInfoDataInfo.getReceiverPhonePrefix());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_PHONE, orderInfoDataInfo.getReceiverPhone());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_CELL_PHONE_PREFIX, orderInfoDataInfo.getReceiverCellPhonePrefix());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_CELL_PHONE, orderInfoDataInfo.getReceiverCellPhone());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_USE_SAFE_NUMBER_FL, orderInfoDataInfo.getReceiverUseSafeNumberFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_SAFE_NUMBER, orderInfoDataInfo.getReceiverSafeNumber());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_SAFE_NUMBER_DT, orderInfoDataInfo.getReceiverSafeNumberDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_COUNTRY, orderInfoDataInfo.getReceiverCountry());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_STATE, orderInfoDataInfo.getReceiverState());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_CITY, orderInfoDataInfo.getReceiverCity());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_ADDRESS, orderInfoDataInfo.getReceiverAddress());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_ADDRESS_SUB, orderInfoDataInfo.getReceiverAddressSub());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_DELIVERY_VISIT, orderInfoDataInfo.getDeliveryVisit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_VISIT_ADDRESS, orderInfoDataInfo.getVisitAddress());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_VISIT_NAME, orderInfoDataInfo.getVisitName());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_VISIT_PHONE, orderInfoDataInfo.getVisitPhone());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_VISIT_MEMO, orderInfoDataInfo.getVisitMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_MEMO, orderInfoDataInfo.getOrderMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_PACKET_CODE, orderInfoDataInfo.getPacketCode());

	}
	
	protected void setOrderGoodsDataInfoMap(Map<String, Object> map, OrderGoodsDataVO orderGoodsDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_IDX, orderGoodsDataInfo.getIdx());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_SNO, orderGoodsDataInfo.getSno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_NO, orderGoodsDataInfo.getOrderNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MALL_SNO, orderGoodsDataInfo.getMallSno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_API_ORDER_GOODS_NO, orderGoodsDataInfo.getApiOrderGoodsNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_CD, orderGoodsDataInfo.getOrderCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_GROUP_CD, orderGoodsDataInfo.getOrderGroupCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_EVENT_SNO, orderGoodsDataInfo.getEventSno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_STATUS, orderGoodsDataInfo.getOrderStatus());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_DELIVERY_SNO, orderGoodsDataInfo.getOrderDeliverySno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_INVOICE_COMPANY_SNO, orderGoodsDataInfo.getInvoiceCompanySno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_INVOICE_NO, orderGoodsDataInfo.getInvoiceNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_SCM_NO, orderGoodsDataInfo.getScmNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PURCHASE_NO, orderGoodsDataInfo.getPurchaseNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_COMMISSION, orderGoodsDataInfo.getCommission());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_SCM_ADJUST_AFTER_NO, orderGoodsDataInfo.getScmAdjustAfterNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_TYPE, orderGoodsDataInfo.getGoodsType());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_TIME_SALE_FL, orderGoodsDataInfo.getTimeSaleFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PARENT_MUST_FL, orderGoodsDataInfo.getParentMustFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PARENT_GOODS_NO, orderGoodsDataInfo.getParentGoodsNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_NO, orderGoodsDataInfo.getGoodsNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_LIST_IMAGE_DATA, orderGoodsDataInfo.getListImageData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_CD, orderGoodsDataInfo.getGoodsCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_MODEL_NO, orderGoodsDataInfo.getGoodsModelNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_NM, orderGoodsDataInfo.getGoodsNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_NM_STANDARD, orderGoodsDataInfo.getGoodsNmStandard());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_CNT, orderGoodsDataInfo.getGoodsCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODSPRICE, orderGoodsDataInfo.getGoodsPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_USE_DEPOSIT, orderGoodsDataInfo.getDivisionUseDeposit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_USE_MILEAGE, orderGoodsDataInfo.getDivisionUseMileage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_GOODS_DELIVERY_USE_DEPOSIT, orderGoodsDataInfo.getDivisionGoodsDeliveryUseDeposit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_GOODS_DELIVERY_USE_MILEAGE, orderGoodsDataInfo.getDivisionGoodsDeliveryUseMileage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_COUPON_ORDER_DCPRICE, orderGoodsDataInfo.getDivisionCouponOrderDcPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_COUPON_ORDER_MILEAGE, orderGoodsDataInfo.getDivisionCouponOrderMileage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ADD_GOODSPRICE, orderGoodsDataInfo.getAddGoodsPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTIONPRICE, orderGoodsDataInfo.getOptionPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_COSTPRICE, orderGoodsDataInfo.getOptionCostPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_TEXTPRICE, orderGoodsDataInfo.getOptionTextPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_FIXEDPRICE, orderGoodsDataInfo.getFixedPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_COSTPRICE, orderGoodsDataInfo.getCostPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_DCPRICE, orderGoodsDataInfo.getGoodsDcPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MEMBER_DCPRICE, orderGoodsDataInfo.getMemberDcPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MEMBER_OVERLAP_DCPRICE, orderGoodsDataInfo.getMemberOverlapDcPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_COUPON_GOODS_DCPRICE, orderGoodsDataInfo.getCouponGoodsDcPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_TIME_SALEPRICE, orderGoodsDataInfo.getTimeSalePrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_BRAND_BANK_SALEPRICE, orderGoodsDataInfo.getBrandBankSalePrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MYAPP_DCPRICE, orderGoodsDataInfo.getMyappDcPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_DELIVERY_COLLECTPRICE, orderGoodsDataInfo.getGoodsDeliveryCollectPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_MILEAGE, orderGoodsDataInfo.getGoodsMileage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MEMBER_MILEAGE, orderGoodsDataInfo.getMemberMileage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_COUPON_GOODS_MILEAGE, orderGoodsDataInfo.getCouponGoodsMileage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_DELIVERY_COLLECT_FL, orderGoodsDataInfo.getGoodsDeliveryCollectFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_DEPOSIT_FL, orderGoodsDataInfo.getMinusDepositFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_RESTORE_DEPOSIT_FL, orderGoodsDataInfo.getMinusRestoreDepositFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_MILEAGE_FL, orderGoodsDataInfo.getMinusMileageFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_RESTORE_MILEAGE_FL, orderGoodsDataInfo.getMinusRestoreMileageFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PLUS_MILEAGE_FL, orderGoodsDataInfo.getPlusMileageFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PLUS_RESTORE_MILEAGE_FL, orderGoodsDataInfo.getPlusRestoreMileageFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_STOCK_FL, orderGoodsDataInfo.getMinusStockFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_RESTORE_STOCK_FL, orderGoodsDataInfo.getMinusRestoreStockFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_SNO, orderGoodsDataInfo.getOptionSno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_INFO, orderGoodsDataInfo.getOptionInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_TEXT_INFO, orderGoodsDataInfo.getOptionTextInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_CATE_ALL_CD, orderGoodsDataInfo.getCateAllCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_HSCODE, orderGoodsDataInfo.getHscode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_CANCEL_DT, orderGoodsDataInfo.getCancelDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PAYMENT_DT, orderGoodsDataInfo.getPaymentDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_INVOICE_DT, orderGoodsDataInfo.getInvoiceDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DELIVERY_DT, orderGoodsDataInfo.getDeliveryDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DELIVERY_COMPLETE_DT, orderGoodsDataInfo.getDeliveryCompleteDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_FINISH_DT, orderGoodsDataInfo.getFinishDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MILEAGE_GIVE_DT, orderGoodsDataInfo.getMileageGiveDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_CHECKOUT_DATA, orderGoodsDataInfo.getCheckoutData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_STATISTICS_ORDER_FL, orderGoodsDataInfo.getStatisticsOrderFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_STATISTICS_GOODS_FL, orderGoodsDataInfo.getStatisticsGoodsFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_SEND_SMS_FL, orderGoodsDataInfo.getSendSmsFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DELIVERY_METHOD_FL, orderGoodsDataInfo.getDeliveryMethodFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ENURI, orderGoodsDataInfo.getEnuri());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_DISCOUNT_INFO, orderGoodsDataInfo.getGoodsDiscountInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_MILEAGE_ADD_INFO, orderGoodsDataInfo.getGoodsMileageAddInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_INFLOW, orderGoodsDataInfo.getInflow());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_LINK_MAIN_THEME, orderGoodsDataInfo.getLinkMainTheme());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_VISIT_ADDRESS, orderGoodsDataInfo.getVisitAddress());
	}
	
	protected void setClaimDataInfoMap(Map<String, Object> map, ClaimDataVO claimDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_USE_MILEAGE, claimDataInfo.getRefundUseMileage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_MODE, claimDataInfo.getHandleMode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_REASON, claimDataInfo.getHandleReason());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_PRICE, claimDataInfo.getRefundPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_USE_DEPOSIT_COMMISSION, claimDataInfo.getRefundUseDepositCommission());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_CHARGE, claimDataInfo.getRefundDeliveryCharge());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_DT, claimDataInfo.getHandleDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_COUPON, claimDataInfo.getRefundDeliveryCoupon());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_INSURANCE_FEE, claimDataInfo.getRefundDeliveryInsuranceFee());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_USE_MILEAGE, claimDataInfo.getRefundDeliveryUseMileage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_BEFORE_STATUS, claimDataInfo.getBeforeStatus());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_CHARGE, claimDataInfo.getRefundCharge());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_COMPLETE_FL, claimDataInfo.getHandleCompleteFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_GROUP_CD, claimDataInfo.getHandleGroupCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_DETAIL_REASON, claimDataInfo.getHandleDetailReason());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_DETAIL_REASON_SHOW_FL, claimDataInfo.getHandleDetailReasonShowFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_USE_DEPOSIT, claimDataInfo.getRefundDeliveryUseDeposit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REG_DT, claimDataInfo.getRegDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_USE_DEPOSIT, claimDataInfo.getRefundUseDeposit());
	}
	
	protected void setWorkInfoMapGodomallItem(Map<String, Object> map, WorkListBaseVOGodomallItem workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NO , workInfo.getGoodsNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM , workInfo.getGoodsNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_FL , workInfo.getGoodsNmFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_MAIN , workInfo.getGoodsNmMain());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_LIST , workInfo.getGoodsNmList());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_DETAIL , workInfo.getGoodsNmDetail());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_PARTNER , workInfo.getGoodsNmPartner());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_GOODS_NM , workInfo.getPurchaseGoodsNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISPLAY_FL , workInfo.getGoodsDisplayFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISPLAY_MOBILE_FL , workInfo.getGoodsDisplayMobileFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_SELL_FL , workInfo.getGoodsSellFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_SELL_MOBIL_FL , workInfo.getGoodsSellMobilFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SCM_NO , workInfo.getScmNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_APPLY_TYPE , workInfo.getApplyType());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_APPLY_MSG , workInfo.getApplyMsg());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_APPLY_DT , workInfo.getApplyDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_COMMISSION , workInfo.getCommission());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_CD , workInfo.getGoodsCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_CAT_CD , workInfo.getCatCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ALL_CATE_CD , workInfo.getAllCateCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_SEARCH_WORD , workInfo.getGoodsSearchWord());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_OPEN_DT , workInfo.getGoodsOpenDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_STATE , workInfo.getGoodsState());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_COLOR , workInfo.getGoodsColor());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_BRAND_CD , workInfo.getBrandCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAKER_NM , workInfo.getMakerNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ORIGIN_NM , workInfo.getOriginNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_MODEL_NO , workInfo.getGoodsModelNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAKE_YMD , workInfo.getMakeYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_LAUNCH_YMD , workInfo.getLaunchYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EFFECTIVE_START_YMD , workInfo.getEffectiveStartYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EFFECTIVE_END_YMD , workInfo.getEffectiveEndYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_PERMISSION , workInfo.getGoodsPermission());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_PERMISSION_GROUP , workInfo.getGoodsPermissionGroup());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ONLY_ADULT_FL , workInfo.getOnlyAdultFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_IMAGE_STORAGE , workInfo.getImageStorage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TAX_FREE_FL , workInfo.getTaxFreeFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TAX_PERCENT , workInfo.getTaxPercent());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_WEIGHT , workInfo.getGoodsWeight());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TOTAL_STOCK , workInfo.getTotalStock());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_STOCK_FL , workInfo.getStockFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SOLD_OUT_FL , workInfo.getSoldOutFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SALES_UNIT , workInfo.getSalesUnit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MIN_ORDER_CNT , workInfo.getMinOrderCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAX_ORDER_CNT , workInfo.getNaxOrderCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SALES_START_YMD , workInfo.getSalesStartYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SALES_END_YMD , workInfo.getSalesEndYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_RESTOCK_FL , workInfo.getRestockFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MILEAGE_FL , workInfo.getMileageFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MILEAGE_GOODS , workInfo.getMileageGoods());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MILEAGE_GOODS_UNIT , workInfo.getMileageGoodsUnit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISCOUNT_FL , workInfo.getGoodsDiscountFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISCOUNT , workInfo.getGoodsDiscount());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISCOUNT_UNIT , workInfo.getGoodsDiscountUnit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PAY_LIMIT_FL , workInfo.getPayLimitFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PAY_LIMIT , workInfo.getPayLimit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_PRICE_STRING , workInfo.getGoodsPriceString());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_PRICE , workInfo.getGoodsPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_FIXED_PRICE , workInfo.getFixedPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_COST_PRICE , workInfo.getCostPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_FL , workInfo.getOptionFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_DISPLAY_FL , workInfo.getOptionDisplayFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_NAME , workInfo.getOptionName());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_TEXT_FL , workInfo.getOptionTextFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ADD_GOODS_FL , workInfo.getAddGoodsFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SHORT_DESCRIPTION , workInfo.getShortDescription());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DESCRIPTION , workInfo.getGoodsDescription());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DESCRIPTION_MOBILE , workInfo.getGoodsDescriptionMobile());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DELIVERY_SNO , workInfo.getDeliverySno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_RELATION_FL , workInfo.getRelationFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_RELATION_SAME_FL , workInfo.getRelationSameFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_RELATION_GOODS_NO , workInfo.getRelationGoodsNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_ICON_START_YMD , workInfo.getGoodsIconStartYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_ICON_END_YMD , workInfo.getGoodsIconEndYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_ICON_CD_PERIOD , workInfo.getGoodsIconCdPeriod());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_ICON_CD , workInfo.getGoodsIconCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_IMG_DETAIL_VIEW_FL , workInfo.getImgDetailViewFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXTERNAL_VIDEO_FL , workInfo.getExternalVideoFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXTERNAL_VIDEO_URL , workInfo.getExternalVideoUrl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXTERNAL_VIDEO_WIDTH , workInfo.getExternalVideoWidth());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXTERNAL_VIDEOHEIGHT , workInfo.getExternalVideoHeight());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_INFO_DELIVERY , workInfo.getDetailInfoDelivery());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_INFO_A_S , workInfo.getDetailInfoAS());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_INFO_REFUND , workInfo.getDetailInfoRefund());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_INFO_EXCHANGE , workInfo.getDetailInfoExchange());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MEMO , workInfo.getMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ORDER_CNT , workInfo.getOrderCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_HIT_CNT , workInfo.getHitCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_REVIEW_CNT , workInfo.getReviewCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXCEL_FL , workInfo.getExcelFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_REG_DT , workInfo.getRegDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MOD_DT , workInfo.getModDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_DATA , workInfo.getOptionData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_DATA , workInfo.getTextOptionData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ADD_GOODS_DATA , workInfo.getAddGoodsData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAGNIFY_IMAGE_DATA , workInfo.getMagnifyImageData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_IMAGE_DATA , workInfo.getDetailImageData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_LIST_IMAGE_DATA , workInfo.getListImageData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAIN_IMAGE_DATA , workInfo.getMainImageData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_MUST_INFO_DATA , workInfo.getGoodsMustInfoData());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_NM , workInfo.getPurchaseNm());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_CATAGORY , workInfo.getPurchaseCatagory());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_PHONE , workInfo.getPurchasePhone());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_ADDRESS , workInfo.getPurchaseAddress());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_ADDRESS_SUB , workInfo.getPurchaseAddressSub());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_MEMO , workInfo.getPurchaseMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_CULTURE_BENEFIT_FL , workInfo.getCultureBenefitFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EVENT_DESCRIPTION , workInfo.getEventDescription());
	}
	
	protected void setWorkInfoMapShopbuy(Map<String, Object> map, WorkListBaseVOShopbuy workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_YMDT,workInfo.getOrderYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_NO,workInfo.getOrderNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_MALL_NO,workInfo.getMallNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDERER_NAME,workInfo.getOrdererName());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_MEMBER_ID,workInfo.getMemberId());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_OAUTH_ID_NO,workInfo.getOauthIdNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDERER_CONTACT1,workInfo.getOrdererContact1());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDERER_CONTACT2,workInfo.getOrdererContact2());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_MEMO,workInfo.getOrderMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_FIRST_CART_COUPON_DISCOUNT_AMT,workInfo.getFirstCartCouponDiscountAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_FIRST_PAY_AMT,workInfo.getFirstPayAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_FIRST_MAIN_PAY_AMT,workInfo.getFirstMainPayAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_FIRST_SUB_PAY_AMT,workInfo.getFirstSubPayAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_LAST_CART_COUPON_DISCOUNT_AMT,workInfo.getLastCartCouponDiscountAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_LAST_PAY_AMT,workInfo.getLastPayAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_LAST_MAIN_PAY_AMT,workInfo.getLastMainPayAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_LAST_SUB_PAY_AMT,workInfo.getLastSubPayAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_EXTRA_DATA,workInfo.getExtraData());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_MEMBER_ADDITIONAL_INFO_JSON,workInfo.getMemberAdditionalInfoJson());
	}
	
	protected void setWorkInfoMapShopbuyItem(Map<String, Object> map, WorkListBaseVOShopbuyItem workInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ACCUMULATION_RATE,workInfo.getAccumulationRate());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ACCUMULATION_USE_YN,workInfo.getAccumulationUseYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ADD_OPTION_IMAGE_YN,workInfo.getAddOptionImageYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ADDITIONAL_DISCOUNT_YN,workInfo.getAdditionalDiscountYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ADMIN_NO,workInfo.getAdminNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_APPLY_STATUS_TYPE,workInfo.getApplyStatusType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_BRAND_NO,workInfo.getBrandNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CART_OFF_END_YMDT,workInfo.getCartOffEndYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CART_OFF_PERIOD_YN,workInfo.getCartOffPeriodYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CART_OFF_START_YMDT,workInfo.getCartOffStartYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CART_USE_YN,workInfo.getCartUseYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CATEGORY_NO,workInfo.getCategoryNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CERTIFICATION_JSON,workInfo.getCertificationJson());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CERTIFICATION_TYPE,workInfo.getCertificationType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CLASS_TYPE,workInfo.getClassType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_COMMISSION_RATE,workInfo.getCommissionRate());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_COMMISSION_RATE_TYPE,workInfo.getCommissionRateType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_COMPARING_PRICE_SITE,workInfo.getComparingPriceSite());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CONTENT,workInfo.getContent());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CONTENT_FOOTER,workInfo.getContentFooter());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CONTENT_HEADER,workInfo.getContentHeader());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_COUPON_YN,workInfo.getCouponYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELETE_YN,workInfo.getDeleteYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_COMBINATION_YN,workInfo.getDeliveryCombinationYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_CUSTOMER_INFO,workInfo.getDeliveryCustomerInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_INTERNATIONAL_YN,workInfo.getDeliveryInternationalYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_TEMPLATE_NO,workInfo.getDeliveryTemplateNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_YN,workInfo.getDeliveryYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DISPLAY_BRAND_NO,workInfo.getDisplayBrandNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DUTY_INFO,workInfo.getDutyInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_EAN_CODE,workInfo.getEanCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_EXPIRATION_YMDT,workInfo.getExpirationYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_EXTRA_JSON,workInfo.getExtraJson());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_FRONT_DISPLAY_YN,workInfo.getFrontDisplayYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_GROUP_TYPE,workInfo.getGroupType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_HS_CODE,workInfo.getHsCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_APPLY_PRICE,workInfo.getImmediateDiscountApplyPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_END_YMDT,workInfo.getImmediateDiscountEndYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_PERIOD_YN,workInfo.getImmediateDiscountPeriodYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_START_YMDT,workInfo.getImmediateDiscountStartYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_UNIT_TYPE,workInfo.getImmediateDiscountUnitType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_VALUE,workInfo.getImmediateDiscountValue());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IS_OPTION_USED,workInfo.getIsOptionUsed());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ITEM_YN,workInfo.getItemYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_KEYWORD,workInfo.getKeyword());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MALL_NO,workInfo.getMallNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MALL_PRODUCT_NO,workInfo.getMallProductNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MANUFACTURE_YMDT,workInfo.getManufactureYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAPPING_KEY,workInfo.getMappingKey());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MASTER_YN,workInfo.getMasterYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAX_BUY_DAYS,workInfo.getMaxBuyDays());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAX_BUY_PERIOD_CNT,workInfo.getMaxBuyPeriodCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAX_BUY_PERSON_CNT,workInfo.getMaxBuyPersonCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAX_BUY_TIME_CNT,workInfo.getMaxBuyTimeCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MEMBER_GRADE_DISPLAY_INFO,workInfo.getMemberGradeDisplayInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MEMBER_GROUP_DISPLAY_INFO,workInfo.getMemberGroupDisplayInfo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MIN_BUY_CNT,workInfo.getMinBuyCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MINOR_PURCHASE_YN,workInfo.getMinorPurchaseYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_NONMEMBER_PURCHASE_YN,workInfo.getNonmemberPurchaseYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PARENT_MALL_PRODUCT_NO,workInfo.getParentMallProductNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PARTNER_CHARGE_AMT,workInfo.getPartnerChargeAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PARTNER_NAME,workInfo.getPartnerName());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PARTNER_NO,workInfo.getPartnerNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PAYMENT_MEANS,workInfo.getPaymentMeans());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PAYMENT_MEANS_CONTROL_YN,workInfo.getPaymentMeansControlYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLACE_ORIGIN,workInfo.getPlaceOrigin());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLACE_ORIGIN_SEQ,workInfo.getPlaceOriginSeq());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLACE_ORIGINS_YN,workInfo.getPlaceOriginsYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLATFORM_DISPLAY_MOBILE_WEB_YN,workInfo.getPlatformDisplayMobileWebYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLATFORM_DISPLAY_MOBILE_YN,workInfo.getPlatformDisplayMobileYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLATFORM_DISPLAY_PC_YN,workInfo.getPlatformDisplayPcYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLATFORM_DISPLAY_YN,workInfo.getPlatformDisplayYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_POINT_RATE,workInfo.getPointRate());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_MANAGEMENT_CD,workInfo.getProductManagementCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_NAME,workInfo.getProductName());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_NAME_EN,workInfo.getProductNameEn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_NO,workInfo.getProductNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_TYPE,workInfo.getProductType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_TEXT,workInfo.getPromotionText());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_TEXT_END_YMDT,workInfo.getPromotionTextEndYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_TEXT_START_YMDT,workInfo.getPromotionTextStartYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_TEXT_YN,workInfo.getPromotionTextYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_YN,workInfo.getPromotionYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_REFUNDABLE_YN,workInfo.getRefundableYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_REGISTER_ADMIN_NO,workInfo.getRegisterAdminNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_REGISTER_SITE_TYPE,workInfo.getRegisterSiteType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_REGISTER_YMDT,workInfo.getRegisterYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_END_YMD,workInfo.getSaleEndYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_END_YMDT,workInfo.getSaleEndYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_METHOD_TYPE,workInfo.getSaleMethodType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_PERIOD_TYPE,workInfo.getSalePeriodType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_PRICE,workInfo.getSalePrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_START_YMD,workInfo.getSaleStartYmd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_START_YMDT,workInfo.getSaleStartYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_STATUS_TYPE,workInfo.getSaleStatusType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SEARCHENGINE_DISPLAY_YN,workInfo.getSearchengineDisplayYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SHIPPING_AREA_PARTNER_NO,workInfo.getShippingAreaPartnerNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SHIPPING_AREA_TYPE,workInfo.getShippingAreaType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SYNC_WMS_YN,workInfo.getSyncWmsYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_TEMP_SAVE,workInfo.getTempSave());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UNIT_NAME,workInfo.getUnitName());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UNIT_NAME_TYPE,workInfo.getUnitNameType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UNIT_PRICE,workInfo.getUnitPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UPDATE_ADMIN_NO,workInfo.getUpdateAdminNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UPDATE_YMDT,workInfo.getUpdateYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_URL_DIRECT_DISPLAY_YN,workInfo.getUrlDirectDisplayYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_VALUE_ADDED_TAX_TYPE,workInfo.getValueAddedTaxType());

	}
	
	
	protected void setAddGoodsDataInfoMap(Map<String, Object> map, AddGoodsDataVO addGoodsDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RTI_SERIAL, addGoodsDataInfo.getRtiSerial());
	}
	
	protected void setGiftDataInfoMap(Map<String, Object> map, GiftDataVO giftDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RTI_SERIAL, giftDataInfo.getRtiSerial());
	}
	
	protected void setExchangeInfoMap(Map<String, Object> map, ExchangeInfoDataVO exchangeInfoDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RTI_SERIAL, exchangeInfoDataInfo.getRtiSerial());
	}
	
	protected void setOrderConsultDataInfoMap(Map<String, Object> map, OrderConsultDataVO orderConsultDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RTI_SERIAL, orderConsultDataInfo.getRtiSerial());
	}
	
	protected void setAddGoodsSnoDataInfoMap(Map<String, Object> map, AddGoodsSnoDataVO addGoodsSnoDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RTI_SERIAL, addGoodsSnoDataInfo.getRtiSerial());
	}
	
	protected void setOptionDataInfoMap(Map<String, Object> map, OptionDataVO optionDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_IDX,  optionDataInfo.getIdx());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_SNO,  optionDataInfo.getSno());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_GOODS_NO,  optionDataInfo.getGoodsNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_NO,  optionDataInfo.getOptionNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_CODE,  optionDataInfo.getOptionCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE1,  optionDataInfo.getOptionValue1());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE2,  optionDataInfo.getOptionValue2());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE3,  optionDataInfo.getOptionValue3());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE4,  optionDataInfo.getOptionValue4());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE5,  optionDataInfo.getOptionValue5());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_SELL_FL,  optionDataInfo.getOptionSellFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_PRICE,  optionDataInfo.getOptionPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_DELIVERY_CODE,  optionDataInfo.getOptionDeliveryCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_CONFIRM_REQUEST_STOCK,  optionDataInfo.getConfirmRequestStock());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_DELIVERY_FL,  optionDataInfo.getOptionDeliveryFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_REQ_DT,  optionDataInfo.getReqDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_MOD_DT,  optionDataInfo.getModDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_COST_PRICE,  optionDataInfo.getOptionCostPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_SELL_CODE,  optionDataInfo.getOptionSellCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_DELI_VERY_SMS_SENT,  optionDataInfo.getDeliverySmsSent());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VIEW_FL,  optionDataInfo.getOptionViewFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_IMAGE,  optionDataInfo.getOptionImage());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_SELL_STOP_STOCK,  optionDataInfo.getSellStopStock());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_STOCK_CNT,  optionDataInfo.getStockCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_SELL_STOCK_FL,  optionDataInfo.getSellStopFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_MEMO,  optionDataInfo.getOptionMemo());

	}

	protected void setTextOptionDataInfoMap(Map<String, Object> map, TextOptionDataVO textOptionDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_GOODS_NO,  textOptionDataInfo.getGoodsNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_INPUT_LIMIT,  textOptionDataInfo.getInputLimit());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_MUST_FL,  textOptionDataInfo.getMustFl());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_ADD_PRICE,  textOptionDataInfo.getAddPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_OPTION_NAME,  textOptionDataInfo.getOptionName());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_REG_DT,  textOptionDataInfo.getRegDt());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_MOD_DT,  textOptionDataInfo.getModDt());

	}

	protected void setGoodsMustInfoDataInfoMap(Map<String, Object> map, GoodsMustInfoDataVO goodsMustInfoDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_MUST_INFO_IDX,  goodsMustInfoDataInfo.getIdx());

	}
	
	protected void setStepDataInfoMap(Map<String, Object> map, StepDataVO stepDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_STEP_DATA_IDX,  stepDataInfo.getIdx());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_STEP_DATA_INFO_VALUE,  stepDataInfo.getInfoValue());
		map.put(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_STEP_DATA_INFO_TITLE,  stepDataInfo.getInfoTitle());

	}
	
	protected void setDeliveryGroupDataInfoMap(Map<String, Object> map, DeliveryGroupsVO deliveryGroupDataInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_NO,deliveryGroupDataInfo.getDeliveryNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_INVOICE_NO,deliveryGroupDataInfo.getInvoiceNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_INVOICE_REGISTER_YMDT,deliveryGroupDataInfo.getInvoiceRegisterYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_TEMPLATE_GROUP_NO,deliveryGroupDataInfo.getDeliveryTemplateGroupNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_TEMPLATE_NO,deliveryGroupDataInfo.getDeliveryTemplateNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_CONDITION_NO,deliveryGroupDataInfo.getDeliveryConditionNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_AMT,deliveryGroupDataInfo.getDeliveryAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RETURN_DELIVERY_AMT,deliveryGroupDataInfo.getReturnDeliveryAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_REMOTE_DELIVERY_AMT,deliveryGroupDataInfo.getRemoteDeliveryAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_ADJUSTED_AMT,deliveryGroupDataInfo.getAdjustedAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_COMPANY_TYPE,deliveryGroupDataInfo.getDeliveryCompanyType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_NAME,deliveryGroupDataInfo.getReceiverName());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_COUNTRY_CD,deliveryGroupDataInfo.getCountryCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_ZIP_CD,deliveryGroupDataInfo.getReceiverZipCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_ADDRESS,deliveryGroupDataInfo.getReceiverAddress());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_JIBUN_ADDRESS,deliveryGroupDataInfo.getReceiverJibunAddress());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_DETAIL_ADDRESS,deliveryGroupDataInfo.getReceiverDetailAddress());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_CONTACT1,deliveryGroupDataInfo.getReceiverContact1());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_CONTACT2,deliveryGroupDataInfo.getReceiverContact2());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_MEMO,deliveryGroupDataInfo.getDeliveryMemo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_TYPE,deliveryGroupDataInfo.getDeliveryType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_SHIPPING_METHOD_TYPE,deliveryGroupDataInfo.getShippingMethodType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_CUSTOMS_ID_NUMBER,deliveryGroupDataInfo.getCustomsIdNumber());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_AMT_IN_ADVANCE_YN,deliveryGroupDataInfo.getDeliveryAmtInAdvanceYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_YN,deliveryGroupDataInfo.getDeliveryYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_COMBINE_DELIVERY_YN,deliveryGroupDataInfo.getCombineDeliveryYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DIVIDE_DELIVERY_YN,deliveryGroupDataInfo.getDivideDeliveryYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_COMBINATION_YN,deliveryGroupDataInfo.getDeliveryCombinationYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_ORIGINAL_DELIVERY_NO,deliveryGroupDataInfo.getOriginalDeliveryNo());
	}
	
	protected void setOrderProductInfoMap(Map<String, Object> map, OrderProductsVO orderProductInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_ORDER_PRODUCT_NO,orderProductInfo.getOrderProductNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_MALL_PRODUCT_NO,orderProductInfo.getMallProductNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_PRODUCT_NAME,orderProductInfo.getProductName());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_PRODUCT_NAME_EN,orderProductInfo.getProductNameEn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_PRODUCT_MANAGEMENT_CD,orderProductInfo.getProductManagementCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_IMAGE_URL,orderProductInfo.getImageUrl());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_HS_CODE,orderProductInfo.getHsCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_FIRST_PRODUCT_COUPON_DISCOUNT_AMT,orderProductInfo.getFirstProductCouponDiscountAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_LAST_PRODUCT_COUPON_DISCOUNT_AMT,orderProductInfo.getLastProductCouponDiscountAmt());

	}

	protected void setOrderProductOptionsMap(Map<String, Object> map, OrderProductOptionsVO orderProductOptionsInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_PRODUCT_OPTION_NO ,orderProductOptionsInfo.getOrderProductOptionNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_TEMPLATE_NO ,orderProductOptionsInfo.getDeliveryTemplateNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_PARTNER_NO ,orderProductOptionsInfo.getPartnerNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_PARTNER_NO ,orderProductOptionsInfo.getDeliveryPartnerNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_CATEGORY_NO ,orderProductOptionsInfo.getCategoryNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_BRAND_NO ,orderProductOptionsInfo.getBrandNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_MALL_OPTION_NO ,orderProductOptionsInfo.getMallOptionNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_MALL_ADDITIONAL_PRODUCT_NO ,orderProductOptionsInfo.getMallAdditionalProductNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_STOCK_NO ,orderProductOptionsInfo.getStockNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_RELEASE_WAREHOUSE_NO ,orderProductOptionsInfo.getReleaseWarehouseNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_OPTION_USE_YN ,orderProductOptionsInfo.getOptionUseYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_OPTION_NAME ,orderProductOptionsInfo.getOptionName());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_OPTION_VALUE ,orderProductOptionsInfo.getOptionValue());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_OPTION_MANAGEMENT_CD ,orderProductOptionsInfo.getOptionManagementCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_IMAGE_URL ,orderProductOptionsInfo.getImageUrl());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_HS_CODE ,orderProductOptionsInfo.getHsCode());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_CNT ,orderProductOptionsInfo.getOrderCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORIGINAL_ORDER_CNT ,orderProductOptionsInfo.getOriginalOrderCnt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_SALE_PRICE ,orderProductOptionsInfo.getSalePrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_IMMEDIATE_DISCOUNT_AMT ,orderProductOptionsInfo.getImmediateDiscountAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ADD_PRICE ,orderProductOptionsInfo.getAddPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ADDITIONAL_DISCOUNT_AMT ,orderProductOptionsInfo.getAdditionalDiscountAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_PARTNER_CHARGE_AMT ,orderProductOptionsInfo.getPartnerChargeAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ADJUSTED_AMT ,orderProductOptionsInfo.getAdjustedAmt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_COMMISSION_RATE ,orderProductOptionsInfo.getCommissionRate());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_PRODUCT_TYPE ,orderProductOptionsInfo.getProductType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_OPTION_TYPE ,orderProductOptionsInfo.getOrderOptionType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_STATUS_TYPE ,orderProductOptionsInfo.getOrderStatusType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_CLAIM_STATUS_TYPE ,orderProductOptionsInfo.getClaimStatusType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_REFUNDABLE_YN ,orderProductOptionsInfo.getRefundableYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_SHIPPING_AREA_TYPE ,orderProductOptionsInfo.getShippingAreaType());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_INTERNATIONAL_YN ,orderProductOptionsInfo.getDeliveryInternationalYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_HOLD_YN ,orderProductOptionsInfo.getHoldYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_EXCHANGE_YN ,orderProductOptionsInfo.getExchangeYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_USER_INPUT_TEXT ,orderProductOptionsInfo.getUserInputText());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_STATUS_CHANGE_YMDT ,orderProductOptionsInfo.getStatusChangeYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_YMDT ,orderProductOptionsInfo.getOrderYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_PAY_YMDT ,orderProductOptionsInfo.getPayYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_ACCEPT_YMDT ,orderProductOptionsInfo.getOrderAcceptYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_RELEASE_READY_YMDT ,orderProductOptionsInfo.getReleaseReadyYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_RELEASE_YMDT ,orderProductOptionsInfo.getReleaseYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_COMPLETE_YMDT ,orderProductOptionsInfo.getDeliveryCompleteYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_BUY_CONFIRM_YMDT ,orderProductOptionsInfo.getBuyConfirmYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_REGISTER_YMDT ,orderProductOptionsInfo.getRegisterYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_YN ,orderProductOptionsInfo.getDeliveryYn());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_UPDATE_YMDT ,orderProductOptionsInfo.getUpdateYmdt());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORIGIN_ORDER_PRODUCT_OPTION_NO ,orderProductOptionsInfo.getOriginOrderProductOptionNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_EXTRA_JSON ,orderProductOptionsInfo.getExtraJson());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_SKU ,orderProductOptionsInfo.getSku());
	}
	
	protected void setSetOptionInfoMap(Map<String, Object> map, SetOptionsVO setOptionInfo) {
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_COUNT,setOptionInfo.getCount());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_MALL_OPTION_NO,setOptionInfo.getMallProductNo());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_MALL_PRODUCT_NO,setOptionInfo.getOption());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_OPTION,setOptionInfo.getOptionManagementCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_OPTION_MANAGEMENT_CD,setOptionInfo.getOptionPrice());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_OPTION_PRICE,setOptionInfo.getProductManagementCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_PRODUCT_MANAGEMENT_CD,setOptionInfo.getProductManagementCd());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_PRODUCT_NAME,setOptionInfo.getProductName());
		map.put(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_SKU,setOptionInfo.getSku());
	}
	
	protected List<Map<String, Object>> getDataRowFromInterfaceVO(InterfaceBaseVO data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel1 = new HashMap<String, Object>();
			interfaceLevel1.put(ConstantWSIF.IF_KEY_TERMINAL_ID, data.getTerminalId());
			interfaceLevel1.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
			
			List<WorkListBaseVO> workList = data.getWorkList();
			if (workList != null) {
				for (WorkListBaseVO workInfo : workList) {
					HashMap<String, Object> interfaceLevel2 = (HashMap<String, Object>) interfaceLevel1.clone();
					setWorkInfoMap(interfaceLevel2, workInfo);

					List<ItemInfoVO> itemList = workInfo.getItemList();
					List<PackingInfoVO> packingList = workInfo.getPackingList();

					if (itemList != null && itemList.size() > 0) {
						for (ItemInfoVO itemInfo : itemList) {
							HashMap<String, Object> interfaceLevel3 = (HashMap<String, Object>) interfaceLevel2.clone();
							setItemInfoMap(interfaceLevel3, itemInfo);

							List<LotInfoVO> lotList = itemInfo.getLotList();
							if (lotList != null) {
								for (LotInfoVO lotInfo : lotList) {
									HashMap<String, Object> interfaceLevel4 = (HashMap<String, Object>) interfaceLevel3.clone();
									setLotInfoMap(interfaceLevel4, lotInfo);

									List<ItemSerialInfoVO> itemSerialList = lotInfo.getItemSerialList();
									if (itemSerialList != null) {
										for (ItemSerialInfoVO itemSerialInfo : itemSerialList) {
											HashMap<String, Object> interfaceLevel5 = (HashMap<String, Object>) interfaceLevel4.clone();
											setItemSerailInfoMap(interfaceLevel5, itemSerialInfo);
											dataList.add(interfaceLevel5);
										}
									} else {
										dataList.add(interfaceLevel4);
									}
								}
							} else {
								dataList.add(interfaceLevel3);
							}
						}
					} else if (packingList != null && packingList.size() > 0) {

						for (PackingInfoVO packingInfo : packingList) {
							HashMap<String, Object> interfaceLevel3 = (HashMap<String, Object>) interfaceLevel2.clone();
							setPackingInfoMap(interfaceLevel3, packingInfo);

							List<RtiSerialInfoVO> rtiSerialList = packingInfo.getRtiSerialList();
							if (rtiSerialList != null && rtiSerialList.size() > 0) {

								// rti_serial_list 는 1회만 반복(사양확인 2016.01.07
								// 김과장님)
								RtiSerialInfoVO rtiSerialInfo = (RtiSerialInfoVO) rtiSerialList.get(0);
								// rti_serial_list 의 정보를 셋팅
								setRtiSerailInfoMap(interfaceLevel3, rtiSerialInfo);

							}

							CargoInfoVO cargoInfoVO = packingInfo.getCargoInfo();
							if (cargoInfoVO != null) {
								List<RtiInfoVO> rtiList = cargoInfoVO.getRtiList();
								List<ItemInfoVO> itemListCargo = cargoInfoVO.getItemList();

								if (itemListCargo != null && itemListCargo.size() > 0) {
									for (ItemInfoVO itemInfo : itemListCargo) {
										HashMap<String, Object> interfaceLevel4 = (HashMap<String, Object>) interfaceLevel3.clone();
										setCargoItemInfoMap(interfaceLevel4, itemInfo);

										List<LotInfoVO> lotList = itemInfo.getLotList();
										if (lotList != null) {
											for (LotInfoVO lotInfo : lotList) {
												HashMap<String, Object> interfaceLevel5 = (HashMap<String, Object>) interfaceLevel4.clone();
												setCargoLotInfoMap(interfaceLevel5, lotInfo);

												List<ItemSerialInfoVO> itemSerialList = lotInfo.getItemSerialList();
												if (itemSerialList != null) {
													for (ItemSerialInfoVO itemSerialInfo : itemSerialList) {
														HashMap<String, Object> interfaceLevel6 = (HashMap<String, Object>) interfaceLevel5.clone();
														setCargoItemSerailInfoMap(interfaceLevel6, itemSerialInfo);
														dataList.add(interfaceLevel6);
													}
												} else {
													dataList.add(interfaceLevel5);
												}
											}
										} else {
											dataList.add(interfaceLevel4);
										}
									}
								}
									
								if (rtiList != null && rtiList.size() > 0) {
									for (RtiInfoVO rtiInfo : rtiList) {
										HashMap<String, Object> interfaceLevel4 = (HashMap<String, Object>) interfaceLevel3.clone();
										setCargoRtiInfoMap(interfaceLevel4, rtiInfo);
										dataList.add(interfaceLevel4);
									}
								}	
								
							} else {
								dataList.add(interfaceLevel3);
							}
						}

					} else {
						dataList.add(interfaceLevel2);
					}
				}
			} else {
				dataList.add(interfaceLevel1);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVOKR(InterfaceBaseVOKR data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			interfaceLevel11.put(ConstantWSIF.IF_KEY_TERMINAL_ID, data.getTerminalId());
			interfaceLevel11.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
			
			List<WorkListBaseVOKR> workList = data.getWorkList();
			if (workList != null) {
				//log.info("worklist is not null");
				for (WorkListBaseVOKR workInfo : workList) {
					HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
					setWorkInfoMapKR(interfaceLevel12, workInfo);
					//dataList.add(interfaceLevel12);
					
					List<RtiSerialInfoVOKR> rtiSerialList = workInfo.getRtiSerialList();

					if (rtiSerialList != null && rtiSerialList.size() > 0) {
						//log.info("rtiSerialList is not null");
						for (RtiSerialInfoVOKR rtiSerialInfo : rtiSerialList) {
							HashMap<String, Object> interfaceLevel13 = (HashMap<String, Object>) interfaceLevel12.clone();
							setItemInfoMapKR(interfaceLevel13, rtiSerialInfo);
							//log.info("1111111111111111111 is not null");
							List<ItemSerialInfoVOKR> itemSerialList = rtiSerialInfo.getItemSerialList();
							//log.info("2222222222222222222 is not null");
							if (itemSerialList != null) {
								//log.info("itemSerialList is not null");
								for (ItemSerialInfoVOKR itemSerialInfo : itemSerialList) {
									HashMap<String, Object> interfaceLevel14 = (HashMap<String, Object>) interfaceLevel13.clone();
									setCargoItemSerailInfoMapKR(interfaceLevel14, itemSerialInfo);
									dataList.add(interfaceLevel14);
								}
							} else {
								dataList.add(interfaceLevel13);
							}
						}
					}
					else {
						dataList.add(interfaceLevel12);
					}
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVOKRDate(InterfaceBaseVOKRDate data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			interfaceLevel11.put(ConstantWSIF.IF_KEY_TERMINAL_ID, data.getTerminalId());
			interfaceLevel11.put(ConstantWSIF.IF_KEY_LOGIN_ID, data.getLoginId());
			
			List<WorkListBaseVOKRDate> workList = data.getWorkList();
			if (workList != null) {
				//log.info("worklist is not null");
				for (WorkListBaseVOKRDate workInfo : workList) {
					HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
					setWorkInfoMapKRDate(interfaceLevel12, workInfo);
					//dataList.add(interfaceLevel12);
					
					List<RtiSerialInfoVOKRDate> rtiSerialList = workInfo.getRtiSerialList();

					if (rtiSerialList != null && rtiSerialList.size() > 0) {
						//log.info("rtiSerialList is not null");
						for (RtiSerialInfoVOKRDate rtiSerialInfo : rtiSerialList) {
							HashMap<String, Object> interfaceLevel13 = (HashMap<String, Object>) interfaceLevel12.clone();
							setItemInfoMapKRDate(interfaceLevel13, rtiSerialInfo);
							//log.info("1111111111111111111 is not null");
							List<ItemSerialInfoVOKRDate> itemSerialList = rtiSerialInfo.getItemSerialList();
							//log.info("2222222222222222222 is not null");
							if (itemSerialList != null) {
								//log.info("itemSerialList is not null");
								for (ItemSerialInfoVOKRDate itemSerialInfo : itemSerialList) {
									HashMap<String, Object> interfaceLevel14 = (HashMap<String, Object>) interfaceLevel13.clone();
									setCargoItemSerailInfoMapKRDate(interfaceLevel14, itemSerialInfo);
									dataList.add(interfaceLevel14);
								}
							} else {
								dataList.add(interfaceLevel13);
							}
						}
					}
					else {
						dataList.add(interfaceLevel12);
					}
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}
	
	protected List<Map<String, Object>> getDataRowFromInterfaceVOForkLift(InterfaceBaseVOForklift data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			List<WorkListBaseVOForklift> workList = data.getWorkList();
			if (workList != null) {
				//log.info("worklist is not null======:"+workList);
				for (WorkListBaseVOForklift workInfo : workList) {
					HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
					setWorkInfoMapForklift(interfaceLevel12, workInfo);
					dataList.add(interfaceLevel12);
					
//					List<RtiSerialInfoVOKR> rtiSerialList = workInfo.getRtiSerialList();
//
//					if (rtiSerialList != null && rtiSerialList.size() > 0) {
//						//log.info("rtiSerialList is not null");
//						for (RtiSerialInfoVOKR rtiSerialInfo : rtiSerialList) {
//							HashMap<String, Object> interfaceLevel13 = (HashMap<String, Object>) interfaceLevel12.clone();
//							setItemInfoMapKR(interfaceLevel13, rtiSerialInfo);
//							//log.info("1111111111111111111 is not null");
//							List<ItemSerialInfoVOKR> itemSerialList = rtiSerialInfo.getItemSerialList();
//							//log.info("2222222222222222222 is not null");
//							if (itemSerialList != null) {
//								//log.info("itemSerialList is not null");
//								for (ItemSerialInfoVOKR itemSerialInfo : itemSerialList) {
//									HashMap<String, Object> interfaceLevel14 = (HashMap<String, Object>) interfaceLevel13.clone();
//									setCargoItemSerailInfoMapKR(interfaceLevel14, itemSerialInfo);
//									dataList.add(interfaceLevel14);
//								}
//							} else {
//								dataList.add(interfaceLevel13);
//							}
//						}
//					}
//					else {
//						dataList.add(interfaceLevel12);
//					}
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVODlv(InterfaceBaseVODlv data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			List<WorkListBaseVODlv> workList = data.getWorkList();
			if (workList != null) {
				//log.info("worklist is not null======:"+workList);
				for (WorkListBaseVODlv workInfo : workList) {
					HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
					setWorkInfoMapDlv(interfaceLevel12, workInfo);
					dataList.add(interfaceLevel12);
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVODlvNew(InterfaceBaseVODlvNew data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			List<WorkListBaseVODlvNew> workList = data.getWorkList();
			if (workList != null) {
				//log.info("worklist is not null======:"+workList);
				for (WorkListBaseVODlvNew workInfo : workList) {
					HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
					setWorkInfoMapDlvNew(interfaceLevel12, workInfo);
					dataList.add(interfaceLevel12);
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVODlv200614(InterfaceBaseVODlv200614 data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			List<WorkListBaseVODlv200614> workList = data.getWorkList();
			if (workList != null) {
				//log.info("worklist is not null======:"+workList);
				for (WorkListBaseVODlv200614 workInfo : workList) {
					HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
					setWorkInfoMapDlv200614(interfaceLevel12, workInfo);
					dataList.add(interfaceLevel12);
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVODlv211231(InterfaceBaseVODlv211231 data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			List<WorkListBaseVODlv211231> workList = data.getWorkList();
			if (workList != null) {
				//log.info("worklist is not null======:"+workList);
				for (WorkListBaseVODlv211231 workInfo : workList) {
					HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
					setWorkInfoMapDlv211231(interfaceLevel12, workInfo);
					//dataList.add(interfaceLevel12);
					List<RtiSerialInfoVOKR> rtiSerialList = workInfo.getRtiSerialList();

					if (rtiSerialList != null && rtiSerialList.size() > 0) {
						//log.info("rtiSerialList is not null");
						for (RtiSerialInfoVOKR rtiSerialInfo : rtiSerialList) {
							HashMap<String, Object> interfaceLevel13 = (HashMap<String, Object>) interfaceLevel12.clone();
							setItemInfoMapKR(interfaceLevel13, rtiSerialInfo);
							//log.info("1111111111111111111 is not null");
							List<ItemSerialInfoVOKR> itemSerialList = rtiSerialInfo.getItemSerialList();
							//log.info("2222222222222222222 is not null");
							if (itemSerialList != null) {
								//log.info("itemSerialList is not null");
								for (ItemSerialInfoVOKR itemSerialInfo : itemSerialList) {
									HashMap<String, Object> interfaceLevel14 = (HashMap<String, Object>) interfaceLevel13.clone();
									setCargoItemSerailInfoMapKR(interfaceLevel14, itemSerialInfo);
									dataList.add(interfaceLevel14);
								}
							} else {
								dataList.add(interfaceLevel13);
							}
						}
					}
					else {
						dataList.add(interfaceLevel12);
					}
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVODlv220501(InterfaceBaseVODlv220501 data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			List<WorkListBaseVODlv220501> workList = data.getWorkList();
			if (workList != null) {
				//log.info("worklist is not null======:"+workList);
				for (WorkListBaseVODlv220501 workInfo : workList) {
					HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
					setWorkInfoMapDlv220501(interfaceLevel12, workInfo);
					//dataList.add(interfaceLevel12);
					List<RtiSerialInfoVOKR> rtiSerialList = workInfo.getRtiSerialList();

					if (rtiSerialList != null && rtiSerialList.size() > 0) {
						//log.info("rtiSerialList is not null");
						for (RtiSerialInfoVOKR rtiSerialInfo : rtiSerialList) {
							HashMap<String, Object> interfaceLevel13 = (HashMap<String, Object>) interfaceLevel12.clone();
							setItemInfoMapKR(interfaceLevel13, rtiSerialInfo);
							//log.info("1111111111111111111 is not null");
							List<ItemSerialInfoVOKR> itemSerialList = rtiSerialInfo.getItemSerialList();
							//log.info("2222222222222222222 is not null");
							if (itemSerialList != null) {
								//log.info("itemSerialList is not null");
								for (ItemSerialInfoVOKR itemSerialInfo : itemSerialList) {
									HashMap<String, Object> interfaceLevel14 = (HashMap<String, Object>) interfaceLevel13.clone();
									setCargoItemSerailInfoMapKR(interfaceLevel14, itemSerialInfo);
									dataList.add(interfaceLevel14);
								}
							} else {
								dataList.add(interfaceLevel13);
							}
						}
					}
					else {
						dataList.add(interfaceLevel12);
					}
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}
	
	protected List<Map<String, Object>> getDataRowFromInterfaceVOHOWSER(InterfaceBaseVOHOWSER data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_ORDER_SRL, data.getOrderSrl());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_AGENCY_MGR_CODE, data.getAgencyMgrCode());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_AGENCY_CUSTOMER_CODE, data.getAgencyCustomerCode());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_STATUS, data.getStatus());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_SVC_TYPE, data.getSvcType());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_RECEIVER_NAME, data.getReceiverName());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_ARRIVE_ADDRESS, data.getArriveAddress());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_RECEIVER_PHONE, data.getReceiverPhone());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_RECEIVER_PHONE_2ND, data.getReceiverPhone2nd());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_ARRIVAL_DATE, data.getArrivalDate());
			interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_ARRIVAL_TIME, data.getArrivalTime());
			//interfaceLevel11.put(ConstantWSIF.MAP_KEY_WORK_HOWSER_ONSITE_IMAGES, data.getOnsiteImages());
			
			List<GoodsResultVO> goodsResult = data.getGoodsResult();
			List<DeliveryManUsersVO> deliveryManUsers = data.getDeliveryManUsers();
			List onsiteImages = data.getOnsiteImages();

			HashMap<String, Object> interfaceLevelBasic = (HashMap<String, Object>) interfaceLevel11.clone();
			dataList.add(interfaceLevelBasic);
			
			if (goodsResult != null && goodsResult.size() > 0) {
				for (GoodsResultVO goodsResultInfo : goodsResult) {
					HashMap<String, Object> interfaceLevel4 = (HashMap<String, Object>) interfaceLevel11.clone();
					setGoodsResultInfoMapHOWSER(interfaceLevel4, goodsResultInfo);
					dataList.add(interfaceLevel4);
				}
			}
				
			if (deliveryManUsers != null && deliveryManUsers.size() > 0) {
				for (DeliveryManUsersVO deliveryManUsersInfo : deliveryManUsers) {
					HashMap<String, Object> interfaceLevel4 = (HashMap<String, Object>) interfaceLevel11.clone();
					setDeliveryManUsersInfoMapHOWSER(interfaceLevel4, deliveryManUsersInfo);
					dataList.add(interfaceLevel4);
				}
			}	
			
			if (onsiteImages != null && onsiteImages.size() > 0) {
				for (int i=0;i<onsiteImages.size();i++) {
					HashMap<String, Object> interfaceLevel4 = (HashMap<String, Object>) interfaceLevel11.clone();
					setOnsiteImagesInfoMapHOWSER(interfaceLevel4, (String)onsiteImages.get(i));
					dataList.add(interfaceLevel4);
				}
			}	

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVO to Data Row :", e);
			}
		}
		
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVOGodomall(InterfaceBaseVOGodomall data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			//WorkListBaseVOGodomall workList = data.getOrderData();
			List<WorkListBaseVOGodomall> workList = data.getOrderData();
			//WorkListBaseVOGodomall workList = data.getOrderData();
			if (workList != null) {
				for (WorkListBaseVOGodomall workInfo : workList) {
					if (workList != null) {
						log.info("worklist is not null======:"+workList);
						
						List<OrderGoodsDataVO> orderGoodsDataList = workInfo.getOrderGoodsData();
						List<OrderInfoDataVO> orderInfoDataVoList = workInfo.getOrderInfoData();
						List<OrderDeliveryDataVO> orderDeliveryDataList = workInfo.getOrderDeliveryData();
						
						
//						List<OrderGoodsDataVO> orderGoodsDataList = new ArrayList<OrderGoodsDataVO>();		
//						orderGoodsDataList.add(workInfo.getOrderGoodsData());
//						List<OrderInfoDataVO> orderInfoDataVoList = new ArrayList<OrderInfoDataVO>();		
//						orderInfoDataVoList.add(workInfo.getOrderInfoData());
//						List<OrderDeliveryDataVO> orderDeliveryDataList = new ArrayList<OrderDeliveryDataVO>();				
//						orderDeliveryDataList.add(workInfo.getOrderDeliveryData());
						
						HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
						setWorkInfoMapGodomall(interfaceLevel12, workInfo);
						dataList.add(interfaceLevel12);
	
						if (orderGoodsDataList != null) {
							for (OrderGoodsDataVO orderGoodsDataInfo : orderGoodsDataList) {
								HashMap<String, Object> interfaceLevel3 = (HashMap<String, Object>) interfaceLevel12.clone();
								setOrderGoodsDataInfoMap(interfaceLevel3, orderGoodsDataInfo);
	
								List<ClaimDataVO> claimDataList = new ArrayList<ClaimDataVO>();		
								claimDataList.add(orderGoodsDataInfo.getClaimData());	
								//ClaimDataVO ClaimData = orderGoodsDataInfo.getClaimData();
								
								//log.info("2222222222222222222 is not null");
								if (orderGoodsDataInfo.getClaimData() != null) {
									//log.info("itemSerialList is not null");
									for (ClaimDataVO claimDataInfo : claimDataList) {
										HashMap<String, Object> interfaceLevel14 = (HashMap<String, Object>) interfaceLevel3.clone();
										setClaimDataInfoMap(interfaceLevel14, claimDataInfo);
										dataList.add(interfaceLevel14);
									}
								} else {
									dataList.add(interfaceLevel3);
								}
							}
						}
	
						if (orderInfoDataVoList != null) {
							for (OrderInfoDataVO orderInfoDataInfo : orderInfoDataVoList) {
								HashMap<String, Object> interfaceLevel3 = (HashMap<String, Object>) interfaceLevel12.clone();
								setOrderInfoDataInfoMap(interfaceLevel3, orderInfoDataInfo);
								dataList.add(interfaceLevel3);
							}
						}
	
						if (orderDeliveryDataList != null) {
							for (OrderDeliveryDataVO orderDeliveryDataInfo : orderDeliveryDataList) {
								HashMap<String, Object> interfaceLevel3 = (HashMap<String, Object>) interfaceLevel12.clone();
								setOrderDeliveryDataInfoMap(interfaceLevel3, orderDeliveryDataInfo);
								dataList.add(interfaceLevel3);
							}
						}	
					}
				}
			} else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVOGodomall to Data Row :", e);
			}
		}
		
		log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}
	
	protected List<Map<String, Object>> getDataRowFromInterfaceVOGodomallItem(InterfaceBaseVOGodomallItem data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel11 = new HashMap<String, Object>();
			
			List<WorkListBaseVOGodomallItem> workList = data.getGoodsData();
			//WorkListBaseVOGodomall workList = data.getOrderData();
			if (workList != null) {
				for (WorkListBaseVOGodomallItem workInfo : workList) {
					if (workList != null) {
						log.info("worklist is not null======:"+workList);
						
						List<OptionDataVO> optionDataList = workInfo.getOptionData();
						
						List<MagnifyImageDataVO> magnifyImageDataList = new ArrayList<MagnifyImageDataVO>();		
						magnifyImageDataList.add(workInfo.getMagnifyImageData());
						List<MainImageDataVO> mainImageData = new ArrayList<MainImageDataVO>();		
						mainImageData.add(workInfo.getMainImageData());
						List<ListImageDataVO> ListImageDataList = new ArrayList<ListImageDataVO>();		
						ListImageDataList.add(workInfo.getListImageData());
						List<DetailImageDataVO> detailImageDataList = new ArrayList<DetailImageDataVO>();		
						detailImageDataList.add(workInfo.getDetailImageData());
						
		
						List<TextOptionDataVO> textOptionDataList = workInfo.getTextOptionData();
						List<GoodsMustInfoDataVO> goodsMustInfoDataList = workInfo.getGoodsMustInfoData();
		
						HashMap<String, Object> interfaceLevel12 = (HashMap<String, Object>) interfaceLevel11.clone();
						setWorkInfoMapGodomallItem(interfaceLevel12, workInfo);
						dataList.add(interfaceLevel12);
						
						if (optionDataList != null) {
							for (OptionDataVO optionDataInfo : optionDataList) {
								HashMap<String, Object> interfaceLevel13 = (HashMap<String, Object>) interfaceLevel12.clone();
								setOptionDataInfoMap(interfaceLevel13, optionDataInfo);
								dataList.add(interfaceLevel13);
							}
						} else if (textOptionDataList != null){
							for (TextOptionDataVO textOptionDataInfo : textOptionDataList) {
								HashMap<String, Object> interfaceLevel13 = (HashMap<String, Object>) interfaceLevel12.clone();
								setTextOptionDataInfoMap(interfaceLevel13, textOptionDataInfo);
								dataList.add(interfaceLevel13);
							}							
						} else if (goodsMustInfoDataList != null){
							for (GoodsMustInfoDataVO goodsMustInfoDataInfo : goodsMustInfoDataList) {

								List<StepDataVO> stepDataList = goodsMustInfoDataInfo.getStepData();
								
								HashMap<String, Object> interfaceLevel13 = (HashMap<String, Object>) interfaceLevel12.clone();
								setGoodsMustInfoDataInfoMap(interfaceLevel13, goodsMustInfoDataInfo);
								if (stepDataList != null){
									for (StepDataVO stepDataInfo : stepDataList) {
										HashMap<String, Object> interfaceLevel14 = (HashMap<String, Object>) interfaceLevel13.clone();
										setStepDataInfoMap(interfaceLevel14, stepDataInfo);	
									}
								} else {
									dataList.add(interfaceLevel13);
								}
								
							}							
						}else {
							dataList.add(interfaceLevel11);
						}
					}
				}
			}
			else {
				dataList.add(interfaceLevel11);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVOGodomall to Data Row :", e);
			}
		}
		
		//log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVOShopbuy(InterfaceBaseVOShopbuy data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel1 = new HashMap<String, Object>();
			
			List<WorkListBaseVOShopbuy> workList = data.getOrderData();

			if (workList != null) {
				for (WorkListBaseVOShopbuy workInfo : workList) {
					if (workList != null) {
						//log.info("worklist is not null======:"+workList);
						
						List<DeliveryGroupsVO> deliveryGroupDataList = workInfo.getDeliveryGroups();
						
						HashMap<String, Object> interfaceLevel2 = (HashMap<String, Object>) interfaceLevel1.clone();
						setWorkInfoMapShopbuy(interfaceLevel2, workInfo);
						dataList.add(interfaceLevel2);

						if (deliveryGroupDataList != null) {
							for (DeliveryGroupsVO deliveryGroupDataInfo : deliveryGroupDataList) {
								HashMap<String, Object> interfaceLevel3 = (HashMap<String, Object>) interfaceLevel2.clone();
								setDeliveryGroupDataInfoMap(interfaceLevel3, deliveryGroupDataInfo);
								dataList.add(interfaceLevel3);
	
								List<OrderProductsVO> orderProductList = deliveryGroupDataInfo.getOrderProducts();
								
								if (orderProductList != null) {
					
									for (OrderProductsVO orderProductInfo : orderProductList) {
										HashMap<String, Object> interfaceLevel4 = (HashMap<String, Object>) interfaceLevel3.clone();
										setOrderProductInfoMap(interfaceLevel4, orderProductInfo);
										dataList.add(interfaceLevel4);

										List<OrderProductOptionsVO> orderProductOptionsList = orderProductInfo.getOrderProductOptions();
										if (orderProductOptionsList != null) {
											for (OrderProductOptionsVO orderProductOptionsInfo : orderProductOptionsList) {
												HashMap<String, Object> interfaceLevel5 = (HashMap<String, Object>) interfaceLevel4.clone();
												setOrderProductOptionsMap(interfaceLevel5, orderProductOptionsInfo);
												dataList.add(interfaceLevel5);

												List<SetOptionsVO> setOptionList = orderProductOptionsInfo.getSetOptions();
												if (setOptionList != null) {
													for (SetOptionsVO setOptionInfo : setOptionList) {
														HashMap<String, Object> interfaceLevel6 = (HashMap<String, Object>) interfaceLevel5.clone();
														setSetOptionInfoMap(interfaceLevel6, setOptionInfo);
														dataList.add(interfaceLevel6);
													}
												} else {
													dataList.add(interfaceLevel5);
												}
											}
										} else {
											dataList.add(interfaceLevel4);
										}										
									}									
								}
							}
						}
					}
				}
			} else {
				dataList.add(interfaceLevel1);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVOGodomall to Data Row :", e);
			}
		}
		
		log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}

	protected List<Map<String, Object>> getDataRowFromInterfaceVOShopbuyItem(InterfaceBaseVOShopbuyItem data) throws Exception {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			HashMap<String, Object> interfaceLevel1 = new HashMap<String, Object>();
			
			List<WorkListBaseVOShopbuyItem> workList = data.getMallProduct();

			if (workList != null) {
				for (WorkListBaseVOShopbuyItem workInfo : workList) {
					if (workList != null) {
						log.info("worklist is not null======:"+workList);
						
						List<DeliveryGroupsVO> deliveryGroupDataList = workInfo.getDeliveryGroups();
						
						HashMap<String, Object> interfaceLevel2 = (HashMap<String, Object>) interfaceLevel1.clone();
						setWorkInfoMapShopbuyItem(interfaceLevel2, workInfo);
						dataList.add(interfaceLevel2);
	
						if (deliveryGroupDataList != null) {
							for (DeliveryGroupsVO deliveryGroupDataInfo : deliveryGroupDataList) {
								HashMap<String, Object> interfaceLevel3 = (HashMap<String, Object>) interfaceLevel2.clone();
								setDeliveryGroupDataInfoMap(interfaceLevel3, deliveryGroupDataInfo);
	
								List<OrderProductsVO> orderProductList = new ArrayList<OrderProductsVO>();		
								
								if (orderProductList != null) {
					
									for (OrderProductsVO orderProductInfo : orderProductList) {
										HashMap<String, Object> interfaceLevel4 = (HashMap<String, Object>) interfaceLevel3.clone();
										setOrderProductInfoMap(interfaceLevel4, orderProductInfo);
										//dataList.add(interfaceLevel14);

										List<OrderProductOptionsVO> orderProductOptionsList = orderProductInfo.getOrderProductOptions();
										if (orderProductOptionsList != null) {
											for (OrderProductOptionsVO orderProductOptionsInfo : orderProductOptionsList) {
												HashMap<String, Object> interfaceLevel5 = (HashMap<String, Object>) interfaceLevel4.clone();
												setOrderProductOptionsMap(interfaceLevel5, orderProductOptionsInfo);

												List<SetOptionsVO> setOptionList = orderProductOptionsInfo.getSetOptions();
												if (setOptionList != null) {
													for (SetOptionsVO setOptionInfo : setOptionList) {
														HashMap<String, Object> interfaceLevel6 = (HashMap<String, Object>) interfaceLevel5.clone();
														setSetOptionInfoMap(interfaceLevel6, setOptionInfo);
														dataList.add(interfaceLevel6);
													}
												} else {
													dataList.add(interfaceLevel5);
												}
											}
										} else {
											dataList.add(interfaceLevel4);
										}
										
									}
									
								} else {
									dataList.add(interfaceLevel3);
								}
							}
						}
					}
				}
			} else {
				dataList.add(interfaceLevel1);
			}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to convert InterfaceVOGodomall to Data Row :", e);
			}
		}
		
		log.info("dataList:=====654654654654==========="+dataList);
		return dataList;
	}
	
	protected Map<String, Object> createSPParamMapKR(Map<String, Object> model) {
		Map<String, Object> modelIns = new HashMap<String, Object>();

		// String[] iOrdSeq = new String[1];
		// if (model.get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
		// 		iOrdSeq[0] = (String) model.get(ConstantWSIF.IF_KEY_ORD_SEQ);
		// }

		modelIns.put("loginID", (String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID));		
		modelIns.put("password", (String) model.get(ConstantWSIF.IF_KEY_PASSWORD));
		modelIns.put("epcCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_EPC_CD));
		modelIns.put("lcId", (String) model.get(ConstantWSIF.IF_KEY_ROW_LC_ID));
		modelIns.put("date", (String) model.get(ConstantWSIF.IF_KEY_ROW_DATE));		
		modelIns.put("custType", (String) model.get(ConstantWSIF.IF_KEY_ROW_CUST_TYPE));	
		modelIns.put("ordId", (String) model.get(ConstantWSIF.IF_KEY_ROW_ORD_ID));	
		modelIns.put("ordSeq", (String) model.get(ConstantWSIF.IF_KEY_ROW_ORD_SEQ_M));	
		modelIns.put("custLotNo", (String) model.get(ConstantWSIF.IF_KEY_ROW_CUST_LOT_NO_M));	
		modelIns.put("custId", (String) model.get(ConstantWSIF.IF_KEY_ROW_CUST_ID));	
		modelIns.put("locBarcodeCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_LOC_BARCODE_CD));
		modelIns.put("ordQty", (String) model.get(ConstantWSIF.IF_KEY_ROW_ORD_QTY));	
		modelIns.put("workQty", (String) model.get(ConstantWSIF.IF_KEY_ROW_WORK_QTY));	
		modelIns.put("workWgt", (String) model.get(ConstantWSIF.IF_KEY_ROW_WORK_WGT));	
		modelIns.put("rtiEpcCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD));
		modelIns.put("searchLotNo", (String) model.get(ConstantWSIF.IF_KEY_ROW_SEARCH_LOT_NO));
		modelIns.put("fromLocBarCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_FROM_LOC_BARCODE_CD));
		modelIns.put("toLocBarCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_TO_LOC_BARCODE_CD));
		modelIns.put("separatorCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD));
		modelIns.put("urn", (String) model.get(ConstantWSIF.IF_KEY_ROW_RTI_SERIAL));
		modelIns.put("rfidReadDt", (String) model.get(ConstantWSIF.IF_KEY_ROW_RFID_READ_DT));
		modelIns.put("ritemId", (String) model.get(ConstantWSIF.IF_KEY_ROW_RITEM_ID));
		modelIns.put("itemBarcodeCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_ITEM_BARCODE_CD));
		modelIns.put("locId", (String) model.get(ConstantWSIF.IF_KEY_ROW_LOC_ID));
		modelIns.put("locCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_LOC_CD));

		return modelIns;
	}
	
	protected Map<String, Object> createSPParamMapDlvTrace(Map<String, Object> model) {
		Map<String, Object> modelIns = new HashMap<String, Object>();

		// String[] iOrdSeq = new String[1];
		// if (model.get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
		// 		iOrdSeq[0] = (String) model.get(ConstantWSIF.IF_KEY_ORD_SEQ);
		// }

		modelIns.put("loginID", (String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID));		
		modelIns.put("password", (String) model.get(ConstantWSIF.IF_KEY_PASSWORD));
		modelIns.put("salesCustNo", (String) model.get(ConstantWSIF.IF_KEY_ROW_DLV_SALES_CUST_NO));
		modelIns.put("traceOrdId", (String) model.get(ConstantWSIF.IF_KEY_ROW_DLV_TRACE_ORD_ID));

		return modelIns;
	}

	protected Map<String, Object> createSPParamMapTwelvecmTrace(Map<String, Object> model) {
		Map<String, Object> modelIns = new HashMap<String, Object>();

		// String[] iOrdSeq = new String[1];
		// if (model.get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
		// 		iOrdSeq[0] = (String) model.get(ConstantWSIF.IF_KEY_ORD_SEQ);
		// }

		modelIns.put("I_LOGIN_ID", (String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID));		
		modelIns.put("I_PASSWORD", (String) model.get(ConstantWSIF.IF_KEY_PASSWORD));
		modelIns.put("I_REFERENCE_NO1", (String) model.get(ConstantWSIF.IF_KEY_ROW_TWELVECM_TRACE_ORD_ID));

		return modelIns;
	}
	
	protected Map<String, Object> createSPParamMapCust(Map<String, Object> model) {
		Map<String, Object> modelIns = new HashMap<String, Object>();

		// String[] iOrdSeq = new String[1];
		// if (model.get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
		// 		iOrdSeq[0] = (String) model.get(ConstantWSIF.IF_KEY_ORD_SEQ);
		// }

		modelIns.put("lcId", (String) model.get(ConstantWSIF.IF_KEY_ROW_LC_ID));
		modelIns.put("custCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_CUST_CD));		
		modelIns.put("ordId", (String) model.get(ConstantWSIF.IF_KEY_ROW_ORD_ID));			
		modelIns.put("ordType", (String) model.get(ConstantWSIF.IF_KEY_ROW_ORD_TYPE));	
		modelIns.put("ritemId", (String) model.get(ConstantWSIF.IF_KEY_ROW_RITEM_ID));	
		modelIns.put("separatorCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_SEPARATOR_CD));	
		modelIns.put("timeFrom", (String) model.get(ConstantWSIF.IF_KEY_ROW_TIME_FROM));
		modelIns.put("timeTo", (String) model.get(ConstantWSIF.IF_KEY_ROW_TIME_TO));

		return modelIns;
	}
	
	protected Map<String, Object> createSPParamMapKrUWMS(Map<String, Object> model) {
		Map<String, Object> modelIns = new HashMap<String, Object>();

		// String[] iOrdSeq = new String[1];
		// if (model.get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
		// 		iOrdSeq[0] = (String) model.get(ConstantWSIF.IF_KEY_ORD_SEQ);
		// }

		modelIns.put("loginID", (String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID));		
		modelIns.put("password", (String) model.get(ConstantWSIF.IF_KEY_PASSWORD));
		modelIns.put("lcId", (String) model.get(ConstantWSIF.IF_KEY_ROW_LC_ID));
		modelIns.put("date", (String) model.get(ConstantWSIF.IF_KEY_ROW_DATE));		
		modelIns.put("rtiEpcCd", (String) model.get(ConstantWSIF.IF_KEY_ROW_RTI_EPC_CD));
		modelIns.put("positionSx", (String) model.get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SX));
		modelIns.put("positionSy", (String) model.get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SY));
		modelIns.put("positionSz", (String) model.get(ConstantWSIF.IF_KEY_ROW_UWMS_POSITION_SZ));

		return modelIns;
	}

	protected Map<String, Object> createSPParamMap(Map<String, Object> model) {
		Map<String, Object> modelIns = new HashMap<String, Object>();

		String[] dummmyArray = new String[1];
		String[] iRegistSeq = new String[1];
		String[] iWorkSeq = new String[1];
		// String[] iOrdSeq = new String[1];
		// if (model.get(ConstantWSIF.IF_KEY_ORD_SEQ) != null) {
		// 		iOrdSeq[0] = (String) model.get(ConstantWSIF.IF_KEY_ORD_SEQ);
		// }

		modelIns.put("loginID", (String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID));		
		modelIns.put("terminalID", (String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID));
		modelIns.put("password", (String) model.get(ConstantWSIF.IF_KEY_PASSWORD));
		
		if (model.get(ConstantWSIF.IF_KEY_WORK_SEQ) != null) {
			iWorkSeq[0] = (String) model.get(ConstantWSIF.IF_KEY_WORK_SEQ);
			modelIns.put("iWorkSeq", iWorkSeq);
		} else {
			modelIns.put("iWorkSeq", dummmyArray);
		}
		
		if (model.get(ConstantWSIF.IF_KEY_REGIST_SEQ) != null) {
			iRegistSeq[0] = (String) model.get(ConstantWSIF.IF_KEY_REGIST_SEQ);
			modelIns.put("iRegistSeq", iRegistSeq);
		} else {
			modelIns.put("iRegistSeq", dummmyArray);
		}
		
		modelIns.put("iEvectCd", dummmyArray);
		modelIns.put("iStateCd", dummmyArray);
		
		modelIns.put("iLocalSlipNo", dummmyArray);
		modelIns.put("iCustomerSlipNo", dummmyArray);
		modelIns.put("iOtherSlipNo", dummmyArray);		
		modelIns.put("iBaseLocCd", dummmyArray);		
		modelIns.put("iOtherPartyLocCd", dummmyArray);

		modelIns.put("iCustomerLocCd", dummmyArray);
		modelIns.put("iWarehouseID", dummmyArray);
		modelIns.put("iWarehouseNm", dummmyArray);
		modelIns.put("iCustomerID", dummmyArray);
		modelIns.put("iCustomerNm", dummmyArray);

		modelIns.put("iShippingDt", dummmyArray);
		modelIns.put("iReceivingDt", dummmyArray);
		modelIns.put("iInOutDt", dummmyArray);
		modelIns.put("iEventDt", dummmyArray);
		modelIns.put("iForwarderCd", dummmyArray);

		modelIns.put("iForwarderNm", dummmyArray);
		modelIns.put("iCarID", dummmyArray);
		modelIns.put("iReasonID", dummmyArray);
		modelIns.put("iUnitWeight", dummmyArray);

		modelIns.put("iLockStateCd", dummmyArray);
		modelIns.put("iRtiCd", dummmyArray);
		modelIns.put("iRtiBarcode ", dummmyArray);
		modelIns.put("iRtiQty", dummmyArray);
		modelIns.put("iRtiRealQty", dummmyArray);

		modelIns.put("iRtiIndividualGrossType", dummmyArray);
		modelIns.put("iRtiSerial", dummmyArray);
		modelIns.put("iRfidReadDt", dummmyArray);
		modelIns.put("iFromLocLocalCd", dummmyArray);
		modelIns.put("iToLocLocalCd", dummmyArray);

		modelIns.put("iFromLocGlobalCd", dummmyArray);
		modelIns.put("iToLocGlobalCd", dummmyArray);
		modelIns.put("iTotalWgtMax", dummmyArray);
		modelIns.put("iTotalWgtMin", dummmyArray);
		modelIns.put("iRealTotalWgt", dummmyArray);

		modelIns.put("iAlertReasonCd", dummmyArray);
		modelIns.put("iRtiFromLocLocalCd", dummmyArray);
		modelIns.put("iRtiToLocLocalCd", dummmyArray);
		modelIns.put("iRtiFromLocGlobalCd", dummmyArray);
		modelIns.put("iRtiToLocGlobalCd", dummmyArray);

		modelIns.put("iRtiChildSerial", dummmyArray);
		modelIns.put("iRfidChildReadDt", dummmyArray);
		modelIns.put("iItemCd", dummmyArray);
		modelIns.put("iCustomerItemCd", dummmyArray);
		modelIns.put("iItfCd", dummmyArray);

		modelIns.put("iJanCd", dummmyArray);
		modelIns.put("iItemNm", dummmyArray);
		modelIns.put("iMakerNm", dummmyArray);
		modelIns.put("iColorNm", dummmyArray);
		modelIns.put("iItemSize", dummmyArray);

		modelIns.put("iQty", dummmyArray);
		modelIns.put("iRealQty", dummmyArray);
		modelIns.put("iUnit", dummmyArray);
		modelIns.put("iWgt", dummmyArray);
		modelIns.put("iWgtArrowRate", dummmyArray);

		modelIns.put("iFinallyReceivingExpiryDt", dummmyArray);
		modelIns.put("iFinallyReceivingMakeDt", dummmyArray);
		modelIns.put("iFinallyReceivingLotNo", dummmyArray);
		modelIns.put("iBestDateNum", dummmyArray);
		modelIns.put("iBestDateUnit", dummmyArray);

		modelIns.put("iIndividualGrossType", dummmyArray);
		modelIns.put("iItemTotalWgtMax", dummmyArray);
		modelIns.put("iItemTotalWgtMin", dummmyArray);
		modelIns.put("iItemRealTotalWgt", dummmyArray);
		modelIns.put("iItemIDCd", dummmyArray);

		modelIns.put("iOrdSeq", dummmyArray);
		modelIns.put("iItemQty", dummmyArray);
		modelIns.put("iItemRealQty", dummmyArray);
		modelIns.put("iItemFromLocLocalCd", dummmyArray);
		modelIns.put("iItemToLocLocalCd", dummmyArray);

		modelIns.put("iItemFromLocGlobalCd", dummmyArray);
		modelIns.put("iItemToLocGlobalCd", dummmyArray);
		modelIns.put("iExpiryDt", dummmyArray);
		modelIns.put("iExpiryDtType", dummmyArray);
		modelIns.put("iMakeDt", dummmyArray);

		modelIns.put("iLotNo", dummmyArray);
		modelIns.put("iItemSerial", dummmyArray);
		modelIns.put("iItemAlertReasonCd", dummmyArray);
		modelIns.put("iItemRfidReadDt", dummmyArray);

		modelIns.put("iUserNo", (String) model.get(ConstantWSIF.SS_USER_NO));
		modelIns.put("iWorkIp", (String) model.get(ConstantWSIF.SS_CLIENT_IP));
		
		modelIns.put("iPackageNo", dummmyArray);
		modelIns.put("iPackageLevel", dummmyArray);		

		return modelIns;
	}

	protected Map<String, Object> getSPCompleteParamMap(InterfaceBaseVO data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_LOGIN_ID);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			String iUser = loginID;
			String iWorkIp = "";

			String[] iEvectCd = new String[workList.size()];
			String[] iStateCd = new String[workList.size()];
			String[] iRegistSeq = new String[workList.size()];
			String[] iWorkSeq = new String[workList.size()];
			String[] iLocalSlipNo = new String[workList.size()];
			String[] iCustomerSlipNo = new String[workList.size()];
			String[] iOtherSlipNo = new String[workList.size()];
			
			// String[] iLocalLocCd = new String[workList.size()];
			String[] iBaseLocCd = new String[workList.size()];
			
			String[] iOtherPartyLocCd = new String[workList.size()];
			String[] iCustomerLocCd = new String[workList.size()];
			String[] iWarehouseId = new String[workList.size()];
			String[] iWarehouseNm = new String[workList.size()];
			String[] iCustomerId = new String[workList.size()];
			String[] iCustomerNm = new String[workList.size()];
			String[] iShippingDt = new String[workList.size()];
			String[] iReceivingDt = new String[workList.size()];
			String[] iInOutDt = new String[workList.size()];
			String[] iEventDt = new String[workList.size()];
			String[] iForwarderCd = new String[workList.size()];
			String[] iForwarderNm = new String[workList.size()];
			String[] iCarId = new String[workList.size()];
			String[] iReasonId = new String[workList.size()];
			String[] iUnitWeight = new String[workList.size()];
			// String[] iLockReasonCd = new String[workList.size()];
			String[] iLockStateCd = new String[workList.size()];
			
			String[] iPackageNo = new String[workList.size()];
			String[] iPackageLevel = new String[workList.size()];
			
			String[] iRtiCd = new String[workList.size()];
			String[] iRtiBarcode = new String[workList.size()];
			String[] iRtiQty = new String[workList.size()];
			String[] iRtiRealQty = new String[workList.size()];
			String[] iRtiIndividualGrossType = new String[workList.size()];
			String[] iRtiSerial = new String[workList.size()];
			String[] iRfidReadDt = new String[workList.size()];
			String[] iFromLocLocalCd = new String[workList.size()];
			String[] iToLocLocalCd = new String[workList.size()];
			String[] iFromLocGlobalCd = new String[workList.size()];
			String[] iToLocGlobalCd = new String[workList.size()];
			String[] iTotalWgtMax = new String[workList.size()];
			String[] iTotalWgtMin = new String[workList.size()];
			String[] iRealTotalWgt = new String[workList.size()];
			String[] iAlertReasonCd = new String[workList.size()];
			String[] iRtiFromLocLocalCd = new String[workList.size()];
			String[] iRtiToLocLocalCd = new String[workList.size()];
			String[] iRtiFromLocGlobalCd = new String[workList.size()];
			String[] iRtiToLocGlobalCd = new String[workList.size()];
			String[] iRtiChildSerial = new String[workList.size()];
			String[] iRfidChildReadDt = new String[workList.size()];
			String[] iItemCd = new String[workList.size()];
			String[] iCustomerItemCd = new String[workList.size()];
			String[] iItfCd = new String[workList.size()];
			String[] iJanCd = new String[workList.size()];
			String[] iItemNm = new String[workList.size()];
			String[] iMakerNm = new String[workList.size()];
			String[] iColorNm = new String[workList.size()];
			String[] iItemSize = new String[workList.size()];
			String[] iQty = new String[workList.size()];
			String[] iRealQty = new String[workList.size()];
			String[] iUnit = new String[workList.size()];
			String[] iWgt = new String[workList.size()];
			String[] iWgtArrowRate = new String[workList.size()];
			String[] iFinallyReceivingExpiryDt = new String[workList.size()];
			String[] iFinallyReceivingMakeDt = new String[workList.size()];
			String[] iFinallyReceivingLotNo = new String[workList.size()];
			String[] iBestDateNum = new String[workList.size()];
			String[] iBestDateUnit = new String[workList.size()];
			String[] iIndividualGrossType = new String[workList.size()];
			String[] iItemTotalWgtMax = new String[workList.size()];
			String[] iItemTotalWgtMin = new String[workList.size()];
			String[] iItemRealTotalWgt = new String[workList.size()];
			String[] iItemIDCd = new String[workList.size()];
			String[] iOrdSeq = new String[workList.size()];
			String[] iItemQty = new String[workList.size()];
			String[] iItemRealQty = new String[workList.size()];
			String[] iItemFromLocLocalCd = new String[workList.size()];
			String[] iItemToLocLocalCd = new String[workList.size()];
			String[] iItemFromLocGlobalCd = new String[workList.size()];
			String[] iItemToLocGlobalCd = new String[workList.size()];
			String[] iExpiryDt = new String[workList.size()];
			String[] iExpiryDtType = new String[workList.size()];
			String[] iMakeDt = new String[workList.size()];
			String[] iLotNo = new String[workList.size()];
			String[] iItemSerial = new String[workList.size()];
			String[] iItemAlertReasonCd = new String[workList.size()];
			String[] iItemRfidReadDt = new String[workList.size()];

			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
                iEvectCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_EVENT_CD));
                iStateCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_STATE_CD));
                iRegistSeq[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_REGIST_SEQ));
                iWorkSeq[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_WORK_SEQ));
                iLocalSlipNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_LOCAL_SLIP_NO));
                iCustomerSlipNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_CUSTOMER_SLIP_NO));
                iOtherSlipNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_OTHER_SLIP_NO));
                iBaseLocCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_BASE_LOC_CD));
                iOtherPartyLocCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_OTHER_PARTY_LOC_CD));
                // 10
                iCustomerLocCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_CUSTOMER_LOC_CD));
                iWarehouseId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_WAREHOUSE_ID));
                iWarehouseNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_WAREHOUSE_NM));
                iCustomerId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_CUSTOMER_ID));
                iCustomerNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_CUSTOMER_NM));
                iShippingDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHIPPING_DT)));
                iReceivingDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_RECEIVING_DT)));
                iInOutDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_IN_OUT_DT)));
                iEventDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_EVENT_DT)));
                iForwarderCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_FORWARDER_CD));
                // 20
                iForwarderNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_FORWARDER_NM));
                iCarId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_CAR_ID));
                iReasonId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_REASON_CD));
                iUnitWeight[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_UNIT_WEIGHT));
                // iLockReasonCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_LOCK_REASON_CD));
                iLockStateCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_LOCK_STATE_CD));

                // WORK_PACKING
                iRtiCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_CD));
                iRtiBarcode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_BARCODE));
                iRtiQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_QTY));
                iRtiRealQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_REAL_QTY));
                // 30
                iRtiIndividualGrossType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_INDIVIDUAL_GROSS_TYPE));
                iRtiSerial[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RTI_SERIAL));
                iRfidReadDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_RTI_SERIAL_RFID_READ_DT)));
                iFromLocLocalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_FROM_LOC_LOCAL_CD));
                iToLocLocalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_TO_LOC_LOCAL_CD));
                iFromLocGlobalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_FROM_LOC_GLOBAL_CD));
                iToLocGlobalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_TO_LOC_GLOBAL_CD));
                iTotalWgtMax[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_TOTAL_WGT_MAX));
                iTotalWgtMin[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_TOTAL_WGT_MIN));
                iRealTotalWgt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_REAL_TOTAL_WGT));
                // 40
                iAlertReasonCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_ALERT_REASON_CD));
                // WORK_PACKING_CARGO_RTI
                iRtiFromLocLocalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_FROM_LOC_LOCAL_CD));
                iRtiToLocLocalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_TO_LOC_LOCAL_CD));
                iRtiFromLocGlobalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_FROM_LOC_GLOBAL_CD));
                iRtiToLocGlobalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_TO_LOC_GLOBAL_CD));
                iRtiChildSerial[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_RTI_SERIAL));
                iRfidChildReadDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_RTI_RFID_READ_DT)));

                if ("PACKING".equals(getWorkType(data))) {

                    // WORK_PACKING_CARGO_ITEM
                    iItemCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_ITEM_CD));
                    iCustomerItemCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_CUSTOMER_ITEM_CD));
                    iItfCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_ITF_CD));
                    // 50
                    iJanCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_JAN_CD));
                    iItemNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_ITEM_NM));
                    iMakerNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_MAKER_NM));
                    iColorNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_COLOR_NM));
                    iItemSize[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_SIZE));
                    iQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_QTY));
                    iRealQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_REAL_QTY));
                    iUnit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_UNIT));
                    iWgt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_WGT));
                    iWgtArrowRate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_WGT_ARROW_RATE));
                    // 60
                    iFinallyReceivingExpiryDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_FINALLY_RECEIVING_EXPIRY_DT));
                    iFinallyReceivingMakeDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_FINALLY_RECEIVING_MAKE_DT));
                    iFinallyReceivingLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_FINALLY_RECEIVING_LOT_NO));
                    iBestDateNum[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_BEST_DATE_NUM));
                    iBestDateUnit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_BEST_DATE_UNIT));
                    iIndividualGrossType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_INDIVIDUAL_GROSS_TYPE));
                    iItemTotalWgtMax[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_TOTAL_WGT_MAX));
                    iItemTotalWgtMin[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_TOTAL_WGT_MIN));
                    iItemRealTotalWgt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_REAL_TOTAL_WGT));
                    iItemIDCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_ITEM_ID_CD));
                    // 70
                    // WORK_PACKING_CARGO_ITEM_LOT
                    iOrdSeq[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_ORDER_SEQ));
                    iItemQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_QTY));
                    iItemRealQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_REAL_QTY));
                    iItemFromLocLocalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_FROM_LOC_LOCAL_CD));
                    iItemToLocLocalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_TO_LOC_LOCAL_CD));

                    iItemFromLocGlobalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_FROM_LOC_GLOBAL_CD));
                    iItemToLocGlobalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_TO_LOC_GLOBAL_CD));
                    iExpiryDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_EXPIRY_DT), "yyyyMMdd"));
                    iExpiryDtType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_EXPIRY_DT_TYPE));
                    iMakeDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_MAKE_DT), "yyyyMMdd"));
                    // 80
                    iLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_NO));
                    iItemSerial[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_ITEM_SERIAL));
                    iItemAlertReasonCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_ALERT_REASON_CD));
                    iItemRfidReadDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_RFID_READ_DT)));
                    
                    iPackageNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_PACKAGE_NO));
                    iPackageLevel[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_PACKAGE_LEVEL));

                } else {
                    // WORK_ITEM
                    iItemCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_ITEM_CD));
                    iCustomerItemCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_CUSTOMER_ITEM_CD));
                    iItfCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_ITF_CD));
                    // 50
                    iJanCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_JAN_CD));
                    iItemNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_ITEM_NM));
                    iMakerNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_MAKER_NM));
                    iColorNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_COLOR_NM));
                    iItemSize[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_SIZE));
                    iQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_QTY));
                    iRealQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_REAL_QTY));
                    iUnit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_UNIT));
                    iWgt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_WGT));
                    iWgtArrowRate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_WGT_ARROW_RATE));
                    // 60
                    iFinallyReceivingExpiryDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_FINALLY_RECEIVING_EXPIRY_DT));
                    iFinallyReceivingMakeDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_FINALLY_RECEIVING_MAKE_DT));
                    iFinallyReceivingLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_FINALLY_RECEIVING_LOT_NO));
                    iBestDateNum[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_BEST_DATE_NUM));
                    iBestDateUnit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_BEST_DATE_UNIT));
                    iIndividualGrossType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_INDIVIDUAL_GROSS_TYPE));
                    iItemTotalWgtMax[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_TOTAL_WGT_MAX));
                    iItemTotalWgtMin[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_TOTAL_WGT_MIN));
                    iItemRealTotalWgt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_REAL_TOTAL_WGT));
                    iItemIDCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_ITEM_ID_CD));
                    // 70
                    // WORK_PACKING_CARGO_ITEM_LOT
                    iOrdSeq[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_ORDER_SEQ));
                    iItemQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_QTY));
                    iItemRealQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_REAL_QTY));
                    iItemFromLocLocalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_FROM_LOC_LOCAL_CD));
                    iItemToLocLocalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_TO_LOC_LOCAL_CD));

                    iItemFromLocGlobalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_FROM_LOC_GLOBAL_CD));
                    iItemToLocGlobalCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_TO_LOC_GLOBAL_CD));
                    iExpiryDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_EXPIRY_DT), "yyyyMMdd"));
                    iExpiryDtType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_EXPIRY_DT_TYPE));
                    iMakeDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_MAKE_DT), "yyyyMMdd"));
                    // 80
                    iLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_LOT_NO));
                    iItemSerial[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_ITEM_SERIAL_ITEM_SERIAL));
                    iItemAlertReasonCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_ITEM_SERIAL_ALERT_REASON_CD));
                    iItemRfidReadDt[idx] = getParameterValue(InterfaceUtil.getDateString((Date) workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_LOT_ITEM_SERIAL_RFID_READ_DT)));
				}
                /*
					//log.info("===========================================================================");
					//log.info("iEvectCd[" + idx + "] : " + iEvectCd[idx]);
					//log.info("iStateCd[" + idx + "] : " + iStateCd[idx]);
					//log.info("iRegistSeq[" + idx + "] : " + iRegistSeq[idx]);
					//log.info("iWorkSeq[" + idx + "] : " + iWorkSeq[idx]);
					//log.info("iLocalSlipNo[" + idx + "] : " + iLocalSlipNo[idx]);
					//log.info("iCustomerSlipNo[" + idx + "] : " + iCustomerSlipNo[idx]);
					//log.info("iOtherSlipNo[" + idx + "] : " + iOtherSlipNo[idx]);
					//log.info("iBaseLocCd[" + idx + "] : " + iBaseLocCd[idx]);
					//log.info("iOtherPartyLocCd[" + idx + "] : " + iOtherPartyLocCd[idx]);
					//log.info("iCustomerLocCd[" + idx + "] : " + iCustomerLocCd[idx]);
					//log.info("iWarehouseId[" + idx + "] : " + iWarehouseId[idx]);
					//log.info("iWarehouseNm[" + idx + "] : " + iWarehouseNm[idx]);
					//log.info("iCustomerId[" + idx + "] : " + iCustomerId[idx]);
					//log.info("iCustomerNm[" + idx + "] : " + iCustomerNm[idx]);
					//log.info("iShippingDt[" + idx + "] : " + iShippingDt[idx]);
					//log.info("iReceivingDt[" + idx + "] : " + iReceivingDt[idx]);
					//log.info("iInOutDt[" + idx + "] : " + iInOutDt[idx]);
					//log.info("iEventDt[" + idx + "] : " + iEventDt[idx]);
					//log.info("iForwarderCd[" + idx + "] : " + iForwarderCd[idx]);
					//log.info("iForwarderNm[" + idx + "] : " + iForwarderNm[idx]);
					//log.info("iCarId[" + idx + "] : " + iCarId[idx]);
					//log.info("iReasonId[" + idx + "] : " + iReasonId[idx]);
					//log.info("iUnitWeight[" + idx + "] : " + iUnitWeight[idx]);
					//log.info("iLockStateCd[" + idx + "] : " + iLockStateCd[idx]);
					//log.info("iRtiCd[" + idx + "] : " + iRtiCd[idx]);
					//log.info("iRtiBarcode[" + idx + "] : " + iRtiBarcode[idx]);
					//log.info("iRtiQty[" + idx + "] : " + iRtiQty[idx]);
					//log.info("iRtiRealQty[" + idx + "] : " + iRtiRealQty[idx]);
					//log.info("iRtiIndividualGrossType[" + idx + "] : " + iRtiIndividualGrossType[idx]);
					//log.info("iRtiSerial[" + idx + "] : " + iRtiSerial[idx]);
					//log.info("iRfidReadDt[" + idx + "] : " + iRfidReadDt[idx]);
					//log.info("iFromLocLocalCd[" + idx + "] : " + iFromLocLocalCd[idx]);
					//log.info("iToLocLocalCd[" + idx + "] : " + iToLocLocalCd[idx]);
					//log.info("iFromLocGlobalCd[" + idx + "] : " + iFromLocGlobalCd[idx]);
					//log.info("iToLocGlobalCd[" + idx + "] : " + iToLocGlobalCd[idx]);
					//log.info("iTotalWgtMax[" + idx + "] : " + iTotalWgtMax[idx]);
					//log.info("iTotalWgtMin[" + idx + "] : " + iTotalWgtMin[idx]);
					//log.info("iRealTotalWgt[" + idx + "] : " + iRealTotalWgt[idx]);
					//log.info("iAlertReasonCd[" + idx + "] : " + iAlertReasonCd[idx]);
					//log.info("iRtiFromLocLocalCd[" + idx + "] : " + iRtiFromLocLocalCd[idx]);
					//log.info("iRtiToLocLocalCd[" + idx + "] : " + iRtiToLocLocalCd[idx]);
					//log.info("iRtiFromLocGlobalCd[" + idx + "] : " + iRtiFromLocGlobalCd[idx]);
					//log.info("iRtiToLocGlobalCd[" + idx + "] : " + iRtiToLocGlobalCd[idx]);
					//log.info("iRtiChildSerial[" + idx + "] : " + iRtiChildSerial[idx]);
					//log.info("iRfidChildReadDt[" + idx + "] : " + iRfidChildReadDt[idx]);
					//log.info("iItemCd[" + idx + "] : " + iItemCd[idx]);
					//log.info("iCustomerItemCd[" + idx + "] : " + iCustomerItemCd[idx]);
					//log.info("iItfCd[" + idx + "] : " + iItfCd[idx]);
					//log.info("iJanCd[" + idx + "] : " + iJanCd[idx]);
					//log.info("iItemNm[" + idx + "] : " + iItemNm[idx]);
					//log.info("iMakerNm[" + idx + "] : " + iMakerNm[idx]);
					//log.info("iColorNm[" + idx + "] : " + iColorNm[idx]);
					//log.info("iItemSize[" + idx + "] : " + iItemSize[idx]);
					//log.info("iQty[" + idx + "] : " + iQty[idx]);
					//log.info("iRealQty[" + idx + "] : " + iRealQty[idx]);
					//log.info("iUnit[" + idx + "] : " + iUnit[idx]);
					//log.info("iWgt[" + idx + "] : " + iWgt[idx]);
					//log.info("iWgtArrowRate[" + idx + "] : " + iWgtArrowRate[idx]);
					//log.info("iFinallyReceivingExpiryDt[" + idx + "] : " + iFinallyReceivingExpiryDt[idx]);
					//log.info("iFinallyReceivingMakeDt[" + idx + "] : " + iFinallyReceivingMakeDt[idx]);
					//log.info("iFinallyReceivingLotNo[" + idx + "] : " + iFinallyReceivingLotNo[idx]);
					//log.info("iBestDateNum[" + idx + "] : " + iBestDateNum[idx]);
					//log.info("iBestDateUnit[" + idx + "] : " + iBestDateUnit[idx]);
					//log.info("iIndividualGrossType[" + idx + "] : " + iIndividualGrossType[idx]);
					//log.info("iItemTotalWgtMax[" + idx + "] : " + iItemTotalWgtMax[idx]);
					//log.info("iItemTotalWgtMin[" + idx + "] : " + iItemTotalWgtMin[idx]);
					//log.info("iItemRealTotalWgt[" + idx + "] : " + iItemRealTotalWgt[idx]);
					//log.info("iItemIDCd[" + idx + "] : " + iItemIDCd[idx]);
					//log.info("iOrdSeq[" + idx + "] : " + iOrdSeq[idx]);
					//log.info("iItemQty[" + idx + "] : " + iItemQty[idx]);
					//log.info("iItemRealQty[" + idx + "] : " + iItemRealQty[idx]);
					//log.info("iItemFromLocLocalCd[" + idx + "] : " + iItemFromLocLocalCd[idx]);
					//log.info("iItemToLocLocalCd[" + idx + "] : " + iItemToLocLocalCd[idx]);
					//log.info("iItemFromLocGlobalCd[" + idx + "] : " + iItemFromLocGlobalCd[idx]);
					//log.info("iItemToLocGlobalCd[" + idx + "] : " + iItemToLocGlobalCd[idx]);
					//log.info("iExpiryDt[" + idx + "] : " + iExpiryDt[idx]);
					//log.info("iExpiryDtType[" + idx + "] : " + iExpiryDtType[idx]);
					//log.info("iMakeDt[" + idx + "] : " + iMakeDt[idx]);
					//log.info("iLotNo[" + idx + "] : " + iLotNo[idx]);
					//log.info("iItemSerial[" + idx + "] : " + iItemSerial[idx]);
					//log.info("iItemAlertReasonCd[" + idx + "] : " + iItemAlertReasonCd[idx]);
					//log.info("iItemRfidReadDt[" + idx + "] : " + iItemRfidReadDt[idx]);
					//log.info("iPackageNo[" + idx + "] : " + iPackageNo[idx]);
					//log.info("iPackageLevel[" + idx + "] : " + iPackageLevel[idx]);
					//log.info("===========================================================================");
				*/
				idx = idx + 1;
			}

			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("loginID", loginID);
			
			modelIns.put("terminalID", terminalID);
			modelIns.put("iEvectCd", iEvectCd);
			modelIns.put("iStateCd", iStateCd);
			modelIns.put("iRegistSeq", iRegistSeq);
			modelIns.put("iWorkSeq", iWorkSeq);
			modelIns.put("iLocalSlipNo", iLocalSlipNo);
			modelIns.put("iCustomerSlipNo", iCustomerSlipNo);
			modelIns.put("iOtherSlipNo", iOtherSlipNo);
			
			// modelIns.put("iLocalLocCd", iLocalLocCd);
			modelIns.put("iBaseLocCd", iBaseLocCd);
			
			modelIns.put("iOtherPartyLocCd", iOtherPartyLocCd);
			modelIns.put("iCustomerLocCd", iCustomerLocCd);
			modelIns.put("iWarehouseId", iWarehouseId);
			modelIns.put("iWarehouseNm", iWarehouseNm);
			modelIns.put("iCustomerId", iCustomerId);
			modelIns.put("iCustomerNm", iCustomerNm);
			modelIns.put("iShippingDt", iShippingDt);
			modelIns.put("iReceivingDt", iReceivingDt);
			modelIns.put("iInOutDt", iInOutDt);
			modelIns.put("iEventDt", iEventDt);
			modelIns.put("iForwarderCd", iForwarderCd);
			modelIns.put("iForwarderNm", iForwarderNm);
			modelIns.put("iCarId", iCarId);
			modelIns.put("iReasonId", iReasonId);
			modelIns.put("iUnitWeight", iUnitWeight);
			
			// modelIns.put("iLockReasonCd", iLockReasonCd);
			
			modelIns.put("iLockStateCd", iLockStateCd);
			modelIns.put("iRtiCd", iRtiCd);
			modelIns.put("iRtiBarcode", iRtiBarcode);
			modelIns.put("iRtiQty", iRtiQty);
			modelIns.put("iRtiRealQty", iRtiRealQty);
			modelIns.put("iRtiIndividualGrossType", iRtiIndividualGrossType);
			modelIns.put("iRtiSerial", iRtiSerial);
			modelIns.put("iRfidReadDt", iRfidReadDt);
			modelIns.put("iFromLocLocalCd", iFromLocLocalCd);
			modelIns.put("iToLocLocalCd", iToLocLocalCd);
			modelIns.put("iFromLocGlobalCd", iFromLocGlobalCd);
			modelIns.put("iToLocGlobalCd", iToLocGlobalCd);
			modelIns.put("iTotalWgtMax", iTotalWgtMax);
			modelIns.put("iTotalWgtMin", iTotalWgtMin);
			modelIns.put("iRealTotalWgt", iRealTotalWgt);
			modelIns.put("iAlertReasonCd", iAlertReasonCd);
			modelIns.put("iRtiFromLocLocalCd", iRtiFromLocLocalCd);
			modelIns.put("iRtiToLocLocalCd", iRtiToLocLocalCd);
			modelIns.put("iRtiFromLocGlobalCd", iRtiFromLocGlobalCd);
			modelIns.put("iRtiToLocGlobalCd", iRtiToLocGlobalCd);
			modelIns.put("iRtiChildSerial", iRtiChildSerial);
			modelIns.put("iRfidChildReadDt", iRfidChildReadDt);
			modelIns.put("iItemCd", iItemCd);
			modelIns.put("iCustomerItemCd", iCustomerItemCd);
			modelIns.put("iItfCd", iItfCd);
			modelIns.put("iJanCd", iJanCd);
			modelIns.put("iItemNm", iItemNm);
			modelIns.put("iMakerNm", iMakerNm);
			modelIns.put("iColorNm", iColorNm);
			modelIns.put("iItemSize", iItemSize);
			modelIns.put("iQty", iQty);
			modelIns.put("iRealQty", iRealQty);
			modelIns.put("iUnit", iUnit);
			modelIns.put("iWgt", iWgt);
			modelIns.put("iWgtArrowRate", iWgtArrowRate);
			modelIns.put("iFinallyReceivingExpiryDt", iFinallyReceivingExpiryDt);
			modelIns.put("iFinallyReceivingMakeDt", iFinallyReceivingMakeDt);
			modelIns.put("iFinallyReceivingLotNo", iFinallyReceivingLotNo);
			modelIns.put("iBestDateNum", iBestDateNum);
			modelIns.put("iBestDateUnit", iBestDateUnit);
			modelIns.put("iIndividualGrossType", iIndividualGrossType);
			modelIns.put("iItemTotalWgtMax", iItemTotalWgtMax);
			modelIns.put("iItemTotalWgtMin", iItemTotalWgtMin);
			modelIns.put("iItemRealTotalWgt", iItemRealTotalWgt);
			modelIns.put("iItemIDCd", iItemIDCd);
			modelIns.put("iOrdSeq", iOrdSeq);
			modelIns.put("iItemQty", iItemQty);
			modelIns.put("iItemRealQty", iItemRealQty);
			modelIns.put("iItemFromLocLocalCd", iItemFromLocLocalCd);
			modelIns.put("iItemToLocLocalCd", iItemToLocLocalCd);
			modelIns.put("iItemFromLocGlobalCd", iItemFromLocGlobalCd);
			modelIns.put("iItemToLocGlobalCd", iItemToLocGlobalCd);
			modelIns.put("iExpiryDt", iExpiryDt);
			modelIns.put("iExpiryDtType", iExpiryDtType);
			modelIns.put("iMakeDt", iMakeDt);
			modelIns.put("iLotNo", iLotNo);
			modelIns.put("iItemSerial", iItemSerial);
			modelIns.put("iItemAlertReasonCd", iItemAlertReasonCd);
			modelIns.put("iItemRfidReadDt", iItemRfidReadDt);
			modelIns.put("iUser", iUser);
			modelIns.put("iWorkIp", iWorkIp);
			
			modelIns.put("iPackageNo", iPackageNo);
			modelIns.put("iPackageLevel", iPackageLevel);

			return modelIns;
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}

	protected Map<String, Object> getSPCompleteParamMapKR(InterfaceBaseVOKR data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			String iWorkIp = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_IP);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			String iUserNo = loginID;

			String[] iLcId = new String[workList.size()];
			String[] iTransCustId = new String[workList.size()];
			String[] iOrdId = new String[workList.size()];
			String[] iOrdSeq = new String[workList.size()];
			String[] iWorkQty = new String[workList.size()];
			String[] iWorkWgt = new String[workList.size()];
			String[] iRefSubLotId = new String[workList.size()];
			String[] iSapBarcode = new String[workList.size()];
			String[] iFromLocCd = new String[workList.size()];
			String[] iToLocCd = new String[workList.size()];
			String[] iCustLotNo = new String[workList.size()];
			String[] iEpcCd = new String[workList.size()];
			String[] iItemSerial = new String[workList.size()];
			String[] iRfidReadDt = new String[workList.size()];
			String[] iChildCustLotNo = new String[workList.size()];
			String[] iMapItemBarCd = new String[workList.size()];
			String[] iMapItemQty = new String[workList.size()];
			String[] iRitemId = new String[workList.size()];
			String[] iItemCode = new String[workList.size()];
			String[] iWorkMemo = new String[workList.size()];
			String[] iSetType = new String[workList.size()];
			String[] iSerialNo = new String[workList.size()];
			String[] iWorkTypeMemo = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iLcId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_LC_ID));		
				iTransCustId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TRANS_CUST_ID));	
				iOrdId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_ID)); 
				iOrdSeq[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_SEQ_M)); 
				iWorkQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_WORK_QTY)); 
				iWorkWgt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_WORK_WGT)); 
				iRefSubLotId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_REF_SUB_LOT_ID));
				iSapBarcode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SAP_BARCODE)); 
				iFromLocCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_FROM_LOC_BARCODE_CD)); 
				iToLocCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TO_LOC_BARCODE_CD));
				iCustLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_CUST_LOT_NO_M)); 
				iEpcCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_EPC_CD));   			
				iItemSerial[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_ITEM_SERIAL));  
				iRfidReadDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_RFID_READ_DT));  
				iChildCustLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_CHILD_CUST_LOT_NO));  
				iMapItemBarCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_BAR_CD));  
				iMapItemQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_QTY));
				iRitemId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_RITEM_ID));   	
				iItemCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_CODE));   
				iWorkMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MEMO));   	  
				iSetType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SET_TYPE));   	  
				iSerialNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO));   	  
				iWorkTypeMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TYPE_MEMO));   	
				idx = idx + 1;			
				
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iWorkIp", iWorkIp);
			modelIns.put("iUserNo", iUserNo);
			modelIns.put("iLcId", iLcId);	
			modelIns.put("iTransCustId", iTransCustId);	
			modelIns.put("iOrdId", iOrdId);	
			modelIns.put("iOrdSeq", iOrdSeq);	
			modelIns.put("iWorkQty", iWorkQty);	
			modelIns.put("iWorkWgt", iWorkWgt);	
			modelIns.put("iRefSubLotId", iRefSubLotId);
			modelIns.put("iSapBarcode", iSapBarcode);
			modelIns.put("iFromLocCd", iFromLocCd);
			modelIns.put("iToLocCd", iToLocCd);
			modelIns.put("iCustLotNo", iCustLotNo);
			modelIns.put("iEpcCd", iEpcCd);
			modelIns.put("iItemSerial", iItemSerial);
			modelIns.put("iRfidReadDt", iRfidReadDt);
			modelIns.put("iChildCustLotNo", iChildCustLotNo);
			modelIns.put("iMapItemBarCd", iMapItemBarCd);
			modelIns.put("iMapItemQty", iMapItemQty);
			modelIns.put("iRitemId", iRitemId);
			modelIns.put("iItemCode", iItemCode);
			modelIns.put("iWorkMemo", iWorkMemo);
			modelIns.put("iSetType", iSetType);
			modelIns.put("iSerialNo", iSerialNo);
			modelIns.put("iWorkTypeMemo", iWorkTypeMemo);

			return modelIns;
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	

	protected Map<String, Object> getSPCompleteParamMapKRDate(InterfaceBaseVOKRDate data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			String iWorkIp = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_IP);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			String iUserNo = loginID;

			String[] iLcId = new String[workList.size()];
			String[] iTransCustId = new String[workList.size()];
			String[] iOrdId = new String[workList.size()];
			String[] iOrdSeq = new String[workList.size()];
			String[] iWorkQty = new String[workList.size()];
			String[] iWorkWgt = new String[workList.size()];
			String[] iRefSubLotId = new String[workList.size()];
			String[] iSapBarcode = new String[workList.size()];
			String[] iFromLocCd = new String[workList.size()];
			String[] iToLocCd = new String[workList.size()];
			String[] iCustLotNo = new String[workList.size()];			

			String[] iOrdType = new String[workList.size()];
			String[] iCustId = new String[workList.size()];
			String[] iInCustId = new String[workList.size()];
			
			String[] iEpcCd = new String[workList.size()];
			String[] iItemSerial = new String[workList.size()];
			String[] iRfidReadDt = new String[workList.size()];
			String[] iChildCustLotNo = new String[workList.size()];
			String[] iMapItemBarCd = new String[workList.size()];
			String[] iMapItemQty = new String[workList.size()];
			String[] iRitemId = new String[workList.size()];
			String[] iItemCode = new String[workList.size()];
			String[] iWorkMemo = new String[workList.size()];
			
			String[] iItemBestDateEnd = new String[workList.size()];
			String[] iBoxInQty = new String[workList.size()];
			String[] iBoxBarCd = new String[workList.size()];
			String[] iItemBarCd = new String[workList.size()];

			String[] iIssueDate = new String[workList.size()];
			String[] iMakeDate = new String[workList.size()];
			String[] iMakeLineNo = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iLcId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_LC_ID));		
				iTransCustId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TRANS_CUST_ID));	
				iOrdId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_ID)); 
				iOrdSeq[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_SEQ_M)); 
				iWorkQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_WORK_QTY)); 
				iWorkWgt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_WORK_WGT)); 
				iRefSubLotId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_REF_SUB_LOT_ID));
				iSapBarcode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SAP_BARCODE)); 
				iFromLocCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_FROM_LOC_BARCODE_CD)); 
				iToLocCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TO_LOC_BARCODE_CD));
				iCustLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_CUST_LOT_NO_M)); 
				
				iOrdType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_TYPE)); 
				iCustId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_CUST_ID)); 
				iInCustId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_IN_CUST_ID)); 
				
				iEpcCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_EPC_CD));   			
				iItemSerial[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_ITEM_SERIAL));  
				iRfidReadDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_RFID_READ_DT));  
				iChildCustLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_CHILD_CUST_LOT_NO));  
				iMapItemBarCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_BAR_CD));  
				iMapItemQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_QTY));
				iRitemId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_RITEM_ID));   	
				iItemCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_CODE));   
				iWorkMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MEMO));   					

				iItemBestDateEnd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_BEST_DATE_END));  
				iBoxInQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_BOX_IN_QTY));  
				iBoxBarCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_BOX_BAR_CD));  
				iItemBarCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ITEM_BAR_CD)); 
				iIssueDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ISSUE_DATE));  
				iMakeDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MAKE_DATE));  
				iMakeLineNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MAKE_LINE_NO));   
				idx = idx + 1;			
				
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iWorkIp", iWorkIp);
			modelIns.put("iUserNo", iUserNo);
			modelIns.put("iLcId", iLcId);	
			modelIns.put("iTransCustId", iTransCustId);	
			modelIns.put("iOrdId", iOrdId);	
			modelIns.put("iOrdSeq", iOrdSeq);	
			modelIns.put("iWorkQty", iWorkQty);	
			modelIns.put("iWorkWgt", iWorkWgt);	
			modelIns.put("iRefSubLotId", iRefSubLotId);
			modelIns.put("iSapBarcode", iSapBarcode);
			modelIns.put("iFromLocCd", iFromLocCd);
			modelIns.put("iToLocCd", iToLocCd);
			modelIns.put("iCustLotNo", iCustLotNo);

			modelIns.put("iOrdType", iOrdType);
			modelIns.put("iCustId", iCustId);
			modelIns.put("iInCustId", iInCustId);
			
			modelIns.put("iEpcCd", iEpcCd);
			modelIns.put("iItemSerial", iItemSerial);
			modelIns.put("iRfidReadDt", iRfidReadDt);
			modelIns.put("iChildCustLotNo", iChildCustLotNo);
			modelIns.put("iMapItemBarCd", iMapItemBarCd);
			modelIns.put("iMapItemQty", iMapItemQty);
			modelIns.put("iRitemId", iRitemId);
			modelIns.put("iItemCode", iItemCode);
			modelIns.put("iWorkMemo", iWorkMemo);			

			modelIns.put("iItemBestDateEnd", iItemBestDateEnd);
			modelIns.put("iBoxInQty", iBoxInQty);
			modelIns.put("iBoxBarCd", iBoxBarCd);
			modelIns.put("iItemBarCd", iItemBarCd);
			modelIns.put("iIssueDate", iIssueDate);
			modelIns.put("iMakeDate", iMakeDate);
			modelIns.put("iMakeLineNo", iMakeLineNo);

			return modelIns;
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	
	protected Map<String, Object> getSPCompleteParamMapForklift(InterfaceBaseVOForklift data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			String iWorkIp = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_IP);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			String iUserNo = loginID;

			String[] iDeviceId = new String[workList.size()];
			String[] iEpcCd = new String[workList.size()];
			String[] iPositionX = new String[workList.size()];
			String[] iPositionY = new String[workList.size()];
			String[] iPositionZ = new String[workList.size()];
			String[] iWgt = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iDeviceId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DEVICE_ID));		
				iEpcCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_EPC_CD));	
				iPositionX[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_POSITION_X)); 
				iPositionY[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_POSITION_Y)); 
				iPositionZ[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_POSITION_Z)); 
				iWgt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_WGT));  	
				idx = idx + 1;			
				
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iDeviceId", iDeviceId);
			modelIns.put("iEpcCd", iEpcCd);
			modelIns.put("iPositionX", iPositionX);	
			modelIns.put("iPositionY", iPositionY);	
			modelIns.put("iPositionZ", iPositionZ);	
			modelIns.put("iWgt", iWgt);	

			return modelIns;
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	
	protected Map<String, Object> getSPCompleteParamMapDlv(InterfaceBaseVODlv data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			String iWorkIp = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_IP);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			String iUserNo = loginID;
			
			String[] iUserId = new String[workList.size()];
			String[] iOrdId = new String[workList.size()];
			String[] iPhoneNo = new String[workList.size()];
			String[] iSerialNo = new String[workList.size()];
			String[] iDate = new String[workList.size()];
			String[] iMemo = new String[workList.size()];
			String[] iTimeArea = new String[workList.size()];
			String[] iVersion = new String[workList.size()];
			String[] iComCost = new String[workList.size()];
			String[] iCostType = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iUserId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_USER_ID));
				iOrdId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_ID));		
				iPhoneNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PHONE_NO));	
				iSerialNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO)); 
				iDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DATE)); 
				iMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MEMO)); 
				iTimeArea[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TIME_AREA)); 
				iVersion[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_VERSION)); 
				iComCost[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COM_COST)); 
				iCostType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COST_TYPE)); 
				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iUserId", iUserId);
			modelIns.put("iOrdId", iOrdId);
			modelIns.put("iPhoneNo", iPhoneNo);
			modelIns.put("iSerialNo", iSerialNo);	
			modelIns.put("iDate", iDate);	
			modelIns.put("iMemo", iMemo);	
			modelIns.put("iTimeArea", iTimeArea);	
			modelIns.put("iVersion", iVersion);
			modelIns.put("iComCost", iComCost);
			modelIns.put("iCostType", iCostType);
			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}	

	protected Map<String, Object> getSPCompleteParamMapDlvNew(InterfaceBaseVODlvNew data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			String iWorkIp = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_IP);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			String iUserNo = loginID;
			
			String[] iUserId = new String[workList.size()];
			String[] iOrdId = new String[workList.size()];
			String[] iPhoneNo = new String[workList.size()];
			String[] iSerialNo = new String[workList.size()];
			String[] iDate = new String[workList.size()];
			String[] iMemo = new String[workList.size()];
			String[] iTimeArea = new String[workList.size()];
			String[] iVersion = new String[workList.size()];
			String[] iComCost = new String[workList.size()];
			String[] iCostType = new String[workList.size()];
			String[] iSetType = new String[workList.size()];
			String[] iDlvOption1 = new String[workList.size()];
			String[] iDlvOption2 = new String[workList.size()];
			String[] iDlvOption3 = new String[workList.size()];
			String[] iDlvOption4 = new String[workList.size()];
			String[] iDlvOption5 = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iUserId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_USER_ID));
				iOrdId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_ID));		
				iPhoneNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PHONE_NO));	
				iSerialNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO)); 
				iDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DATE)); 
				iMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MEMO)); 
				iTimeArea[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TIME_AREA)); 
				iVersion[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_VERSION)); 
				iComCost[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COM_COST)); 
				iCostType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COST_TYPE)); 
				iSetType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SET_TYPE)); 
				iDlvOption1[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION1)); 
				iDlvOption2[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION2)); 
				iDlvOption3[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION3)); 
				iDlvOption4[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION4)); 
				iDlvOption5[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION5)); 
				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iUserId", iUserId);
			modelIns.put("iOrdId", iOrdId);
			modelIns.put("iPhoneNo", iPhoneNo);
			modelIns.put("iSerialNo", iSerialNo);	
			modelIns.put("iDate", iDate);	
			modelIns.put("iMemo", iMemo);	
			modelIns.put("iTimeArea", iTimeArea);	
			modelIns.put("iVersion", iVersion);
			modelIns.put("iComCost", iComCost);
			modelIns.put("iCostType", iCostType);
			modelIns.put("iSetType", iSetType);
			modelIns.put("iDlvOption1", iDlvOption1);
			modelIns.put("iDlvOption2", iDlvOption2);
			modelIns.put("iDlvOption3", iDlvOption3);
			modelIns.put("iDlvOption4", iDlvOption4);
			modelIns.put("iDlvOption5", iDlvOption5);
			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}

	protected Map<String, Object> getSPCompleteParamMapDlv200614(InterfaceBaseVODlv200614 data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			String iWorkIp = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_IP);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			String iUserNo = loginID;
			
			String[] iUserId = new String[workList.size()];
			String[] iOrdId = new String[workList.size()];
			String[] iPhoneNo = new String[workList.size()];
			String[] iSerialNo = new String[workList.size()];
			String[] iDate = new String[workList.size()];
			String[] iMemo = new String[workList.size()];
			String[] iTimeArea = new String[workList.size()];
			String[] iVersion = new String[workList.size()];
			String[] iComCost = new String[workList.size()];
			String[] iCostType = new String[workList.size()];
			String[] iSetType = new String[workList.size()];
			String[] iDlvOption1 = new String[workList.size()];
			String[] iDlvOption2 = new String[workList.size()];
			String[] iDlvOption3 = new String[workList.size()];
			String[] iDlvOption4 = new String[workList.size()];
			String[] iDlvOption5 = new String[workList.size()];
			String[] iDlvOption6 = new String[workList.size()];
			String[] iDlvOption7 = new String[workList.size()];
			String[] iDlvOption8 = new String[workList.size()];
			String[] iDlvOption9 = new String[workList.size()];
			String[] iDlvOption10 = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iUserId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_USER_ID));
				iOrdId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_ID));		
				iPhoneNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PHONE_NO));	
				iSerialNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO)); 
				iDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DATE)); 
				iMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MEMO)); 
				iTimeArea[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TIME_AREA)); 
				iVersion[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_VERSION)); 
				iComCost[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COM_COST)); 
				iCostType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COST_TYPE)); 
				iSetType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SET_TYPE)); 
				iDlvOption1[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION1)); 
				iDlvOption2[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION2)); 
				iDlvOption3[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION3)); 
				iDlvOption4[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION4)); 
				iDlvOption5[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION5)); 
				iDlvOption6[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION6)); 
				iDlvOption7[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION7)); 
				iDlvOption8[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION8)); 
				iDlvOption9[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION9)); 
				iDlvOption10[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION10)); 
				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iUserId", iUserId);
			modelIns.put("iOrdId", iOrdId);
			modelIns.put("iPhoneNo", iPhoneNo);
			modelIns.put("iSerialNo", iSerialNo);	
			modelIns.put("iDate", iDate);	
			modelIns.put("iMemo", iMemo);	
			modelIns.put("iTimeArea", iTimeArea);	
			modelIns.put("iVersion", iVersion);
			modelIns.put("iComCost", iComCost);
			modelIns.put("iCostType", iCostType);
			modelIns.put("iSetType", iSetType);
			modelIns.put("iDlvOption1", iDlvOption1);
			modelIns.put("iDlvOption2", iDlvOption2);
			modelIns.put("iDlvOption3", iDlvOption3);
			modelIns.put("iDlvOption4", iDlvOption4);
			modelIns.put("iDlvOption5", iDlvOption5);
			modelIns.put("iDlvOption6", iDlvOption6);
			modelIns.put("iDlvOption7", iDlvOption7);
			modelIns.put("iDlvOption8", iDlvOption8);
			modelIns.put("iDlvOption9", iDlvOption9);
			modelIns.put("iDlvOption10", iDlvOption10);
			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}

	protected Map<String, Object> getSPCompleteParamMapDlv211231(InterfaceBaseVODlv211231 data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			String iWorkIp = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_IP);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			String iUserNo = loginID;
			
			String[] iUserId = new String[workList.size()];
			String[] iOrdId = new String[workList.size()];
			String[] iPhoneNo = new String[workList.size()];
			String[] iSerialNo = new String[workList.size()];
			String[] iDate = new String[workList.size()];
			String[] iMemo = new String[workList.size()];
			String[] iTimeArea = new String[workList.size()];
			String[] iVersion = new String[workList.size()];
			String[] iComCost = new String[workList.size()];
			String[] iCostType = new String[workList.size()];
			String[] iSetType = new String[workList.size()];
			String[] iDlvOption1 = new String[workList.size()];
			String[] iDlvOption2 = new String[workList.size()];
			String[] iDlvOption3 = new String[workList.size()];
			String[] iDlvOption4 = new String[workList.size()];
			String[] iDlvOption5 = new String[workList.size()];
			String[] iDlvOption6 = new String[workList.size()];
			String[] iDlvOption7 = new String[workList.size()];
			String[] iDlvOption8 = new String[workList.size()];
			String[] iDlvOption9 = new String[workList.size()];
			String[] iDlvOption10 = new String[workList.size()];
			String[] iEpcCd = new String[workList.size()];
			String[] iChildCustLotNo = new String[workList.size()];
			String[] iMapItemQty = new String[workList.size()];
			String[] iMapItemBarCd = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iUserId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_USER_ID));
				iOrdId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_ID));		
				iPhoneNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PHONE_NO));	
				iSerialNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO)); 
				iDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DATE)); 
				iMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MEMO)); 
				iTimeArea[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TIME_AREA)); 
				iVersion[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_VERSION)); 
				iComCost[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COM_COST)); 
				iCostType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COST_TYPE)); 
				iSetType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SET_TYPE)); 
				iDlvOption1[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION1)); 
				iDlvOption2[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION2)); 
				iDlvOption3[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION3)); 
				iDlvOption4[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION4)); 
				iDlvOption5[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION5)); 
				iDlvOption6[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION6)); 
				iDlvOption7[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION7)); 
				iDlvOption8[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION8)); 
				iDlvOption9[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION9)); 
				iDlvOption10[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION10));
				iEpcCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_EPC_CD)); 
				iChildCustLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_CHILD_CUST_LOT_NO));  
				iMapItemBarCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_BAR_CD));  
				iMapItemQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_QTY));
				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iUserId", iUserId);
			modelIns.put("iOrdId", iOrdId);
			modelIns.put("iPhoneNo", iPhoneNo);
			modelIns.put("iSerialNo", iSerialNo);	
			modelIns.put("iDate", iDate);	
			modelIns.put("iMemo", iMemo);	
			modelIns.put("iTimeArea", iTimeArea);	
			modelIns.put("iVersion", iVersion);
			modelIns.put("iComCost", iComCost);
			modelIns.put("iCostType", iCostType);
			modelIns.put("iSetType", iSetType);
			modelIns.put("iDlvOption1", iDlvOption1);
			modelIns.put("iDlvOption2", iDlvOption2);
			modelIns.put("iDlvOption3", iDlvOption3);
			modelIns.put("iDlvOption4", iDlvOption4);
			modelIns.put("iDlvOption5", iDlvOption5);
			modelIns.put("iDlvOption6", iDlvOption6);
			modelIns.put("iDlvOption7", iDlvOption7);
			modelIns.put("iDlvOption8", iDlvOption8);
			modelIns.put("iDlvOption9", iDlvOption9);
			modelIns.put("iDlvOption10", iDlvOption10);
			modelIns.put("iEpcCd", iEpcCd);
			modelIns.put("iChildCustLotNo", iChildCustLotNo);
			modelIns.put("iMapItemBarCd", iMapItemBarCd);
			modelIns.put("iMapItemQty", iMapItemQty);
			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}

	protected Map<String, Object> getSPCompleteParamMapDlv220501(InterfaceBaseVODlv220501 data, List<Map<String, Object>> workList) throws Exception {
		try {
			String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			String terminalID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.IF_KEY_TERMINAL_ID);
			String iWorkIp = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_IP);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			String iUserNo = loginID;
			
			String[] iUserId = new String[workList.size()];
			String[] iOrdId = new String[workList.size()];
			String[] iPhoneNo = new String[workList.size()];
			String[] iSerialNo = new String[workList.size()];
			String[] iDate = new String[workList.size()];
			String[] iMemo = new String[workList.size()];
			String[] iTimeArea = new String[workList.size()];
			String[] iVersion = new String[workList.size()];
			String[] iComCost = new String[workList.size()];
			String[] iCostType = new String[workList.size()];
			String[] iSetType = new String[workList.size()];
			String[] iDlvOption1 = new String[workList.size()];
			String[] iDlvOption2 = new String[workList.size()];
			String[] iDlvOption3 = new String[workList.size()];
			String[] iDlvOption4 = new String[workList.size()];
			String[] iDlvOption5 = new String[workList.size()];
			String[] iDlvOption6 = new String[workList.size()];
			String[] iDlvOption7 = new String[workList.size()];
			String[] iDlvOption8 = new String[workList.size()];
			String[] iDlvOption9 = new String[workList.size()];
			String[] iDlvOption10 = new String[workList.size()];
			String[] iDlvOption11 = new String[workList.size()];
			String[] iDlvOption12 = new String[workList.size()];
			String[] iDlvOption13 = new String[workList.size()];
			String[] iDlvOption14 = new String[workList.size()];
			String[] iDlvOption15 = new String[workList.size()];
			String[] iDlvOption16 = new String[workList.size()];
			String[] iDlvOption17 = new String[workList.size()];
			String[] iDlvOption18 = new String[workList.size()];
			String[] iDlvOption19 = new String[workList.size()];
			String[] iDlvOption20 = new String[workList.size()];
			String[] iEpcCd = new String[workList.size()];
			String[] iChildCustLotNo = new String[workList.size()];
			String[] iMapItemQty = new String[workList.size()];
			String[] iMapItemBarCd = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iUserId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_USER_ID));
				iOrdId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_ORD_ID));		
				iPhoneNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PHONE_NO));	
				iSerialNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SERIAL_NO)); 
				iDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DATE)); 
				iMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_MEMO)); 
				iTimeArea[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_TIME_AREA)); 
				iVersion[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_VERSION)); 
				iComCost[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COM_COST)); 
				iCostType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_COST_TYPE)); 
				iSetType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SET_TYPE)); 
				iDlvOption1[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION1)); 
				iDlvOption2[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION2)); 
				iDlvOption3[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION3)); 
				iDlvOption4[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION4)); 
				iDlvOption5[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION5)); 
				iDlvOption6[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION6)); 
				iDlvOption7[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION7)); 
				iDlvOption8[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION8)); 
				iDlvOption9[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION9)); 
				iDlvOption10[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION10));
				iDlvOption11[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION11)); 
				iDlvOption12[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION12)); 
				iDlvOption13[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION13)); 
				iDlvOption14[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION14)); 
				iDlvOption15[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION15)); 
				iDlvOption16[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION16)); 
				iDlvOption17[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION17)); 
				iDlvOption18[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION18)); 
				iDlvOption19[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION19)); 
				iDlvOption20[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_DLV_OPTION20));
				iEpcCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_EPC_CD)); 
				iChildCustLotNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_CHILD_CUST_LOT_NO));  
				iMapItemBarCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_BAR_CD));  
				iMapItemQty[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_PACKING_CARGO_ITEM_LOT_LOT_ITEM_SERIAL_MAP_ITEM_QTY));
				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iUserId", iUserId);
			modelIns.put("iOrdId", iOrdId);
			modelIns.put("iPhoneNo", iPhoneNo);
			modelIns.put("iSerialNo", iSerialNo);	
			modelIns.put("iDate", iDate);	
			modelIns.put("iMemo", iMemo);	
			modelIns.put("iTimeArea", iTimeArea);	
			modelIns.put("iVersion", iVersion);
			modelIns.put("iComCost", iComCost);
			modelIns.put("iCostType", iCostType);
			modelIns.put("iSetType", iSetType);
			modelIns.put("iDlvOption1", iDlvOption1);
			modelIns.put("iDlvOption2", iDlvOption2);
			modelIns.put("iDlvOption3", iDlvOption3);
			modelIns.put("iDlvOption4", iDlvOption4);
			modelIns.put("iDlvOption5", iDlvOption5);
			modelIns.put("iDlvOption6", iDlvOption6);
			modelIns.put("iDlvOption7", iDlvOption7);
			modelIns.put("iDlvOption8", iDlvOption8);
			modelIns.put("iDlvOption9", iDlvOption9);
			modelIns.put("iDlvOption10", iDlvOption10);
			modelIns.put("iDlvOption11", iDlvOption11);
			modelIns.put("iDlvOption12", iDlvOption12);
			modelIns.put("iDlvOption13", iDlvOption13);
			modelIns.put("iDlvOption14", iDlvOption14);
			modelIns.put("iDlvOption15", iDlvOption15);
			modelIns.put("iDlvOption16", iDlvOption16);
			modelIns.put("iDlvOption17", iDlvOption17);
			modelIns.put("iDlvOption18", iDlvOption18);
			modelIns.put("iDlvOption19", iDlvOption19);
			modelIns.put("iDlvOption20", iDlvOption20);
			modelIns.put("iEpcCd", iEpcCd);
			modelIns.put("iChildCustLotNo", iChildCustLotNo);
			modelIns.put("iMapItemBarCd", iMapItemBarCd);
			modelIns.put("iMapItemQty", iMapItemQty);
			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	
	protected Map<String, Object> getSPCompleteParamMapGodomall(InterfaceBaseVOGodomall data, List<Map<String, Object>> workList) throws Exception {
		try {		
			
			
			String[] iOrderNo	  = new String[workList.size()];
			String[] iMemNo	  = new String[workList.size()];
			String[] iOrderStatus	  = new String[workList.size()];
			String[] iOrderIp	  = new String[workList.size()];
			String[] iOrderChannelFl	  = new String[workList.size()];
			String[] iOrderTypeFl	  = new String[workList.size()];
			String[] iOrderEmail	  = new String[workList.size()];
			String[] iOrderGoodsNm	  = new String[workList.size()];
			String[] iOrderGoodsCnt	  = new String[workList.size()];
			String[] iSettlePrice	  = new String[workList.size()];
			String[] iTaxSupplyPrice	  = new String[workList.size()];
			String[] iTaxVatPrice	  = new String[workList.size()];
			String[] iTaxFreePrice	  = new String[workList.size()];
			String[] iRealTaxSupplyPrice	  = new String[workList.size()];
			String[] iRealTaxvatPrice	  = new String[workList.size()];
			String[] iRealTaxFreePrice	  = new String[workList.size()];
			String[] iuseMileage	  = new String[workList.size()];
			String[] iuseDeposit	  = new String[workList.size()];
			String[] iTotalGoodsPrice	  = new String[workList.size()];
			String[] iTotalDeliveryCharge	  = new String[workList.size()];
			String[] iTotalGoodsDcPrice	  = new String[workList.size()];
			String[] iTotalMebOvlapDcPrice	  = new String[workList.size()];
			String[] iTotalCouponGoodsDcPrice	  = new String[workList.size()];
			String[] iTotalCouponOrderDcPrice	  = new String[workList.size()];
			String[] iTotalCoupDlvDcPrice	  = new String[workList.size()];
			String[] iTotalMileage	  = new String[workList.size()];
			String[] iTotalGoodsMileage	  = new String[workList.size()];
			String[] iTotalMemberMileage	  = new String[workList.size()];
			String[] iTotalCoupGoodsMilg	  = new String[workList.size()];
			String[] iTotalCoupOrdMilg	  = new String[workList.size()];
			String[] iFirstSaleFl	  = new String[workList.size()];
			String[] iSettleKind	  = new String[workList.size()];
			String[] iPaymentDt	  = new String[workList.size()];
			String[] iOrderDate	  = new String[workList.size()];
			String[] iApiOrderNo	  = new String[workList.size()];
			String[] iAddField	  = new String[workList.size()];
			String[] iMultiShippingFl	  = new String[workList.size()];
			String[] iBankName	  = new String[workList.size()];
			String[] iAccountNumber	  = new String[workList.size()];
			String[] iDepositor	  = new String[workList.size()];
			String[] iMemId	  = new String[workList.size()];
			String[] iMemGroupNm	  = new String[workList.size()];				
			String[] iMallSno	  = new String[workList.size()];
			
			String[] iOrderGoodsIdx	  = new String[workList.size()];
			String[] iOrderGoodsSno	  = new String[workList.size()];
			String[] iOrderGoodsOrderNo	  = new String[workList.size()];
			String[] iOrderGoodsMallSno	  = new String[workList.size()];
			String[] iOrderGoodsApiOrderGoodsNo	  = new String[workList.size()];
			String[] iOrderGoodsOrderCd	  = new String[workList.size()];
			String[] iOrderGoodsOrderGroupCd	  = new String[workList.size()];
			String[] iOrderGoodsEventSno	  = new String[workList.size()];
			String[] iOrderGoodsOrderStatus	  = new String[workList.size()];
			String[] iOrderGoodsOrderDlvSno	  = new String[workList.size()];
			String[] iOrderGoodsInvCompSno	  = new String[workList.size()];
			String[] iOrderGoodsInvoiceNo	  = new String[workList.size()];
			String[] iOrderGoodsScmNo	  = new String[workList.size()];
			String[] iOrderGoodsPurchaseNo	  = new String[workList.size()];
			String[] iOrderGoodsCommission	  = new String[workList.size()];
			String[] iOrderGoodsScmAdjAfterNo	  = new String[workList.size()];
			String[] iOrderGoodsGoodsType	  = new String[workList.size()];
			String[] iOrderGoodsTimeSaleFl	  = new String[workList.size()];
			String[] iOrderGoodsParentMustFl	  = new String[workList.size()];
			String[] iOrderGoodsParentGoodsNo	  = new String[workList.size()];
			String[] iOrderGoodsGoodsNo	  = new String[workList.size()];
			String[] iOrderGoodsListImageData	  = new String[workList.size()];
			String[] iOrderGoodsGoodsCd	  = new String[workList.size()];
			String[] iOrderGoodsGoodsModelNo	  = new String[workList.size()];
			String[] iOrderGoodsGoodsNm	  = new String[workList.size()];
			String[] iOrderGoodsGoodsNmStd	  = new String[workList.size()];
			String[] iOrderGoodsGoodsCnt	  = new String[workList.size()];
			String[] iOrderGoodsGoodsPrice	  = new String[workList.size()];
			String[] iOrderGoodsDivUseDep	  = new String[workList.size()];
			String[] iOrderGoodsDivUseMileage	  = new String[workList.size()];
			String[] iOrderGoodsDivUseDop	  = new String[workList.size()];
			String[] iOrderGoodsDivUseMil	  = new String[workList.size()];
			String[] iOrderGoodsDivCouDcPri	  = new String[workList.size()];
			String[] iOrderGoodsDivCouOrdMil	  = new String[workList.size()];
			String[] iOrderGoodsAddGoodsPrice	  = new String[workList.size()];
			String[] iOrderGoodsOptionPrice	  = new String[workList.size()];
			String[] iOrderGoodsOptCostPrice	  = new String[workList.size()];
			String[] iOrderGoodsOptTextPrice	  = new String[workList.size()];
			String[] iOrderGoodsFixedPrice	  = new String[workList.size()];
			String[] iOrderGoodsCostPrice	  = new String[workList.size()];
			String[] iOrderGoodsGoodsDcPrice	  = new String[workList.size()];
			String[] iOrderGoodsMemberDcPrice	  = new String[workList.size()];
			String[] iOrderGoodsMemOverDcPri	  = new String[workList.size()];
			String[] iOrderGoodsCoupDcPri	  = new String[workList.size()];
			String[] iOrderGoodsTimeSalePrice	  = new String[workList.size()];
			String[] iOrderGoodsBndBnkSalePri	  = new String[workList.size()];
			String[] iOrderGoodsMyappDcPrice	  = new String[workList.size()];
			String[] iOrderGoodsDivCollectPri	  = new String[workList.size()];
			String[] iOrderGoodsGoodsMileage	  = new String[workList.size()];
			String[] iOrderGoodsMemberMileage	  = new String[workList.size()];
			String[] iOrderGoodsCouponGoodsMil	  = new String[workList.size()];
			String[] iOrderGoodsDivCollectFl	  = new String[workList.size()];
			String[] iOrderGoodsMinusDepositFl	  = new String[workList.size()];
			String[] iOrderGoodsMinResDepFl	  = new String[workList.size()];
			String[] iOrderGoodsMinusMileageFl	  = new String[workList.size()];
			String[] iOrderGoodsMinResMilFl	  = new String[workList.size()];
			String[] iOrderGoodsPlusMileageFl	  = new String[workList.size()];
			String[] iOrderGoodsPlusResMilFl	  = new String[workList.size()];
			String[] iOrderGoodsMinusStockFl	  = new String[workList.size()];
			String[] iOrderGoodsMinResStockFl	  = new String[workList.size()];
			String[] iOrderGoodsOptionSno	  = new String[workList.size()];
			String[] iOrderGoodsOptionInfo	  = new String[workList.size()];
			String[] iOrderGoodsOptionTextInfo	  = new String[workList.size()];
			String[] iOrderGoodsCateAllCd	  = new String[workList.size()];
			String[] iOrderGoodsHscode	  = new String[workList.size()];
			String[] iOrderGoodsCancelDt	  = new String[workList.size()];
			String[] iOrderGoodsPaymentDt	  = new String[workList.size()];
			String[] iOrderGoodsInvoiceDt	  = new String[workList.size()];
			String[] iOrderGoodsDeliveryDt	  = new String[workList.size()];
			String[] iOrderGoodsDlvCompDt	  = new String[workList.size()];
			String[] iOrderGoodsFinishDt	  = new String[workList.size()];
			String[] iOrderGoodsMileageGiveDt	  = new String[workList.size()];
			String[] iOrderGoodsCheckoutData	  = new String[workList.size()];
			String[] iOrderGoodsStatOrderFl	  = new String[workList.size()];
			String[] iOrderGoodsStatGoodsFl	  = new String[workList.size()];
			String[] iOrderGoodsSendSmsFl	  = new String[workList.size()];
			String[] iOrderGoodsDlvMethodFl	  = new String[workList.size()];
			String[] iOrderGoodsEnuri	  = new String[workList.size()];
			String[] iOrderGoodsGoodsDistInfo	  = new String[workList.size()];
			String[] iOrderGoodsMilAddInfo	  = new String[workList.size()];
			String[] iOrderGoodsInflow	  = new String[workList.size()];
			String[] iOrderGoodsLinkMainTheme	  = new String[workList.size()];
			String[] iOrderGoodsVisitAddress	  = new String[workList.size()];	
			
			String[] iDataSno	  = new String[workList.size()];
			String[] iDataScmNo	  = new String[workList.size()];
			String[] iDataCommission	  = new String[workList.size()];
			String[] iDataScmAdjustNo	  = new String[workList.size()];
			String[] iDataScmAdjustAfterNo	  = new String[workList.size()];
			String[] iDataDeliveryCharge	  = new String[workList.size()];
			String[] iDataDeliveryPolicyCharge	  = new String[workList.size()];
			String[] iDataDeliveryAreaCharge	  = new String[workList.size()];
			String[] iDataDivDlvUseDep	  = new String[workList.size()];
			String[] iDataDivDlvUseMil	  = new String[workList.size()];
			String[] iDataDivDlvCharge	  = new String[workList.size()];
			String[] iDataDivMemDlvDcPrice	  = new String[workList.size()];
			String[] iDataDeliveryInsuranceFee	  = new String[workList.size()];
			String[] iDataDeliveryFixFl	  = new String[workList.size()];
			String[] iDataDeliveryWeightInfo	  = new String[workList.size()];
			String[] iDataOveDlvPolicy	  = new String[workList.size()];
			String[] iDataDeliveryCollectFl	  = new String[workList.size()];
			String[] iDataDeliveryCollectPrice	  = new String[workList.size()];
			String[] iDataDlvWholeFreePrice	  = new String[workList.size()];
			String[] iDataStatisticsOrderFl	  = new String[workList.size()];
			
			String[] iOrderInfoCd	  = new String[workList.size()];
			String[] iOrderName	  = new String[workList.size()];
			String[] iInfoOrderEmail	  = new String[workList.size()];
			String[] iOrderPhonePrefixCode	  = new String[workList.size()];
			String[] iOrderPhonePrefix	  = new String[workList.size()];
			String[] iOrderPhone	  = new String[workList.size()];
			String[] iOrderCellPhonePrefixCode	  = new String[workList.size()];
			String[] iOrderCellPhonePrefix	  = new String[workList.size()];
			String[] iOrderCellPhone	  = new String[workList.size()];
			String[] iOrderZipcode	  = new String[workList.size()];
			String[] iOrderZonecode	  = new String[workList.size()];
			String[] iOrderState	  = new String[workList.size()];
			String[] iOrderCity	  = new String[workList.size()];
			String[] iOrderAddress	  = new String[workList.size()];
			String[] iOrderAddressSub	  = new String[workList.size()];
			String[] iReceiverName	  = new String[workList.size()];
			String[] iReceiverCountryCode	  = new String[workList.size()];
			String[] iReceiverPhonePrefixCode	  = new String[workList.size()];
			String[] iReceiverPhonePrefix	  = new String[workList.size()];
			String[] iReceiverPhone	  = new String[workList.size()];
			String[] iRcvCellPhonePrefixCode	  = new String[workList.size()];
			String[] iReceiverCellPhonePrefix	  = new String[workList.size()];
			String[] iReceiverCellPhone	  = new String[workList.size()];
			String[] iReceiverUseSafeNumberFl	  = new String[workList.size()];
			String[] iReceiverSafeNumber	  = new String[workList.size()];
			String[] iReceiverSafeNumberDt	  = new String[workList.size()];
			String[] iReceiverZipcode	  = new String[workList.size()];
			String[] iReceiverZonecode	  = new String[workList.size()];
			String[] iReceiverCountry	  = new String[workList.size()];
			String[] iReceiverState	  = new String[workList.size()];
			String[] iReceiverCity	  = new String[workList.size()];
			String[] iReceiverAddress	  = new String[workList.size()];
			String[] iReceiverAddressSub	  = new String[workList.size()];
			String[] iDeliveryVisit	  = new String[workList.size()];
			String[] iVisitAddress	  = new String[workList.size()];
			String[] iVisitName	  = new String[workList.size()];
			String[] iVisitPhone	  = new String[workList.size()];
			String[] iVisitMemo	  = new String[workList.size()];
			String[] iCustomIdNumber	  = new String[workList.size()];
			String[] iOrderMemo	  = new String[workList.size()];
			String[] iPacketCode	  = new String[workList.size()];
			
			String[] iRefundUseMileage	  = new String[workList.size()];
			String[] iHandleMode	  = new String[workList.size()];
			String[] iHandleReason	  = new String[workList.size()];
			String[] iRefundPrice	  = new String[workList.size()];
			String[] iRefundUseDepsCommission	  = new String[workList.size()];
			String[] iRefundDeliveryCharge	  = new String[workList.size()];
			String[] iHandleDt	  = new String[workList.size()];
			String[] iRefundDeliveryCoupon	  = new String[workList.size()];
			String[] iRefundDlvInsuranceFee	  = new String[workList.size()];
			String[] iRefundDeliveryUseMileage	  = new String[workList.size()];
			String[] iBeforeStatus	  = new String[workList.size()];
			String[] iRefundCharge	  = new String[workList.size()];
			String[] iHandleCompleteFl	  = new String[workList.size()];
			String[] iHandleGroupCd	  = new String[workList.size()];
			String[] iHandleDetailReason	  = new String[workList.size()];
			String[] iHandleDetailReasonShowFl	  = new String[workList.size()];
			String[] iRefundDeliveryUseDeposit	  = new String[workList.size()];
			String[] iRegDt	  = new String[workList.size()];
			String[] iRefundUseDeposit	  = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iOrderNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_NO));
				iMemNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MEM_NO));
				iOrderStatus[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_STATUS));
				iOrderIp[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_IP));
				iOrderChannelFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_CHANNEL_FL));
				iOrderTypeFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_TYPE_FL));
				iOrderEmail[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_EMAIL));
				iOrderGoodsNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_NM));
				iOrderGoodsCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_CNT));
				iSettlePrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_SETTLE_PRICE));
				iTaxSupplyPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TAX_SUPPLY_PRICE));
				iTaxVatPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TAX_VAT_PRICE));
				iTaxFreePrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TAX_FREE_PRICE));
				iRealTaxSupplyPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REAL_TAX_SUPPLY_PRICE));
				iRealTaxvatPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REAL_TAXVAT_PRICE));
				iRealTaxFreePrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REAL_TAX_FREE_PRICE));
				iuseMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_USE_MILEAGE));
				iuseDeposit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_USE_DEPOSIT));
				iTotalGoodsPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_GOODS_PRICE));
				iTotalDeliveryCharge[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_DELIVERY_CHARGE));
				iTotalGoodsDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_GOODS_DC_PRICE));
				iTotalMebOvlapDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_MEMBER_OVERLAP_DC_PRICE));
				iTotalCouponGoodsDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_GOODS_DC_PRICE));
				iTotalCouponOrderDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_ORDER_DC_PRICE));
				iTotalCoupDlvDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_DELIVERY_DC_PRICE));
				iTotalMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_MILEAGE));
				iTotalGoodsMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_GOODS_MILEAGE));
				iTotalMemberMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_MEMBER_MILEAGE));
				iTotalCoupGoodsMilg[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_GOODS_MILEAGE));
				iTotalCoupOrdMilg[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_TOTAL_COUPON_ORDER_MILEAGE));
				iFirstSaleFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_FIRST_SALE_FL));
				iSettleKind[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_SETTLE_KIND));
				iPaymentDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_PAYMENT_DT));
				iOrderDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_DATE));
				iApiOrderNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_API_ORDER_NO));
				iAddField[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ADD_FIELD));
				iMultiShippingFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MULTI_SHIPPING_FL));
				iBankName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_BANK_NAME));
				iAccountNumber[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ACCOUNT_NUMBER));
				iDepositor[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DEPOSITOR));
				iMemId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MEM_ID));
				iMemGroupNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MEM_GROUP_NM));
				iMallSno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_MALL_SNO));

				iOrderGoodsIdx[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_IDX));
				iOrderGoodsSno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_SNO));
				iOrderGoodsOrderNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_NO));
				iOrderGoodsMallSno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MALL_SNO));
				iOrderGoodsApiOrderGoodsNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_API_ORDER_GOODS_NO));
				iOrderGoodsOrderCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_CD));
				iOrderGoodsOrderGroupCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_GROUP_CD));
				iOrderGoodsEventSno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_EVENT_SNO));
				iOrderGoodsOrderStatus[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_STATUS));
				iOrderGoodsOrderDlvSno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ORDER_DELIVERY_SNO));
				iOrderGoodsInvCompSno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_INVOICE_COMPANY_SNO));
				iOrderGoodsInvoiceNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_INVOICE_NO));
				iOrderGoodsScmNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_SCM_NO));
				iOrderGoodsPurchaseNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PURCHASE_NO));
				iOrderGoodsCommission[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_COMMISSION));
				iOrderGoodsScmAdjAfterNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_SCM_ADJUST_AFTER_NO));
				iOrderGoodsGoodsType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_TYPE));
				iOrderGoodsTimeSaleFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_TIME_SALE_FL));
				iOrderGoodsParentMustFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PARENT_MUST_FL));
				iOrderGoodsParentGoodsNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PARENT_GOODS_NO));
				iOrderGoodsGoodsNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_NO));
				iOrderGoodsListImageData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_LIST_IMAGE_DATA));
				iOrderGoodsGoodsCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_CD));
				iOrderGoodsGoodsModelNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_MODEL_NO));
				iOrderGoodsGoodsNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_NM));
				iOrderGoodsGoodsNmStd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_NM_STANDARD));
				iOrderGoodsGoodsCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_CNT));
				iOrderGoodsGoodsPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODSPRICE));
				iOrderGoodsDivUseDep[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_USE_DEPOSIT));
				iOrderGoodsDivUseMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_USE_MILEAGE));
				iOrderGoodsDivUseDop[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_GOODS_DELIVERY_USE_DEPOSIT));
				iOrderGoodsDivUseMil[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_GOODS_DELIVERY_USE_MILEAGE));
				iOrderGoodsDivCouDcPri[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_COUPON_ORDER_DCPRICE));
				iOrderGoodsDivCouOrdMil[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DIVISION_COUPON_ORDER_MILEAGE));
				iOrderGoodsAddGoodsPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ADD_GOODSPRICE));
				iOrderGoodsOptionPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTIONPRICE));
				iOrderGoodsOptCostPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_COSTPRICE));
				iOrderGoodsOptTextPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_TEXTPRICE));
				iOrderGoodsFixedPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_FIXEDPRICE));
				iOrderGoodsCostPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_COSTPRICE));
				iOrderGoodsGoodsDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_DCPRICE));
				iOrderGoodsMemberDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MEMBER_DCPRICE));
				iOrderGoodsMemOverDcPri[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MEMBER_OVERLAP_DCPRICE));
				iOrderGoodsCoupDcPri[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_COUPON_GOODS_DCPRICE));
				iOrderGoodsTimeSalePrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_TIME_SALEPRICE));
				iOrderGoodsBndBnkSalePri[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_BRAND_BANK_SALEPRICE));
				iOrderGoodsMyappDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MYAPP_DCPRICE));
				iOrderGoodsDivCollectPri[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_DELIVERY_COLLECTPRICE));
				iOrderGoodsGoodsMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_MILEAGE));
				iOrderGoodsMemberMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MEMBER_MILEAGE));
				iOrderGoodsCouponGoodsMil[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_COUPON_GOODS_MILEAGE));
				iOrderGoodsDivCollectFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_DELIVERY_COLLECT_FL));
				iOrderGoodsMinusDepositFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_DEPOSIT_FL));
				iOrderGoodsMinResDepFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_RESTORE_DEPOSIT_FL));
				iOrderGoodsMinusMileageFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_MILEAGE_FL));
				iOrderGoodsMinResMilFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_RESTORE_MILEAGE_FL));
				iOrderGoodsPlusMileageFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PLUS_MILEAGE_FL));
				iOrderGoodsPlusResMilFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PLUS_RESTORE_MILEAGE_FL));
				iOrderGoodsMinusStockFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_STOCK_FL));
				iOrderGoodsMinResStockFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MINUS_RESTORE_STOCK_FL));
				iOrderGoodsOptionSno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_SNO));
				iOrderGoodsOptionInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_INFO));
				iOrderGoodsOptionTextInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_OPTION_TEXT_INFO));
				iOrderGoodsCateAllCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_CATE_ALL_CD));
				iOrderGoodsHscode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_HSCODE));
				iOrderGoodsCancelDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_CANCEL_DT));
				iOrderGoodsPaymentDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_PAYMENT_DT));
				iOrderGoodsInvoiceDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_INVOICE_DT));
				iOrderGoodsDeliveryDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DELIVERY_DT));
				iOrderGoodsDlvCompDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DELIVERY_COMPLETE_DT));
				iOrderGoodsFinishDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_FINISH_DT));
				iOrderGoodsMileageGiveDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_MILEAGE_GIVE_DT));
				iOrderGoodsCheckoutData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_CHECKOUT_DATA));
				iOrderGoodsStatOrderFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_STATISTICS_ORDER_FL));
				iOrderGoodsStatGoodsFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_STATISTICS_GOODS_FL));
				iOrderGoodsSendSmsFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_SEND_SMS_FL));
				iOrderGoodsDlvMethodFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_DELIVERY_METHOD_FL));
				iOrderGoodsEnuri[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_ENURI));
				iOrderGoodsGoodsDistInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_DISCOUNT_INFO));
				iOrderGoodsMilAddInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_GOODS_MILEAGE_ADD_INFO));
				iOrderGoodsInflow[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_INFLOW));
				iOrderGoodsLinkMainTheme[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_LINK_MAIN_THEME));
				iOrderGoodsVisitAddress[idx]= getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ORDER_GOODS_VISIT_ADDRESS));
				
				iDataSno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_SNO));
				iDataScmNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_SCM_NO));
				iDataCommission[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_COMMISSION));
				iDataScmAdjustNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_SCM_ADJUST_NO));
				iDataScmAdjustAfterNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_SCM_ADJUST_AFTER_NO));
				iDataDeliveryCharge[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_CHARGE));
				iDataDeliveryPolicyCharge[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_POLICY_CHARGE));
				iDataDeliveryAreaCharge[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_AREA_CHARGE));
				iDataDivDlvUseDep[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DIVISION_DELIVERY_USE_DEPOSIT));
				iDataDivDlvUseMil[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DIVISION_DELIVERY_USE_MILEAGE));
				iDataDivDlvCharge[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DIVISION_DELIVERY_CHARGE));
				iDataDivMemDlvDcPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DIVISION_MEMBER_DELIVERY_DC_PRICE));
				iDataDeliveryInsuranceFee[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_INSURANCE_FEE));
				iDataDeliveryFixFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_FIX_FL));
				iDataDeliveryWeightInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_WEIGHT_INFO));
				iDataOveDlvPolicy[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_OVERSEAS_DELIVERY_POLICY));
				iDataDeliveryCollectFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DELIVERY_COLLECT_FL));
				iDataDeliveryCollectPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_COLLECT_PRICE));
				iDataDlvWholeFreePrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_DELIVERY_WHOLE_FREE_PRICE));
				iDataStatisticsOrderFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_DATA_STATISTICS_ORDER_FL));
				
				iOrderInfoCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_INFO_CD));
				iOrderName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_NAME));
				iInfoOrderEmail[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_EMAIL));
				iOrderPhonePrefixCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_PHONE_PREFIX_CODE));
				iOrderPhonePrefix[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_PHONE_PREFIX));
				iOrderPhone[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_PHONE));
				iOrderCellPhonePrefixCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_CELL_PHONE_PREFIX_CODE));
				iOrderCellPhonePrefix[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_CELL_PHONE_PREFIX));
				iOrderCellPhone[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_CELL_PHONE));
				iOrderZipcode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_ZIPCODE));
				iOrderZonecode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_ZONECODE));
				iOrderState[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_STATE));
				iOrderCity[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_CITY));
				iOrderAddress[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_ADDRESS));
				iOrderAddressSub[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_ADDRESS_SUB));
				iReceiverName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_NAME));
				iReceiverCountryCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_COUNTRY_CODE));
				iReceiverPhonePrefixCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_PHONE_PREFIX_CODE));
				iReceiverPhonePrefix[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_PHONE_PREFIX));
				iReceiverPhone[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_PHONE));
				iRcvCellPhonePrefixCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_CELL_PHONE_PREFIX_CODE));
				iReceiverCellPhonePrefix[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_CELL_PHONE_PREFIX));
				iReceiverCellPhone[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_CELL_PHONE));
				iReceiverUseSafeNumberFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_USE_SAFE_NUMBER_FL));
				iReceiverSafeNumber[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_SAFE_NUMBER));
				iReceiverSafeNumberDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_SAFE_NUMBER_DT));
				iReceiverZipcode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_ZIPCODE));
				iReceiverZonecode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_ZONECODE));
				iReceiverCountry[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_COUNTRY));
				iReceiverState[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_STATE));
				iReceiverCity[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_CITY));
				iReceiverAddress[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_ADDRESS));
				iReceiverAddressSub[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_RECEIVER_ADDRESS_SUB));
				iDeliveryVisit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_DELIVERY_VISIT));
				iVisitAddress[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_VISIT_ADDRESS));
				iVisitName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_VISIT_NAME));
				iVisitPhone[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_VISIT_PHONE));
				iVisitMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_VISIT_MEMO));
				iCustomIdNumber[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_CUSTOMID_NUMBER));
				iOrderMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_ORDER_MEMO));
				iPacketCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_INFO_PACKET_CODE));
				
				iRefundUseMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_USE_MILEAGE));
				iHandleMode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_MODE));
				iHandleReason[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_REASON));
				iRefundPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_PRICE));
				iRefundUseDepsCommission[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_USE_DEPOSIT_COMMISSION));
				iRefundDeliveryCharge[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_CHARGE));
				iHandleDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_DT));
				iRefundDeliveryCoupon[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_COUPON));
				iRefundDlvInsuranceFee[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_INSURANCE_FEE));
				iRefundDeliveryUseMileage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_USE_MILEAGE));
				iBeforeStatus[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_BEFORE_STATUS));
				iRefundCharge[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_CHARGE));
				iHandleCompleteFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_COMPLETE_FL));
				iHandleGroupCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_GROUP_CD));
				iHandleDetailReason[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_DETAIL_REASON));
				iHandleDetailReasonShowFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_HANDLE_DETAIL_REASON_SHOW_FL));
				iRefundDeliveryUseDeposit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_DELIVERY_USE_DEPOSIT));
				iRegDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REG_DT));
				iRefundUseDeposit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_REFUND_USE_DEPOSIT));


				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iOrderNo",iOrderNo);
			modelIns.put("iMemNo",iMemNo);
			modelIns.put("iOrderStatus",iOrderStatus);
			modelIns.put("iOrderIp",iOrderIp);
			modelIns.put("iOrderChannelFl",iOrderChannelFl);
			modelIns.put("iOrderTypeFl",iOrderTypeFl);
			modelIns.put("iOrderEmail",iOrderEmail);
			modelIns.put("iOrderGoodsNm",iOrderGoodsNm);
			modelIns.put("iOrderGoodsCnt",iOrderGoodsCnt);
			modelIns.put("iSettlePrice",iSettlePrice);
			modelIns.put("iTaxSupplyPrice",iTaxSupplyPrice);
			modelIns.put("iTaxVatPrice",iTaxVatPrice);
			modelIns.put("iTaxFreePrice",iTaxFreePrice);
			modelIns.put("iRealTaxSupplyPrice",iRealTaxSupplyPrice);
			modelIns.put("iRealTaxvatPrice",iRealTaxvatPrice);
			modelIns.put("iRealTaxFreePrice",iRealTaxFreePrice);
			modelIns.put("iuseMileage",iuseMileage);
			modelIns.put("iuseDeposit",iuseDeposit);
			modelIns.put("iTotalGoodsPrice",iTotalGoodsPrice);
			modelIns.put("iTotalDeliveryCharge",iTotalDeliveryCharge);
			modelIns.put("iTotalGoodsDcPrice",iTotalGoodsDcPrice);
			modelIns.put("iTotalMebOvlapDcPrice",iTotalMebOvlapDcPrice);
			modelIns.put("iTotalCouponGoodsDcPrice",iTotalCouponGoodsDcPrice);
			modelIns.put("iTotalCouponOrderDcPrice",iTotalCouponOrderDcPrice);
			modelIns.put("iTotalCoupDlvDcPrice",iTotalCoupDlvDcPrice);
			modelIns.put("iTotalMileage",iTotalMileage);
			modelIns.put("iTotalGoodsMileage",iTotalGoodsMileage);
			modelIns.put("iTotalMemberMileage",iTotalMemberMileage);
			modelIns.put("iTotalCoupGoodsMilg",iTotalCoupGoodsMilg);
			modelIns.put("iTotalCoupOrdMilg",iTotalCoupOrdMilg);
			modelIns.put("iFirstSaleFl",iFirstSaleFl);
			modelIns.put("iSettleKind",iSettleKind);
			modelIns.put("iPaymentDt",iPaymentDt);
			modelIns.put("iOrderDate",iOrderDate);
			modelIns.put("iApiOrderNo",iApiOrderNo);
			modelIns.put("iAddField",iAddField);
			modelIns.put("iMultiShippingFl",iMultiShippingFl);
			modelIns.put("iBankName",iBankName);
			modelIns.put("iAccountNumber",iAccountNumber);
			modelIns.put("iDepositor",iDepositor);
			modelIns.put("iMemId",iMemId);
			modelIns.put("iMemGroupNm",iMemGroupNm);
			modelIns.put("iMallSno",iMallSno);

			modelIns.put("iOrderGoodsIdx",iOrderGoodsIdx);
			modelIns.put("iOrderGoodsSno",iOrderGoodsSno);
			modelIns.put("iOrderGoodsOrderNo",iOrderGoodsOrderNo);
			modelIns.put("iOrderGoodsMallSno",iOrderGoodsMallSno);
			modelIns.put("iOrderGoodsApiOrderGoodsNo",iOrderGoodsApiOrderGoodsNo);
			modelIns.put("iOrderGoodsOrderCd",iOrderGoodsOrderCd);
			modelIns.put("iOrderGoodsOrderGroupCd",iOrderGoodsOrderGroupCd);
			modelIns.put("iOrderGoodsEventSno",iOrderGoodsEventSno);
			modelIns.put("iOrderGoodsOrderStatus",iOrderGoodsOrderStatus);
			modelIns.put("iOrderGoodsOrderDlvSno",iOrderGoodsOrderDlvSno);
			modelIns.put("iOrderGoodsInvCompSno",iOrderGoodsInvCompSno);
			modelIns.put("iOrderGoodsInvoiceNo",iOrderGoodsInvoiceNo);
			modelIns.put("iOrderGoodsScmNo",iOrderGoodsScmNo);
			modelIns.put("iOrderGoodsPurchaseNo",iOrderGoodsPurchaseNo);
			modelIns.put("iOrderGoodsCommission",iOrderGoodsCommission);
			modelIns.put("iOrderGoodsScmAdjAfterNo",iOrderGoodsScmAdjAfterNo);
			modelIns.put("iOrderGoodsGoodsType",iOrderGoodsGoodsType);
			modelIns.put("iOrderGoodsTimeSaleFl",iOrderGoodsTimeSaleFl);
			modelIns.put("iOrderGoodsParentMustFl",iOrderGoodsParentMustFl);
			modelIns.put("iOrderGoodsParentGoodsNo",iOrderGoodsParentGoodsNo);
			modelIns.put("iOrderGoodsGoodsNo",iOrderGoodsGoodsNo);
			modelIns.put("iOrderGoodsListImageData",iOrderGoodsListImageData);
			modelIns.put("iOrderGoodsGoodsCd",iOrderGoodsGoodsCd);
			modelIns.put("iOrderGoodsGoodsModelNo",iOrderGoodsGoodsModelNo);
			modelIns.put("iOrderGoodsGoodsNm",iOrderGoodsGoodsNm);
			modelIns.put("iOrderGoodsGoodsNmStd",iOrderGoodsGoodsNmStd);
			modelIns.put("iOrderGoodsGoodsCnt",iOrderGoodsGoodsCnt);
			modelIns.put("iOrderGoodsGoodsPrice",iOrderGoodsGoodsPrice);
			modelIns.put("iOrderGoodsDivUseDep",iOrderGoodsDivUseDep);
			modelIns.put("iOrderGoodsDivUseMileage",iOrderGoodsDivUseMileage);
			modelIns.put("iOrderGoodsDivUseDop",iOrderGoodsDivUseDop);
			modelIns.put("iOrderGoodsDivUseMil",iOrderGoodsDivUseMil);
			modelIns.put("iOrderGoodsDivCouDcPri",iOrderGoodsDivCouDcPri);
			modelIns.put("iOrderGoodsDivCouOrdMil",iOrderGoodsDivCouOrdMil);
			modelIns.put("iOrderGoodsAddGoodsPrice",iOrderGoodsAddGoodsPrice);
			modelIns.put("iOrderGoodsOptionPrice",iOrderGoodsOptionPrice);
			modelIns.put("iOrderGoodsOptCostPrice",iOrderGoodsOptCostPrice);
			modelIns.put("iOrderGoodsOptTextPrice",iOrderGoodsOptTextPrice);
			modelIns.put("iOrderGoodsFixedPrice",iOrderGoodsFixedPrice);
			modelIns.put("iOrderGoodsCostPrice",iOrderGoodsCostPrice);
			modelIns.put("iOrderGoodsGoodsDcPrice",iOrderGoodsGoodsDcPrice);
			modelIns.put("iOrderGoodsMemberDcPrice",iOrderGoodsMemberDcPrice);
			modelIns.put("iOrderGoodsMemOverDcPri",iOrderGoodsMemOverDcPri);
			modelIns.put("iOrderGoodsCoupDcPri",iOrderGoodsCoupDcPri);
			modelIns.put("iOrderGoodsTimeSalePrice",iOrderGoodsTimeSalePrice);
			modelIns.put("iOrderGoodsBndBnkSalePri",iOrderGoodsBndBnkSalePri);
			modelIns.put("iOrderGoodsMyappDcPrice",iOrderGoodsMyappDcPrice);
			modelIns.put("iOrderGoodsDivCollectPri",iOrderGoodsDivCollectPri);
			modelIns.put("iOrderGoodsGoodsMileage",iOrderGoodsGoodsMileage);
			modelIns.put("iOrderGoodsMemberMileage",iOrderGoodsMemberMileage);
			modelIns.put("iOrderGoodsCouponGoodsMil",iOrderGoodsCouponGoodsMil);
			modelIns.put("iOrderGoodsDivCollectFl",iOrderGoodsDivCollectFl);
			modelIns.put("iOrderGoodsMinusDepositFl",iOrderGoodsMinusDepositFl);
			modelIns.put("iOrderGoodsMinResDepFl",iOrderGoodsMinResDepFl);
			modelIns.put("iOrderGoodsMinusMileageFl",iOrderGoodsMinusMileageFl);
			modelIns.put("iOrderGoodsMinResMilFl",iOrderGoodsMinResMilFl);
			modelIns.put("iOrderGoodsPlusMileageFl",iOrderGoodsPlusMileageFl);
			modelIns.put("iOrderGoodsPlusResMilFl",iOrderGoodsPlusResMilFl);
			modelIns.put("iOrderGoodsMinusStockFl",iOrderGoodsMinusStockFl);
			modelIns.put("iOrderGoodsMinResStockFl",iOrderGoodsMinResStockFl);
			modelIns.put("iOrderGoodsOptionSno",iOrderGoodsOptionSno);
			modelIns.put("iOrderGoodsOptionInfo",iOrderGoodsOptionInfo);
			modelIns.put("iOrderGoodsOptionTextInfo",iOrderGoodsOptionTextInfo);
			modelIns.put("iOrderGoodsCateAllCd",iOrderGoodsCateAllCd);
			modelIns.put("iOrderGoodsHscode",iOrderGoodsHscode);
			modelIns.put("iOrderGoodsCancelDt",iOrderGoodsCancelDt);
			modelIns.put("iOrderGoodsPaymentDt",iOrderGoodsPaymentDt);
			modelIns.put("iOrderGoodsInvoiceDt",iOrderGoodsInvoiceDt);
			modelIns.put("iOrderGoodsDeliveryDt",iOrderGoodsDeliveryDt);
			modelIns.put("iOrderGoodsDlvCompDt",iOrderGoodsDlvCompDt);
			modelIns.put("iOrderGoodsFinishDt",iOrderGoodsFinishDt);
			modelIns.put("iOrderGoodsMileageGiveDt",iOrderGoodsMileageGiveDt);
			modelIns.put("iOrderGoodsCheckoutData",iOrderGoodsCheckoutData);
			modelIns.put("iOrderGoodsStatOrderFl",iOrderGoodsStatOrderFl);
			modelIns.put("iOrderGoodsStatGoodsFl",iOrderGoodsStatGoodsFl);
			modelIns.put("iOrderGoodsSendSmsFl",iOrderGoodsSendSmsFl);
			modelIns.put("iOrderGoodsDlvMethodFl",iOrderGoodsDlvMethodFl);
			modelIns.put("iOrderGoodsEnuri",iOrderGoodsEnuri);
			modelIns.put("iOrderGoodsGoodsDistInfo",iOrderGoodsGoodsDistInfo);
			modelIns.put("iOrderGoodsMilAddInfo",iOrderGoodsMilAddInfo);
			modelIns.put("iOrderGoodsInflow",iOrderGoodsInflow);
			modelIns.put("iOrderGoodsLinkMainTheme",iOrderGoodsLinkMainTheme);
			modelIns.put("iOrderGoodsVisitAddress",iOrderGoodsVisitAddress);

			modelIns.put("iDataSno",iDataSno);
			modelIns.put("iDataScmNo",iDataScmNo);
			modelIns.put("iDataCommission",iDataCommission);
			modelIns.put("iDataScmAdjustNo",iDataScmAdjustNo);
			modelIns.put("iDataScmAdjustAfterNo",iDataScmAdjustAfterNo);
			modelIns.put("iDataDeliveryCharge",iDataDeliveryCharge);
			modelIns.put("iDataDeliveryPolicyCharge",iDataDeliveryPolicyCharge);
			modelIns.put("iDataDeliveryAreaCharge",iDataDeliveryAreaCharge);
			modelIns.put("iDataDivDlvUseDep",iDataDivDlvUseDep);
			modelIns.put("iDataDivDlvUseMil",iDataDivDlvUseMil);
			modelIns.put("iDataDivDlvCharge",iDataDivDlvCharge);
			modelIns.put("iDataDivMemDlvDcPrice",iDataDivMemDlvDcPrice);
			modelIns.put("iDataDeliveryInsuranceFee",iDataDeliveryInsuranceFee);
			modelIns.put("iDataDeliveryFixFl",iDataDeliveryFixFl);
			modelIns.put("iDataDeliveryWeightInfo",iDataDeliveryWeightInfo);
			modelIns.put("iDataOveDlvPolicy",iDataOveDlvPolicy);
			modelIns.put("iDataDeliveryCollectFl",iDataDeliveryCollectFl);
			modelIns.put("iDataDeliveryCollectPrice",iDataDeliveryCollectPrice);
			modelIns.put("iDataDlvWholeFreePrice",iDataDlvWholeFreePrice);
			modelIns.put("iDataStatisticsOrderFl",iDataStatisticsOrderFl);

			modelIns.put("iOrderInfoCd",iOrderInfoCd);
			modelIns.put("iOrderName",iOrderName);
			modelIns.put("iInfoOrderEmail",iInfoOrderEmail);
			modelIns.put("iOrderPhonePrefixCode",iOrderPhonePrefixCode);
			modelIns.put("iOrderPhonePrefix",iOrderPhonePrefix);
			modelIns.put("iOrderPhone",iOrderPhone);
			modelIns.put("iOrderCellPhonePrefixCode",iOrderCellPhonePrefixCode);
			modelIns.put("iOrderCellPhonePrefix",iOrderCellPhonePrefix);
			modelIns.put("iOrderCellPhone",iOrderCellPhone);
			modelIns.put("iOrderZipcode",iOrderZipcode);
			modelIns.put("iOrderZonecode",iOrderZonecode);
			modelIns.put("iOrderState",iOrderState);
			modelIns.put("iOrderCity",iOrderCity);
			modelIns.put("iOrderAddress",iOrderAddress);
			modelIns.put("iOrderAddressSub",iOrderAddressSub);
			modelIns.put("iReceiverName",iReceiverName);
			modelIns.put("iReceiverCountryCode",iReceiverCountryCode);
			modelIns.put("iReceiverPhonePrefixCode",iReceiverPhonePrefixCode);
			modelIns.put("iReceiverPhonePrefix",iReceiverPhonePrefix);
			modelIns.put("iReceiverPhone",iReceiverPhone);
			modelIns.put("iRcvCellPhonePrefixCode",iRcvCellPhonePrefixCode);
			modelIns.put("iReceiverCellPhonePrefix",iReceiverCellPhonePrefix);
			modelIns.put("iReceiverCellPhone",iReceiverCellPhone);
			modelIns.put("iReceiverUseSafeNumberFl",iReceiverUseSafeNumberFl);
			modelIns.put("iReceiverSafeNumber",iReceiverSafeNumber);
			modelIns.put("iReceiverSafeNumberDt",iReceiverSafeNumberDt);
			modelIns.put("iReceiverZipcode",iReceiverZipcode);
			modelIns.put("iReceiverZonecode",iReceiverZonecode);
			modelIns.put("iReceiverCountry",iReceiverCountry);
			modelIns.put("iReceiverState",iReceiverState);
			modelIns.put("iReceiverCity",iReceiverCity);
			modelIns.put("iReceiverAddress",iReceiverAddress);
			modelIns.put("iReceiverAddressSub",iReceiverAddressSub);
			modelIns.put("iDeliveryVisit",iDeliveryVisit);
			modelIns.put("iVisitAddress",iVisitAddress);
			modelIns.put("iVisitName",iVisitName);
			modelIns.put("iVisitPhone",iVisitPhone);
			modelIns.put("iVisitMemo",iVisitMemo);
			modelIns.put("iCustomIdNumber",iCustomIdNumber);
			modelIns.put("iOrderMemo",iOrderMemo);
			modelIns.put("iPacketCode",iPacketCode);

			modelIns.put("iRefundUseMileage",iRefundUseMileage);
			modelIns.put("iHandleMode",iHandleMode);
			modelIns.put("iHandleReason",iHandleReason);
			modelIns.put("iRefundPrice",iRefundPrice);
			modelIns.put("iRefundUseDepsCommission",iRefundUseDepsCommission);
			modelIns.put("iRefundDeliveryCharge",iRefundDeliveryCharge);
			modelIns.put("iHandleDt",iHandleDt);
			modelIns.put("iRefundDeliveryCoupon",iRefundDeliveryCoupon);
			modelIns.put("iRefundDlvInsuranceFee",iRefundDlvInsuranceFee);
			modelIns.put("iRefundDeliveryUseMileage",iRefundDeliveryUseMileage);
			modelIns.put("iBeforeStatus",iBeforeStatus);
			modelIns.put("iRefundCharge",iRefundCharge);
			modelIns.put("iHandleCompleteFl",iHandleCompleteFl);
			modelIns.put("iHandleGroupCd",iHandleGroupCd);
			modelIns.put("iHandleDetailReason",iHandleDetailReason);
			modelIns.put("iHandleDetailReasonShowFl",iHandleDetailReasonShowFl);
			modelIns.put("iRefundDeliveryUseDeposit",iRefundDeliveryUseDeposit);
			modelIns.put("iRegDt",iRegDt);
			modelIns.put("iRefundUseDeposit",iRefundUseDeposit);


			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	
	protected Map<String, Object> getSPCompleteParamMapGodomallItem(InterfaceBaseVOGodomallItem data, List<Map<String, Object>> workList) throws Exception {
		try {		
			
			
			String[] iGoodsNo  = new String[workList.size()];
			String[] iGoodsNm  = new String[workList.size()];
			String[] iGoodsNmFl  = new String[workList.size()];
			String[] iGoodsNmMain  = new String[workList.size()];
			String[] iGoodsNmList  = new String[workList.size()];
			String[] iGoodsNmDetail  = new String[workList.size()];
			String[] iGoodsNmPartner  = new String[workList.size()];
			String[] iPurchaseGoodsNm  = new String[workList.size()];
			String[] iGoodsDisplayFl  = new String[workList.size()];
			String[] iGoodsDisplayMobileFl  = new String[workList.size()];
			String[] iGoodsSellFl  = new String[workList.size()];
			String[] iGoodsSellMobilFl  = new String[workList.size()];
			String[] iScmNo  = new String[workList.size()];
			String[] iApplyType  = new String[workList.size()];
			String[] iApplyMsg  = new String[workList.size()];
			String[] iApplyDt  = new String[workList.size()];
			String[] iCommission  = new String[workList.size()];
			String[] iGoodsCd  = new String[workList.size()];
			String[] iCatCd  = new String[workList.size()];
			String[] iAllCateCd  = new String[workList.size()];
			String[] iGoodsSearchWord  = new String[workList.size()];
			String[] iGoodsOpenDt  = new String[workList.size()];
			String[] iGoodsState  = new String[workList.size()];
			String[] iGoodsColor  = new String[workList.size()];
			String[] iBrandCd  = new String[workList.size()];
			String[] iMakerNm  = new String[workList.size()];
			String[] iOriginNm  = new String[workList.size()];
			String[] iGoodsModelNo  = new String[workList.size()];
			String[] iMakeYmd  = new String[workList.size()];
			String[] iLaunchYmd  = new String[workList.size()];
			String[] iEffectiveStartYmd  = new String[workList.size()];
			String[] iEffectiveEndYmd  = new String[workList.size()];
			String[] iGoodsPermission  = new String[workList.size()];
			String[] iGoodsPermissionGroup  = new String[workList.size()];
			String[] iOnlyAdultFl  = new String[workList.size()];
			String[] iImageStorage  = new String[workList.size()];
			String[] iTaxFreeFl  = new String[workList.size()];
			String[] iTaxPercent  = new String[workList.size()];
			String[] iGoodsWeight  = new String[workList.size()];
			String[] iTotalStock  = new String[workList.size()];
			String[] iStockFl  = new String[workList.size()];
			String[] iSoldOutFl  = new String[workList.size()];
			String[] iSalesUnit  = new String[workList.size()];
			String[] iMinOrderCnt  = new String[workList.size()];
			String[] iNaxOrderCnt  = new String[workList.size()];
			String[] iSalesStartYmd  = new String[workList.size()];
			String[] iSalesEndYmd  = new String[workList.size()];
			String[] iRestockFl  = new String[workList.size()];
			String[] iMileageFl  = new String[workList.size()];
			String[] iMileageGoods  = new String[workList.size()];
			String[] iMileageGoodsUnit  = new String[workList.size()];
			String[] iGoodsDiscountFl  = new String[workList.size()];
			String[] iGoodsDiscount  = new String[workList.size()];
			String[] iGoodsDiscountUnit  = new String[workList.size()];
			String[] iPayLimitFl  = new String[workList.size()];
			String[] iPayLimit  = new String[workList.size()];
			String[] iGoodsPriceString  = new String[workList.size()];
			String[] iGoodsPrice  = new String[workList.size()];
			String[] iFixedPrice  = new String[workList.size()];
			String[] iCostPrice  = new String[workList.size()];
			String[] iOptionFl  = new String[workList.size()];
			String[] iOptionDisplayFl  = new String[workList.size()];
			String[] iOptionName  = new String[workList.size()];
			String[] iOptionTextFl  = new String[workList.size()];
			String[] iAddGoodsFl  = new String[workList.size()];
			String[] iShortDescription  = new String[workList.size()];
			String[] iGoodsDescription  = new String[workList.size()];
			String[] iGoodsDescriptionMobile  = new String[workList.size()];
			String[] iDeliverySno  = new String[workList.size()];
			String[] iRelationFl  = new String[workList.size()];
			String[] iRelationSameFl  = new String[workList.size()];
			String[] iRelationGoodsNo  = new String[workList.size()];
			String[] iGoodsIconStartYmd  = new String[workList.size()];
			String[] iGoodsIconEndYmd  = new String[workList.size()];
			String[] iGoodsIconCdPeriod  = new String[workList.size()];
			String[] iGoodsIconCd  = new String[workList.size()];
			String[] iImgDetailViewFl  = new String[workList.size()];
			String[] iExternalVideoFl  = new String[workList.size()];
			String[] iExternalVideoUrl  = new String[workList.size()];
			String[] iExternalVideoWidth  = new String[workList.size()];
			String[] iExternalVideoHeight  = new String[workList.size()];
			String[] iDetailInfoDelivery  = new String[workList.size()];
			String[] iDetailInfoAS  = new String[workList.size()];
			String[] iDetailInfoRefund  = new String[workList.size()];
			String[] iDetailInfoExchange  = new String[workList.size()];
			String[] iMemo  = new String[workList.size()];
			String[] iOrderCnt  = new String[workList.size()];
			String[] iHitCnt  = new String[workList.size()];
			String[] iReviewCnt  = new String[workList.size()];
			String[] iExcelFl  = new String[workList.size()];
			String[] iRegDt  = new String[workList.size()];
			String[] iModDt  = new String[workList.size()];
			String[] iOptionData  = new String[workList.size()];
			String[] iTextOptionData  = new String[workList.size()];
			String[] iAddGoodsData  = new String[workList.size()];
			String[] iMagnifyImageData  = new String[workList.size()];
			String[] iDetailImageData  = new String[workList.size()];
			String[] iListImageData  = new String[workList.size()];
			String[] iMainImageData  = new String[workList.size()];
			String[] iGoodsMustInfoData  = new String[workList.size()];
			String[] iPurchaseNm  = new String[workList.size()];
			String[] iPurchaseCatagory  = new String[workList.size()];
			String[] iPurchasePhone  = new String[workList.size()];
			String[] iPurchaseAddress  = new String[workList.size()];
			String[] iPurchaseAddressSub  = new String[workList.size()];
			String[] iPurchaseMemo  = new String[workList.size()];
			String[] iCultureBenefitFl  = new String[workList.size()];
			String[] iEventDescription  = new String[workList.size()];
			
			String[] iOptionIdx= new String[workList.size()];
			String[] iOptionSno= new String[workList.size()];
			String[] iOptionGoodsNo= new String[workList.size()];
			String[] iOptionOptionNo= new String[workList.size()];
			String[] iOptionOptionCode= new String[workList.size()];
			String[] iOptionOptionValue1= new String[workList.size()];
			String[] iOptionOptionValue2= new String[workList.size()];
			String[] iOptionOptionValue3= new String[workList.size()];
			String[] iOptionOptionValue4= new String[workList.size()];
			String[] iOptionOptionValue5= new String[workList.size()];
			String[] iOptionOptionSellFl= new String[workList.size()];
			String[] iOptionOptionPrice= new String[workList.size()];
			String[] iOptionOptionDeliveryCode= new String[workList.size()];
			String[] iOptionConfirmRequestStock= new String[workList.size()];
			String[] iOptionOptionDeliveryFl= new String[workList.size()];
			String[] iOptionReqDt= new String[workList.size()];
			String[] iOptionModDt= new String[workList.size()];
			String[] iOptionOptionCostPrice= new String[workList.size()];
			String[] iOptionOptionSellCode= new String[workList.size()];
			String[] iOptionDeliverySmsSent= new String[workList.size()];
			String[] iOptionOptionViewFl= new String[workList.size()];
			String[] iOptionOptionImage= new String[workList.size()];
			String[] iOptionSellStopStock= new String[workList.size()];
			String[] iOptionStockCnt= new String[workList.size()];
			String[] iOptionSellStopFl= new String[workList.size()];
			String[] iOptionOptionMemo = new String[workList.size()];
			
			String[] iTextOptionGoodsNo = new String[workList.size()];
			String[] iTextOptionInputLimit = new String[workList.size()];
			String[] iTextOptionMustFl = new String[workList.size()];
			String[] iTextOptionAddPrice = new String[workList.size()];
			String[] iTextOptionOptionName = new String[workList.size()];
			String[] iTextOptionRegDt = new String[workList.size()];
			String[] iTextOptionModDt = new String[workList.size()];
			
			String[] iGoodsMustInfoIdx  = new String[workList.size()];
			
			String[] iStepDataIdx = new String[workList.size()];
			String[] iStepDataInfoValue = new String[workList.size()];
			String[] iStepDataInfoTitle = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iGoodsNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NO ));
				iGoodsNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM ));
				iGoodsNmFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_FL ));
				iGoodsNmMain[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_MAIN ));
				iGoodsNmList[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_LIST ));
				iGoodsNmDetail[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_DETAIL ));
				iGoodsNmPartner[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_NM_PARTNER ));
				iPurchaseGoodsNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_GOODS_NM ));
				iGoodsDisplayFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISPLAY_FL ));
				iGoodsDisplayMobileFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISPLAY_MOBILE_FL ));
				iGoodsSellFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_SELL_FL ));
				iGoodsSellMobilFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_SELL_MOBIL_FL ));
				iScmNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SCM_NO ));
				iApplyType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_APPLY_TYPE ));
				iApplyMsg[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_APPLY_MSG ));
				iApplyDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_APPLY_DT ));
				iCommission[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_COMMISSION ));
				iGoodsCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_CD ));
				iCatCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_CAT_CD ));
				iAllCateCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ALL_CATE_CD ));
				iGoodsSearchWord[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_SEARCH_WORD ));
				iGoodsOpenDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_OPEN_DT ));
				iGoodsState[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_STATE ));
				iGoodsColor[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_COLOR ));
				iBrandCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_BRAND_CD ));
				iMakerNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAKER_NM ));
				iOriginNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ORIGIN_NM ));
				iGoodsModelNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_MODEL_NO ));
				iMakeYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAKE_YMD ));
				iLaunchYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_LAUNCH_YMD ));
				iEffectiveStartYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EFFECTIVE_START_YMD ));
				iEffectiveEndYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EFFECTIVE_END_YMD ));
				iGoodsPermission[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_PERMISSION ));
				iGoodsPermissionGroup[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_PERMISSION_GROUP ));
				iOnlyAdultFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ONLY_ADULT_FL ));
				iImageStorage[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_IMAGE_STORAGE ));
				iTaxFreeFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TAX_FREE_FL ));
				iTaxPercent[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TAX_PERCENT ));
				iGoodsWeight[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_WEIGHT ));
				iTotalStock[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TOTAL_STOCK ));
				iStockFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_STOCK_FL ));
				iSoldOutFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SOLD_OUT_FL ));
				iSalesUnit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SALES_UNIT ));
				iMinOrderCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MIN_ORDER_CNT ));
				iNaxOrderCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAX_ORDER_CNT ));
				iSalesStartYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SALES_START_YMD ));
				iSalesEndYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SALES_END_YMD ));
				iRestockFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_RESTOCK_FL ));
				iMileageFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MILEAGE_FL ));
				iMileageGoods[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MILEAGE_GOODS ));
				iMileageGoodsUnit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MILEAGE_GOODS_UNIT ));
				iGoodsDiscountFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISCOUNT_FL ));
				iGoodsDiscount[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISCOUNT ));
				iGoodsDiscountUnit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DISCOUNT_UNIT ));
				iPayLimitFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PAY_LIMIT_FL ));
				iPayLimit[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PAY_LIMIT ));
				iGoodsPriceString[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_PRICE_STRING ));
				iGoodsPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_PRICE ));
				iFixedPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_FIXED_PRICE ));
				iCostPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_COST_PRICE ));
				iOptionFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_FL ));
				iOptionDisplayFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_DISPLAY_FL ));
				iOptionName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_NAME ));
				iOptionTextFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_TEXT_FL ));
				iAddGoodsFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ADD_GOODS_FL ));
				iShortDescription[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_SHORT_DESCRIPTION ));
				iGoodsDescription[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DESCRIPTION ));
				iGoodsDescriptionMobile[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_DESCRIPTION_MOBILE ));
				iDeliverySno[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DELIVERY_SNO ));
				iRelationFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_RELATION_FL ));
				iRelationSameFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_RELATION_SAME_FL ));
				iRelationGoodsNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_RELATION_GOODS_NO ));
				iGoodsIconStartYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_ICON_START_YMD ));
				iGoodsIconEndYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_ICON_END_YMD ));
				iGoodsIconCdPeriod[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_ICON_CD_PERIOD ));
				iGoodsIconCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_ICON_CD ));
				iImgDetailViewFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_IMG_DETAIL_VIEW_FL ));
				iExternalVideoFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXTERNAL_VIDEO_FL ));
				iExternalVideoUrl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXTERNAL_VIDEO_URL ));
				iExternalVideoWidth[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXTERNAL_VIDEO_WIDTH ));
				iExternalVideoHeight[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXTERNAL_VIDEOHEIGHT ));
				iDetailInfoDelivery[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_INFO_DELIVERY ));
				iDetailInfoAS[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_INFO_A_S ));
				iDetailInfoRefund[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_INFO_REFUND ));
				iDetailInfoExchange[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_INFO_EXCHANGE ));
				iMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MEMO ));
				iOrderCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ORDER_CNT ));
				iHitCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_HIT_CNT ));
				iReviewCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_REVIEW_CNT ));
				iExcelFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EXCEL_FL ));
				iRegDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_REG_DT ));
				iModDt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MOD_DT ));
				iOptionData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_DATA ));
				iTextOptionData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_DATA ));
				iAddGoodsData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_ADD_GOODS_DATA ));
				iMagnifyImageData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAGNIFY_IMAGE_DATA ));
				iDetailImageData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_DETAIL_IMAGE_DATA ));
				iListImageData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_LIST_IMAGE_DATA ));
				iMainImageData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_MAIN_IMAGE_DATA ));
				iGoodsMustInfoData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_MUST_INFO_DATA ));
				iPurchaseNm[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_NM ));
				iPurchaseCatagory[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_CATAGORY ));
				iPurchasePhone[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_PHONE ));
				iPurchaseAddress[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_ADDRESS ));
				iPurchaseAddressSub[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_ADDRESS_SUB ));
				iPurchaseMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_PURCHASE_MEMO ));
				iCultureBenefitFl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_CULTURE_BENEFIT_FL ));
				iEventDescription[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_EVENT_DESCRIPTION ));
				
				iOptionIdx[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_IDX));
				iOptionSno[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_SNO));
				iOptionGoodsNo[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_GOODS_NO));
				iOptionOptionNo[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_NO));
				iOptionOptionCode[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_CODE));
				iOptionOptionValue1[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE1));
				iOptionOptionValue2[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE2));
				iOptionOptionValue3[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE3));
				iOptionOptionValue4[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE4));
				iOptionOptionValue5[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VALUE5));
				iOptionOptionSellFl[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_SELL_FL));
				iOptionOptionPrice[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_PRICE));
				iOptionOptionDeliveryCode[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_DELIVERY_CODE));
				iOptionConfirmRequestStock[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_CONFIRM_REQUEST_STOCK));
				iOptionOptionDeliveryFl[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_DELIVERY_FL));
				iOptionReqDt[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_REQ_DT));
				iOptionModDt[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_MOD_DT));
				iOptionOptionCostPrice[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_COST_PRICE));
				iOptionOptionSellCode[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_SELL_CODE));
				iOptionDeliverySmsSent[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_DELI_VERY_SMS_SENT));
				iOptionOptionViewFl[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_VIEW_FL));
				iOptionOptionImage[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_IMAGE));
				iOptionSellStopStock[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_SELL_STOP_STOCK));
				iOptionStockCnt[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_STOCK_CNT));
				iOptionSellStopFl[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_SELL_STOCK_FL));
				iOptionOptionMemo[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_OPTION_OPTION_MEMO));

				iTextOptionGoodsNo[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_GOODS_NO));
				iTextOptionInputLimit[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_INPUT_LIMIT));
				iTextOptionMustFl[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_MUST_FL));
				iTextOptionAddPrice[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_ADD_PRICE));
				iTextOptionOptionName[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_OPTION_NAME));
				iTextOptionRegDt[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_REG_DT));
				iTextOptionModDt[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_TEXT_OPTION_MOD_DT));

				iGoodsMustInfoIdx [idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_GOODS_MUST_INFO_IDX));

				iStepDataIdx[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_STEP_DATA_IDX));
				iStepDataInfoValue[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_STEP_DATA_INFO_VALUE));
				iStepDataInfoTitle[idx]=getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_GODOMALL_ITEM_STEP_DATA_INFO_TITLE));
								
				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iGoodsNo",iGoodsNo);
			modelIns.put("iGoodsNm",iGoodsNm);
			modelIns.put("iGoodsNmFl",iGoodsNmFl);
			modelIns.put("iGoodsNmMain",iGoodsNmMain);
			modelIns.put("iGoodsNmList",iGoodsNmList);
			modelIns.put("iGoodsNmDetail",iGoodsNmDetail);
			modelIns.put("iGoodsNmPartner",iGoodsNmPartner);
			modelIns.put("iPurchaseGoodsNm",iPurchaseGoodsNm);
			modelIns.put("iGoodsDisplayFl",iGoodsDisplayFl);
			modelIns.put("iGoodsDisplayMobileFl",iGoodsDisplayMobileFl);
			modelIns.put("iGoodsSellFl",iGoodsSellFl);
			modelIns.put("iGoodsSellMobilFl",iGoodsSellMobilFl);
			modelIns.put("iScmNo",iScmNo);
			modelIns.put("iApplyType",iApplyType);
			modelIns.put("iApplyMsg",iApplyMsg);
			modelIns.put("iApplyDt",iApplyDt);
			modelIns.put("iCommission",iCommission);
			modelIns.put("iGoodsCd",iGoodsCd);
			modelIns.put("iCatCd",iCatCd);
			modelIns.put("iAllCateCd",iAllCateCd);
			modelIns.put("iGoodsSearchWord",iGoodsSearchWord);
			modelIns.put("iGoodsOpenDt",iGoodsOpenDt);
			modelIns.put("iGoodsState",iGoodsState);
			modelIns.put("iGoodsColor",iGoodsColor);
			modelIns.put("iBrandCd",iBrandCd);
			modelIns.put("iMakerNm",iMakerNm);
			modelIns.put("iOriginNm",iOriginNm);
			modelIns.put("iGoodsModelNo",iGoodsModelNo);
			modelIns.put("iMakeYmd",iMakeYmd);
			modelIns.put("iLaunchYmd",iLaunchYmd);
			modelIns.put("iEffectiveStartYmd",iEffectiveStartYmd);
			modelIns.put("iEffectiveEndYmd",iEffectiveEndYmd);
			modelIns.put("iGoodsPermission",iGoodsPermission);
			modelIns.put("iGoodsPermissionGroup",iGoodsPermissionGroup);
			modelIns.put("iOnlyAdultFl",iOnlyAdultFl);
			modelIns.put("iImageStorage",iImageStorage);
			modelIns.put("iTaxFreeFl",iTaxFreeFl);
			modelIns.put("iTaxPercent",iTaxPercent);
			modelIns.put("iGoodsWeight",iGoodsWeight);
			modelIns.put("iTotalStock",iTotalStock);
			modelIns.put("iStockFl",iStockFl);
			modelIns.put("iSoldOutFl",iSoldOutFl);
			modelIns.put("iSalesUnit",iSalesUnit);
			modelIns.put("iMinOrderCnt",iMinOrderCnt);
			modelIns.put("iNaxOrderCnt",iNaxOrderCnt);
			modelIns.put("iSalesStartYmd",iSalesStartYmd);
			modelIns.put("iSalesEndYmd",iSalesEndYmd);
			modelIns.put("iRestockFl",iRestockFl);
			modelIns.put("iMileageFl",iMileageFl);
			modelIns.put("iMileageGoods",iMileageGoods);
			modelIns.put("iMileageGoodsUnit",iMileageGoodsUnit);
			modelIns.put("iGoodsDiscountFl",iGoodsDiscountFl);
			modelIns.put("iGoodsDiscount",iGoodsDiscount);
			modelIns.put("iGoodsDiscountUnit",iGoodsDiscountUnit);
			modelIns.put("iPayLimitFl",iPayLimitFl);
			modelIns.put("iPayLimit",iPayLimit);
			modelIns.put("iGoodsPriceString",iGoodsPriceString);
			modelIns.put("iGoodsPrice",iGoodsPrice);
			modelIns.put("iFixedPrice",iFixedPrice);
			modelIns.put("iCostPrice",iCostPrice);
			modelIns.put("iOptionFl",iOptionFl);
			modelIns.put("iOptionDisplayFl",iOptionDisplayFl);
			modelIns.put("iOptionName",iOptionName);
			modelIns.put("iOptionTextFl",iOptionTextFl);
			modelIns.put("iAddGoodsFl",iAddGoodsFl);
			modelIns.put("iShortDescription",iShortDescription);
			modelIns.put("iGoodsDescription",iGoodsDescription);
			modelIns.put("iGoodsDescriptionMobile",iGoodsDescriptionMobile);
			modelIns.put("iDeliverySno",iDeliverySno);
			modelIns.put("iRelationFl",iRelationFl);
			modelIns.put("iRelationSameFl",iRelationSameFl);
			modelIns.put("iRelationGoodsNo",iRelationGoodsNo);
			modelIns.put("iGoodsIconStartYmd",iGoodsIconStartYmd);
			modelIns.put("iGoodsIconEndYmd",iGoodsIconEndYmd);
			modelIns.put("iGoodsIconCdPeriod",iGoodsIconCdPeriod);
			modelIns.put("iGoodsIconCd",iGoodsIconCd);
			modelIns.put("iImgDetailViewFl",iImgDetailViewFl);
			modelIns.put("iExternalVideoFl",iExternalVideoFl);
			modelIns.put("iExternalVideoUrl",iExternalVideoUrl);
			modelIns.put("iExternalVideoWidth",iExternalVideoWidth);
			modelIns.put("iExternalVideoHeight",iExternalVideoHeight);
			modelIns.put("iDetailInfoDelivery",iDetailInfoDelivery);
			modelIns.put("iDetailInfoAS",iDetailInfoAS);
			modelIns.put("iDetailInfoRefund",iDetailInfoRefund);
			modelIns.put("iDetailInfoExchange",iDetailInfoExchange);
			modelIns.put("iMemo",iMemo);
			modelIns.put("iOrderCnt",iOrderCnt);
			modelIns.put("iHitCnt",iHitCnt);
			modelIns.put("iReviewCnt",iReviewCnt);
			modelIns.put("iExcelFl",iExcelFl);
			modelIns.put("iRegDt",iRegDt);
			modelIns.put("iModDt",iModDt);
			modelIns.put("iOptionData",iOptionData);
			modelIns.put("iTextOptionData",iTextOptionData);
			modelIns.put("iAddGoodsData",iAddGoodsData);
			modelIns.put("iMagnifyImageData",iMagnifyImageData);
			modelIns.put("iDetailImageData",iDetailImageData);
			modelIns.put("iListImageData",iListImageData);
			modelIns.put("iMainImageData",iMainImageData);
			modelIns.put("iGoodsMustInfoData",iGoodsMustInfoData);
			modelIns.put("iPurchaseNm",iPurchaseNm);
			modelIns.put("iPurchaseCatagory",iPurchaseCatagory);
			modelIns.put("iPurchasePhone",iPurchasePhone);
			modelIns.put("iPurchaseAddress",iPurchaseAddress);
			modelIns.put("iPurchaseAddressSub",iPurchaseAddressSub);
			modelIns.put("iPurchaseMemo",iPurchaseMemo);
			modelIns.put("iCultureBenefitFl",iCultureBenefitFl);
			modelIns.put("iEventDescription",iEventDescription);
			
			modelIns.put("iOptionIdx",iOptionIdx);
			modelIns.put("iOptionSno",iOptionSno);
			modelIns.put("iOptionGoodsNo",iOptionGoodsNo);
			modelIns.put("iOptionOptionNo",iOptionOptionNo);
			modelIns.put("iOptionOptionCode",iOptionOptionCode);
			modelIns.put("iOptionOptionValue1",iOptionOptionValue1);
			modelIns.put("iOptionOptionValue2",iOptionOptionValue2);
			modelIns.put("iOptionOptionValue3",iOptionOptionValue3);
			modelIns.put("iOptionOptionValue4",iOptionOptionValue4);
			modelIns.put("iOptionOptionValue5",iOptionOptionValue5);
			modelIns.put("iOptionOptionSellFl",iOptionOptionSellFl);
			modelIns.put("iOptionOptionPrice",iOptionOptionPrice);
			modelIns.put("iOptionOptionDeliveryCode",iOptionOptionDeliveryCode);
			modelIns.put("iOptionConfirmRequestStock",iOptionConfirmRequestStock);
			modelIns.put("iOptionOptionDeliveryFl",iOptionOptionDeliveryFl);
			modelIns.put("iOptionReqDt",iOptionReqDt);
			modelIns.put("iOptionModDt",iOptionModDt);
			modelIns.put("iOptionOptionCostPrice",iOptionOptionCostPrice);
			modelIns.put("iOptionOptionSellCode",iOptionOptionSellCode);
			modelIns.put("iOptionDeliverySmsSent",iOptionDeliverySmsSent);
			modelIns.put("iOptionOptionViewFl",iOptionOptionViewFl);
			modelIns.put("iOptionOptionImage",iOptionOptionImage);
			modelIns.put("iOptionSellStopStock",iOptionSellStopStock);
			modelIns.put("iOptionStockCnt",iOptionStockCnt);
			modelIns.put("iOptionSellStopFl",iOptionSellStopFl);
			modelIns.put("iOptionOptionMemo",iOptionOptionMemo);
			
			modelIns.put("iTextOptionGoodsNo",iTextOptionGoodsNo);
			modelIns.put("iTextOptionInputLimit",iTextOptionInputLimit);
			modelIns.put("iTextOptionMustFl",iTextOptionMustFl);
			modelIns.put("iTextOptionAddPrice",iTextOptionAddPrice);
			modelIns.put("iTextOptionOptionName",iTextOptionOptionName);
			modelIns.put("iTextOptionRegDt",iTextOptionRegDt);
			modelIns.put("iTextOptionModDt",iTextOptionModDt);

			modelIns.put("iGoodsMustInfoIdx",iGoodsMustInfoIdx);

			modelIns.put("iStepDataIdx",iStepDataIdx);
			modelIns.put("iStepDataInfoValue",iStepDataInfoValue);
			modelIns.put("iStepDataInfoTitle",iStepDataInfoTitle);

			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	
	protected Map<String, Object> getSPCompleteParamMapShopbuy(InterfaceBaseVOShopbuy data, List<Map<String, Object>> workList) throws Exception {
		try {		
			
			String[] iOrderYmdt  = new String[workList.size()];
			String[] iOrderNo  = new String[workList.size()];
			String[] iMallNo  = new String[workList.size()];
			String[] iOrdererName  = new String[workList.size()];
			String[] iMemberId  = new String[workList.size()];
			String[] iOauthIdNo  = new String[workList.size()];
			String[] iOrdererContact1  = new String[workList.size()];
			String[] iOrdererContact2  = new String[workList.size()];
			String[] iOrderMemo  = new String[workList.size()];
			String[] iFirstCartCouponDiscountAmt  = new String[workList.size()];
			String[] iFirstPayAmt  = new String[workList.size()];
			String[] iFirstMainPayAmt  = new String[workList.size()];
			String[] iFirstSubPayAmt  = new String[workList.size()];
			String[] iLastCartCouponDiscountAmt  = new String[workList.size()];
			String[] iLastPayAmt  = new String[workList.size()];
			String[] iLastMainPayAmt  = new String[workList.size()];
			String[] iLastSubPayAmt  = new String[workList.size()];
			String[] iExtraData  = new String[workList.size()];
			String[] iMemberAdditionalInfoJson  = new String[workList.size()];
			
			String[] iDeliveryNo  = new String[workList.size()];
			String[] iInvoiceNo  = new String[workList.size()];
			String[] iInvoiceRegisterYmdt  = new String[workList.size()];
			String[] iDeliveryTemplateGroupNo  = new String[workList.size()];
			String[] iDeliveryTemplateNo  = new String[workList.size()];
			String[] iDeliveryConditionNo  = new String[workList.size()];
			String[] iDeliveryAmt  = new String[workList.size()];
			String[] iReturnDeliveryAmt  = new String[workList.size()];
			String[] iRemoteDeliveryAmt  = new String[workList.size()];
			String[] iAdjustedAmt  = new String[workList.size()];
			String[] iDeliveryCompanyType  = new String[workList.size()];
			String[] iReceiverName  = new String[workList.size()];
			String[] iCountryCd  = new String[workList.size()];
			String[] iReceiverZipCd  = new String[workList.size()];
			String[] iReceiverAddress  = new String[workList.size()];
			String[] iReceiverJibunAddress  = new String[workList.size()];
			String[] iReceiverDetailAddress  = new String[workList.size()];
			String[] iReceiverContact1  = new String[workList.size()];
			String[] iReceiverContact2  = new String[workList.size()];
			String[] iDeliveryMemo  = new String[workList.size()];
			String[] iDeliveryType  = new String[workList.size()];
			String[] iShippingMethodType  = new String[workList.size()];
			String[] iCustomsIdNumber  = new String[workList.size()];
			String[] iDeliveryAmtInAdvanceYn  = new String[workList.size()];
			String[] iDeliveryYn  = new String[workList.size()];
			String[] iCombineDeliveryYn  = new String[workList.size()];
			String[] iDivideDeliveryYn  = new String[workList.size()];
			String[] iDeliveryCombinationYn  = new String[workList.size()];
			String[] iOriginalDeliveryNo  = new String[workList.size()];

			String[] iOrderProductNo  = new String[workList.size()];
			String[] iMallProductNo  = new String[workList.size()];
			String[] iProductName  = new String[workList.size()];
			String[] iProductNameEn  = new String[workList.size()];
			String[] iProductManagementCd  = new String[workList.size()];
			String[] iImageUrl  = new String[workList.size()];
			String[] iHsCode  = new String[workList.size()];
			String[] iFirstProductCouponDiscountAmt  = new String[workList.size()];
			String[] iLastProductCouponDiscountAmt  = new String[workList.size()];
			String[] iOrderProductOptionNo  = new String[workList.size()];
			String[] iOptionDeliveryTemplateNo  = new String[workList.size()];
			String[] iOptionPartnerNo  = new String[workList.size()];
			String[] iOptionDeliveryPartnerNo  = new String[workList.size()];
			String[] iOptionCategoryNo  = new String[workList.size()];
			String[] iOptionBrandNo  = new String[workList.size()];
			String[] iOptionMallOptionNo  = new String[workList.size()];
			String[] iOptionMallAdditionalProductNo  = new String[workList.size()];
			String[] iOptionStockNo  = new String[workList.size()];
			String[] iOptionReleaseWarehouseNo  = new String[workList.size()];
			String[] iOptionOptionUseYn  = new String[workList.size()];
			String[] iOptionOptionName  = new String[workList.size()];
			String[] iOptionOptionValue  = new String[workList.size()];
			String[] iOptionOptionManagementCd  = new String[workList.size()];
			String[] iOptionImageUrl  = new String[workList.size()];
			String[] iOptionHsCode  = new String[workList.size()];
			String[] iOptionOrderCnt  = new String[workList.size()];
			String[] iOptionOriginalOrderCnt  = new String[workList.size()];
			String[] iOptionSalePrice  = new String[workList.size()];
			String[] iOptionImmediateDiscountAmt  = new String[workList.size()];
			String[] iOptionAddPrice  = new String[workList.size()];
			String[] iOptionAdditionalDiscountAmt  = new String[workList.size()];
			String[] iOptionPartnerChargeAmt  = new String[workList.size()];
			String[] iOptionAdjustedAmt  = new String[workList.size()];
			String[] iOptionCommissionRate  = new String[workList.size()];
			String[] iOptionProductType  = new String[workList.size()];
			String[] iOptionOrderOptionType  = new String[workList.size()];
			String[] iOptionOrderStatusType  = new String[workList.size()];
			String[] iOptionClaimStatusType  = new String[workList.size()];
			String[] iOptionRefundableYn  = new String[workList.size()];
			String[] iOptionShippingAreaType  = new String[workList.size()];
			String[] iOptionDeliveryInternationalYn  = new String[workList.size()];
			String[] iOptionHoldYn  = new String[workList.size()];
			String[] iOptionExchangeYn  = new String[workList.size()];
			String[] iOptionUserInputText  = new String[workList.size()];
			String[] iOptionStatusChangeYmdt  = new String[workList.size()];
			String[] iOptionOrderYmdt  = new String[workList.size()];
			String[] iOptionPayYmdt  = new String[workList.size()];
			String[] iOptionOrderAcceptYmdt  = new String[workList.size()];
			String[] iOptionReleaseReadyYmdt  = new String[workList.size()];
			String[] iOptionReleaseYmdt  = new String[workList.size()];
			String[] iOptionDeliveryCompleteYmdt  = new String[workList.size()];
			String[] iOptionBuyConfirmYmdt  = new String[workList.size()];
			String[] iOptionRegisterYmdt  = new String[workList.size()];
			String[] iOptionDeliveryYn  = new String[workList.size()];
			String[] iOptionUpdateYmdt  = new String[workList.size()];
			String[] iOptionOriginOrderProductOptionNo  = new String[workList.size()];
			String[] iOptionExtraJson  = new String[workList.size()];
			String[] iOptionSku  = new String[workList.size()];

			String[] iSetCount  = new String[workList.size()];
			String[] iSetMallOptionNo  = new String[workList.size()];
			String[] iSetMallProductNo  = new String[workList.size()];
			String[] iSetOption  = new String[workList.size()];
			String[] iSetOptionManagementCd  = new String[workList.size()];
			String[] iSetOptionPrice  = new String[workList.size()];
			String[] iSetProductManagementCd  = new String[workList.size()];
			String[] iSetProductName  = new String[workList.size()];
			String[] iSetSku  = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iOrderYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_YMDT));
				iOrderNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_NO));
				iMallNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_MALL_NO));
				iOrdererName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDERER_NAME));
				iMemberId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_MEMBER_ID));
				iOauthIdNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_OAUTH_ID_NO));
				iOrdererContact1[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDERER_CONTACT1));
				iOrdererContact2[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDERER_CONTACT2));
				iOrderMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_MEMO));
				iFirstCartCouponDiscountAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_FIRST_CART_COUPON_DISCOUNT_AMT));
				iFirstPayAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_FIRST_PAY_AMT));
				iFirstMainPayAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_FIRST_MAIN_PAY_AMT));
				iFirstSubPayAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_FIRST_SUB_PAY_AMT));
				iLastCartCouponDiscountAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_LAST_CART_COUPON_DISCOUNT_AMT));
				iLastPayAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_LAST_PAY_AMT));
				iLastMainPayAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_LAST_MAIN_PAY_AMT));
				iLastSubPayAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_LAST_SUB_PAY_AMT));
				iExtraData[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_EXTRA_DATA));
				iMemberAdditionalInfoJson[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_MEMBER_ADDITIONAL_INFO_JSON));

				iDeliveryNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_NO));
				iInvoiceNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_INVOICE_NO));
				iInvoiceRegisterYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_INVOICE_REGISTER_YMDT));
				iDeliveryTemplateGroupNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_TEMPLATE_GROUP_NO));
				iDeliveryTemplateNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_TEMPLATE_NO));
				iDeliveryConditionNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_CONDITION_NO));
				iDeliveryAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_AMT));
				iReturnDeliveryAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RETURN_DELIVERY_AMT));
				iRemoteDeliveryAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_REMOTE_DELIVERY_AMT));
				iAdjustedAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_ADJUSTED_AMT));
				iDeliveryCompanyType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_COMPANY_TYPE));
				iReceiverName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_NAME));
				iCountryCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_COUNTRY_CD));
				iReceiverZipCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_ZIP_CD));
				iReceiverAddress[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_ADDRESS));
				iReceiverJibunAddress[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_JIBUN_ADDRESS));
				iReceiverDetailAddress[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_DETAIL_ADDRESS));
				iReceiverContact1[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_CONTACT1));
				iReceiverContact2[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_RECEIVER_CONTACT2));
				iDeliveryMemo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_MEMO));
				iDeliveryType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_TYPE));
				iShippingMethodType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_SHIPPING_METHOD_TYPE));
				iCustomsIdNumber[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_CUSTOMS_ID_NUMBER));
				iDeliveryAmtInAdvanceYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_AMT_IN_ADVANCE_YN));
				iDeliveryYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_YN));
				iCombineDeliveryYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_COMBINE_DELIVERY_YN));
				iDivideDeliveryYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DIVIDE_DELIVERY_YN));
				iDeliveryCombinationYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_DELIVERY_COMBINATION_YN));
				iOriginalDeliveryNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_DELIVERY_GROUP_ORIGINAL_DELIVERY_NO));

				iOrderProductNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_ORDER_PRODUCT_NO));
				iMallProductNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_MALL_PRODUCT_NO));
				iProductName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_PRODUCT_NAME));
				iProductNameEn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_PRODUCT_NAME_EN));
				iProductManagementCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_PRODUCT_MANAGEMENT_CD));
				iImageUrl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_IMAGE_URL));
				iHsCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_HS_CODE));
				iFirstProductCouponDiscountAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_FIRST_PRODUCT_COUPON_DISCOUNT_AMT));
				iLastProductCouponDiscountAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_LAST_PRODUCT_COUPON_DISCOUNT_AMT));
				iOrderProductOptionNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_PRODUCT_OPTION_NO));
				iOptionDeliveryTemplateNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_TEMPLATE_NO));
				iOptionPartnerNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_PARTNER_NO));
				iOptionDeliveryPartnerNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_PARTNER_NO));
				iOptionCategoryNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_CATEGORY_NO));
				iOptionBrandNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_BRAND_NO));
				iOptionMallOptionNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_MALL_OPTION_NO));
				iOptionMallAdditionalProductNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_MALL_ADDITIONAL_PRODUCT_NO));
				iOptionStockNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_STOCK_NO));
				iOptionReleaseWarehouseNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_RELEASE_WAREHOUSE_NO));
				iOptionOptionUseYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_OPTION_USE_YN));
				iOptionOptionName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_OPTION_NAME));
				iOptionOptionValue[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_OPTION_VALUE));
				iOptionOptionManagementCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_OPTION_MANAGEMENT_CD));
				iOptionImageUrl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_IMAGE_URL));
				iOptionHsCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_HS_CODE));
				iOptionOrderCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_CNT));
				iOptionOriginalOrderCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORIGINAL_ORDER_CNT));
				iOptionSalePrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_SALE_PRICE));
				iOptionImmediateDiscountAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_IMMEDIATE_DISCOUNT_AMT));
				iOptionAddPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ADD_PRICE));
				iOptionAdditionalDiscountAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ADDITIONAL_DISCOUNT_AMT));
				iOptionPartnerChargeAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_PARTNER_CHARGE_AMT));
				iOptionAdjustedAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ADJUSTED_AMT));
				iOptionCommissionRate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_COMMISSION_RATE));
				iOptionProductType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_PRODUCT_TYPE));
				iOptionOrderOptionType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_OPTION_TYPE));
				iOptionOrderStatusType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_STATUS_TYPE));
				iOptionClaimStatusType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_CLAIM_STATUS_TYPE));
				iOptionRefundableYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_REFUNDABLE_YN));
				iOptionShippingAreaType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_SHIPPING_AREA_TYPE));
				iOptionDeliveryInternationalYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_INTERNATIONAL_YN));
				iOptionHoldYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_HOLD_YN));
				iOptionExchangeYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_EXCHANGE_YN));
				iOptionUserInputText[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_USER_INPUT_TEXT));
				iOptionStatusChangeYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_STATUS_CHANGE_YMDT));
				iOptionOrderYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_YMDT));
				iOptionPayYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_PAY_YMDT));
				iOptionOrderAcceptYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORDER_ACCEPT_YMDT));
				iOptionReleaseReadyYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_RELEASE_READY_YMDT));
				iOptionReleaseYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_RELEASE_YMDT));
				iOptionDeliveryCompleteYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_COMPLETE_YMDT));
				iOptionBuyConfirmYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_BUY_CONFIRM_YMDT));
				iOptionRegisterYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_REGISTER_YMDT));
				iOptionDeliveryYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_DELIVERY_YN));
				iOptionUpdateYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_UPDATE_YMDT));
				iOptionOriginOrderProductOptionNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_ORIGIN_ORDER_PRODUCT_OPTION_NO));
				iOptionExtraJson[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_EXTRA_JSON));
				iOptionSku[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_OPTION_SKU));

				iSetCount[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_COUNT));
				iSetMallOptionNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_MALL_OPTION_NO));
				iSetMallProductNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_MALL_PRODUCT_NO));
				iSetOption[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_OPTION));
				iSetOptionManagementCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_OPTION_MANAGEMENT_CD));
				iSetOptionPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_OPTION_PRICE));
				iSetProductManagementCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_PRODUCT_MANAGEMENT_CD));
				iSetProductName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_PRODUCT_NAME));
				iSetSku[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ORDER_PRODUCT_SET_OPTIONS_SKU));

				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iOrderYmdt",iOrderYmdt);
			modelIns.put("iOrderNo",iOrderNo);
			modelIns.put("iMallNo",iMallNo);
			modelIns.put("iOrdererName",iOrdererName);
			modelIns.put("iMemberId",iMemberId);
			modelIns.put("iOauthIdNo",iOauthIdNo);
			modelIns.put("iOrdererContact1",iOrdererContact1);
			modelIns.put("iOrdererContact2",iOrdererContact2);
			modelIns.put("iOrderMemo",iOrderMemo);
			modelIns.put("iFirstCartCouponDiscountAmt",iFirstCartCouponDiscountAmt);
			modelIns.put("iFirstPayAmt",iFirstPayAmt);
			modelIns.put("iFirstMainPayAmt",iFirstMainPayAmt);
			modelIns.put("iFirstSubPayAmt",iFirstSubPayAmt);
			modelIns.put("iLastCartCouponDiscountAmt",iLastCartCouponDiscountAmt);
			modelIns.put("iLastPayAmt",iLastPayAmt);
			modelIns.put("iLastMainPayAmt",iLastMainPayAmt);
			modelIns.put("iLastSubPayAmt",iLastSubPayAmt);
			modelIns.put("iExtraData",iExtraData);
			modelIns.put("iMemberAdditionalInfoJson",iMemberAdditionalInfoJson);

			modelIns.put("iDeliveryNo",iDeliveryNo);
			modelIns.put("iInvoiceNo",iInvoiceNo);
			modelIns.put("iInvoiceRegisterYmdt",iInvoiceRegisterYmdt);
			modelIns.put("iDeliveryTemplateGroupNo",iDeliveryTemplateGroupNo);
			modelIns.put("iDeliveryTemplateNo",iDeliveryTemplateNo);
			modelIns.put("iDeliveryConditionNo",iDeliveryConditionNo);
			modelIns.put("iDeliveryAmt",iDeliveryAmt);
			modelIns.put("iReturnDeliveryAmt",iReturnDeliveryAmt);
			modelIns.put("iRemoteDeliveryAmt",iRemoteDeliveryAmt);
			modelIns.put("iAdjustedAmt",iAdjustedAmt);
			modelIns.put("iDeliveryCompanyType",iDeliveryCompanyType);
			modelIns.put("iReceiverName",iReceiverName);
			modelIns.put("iCountryCd",iCountryCd);
			modelIns.put("iReceiverZipCd",iReceiverZipCd);
			modelIns.put("iReceiverAddress",iReceiverAddress);
			modelIns.put("iReceiverJibunAddress",iReceiverJibunAddress);
			modelIns.put("iReceiverDetailAddress",iReceiverDetailAddress);
			modelIns.put("iReceiverContact1",iReceiverContact1);
			modelIns.put("iReceiverContact2",iReceiverContact2);
			modelIns.put("iDeliveryMemo",iDeliveryMemo);
			modelIns.put("iDeliveryType",iDeliveryType);
			modelIns.put("iShippingMethodType",iShippingMethodType);
			modelIns.put("iCustomsIdNumber",iCustomsIdNumber);
			modelIns.put("iDeliveryAmtInAdvanceYn",iDeliveryAmtInAdvanceYn);
			modelIns.put("iDeliveryYn",iDeliveryYn);
			modelIns.put("iCombineDeliveryYn",iCombineDeliveryYn);
			modelIns.put("iDivideDeliveryYn",iDivideDeliveryYn);
			modelIns.put("iDeliveryCombinationYn",iDeliveryCombinationYn);
			modelIns.put("iOriginalDeliveryNo",iOriginalDeliveryNo);

			modelIns.put("iOrderProductNo",iOrderProductNo);
			modelIns.put("iMallProductNo",iMallProductNo);
			modelIns.put("iProductName",iProductName);
			modelIns.put("iProductNameEn",iProductNameEn);
			modelIns.put("iProductManagementCd",iProductManagementCd);
			modelIns.put("iImageUrl",iImageUrl);
			modelIns.put("iHsCode",iHsCode);
			modelIns.put("iFirstProductCouponDiscountAmt",iFirstProductCouponDiscountAmt);
			modelIns.put("iLastProductCouponDiscountAmt",iLastProductCouponDiscountAmt);
			modelIns.put("iOrderProductOptionNo",iOrderProductOptionNo);
			modelIns.put("iOptionDeliveryTemplateNo",iOptionDeliveryTemplateNo);
			modelIns.put("iOptionPartnerNo",iOptionPartnerNo);
			modelIns.put("iOptionDeliveryPartnerNo",iOptionDeliveryPartnerNo);
			modelIns.put("iOptionCategoryNo",iOptionCategoryNo);
			modelIns.put("iOptionBrandNo",iOptionBrandNo);
			modelIns.put("iOptionMallOptionNo",iOptionMallOptionNo);
			modelIns.put("iOptionMallAdditionalProductNo",iOptionMallAdditionalProductNo);
			modelIns.put("iOptionStockNo",iOptionStockNo);
			modelIns.put("iOptionReleaseWarehouseNo",iOptionReleaseWarehouseNo);
			modelIns.put("iOptionOptionUseYn",iOptionOptionUseYn);
			modelIns.put("iOptionOptionName",iOptionOptionName);
			modelIns.put("iOptionOptionValue",iOptionOptionValue);
			modelIns.put("iOptionOptionManagementCd",iOptionOptionManagementCd);
			modelIns.put("iOptionImageUrl",iOptionImageUrl);
			modelIns.put("iOptionHsCode",iOptionHsCode);
			modelIns.put("iOptionOrderCnt",iOptionOrderCnt);
			modelIns.put("iOptionOriginalOrderCnt",iOptionOriginalOrderCnt);
			modelIns.put("iOptionSalePrice",iOptionSalePrice);
			modelIns.put("iOptionImmediateDiscountAmt",iOptionImmediateDiscountAmt);
			modelIns.put("iOptionAddPrice",iOptionAddPrice);
			modelIns.put("iOptionAdditionalDiscountAmt",iOptionAdditionalDiscountAmt);
			modelIns.put("iOptionPartnerChargeAmt",iOptionPartnerChargeAmt);
			modelIns.put("iOptionAdjustedAmt",iOptionAdjustedAmt);
			modelIns.put("iOptionCommissionRate",iOptionCommissionRate);
			modelIns.put("iOptionProductType",iOptionProductType);
			modelIns.put("iOptionOrderOptionType",iOptionOrderOptionType);
			modelIns.put("iOptionOrderStatusType",iOptionOrderStatusType);
			modelIns.put("iOptionClaimStatusType",iOptionClaimStatusType);
			modelIns.put("iOptionRefundableYn",iOptionRefundableYn);
			modelIns.put("iOptionShippingAreaType",iOptionShippingAreaType);
			modelIns.put("iOptionDeliveryInternationalYn",iOptionDeliveryInternationalYn);
			modelIns.put("iOptionHoldYn",iOptionHoldYn);
			modelIns.put("iOptionExchangeYn",iOptionExchangeYn);
			modelIns.put("iOptionUserInputText",iOptionUserInputText);
			modelIns.put("iOptionStatusChangeYmdt",iOptionStatusChangeYmdt);
			modelIns.put("iOptionOrderYmdt",iOptionOrderYmdt);
			modelIns.put("iOptionPayYmdt",iOptionPayYmdt);
			modelIns.put("iOptionOrderAcceptYmdt",iOptionOrderAcceptYmdt);
			modelIns.put("iOptionReleaseReadyYmdt",iOptionReleaseReadyYmdt);
			modelIns.put("iOptionReleaseYmdt",iOptionReleaseYmdt);
			modelIns.put("iOptionDeliveryCompleteYmdt",iOptionDeliveryCompleteYmdt);
			modelIns.put("iOptionBuyConfirmYmdt",iOptionBuyConfirmYmdt);
			modelIns.put("iOptionRegisterYmdt",iOptionRegisterYmdt);
			modelIns.put("iOptionDeliveryYn",iOptionDeliveryYn);
			modelIns.put("iOptionUpdateYmdt",iOptionUpdateYmdt);
			modelIns.put("iOptionOriginOrderProductOptionNo",iOptionOriginOrderProductOptionNo);
			modelIns.put("iOptionExtraJson",iOptionExtraJson);
			modelIns.put("iOptionSku",iOptionSku);

			modelIns.put("iSetCount",iSetCount);
			modelIns.put("iSetMallOptionNo",iSetMallOptionNo);
			modelIns.put("iSetMallProductNo",iSetMallProductNo);
			modelIns.put("iSetOption",iSetOption);
			modelIns.put("iSetOptionManagementCd",iSetOptionManagementCd);
			modelIns.put("iSetOptionPrice",iSetOptionPrice);
			modelIns.put("iSetProductManagementCd",iSetProductManagementCd);
			modelIns.put("iSetProductName",iSetProductName);
			modelIns.put("iSetSku",iSetSku);

			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	
	protected Map<String, Object> getSPCompleteParamMapShopbuyItem(InterfaceBaseVOShopbuyItem data, List<Map<String, Object>> workList) throws Exception {
		try {		
			
			String[] iAccumulationRate = new String[workList.size()];
			String[] iAccumulationUseYn = new String[workList.size()];
			String[] iAddOptionImageYn = new String[workList.size()];
			String[] iAdditionalDiscountYn = new String[workList.size()];
			String[] iAdminNo = new String[workList.size()];
			String[] iApplyStatusType = new String[workList.size()];
			String[] iBrandNo = new String[workList.size()];
			String[] iCartOffEndYmdt = new String[workList.size()];
			String[] iCartOffPeriodYn = new String[workList.size()];
			String[] iCartOffStartYmdt = new String[workList.size()];
			String[] iCartUseYn = new String[workList.size()];
			String[] iCategoryNo = new String[workList.size()];
			String[] iCertificationJson = new String[workList.size()];
			String[] iCertificationType = new String[workList.size()];
			String[] iClassType = new String[workList.size()];
			String[] iCommissionRate = new String[workList.size()];
			String[] iCommissionRateType = new String[workList.size()];
			String[] iComparingPriceSite = new String[workList.size()];
			String[] iContent = new String[workList.size()];
			String[] iContentFooter = new String[workList.size()];
			String[] iContentHeader = new String[workList.size()];
			String[] iCouponYn = new String[workList.size()];
			String[] iDeleteYn = new String[workList.size()];
			String[] iDeliveryCombinationYn = new String[workList.size()];
			String[] iDeliveryCustomerInfo = new String[workList.size()];
			String[] iDeliveryInternationalYn = new String[workList.size()];
			String[] iDeliveryTemplateNo = new String[workList.size()];
			String[] iDeliveryYn = new String[workList.size()];
			String[] iDisplayBrandNo = new String[workList.size()];
			String[] iDutyInfo = new String[workList.size()];
			String[] iEanCode = new String[workList.size()];
			String[] iExpirationYmdt = new String[workList.size()];
			String[] iExtraJson = new String[workList.size()];
			String[] iFrontDisplayYn = new String[workList.size()];
			String[] iGroupType = new String[workList.size()];
			String[] iHsCode = new String[workList.size()];
			String[] iImmediateDiscountApplyPrice = new String[workList.size()];
			String[] iImmediateDiscountEndYmdt = new String[workList.size()];
			String[] iImmediateDiscountPeriodYn = new String[workList.size()];
			String[] iImmediateDiscountStartYmdt = new String[workList.size()];
			String[] iImmediateDiscountUnitType = new String[workList.size()];
			String[] iImmediateDiscountValue = new String[workList.size()];
			String[] iIsOptionUsed = new String[workList.size()];
			String[] iItemYn = new String[workList.size()];
			String[] iKeyword = new String[workList.size()];
			String[] iMallNo = new String[workList.size()];
			String[] iMallProductNo = new String[workList.size()];
			String[] iManufactureYmdt = new String[workList.size()];
			String[] iMappingKey = new String[workList.size()];
			String[] iMasterYn = new String[workList.size()];
			String[] iMaxBuyDays = new String[workList.size()];
			String[] iMaxBuyPeriodCnt = new String[workList.size()];
			String[] iMaxBuyPersonCnt = new String[workList.size()];
			String[] iMaxBuyTimeCnt = new String[workList.size()];
			String[] iMemberGradeDisplayInfo = new String[workList.size()];
			String[] iMemberGroupDisplayInfo = new String[workList.size()];
			String[] iMinBuyCnt = new String[workList.size()];
			String[] iMinorPurchaseYn = new String[workList.size()];
			String[] iNonmemberPurchaseYn = new String[workList.size()];
			String[] iParentMallProductNo = new String[workList.size()];
			String[] iPartnerChargeAmt = new String[workList.size()];
			String[] iPartnerName = new String[workList.size()];
			String[] iPartnerNo = new String[workList.size()];
			String[] iPaymentMeans = new String[workList.size()];
			String[] iPaymentMeansControlYn = new String[workList.size()];
			String[] iPlaceOrigin = new String[workList.size()];
			String[] iPlaceOriginSeq = new String[workList.size()];
			String[] iPlaceOriginsYn = new String[workList.size()];
			String[] iPlatformDisplayMobileWebYn = new String[workList.size()];
			String[] iPlatformDisplayMobileYn = new String[workList.size()];
			String[] iPlatformDisplayPcYn = new String[workList.size()];
			String[] iPlatformDisplayYn = new String[workList.size()];
			String[] iPointRate = new String[workList.size()];
			String[] iProductManagementCd = new String[workList.size()];
			String[] iProductName = new String[workList.size()];
			String[] iProductNameEn = new String[workList.size()];
			String[] iProductNo = new String[workList.size()];
			String[] iProductType = new String[workList.size()];
			String[] iPromotionText = new String[workList.size()];
			String[] iPromotionTextEndYmdt = new String[workList.size()];
			String[] iPromotionTextStartYmdt = new String[workList.size()];
			String[] iPromotionTextYn = new String[workList.size()];
			String[] iPromotionYn = new String[workList.size()];
			String[] iRefundableYn = new String[workList.size()];
			String[] iRegisterAdminNo = new String[workList.size()];
			String[] iRegisterSiteType = new String[workList.size()];
			String[] iRegisterYmdt = new String[workList.size()];
			String[] iSaleEndYmd = new String[workList.size()];
			String[] iSaleEndYmdt = new String[workList.size()];
			String[] iSaleMethodType = new String[workList.size()];
			String[] iSalePeriodType = new String[workList.size()];
			String[] iSalePrice = new String[workList.size()];
			String[] iSaleStartYmd = new String[workList.size()];
			String[] iSaleStartYmdt = new String[workList.size()];
			String[] iSaleStatusType = new String[workList.size()];
			String[] iSearchengineDisplayYn = new String[workList.size()];
			String[] iShippingAreaPartnerNo = new String[workList.size()];
			String[] iShippingAreaType = new String[workList.size()];
			String[] iSyncWmsYn = new String[workList.size()];
			String[] iTempSave = new String[workList.size()];
			String[] iUnitName = new String[workList.size()];
			String[] iUnitNameType = new String[workList.size()];
			String[] iUnitPrice = new String[workList.size()];
			String[] iUpdateAdminNo = new String[workList.size()];
			String[] iUpdateYmdt = new String[workList.size()];
			String[] iUrlDirectDisplayYn = new String[workList.size()];
			String[] iValueAddedTaxType = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {
				
				iAccumulationRate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ACCUMULATION_RATE));
				iAccumulationUseYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ACCUMULATION_USE_YN));
				iAddOptionImageYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ADD_OPTION_IMAGE_YN));
				iAdditionalDiscountYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ADDITIONAL_DISCOUNT_YN));
				iAdminNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ADMIN_NO));
				iApplyStatusType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_APPLY_STATUS_TYPE));
				iBrandNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_BRAND_NO));
				iCartOffEndYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CART_OFF_END_YMDT));
				iCartOffPeriodYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CART_OFF_PERIOD_YN));
				iCartOffStartYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CART_OFF_START_YMDT));
				iCartUseYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CART_USE_YN));
				iCategoryNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CATEGORY_NO));
				iCertificationJson[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CERTIFICATION_JSON));
				iCertificationType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CERTIFICATION_TYPE));
				iClassType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CLASS_TYPE));
				iCommissionRate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_COMMISSION_RATE));
				iCommissionRateType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_COMMISSION_RATE_TYPE));
				iComparingPriceSite[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_COMPARING_PRICE_SITE));
				iContent[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CONTENT));
				iContentFooter[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CONTENT_FOOTER));
				iContentHeader[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_CONTENT_HEADER));
				iCouponYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_COUPON_YN));
				iDeleteYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELETE_YN));
				iDeliveryCombinationYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_COMBINATION_YN));
				iDeliveryCustomerInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_CUSTOMER_INFO));
				iDeliveryInternationalYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_INTERNATIONAL_YN));
				iDeliveryTemplateNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_TEMPLATE_NO));
				iDeliveryYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DELIVERY_YN));
				iDisplayBrandNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DISPLAY_BRAND_NO));
				iDutyInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_DUTY_INFO));
				iEanCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_EAN_CODE));
				iExpirationYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_EXPIRATION_YMDT));
				iExtraJson[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_EXTRA_JSON));
				iFrontDisplayYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_FRONT_DISPLAY_YN));
				iGroupType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_GROUP_TYPE));
				iHsCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_HS_CODE));
				iImmediateDiscountApplyPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_APPLY_PRICE));
				iImmediateDiscountEndYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_END_YMDT));
				iImmediateDiscountPeriodYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_PERIOD_YN));
				iImmediateDiscountStartYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_START_YMDT));
				iImmediateDiscountUnitType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_UNIT_TYPE));
				iImmediateDiscountValue[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IMMEDIATE_DISCOUNT_VALUE));
				iIsOptionUsed[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_IS_OPTION_USED));
				iItemYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_ITEM_YN));
				iKeyword[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_KEYWORD));
				iMallNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MALL_NO));
				iMallProductNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MALL_PRODUCT_NO));
				iManufactureYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MANUFACTURE_YMDT));
				iMappingKey[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAPPING_KEY));
				iMasterYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MASTER_YN));
				iMaxBuyDays[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAX_BUY_DAYS));
				iMaxBuyPeriodCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAX_BUY_PERIOD_CNT));
				iMaxBuyPersonCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAX_BUY_PERSON_CNT));
				iMaxBuyTimeCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MAX_BUY_TIME_CNT));
				iMemberGradeDisplayInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MEMBER_GRADE_DISPLAY_INFO));
				iMemberGroupDisplayInfo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MEMBER_GROUP_DISPLAY_INFO));
				iMinBuyCnt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MIN_BUY_CNT));
				iMinorPurchaseYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_MINOR_PURCHASE_YN));
				iNonmemberPurchaseYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_NONMEMBER_PURCHASE_YN));
				iParentMallProductNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PARENT_MALL_PRODUCT_NO));
				iPartnerChargeAmt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PARTNER_CHARGE_AMT));
				iPartnerName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PARTNER_NAME));
				iPartnerNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PARTNER_NO));
				iPaymentMeans[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PAYMENT_MEANS));
				iPaymentMeansControlYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PAYMENT_MEANS_CONTROL_YN));
				iPlaceOrigin[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLACE_ORIGIN));
				iPlaceOriginSeq[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLACE_ORIGIN_SEQ));
				iPlaceOriginsYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLACE_ORIGINS_YN));
				iPlatformDisplayMobileWebYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLATFORM_DISPLAY_MOBILE_WEB_YN));
				iPlatformDisplayMobileYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLATFORM_DISPLAY_MOBILE_YN));
				iPlatformDisplayPcYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLATFORM_DISPLAY_PC_YN));
				iPlatformDisplayYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PLATFORM_DISPLAY_YN));
				iPointRate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_POINT_RATE));
				iProductManagementCd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_MANAGEMENT_CD));
				iProductName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_NAME));
				iProductNameEn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_NAME_EN));
				iProductNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_NO));
				iProductType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PRODUCT_TYPE));
				iPromotionText[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_TEXT));
				iPromotionTextEndYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_TEXT_END_YMDT));
				iPromotionTextStartYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_TEXT_START_YMDT));
				iPromotionTextYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_TEXT_YN));
				iPromotionYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_PROMOTION_YN));
				iRefundableYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_REFUNDABLE_YN));
				iRegisterAdminNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_REGISTER_ADMIN_NO));
				iRegisterSiteType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_REGISTER_SITE_TYPE));
				iRegisterYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_REGISTER_YMDT));
				iSaleEndYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_END_YMD));
				iSaleEndYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_END_YMDT));
				iSaleMethodType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_METHOD_TYPE));
				iSalePeriodType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_PERIOD_TYPE));
				iSalePrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_PRICE));
				iSaleStartYmd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_START_YMD));
				iSaleStartYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_START_YMDT));
				iSaleStatusType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SALE_STATUS_TYPE));
				iSearchengineDisplayYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SEARCHENGINE_DISPLAY_YN));
				iShippingAreaPartnerNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SHIPPING_AREA_PARTNER_NO));
				iShippingAreaType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SHIPPING_AREA_TYPE));
				iSyncWmsYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_SYNC_WMS_YN));
				iTempSave[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_TEMP_SAVE));
				iUnitName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UNIT_NAME));
				iUnitNameType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UNIT_NAME_TYPE));
				iUnitPrice[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UNIT_PRICE));
				iUpdateAdminNo[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UPDATE_ADMIN_NO));
				iUpdateYmdt[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_UPDATE_YMDT));
				iUrlDirectDisplayYn[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_URL_DIRECT_DISPLAY_YN));
				iValueAddedTaxType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_SHOPBUY_ITEM_VALUE_ADDED_TAX_TYPE));

				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iAccumulationRate",iAccumulationRate);
			modelIns.put("iAccumulationUseYn",iAccumulationUseYn);
			modelIns.put("iAddOptionImageYn",iAddOptionImageYn);
			modelIns.put("iAdditionalDiscountYn",iAdditionalDiscountYn);
			modelIns.put("iAdminNo",iAdminNo);
			modelIns.put("iApplyStatusType",iApplyStatusType);
			modelIns.put("iBrandNo",iBrandNo);
			modelIns.put("iCartOffEndYmdt",iCartOffEndYmdt);
			modelIns.put("iCartOffPeriodYn",iCartOffPeriodYn);
			modelIns.put("iCartOffStartYmdt",iCartOffStartYmdt);
			modelIns.put("iCartUseYn",iCartUseYn);
			modelIns.put("iCategoryNo",iCategoryNo);
			modelIns.put("iCertificationJson",iCertificationJson);
			modelIns.put("iCertificationType",iCertificationType);
			modelIns.put("iClassType",iClassType);
			modelIns.put("iCommissionRate",iCommissionRate);
			modelIns.put("iCommissionRateType",iCommissionRateType);
			modelIns.put("iComparingPriceSite",iComparingPriceSite);
			modelIns.put("iContent",iContent);
			modelIns.put("iContentFooter",iContentFooter);
			modelIns.put("iContentHeader",iContentHeader);
			modelIns.put("iCouponYn",iCouponYn);
			modelIns.put("iDeleteYn",iDeleteYn);
			modelIns.put("iDeliveryCombinationYn",iDeliveryCombinationYn);
			modelIns.put("iDeliveryCustomerInfo",iDeliveryCustomerInfo);
			modelIns.put("iDeliveryInternationalYn",iDeliveryInternationalYn);
			modelIns.put("iDeliveryTemplateNo",iDeliveryTemplateNo);
			modelIns.put("iDeliveryYn",iDeliveryYn);
			modelIns.put("iDisplayBrandNo",iDisplayBrandNo);
			modelIns.put("iDutyInfo",iDutyInfo);
			modelIns.put("iEanCode",iEanCode);
			modelIns.put("iExpirationYmdt",iExpirationYmdt);
			modelIns.put("iExtraJson",iExtraJson);
			modelIns.put("iFrontDisplayYn",iFrontDisplayYn);
			modelIns.put("iGroupType",iGroupType);
			modelIns.put("iHsCode",iHsCode);
			modelIns.put("iImmediateDiscountApplyPrice",iImmediateDiscountApplyPrice);
			modelIns.put("iImmediateDiscountEndYmdt",iImmediateDiscountEndYmdt);
			modelIns.put("iImmediateDiscountPeriodYn",iImmediateDiscountPeriodYn);
			modelIns.put("iImmediateDiscountStartYmdt",iImmediateDiscountStartYmdt);
			modelIns.put("iImmediateDiscountUnitType",iImmediateDiscountUnitType);
			modelIns.put("iImmediateDiscountValue",iImmediateDiscountValue);
			modelIns.put("iIsOptionUsed",iIsOptionUsed);
			modelIns.put("iItemYn",iItemYn);
			modelIns.put("iKeyword",iKeyword);
			modelIns.put("iMallNo",iMallNo);
			modelIns.put("iMallProductNo",iMallProductNo);
			modelIns.put("iManufactureYmdt",iManufactureYmdt);
			modelIns.put("iMappingKey",iMappingKey);
			modelIns.put("iMasterYn",iMasterYn);
			modelIns.put("iMaxBuyDays",iMaxBuyDays);
			modelIns.put("iMaxBuyPeriodCnt",iMaxBuyPeriodCnt);
			modelIns.put("iMaxBuyPersonCnt",iMaxBuyPersonCnt);
			modelIns.put("iMaxBuyTimeCnt",iMaxBuyTimeCnt);
			modelIns.put("iMemberGradeDisplayInfo",iMemberGradeDisplayInfo);
			modelIns.put("iMemberGroupDisplayInfo",iMemberGroupDisplayInfo);
			modelIns.put("iMinBuyCnt",iMinBuyCnt);
			modelIns.put("iMinorPurchaseYn",iMinorPurchaseYn);
			modelIns.put("iNonmemberPurchaseYn",iNonmemberPurchaseYn);
			modelIns.put("iParentMallProductNo",iParentMallProductNo);
			modelIns.put("iPartnerChargeAmt",iPartnerChargeAmt);
			modelIns.put("iPartnerName",iPartnerName);
			modelIns.put("iPartnerNo",iPartnerNo);
			modelIns.put("iPaymentMeans",iPaymentMeans);
			modelIns.put("iPaymentMeansControlYn",iPaymentMeansControlYn);
			modelIns.put("iPlaceOrigin",iPlaceOrigin);
			modelIns.put("iPlaceOriginSeq",iPlaceOriginSeq);
			modelIns.put("iPlaceOriginsYn",iPlaceOriginsYn);
			modelIns.put("iPlatformDisplayMobileWebYn",iPlatformDisplayMobileWebYn);
			modelIns.put("iPlatformDisplayMobileYn",iPlatformDisplayMobileYn);
			modelIns.put("iPlatformDisplayPcYn",iPlatformDisplayPcYn);
			modelIns.put("iPlatformDisplayYn",iPlatformDisplayYn);
			modelIns.put("iPointRate",iPointRate);
			modelIns.put("iProductManagementCd",iProductManagementCd);
			modelIns.put("iProductName",iProductName);
			modelIns.put("iProductNameEn",iProductNameEn);
			modelIns.put("iProductNo",iProductNo);
			modelIns.put("iProductType",iProductType);
			modelIns.put("iPromotionText",iPromotionText);
			modelIns.put("iPromotionTextEndYmdt",iPromotionTextEndYmdt);
			modelIns.put("iPromotionTextStartYmdt",iPromotionTextStartYmdt);
			modelIns.put("iPromotionTextYn",iPromotionTextYn);
			modelIns.put("iPromotionYn",iPromotionYn);
			modelIns.put("iRefundableYn",iRefundableYn);
			modelIns.put("iRegisterAdminNo",iRegisterAdminNo);
			modelIns.put("iRegisterSiteType",iRegisterSiteType);
			modelIns.put("iRegisterYmdt",iRegisterYmdt);
			modelIns.put("iSaleEndYmd",iSaleEndYmd);
			modelIns.put("iSaleEndYmdt",iSaleEndYmdt);
			modelIns.put("iSaleMethodType",iSaleMethodType);
			modelIns.put("iSalePeriodType",iSalePeriodType);
			modelIns.put("iSalePrice",iSalePrice);
			modelIns.put("iSaleStartYmd",iSaleStartYmd);
			modelIns.put("iSaleStartYmdt",iSaleStartYmdt);
			modelIns.put("iSaleStatusType",iSaleStatusType);
			modelIns.put("iSearchengineDisplayYn",iSearchengineDisplayYn);
			modelIns.put("iShippingAreaPartnerNo",iShippingAreaPartnerNo);
			modelIns.put("iShippingAreaType",iShippingAreaType);
			modelIns.put("iSyncWmsYn",iSyncWmsYn);
			modelIns.put("iTempSave",iTempSave);
			modelIns.put("iUnitName",iUnitName);
			modelIns.put("iUnitNameType",iUnitNameType);
			modelIns.put("iUnitPrice",iUnitPrice);
			modelIns.put("iUpdateAdminNo",iUpdateAdminNo);
			modelIns.put("iUpdateYmdt",iUpdateYmdt);
			modelIns.put("iUrlDirectDisplayYn",iUrlDirectDisplayYn);
			modelIns.put("iValueAddedTaxType",iValueAddedTaxType);

			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	
	protected Map<String, Object> getSPCompleteParamMapHOWSER(InterfaceBaseVOHOWSER data, List<Map<String, Object>> workList) throws Exception {
		try {
			//String loginID = (String) ((Map<String, Object>) workList.get(0)).get(ConstantWSIF.MAP_KEY_WORK_USER_NO);
			/*
				String iUser = "TEST";
				String iWorkIp = "100.100.100";
			*/
			
			//String iUserNo = loginID;
			
			String[] iOrderSrl = new String[workList.size()];
			String[] iAgencyMgrCode = new String[workList.size()];
			String[] iAgencyCustomerCode = new String[workList.size()];
			String[] iStatus = new String[workList.size()];
			String[] iSvcType = new String[workList.size()];
			String[] iReceiverName = new String[workList.size()];
			String[] iArriveAddress = new String[workList.size()];
			String[] iReceiverPhone = new String[workList.size()];
			String[] iReceiverPhone2nd = new String[workList.size()];
			String[] iMemo = new String[workList.size()];
			String[] iArrivalDate = new String[workList.size()];
			String[] iArrivalTime = new String[workList.size()];
			String[] iOnsiteImages = new String[workList.size()];			
			String[] iGoodsSrl = new String[workList.size()];
			String[] iAmount = new String[workList.size()];
			String[] iOriginCode = new String[workList.size()];
			String[] iResultType = new String[workList.size()];
			String[] iCauseType = new String[workList.size()];
			String[] iCauseDescr = new String[workList.size()];
			String[] iRentalSrl = new String[workList.size()];
			String[] iRentalSrlImg = new String[workList.size()];
			String[] iId = new String[workList.size()];
			String[] iName = new String[workList.size()];
			
			int idx = 0;
			for (Map<String, Object> workInfo : workList) {

				iOrderSrl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_ORDER_SRL));
				iAgencyMgrCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_AGENCY_MGR_CODE));		
				iAgencyCustomerCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_AGENCY_CUSTOMER_CODE));	
				iStatus[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_STATUS)); 
				iSvcType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_SVC_TYPE)); 
				iReceiverName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_RECEIVER_NAME)); 
				iArriveAddress[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_ARRIVE_ADDRESS)); 
				iReceiverPhone[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_RECEIVER_PHONE)); 
				iReceiverPhone2nd[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_RECEIVER_PHONE_2ND)); 
				iOnsiteImages[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_ONSITE_IMAGES)); 
				iArrivalDate[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_ARRIVAL_DATE)); 
				iArrivalTime[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_ARRIVAL_TIME)); 
				iGoodsSrl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_GOODS_SRL));
				iAmount[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_AMOUNT));		
				iOriginCode[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_ORIGIN_CODE));	
				iResultType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_RESULT_TYPE)); 
				iCauseType[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_CAUSE_TYPE)); 
				iCauseDescr[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_CAUSE_DESCR)); 
				iRentalSrl[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_RENTAL_SRL)); 
				iRentalSrlImg[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_GOODS_RENTAL_SRL_IMG)); 
				iId[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_USERS_ID)); 
				iName[idx] = getParameterValue(workInfo.get(ConstantWSIF.MAP_KEY_WORK_HOWSER_USERS_NAME));
				
				idx = idx + 1;			
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("iOrderSrl", iOrderSrl);
			modelIns.put("iAgencyMgrCode", iAgencyMgrCode);
			modelIns.put("iAgencyCustomerCode", iAgencyCustomerCode);
			modelIns.put("iStatus", iStatus);	
			modelIns.put("iSvcType", iSvcType);	
			modelIns.put("iReceiverName", iReceiverName);	
			modelIns.put("iArriveAddress", iArriveAddress);	
			modelIns.put("iReceiverPhone", iReceiverPhone);
			modelIns.put("iReceiverPhone2nd", iReceiverPhone2nd);
			modelIns.put("iMemo", iMemo);
			modelIns.put("iOnsiteImages", iOnsiteImages);
			modelIns.put("iArrivalDate", iArrivalDate);
			modelIns.put("iArrivalTime", iArrivalTime);
			modelIns.put("iGoodsSrl", iGoodsSrl);
			modelIns.put("iAmount", iAmount);
			modelIns.put("iOriginCode", iOriginCode);
			modelIns.put("iResultType", iResultType);
			modelIns.put("iCauseType", iCauseType);
			modelIns.put("iCauseDescr", iCauseDescr);
			modelIns.put("iRentalSrl", iRentalSrl);
			modelIns.put("iRentalSrlImg", iRentalSrlImg);
			modelIns.put("iId", iId);
			modelIns.put("iName", iName);
			return modelIns;
			
		} catch (Exception e) {
			throw new InterfaceException("5007", "Make SP Paramter Error");
		}
	}
	
	protected abstract void checkWorkInfoValidation(Map<String, Object> map) throws Exception;
	
	protected abstract void checkWorkInfoValidationKr(Map<String, Object> map) throws Exception;

	protected abstract void checkParamMapValidation(Map<String, Object> model) throws Exception;
	
	protected final void checkParamMapValidationForList(Map<String, Object> model) throws Exception {
		if (model == null || model.isEmpty()) {
			throw new InterfaceException("5001", "Parameter Error");
		}
		if (model.get(ConstantWSIF.IF_KEY_LOGIN_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_LOGIN_ID))) {
			throw new InterfaceException("5010", "LOGIN ID is NOT VALID");
		}
		if (model.get(ConstantWSIF.IF_KEY_PASSWORD) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_PASSWORD))) {
			throw new InterfaceException("5011", "PASSWORD is NOT VALID");
		}
		//if (model.get(ConstantWSIF.IF_KEY_TERMINAL_ID) == null || StringUtils.isEmpty((String) model.get(ConstantWSIF.IF_KEY_TERMINAL_ID))) {
		//	throw new InterfaceException("5012", "TERMINAL ID is NOT VALID");
		//}
	}
	
	private static String getRowValue(Map<String, Object> outputRow, String key) {
		Object valueObj = outputRow.get(key);
		return convertString(valueObj);
	}
	
	private static String convertString(Object obj) {
		String returnStr = "";
		if ( obj != null) {
			if (obj instanceof BigDecimal) {
				returnStr = ((BigDecimal)obj).toString();
			} else {
				returnStr = obj.toString();
			}			
		} 
		return returnStr;
	}
	
	private static void setJsonMap(Map<String, Object> map, String key, Object value) {
		if (value == null) {
			return;
		} else {
			if (value instanceof BigDecimal) {
				if ( !StringUtils.isEmpty(((BigDecimal)value).toString())) {
					map.put(key, value);
				}
			} else if (value instanceof String) {
				if ( !StringUtils.isEmpty(value.toString())) {
					map.put(key, value);
				}				
			} else if (value instanceof Date) {
				if ( !StringUtils.isEmpty(((Date)value).toString())) {
					map.put(key, InterfaceUtil.getISODateString((Date)value));
				}
			} else {
				map.put(key, value);
			}
		}
	}
	
	private static String getParameterValue(Object value) {
		if (value == null || StringUtils.isEmpty(value.toString())) {
			return "";
		} else {
			if ("null".equalsIgnoreCase(value.toString())) {
				return "";
			} else {
				return value.toString();
			}
		}
	}

}
