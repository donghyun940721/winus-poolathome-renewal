package com.logisall.ws.interfaces.common.service;

import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

public interface AbstractInterfaceService {

    public Map<String, Object> insertRetry(Map<String, Object> model) throws Exception;
    
    public void updateDocState(Map<String, Object> model) throws Exception;

    public void sendJSON(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception;
    
    public void sendResponse(HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception;
    
    public void comSendResponse(String custGb, HttpServletResponse response, Map<String, Object> model, Map<String, Object> paramMap) throws Exception;
    
    public void receiveJSON(Map<String, Object> model, String inputJSON) throws Exception;
}
