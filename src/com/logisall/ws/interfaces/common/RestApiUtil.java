package com.logisall.ws.interfaces.common;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;

public final class RestApiUtil {
	
	public static String getGetJson(String strUrl, Map<String, Object> modelHeaderParam, Map<String, Object> modelBodyParam){
		HttpURLConnection con = null;
		URL url = null; 
		String sUrl = strUrl; // 연결할 주소
		String result = "";
		
		try{
			/* BodyParam url 파라미터 셋팅 */
			StringBuffer sb = new StringBuffer();
			for(Iterator linkitr = modelBodyParam.keySet().iterator(); linkitr.hasNext();){
				String key	 = linkitr.next().toString();
				String value = modelBodyParam.get(key).toString();
				sb.append(key).append("=").append(value).append("&");
			}
			
			//url = new URL("http://alpha-ariel.e-ncp.com/orders?startYmd=2019-01-01&endYmd=2020-05-13&");
			url = new URL(sUrl + "?" + sb.toString());
            con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
			con.setRequestMethod("GET");
			con.setConnectTimeout(10000);
			con.setRequestProperty("Content-Type", "application/json; utf-8");
			con.setRequestProperty("Accept", "application/json");
			/* HeaderParam setRequestProperty 셋팅 */
			for(Iterator linkitr = modelHeaderParam.keySet().iterator(); linkitr.hasNext();){
				String key	 = linkitr.next().toString();
				String value = modelHeaderParam.get(key).toString();
				con.setRequestProperty(key, value);
			}
			
			int resCode = 0;
			resCode = con.getResponseCode();
			//System.out.println("resCode : " + resCode);
			
			if(resCode < 400){
				StringBuilder response = new StringBuilder();
				try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))){
					String responseLine = null;
				    while ((responseLine = br.readLine()) != null) {
				        response.append(responseLine.trim());
				    }
				    //System.out.println(response.toString());
				}
				
				//파싱
				JSONArray jsonObjData = new JSONArray(response.toString());
				result = forceToJSONArray_OrderData_noneKey(jsonObjData);
				//result = response.toString();
			}else{
				result = con.getResponseMessage();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return result;
	}
	
	public static String postGetXmltoJson(String strUrl, Map<String, Object> modelParam){
		HttpURLConnection con = null;
        URL url = null; 
        String sUrl = strUrl; // 연결할 주소
        String result = "";
         
         try {             
        	 url = new URL(sUrl);
             con = (HttpURLConnection) url.openConnection();
             con.setDoInput(true);
             con.setDoOutput(true);
             con.setUseCaches(false);
             con.setRequestMethod("POST");
             con.setConnectTimeout(10000);
             con.setAllowUserInteraction(true);
             con.setRequestProperty("Content-type", "application/x-www-form-urlencoded;charset=utf-8");
             
             StringBuffer sb = new StringBuffer();
             for(Iterator linkitr = modelParam.keySet().iterator(); linkitr.hasNext();){
 				String key    = linkitr.next().toString();
 				String value = modelParam.get(key).toString();
 				
 				sb.append(key).append("=").append(value).append("&");
 			}
             
             PrintWriter pw = new PrintWriter(new OutputStreamWriter(con.getOutputStream(), "utf-8"));
             pw.write(sb.toString());
             pw.flush();
             
             int resCode = 0;
             resCode = con.getResponseCode();
             
             StringBuffer resp = new StringBuffer();
             if(resCode < 400){
                 String line;
                 BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
                 while ((line = br.readLine()) != null) {
                     System.out.println(line);
                     resp.append(line);
                 }
                 
                 pw.close();
                 br.close();
                 
                 //xml파싱
                 result = resp.toString();
             }else{
                 result = con.getResponseMessage();
             }
         }catch(Exception e){
             e.printStackTrace();
         }
         
         return result;
	}
	
	// 기존 xml to json (서버에서 xml을 가져올 때 생성 함)
	public static  String xmlToJsonStrBasic(String xmlString){
        JSONObject xmlJSONObj = XML.toJSONObject(xmlString.toString(), true);
        String jsonPrettyPrintString = xmlJSONObj.toString(4);
        
        return jsonPrettyPrintString;
	}

	//json camelCase <> underScore
	public static  String nameCamelcaseToUnderscore(String xmlString){
		String text = xmlString;
		Matcher m = Pattern.compile("(?<=[a-z])[A-Z]").matcher(text);

		StringBuffer sb = new StringBuffer();
		while (m.find()) {
		    m.appendReplacement(sb, "_"+m.group().toLowerCase());
		}
		m.appendTail(sb);

		//System.out.println(sb.toString());
		return sb.toString();
	}
	
	
	
	/***** godo5 주문정보 시작 *******************************************************************************/
	//godo5 주문 xml to json
	public static  String xmlToJsonStrOrderInfo(String xmlString){
        JSONObject xmlJSONObj = XML.toJSONObject(xmlString.toString(), true);
        String jsonPrettyPrintString = xmlJSONObj.toString(4);
        
        JSONObject jsonDataOrderData = new JSONObject(jsonPrettyPrintString);
        String jsonObjParamOrderData  = forceToJSONArray_OrderData(jsonDataOrderData);
        
        JSONObject jsonDataOrderGoodsData = new JSONObject(jsonObjParamOrderData);
        String jsonObjParamOrderGoodsData = forceToJSONArray_OrderGoodsData(jsonDataOrderGoodsData);
        
        JSONObject jsonDataOrderDeliveryData = new JSONObject(jsonObjParamOrderGoodsData);
        String jsonObjParamOrderDeliveryData = forceToJSONArray_OrderDeliveryData(jsonDataOrderDeliveryData);
        
        JSONObject jsonDataOrderInfoData = new JSONObject(jsonObjParamOrderDeliveryData);
        String jsonObjParamOrderInfoData = forceToJSONArray_OrderInfoData(jsonDataOrderInfoData);
        
        return jsonObjParamOrderInfoData;
	}
	
	//godo5 주문정보 JSONObject <> JSONArray
	public static String forceToJSONArray_OrderData(JSONObject xmlJSONObj){
		JSONObject dataData	= xmlJSONObj.getJSONObject("data");
		JSONObject dataReturn	= dataData.getJSONObject("return");
		
		Object myObj = dataReturn.opt("order_data");
		if( myObj instanceof JSONObject ){
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(myObj);
	
			dataReturn.put("order_data", jsonArray);
			dataData.put("return", dataReturn);
			xmlJSONObj.put("data", dataData);
		}
		
	    return xmlJSONObj.toString(4);
	}
	
	public static String forceToJSONArray_OrderGoodsData(JSONObject xmlJSONObj){
		JSONObject dataData		= xmlJSONObj.getJSONObject("data");
		JSONObject dataReturn		= dataData.getJSONObject("return");
		JSONArray dataOrderData	= dataReturn.getJSONArray("order_data");
		
		for (int i = 0; i < dataOrderData.length(); i++) {
		   JSONObject recOrderData = (JSONObject) dataOrderData.get(i);
		   
		   Object myObj = recOrderData.opt("orderGoodsData");
		   if( myObj instanceof JSONObject ){
		    	JSONArray jsonArray = new JSONArray();
		    	jsonArray.put(myObj);
		    	
		    	recOrderData.put("orderGoodsData", jsonArray);
		    	dataOrderData.put(i, recOrderData);
		    }
		}
		
    	dataReturn.put("order_data", dataOrderData);   	
    	dataData.put("return", dataReturn);
    	xmlJSONObj.put("data", dataData);
		
	    return xmlJSONObj.toString(4);
	}
	
	public static String forceToJSONArray_OrderDeliveryData(JSONObject xmlJSONObj){
		JSONObject dataData		= xmlJSONObj.getJSONObject("data");
		JSONObject dataReturn		= dataData.getJSONObject("return");
		JSONArray dataOrderData	= dataReturn.getJSONArray("order_data");
		
		for (int i = 0; i < dataOrderData.length(); i++) {
		   JSONObject recOrderData = (JSONObject) dataOrderData.get(i);
		   
		   Object myObj = recOrderData.opt("orderDeliveryData");
		   if( myObj instanceof JSONObject ){
		    	JSONArray jsonArray = new JSONArray();
		    	jsonArray.put(myObj);
		    	
		    	recOrderData.put("orderDeliveryData", jsonArray);
		    	dataOrderData.put(i, recOrderData);
		    }
		}
		
    	dataReturn.put("order_data", dataOrderData);   	
    	dataData.put("return", dataReturn);
    	xmlJSONObj.put("data", dataData);
		
	    return xmlJSONObj.toString(4);
	}
	
	public static String forceToJSONArray_OrderInfoData(JSONObject xmlJSONObj){
		JSONObject dataData		= xmlJSONObj.getJSONObject("data");
		JSONObject dataReturn		= dataData.getJSONObject("return");
		JSONArray dataOrderData	= dataReturn.getJSONArray("order_data");
		
		for (int i = 0; i < dataOrderData.length(); i++) {
		   JSONObject recOrderData = (JSONObject) dataOrderData.get(i);
		   
		   Object myObj = recOrderData.opt("orderInfoData");
		   if( myObj instanceof JSONObject ){
		    	JSONArray jsonArray = new JSONArray();
		    	jsonArray.put(myObj);
		    	
		    	recOrderData.put("orderInfoData", jsonArray);
		    	dataOrderData.put(i, recOrderData);
		    }
		}
		
    	dataReturn.put("order_data", dataOrderData);   	
    	dataData.put("return", dataReturn);
    	xmlJSONObj.put("data", dataData);
		
	    return xmlJSONObj.toString(4);
	}
	/***** godo5 주문정보 종료 *******************************************************************************/
	
	
	/***** godo5 상품정보 시작 *******************************************************************************/
	//godo5 상품 xml to json
	public static  String xmlToJsonStrItemInfo(String xmlString){
        JSONObject xmlJSONObj = XML.toJSONObject(xmlString.toString(), true);
        String jsonPrettyPrintString = xmlJSONObj.toString(4);
        
        JSONObject jsonData = new JSONObject(jsonPrettyPrintString);
        String jsonObjParamGoodsData  = forceToJSONArray_GoodsData(jsonData);
        
        JSONObject jsonDataConvLvl1 = new JSONObject(jsonObjParamGoodsData);
        String jsonObjParamOptionData = forceToJSONArray_OptionData(jsonDataConvLvl1);
        
        JSONObject jsonDataConvLvl2 = new JSONObject(jsonObjParamOptionData);
        String jsonObjParamGoodsMustInfoData = forceToJSONArray_StepData(jsonDataConvLvl2);
	    
        return jsonObjParamGoodsMustInfoData;
	}
	
	
	//godo5 상품정보 JSONObject <> JSONArray
	public static String forceToJSONArray_GoodsData(JSONObject xmlJSONObj){
		JSONObject dataData   = xmlJSONObj.getJSONObject("data");
		JSONObject dataReturn = dataData.getJSONObject("return");
		
		Object myObj = dataReturn.opt("goods_data");
		if( myObj instanceof JSONObject ){
			JSONArray jsonArray = new JSONArray();
			jsonArray.put(myObj);
	
			dataReturn.put("goods_data", jsonArray);
			dataData.put("return", dataReturn);
			xmlJSONObj.put("data", dataData);
		}
		
	    return xmlJSONObj.toString(4);
	}
	
	public static String forceToJSONArray_OptionData(JSONObject xmlJSONObj){
		JSONObject dataData     = xmlJSONObj.getJSONObject("data");
		JSONObject dataReturn   = dataData.getJSONObject("return");
		JSONArray dataGoodsData = dataReturn.getJSONArray("goods_data");
		
		for (int i = 0; i < dataGoodsData.length(); i++) {
		   JSONObject recOptionData = (JSONObject) dataGoodsData.get(i);
		   
		   Object myObj = recOptionData.opt("optionData");
		   if( myObj instanceof JSONObject ){
		    	JSONArray jsonArray = new JSONArray();
		    	jsonArray.put(myObj);
		    	
		    	recOptionData.put("optionData", jsonArray);
		    	dataGoodsData.put(i, recOptionData);
		    }
		}
		
    	dataReturn.put("goods_data", dataGoodsData);   	
    	dataData.put("return", dataReturn);
    	xmlJSONObj.put("data", dataData);
		
	    return xmlJSONObj.toString(4);
	}
	
	public static String forceToJSONArray_StepData(JSONObject xmlJSONObj){
		JSONObject dataData     = xmlJSONObj.getJSONObject("data");
		JSONObject dataReturn   = dataData.getJSONObject("return");
		JSONArray dataGoodsData = dataReturn.getJSONArray("goods_data");
		
		for (int i = 0; i < dataGoodsData.length(); i++) {
		   JSONObject recGoodsMustInfoData = (JSONObject) dataGoodsData.get(i);
		   
		   /** goodsMustInfoData Arr처리 **/
		   try{
			   JSONArray dataGoodsMustInfoData = recGoodsMustInfoData.getJSONArray("goodsMustInfoData");
			   
			   for (int j = 0; j < dataGoodsMustInfoData.length(); j++) {
				   JSONObject recStepData = (JSONObject) dataGoodsMustInfoData.get(j);
				   
				   Object myObj = recStepData.opt("stepData");
				   if( myObj instanceof JSONObject ){
				    	JSONArray jsonArray = new JSONArray();
				    	jsonArray.put(myObj);
				    	
				    	recStepData.put("stepData", jsonArray);
				    	dataGoodsMustInfoData.put(j, recStepData);
				    	recGoodsMustInfoData.put("goodsMustInfoData", dataGoodsMustInfoData);
				    }
			   }
			   
			   dataGoodsData.put(i, recGoodsMustInfoData);
		   }catch (Exception e){
			   JSONObject recGoodMustInfoData = (JSONObject) dataGoodsData.get(i);
			   
			   Object myObj = recGoodMustInfoData.opt("goodsMustInfoData");
			   System.out.println(">myObj  >> ");
			   if( myObj instanceof JSONObject ){
				   System.out.println(">>>>");
			    	JSONArray jsonArray = new JSONArray();
			    	jsonArray.put(myObj);
			    	
			    	recGoodMustInfoData.put("goodsMustInfoData", jsonArray);
			    }

			   dataGoodsData.put(i, recGoodsMustInfoData);
		   }
		   

		   /** textOptionData Arr처리 **/
		   try{
			   JSONArray dataTextOptionData = recGoodsMustInfoData.getJSONArray("textOptionData");
			   
		   }catch (Exception e){
			   JSONObject recTextOptionData = (JSONObject) dataGoodsData.get(i);
			   
			   Object myObj = recTextOptionData.opt("textOptionData");
			   System.out.println(">myObj  >> ");
			   if( myObj instanceof JSONObject ){
				   System.out.println(">>>>");
			    	JSONArray jsonArray = new JSONArray();
			    	jsonArray.put(myObj);
			    	
			    	recTextOptionData.put("textOptionData", jsonArray);
			    }

			   dataGoodsData.put(i, recGoodsMustInfoData);
		   }
		}
		
    	dataReturn.put("goods_data", dataGoodsData);   	
    	dataData.put("return", dataReturn);
    	xmlJSONObj.put("data", dataData);
		
	    return xmlJSONObj.toString(4);
	}
	/***** godo5 상품정보 종료 *******************************************************************************/
	
	/***** cj return필드 시작 *******************************************************************************/
	//cj return필드 xml to json
	public static  String xmlToJsonStrReturn(String xmlString){
        JSONObject xmlJSONObj = XML.toJSONObject(xmlString.toString(), true);
        String jsonPrettyPrintString = xmlJSONObj.toString(4);
        
        JSONObject jsonData = new JSONObject(jsonPrettyPrintString);
        String jsonObjParamGoodsData  = forceToJSONArray_Return(jsonData);
	    
        return jsonObjParamGoodsData;
	}
	
	//cj return필드 JSONObject <> JSONArray
	public static String forceToJSONArray_Return(JSONObject xmlJSONObj){
		JSONObject dataEnvelope			= xmlJSONObj.getJSONObject("S:Envelope");
		JSONObject dataBody				= dataEnvelope.getJSONObject("S:Body");
		JSONObject dataValueResponse	= dataBody.getJSONObject("ns2:getAddressInformationByValueResponse");
		
		/** textOptionData Arr처리 **/
		try{
			JSONArray dataReturn = dataValueResponse.getJSONArray("return");
		}catch (Exception e){
			Object myObj = dataValueResponse.opt("return");
			if( myObj instanceof JSONObject ){
				JSONArray jsonArray = new JSONArray();
				jsonArray.put(myObj);
				
				dataValueResponse.put("return", jsonArray);
			}
		}
		
		return xmlJSONObj.toString(4);
	}
	/***** cj return필드 종료 *******************************************************************************/
	
	//SHOPBY 주문정보 JSONObject <> JSONArray
	public static String forceToJSONArray_OrderData_noneKey(JSONArray JSONArrData){
		
		
		JSONObject jsonObj = new JSONObject();
		

		jsonObj.put("order_data", JSONArrData);
		
		
	    return jsonObj.toString(4);
	}
	
	public static Map<String, Object> getRealPacking(String strUrl, Map<String, Object> modelBodyParam){
		HttpURLConnection con = null;
		URL url = null; 
		String sUrl = strUrl; // 연결할 주소
		Map<String, Object> result = new HashMap<String, Object>();
		
		try{
			/* BodyParam url 파라미터 셋팅 */
			JSONObject param  = new JSONObject();
			
			StringBuffer sb = new StringBuffer();
			for(Iterator linkitr = modelBodyParam.keySet().iterator(); linkitr.hasNext();){
				//String key	 = URLEncoder.encode(linkitr.next().toString(), "UTF-8");
				//String value = URLEncoder.encode(modelBodyParam.get(key).toString(), "UTF-8");
				String key	 = linkitr.next().toString();
				String value = modelBodyParam.get(key).toString();
				param.put(key, value);
				sb.append(key).append("=").append(value);
				if(linkitr.hasNext()){
					sb.append("&");
				}
			}
			
			//url = new URL("http://alpha-ariel.e-ncp.com/orders?startYmd=2019-01-01&endYmd=2020-05-13&");
			url = new URL(sUrl);
            con = (HttpURLConnection) url.openConnection();
            con.setDoInput(true);
            con.setDoOutput(true);
            con.setUseCaches(false);
			con.setRequestMethod("POST");
			con.setConnectTimeout(10000);
			con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			
			
			OutputStreamWriter os = new OutputStreamWriter(con.getOutputStream(), "UTF-8");
			//DataOutputStream os = new DataOutputStream(con.getOutputStream());
			
			try{
				os.write(param.toString());
				//os.write(param.toString().getBytes("UTF-8"));;
				
			}catch(Exception e){
				
			}finally{
				os.flush();
				os.close();
			}
			
			int resCode = 0;
			resCode = con.getResponseCode();
			//System.out.println("resCode : " + resCode);
			
			if(resCode < 400){
				StringBuilder response = new StringBuilder();
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));;
				try{
				
					String responseLine = null;
				    while ((responseLine = br.readLine()) != null) {
				        response.append(responseLine.trim());
				    }
				    //System.out.println(response.toString());
				}catch(Exception e){
					
				}finally{
					br.close();
				}
				
				System.out.println(response.toString());
				//파싱
				
				JSONObject json = new JSONObject(String.valueOf(response)); // 받아온 string을 json 으로로 변환
			       
		        Iterator i = json.keys(); // json key 요소읽어옴
		         
		        while(i.hasNext()){
		           
		             String k = i.next().toString(); // key 순차적으로 추출
		            
		             result.put(k, json.get(k)); // key, value를 map에 삽입
		        }
		 

				//result = response.toString();
			}else{
				//result = con.getResponseMessage();
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			con.disconnect();
		}
		
		return result;
	}
}