package com.logisall.ws.interfaces.common;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVO;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKR;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOForklift;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlvNew;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOHOWSER;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOGodomall;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOKRDate;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOGodomallItem;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOShopbuy;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVOShopbuyItem;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv200614;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv211231;
import com.logisall.ws.interfaces.common.vo.InterfaceBaseVODlv220501;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

/**
 @ isCharLen : 한글 영문 복합 문자길이 구하기
 @ nullChk : Null인지를 체크하여 Null인 경우에는 디폴트 값을 리턴한다.
 @ getShortString : 한글 존재 여부와 상관없이 일정한 Byte 단위로 String을 잘라주는 메소드  입력된 Byte Size 보다 큰 경우에는 &quot;...&quot;을 추가 한다.
 @ replace : 문자열(s) 중에서 특정 문자열(old)을 찾아서 원하는 문자열(replacement)을 변환한다.
 @ getN2Br : '\n'를  HTML CODE '<br>' 로 변환
 @ ignoreSeparator : 문자열중 특정 문자(sep)를 제거
 @ fillCharFront : int나 Strig 형으로 넘어온 iZero를 String으로 변환하여 그 앞에 sFill을 넣어  길이가 iLen이 되도록 하는 메소드
 @ isNull : null 여부 체크  null 이면 true, null이 아니면  false
 @ setSafeStr : 오라클에 입력할  "'" 으로 감싸진 string으로 만드는 함수
 @ setCheckSingleQuote : Single Quote (') => Double Quote ('') 로 변환
 @ getSafeNumber : 값이 없을경우 "0" 리턴
 @ isNumber : number 여부 체크  숫자이면 true, 아니면 false
 @ setEncryptorString : String을 암호화된 문자열로 Encrypt
 @ getDecryptorString : 암호화된 String을 원래꺼로 decrypt
 @ setOracleDate : string을 정해진 오라클 date 타입(yyyy-mm-dd)으로 변환, null 일경우 'sysdate' 리턴
 @ setOracleDateOnly : string을 정해진 오라클 date 타입(yyyy-mm-dd)으로 변환, null일경우 ''리턴
 @ setOracleDateTime : string을 정해진 오라클 date 타입(yyyy-mm-dd hh24)으로 변환, null일 경우 'sysdate' 리턴
 @ setOracleDatePattern : string을 사용자가 입력한 srt_Pattern의 오라클 date 타입으로 변환, null일 경우 'sysdate' 리턴
 @ setDatePattern : string을 사용자가 입력한 srt_Pattern의 오라클 date 타입으로 변환, null일 경우 'null' 리턴
 @ getStr2int : String 형을 int형으로 변환
 @ getStr2dou : String형을 float형으로 변환
 @ getShotString : 긴문장을 지정 길이로 잘라서 String[] 형태로 리턴
 @ getMoneyType : int나 String형 숫자를 금액표시타입으로 변환한다.
 @ setResCheckScript : "<",">" 를 "&lt;","&gt;" 로 변경
 @ getStrLenBr : asStr을 strSize만큼 자르고 asStr이 strSize보다 더 클경우 strSize만큼 자른후 "<br>" 을 붙임
 @ getSubstr : 문자열 자르기     value1 에서 value2 자리수 까지 자르기
 @ getFormatComa : 문자열을  100,000,000 포멧으로 변환, 문자열이 null 이면 "0" 리턴
 @ getFormatComaDot : 문자열을  100,000,000.00 포멧으로 변환, 문자열이 null 이면 "0"리턴
 @ getFormatComaDot2 : 문자열을 100,000,000.0000 포멧으로 변환, 문자열이 null 이면 "0"리턴
 @ getFormatNone : 문자열을  100,000,000.00 포멧으로 변환, 문자열이 null 이면 "0"리턴
 @ setNoCommaString : 문자열중 콤마제거
 @ getNoDashString : 문자열중 하이픈(-) 제거
 @ getLenchk : int 나 String 형  한자리 숫자값에 0추가 , 두자리이면 그냥 리턴
 @ getSepOneDash : temp 문자열에 지정된 idx 위치에 "-" 삽입
 @ getSepTwoDash : temp 문자열에 지정된 idx1, idx2 위치에 "-" 삽입
 @ getRightString : 문자열 오른쪽부터 result_length길이의 문자열을 리턴한다.
 @ getArrayStrValue : array 의 지정된 index 의 value 값을  String형 으로 return
 @ getRoundIndigo : 지정된 소수점자릿만허용(그이하에선  반올림)
 @ prtLog : log4j대신 로그 출력
 @ getParameterCount : 파라메터 갯수를 반환
 @ getParameterName : 파라메터 이름 리스트를 반환
 @ split : 입력받은 문자를 구분자로 나누어 ArrayList형태로 반환
 @ mod : 나머지를 반환
 @ getErasedTag : 문자열에서 HTML TAG를 지운다.
 */

/**
 * @author minjae
 * 
 */
@SuppressWarnings("PMD")
public final class InterfaceUtil {

	protected static final Log logger = LogFactory.getLog(InterfaceUtil.class);

	private static final String RESULT_CODE_FOR_NO_ERROR = "0";
	private static final String RESULT_MSG_FOR_SP_ERROR = "ORA-";

	private InterfaceUtil() {
	}


	public static String camelToDbStyle(String str) {
		String regex = "([a-z])([A-Z])";
		String replacement = "$1_$2";

		String value = "";
		try {
			value = str.replaceAll(regex, replacement).toUpperCase();
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert camel String to DBStyle :", e);
			}
		}
		return value;

	}

	public static void saveJSONStringToFile(Map<String, Object> model, String jsonString) throws Exception {

		String filePrefix = (String) model.get("FILE_PREFIX");
		String pathDirectory = DateUtil.getDateByPattern2();
		String filePath = (new StringBuffer().append(ConstantWSIF.TEMP_PATH).append(pathDirectory)).toString();

		String fileName = (new StringBuffer().append(filePrefix).append(UUID.randomUUID().toString()).append(".json")).toString();

		saveJSONStringToFile(filePath, fileName, jsonString, ConstantWSIF.ENCODING_TYPE_JSON_FILE);

		model.put("D_XML_FILE_PATH", pathDirectory);
		model.put("D_DOC_NAME", fileName);
	}

	public static String saveJSONStringToFile(String filePrefix, String jsonString) throws Exception {
		String pathDirectory = DateUtil.getDateByPattern2();
		String filePath = (new StringBuffer().append(ConstantWSIF.TEMP_PATH).append(pathDirectory)).toString();

		String fileName = (new StringBuffer().append(filePrefix).append(UUID.randomUUID().toString()).append(".json")).toString();

		saveJSONStringToFile(filePath, fileName, jsonString, ConstantWSIF.ENCODING_TYPE_JSON_FILE);

		return fileName;
	}

	public static void saveJSONStringToFile(String filePath, String fileName, String jsonString, String encodingType) throws Exception {

		FileOutputStream fileOutputStream = null;
		OutputStreamWriter outputStreamWriter = null;
		BufferedWriter bufferedWriter = null;

		try {
			if (!CommonUtil.isNull(filePath)) {
				if (!FileUtil.existDirectory(filePath)) {
					FileHelper.createDirectorys(filePath);
				}
			}
			File jsonFile = new File(filePath, fileName);
			fileOutputStream = new FileOutputStream(jsonFile);
			outputStreamWriter = new OutputStreamWriter(fileOutputStream, encodingType);
			bufferedWriter = new BufferedWriter(outputStreamWriter);

			bufferedWriter.write(jsonString);
			bufferedWriter.flush();

			bufferedWriter.close();
			fileOutputStream.close();
			fileOutputStream.close();

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to Save JSON File :", e);
			}
			throw e;

		} finally {
			if (bufferedWriter != null) {
				try {
					bufferedWriter.close();
				} catch (Exception e) {
				}
			}

			if (outputStreamWriter != null) {
				try {
					outputStreamWriter.close();
				} catch (Exception e) {
				}
			}

			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (Exception e) {
				}
			}

		}
	}

	/*-
	 * Method ID : saveBytesToFile
	 * Method 설명 : Byte array 를 파일로 저장하는 함수
	 * 작성자 : kdalma
	 *
	 * @param model : model 객체
	 * @param filename : 파일 명
	 * @param datas : 첨부된 파일의 byte array
	 * @throws Exception
	 */
	public static void saveBytesToFile(String filePath, String fileName, byte[] datas) throws Exception {

		FileOutputStream fileOutputStream = null;
				
		try {
			if (!CommonUtil.isNull(filePath)) {
				if (!FileUtil.existDirectory(filePath)) {
					FileHelper.createDirectorys(filePath);
				}
			}
			File file = new File(filePath, fileName);
			fileOutputStream = new FileOutputStream(file);
			fileOutputStream.write(datas);
			fileOutputStream.flush();
			fileOutputStream.close();

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to Save Binary File :", e);
			}
			throw e;

		} finally {

			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (Exception e) {
				}
			}

		}
	}

	public static InterfaceBaseVO convertInterfaceVOFromJSON(String jsonString) throws Exception {
		return convertInterfaceVOFromJSON(jsonString, null, null);
	}

	public static InterfaceBaseVO convertInterfaceVOFromJSON(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVO data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVO>() {
			}.getType());

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVO :", e);
			}
		}
		return null;
	}

	public static InterfaceBaseVOKR convertInterfaceVOFromJSONKR(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONKR(jsonString, null, null);
	}

	public static InterfaceBaseVOKR convertInterfaceVOFromJSONKR(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVOKR data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVOKR>() {
			}.getType());
			
			logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVOKRDate convertInterfaceVOFromJSONKRDate(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONKRDate(jsonString, null, null);
	}

	public static InterfaceBaseVOKRDate convertInterfaceVOFromJSONKRDate(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVOKRDate data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVOKRDate>() {
			}.getType());
			
			logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVOForklift convertInterfaceVOFromJSONForklift(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONForklift(jsonString, null, null);
	}

	public static InterfaceBaseVOForklift convertInterfaceVOFromJSONForklift(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVOForklift data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVOForklift>() {
			}.getType());
			
			logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVODlv convertInterfaceVOFromJSONDlv(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONDlv(jsonString, null, null);
	}

	public static InterfaceBaseVODlv convertInterfaceVOFromJSONDlv(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVODlv data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVODlv>() {
			}.getType());
			
			logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVODlvNew convertInterfaceVOFromJSONDlvNew(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONDlvNew(jsonString, null, null);
	}

	public static InterfaceBaseVODlvNew convertInterfaceVOFromJSONDlvNew(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVODlvNew data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVODlvNew>() {
			}.getType());
			
			logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVOHOWSER convertInterfaceVOFromJSONHOWSER(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONHOWSER(jsonString, null, null);
	}

	public static InterfaceBaseVOHOWSER convertInterfaceVOFromJSONHOWSER(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

//		if (fileNamingPolicy == null) {
//			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
//		}
		try {
			
			//logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			//Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).create();

			InterfaceBaseVOHOWSER data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVOHOWSER>() {
			}.getType());
			
			//logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVOGodomall convertInterfaceVOFromJSONGodomall(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONGodomall(jsonString, null, null);
	}

	public static InterfaceBaseVOGodomall convertInterfaceVOFromJSONGodomall(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			//logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();
		
//			List<Object> templist = new ArrayList<Object>();
			Map<String, Object> temp_map = new HashMap<String, Object>();
//			Map<String, Object> temp_map2 = new HashMap<String, Object>();
			temp_map = gson.fromJson(jsonString,  temp_map.getClass());
			temp_map = (Map)temp_map.get("data");  
			temp_map = (Map)temp_map.get("return");
			String temp_map_string = gson.toJson(temp_map);
			//logger.info("[ temp_map ***************************temp_map_string*] :" + temp_map_string);
			InterfaceBaseVOGodomall data = gson.fromJson(temp_map_string, new TypeToken<InterfaceBaseVOGodomall>() {
			}.getType());
			
			//logger.info("[====== Interface Util data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVOGodomallItem convertInterfaceVOFromJSONGodomallItem(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONGodomallItem(jsonString, null, null);
	}

	public static InterfaceBaseVOGodomallItem convertInterfaceVOFromJSONGodomallItem(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			//logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();
		
//			List<Object> templist = new ArrayList<Object>();
			Map<String, Object> temp_map = new HashMap<String, Object>();
//			Map<String, Object> temp_map2 = new HashMap<String, Object>();
			temp_map = gson.fromJson(jsonString,  temp_map.getClass());
			temp_map = (Map)temp_map.get("data");
			temp_map = (Map)temp_map.get("return");
//			templist.add(temp_map.get("order_data"));
//			temp_map2.put("order_data", templist);
			//logger.info("[ temp_map ***************************temp_map*] :" + temp_map);
			String temp_map_string = gson.toJson(temp_map);
			//logger.info("[ temp_map ***************************temp_map_string*] :" + temp_map_string);
			InterfaceBaseVOGodomallItem data = gson.fromJson(temp_map_string, new TypeToken<InterfaceBaseVOGodomallItem>() {
			}.getType());
			
			//logger.info("[ ============ Interface Util data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceBaseVOGodomallItem :", e);
			}
		}
		return null;
	}

	
	public static InterfaceBaseVOShopbuy convertInterfaceVOFromJSONShopbuy(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONShopbuy(jsonString, null, null);
	}

	public static InterfaceBaseVOShopbuy convertInterfaceVOFromJSONShopbuy(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();
		
			InterfaceBaseVOShopbuy data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVOShopbuy>() {
			}.getType());
			
			logger.info("[====== Interface Util data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVOShopbuyItem convertInterfaceVOFromJSONShopbuyItem(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONShopbuyItem(jsonString, null, null);
	}

	public static InterfaceBaseVOShopbuyItem convertInterfaceVOFromJSONShopbuyItem(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			//logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();
		
//			List<Object> templist = new ArrayList<Object>();
			Map<String, Object> temp_map = new HashMap<String, Object>();
//			Map<String, Object> temp_map2 = new HashMap<String, Object>();
			temp_map = gson.fromJson(jsonString,  temp_map.getClass());
			temp_map = (Map)temp_map.get("data");  
			temp_map = (Map)temp_map.get("return");
			String temp_map_string = gson.toJson(temp_map);
			//logger.info("[ temp_map ***************************temp_map_string*] :" + temp_map_string);
			InterfaceBaseVOShopbuyItem data = gson.fromJson(temp_map_string, new TypeToken<InterfaceBaseVOShopbuyItem>() {
			}.getType());
			
			//logger.info("[====== Interface Util data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static String getISODateString(Date date) {
		return getDateString(date, ConstantWSIF.WS_JSON_DATEFORMAT);
	}

	public static String getDateString(Date date) {
		return getDateString(date, null);
	}

	public static String getDateString(Date date, String dateFormat) {
		String isoDateString = "null";
		if (date != null) {
			if (dateFormat == null) {
				dateFormat = ConstantWSIF.WS_STRING_DATEFORMAT;
			}
			isoDateString = new SimpleDateFormat(dateFormat).format(date);
		}
		return isoDateString;
	}

	// 정산관련 SP결과코드 판정
	public static void isValidReturnCode(String workName, String retCode, String retMsg) throws InterfaceException {
		boolean retFlag = true;
		int retCd = -1;
		String errCode = "";

		// 결과코드가 NOT Null 이고, 설정값이 있을때만 확인
		if (retCode != null && retCode.trim().length() > 0) {

			// 결과코드가 '정상'일때
			if (RESULT_CODE_FOR_NO_ERROR.equals(retCode)) {
				retFlag = true;

				// 그외 결과코드가 음수가 아닐때
			} else {
				try {
					retCd = Integer.parseInt(retCode);
					errCode = String.valueOf(Math.abs(retCd));
				} catch (Exception e) {
					StringBuffer sb = new StringBuffer().append("[").append(workName).append("] ").append(retCode).append("/").append(retMsg);
					if (logger.isErrorEnabled()) {
						logger.error("Fail to SP return Code :" + sb.toString(), e);
					}
					throw new InterfaceException(e);
				}

				// 그외 결과코드가 음수가 아닐때 유효한 결과로 판정
				if (retCd > -1) {
					retFlag = true;
				} else {
					retFlag = false;
				}
			}

		}

		if (!retFlag) {
			throw new InterfaceException(errCode, retMsg);
		}
	}
	
	public static InterfaceBaseVODlv200614 convertInterfaceVOFromJSONDlv200614(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONDlv200614(jsonString, null, null);
	}

	public static InterfaceBaseVODlv200614 convertInterfaceVOFromJSONDlv200614(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVODlv200614 data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVODlv200614>() {
			}.getType());
			
			logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVODlv211231 convertInterfaceVOFromJSONDlv211231(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONDlv211231(jsonString, null, null);
	}

	public static InterfaceBaseVODlv211231 convertInterfaceVOFromJSONDlv211231(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVODlv211231 data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVODlv211231>() {
			}.getType());
			
			logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}
	
	public static InterfaceBaseVODlv220501 convertInterfaceVOFromJSONDlv220501(String jsonString) throws Exception {
		return convertInterfaceVOFromJSONDlv220501(jsonString, null, null);
	}

	public static InterfaceBaseVODlv220501 convertInterfaceVOFromJSONDlv220501(String jsonString, String dateFormat, FieldNamingPolicy fileNamingPolicy) throws Exception {
		if (dateFormat == null) {
			dateFormat = ConstantWSIF.WS_JSON_DATEFORMAT;
		}

		if (fileNamingPolicy == null) {
			fileNamingPolicy = FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES;
		}
		try {
			
			logger.info("[ jsonString ***************************jsonString*] :" + jsonString);
			Gson gson = new GsonBuilder().setDateFormat(dateFormat).setFieldNamingPolicy(fileNamingPolicy).create();

			InterfaceBaseVODlv220501 data = gson.fromJson(jsonString, new TypeToken<InterfaceBaseVODlv220501>() {
			}.getType());
			
			logger.info("[ data ****************************] :" + data);

			return data;

		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to convert JSON TO InterfaceVOKR :", e);
			}
		}
		return null;
	}

}