package com.logisall.winus.wmsom.service;

import java.util.Map;

public interface WMSOM010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listTelDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> listCar(Map<String, Object> model) throws Exception;
	public Map<String, Object> confirmOrder(Map<String, Object> model) throws Exception;
	
}
