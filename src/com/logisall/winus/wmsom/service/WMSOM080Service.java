package com.logisall.winus.wmsom.service;

import java.util.Map;

public interface WMSOM080Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSimpleOutOrderTemp(Map<String, Object> model) throws Exception;
}
