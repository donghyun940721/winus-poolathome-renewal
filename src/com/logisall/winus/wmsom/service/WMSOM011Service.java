package com.logisall.winus.wmsom.service;

import java.util.Map;

public interface WMSOM011Service {
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
}
