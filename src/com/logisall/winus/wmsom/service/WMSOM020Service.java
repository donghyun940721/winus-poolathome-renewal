package com.logisall.winus.wmsom.service;

import java.util.Map;

public interface WMSOM020Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> confirmOrder(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception;
	public Map<String, Object> listNew(Map<String, Object> model) throws Exception;
	public Map<String, Object> deleteInOrder(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveCustLotNo(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveCustLotNoUpdate(Map<String, Object> model) throws Exception;
	
}
