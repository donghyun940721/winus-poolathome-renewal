package com.logisall.winus.wmsom.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOM011Dao")
public class WMSOM011Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : listDetail
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsom011.listDetail", model);
    }
    
}


