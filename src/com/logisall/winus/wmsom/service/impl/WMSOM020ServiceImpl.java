package com.logisall.winus.wmsom.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsom.service.WMSOM020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOM020Service")
public class WMSOM020ServiceImpl implements WMSOM020Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM020Dao")
    private WMSOM020Dao dao;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listDetail
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listDetail(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: confirmOrder
     * 대체 Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> confirmOrder(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	        
    	map.put("I_INSERT_DATE",      model.get("vrCalDlvDt"));
    	map.put("I_WORK_IP",      model.get("SS_CLIENT_IP"));
    	map.put("I_USER_NO",      model.get("SS_USER_NO"));
        
    	@SuppressWarnings("unchecked")
		Map<String, Object> returnMap = (Map<String, Object>) dao.confirmOrder(model);
        /*int errCode = Integer.parseInt(returnMap.get("O_MSG_CODE").toString());
        String errMsg = returnMap.get("O_MSG_NAME").toString();
        
    	map.put("errCode", errCode);
    	map.put("errMsg", errMsg);*/
    
        
    	return returnMap;
    }
    
    /**
     * 분할전
     * 대체 Method ID   : saveInOrder
     * 대체 Method 설명    : 입고확정
     * 작성자                      : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
           
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
            	
	            for(int i = 0 ; i < insCnt ; i ++){
	                //저장, 수정
	                String[] dsSubRowStatus = new String[insCnt];                
	                String[] ordSeq         = new String[insCnt];         
	                String[] ritemId        = new String[insCnt];     
	                String[] custLotNo      = new String[insCnt];     
	                String[] realInQty      = new String[insCnt];     
	                
	                String[] realOutQty     = new String[insCnt];                     
	                String[] makeDt         = new String[insCnt];         
	                String[] timePeriodDay  = new String[insCnt];     
	                String[] locYn          = new String[insCnt];     
	                String[] pdaCd          = new String[insCnt];     
	                
	                String[] workYn         = new String[insCnt];                
	                String[] rjType         = new String[insCnt];         
	                String[] realPltQty     = new String[insCnt];     
	                String[] realBoxQty     = new String[insCnt];     
	                String[] confYn         = new String[insCnt];     
	                
	                String[] unitAmt        = new String[insCnt];                
	                String[] amt            = new String[insCnt];         
	                String[] eaCapa         = new String[insCnt];     
	                String[] boxBarcode     = new String[insCnt];     
	                String[] inOrdUomId     = new String[insCnt];     
	                
	                String[] inWorkUomId 	= new String[insCnt]; 
	                String[] outOrdUomId    = new String[insCnt];       
	                String[] outWorkUomId	= new String[insCnt]; 
	                String[] inOrdQty       = new String[insCnt]; 
	                String[] inWorkOrdQty   = new String[insCnt]; 
	                
	                String[] outOrdQty      = new String[insCnt];     
	                String[] outWorkOrdQty  = new String[insCnt]; 
	                String[] refSubLotId    = new String[insCnt];     
	                String[] dspId          = new String[insCnt];     
	                String[] carId          = new String[insCnt];                
	                
	                String[] cntrId         = new String[insCnt];         
	                String[] cntrNo         = new String[insCnt];     
	                String[] cntrType       = new String[insCnt];     
	                String[] badQty         = new String[insCnt];     
	                String[] uomNm          = new String[insCnt];                
	                
	                String[] unitPrice      = new String[insCnt];         
	                String[] whNm           = new String[insCnt];     
	                String[] itemKorNm      = new String[insCnt];     
	                String[] itemEngNm      = new String[insCnt];     
	                String[] repUomId       = new String[insCnt];                
	                
	                String[] uomCd          = new String[insCnt];         
	                String[] uomId          = new String[insCnt];     
	                String[] repUomCd       = new String[insCnt];     
	                String[] repuomNm       = new String[insCnt];     
	                String[] itemGrpId      = new String[insCnt];                
	                
	                String[] expiryDate     = new String[insCnt];         
	                String[] inOrdWeight    = new String[insCnt];                 
	                String[] unitNo         = new String[insCnt];
	                String[] ordDesc        = new String[insCnt];
	                String[] validDt        = new String[insCnt];
	                
	                String[] etc2           = new String[insCnt];
	                String[] locCd           = new String[insCnt];
	                
	                //추가
	                String[] itemBestDate     = new String[insCnt];   //상품유효기간     
	                String[] itemBestDateEnd  = new String[insCnt];   //상품유효기간만료일
	                String[] rtiNm            = new String[insCnt];   //물류기기명
	                
	                
	                dsSubRowStatus[0]  = (String)model.get("I_ST_GUBUN"+i);               
	                ordSeq[0]           = null;        
	                ritemId[0]          = (String)model.get("I_RITEM_ID"+i);      
	                custLotNo[0]        = null;      
	                realInQty[0]        = null;      
	                
	                realOutQty[0]       = null;                      
	                makeDt[0]           = null;          
	                timePeriodDay[0]    = null;      
	                locYn[0]            = null;      
	                pdaCd[0]            = null;      
	                
	                workYn[0]           = null;                 
	                rjType[0]           = null;          
	                realPltQty[0]       = null;      
	                realBoxQty[0]       = null;      
	                confYn[0]           = null;      
	                
	                unitAmt[0]          = null;                 
	                amt[0]              = null;          
	                eaCapa[0]           = null;      
	                boxBarcode[0]       = null;      
	                inOrdUomId[0]       = (String)model.get("I_IN_ORD_UOM_ID"+i);      
	                inWorkUomId[0]       = (String)model.get("I_IN_WORK_UOM_ID"+i);
	                outOrdUomId[0]      = null;                 
	                outWorkUomId[0]      = null;
	                inOrdQty[0]         = (String)model.get("I_IN_ORD_QTY"+i);          
	                inWorkOrdQty[0]         = (String)model.get("I_IN_WORK_ORD_QTY"+i);
	                outOrdQty[0]        = null;     
	                outWorkOrdQty[0]        = null; 
	                refSubLotId[0]      = null;      
	                dspId[0]            = null;      
	                
	                carId[0]            = null;                 
	                cntrId[0]           = null;          
	                cntrNo[0]           = null;      
	                cntrType[0]         = null;      
	                badQty[0]           = null;      
	                
	                uomNm[0]            = null;                 
	                unitPrice[0]        = null;          
	                whNm[0]             = null;      
	                itemKorNm[0]        = null;      
	                itemEngNm[0]        = null;      
	                
	                repUomId[0]         = null;                 
	                uomCd[0]            = null;          
	                uomId[0]            = null;      
	                repUomCd[0]         = null;      
	                repuomNm[0]         = null;      
	                
	                itemGrpId[0]        = null;                 
	                expiryDate[0]       = null;          
	                inOrdWeight[0]      = null;      
	                unitNo[0]           = null;      
	                ordDesc[0]          = null;
	                
	                validDt[0]          = (String)model.get("I_IN_VALID_DT"+i);      
	                
	                etc2[0]             = null; 
	                locCd[0]            = null;

	                itemBestDateEnd[0]  = (String)model.get("I_IN_VALID_DT"+i);   //유통기한   

	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                
	                //main
	                modelIns.put("dsMain_rowStatus" , null);
	                modelIns.put("vrOrdId"          , null);
	                modelIns.put("inReqDt"          , ((String)model.get("I_IN_DLV_DT"+i)).replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
	                modelIns.put("inDt"             , null);
	                modelIns.put("custPoid"         , null);
	                
	                modelIns.put("custPoseq"        , null);
	                modelIns.put("orgOrdId"         , (String)model.get("I_IN_ORG_ORD_ID"+i));   
	                modelIns.put("orgOrdSeq"        , (String)model.get("I_IN_ORD_DETAIL_NO"+i));
	                modelIns.put("vrWhId"           , null);
	                modelIns.put("outWhId"          , null);
	                
	                modelIns.put("transCustId"      , null);
	                modelIns.put("vrCustId"         , (String)model.get("vrSrchCustId"));
	                modelIns.put("pdaFinishYn"      , null);
	                modelIns.put("blNo"             , null);
	                modelIns.put("workStat"         , "100");
	                
	                modelIns.put("ordType"          , "01");
	                modelIns.put("ordSubtype"       , (String)model.get("I_IN_ORD_SUBTYPE"+i)); //주문상세
	                modelIns.put("outReqDt"         , null);
	                modelIns.put("outDt"            , null);
	                modelIns.put("pdaStat"          , null);
	                
	                modelIns.put("workSeq"          , null);
	                modelIns.put("capaTot"          , "0");
	                modelIns.put("kinOutYn"         , "N");
	                modelIns.put("carConfYn"        , "N");
	                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
	                
	                modelIns.put("tplOrdId"         , null);
	                modelIns.put("approveYn"        , "N");
	                modelIns.put("payYn"            , "N");
	                modelIns.put("inCustId"         , (String)model.get("I_IN_CUST_ID"+i));
	                modelIns.put("inCustAddr"       , null);
	                
	                modelIns.put("inCustEmpNm"      , null);
	                modelIns.put("inCustTel"        , null);
	                modelIns.put("lotType"          , (String)model.get("I_LOT_TYPE"+i));
	                modelIns.put("ownerCd"          , (String)model.get("I_OWNER_CD"+i));
	                
	                //sub
	                modelIns.put("dsSub_rowStatus"  , dsSubRowStatus);
	                modelIns.put("ordSeq"           , ordSeq);
	                modelIns.put("ritemId"          , ritemId);
	                modelIns.put("custLotNo"        , custLotNo);
	                modelIns.put("realInQty"        , realInQty);
	                
	                modelIns.put("realOutQty"       , realOutQty);
	                modelIns.put("makeDt"           , makeDt);
	                modelIns.put("timePeriodDay"    , timePeriodDay);
	                modelIns.put("locYn"            , locYn);
	                modelIns.put("pdaCd"            , pdaCd);
	                
	                modelIns.put("workYn"           , workYn);
	                modelIns.put("rjType"           , rjType);
	                modelIns.put("realPltQty"       , realPltQty);
	                modelIns.put("realBoxQty"       , realBoxQty);
	                modelIns.put("confYn"           , confYn);
	                
	                modelIns.put("unitAmt"          , unitAmt);
	                modelIns.put("amt"              , amt);
	                modelIns.put("eaCapa"           , eaCapa);
	                modelIns.put("boxBarcode"       , boxBarcode);
	                modelIns.put("inOrdUomId"       , inOrdUomId);
	                modelIns.put("inWorkUomId"   , inWorkUomId);
	                
	                modelIns.put("outOrdUomId"      , outOrdUomId);
	                modelIns.put("outWorkUomId"  , outWorkUomId);
	                modelIns.put("inOrdQty"         , inOrdQty);
	                modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
	                modelIns.put("outOrdQty"        , outOrdQty);
	                
	                modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
	                modelIns.put("refSubLotId"      , refSubLotId);
	                modelIns.put("dspId"            , dspId);                
	                modelIns.put("carId"            , carId);
	                modelIns.put("cntrId"           , cntrId);
	                
	                modelIns.put("cntrNo"           , cntrNo);
	                modelIns.put("cntrType"         , cntrType);
	                modelIns.put("badQty"           , badQty);
	                modelIns.put("uomNm"            , uomNm);
	                modelIns.put("unitPrice"        , unitPrice);
	                
	                modelIns.put("whNm"             , whNm);
	                modelIns.put("itemKorNm"        , itemKorNm);
	                modelIns.put("itemEngNm"        , itemEngNm);
	                modelIns.put("repUomId"         , repUomId);
	                modelIns.put("uomCd"            , uomCd);
	                
	                modelIns.put("uomId"            , uomId);
	                modelIns.put("repUomCd"         , repUomCd);
	                modelIns.put("repuomNm"         , repuomNm);
	                modelIns.put("itemGrpId"        , itemGrpId);
	                modelIns.put("expiryDate"       , expiryDate);
	                
	                modelIns.put("inOrdWeight"      , inOrdWeight); 
	                modelIns.put("unitNo"           , unitNo);
	                modelIns.put("ordDesc"          , ordDesc);
	                modelIns.put("validDt"          , validDt);           
	                modelIns.put("etc2"             , etc2);
	                modelIns.put("locCd"             , locCd);
	                
	                //추가
	                modelIns.put("shipmentNo"       , (String)model.get("I_ORD_DIV"+i));
	                
	                //추가된거(아직프로시져는 안탐)
	                modelIns.put("itemBestDate"     , itemBestDate);
	                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);               
	                modelIns.put("rtiNm"            , rtiNm);
	                
	                //session 정보
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	
	               //dao                
//	                modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
	                
  	                //dao 변경 (2021.07.15)
	                //modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
	                //System.out.println((String)model.get("I_ST_GUBUN"+i));

	                
	                //dao 변경 (2021.11.08) 
	                //System.out.println("------------shipmentNo : " +(String)model.get("I_ORD_DIV"+i));
	                modelIns = (Map<String, Object>)dao.saveInOrder_2048_21(modelIns);
	                
	                ServiceUtil.isValidReturnCode("WMSOM020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));  
	            }
                              

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }
            
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    

    /**
     * 
     * 대체 Method ID	: listDetail
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listNew(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listNew(model));
        return map;
    }
    

    /**
     * 분할전
     * 대체 Method ID   : deleteInOrder
     * 대체 Method 설명    : 주문삭제
     * 작성자                      : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteInOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
           
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
            	
	            for(int i = 0 ; i < insCnt ; i ++){
	                //저장, 수정

	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                
	                //main
	                modelIns.put("orgOrdId"         , (String)model.get("I_IN_ORG_ORD_ID"+i));   
	                modelIns.put("orgOrdSeq"        , (String)model.get("I_IN_ORD_DETAIL_NO"+i));
	                
	                
	                //session 정보
	                modelIns.put("LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	
	                //dao         
	                try{
	                	dao.deleteInOrder(modelIns);
	                }catch(Exception e){
	                	throw new BizException("삭제 실패");
	                }
	                //System.out.println((String)model.get("I_ST_GUBUN"+i));
	                
	                //ServiceUtil.isValidReturnCode("WMSOM020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));  
	            }
                              

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }
            
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    @Override
    public Map<String, Object> saveCustLotNo(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try{
             int temCnt = Integer.parseInt(model.get("selectIds").toString());
             
             if(temCnt > 0){
                 for(int i = 0 ; i < temCnt ; i ++){
                	 int size = Integer.parseInt((String)model.get("ORG_ORD_QTY"+i));
                	 
                	 String[] orgDetailNo = new String[size];
                     String[] orgOrdQty 	= new String[size];
                     String[] orgOrdNo 		= new String[size];
                     String[] orgItemCd 	= new String[size];
                     String[] custLotNo 	= new String[size];
                     String[] custId 		= new String[size];
                     String[] itemBarCd	 	= new String[size];
                     String[] ritemId 		= new String[size];
                     String[] lcId			= new String[size];
                     String[] issueDate			= new String[size];
	                     
                     String[] mapItemBarCd	= new String[size];
                     String[] makeLineNo	= new String[size];
                     
                     
                	 Map<String, Object> modelDS = new HashMap<String, Object>();
                     for(int k = 0 ; k < size ; k++ ) {
                    	orgOrdQty[k]   	= "1";
	                   	orgItemCd[k]   	= (String)model.get("ORG_ITEM_CD"+i);
	                   	orgOrdNo[k]    	= (String)model.get("ORG_ORD_NO"+i);
	                   	orgDetailNo[k] 	= (String)model.get("ORG_DETAIL_NO"+i);
	                   	custId[k]		= (String)model.get("CUST_ID"+i);
	                   	itemBarCd[k]	= (String)model.get("ITEM_BAR_CD"+i);
	                   	ritemId[k]		= (String)model.get("RITEM_ID"+i);
	                   	lcId[k]			= (String)model.get("LC_ID"+i);
	                   	issueDate[k]	= DateUtil.getCurrentDate("yyyyMMdd");
	                   	
	                   	mapItemBarCd[k]	= "";
	                   	makeLineNo[k]	= "";
                   	 
	                   	//dao.getCustLotNo Map
	                   	modelDS.put("lcId"     		, (String)model.get("LC_ID"+i));
	                   	modelDS.put("custId"     	, (String)model.get("CUST_ID"+i));
	                   	modelDS.put("orgItemCd"    	, (String)model.get("ORG_ITEM_CD"+i));
                        modelDS.put("orgGubun"    	, (String)model.get("ORG_DETAIL_NO"+i) + k);
                        
                    	custLotNo[k] = (String)dao.getCustLotNo(modelDS);
                    }
                     
	                // 프로시저 wmsom020.wmsif050.sp_in_issue_label_lb_ord_complate
                    Map<String, Object> modelIns = new HashMap<String, Object>();
                    modelIns.put("epcCd"   			, null);
                    modelIns.put("lcId"    			, lcId);
                    modelIns.put("ordId"   			, orgOrdNo);
                    modelIns.put("ordSeq"   		, orgDetailNo);
                    modelIns.put("custId"   		, custId);
                    
                    modelIns.put("ritemId"   		, ritemId);
                    modelIns.put("issueDate" 		, issueDate);
                    modelIns.put("makeDate"   		, null);
                    modelIns.put("itemBestEndDate"  , null);
                    modelIns.put("workQty"   		, null);
                    
                    modelIns.put("makeLineNo"   	, makeLineNo);
                    modelIns.put("itemBarCd"   		, itemBarCd);
                    modelIns.put("childCustLotNo"   , custLotNo);
                    modelIns.put("mapItemQty"   	, orgOrdQty);
                    modelIns.put("mapItemBarCd"   	, mapItemBarCd);
                    
                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                    
                    modelIns = (Map<String, Object>)dao.issueLabelOrdCom(modelIns);
                    ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                 }
             }   
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("save.success"));
             
         } catch(BizException be) {
             m.put("errCnt", -1);
             m.put("MSG", be.getMessage() );
             
         } catch(Exception e){
        	 m.put("errCnt", 0);
             throw e;
         }
         return m;
     }  
    
//    @Override
//    public Map<String, Object> saveCustLotNoUpdate(Map<String, Object> model) throws Exception {
//    	 Map<String, Object> m = new HashMap<String, Object>();
//    	 try{
//             int temCnt = Integer.parseInt(model.get("selectIds").toString());
//             
//             if(temCnt > 0){ // 2
//                 for(int i = 0 ; i < temCnt ; i ++){ // 0 1
//                	 int size = Integer.parseInt((String)model.get("ORG_ORD_QTY"+i));
//                	 
//                	 String[] orgDetailNo = new String[size];
//                     String[] orgOrdQty 	= new String[size];
//                     String[] orgOrdNo 		= new String[size];
//                     String[] orgItemCd 	= new String[size];
//                     String[] custLotNo 	= new String[size];
//                     String[] custId 		= new String[size];
//                     String[] itemBarCd	 	= new String[size];
//                     String[] ritemId 		= new String[size];
//                     String[] lcId			= new String[size];
//                     String[] issueDate			= new String[size];
//	                     
////                	 Map<String, Object> modelDS = new HashMap<String, Object>();
//                     for(int k = 0 ; k < size ; k++ ) {
//                    	orgOrdQty[k]   	= "1";
//	                   	orgItemCd[k]   	= (String)model.get("ORG_ITEM_CD"+i);
//	                   	orgOrdNo[k]    	= (String)model.get("ORG_ORD_NO"+i);
//	                   	orgDetailNo[k] 	= (String)model.get("ORG_DETAIL_NO"+i);
//	                   	custId[k]		= (String)model.get("CUST_ID"+i);
//	                   	itemBarCd[k]	= (String)model.get("ITEM_BAR_CD"+i);
//	                   	ritemId[k]		= (String)model.get("RITEM_ID"+i);
//	                   	lcId[k]			= (String)model.get("LC_ID"+i);
//	                   	issueDate[k]	= DateUtil.getCurrentDate("yyyyMMdd");
//	                   	custLotNo[k]	= (String)model.get("CUST_LOT_NO"+i);
//	                   	//dao.getCustLotNo Map
////	                   	modelDS.put("lcId"     		, (String)model.get("LC_ID"+i));
////	                   	modelDS.put("custId"     	, (String)model.get("CUST_ID"+i));
////	                   	modelDS.put("orgItemCd"    	, (String)model.get("ORG_ITEM_CD"+i));
////                        modelDS.put("orgGubun"    	, (String)model.get("ORG_DETAIL_NO"+i) + k);
//                        
////                    	custLotNo[k] = (String)dao.getCustLotNo(modelDS);
//                    }
//                     
//	                // 프로시저 wmsom020.wmsif050.sp_in_issue_label_lb_ord_complate
//                    Map<String, Object> modelIns = new HashMap<String, Object>();
//                    modelIns.put("epcCd"   			, null);
//                    modelIns.put("lcId"    			, lcId);
//                    modelIns.put("ordId"   			, orgOrdNo);
//                    modelIns.put("ordSeq"   		, orgDetailNo);
//                    modelIns.put("custId"   		, custId);
//                    
//                    modelIns.put("ritemId"   		, ritemId);
//                    modelIns.put("issueDate" 		, issueDate);
//                    modelIns.put("makeDate"   		, null);
//                    modelIns.put("itemBestEndDate"  , null);
//                    modelIns.put("workQty"   		, null);
//                    
//                    modelIns.put("makeLineNo"   	, null);
//                    modelIns.put("itemBarCd"   		, itemBarCd);
//                    modelIns.put("childCustLotNo"   , custLotNo);
//                    modelIns.put("mapItemQty"   	, orgOrdQty);
//                    modelIns.put("mapItemBarCd"   	, null);
//                    
//                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
//                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
//                    
//                    modelIns = (Map<String, Object>)dao.issueLabelOrdCom(modelIns);
//                    ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
//                 }
//             }   
//             m.put("errCnt", 0);
//             m.put("MSG", MessageResolver.getMessage("save.success"));
//             
//         } catch(BizException be) {
//             m.put("errCnt", -1);
//             m.put("MSG", be.getMessage() );
//             
//         } catch(Exception e){
//        	 m.put("errCnt", 0);
//             throw e;
//         }
//         return m;
//     }  
    
    @Override
    public Map<String, Object> saveCustLotNoUpdate(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         try{
             int temCnt = Integer.parseInt(model.get("selectIds").toString());
             
             if(temCnt > 0){
                 for(int i = 0 ; i < temCnt ; i ++){
                	 int size = Integer.parseInt((String)model.get("ORG_ORD_QTY"+i));
                	 
                	 String[] orgDetailNo = new String[size];
                     String[] orgOrdQty 	= new String[size];
                     String[] orgOrdNo 		= new String[size];
                     String[] orgItemCd 	= new String[size];
                     String[] custLotNo 	= new String[size];
                     String[] custId 		= new String[size];
                     String[] itemBarCd	 	= new String[size];
                     String[] ritemId 		= new String[size];
                     String[] lcId			= new String[size];
                     String[] issueDate			= new String[size];
	                     
                     String[] mapItemBarCd	= new String[size];
                     String[] makeLineNo	= new String[size];
                     
                	 Map<String, Object> modelDS = new HashMap<String, Object>();
                     for(int k = 0 ; k < size ; k++ ) {
                    	orgOrdQty[k]   	= "1";
	                   	orgItemCd[k]   	= (String)model.get("ORG_ITEM_CD"+i);
	                   	orgOrdNo[k]    	= (String)model.get("ORG_ORD_NO"+i);
	                   	orgDetailNo[k] 	= (String)model.get("ORG_DETAIL_NO"+i);
	                   	custId[k]		= (String)model.get("CUST_ID"+i);
	                   	itemBarCd[k]	= (String)model.get("ITEM_BAR_CD"+i);
	                   	ritemId[k]		= (String)model.get("RITEM_ID"+i);
	                   	lcId[k]			= (String)model.get("LC_ID"+i);
	                   	issueDate[k]	= DateUtil.getCurrentDate("yyyyMMdd");
	                   	
	                   	makeLineNo[k]	= "";

	                   	mapItemBarCd[k]	= (String)model.get("MAP_ITEM_BAR_CD"+i);
//	                   	System.out.println("MAP_ITEM_BAR_CD : "+(String)model.get("MAP_ITEM_BAR_CD"+i));
	                   	
	                   	//dao.getCustLotNo Map
	                   	modelDS.put("lcId"     		, (String)model.get("LC_ID"+i));
	                   	modelDS.put("custId"     	, (String)model.get("CUST_ID"+i));
	                   	modelDS.put("orgItemCd"    	, (String)model.get("ORG_ITEM_CD"+i));
                        modelDS.put("orgGubun"    	, (String)model.get("ORG_DETAIL_NO"+i) + k);
                        
                    	custLotNo[k] = (String)dao.getCustLotNo(modelDS);
//                    	System.out.println("LOT_NO : " + custLotNo[k]);
                    }
                     
	                // 프로시저 wmsom020.wmsif050.sp_in_issue_label_lb_ord_complate
                    Map<String, Object> modelIns = new HashMap<String, Object>();
                    modelIns.put("epcCd"   			, null);
                    modelIns.put("lcId"    			, lcId);
                    modelIns.put("ordId"   			, orgOrdNo);
                    modelIns.put("ordSeq"   		, orgDetailNo);
                    modelIns.put("custId"   		, custId);
                    
                    modelIns.put("ritemId"   		, ritemId);
                    modelIns.put("issueDate" 		, issueDate);
                    modelIns.put("makeDate"   		, null);
                    modelIns.put("itemBestEndDate"  , null);
                    modelIns.put("workQty"   		, null);
                    
                    modelIns.put("makeLineNo"   	, makeLineNo);
                    modelIns.put("itemBarCd"   		, itemBarCd);
                    modelIns.put("childCustLotNo"   , custLotNo);
                    modelIns.put("mapItemQty"   	, orgOrdQty);
                    modelIns.put("mapItemBarCd"   	, mapItemBarCd);
                    
                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                    
                    modelIns = (Map<String, Object>)dao.issueLabelOrdCom(modelIns);
                    ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                 }
             }   
             m.put("errCnt", 0);
             m.put("MSG", MessageResolver.getMessage("save.success"));
             
         } catch(BizException be) {
             m.put("errCnt", -1);
             m.put("MSG", be.getMessage() );
             
         } catch(Exception e){
        	 m.put("errCnt", 0);
             throw e;
         }
         return m;
     }  
}
