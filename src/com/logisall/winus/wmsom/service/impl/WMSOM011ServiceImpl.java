package com.logisall.winus.wmsom.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsom.service.WMSOM011Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOM011Service")
public class WMSOM011ServiceImpl implements WMSOM011Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM011Dao")
    private WMSOM011Dao dao;
    
    /**
     * 
     * 대체 Method ID	: listDetail
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listDetail(model));
        return map;
    }
}
