package com.logisall.winus.wmsom.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOM090Dao")
public class WMSOM090Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 메인 거래처 조회
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsom090.list", model);
    } 
    
    /**
	 * Method ID : listDetail
	 * Method 설명 : 매출거래처 정보 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listItemTabs1(Map<String, Object> model) {
		return executeQueryPageWq("wmsom090.listItemTabs1", model);
	}
	
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID    : saveSimpleOutOrderTemp
     * Method 설명      : 거래처발주 Temp 입력
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSimpleOutOrderTemp(Map<String, Object> model){
        executeUpdate("wmsom090.pk_wmsop010.sp_simple_insert_order", model);
        return model;
    }
    
    /**
	 * Method ID : ReqOrdSeq
	 * 
	 * @param model
	 * @return
	 */
	public Object ReqOrdSeq(Map<String, Object> model){
        return executeQueryForList("wmsom090.ReqOrdSeq", model);
    }
	
	/**
	 * Method ID : listOrdDetail
	 * Method 설명 : 매출거래처 정보 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listOrdDetail(Map<String, Object> model) {
		return executeQueryPageWq("wmsom090.listOrdDetail", model);
	}
	
	public void updateOm090(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmsom090.updateOm090", modelDt);
	}
	
	public Object listSubQtyExchange(Map<String, Object> model){
        return executeQueryForList("wmsom090.listSubQtyExchange", model);
    }
	
	
}



