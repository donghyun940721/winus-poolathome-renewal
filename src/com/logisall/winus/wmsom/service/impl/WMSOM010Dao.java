package com.logisall.winus.wmsom.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOM010Dao")
public class WMSOM010Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsom010.list", model);
    }
    
    /**
     * Method ID  : listDetail
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsom010.listDetail", model);
    }
    
    
    /**
     * Method ID  : listTel
     * Method 설명   : 
     * 작성자                : khkim
     * @param   model
     * @return
     */
    public GenericResultSet listCar(Map<String, Object> model) {
        return executeQueryPageWq("wmsom010.listCar", model);
    }
    
    /**
     * Method ID  : listTelDetail
     * Method 설명   : 
     * 작성자                : khkim
     * @param   model
     * @return
     */
    public GenericResultSet listTelDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsom010.listTelDetail", model);
    }

	 /**
     * Method ID : confirmOrder 
     * Method 설명 : 상품정보 저장 
     * 작성자 : khkim
     * 
     * @param model
     * @return
     */
    public Object confirmOrder(Map<String, Object> model) {
	return executeUpdate("wmsom010.PK_WMSOM010.SP_EDIYA_WINUS_INSERT_ORDER", model);
    }
}


