package com.logisall.winus.wmsom.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsom.service.WMSOM080Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOM080Service")
public class WMSOM080ServiceImpl implements WMSOM080Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM080Dao")
    private WMSOM080Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 메인 거래처 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * Method ID : listDetail
     * Method 설명 : 매출거래처정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {          
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listDetail(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * 대체 Method ID   : selectItemGrp
     * 대체 Method 설명    : 입고관리 화면에서 필요한 데이터 
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        log.info(model);
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveSimpleOutOrderTemp
     * 대체 Method 설명    : 거래처발주 Temp 입력
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSimpleOutOrderTemp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            if(totCnt > 0){
                //저장, 수정            
                         
                String[] ritemId    = new String[totCnt];     
                String[] custLotNo  = new String[totCnt];     
                String[] repUomId   = new String[totCnt];
                
                String[] ordQty     = new String[totCnt];
                String[] ordDesc    = new String[totCnt];   
                String[] etc2       = new String[totCnt];
                
                for(int i = 0 ; i < totCnt ; i ++){             
                    ritemId[i]      = (String)model.get("RITEM_ID"+i);      
                    custLotNo[i]    = (String)model.get("CUST_LOT_NO"+i);      
                    repUomId[i]     = (String)model.get("UOM_ID"+i);    
                    
                    ordQty[i]       = (String)model.get("ORD_QTY"+i);
                    ordDesc[i]      = (String)model.get("ORD_DESC"+i); 
                    etc2[i]         = (String)model.get("ETC2"+i); 
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("whId"    			, model.get("vrWhId"));
                modelIns.put("transCustId"      , model.get("txtSrchCustId"));
                modelIns.put("custId"           , model.get("vrSrchCustId"));
                modelIns.put("ordType"          , model.get("vrOrdType"));
                modelIns.put("ReqDt"            , model.get("vrCalOutReqDt2").toString().replace("-", ""));
                modelIns.put("lcId"             , (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("payYn"            , model.get("vrPayYn"));
                modelIns.put("vrConfirmYn"      , model.get("vrConfirmYn"));
                
                //sub
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("repUomId"         , repUomId);
      
                modelIns.put("ordQty"           , ordQty);
                modelIns.put("ordDesc"          , ordDesc);          
                modelIns.put("etc2"             , etc2);
                
                //session 정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveSimpleOutOrderTemp(modelIns);
                ServiceUtil.isValidReturnCode("WMSOM080", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }                     
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
}
