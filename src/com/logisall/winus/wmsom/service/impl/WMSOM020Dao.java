package com.logisall.winus.wmsom.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOM020Dao")
public class WMSOM020Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsom020.list", model);
    }
    
    /**
     * Method ID  : listDetail
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsom020.listDetail", model);
    }
    
    
    /**
     * Method ID  : listTel
     * Method 설명   : 
     * 작성자                : khkim
     * @param   model
     * @return
     */
    public GenericResultSet listCar(Map<String, Object> model) {
        return executeQueryPageWq("wmsom020.listCar", model);
    }
    
    /**
     * Method ID  : listTelDetail
     * Method 설명   : 
     * 작성자                : khkim
     * @param   model
     * @return
     */
    public GenericResultSet listTelDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsom020.listTelDetail", model);
    }
    
	 /**
     * Method ID : confirmOrder 
     * Method 설명 : 입고확정- 날짜로만
     * 작성자 : khkim
     * 
     * @param model
     * @return
     */
    public Object confirmOrder(Map<String, Object> model) {
    	return executeUpdate("wmsom020.PK_WMSOM010.SP_EDIYA_WINUS_INSERT_ORDER", model);
    }
    
    /**
     * Method ID    : saveInOrder
     * Method 설명      : 입고확정
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object saveInOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_insert_order", model);
        return model;
    }
    
    /**
     * Method ID    : saveInOrder
     * Method 설명      : 입고확정
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object saveInOrder_2048(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_insert_order_2048", model);
        return model;
    }

    
    /**
     * Method ID    : saveInOrder_2048_21
     * Method 설명      : 입고확정
     * 작성자                 : YHKU
     * @param   model
     * @return
     */
    public Object saveInOrder_2048_21(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop021.sp_new_insert_order_2048", model);
        return model;
    }
    
    
    
    /**
     * Method ID    : listNew
     * Method 설명      : 
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
	public Object listNew(Map<String, Object> model) {
		return executeQueryPageWq("wmsom020.listNew", model);
	}
	
	/**
     * Method ID    : deleteInOrder
     * Method 설명      : 입고확정
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
	public Object deleteInOrder(Map<String, Object> model) {
		return executeUpdate("wmsom020.deleteInOrder", model);
	}
	
	 public Object getCustLotNo(Map<String, Object> model){
        return executeView("wmsom020.getCustLotNo", model);
    }
	 
	 public Object issueLabelOrdCom(Map<String, Object> model){
        executeUpdate("wmsom020.wmsif050.sp_in_issue_label_lb_ord_complate", model);
        return model;
    }
	 
	 
}


