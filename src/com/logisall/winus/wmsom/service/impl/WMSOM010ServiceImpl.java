package com.logisall.winus.wmsom.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsom.service.WMSOM010Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOM010Service")
public class WMSOM010ServiceImpl implements WMSOM010Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM010Dao")
    private WMSOM010Dao dao;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listDetail
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listDetail(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listCar
     * 대체 Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCar(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listCar(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listTelDetail
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listTelDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listTelDetail(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: confirmOrder
     * 대체 Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> confirmOrder(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
        
    	map.put("I_INSERT_DATE",      model.get("vrCalDlvDt"));
    	map.put("I_WORK_IP",      model.get("SS_CLIENT_IP"));
    	map.put("I_USER_NO",      model.get("SS_USER_NO"));
	    
    	@SuppressWarnings("unchecked")
		Map<String, Object> returnMap = (Map<String, Object>) dao.confirmOrder(model);
    	
    	/*int errCode = Integer.parseInt(returnMap.get("O_MSG_CODE").toString());
        String errMsg = returnMap.get("O_MSG_NAME").toString();
        
    	map.put("errCode", errCode);
    	map.put("errMsg", errMsg);*/
    
        
    	return returnMap;
    }
}
