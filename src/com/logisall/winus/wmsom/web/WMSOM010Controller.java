package com.logisall.winus.wmsom.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsom.service.WMSOM010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOM010Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM010Service")
    private WMSOM010Service service;
    
    /**
     * Method ID	: wmsom010
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM010.action")
    public ModelAndView wmsom010(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM010");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM010/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM010/listDetail.action")
    public ModelAndView listDetail(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
	 * Method ID    : /WMSOM010pop.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM010pop.action")
	public ModelAndView wmsom010pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM010pop");
	}
	
	/*-
	 * Method ID    : /WMSOM010pop2.action
	 * Method 설명      : 
	 * 작성자                 : khKim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM010pop2.action")
	public ModelAndView wmsom010pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM010pop2");
	}
	
    /**
     * Method ID	: listCar
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM010/listCar.action")
    public ModelAndView listCar(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listCar(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    

    /**
     * Method ID	: listTelDetail
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM010/listTelDetail.action")
    public ModelAndView listTelDetail(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listTelDetail(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: confirmOrder
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM010/confirmOrder.action")
    public ModelAndView confirmOrder(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.confirmOrder(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
}
