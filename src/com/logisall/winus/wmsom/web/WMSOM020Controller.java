package com.logisall.winus.wmsom.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsom.service.WMSOM020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOM020Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM020Service")
    private WMSOM020Service service;
    
    /**
     * Method ID	: wmsom120
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM120.action")
    public ModelAndView wmsom120(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM120");
    }
    
    /**
     * Method ID	: wmsom010
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM020.action")
    public ModelAndView wmsom010(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM020");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/listDetail.action")
    public ModelAndView listDetail(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
   
    
    /*-
	 * Method ID    : /WMSOM020pop.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM020pop.action")
	public ModelAndView wmsom010pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM020pop");
	}
	
	/*-
	 * Method ID    : /WMSOM020pop2.action
	 * Method 설명      : 
	 * 작성자                 : khKim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM020pop2.action")
	public ModelAndView wmsom010pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM020pop2");
	}
	
    /**
     * Method ID	: confirmOrder
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/confirmOrder.action")
    public ModelAndView confirmOrder(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.confirmOrder(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: confirmOrder
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/saveInOrder.action")
    public ModelAndView saveInOrder(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jsonView", service.saveInOrder(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: KHKIM
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM020/listNew.action")
    public ModelAndView listNew(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listNew(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
    /*
    * Method ID	: wmsom120pop
    * Method 설명	: 
    * 작성자			: KHKIM
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSOM120pop.action")
   public ModelAndView wmsom120pop(Map<String, Object> model) throws Exception {
   	return new ModelAndView("winus/wmsom/WMSOM120pop");
   }
   
   @RequestMapping("/WMSOM120pop2.action")
   public ModelAndView WMSOM120pop2(Map<String, Object> model) throws Exception {
   	return new ModelAndView("winus/wmsom/WMSOM120pop2");
   }
   
   /**
    * Method ID	: confirmOrder
    * Method 설명	: 
    * 작성자			: khkim
    * @param   model
    * @return  
    * @throws Exception 
    */
   @RequestMapping("/WMSOM020/deleteInOrder.action")
   public ModelAndView deleteInOrder(Map<String, Object> model) throws Exception {
   	ModelAndView mav = null;
       
       try {
           mav = new ModelAndView("jsonView", service.deleteInOrder(model));
       } catch (Exception e) {
           e.printStackTrace();
       }
       return mav;
   }
   
   
   @RequestMapping("/WMSOM020/saveCustLotNo.action")
	public ModelAndView outPickingCancel(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCustLotNo(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}				 
		mav.addAllObjects(m);
		return mav;
	}
   
   @RequestMapping("/WMSOM020/saveCustLotNoUpdate.action")
	public ModelAndView saveCustLotNoUpdate(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCustLotNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}				 
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOM120E2.action")
	public ModelAndView wmsom120e2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("winus/wmsom/WMSOM120E2");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
}
