package com.logisall.winus.wmsom.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsom.service.WMSOM010Service;
import com.logisall.winus.wmsom.service.WMSOM011Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOM030Controller {
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSOM011Service")
    private WMSOM011Service service;
    
    /**
     * Method ID	: wmsom010
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM030.action")
    public ModelAndView wmsom010(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM030");
    }
	
    /*-
	 * Method ID    : /WMSOM030pop.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM030pop.action")
	public ModelAndView wmsom030pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM030pop");
	}
	
	/*-
	 * Method ID    : /WMSOM030popSearo.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM030popSearo.action")
	public ModelAndView WMSOM030popSearo(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM030popSearo");
	}
	
	/*-
	 * Method ID    : /WMSOM030pop2.action
	 * Method 설명      : 
	 * 작성자                 : khKim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM030pop2.action")
	public ModelAndView wmsom030pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM011pop");
		//return new ModelAndView("winus/wmsom/WMSOM030pop2");
	}

	/*-
	 * Method ID    : /WMSOM030pop3.action
	 * Method 설명      : 
	 * 작성자                 : khKim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM030pop3.action")
	public ModelAndView wmsom030pop3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSOM012pop");
	}
	
	/*-
	 * Method ID    : /WMSOM030pop4.action
	 * Method 설명      : 
	 * 작성자                 : khKim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOM030pop4.action")
	public ModelAndView wmsom030pop4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmspk/WMSPK_REPORT2");
	}
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM030/listDetail.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
	
}
