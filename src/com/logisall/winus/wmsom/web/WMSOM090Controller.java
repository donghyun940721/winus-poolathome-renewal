package com.logisall.winus.wmsom.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsom.service.WMSOM090Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOM090Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOM090Service")
    private WMSOM090Service service;
    
    /**
     * Method ID    : wmsom090
     * Method 설명      : 거래처발주
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOM090.action")
    public ModelAndView wmsom090(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsom/WMSOM090", service.selectItemGrp(model));
    }
	
    /**
     * Method ID    : list
     * Method 설명      : 메인 거래처 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOM090/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
     * Method ID : listItemTabs1
     * Method 설명 : 매출거래처관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSOM090/listItemTabs1.action")
    public ModelAndView listItemTabs1(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listItemTabs1(model));
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
        }
        return mav;
    }
    
    /*-
	 * Method ID    : saveSimpleOutOrderTemp
	 * Method 설명      : 거래처발주 Temp 입력
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOM090/saveSimpleOutOrderTemp.action")
	public ModelAndView saveSimpleOutOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSimpleOutOrderTemp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : ReqOrdSeq
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOM090/ReqOrdSeq.action")
	public ModelAndView ReqOrdSeq(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.ReqOrdSeq(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID : listOrdDetail
     * Method 설명 : 매출거래처관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSOM090/listOrdDetail.action")
    public ModelAndView listOrdDetail(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listOrdDetail(model));
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
        }
        return mav;
    }
    
    @RequestMapping("/WMSOM090/updateOm090.action")
    public ModelAndView updateE03(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.updateOm090(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }  
    
    @RequestMapping("/WMSOM090/listSubQtyExchange.action")
	public ModelAndView listSubQtyExchange(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.listSubQtyExchange(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
    
}
