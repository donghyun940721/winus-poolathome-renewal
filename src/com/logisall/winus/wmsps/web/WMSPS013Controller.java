package com.logisall.winus.wmsps.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsps.service.WMSPS013Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSPS013Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPS013Service")
	private WMSPS013Service service;

	/*-
	 * Method ID    : wmsps013
	 * Method 설명      : 물류용기재고화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSPS013.action")
	public ModelAndView wmsps013(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsps/WMSPS013", service.selectPoolGrp(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 물류용기 현재고 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPS013/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listSub
	 * Method 설명      : 물류용기 현재고 상세조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPS013/listSub.action")
	public ModelAndView listSub(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list(sub) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : save
	 * Method 설명      : 물류용기 현재고 비고저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPS013/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 물류용기 현재고 엑셀
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPS013/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                    {MessageResolver.getMessage("화주")        ,"0"  ,"1" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("부자재군")    ,"2"  ,"2" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("포장부자재")     ,"3"  ,"4" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("전일재고")     ,"5"  ,"5" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("입고")        ,"6"  ,"6" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("일반출고")     ,"7"  ,"7" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("이벤트출고")    ,"8"  ,"8" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("정상재고")     ,"9"  ,"9" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("불량재고")     ,"10" ,"10","0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("AS재고")      ,"11" ,"11","0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("적정재고")     ,"12" ,"12","0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("비고")        ,"13" ,"13","0" ,"0" ,"200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                     {"CUST_CD"           , "S"}
                                    ,{"CUST_NM"           , "S"}
                                    ,{"ITEM_GRP_NM"       , "S"}
                                    ,{"RITEM_CD"          , "S"}
                                    ,{"RITEM_NM"          , "S"}
                                    
                                    ,{"BEFORE_STOCK_QTY"  , "N"}
                                    ,{"IN_QTY"            , "N"}
                                    ,{"OUT_QTY"           , "N"}
                                    ,{"EVENT_QTY"         , "N"}
                                    ,{"STOCK_QTY"         , "N"}
                                    
                                    ,{"BAD_QTY"           , "N"}
                                    ,{"AS_QTY"            , "N"}
                                    ,{"PROP_QTY"          , "N"}
                                    ,{"REMARK"            , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getMessage("포장부자재재고");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
    }
}
