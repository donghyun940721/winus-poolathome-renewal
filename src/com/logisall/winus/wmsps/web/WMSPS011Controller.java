package com.logisall.winus.wmsps.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsps.service.WMSPS011Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSPS011Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPS011Service")
	private WMSPS011Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 현재고 조회 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSPS011.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsps/WMSPS011", service.selectBox(model));
	}

	/*-
	 * Method ID : poplist
	 * Method 설명 : 세트상품 조회 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPS011/poplist.action")
	public ModelAndView poplist(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listPop(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : popdetail
	 * Method 설명 : 세트상품 상세조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPS011/popdetail.action")
	public ModelAndView popdetail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.detailPop(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : list
	 * Method 설명 : 현재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPS011/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : sublist
	 * Method 설명 : 현재고 서브 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPS011/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get sub list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 현재고  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSPS011/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 현재고 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSPS011/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주코드"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("완제품군"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("완제품코드"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("완제품명"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("전일재고"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("일반출고"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("크로스도킹"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("이벤트출고"), "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("반품출고"), "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("정상재고"), "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("BOX수량"), "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량"), "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("불량재고"), "14", "14", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("AS재고"), "15", "15", "0", "0", "200"},
                                   {"UOM", "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("적정재고"), "17", "17", "0", "0", "200"},
                                   //{MessageResolver.getMessage("입고예정"), "18", "18", "0", "0", "200"},
                                   //{MessageResolver.getMessage("입고완료후예정재고"), "19", "19", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("비고"), "18", "18", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"           	, "S"},
                                    {"CUST_NM"           	, "S"},
                                    {"ITEM_GRP_NM"       	, "S"},
                                    {"RITEM_CD"          	, "S"},
                                    {"RITEM_NM"          	, "S"},
                                    
                                    {"BEFORE_STOCK_QTY"  	, "N"},
                                    {"IN_QTY"            	, "N"},
                                    {"OUT_QTY"           	, "N"},
                                    {"CROSS_QTY"         	, "N"},
                                    {"EVENT_QTY"         	, "N"},
                                    
                                    {"RETURN_QTY"        	, "N"},
                                    {"STOCK_QTY"        	, "N"},
                                    {"BOX_QTY"           	, "N"},
                                    {"PLT_QTY"           	, "N"},
                                    {"BAD_QTY"           	, "N"},
                                    
                                    {"AS_QTY"            	, "N"},
                                    {"UOM_NM"            	, "S"},
                                    {"PROP_QTY"             , "N"},
                                    //{"IN_ORD_EXP_QTY"       , "N"},
                                    //{"IN_ORD_EXP_TOTAL_QTY" , "N"},
                                    
                                    {"REMARK"               , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("포장완제품재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID : popExcel
	 * Method 설명 : 세트팝업 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSPS011/popexcel.action")
	public void popExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.selectPopExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pop excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown2
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주명"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("완제품코드"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("완제품명"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("수량"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM"), "4", "4", "0", "0", "200"},
                                   {"/", "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("코드"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("완제품명"), "7", "7", "0", "0", "200"},
                                   {"QTY", "8", "8", "0", "0", "200"},
                                   {"UOM", "9", "9", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"           , "S"},
                                    {"RITEM_CD"          , "S"},
                                    {"RITEM_NM"          , "S"},
                                    {"STOCK_QTY"         , "N"},
                                    {"UOM_NM"            , "S"},
                                    
                                    {"M"                 , "S"},
                                    {"PART_RITEM_CD"     , "S"},
                                    {"PART_RITEM_NM"     , "S"},
                                    {"PART_QTY"          , "N"},
                                    {"PART_UOM_NM"       , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("포장완제품재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID : listExcel2
	 * Method 설명 : 현재고 상세 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSPS011/excel2.action")
	public void listExcel2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown3(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	/*-
	 * Method ID : doExcelDown3
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown3(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("포장완제품"), "2", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("재고수량"), "4", "4", "0", "0", "200"},
                                   {"UOM", "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("B/L번호"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호"), "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품등급"), "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("제조일자"), "11", "11", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"           , "S"},
                                    {"LOC_NM"           , "S"},
                                    {"RITEM_CD"          , "S"},
                                    {"RITEM_NM"          , "S"},
                                    
                                    {"STOCK_QTY"  , "N"},
                                    {"UOM_NM"            , "S"},
                                    {"REAL_PLT_QTY"           , "N"},
                                    {"BL_NO"         , "S"},
                                    {"WH_NM"         , "S"},
                                    
                                    {"CUST_LOT_NO"           , "S"},
                                    {"ITEM_CLASS"            , "S"},
                                    {"MAKE_DT"            , "S"}
                                   
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("포장완제품재고상세");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
}
