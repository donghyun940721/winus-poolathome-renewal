package com.logisall.winus.wmsps.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPS013Dao")
public class WMSPS013Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID  : selectPoolGrp
     * Method 설명  : 화면내 필요한 용기군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectPoolGrp(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 현재고 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsps011.listPool", model);
    }   
    /**
     * Method ID  : listSub 
     * Method 설명  : 현재고 상세조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listSub(Map<String, Object> model){
        return executeQueryPageWq("wmsps011.listSubPool", model);
    }
    
    /**
     * Method ID  : update
     * Method 설명  : 현재고 비고수정
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model){
        return executeUpdate("wmsps011.updatePool", model);
    }
}
