package com.logisall.winus.wmsrp.web;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.wmsrp.service.WMSRP080Service;
import com.m2m.jdfw5x.document.DocumentView;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSRP080Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSRP080Service")
	private WMSRP080Service service;

	/*-
	 * Method ID   : pdfView
	 * Method 설명    : 거래명세서PDF view
	 * 작성자               : chsong
	 * @param   model
	 * @return
	 */
	@RequestMapping("/WMSRP080/pdfView.action")
	public void pdfView(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			// log.info("[ model ] :" + model);

			String pathDirectory = request.getParameter("pdfFilePath");
			// String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
			String pdfFilePath = (new StringBuffer()
					.append(ConstantIF.SERVER_PDF_PATH)
					.append(CommonUtil.convertSafePath(pathDirectory))).toString();
			String pdfFileName = request.getParameter("pdfFileName");

			// log.info("[ pdfFilePath ] :" + pdfFilePath);
			// log.info("[ pdfFileName ] :" + pdfFileName);

			DocumentView.getPDFView(request, response, new File(pdfFilePath, pdfFileName));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to view pdf :", e);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/TMSYS999Err.action");
			rd.forward(request, response);
		}
	}

	/*-
	 * Method ID    : wmsrp080
	 * Method 설명      : 거래명세서
	 * 작성자                 : chSong 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP080.action")
	public ModelAndView wmsrp080(Map<String, Object> model) {
		return new ModelAndView("winus/wmsrp/WMSRP080Q1");
	}

	/*-
	 * Method ID    : search
	 * Method 설명      : 거래명세서 발행 pdf생성
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP080/search.action")
	public ModelAndView search(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "DealStatement";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();

		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);

			m = service.getResult(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);

			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search result :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSRP080/search_OLD.action")
	public ModelAndView search_OLD(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "DealStatement";

		// 현재날짜를 문서에 붙인다
		// 추후 문서 제목구성에따라 수정할것!!
		// 현재는 임시로!!
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
		String dateName = dayTime.format(new Date(time));

		// PDF 파일이름
		// String fileName =
		// ConstantIF.serverPdfPath+"sample/"+langName+dateName+"_sample.pdf";
		// String fileName = langName+dateName+"_sample";
		String fileName = langName + "_" + dateName;

		model.put("langName", langName);
		model.put("dateName", dateName);

		try {
			m = service.getResult(model);

			m.put("fileName", fileName);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search result :", e);
			}
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

}
