package com.logisall.winus.wmsrp.web;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.CsvWriter;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.wmsrp.service.WMSRP020Service;
import com.m2m.jdfw5x.document.DocumentView;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSRP020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSRP020Service")
	private WMSRP020Service service;

	/*-
	 * Method ID   : pdfView
	 * Method 설명    : 입하작업지시서PDF view
	 * 작성자               : chsong
	 * @param   model
	 * @return
	 */
	@RequestMapping("/WMSRP020/pdfView.action")
	public void pdfView(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			String pathDirectory = request.getParameter("pdfFilePath");
			// String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
			String pdfFilePath = (new StringBuffer()
					.append(ConstantIF.SERVER_PDF_PATH)
					.append(CommonUtil.convertSafePath(pathDirectory))).toString();
			String pdfFileName = request.getParameter("pdfFileName");

			// log.info("[ pdfFilePath ] :" + pdfFilePath);
			// log.info("[ pdfFileName ] :" + pdfFileName);

			DocumentView.getPDFView(request, response, new File(pdfFilePath, pdfFileName));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pdf view :", e);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/TMSYS999Err.action");
			rd.forward(request, response);
		}
	}

	/*-
	 * Method ID    : wmsrp020
	 * Method 설명      : 입하작업지시서
	 * 작성자                 : chSong 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP020.action")
	public ModelAndView wmsrp020(Map<String, Object> model) {
		return new ModelAndView("winus/wmsrp/WMSRP020Q1");
	}

	/*-
	 * Method ID    : grnSearch
	 * Method 설명      : 작업지시서(입하) 프로시져및 pdf생성
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP020/grnSearch.action")
	public ModelAndView grnSearch(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsIn";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();

		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);

			m = service.updateGrn(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);

			/*
			 * InputStream inputStream = new FileInputStream(fileToDownload); //
			 * response.setContentType("application/force-download");
			 * response.setHeader("Content-Disposition", "attachment; filename="
			 * +pdfFileName); IOUtils.copy(inputStream,
			 * response.getOutputStream()); response.flushBuffer();
			 * inputStream.close();
			 */

			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search result :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSRP020/grnSearch_OLD.action")
	public ModelAndView grnSearch_OLD(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsIn";

		// 현재날짜를 문서에 붙인다
		// 추후 문서 제목구성에따라 수정할것!!
		// 현재는 임시로!!
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
		String dateName = dayTime.format(new Date(time));

		// PDF 파일이름
		// String fileName =
		// ConstantIF.serverPdfPath+"sample/"+langName+dateName+"_sample.pdf";

		// String fileName = langName+dateName+"_sample";
		String fileName = langName + "_" + dateName;

		model.put("langName", langName);
		model.put("dateName", dateName);

		try {
			m = service.updateGrn(model);

			m.put("fileName", fileName);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search result :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listCsv
	 * Method 설명      : Csv다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSRP020/csv.action")
	public void listCsv(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listCsv(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doCsvDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doCsvDown
	 * Method 설명 : CSV 다운로드 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doCsvDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			String[] header = {"SYS_DT", "USER_NM", "CUST_NM"};
			String[] detail = {"ORD_ID", "RITEM_NM", "IN_ORD_QTY", "IN_ORD_UOM_ID", "IN_ORD_WEIGHT", "IN_WH_NM", "LOC_CD"};

			// 파일명
			String fileName = MessageResolver.getText("입하작업지시서");
			CsvWriter wr = new CsvWriter();
			wr.downCsv(ConstantIF.FILE_ATTACH_PATH, grs, header, detail, fileName, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : grnSearch
	 * Method 설명      : 작업지시서(입고) 프로시져및 pdf생성
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP020/grnSearch2.action")
	public ModelAndView grnSearch2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsIn2";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();

		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);

			m = service.updateGrn2(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);

			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get pdf info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSRP020/grnSearch2_OLD.action")
	public ModelAndView grnSearch2_OLD(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsEntrada";

		// 현재날짜를 문서에 붙인다
		// 추후 문서 제목구성에따라 수정할것!!
		// 현재는 임시로!!
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
		String dateName = dayTime.format(new Date(time));

		// PDF 파일이름
		// String fileName =
		// ConstantIF.serverPdfPath+"sample/"+langName+dateName+"_sample.pdf";
		// String fileName = langName+dateName+"_sample";
		String fileName = langName + "_" + dateName;

		model.put("langName", langName);
		model.put("dateName", dateName);

		try {
			m = service.updateGrn2(model);

			m.put("fileName", fileName);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get pdf info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
