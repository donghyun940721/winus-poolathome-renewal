package com.logisall.winus.wmsrp.web;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.wmsrp.service.WMSRP030Service;
import com.m2m.jdfw5x.document.DocumentView;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSRP030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSRP030Service")
	private WMSRP030Service service;

	/*-
	 * Method ID   : pdfView
	 * Method 설명    : 피킹리스트발행 PDF view
	 * 작성자               : chsong
	 * @param   model
	 * @return
	 */
	@RequestMapping("/WMSRP030/pdfView.action")
	public void pdfView(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		try {
			String pathDirectory = request.getParameter("pdfFilePath");
			// String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
			String pdfFilePath = (new StringBuffer()
					.append(ConstantIF.SERVER_PDF_PATH)
					.append(CommonUtil.convertSafePath(pathDirectory))).toString();
			String pdfFileName = request.getParameter("pdfFileName");

			// log.info("[ pdfFilePath ] :" + pdfFilePath);
			// log.info("[ pdfFileName ] :" + pdfFileName);

			DocumentView.getPDFView(request, response, new File(pdfFilePath, pdfFileName));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pdf view :", e);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/TMSYS999Err.action");
			rd.forward(request, response);
		}
	}

	/*-
	 * Method ID    : wmsrp030
	 * Method 설명      : 피킹리스트발행 (피킹)
	 * 작성자                 : chSong 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP030.action")
	public ModelAndView wmsrp030(Map<String, Object> model) {
		return new ModelAndView("winus/wmsrp/WMSRP030Q1");
	}

	/*-
	 * Method ID    : pickingSearch
	 * Method 설명      : 피킹리스트발행 프로시져및 pdf생성
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP030/pickingSearch.action")
	public ModelAndView pickingSearch(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsPicking";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();

		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);

			m = service.updatePicking(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);
			
			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    		: pickinglSearchProcedure
	 * Method 설명      	: 피킹리스트발행 프로시져
	 * 작성자               : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP030/pickingSearchProcedure.action")
	public ModelAndView pickinglSearchProcedure(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.updatePickingProcedure(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    		 : pickingTotalSearchProcedure
	 * Method 설명      	 : 토탈 피킹리스트발행 프로시져
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP030/pickingTotalSearchProcedure.action")
	public ModelAndView pickingTotalSearchProcedure(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.updatePickingTotalProcedure(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : pickingTotalSearch
	 * Method 설명      : 토탈 피킹리스트발행 프로시져및 pdf생성
	 * 작성자                 : MonkeySeok
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP030/pickingTotalSearch.action")
	public ModelAndView pickingTotalSearch(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsPicking";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();

		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);

			m = service.updatePickingTotal(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);
			
			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : pickingTotalSearch
	 * Method 설명      : 토탈 피킹리스트발행 프로시져및 pdf생성
	 * 작성자                 : MonkeySeok
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP030/pickingTotalSearchV2.action")
	public ModelAndView pickingTotalSearchV2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsPicking";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();

		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);

			m = service.updatePickingTotalV2(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);
			
			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSRP030/pickingSearch_OLD.action")
	public ModelAndView pickingSearch_OLD(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsPickin";

		// 현재날짜를 문서에 붙인다
		// 추후 문서 제목구성에따라 수정할것!!
		// 현재는 임시로!!
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
		String dateName = dayTime.format(new Date(time));

		// PDF 파일이름
		// String fileName =
		// ConstantIF.serverPdfPath+"sample/"+langName+dateName+"_sample.pdf";
		// String fileName = langName+dateName+"_sample";
		String fileName = langName + "_" + dateName;

		model.put("langName", langName);
		model.put("dateName", dateName);

		try {
			m = service.updatePicking(model);

			m.put("fileName", fileName);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : ordInitListSearch
	 * Method 설명      : 주문작업명세서 pdf생성
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP030/ordInitListSearch.action")
	public ModelAndView ordInitListSearch(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String langName = "OrderInitList";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();
		
		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);
			
			m = service.ordInitListSearch(model);
			
			// log.info("[ pdfFileName ] :" + pdfFileName);
			
			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : pickingSearch
	 * Method 설명      : 피킹리스트발행 프로시져및 pdf생성
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP030/pickingSearch_godo.action")
	public ModelAndView pickingSearch_godo(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "WorkDirectionsPicking";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();

		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);

			m = service.updatePickingGodo(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);
			
			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSRP030Q2
	 * Method 설명      : 피킹리스트 (바코드 미출력) 발행 - 오즈
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSRP030Q2.action")
	public ModelAndView WMSRP030Q2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsrp/WMSRP030Q2");
	}
	
	/*-
	 * Method ID    : WMSRP030Q3
	 * Method 설명      : 토탈 피킹리스트 (바코드 미출력) 발행 - 오즈
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSRP030Q3.action")
	public ModelAndView WMSRP030Q3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsrp/WMSRP030Q3");
	}
	
	/*-
	 * Method ID    : WMSRP030Q4
	 * Method 설명      : 피킹리스트 (바코드 미출력) 발행 - 오즈 X,  오직 HTML
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSRP030Q4.action")
	public ModelAndView WMSRP030Q4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsrp/WMSRP030Q4");
	}
	
	/*-
	 * Method ID         : pickingSearchHtml
	 * Method 설명      : 피킹리스트 (바코드 미출력) 발행 - 오즈 X,  오직 HTML 출력
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSRP030/pickingSearchHtml.action")
	public ModelAndView pickingSearchHtml(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.updatePickingHtml(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
