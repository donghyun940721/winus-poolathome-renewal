package com.logisall.winus.wmsrp.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsrp.service.WMSRP070Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSRP070Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSRP070Service")
	private WMSRP070Service service;

	/*-
	 * Method ID    : wmsrp070
	 * Method 설명      : 바코드발행
	 * 작성자                 : chSong 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP070.action")
	public ModelAndView wmsrp070(Map<String, Object> model) {
		return new ModelAndView("winus/wmsrp/WMSRP070Q1");
	}

	/*-
	 * Method ID    : newBarcodePa
	 * Method 설명      : 바코드 생성 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP070/newBarcodePa.action")
	public ModelAndView newBarcodePa(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateBarcodePa(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get pdf info :", e);
			}
			m.put("errCnt", -1);
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
