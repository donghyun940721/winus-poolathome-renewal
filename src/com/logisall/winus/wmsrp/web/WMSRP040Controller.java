package com.logisall.winus.wmsrp.web;

import java.io.File;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.wmsrp.service.WMSRP040Service;
import com.m2m.jdfw5x.document.DocumentView;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSRP040Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSRP040Service")
	private WMSRP040Service service;

	/*-
	 * Method ID   : pdfView
	 * Method 설명    : 상차리스트PDF view
	 * 작성자               : chsong
	 * @param   model
	 * @return
	 */
	@RequestMapping("/WMSRP040/pdfView.action")
	public void pdfView(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		try {
			// log.info("[ model ] :" + model);

			String pathDirectory = request.getParameter("pdfFilePath");
			// String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
			String pdfFilePath = (new StringBuffer()
					.append(ConstantIF.SERVER_PDF_PATH)
					.append(CommonUtil.convertSafePath(pathDirectory))).toString();
			String pdfFileName = request.getParameter("pdfFileName");

			// log.info("[ pdfFilePath ] :" + pdfFilePath);
			// log.info("[ pdfFileName ] :" + pdfFileName);

			DocumentView.getPDFView(request, response, new File(pdfFilePath, pdfFileName));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pdf view :", e);
			}
			RequestDispatcher rd = request.getRequestDispatcher("/TMSYS999Err.action");
			rd.forward(request, response);
		}
	}

	/*-
	 * Method ID    : wmsrp040
	 * Method 설명      : 상차리스트
	 * 작성자                 : chSong 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP040.action")
	public ModelAndView wmsrp040(Map<String, Object> model) {
		return new ModelAndView("winus/wmsrp/WMSRP040Q1");
	}

	/*-
	 * Method ID    : outSearch
	 * Method 설명      : 상차리스트발행 프로시져및 pdf생성
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSRP040/outSearch.action")
	public ModelAndView outSearch(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "PutUpList";
		String pathDirectory = DateUtil.getDateByPattern2();
		String pdfFilePath = (new StringBuffer().append(ConstantIF.SERVER_PDF_PATH).append(pathDirectory)).toString();
		String pdfFileName = (new StringBuffer().append(langName).append("_").append(UUID.randomUUID().toString()).append(".pdf")).toString();

		try {
			model.put("pdfFilePath", pdfFilePath);
			model.put("pdfFileName", pdfFileName);

			m = service.updateOut(model);
			// log.info("[ pdfFileName ] :" + pdfFileName);

			m.put("pdfFilePath", pathDirectory);
			m.put("pdfFileName", pdfFileName);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/WMSRP040/outSearch_OLD.action")
	public ModelAndView outSearch_OLD(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		String langName = "PutUpList";

		// 현재날짜를 문서에 붙인다
		// 추후 문서 제목구성에따라 수정할것!!
		// 현재는 임시로!!
		long time = System.currentTimeMillis();
		SimpleDateFormat dayTime = new SimpleDateFormat("yyyyMMdd");
		String dateName = dayTime.format(new Date(time));

		// PDF 파일이름
		// String fileName =
		// ConstantIF.serverPdfPath+"sample/"+langName+dateName+"_sample.pdf";
		// String fileName = langName+dateName+"_sample";
		String fileName = langName + "_" + dateName;

		model.put("langName", langName);
		model.put("dateName", dateName);

		try {
			m = service.updateOut(model);

			m.put("fileName", fileName);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get  info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSRP040oz
	 * Method 설명      : 상차리스트 발행 OZ report 
	 * 작성자                 : Seongjun Kwon
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSRP040oz.action")
	public ModelAndView WMSRP040oz(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsrp/WMSRP040oz");
	}
	
	/*
	 * Method ID    : updatePutup
	 * Method 설명      : 상차리스트 발행 프로시저  
	 * 작성자                 : Seongjun Kwon
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSRP040oz/updatePutup.action")
	public ModelAndView updatePutup(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("winus/wmsop/WMSRP040oz");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.updateOutOz(model);
		} catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to OZ report :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		
		mav.addAllObjects(m);
		
		return mav;
	}
	

}
