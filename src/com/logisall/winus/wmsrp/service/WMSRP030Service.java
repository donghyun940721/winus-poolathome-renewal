package com.logisall.winus.wmsrp.service;

import java.util.Map;



public interface WMSRP030Service {
    public Map<String, Object> updatePicking(Map<String, Object> model) throws Exception;
    public Map<String, Object> updatePickingProcedure(Map<String, Object> model) throws Exception;
    public Map<String, Object> updatePickingTotal(Map<String, Object> model) throws Exception;
    public Map<String, Object> updatePickingTotalProcedure(Map<String, Object> model) throws Exception;
    public Map<String, Object> updatePickingTotalV2(Map<String, Object> model) throws Exception;
    public Map<String, Object> ordInitListSearch(Map<String, Object> model) throws Exception;
    public Map<String, Object> updatePickingGodo(Map<String, Object> model) throws Exception;
    public Map<String, Object> updatePickingHtml(Map<String, Object> model) throws Exception;
}
