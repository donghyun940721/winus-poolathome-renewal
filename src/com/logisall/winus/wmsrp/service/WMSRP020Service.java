package com.logisall.winus.wmsrp.service;

import java.util.Map;



public interface WMSRP020Service {
    
    public Map<String, Object> updateGrn(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateGrn2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listCsv(Map<String, Object> model) throws Exception;
    
}
