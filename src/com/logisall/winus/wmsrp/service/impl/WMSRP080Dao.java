package com.logisall.winus.wmsrp.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.PDF.model.DocOrderListDetailVO;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSRP080Dao")
public class WMSRP080Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
 
    
    /**
     * Method ID    : orderList
     * Method 설명      : 거래명세서 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    @SuppressWarnings("unchecked")
    public DocOrderListDetailVO[] orderList(Map<String, Object> model) throws Exception{
        return (DocOrderListDetailVO[])getSqlMapClientTemplate().queryForList("wmsop010.orderList", model).toArray(new DocOrderListDetailVO[0]);
    }
    
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > orderListAsList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("wmsop010.orderListAsList", model);
    }    
}
