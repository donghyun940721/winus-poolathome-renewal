package com.logisall.winus.wmsrp.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.PDF.model.DocDefaultVO;
import com.logisall.winus.frm.common.PDF.model.DocOrderListDetailVO;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsrp.service.WMSRP080Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service("WMSRP080Service")
public class WMSRP080ServiceImpl extends AbstractServiceImpl implements WMSRP080Service {
    
    @Resource(name = "WMSRP080Dao")
    private WMSRP080Dao dao;
    
    private static final int NUMBER_OF_DETAIL_ROWS = 20;

    
    /**
     *  Method ID  : getResult 
     *  Method 설명  : 거래명세서발행
     *  작성자             : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getResult(Map<String, Object> model) throws Exception {
        
        Map<String, Object> m = new HashMap<String, Object>();        
        try{
            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();            
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());            
            List order = new ArrayList();            
            for(int i = 0 ; i < tmpCnt ; i++){
                Map orderMap = new HashMap<String, Object>(); 
                orderMap.put("ordId", (String)model.get("ORD_ID"+i));
                orderMap.put("ordSeq", (String)model.get("ORD_SEQ"+i));
                order.add(orderMap);
            }
            modelPdf.put("order", order);                 
            modelPdf.put("gvUserNo", (String)model.get(ConstantIF.SS_USER_NO));
            modelPdf.put("gvLcId", (String)model.get(ConstantIF.SS_SVC_NO));

	        // log.info("[ modelPdf      ] :" + modelPdf);            			
			List<Map<String, Object>> parameterList = dao.orderListAsList(modelPdf);
	    	
	    	Map<String, Object> parameters = new HashMap<String, Object>();			
			if ( parameterList != null && parameterList.size() > 0) {
				Map<String, Object> parameter = parameterList.get(0);
				
				parameters.put("GROUP12", parameter.get("GROUP12"));
				parameters.put("GROUP13", parameter.get("GROUP13"));
				parameters.put("GROUP14", parameter.get("GROUP14"));
				parameters.put("GROUP15", parameter.get("GROUP15"));
				parameters.put("GROUP16", parameter.get("GROUP16"));
				parameters.put("GROUP19", parameter.get("GROUP19"));
				
				parameters.put("GROUP30", parameter.get("GROUP30"));
				parameters.put("GROUP31", parameter.get("GROUP31"));
				parameters.put("GROUP32", parameter.get("GROUP32"));
				parameters.put("GROUP33", parameter.get("GROUP33"));
				
				int dummyRows = NUMBER_OF_DETAIL_ROWS - (parameterList.size()%NUMBER_OF_DETAIL_ROWS);
				while( dummyRows > 0) {
					parameterList.add(new HashMap<String, Object>());
					dummyRows = dummyRows - 1;
				}
			}
	    	
	    	setReportParameter(parameters);
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}    	    	
			JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_4);    	    	
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
			
			String createFile = pdfFilepath + File.separatorChar + pdfFileName;    		
			JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
	
	        m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("list.success"));
	        
	    } catch(BizException be) {
	    	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}           	
	        m.put("errCnt", 1);
	        m.put("MSG", be.getMessage() );
	        
	    }catch(Exception e){
	    	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			}
	        m.put("errCnt", 1);
	        m.put("MSG", MessageResolver.getMessage("list.error") );
	    }
	    return m;        

    }
    
    private void setReportParameter(Map<String, Object> param) {
    	if (param == null) {
    		param = new HashMap<String, Object>(); 
    	}
    	
    	param.put("SUB_TITLE", MessageResolver.getMessage("거래명세서"));
    	
    	param.put("MAIN_HEADER_LABEL_1", MessageResolver.getMessage("상호"));
    	param.put("MAIN_HEADER_LABEL_2", MessageResolver.getMessage("성명"));
    	param.put("MAIN_HEADER_LABEL_3", MessageResolver.getMessage("일자") + " : ");
    	param.put("MAIN_HEADER_LABEL_4", MessageResolver.getMessage("귀하"));
    	param.put("MAIN_HEADER_LABEL_5", MessageResolver.getMessage("배송지"));
    	param.put("MAIN_HEADER_LABEL_6", MessageResolver.getMessage("주소"));
    	param.put("MAIN_HEADER_LABEL_7", MessageResolver.getMessage("전화번호"));
    	param.put("MAIN_HEADER_LABEL_8", MessageResolver.getMessage("출력일자") + " :");
    	param.put("MAIN_HEADER_LABEL_9", MessageResolver.getMessage("출고일자") + " :");
    	
    	
    	param.put("MAIN_FOOTER_LABEL_1", MessageResolver.getMessage("공급액"));
    	param.put("MAIN_FOOTER_LABEL_2", MessageResolver.getMessage("부가가치세"));
    	param.put("MAIN_FOOTER_LABEL_3", MessageResolver.getMessage("합계"));
    	param.put("MAIN_FOOTER_LABEL_4", MessageResolver.getMessage("배송업체"));
    	param.put("MAIN_FOOTER_LABEL_5", MessageResolver.getMessage("상호"));
    	param.put("MAIN_FOOTER_LABEL_6", MessageResolver.getMessage("대표"));
    	param.put("MAIN_FOOTER_LABEL_7", MessageResolver.getMessage("주소"));
    	param.put("MAIN_FOOTER_LABEL_8", MessageResolver.getMessage("전화번호"));
    	param.put("MAIN_FOOTER_LABEL_9", MessageResolver.getMessage("팩스번호"));
    	param.put("MAIN_FOOTER_LABEL_10", MessageResolver.getMessage("담당자"));
    	param.put("MAIN_FOOTER_LABEL_11", MessageResolver.getMessage("인수자확인"));
    	
    	param.put("MAIN_LIST_COLUMN_NAME_1", MessageResolver.getMessage("No."));
    	param.put("MAIN_LIST_COLUMN_NAME_2", MessageResolver.getMessage("품목명"));
    	param.put("MAIN_LIST_COLUMN_NAME_3", MessageResolver.getMessage("품목코드"));
    	param.put("MAIN_LIST_COLUMN_NAME_4", MessageResolver.getMessage("수량"));
    	param.put("MAIN_LIST_COLUMN_NAME_5", MessageResolver.getMessage("출고량"));
    	param.put("MAIN_LIST_COLUMN_NAME_6", MessageResolver.getMessage("단가"));
    	param.put("MAIN_LIST_COLUMN_NAME_7", MessageResolver.getMessage("금액"));
    	param.put("MAIN_LIST_COLUMN_NAME_8", MessageResolver.getMessage("비고"));
    	param.put("MAIN_LIST_COLUMN_NAME_9", MessageResolver.getMessage("단위"));

    }     

/*
    public Map<String, Object> getResult_OLD(Map<String, Object> model) throws Exception {
        
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt    = 1;
        String errMsg = MessageResolver.getMessage("list.error");
        
        try{
            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();
            
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            List order = new ArrayList();
            
            for(int i = 0 ; i < tmpCnt ; i++){
                Map orderMap = new HashMap<String, Object>(); 
                orderMap.put("ordId", (String)model.get("ORD_ID"+i));
                orderMap.put("ordSeq", (String)model.get("ORD_SEQ"+i));
                order.add(orderMap);
            }
            modelPdf.put("order", order);     
            
            modelPdf.put("gvUserNo", (String)model.get(ConstantIF.SS_USER_NO));
            //orderList
            DocDefaultVO h = new DocDefaultVO();
            DocOrderListDetailVO[] d = dao.orderList(modelPdf);       
            h.setFontPath(ConstantIF.FONT_NORMAL);
            h.setTitleFontPath(ConstantIF.FONT_TITLE);
            h.setServerPath(ConstantIF.SERVER_PDF_PATH);
            
            // h.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
            h.setFileName(model.get("langName") + "_" + model.get("dateName"));

            // PDFCreate pdf = new PDFCreate();
            // modelPdf = pdf.createOrderListPdf(h, d);
            
            errCnt = Integer.parseInt(modelPdf.get("ERRCNT").toString());
            m.put("errCnt", errCnt);

        }catch(Exception e){
            throw e;
        }
        return m;
    }    
*/
}
