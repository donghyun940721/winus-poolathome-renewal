package com.logisall.winus.wmsrp.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.PDF.model.DocDefaultVO;
import com.logisall.winus.frm.common.PDF.model.DocRptGRNWorkOrderListDetailVO;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsrp.service.WMSRP040Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service("WMSRP040Service")
public class WMSRP040ServiceImpl extends AbstractServiceImpl implements WMSRP040Service {
    
    @Resource(name = "WMSRP040Dao")
    private WMSRP040Dao dao;
    
    /**
     *  Method ID  : outSearch 
     *  Method 설명  : 상차리스트발행
     *  작성자             : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateOut(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();        
        try{
            // int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("ordId", (String)model.get("ORD_ID0"));
            modelIns.put("ordSeq", (String)model.get("ORD_SEQ0"));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            modelIns = (Map<String, Object>)dao.updateOut(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();               
            
            //한건만 출력하기에 우선 첫번째 주문번호로 했음
            modelPdf.put("vrOrdId"      ,  (String)model.get("ORD_ID0"));
            modelPdf.put("USER_NAME"    , (String)model.get(ConstantIF.SS_USER_NAME));
            
    		Map<String, Object> parameters = new HashMap<String, Object>();    		
    		List<Map<String, Object>> parameterList = dao.outSearchDetailList(modelPdf);

    		if ( parameterList != null && parameterList.size() > 1) {
				Map<String, Object> param = parameterList.get(0);
				
				parameters.put("SYS_DT", param.get("SYS_DT"));
				parameters.put("CAR_CD", param.get("CAR_CD"));
				parameters.put("USER_NM", param.get("USER_NM"));
				parameters.put("CUST_NM", param.get("CUST_NM"));
				parameters.put("RITEM_NM", param.get("RITEM_NM"));
    		}
	    	
	    	setReportParameter(parameters);
	    	
    		parameters.put("subReportDataSource", parameterList);
    		parameters.put("SUBREPORT_DIR", ConstantIF.REPORT_FILE_PATH);
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}    	    	
    		JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_5);    	    	
    		JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);

    		String createFile = pdfFilepath + File.separatorChar + pdfFileName;    		
    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
		    		
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    private void setReportParameter(Map<String, Object> param) {
    	if (param == null) {
    		param = new HashMap<String, Object>(); 
    	}
    	
    	param.put("SUB_TITLE_1", MessageResolver.getMessage("상차리스트(물류센터용)"));
    	param.put("SUB_TITLE_2", MessageResolver.getMessage("상차리스트(화주용)"));
    	param.put("SUB_TITLE_3", MessageResolver.getMessage("상차리스트(화주용)"));
    	
    	param.put("MAIN_HEADER_LABEL_1", MessageResolver.getMessage("발행일자"));
    	param.put("MAIN_HEADER_LABEL_2", MessageResolver.getMessage("발행자"));
    	param.put("MAIN_HEADER_LABEL_3", MessageResolver.getMessage("화주"));
    	param.put("MAIN_HEADER_LABEL_4", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_HEADER_LABEL_5", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_HEADER_LABEL_6", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_FOOTER_LABEL_1", MessageResolver.getMessage("비고") + " : ");
    	
    	param.put("MAIN_LIST_COLUMN_NAME_1", MessageResolver.getMessage("No."));
    	param.put("MAIN_LIST_COLUMN_NAME_2", MessageResolver.getMessage("주문번호"));
    	param.put("MAIN_LIST_COLUMN_NAME_3", MessageResolver.getMessage("제품"));
    	param.put("MAIN_LIST_COLUMN_NAME_4", MessageResolver.getMessage("수량"));
    	param.put("MAIN_LIST_COLUMN_NAME_5", MessageResolver.getMessage("UOM"));
    	param.put("MAIN_LIST_COLUMN_NAME_6", MessageResolver.getMessage("중량"));
    	param.put("MAIN_LIST_COLUMN_NAME_7", MessageResolver.getMessage("창고"));
    	param.put("MAIN_LIST_COLUMN_NAME_8", MessageResolver.getMessage("로케이션"));
    }    
    
    /**
     *  Method ID  : outSearch 
     *  Method 설명  : 상차리스트발행
     *  작성자             : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateOutOz(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();        
        try{
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            String vrOrdId = (String)model.get("vrOrdId");
            String [] vrOrdIds = vrOrdId.split(",");
            
            String vrOrdSeq = (String)model.get("vrOrdSeq");
            String [] vrOrdSeqs = vrOrdSeq.split(",");
            
            modelIns.put("ordId", vrOrdIds);
            modelIns.put("ordSeq", vrOrdSeqs);
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>) dao.updateOut(modelIns); // 기존 프로시저
            
            ServiceUtil.isValidReturnCode("WMSRP040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        
        return m;
    }
    
    /*
    public Map<String, Object> updateOut_OLD(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt    = 1;
        String errMsg = MessageResolver.getMessage("save.error");
        
        try{
            // int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("ordId", (String)model.get("ORD_ID0"));
            modelIns.put("ordSeq", (String)model.get("ORD_SEQ0"));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            // dao
            modelIns = (Map<String, Object>)dao.updateOut(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();               
            
            //한건만 출력하기에 우선 첫번째 주문번호로 했음
            modelPdf.put("vrOrdId"      ,  (String)model.get("ORD_ID0"));
            modelPdf.put("USER_NAME"    , (String)model.get(ConstantIF.SS_USER_NAME));
            
            ///////////pdf생성 시작 2015-07-28 chsong////////////////////
            DocDefaultVO h = new DocDefaultVO();
            DocRptGRNWorkOrderListDetailVO[] d = dao.outSearchDetail(modelPdf);
            
//                for(int i = 0 ; i < d.length ; i ++){
//                    System.out.println(d[i].getRitemNm());
//                }
//                System.out.println("파일-----------------------------");
            
            h.setFontPath(ConstantIF.FONT_NORMAL);
            h.setTitleFontPath(ConstantIF.FONT_TITLE);
            h.setServerPath(ConstantIF.SERVER_PDF_PATH);
            
            // h.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
            h.setFileName(model.get("langName") + "_" + model.get("dateName"));

            // PDFCreate pdf = new PDFCreate();
            // modelPdf = pdf.createTallyPdf(h, d);
            
            errCnt = Integer.parseInt(modelPdf.get("ERRCNT").toString());                

            if (errCnt > 0) {
            	m.put("errCnt", errCnt);
            	m.put("MSG", errMsg);
            	 
            } else {
            	m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("list.success"));
                
            }
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );            
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }    
    */
    
}
