package com.logisall.winus.wmsrp.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.PDF.model.DocPickingListDetailVO;
import com.logisall.winus.frm.common.PDF.model.DocPickingListHeaderVO;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSRP030Dao")
public class WMSRP030Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    

	
    /**
     * Method ID    : updatePicking
     * Method 설명      : 피킹리스트발행
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object updatePicking(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_print_picking", model);
        return model;
    }

    /**
     * Method ID    : updatePickingTotal
     * Method 설명      : 토탈피킹리스트발행
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object updatePickingTotal(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_total_print_picking", model);
        return model;
    }
    /**
     * Method ID    : selectPickingHeader
     * Method 설명      : 피킹리스트발행 헤더
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Map<String, Object> selectPickingHeaderMap(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsop011.selectPickingHeaderMap", model);
    }
    
    public DocPickingListHeaderVO selectPickingHeader(Map<String, Object> model) throws Exception{
        return (DocPickingListHeaderVO)getSqlMapClientTemplate().queryForObject("wmsop011.selectPickingHeader", model);
    }    
    

    /**
     * Method ID    : selectPickingHeader
     * Method 설명      : 피킹리스트발행 헤더
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Map<String, Object> selectTotalPickingHeaderMap(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsop011.selectPickingHeaderMap", model);
    }
    public Map<String, Object> selectTotalPickingHeaderMapV2(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsop011.selectPickingHeaderMapV2", model);
    }
    
    public DocPickingListHeaderVO selectTotalPickingHeader(Map<String, Object> model) throws Exception{
        return (DocPickingListHeaderVO)getSqlMapClientTemplate().queryForObject("wmsop011.selectPickingHeader", model);
    }    
    
    /**
     * Method ID    : selectPickingDetail
     * Method 설명      : 피킹리스트발행 상세
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > selectPickingDetailList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsop011.selectPickingDetailList_menu110", model);
    }
    
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > selectTotalPickingDetailList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsop011.selectPickingDetailList_total", model);
    }
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > selectTotalPickingDetailListV2(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsop011.selectPickingDetailList_totalV2", model);
    }
    
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > selectPickingDetailListIn(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsop011.selectPickingDetailList", model);
    }
    
    @SuppressWarnings("unchecked")
    public DocPickingListDetailVO[] selectPickingDetail(Map<String, Object> model) throws Exception{
        return (DocPickingListDetailVO[])getSqlMapClientTemplate().queryForList("wmsop011.selectPickingDetail", model).toArray(new DocPickingListDetailVO[0]);
    }    
    
    /**
     * Method ID    		 : selectPickingDetailListHtml
     * Method 설명      	 : 바코드 미출력 - 피킹리스트발행(간편출고) 헤더
     * 작성자                 : KSJ
     * @param   model
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > selectPickingDetailListHtml(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsop011.selectPickingDetailListHtml", model);
    }
 
    /**
     * Method ID    : selectOrdListHeaderMap
     * Method 설명      : 주문작업리스트발행 헤더
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Map<String, Object> selectOrdListHeaderMap(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsop011.selectOrdListHeaderMap", model);
    }
	
	/**
     * Method ID    : selectOrdListDetailList
     * Method 설명      : 주문작업리스트발행 상세
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > selectOrdListDetailList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsop011.selectOrdListDetailList_menu54", model);
    }
}
