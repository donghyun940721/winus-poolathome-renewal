package com.logisall.winus.wmsrp.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.PDF.model.DocPickingListDetailVO;
import com.logisall.winus.frm.common.PDF.model.DocPickingListHeaderVO;
import com.logisall.winus.frm.common.PDF.model.DocRptGRNWorkOrderListDetailVO;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsrp.service.WMSRP020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRFileVirtualizer;

@Service("WMSRP020Service")
public class WMSRP020ServiceImpl extends AbstractServiceImpl implements WMSRP020Service {
    
    @Resource(name = "WMSRP020Dao")
    private WMSRP020Dao dao;

    @Resource(name = "WMSRP030Dao")
    private WMSRP030Dao dao2;
    
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     *  Method ID  : grnSearch 
     *  Method 설명  : 작업지시서(입하)발행
     *  작성자             : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateGrn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            Map<String, Object> modelIns = new HashMap<String, Object>();
//          int tmpCnt = Integer.parseInt(model.get("selectIds").toString());            
            //왜 한건만 하는지 모르겠습니다.
//            String[] ordId = new String[tmpCnt];
//            String[] ordSeq = new String[tmpCnt];

//            for (int i = 0; i < tmpCnt; i++) {
//                ordId[i] = (String)model.get("ORD_ID" + i);
//                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
//            }
//            modelIns.put("ordId", ordId);
//            modelIns.put("ordSeq", ordSeq);
            modelIns.put("ordId", (String)model.get("ORD_ID0"));
            modelIns.put("ordSeq", (String)model.get("ORD_SEQ0"));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            // dao
            modelIns = (Map<String, Object>)dao.updateGrn(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
          
            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();               
            
            //한건만 출력하기에 우선 첫번째 주문번호로 했음
            modelPdf.put("vrSrchOrderPhase" ,  (String)model.get("vrSrchOrderPhase"));
            modelPdf.put("vrSrchOrderId"    ,  (String)model.get("vrSrchOrderId"));
            modelPdf.put("vrSrchReqDtFrom"  ,  (String)model.get("vrSrchReqDtFrom"));
            modelPdf.put("vrSrchReqDtTo"    ,  (String)model.get("vrSrchReqDtTo"));
            modelPdf.put("vrSrchCustCd"     ,  (String)model.get("vrSrchCustCd"));
            modelPdf.put("vrSrchCustNm"     ,  (String)model.get("vrSrchCustNm"));
            modelPdf.put("vrSrchRitemCd"    ,  (String)model.get("vrSrchRitemCd"));
            modelPdf.put("vrSrchRitemNm"    ,  (String)model.get("vrSrchRitemNm"));
            modelPdf.put("vrSrchWhNm"       ,  (String)model.get("vrSrchWhNm"));
            modelPdf.put("vrSrchOrdSubtype" ,  (String)model.get("vrSrchOrdSubtype"));
            modelPdf.put("vrOrdId"          ,  (String)model.get("ORD_ID0"));            
            modelPdf.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
            modelPdf.put("USER_NAME"        , (String)model.get(ConstantIF.SS_USER_NAME));
            
    		Map<String, Object> parameters = dao.selectGrnHeaderMap(modelPdf);
    		List<Map<String, Object>> parameterList = dao.selectGrnDetailList(modelPdf);
	    	
	    	setReportParameterType1(parameters);
    		parameters.put("subReportDataSource", parameterList);
    		parameters.put("SUBREPORT_DIR", ConstantIF.REPORT_FILE_PATH);
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}  
			
			String createFile = pdfFilepath + File.separatorChar + pdfFileName; 
			// JRFileVirtualizer virtualizer = new JRFileVirtualizer(2, pdfFilepath);
			// parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);
			
    		JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_1); 
    		
    		// JasperFillManager.fillReport(report, parameters, src);
  
    		JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
    		
    		// virtualizer.cleanup();
    		// virtualizer = null;
		    		
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", e.getMessage() );
        }
        return m;
    }
    
    private void setReportParameterType1(Map<String, Object> param) {
    	if (param == null) {
    		param = new HashMap<String, Object>(); 
    	}
    	
    	param.put("SUB_TITLE_1", MessageResolver.getMessage("입하작업지시서(물류센터용)"));
    	param.put("SUB_TITLE_2", MessageResolver.getMessage("입하작업지시서(화주용)"));
    	param.put("SUB_TITLE_3", MessageResolver.getMessage("입하작업지시서(화주용)"));
    	
    	param.put("MAIN_HEADER_LABEL_1", MessageResolver.getMessage("발행일자"));
    	param.put("MAIN_HEADER_LABEL_2", MessageResolver.getMessage("발행자"));
    	param.put("MAIN_HEADER_LABEL_3", MessageResolver.getMessage("화주"));
    	param.put("MAIN_HEADER_LABEL_4", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_FOOTER_LABEL_1", MessageResolver.getMessage("비고") + " : ");
    	
    	param.put("MAIN_LIST_COLUMN_NAME_1", MessageResolver.getMessage("No."));
    	param.put("MAIN_LIST_COLUMN_NAME_2", MessageResolver.getMessage("주문번호"));
    	param.put("MAIN_LIST_COLUMN_NAME_3", MessageResolver.getMessage("제품"));
    	param.put("MAIN_LIST_COLUMN_NAME_4", MessageResolver.getMessage("수량"));
    	param.put("MAIN_LIST_COLUMN_NAME_5", MessageResolver.getMessage("UOM"));
    	param.put("MAIN_LIST_COLUMN_NAME_6", MessageResolver.getMessage("중량"));
    	param.put("MAIN_LIST_COLUMN_NAME_7", MessageResolver.getMessage("창고"));
    	param.put("MAIN_LIST_COLUMN_NAME_8", MessageResolver.getMessage("로케이션"));
    }
    
    private void setReportParameterType2(Map<String, Object> param) {
    	if (param == null) {
    		param = new HashMap<String, Object>(); 
    	}
    	
    	param.put("SUB_TITLE", MessageResolver.getMessage("작업지시서"));
    	
    	param.put("MAIN_HEADER_LABEL_1", MessageResolver.getMessage("발행일자"));
    	param.put("MAIN_HEADER_LABEL_2", MessageResolver.getMessage("발행자"));
    	param.put("MAIN_HEADER_LABEL_3", MessageResolver.getMessage("화주"));
    	param.put("MAIN_HEADER_LABEL_4", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_FOOTER_LABEL_1", MessageResolver.getMessage("비고") + " : ");
    	
    	param.put("MAIN_LIST_COLUMN_NAME_1", MessageResolver.getMessage("No."));
    	param.put("MAIN_LIST_COLUMN_NAME_2", MessageResolver.getMessage("주문번호"));
    	param.put("MAIN_LIST_COLUMN_NAME_3", MessageResolver.getMessage("제품"));
    	param.put("MAIN_LIST_COLUMN_NAME_4", MessageResolver.getMessage("수량"));
    	param.put("MAIN_LIST_COLUMN_NAME_5", MessageResolver.getMessage("UOM"));
    	param.put("MAIN_LIST_COLUMN_NAME_6", MessageResolver.getMessage("중량"));
    	param.put("MAIN_LIST_COLUMN_NAME_7", MessageResolver.getMessage("창고"));
    	param.put("MAIN_LIST_COLUMN_NAME_8", MessageResolver.getMessage("로케이션"));
    	param.put("MAIN_LIST_COLUMN_NAME_9", MessageResolver.getMessage("LOT번호"));
    }    
        
    /*
    public Map<String, Object> updateGrn_OLD(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt    = 1;
        boolean isProcess = false;
        String errMsg = MessageResolver.getMessage("save.error");
        
        try{

//            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            //왜 한건만 하는지 모르겠습니다.
//            String[] ordId = new String[tmpCnt];
//            String[] ordSeq = new String[tmpCnt];

//            for (int i = 0; i < tmpCnt; i++) {
//                ordId[i] = (String)model.get("ORD_ID" + i);
//                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
//            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

//            modelIns.put("ordId", ordId);
//            modelIns.put("ordSeq", ordSeq);
            modelIns.put("ordId", (String)model.get("ORD_ID0"));
            modelIns.put("ordSeq", (String)model.get("ORD_SEQ0"));
            
            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updateGrn(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            // if(isProcess){           
                //조회
                Map<String, Object> modelPdf = new HashMap<String, Object>();               
                
                //한건만 출력하기에 우선 첫번째 주문번호로 했음
                modelPdf.put("vrSrchOrderPhase" ,  (String)model.get("vrSrchOrderPhase"));
                modelPdf.put("vrSrchOrderId"    ,  (String)model.get("vrSrchOrderId"));
                modelPdf.put("vrSrchReqDtFrom"  ,  (String)model.get("vrSrchReqDtFrom"));
                modelPdf.put("vrSrchReqDtTo"    ,  (String)model.get("vrSrchReqDtTo"));
                modelPdf.put("vrSrchCustCd"     ,  (String)model.get("vrSrchCustCd"));
                modelPdf.put("vrSrchCustNm"     ,  (String)model.get("vrSrchCustNm"));
                modelPdf.put("vrSrchRitemCd"    ,  (String)model.get("vrSrchRitemCd"));
                modelPdf.put("vrSrchRitemNm"    ,  (String)model.get("vrSrchRitemNm"));
                modelPdf.put("vrSrchWhNm"       ,  (String)model.get("vrSrchWhNm"));
                modelPdf.put("vrSrchOrdSubtype" ,  (String)model.get("vrSrchOrdSubtype"));
                modelPdf.put("vrOrdId"          ,  (String)model.get("ORD_ID0"));
                
                modelPdf.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                modelPdf.put("USER_NAME"        , (String)model.get(ConstantIF.SS_USER_NAME));
                
                ///////////pdf생성 시작 2015-04-30 chsong////////////////////
                DocPickingListHeaderVO h = dao.selectGrnHeader(modelPdf);
                DocRptGRNWorkOrderListDetailVO[] d = dao.selectGrnDetail(modelPdf); 
//                System.out.println("파일----------------------------------------------------------------------------------");
                h.setFontPath(ConstantIF.FONT_NORMAL);
                h.setTitleFontPath(ConstantIF.FONT_TITLE);
                h.setServerPath(ConstantIF.SERVER_PDF_PATH);
                
                // h.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
                h.setFileName(model.get("langName") + "_" + model.get("dateName"));
                
//                System.out.println("파일명::"+h.getFileName());
//                System.out.println("파일경로::"+h.getServerPath());
                // PDFCreate pdf = new PDFCreate();
                // modelPdf = pdf.createGrnPdf(h, d);
                errCnt = Integer.parseInt(modelPdf.get("ERRCNT").toString());

                //bean -> MAP 으로바꾸는거 (참고용으로 놨둠)
//                    Map<String, Object> detail = new HashMap<String, Object>();
//                    BeanInfo info = Introspector.getBeanInfo(d[i].getClass());
//                    for(PropertyDescriptor pd : info.getPropertyDescriptors()){
//                        Method reader = pd.getReadMethod();
//                        if(reader != null){
//                            detail.put(pd.getName(), reader.invoke(d[i]));
//                        }
//                    }

            // }
            m.put("errCnt", errCnt);
            if (errCnt == 0) {
                m.put("MSG", MessageResolver.getMessage("list.success"));
            } else {
            	m.put("errCnt", 1);
                m.put("MSG", errMsg);
            }
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }    
    */
 
    /**
     *  Method ID  : grnSearch 
     *  Method 설명  : 작업지시서(입하)발행
     *  작성자             : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateGrn2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();    
        try{
			Map<String, Object> modelIns = new HashMap<String, Object>();
			modelIns.put("ordId", (String)model.get("ORD_ID0"));
			modelIns.put("ordSeq", (String)model.get("ORD_SEQ0"));
			modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			  
			modelIns = (Map<String, Object>)dao.updateGrn(modelIns);
			ServiceUtil.isValidReturnCode("WMSRP040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));   
        
            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();               
            
            //한건만 출력하기에 우선 첫번째 주문번호로 했음
            modelPdf.put("vrSrchOrderPhase" ,  (String)model.get("vrSrchOrderPhase"));
            modelPdf.put("vrSrchOrderId"    ,  (String)model.get("vrSrchOrderId"));
            modelPdf.put("vrSrchReqDtFrom"  ,  (String)model.get("vrSrchReqDtFrom"));
            modelPdf.put("vrSrchReqDtTo"    ,  (String)model.get("vrSrchReqDtTo"));
            modelPdf.put("vrSrchCustCd"     ,  (String)model.get("vrSrchCustCd"));
            modelPdf.put("vrSrchCustNm"     ,  (String)model.get("vrSrchCustNm"));
            modelPdf.put("vrSrchRitemCd"    ,  (String)model.get("vrSrchRitemCd"));
            modelPdf.put("vrSrchRitemNm"    ,  (String)model.get("vrSrchRitemNm"));
            modelPdf.put("vrSrchWhNm"       ,  (String)model.get("vrSrchWhNm"));
            modelPdf.put("vrSrchOrdSubtype" ,  (String)model.get("vrSrchOrdSubtype"));
            modelPdf.put("vrOrdId"          ,  (String)model.get("ORD_ID0"));
            modelPdf.put("vrsrpGB"          , "20");
            modelPdf.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
            modelPdf.put("USER_NAME"        , (String)model.get(ConstantIF.SS_USER_NAME));
            
    		Map<String, Object> parameters = dao.selectGrnHeaderMap(modelPdf);
    		List<Map<String, Object>> parameterList = dao2.selectPickingDetailListIn(modelPdf);
    	
	    	
	    	setReportParameterType2(parameters);
    		
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}    	    	
    		JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_2);    	    	
    		JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
    		
    		String createFile = pdfFilepath + File.separatorChar + pdfFileName;    		
    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
		    		
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*
    public Map<String, Object> updateGrn2_OLD(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt    = 1;
        boolean isProcess = false;
        String errMsg = MessageResolver.getMessage("save.error");
        
        try{

//            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            //왜 한건만 하는지 모르겠습니다.
//            String[] ordId = new String[tmpCnt];
//            String[] ordSeq = new String[tmpCnt];

//            for (int i = 0; i < tmpCnt; i++) {
//                ordId[i] = (String)model.get("ORD_ID" + i);
//                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
//            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

//            modelIns.put("ordId", ordId);
//            modelIns.put("ordSeq", ordSeq);
            modelIns.put("ordId", (String)model.get("ORD_ID0"));
            modelIns.put("ordSeq", (String)model.get("ORD_SEQ0"));
            
            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
//            modelIns = (Map<String, Object>)dao.updateGrn(modelIns);
//            ServiceUtil.isValidReturnCode("WMSRP040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            // if(isProcess){           
                //조회
                Map<String, Object> modelPdf = new HashMap<String, Object>();               
                
                //한건만 출력하기에 우선 첫번째 주문번호로 했음
                modelPdf.put("vrSrchOrderPhase" ,  (String)model.get("vrSrchOrderPhase"));
                modelPdf.put("vrSrchOrderId"    ,  (String)model.get("vrSrchOrderId"));
                modelPdf.put("vrSrchReqDtFrom"  ,  (String)model.get("vrSrchReqDtFrom"));
                modelPdf.put("vrSrchReqDtTo"    ,  (String)model.get("vrSrchReqDtTo"));
                modelPdf.put("vrSrchCustCd"     ,  (String)model.get("vrSrchCustCd"));
                modelPdf.put("vrSrchCustNm"     ,  (String)model.get("vrSrchCustNm"));
                modelPdf.put("vrSrchRitemCd"    ,  (String)model.get("vrSrchRitemCd"));
                modelPdf.put("vrSrchRitemNm"    ,  (String)model.get("vrSrchRitemNm"));
                modelPdf.put("vrSrchWhNm"       ,  (String)model.get("vrSrchWhNm"));
                modelPdf.put("vrSrchOrdSubtype" ,  (String)model.get("vrSrchOrdSubtype"));
                modelPdf.put("vrOrdId"          ,  (String)model.get("ORD_ID0"));
                modelPdf.put("vrsrpGB"          , "20");
                
                modelPdf.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                modelPdf.put("USER_NAME"        , (String)model.get(ConstantIF.SS_USER_NAME));
                
                ///////////pdf생성 시작 2015-04-30 chsong////////////////////
                DocPickingListHeaderVO h = dao.selectGrnHeader(modelPdf);
                DocPickingListDetailVO[] d = dao2.selectPickingDetail(modelPdf);   
//                System.out.println("파일----------------------------------------------------------------------------------");
                h.setFontPath(ConstantIF.FONT_NORMAL);
                h.setTitleFontPath(ConstantIF.FONT_TITLE);
                h.setServerPath(ConstantIF.SERVER_PDF_PATH);
                h.setSrp_gb("20");
                
                // h.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
                h.setFileName(model.get("langName") + "_" + model.get("dateName"));
                
                // System.out.println("파일명::"+h.getFileName());
                // System.out.println("파일경로::"+h.getServerPath());
                // PDFCreate pdf = new PDFCreate();
                // modelPdf = pdf.createPickingPdf(h, d);
                errCnt = Integer.parseInt(modelPdf.get("ERRCNT").toString());       

                //bean -> MAP 으로바꾸는거 (참고용으로 놨둠)
//                    Map<String, Object> detail = new HashMap<String, Object>();
//                    BeanInfo info = Introspector.getBeanInfo(d[i].getClass());
//                    for(PropertyDescriptor pd : info.getPropertyDescriptors()){
//                        Method reader = pd.getReadMethod();
//                        if(reader != null){
//                            detail.put(pd.getName(), reader.invoke(d[i]));
//                        }
//                    }

            // }
            m.put("errCnt", errCnt);
            if (errCnt == 0) {
                m.put("MSG", MessageResolver.getMessage("list.success"));
            } else {
                m.put("MSG", errMsg);
            }
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }    
    */
    
    /**
     * Method ID        : listCsv
     * Method 설명              : 작업지시서 엑셀용조회
     * 작성자                         : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCsv(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("vrSrchOrderPhase" ,  (String)model.get("vrSrchOrderPhase"));
        model.put("vrSrchOrderId"    ,  (String)model.get("vrSrchOrderId"));
        model.put("vrSrchReqDtFrom"  ,  (String)model.get("vrSrchReqDtFrom"));
        model.put("vrSrchReqDtTo"    ,  (String)model.get("vrSrchReqDtTo"));
        model.put("vrSrchCustCd"     ,  (String)model.get("vrSrchCustCd"));
        model.put("vrSrchCustNm"     ,  (String)model.get("vrSrchCustNm"));
        model.put("vrSrchRitemCd"    ,  (String)model.get("vrSrchRitemCd"));
        model.put("vrSrchRitemNm"    ,  (String)model.get("vrSrchRitemNm"));
        model.put("vrSrchWhNm"       ,  (String)model.get("vrSrchWhNm"));
        model.put("vrSrchOrdSubtype" ,  (String)model.get("vrSrchOrdSubtype"));
        model.put("vrOrdId"          ,  (String)model.get("ORD_ID0"));
        
        model.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
        model.put("USER_NAME"        , (String)model.get(ConstantIF.SS_USER_NAME));
        
        map.put("LIST", dao.selectGrnListCSV(model));
        return map;
    } 
}
