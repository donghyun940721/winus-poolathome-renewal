package com.logisall.winus.wmsrp.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.PDF.model.DocRptGRNWorkOrderListDetailVO;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSRP040Dao")
public class WMSRP040Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
     * Method ID    : outSearch
     * Method 설명      : 상차리스트발행 프로시져
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object updateOut(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_print_loading", model);
        return model;
    }
    
    /**
     * Method ID    : outSearchDetail
     * Method 설명      : 상차리스트발행 상세
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    @SuppressWarnings("unchecked")
    public List<Map<String, Object> > outSearchDetailList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsop010.selectOutsearchDetailList", model);
    }
    
    @SuppressWarnings("unchecked")
    public DocRptGRNWorkOrderListDetailVO[] outSearchDetail(Map<String, Object> model) throws Exception{
        return (DocRptGRNWorkOrderListDetailVO[])getSqlMapClientTemplate().queryForList("wmsop010.selectOutsearchDetail", model).toArray(new DocRptGRNWorkOrderListDetailVO[0]);
    }

}
