package com.logisall.winus.wmsrp.service.impl;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.PDF.model.DocPickingListDetailVO;
import com.logisall.winus.frm.common.PDF.model.DocPickingListHeaderVO;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsrp.service.WMSRP030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service("WMSRP030Service")
public class WMSRP030ServiceImpl extends AbstractServiceImpl implements WMSRP030Service {
    
    @Resource(name = "WMSRP030Dao")
    private WMSRP030Dao dao;

    /**
     *  Method ID  : pickingSearch 
     *  Method 설명  : 피킹리스트발행
     *  작성자             : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatePicking(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // String vrOrdId = "";
            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i] = (String)model.get("ORD_ID" + i);
                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updatePicking(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();
            /*
	            for(int i = 0 ; i < ordId.length ; i++){
	                if(ordId.length - i == 1){
	                    vrOrdId += "'" + ordId[i] + "'";
	                }else{
	                    vrOrdId += "'" + ordId[i] + "',";
	                }
	            }
            */
            modelPdf.put("vrOrdId"		, ordId);
            modelPdf.put("vrOrdSeq"		, ordSeq);
            modelPdf.put("USER_NAME"	, (String)model.get(ConstantIF.SS_USER_NAME));
            modelPdf.put("vrOrderByGb"	, (String)model.get("vrOrderByGb"));
            modelPdf.put("SS_SVC_NO"	, (String)model.get(ConstantIF.SS_SVC_NO));
            
    		Map<String, Object> parameters = dao.selectPickingHeaderMap(modelPdf);
    		List<Map<String, Object>> parameterList = dao.selectPickingDetailList(modelPdf);
	    	
	    	setReportParameter(parameters);
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}
			
			if(model.get("vrBarCodeGb").equals("1")){
				//1D 바코드 일 경우 (Code128)
				if(model.get("vrPageGb").equals("0")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}else if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V2);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}else if(model.get("vrBarCodeGb").equals("2")){
				//2D 이미지바코드 일 경우 (DataMatrix)
				if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V3);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}
			
    		    	    	
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    /**
     *  Method ID 	 : updatePickingProcedure 
     *  Method 설명   : 피킹리스트발행 프로시저
     *  작성자             : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatePickingProcedure(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // String vrOrdId = "";
            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i] = (String)model.get("ORD_ID" + i);
                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updatePicking(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    /**
     *  Method ID  : updatePickingTotal 
     *  Method 설명  : 토탈피킹리스트발행
     *  작성자             : MonkeySeok
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatePickingTotal(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // String vrOrdId = "";
            
            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i] = (String)model.get("ORD_ID" + i);
                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updatePickingTotal(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();
            /*
	            for(int i = 0 ; i < ordId.length ; i++){
	                if(ordId.length - i == 1){
	                    vrOrdId += "'" + ordId[i] + "'";
	                }else{
	                    vrOrdId += "'" + ordId[i] + "',";
	                }
	            }
            */
            modelPdf.put("vrOrdId"		, ordId);
            modelPdf.put("vrOrdSeq"		, ordSeq);
            modelPdf.put("USER_NAME"	, (String)model.get(ConstantIF.SS_USER_NAME));
            modelPdf.put("vrOrderByGb"	, (String)model.get("vrOrderByGb"));
            modelPdf.put("SS_SVC_NO"	, (String)model.get(ConstantIF.SS_SVC_NO));
            
    		Map<String, Object> parameters = dao.selectTotalPickingHeaderMap(modelPdf);
    		List<Map<String, Object>> parameterList = dao.selectTotalPickingDetailList(modelPdf);
	    	
	    	setReportParameterTotal(parameters);
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}    	    	
			
			/*pageGb 0: 가로, 1: 세로, / barcodeGb 0: NONE바코드, 1: 1차원바코드 , 2: 2차원바코드 */
			if(model.get("vrBarCodeGb").equals("1")){
				//1D 바코드 일 경우 (Code128)
				if(model.get("vrPageGb").equals("0")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}else if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V2);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}else if(model.get("vrBarCodeGb").equals("2")){
				//2D 이미지바코드 일 경우 (DataMatrix)
				if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V3);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}else if(model.get("vrBarCodeGb").equals("0")){
				//0: NONE 바코드
				if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V4);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}else{
				JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3);
				JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
				String createFile = pdfFilepath + File.separatorChar + pdfFileName;
	    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
			}
			
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }    
    
    /**
     *  Method ID  		: updatePickingTotalProcedure 
     *  Method 설명  	: 토탈피킹리스트발행 프로시저
     *  작성자            	: KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatePickingTotalProcedure(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i] = (String)model.get("ORD_ID" + i);
                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updatePickingTotal(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }    
    
    /**
     *  Method ID  : updatePickingTotalV2 
     *  Method 설명  : 토탈피킹리스트발행
     *  작성자             : MonkeySeok
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatePickingTotalV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // String vrOrdId = "";
            
            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i] = (String)model.get("ORD_ID" + i);
                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updatePickingTotal(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();
            /*
	            for(int i = 0 ; i < ordId.length ; i++){
	                if(ordId.length - i == 1){
	                    vrOrdId += "'" + ordId[i] + "'";
	                }else{
	                    vrOrdId += "'" + ordId[i] + "',";
	                }
	            }
            */
            //modelPdf.put("vrOrdId"		, ordId);
            //modelPdf.put("vrOrdSeq"		, ordSeq);
            model.put("USER_NAME"	, (String)model.get(ConstantIF.SS_USER_NAME));
            model.put("SS_SVC_NO"	, (String)model.get(ConstantIF.SS_SVC_NO));
            
    		Map<String, Object> parameters          = dao.selectTotalPickingHeaderMapV2(model);
    		List<Map<String, Object>> parameterList = dao.selectTotalPickingDetailListV2(model);
	    	
	    	setReportParameterTotal(parameters);
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}    	    	
			
			/*pageGb 0: 가로, 1: 세로, / barcodeGb 0: NONE바코드, 1: 1차원바코드 , 2: 2차원바코드 */
			if(model.get("pkList_vrBarCodeGb").equals("1")){
				//1D 바코드 일 경우 (Code128)
				if(model.get("pkList_vrPageGb").equals("0")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}else if(model.get("pkList_vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V2);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}else if(model.get("pkList_vrBarCodeGb").equals("2")){
				//2D 이미지바코드 일 경우 (DataMatrix)
				if(model.get("pkList_vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V3);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}else if(model.get("pkList_vrBarCodeGb").equals("0")){
				//0: NONE 바코드
				if(model.get("pkList_vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V4);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}
			
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    /*
	    public Map<String, Object> updatePicking_OLD(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();
	        int errCnt    = 1;
	        String errMsg = MessageResolver.getMessage("save.error");
	        
	        try{
	
	            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	            String vrOrdId = "";
	            
	            String[] ordId = new String[tmpCnt];
	            String[] ordSeq = new String[tmpCnt];
	
	            for (int i = 0; i < tmpCnt; i++) {
	                ordId[i] = (String)model.get("ORD_ID" + i);
	                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
	            }
	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();
	
	            modelIns.put("ordId", ordId);
	            modelIns.put("ordSeq", ordSeq);
	
	            // session 및 등록정보
	            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
	            // dao
	            modelIns = (Map<String, Object>)dao.updatePicking(modelIns);
	            ServiceUtil.isValidReturnCode("WMSRP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
	
	            //조회
	            Map<String, Object> modelPdf = new HashMap<String, Object>();
	
	            
	            for(int i = 0 ; i < ordId.length ; i++){
	                if(ordId.length - i == 1){
	                    vrOrdId += "'" + ordId[i] + "'";
	                }else{
	                    vrOrdId += "'" + ordId[i] + "',";
	                }
	            }
	            modelPdf.put("vrOrdId", vrOrdId);
	            modelPdf.put("USER_NAME", (String)model.get(ConstantIF.SS_USER_NAME));
	            
	            ///////////pdf생성 시작 2015-04-30 chsong////////////////////
	            DocPickingListHeaderVO h = dao.selectPickingHeader(modelPdf);
	            DocPickingListDetailVO[] d = dao.selectPickingDetail(modelPdf);                
	            // System.out.println("파일----------------------------------------------------------------------------------");
	            h.setFontPath(ConstantIF.FONT_NORMAL);
	            h.setTitleFontPath(ConstantIF.FONT_TITLE);
	            h.setServerPath(ConstantIF.SERVER_PDF_PATH);
	            
	            // h.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
	            h.setFileName(model.get("langName") + "_" + model.get("dateName"));
	            
	            // System.out.println("파일명::"+h.getFileName());
	            // System.out.println("파일경로::"+h.getServerPath());
	            
	            // PDFCreate pdf = new PDFCreate();
	            // modelPdf = pdf.createPickingPdf(h, d);
	            
	            errCnt = Integer.parseInt(modelPdf.get("ERRCNT").toString());
	            
	            if (errCnt == 0) {
	            	m.put("errCnt", 0);
	                m.put("MSG", MessageResolver.getMessage("save.success"));
	            } else {
	            	m.put("errCnt", errCnt);
	                m.put("MSG", errMsg);
	            }
	            
	        } catch(BizException be) {
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", be);
				}
	            m.put("errCnt", 1);
	            m.put("MSG", be.getMessage() );
	            
	        }catch(Exception e){
	            throw e;
	        }
	        return m;
	    }    
    */
    
    private void setReportParameter(Map<String, Object> param) {
    	if (param == null) {
    		param = new HashMap<String, Object>(); 
    	}
    	
    	param.put("SUB_TITLE", MessageResolver.getMessage("작업지시서(피킹리스트)"));
    	
    	param.put("MAIN_HEADER_LABEL_1", MessageResolver.getMessage("발행일자"));
    	param.put("MAIN_HEADER_LABEL_2", MessageResolver.getMessage("발행자"));
    	param.put("MAIN_HEADER_LABEL_3", MessageResolver.getMessage("화주"));
    	param.put("MAIN_HEADER_LABEL_4", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_HEADER_LABEL_5", MessageResolver.getMessage("배송처"));
    	param.put("MAIN_HEADER_LABEL_6", MessageResolver.getMessage("서명"));
    	param.put("MAIN_FOOTER_LABEL_1", MessageResolver.getMessage("비고") + " : ");
    	
    	param.put("MAIN_LIST_COLUMN_NAME_1", MessageResolver.getMessage("No."));
    	param.put("MAIN_LIST_COLUMN_NAME_2", MessageResolver.getMessage("주문번호"));
    	param.put("MAIN_LIST_COLUMN_NAME_3", MessageResolver.getMessage("제품"));
    	param.put("MAIN_LIST_COLUMN_NAME_4", MessageResolver.getMessage("수량"));
    	param.put("MAIN_LIST_COLUMN_NAME_5", MessageResolver.getMessage("UOM"));
    	param.put("MAIN_LIST_COLUMN_NAME_6", MessageResolver.getMessage("중량"));
    	param.put("MAIN_LIST_COLUMN_NAME_7", MessageResolver.getMessage("창고"));
    	param.put("MAIN_LIST_COLUMN_NAME_8", MessageResolver.getMessage("로케이션"));
    	param.put("MAIN_LIST_COLUMN_NAME_9", MessageResolver.getMessage("LOT번호"));
    	param.put("MAIN_LIST_COLUMN_NAME_10", MessageResolver.getMessage("기타"));
    	param.put("MAIN_LIST_COLUMN_NAME_11", MessageResolver.getMessage("유통기한"));
    }   
    
    private void setReportParameterTotal(Map<String, Object> param) {
    	if (param == null) {
    		param = new HashMap<String, Object>(); 
    	}
    	
    	param.put("SUB_TITLE", MessageResolver.getMessage("작업지시서(토탈 피킹리스트)"));
    	
    	param.put("MAIN_HEADER_LABEL_1", MessageResolver.getMessage("발행일자"));
    	param.put("MAIN_HEADER_LABEL_2", MessageResolver.getMessage("발행자"));
    	param.put("MAIN_HEADER_LABEL_3", MessageResolver.getMessage("화주"));
    	param.put("MAIN_HEADER_LABEL_4", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_HEADER_LABEL_5", MessageResolver.getMessage("배송처"));
    	param.put("MAIN_HEADER_LABEL_6", MessageResolver.getMessage("서명"));
    	param.put("MAIN_FOOTER_LABEL_1", MessageResolver.getMessage("비고") + " : ");
    	
    	param.put("MAIN_LIST_COLUMN_NAME_1", MessageResolver.getMessage("No."));
    	param.put("MAIN_LIST_COLUMN_NAME_2", MessageResolver.getMessage("주문번호"));
    	param.put("MAIN_LIST_COLUMN_NAME_3", MessageResolver.getMessage("제품"));
    	param.put("MAIN_LIST_COLUMN_NAME_4", MessageResolver.getMessage("수량"));
    	param.put("MAIN_LIST_COLUMN_NAME_5", MessageResolver.getMessage("UOM"));
    	param.put("MAIN_LIST_COLUMN_NAME_6", MessageResolver.getMessage("중량"));
    	param.put("MAIN_LIST_COLUMN_NAME_7", MessageResolver.getMessage("창고"));
    	param.put("MAIN_LIST_COLUMN_NAME_8", MessageResolver.getMessage("로케이션"));
    	param.put("MAIN_LIST_COLUMN_NAME_9", MessageResolver.getMessage("LOT번호"));
    	param.put("MAIN_LIST_COLUMN_NAME_11", MessageResolver.getMessage("유통기한"));
    }  
    
    private void setReportParameterOrdInitList(Map<String, Object> param) {
    	if (param == null) {
    		param = new HashMap<String, Object>(); 
    	}
    	
    	param.put("SUB_TITLE", MessageResolver.getMessage("주문작업명세서"));
    	
    	param.put("MAIN_HEADER_LABEL_1", MessageResolver.getMessage("발행일자"));
    	param.put("MAIN_HEADER_LABEL_2", MessageResolver.getMessage("발행자"));
    	param.put("MAIN_HEADER_LABEL_3", MessageResolver.getMessage("화주"));
    	param.put("MAIN_HEADER_LABEL_4", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_HEADER_LABEL_5", MessageResolver.getMessage("배송처"));
    	param.put("MAIN_HEADER_LABEL_6", MessageResolver.getMessage("서명"));
    	param.put("MAIN_FOOTER_LABEL_1", MessageResolver.getMessage("비고") + " : ");
    	
    	param.put("MAIN_LIST_COLUMN_NAME_1", MessageResolver.getMessage("No."));
    	param.put("MAIN_LIST_COLUMN_NAME_2", MessageResolver.getMessage("주문번호"));
    	param.put("MAIN_LIST_COLUMN_NAME_3", MessageResolver.getMessage("제품"));
    	param.put("MAIN_LIST_COLUMN_NAME_4", MessageResolver.getMessage("수량"));
    	param.put("MAIN_LIST_COLUMN_NAME_5", MessageResolver.getMessage("UOM"));
    	param.put("MAIN_LIST_COLUMN_NAME_6", MessageResolver.getMessage("중량"));
    	param.put("MAIN_LIST_COLUMN_NAME_7", MessageResolver.getMessage("창고"));
    	param.put("MAIN_LIST_COLUMN_NAME_8", MessageResolver.getMessage("로케이션"));
    	param.put("MAIN_LIST_COLUMN_NAME_9", MessageResolver.getMessage("LOT번호"));
    }   
    
    /**
     *  Method ID  : ordInitListSearch 
     *  Method 설명  : 주문작명세서 발행
     *  작성자             : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> ordInitListSearch(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            // String vrOrdId = "";
            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];
            
            for (int i = 0; i < tmpCnt; i++) {
                ordId[i] = (String)model.get("ORD_ID" + i);
                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
            }
            
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);
            
            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();
            
            modelPdf.put("vrOrdId"		, ordId);
            modelPdf.put("vrOrdSeq"		, ordSeq);
            modelPdf.put("USER_NAME"	, (String)model.get(ConstantIF.SS_USER_NAME));
            modelPdf.put("vrOrderByGb"	, (String)model.get("vrOrderByGb"));
            modelPdf.put("SS_SVC_NO"	, (String)model.get(ConstantIF.SS_SVC_NO));
            
    		Map<String, Object> parameters = dao.selectOrdListHeaderMap(modelPdf);
    		List<Map<String, Object>> parameterList = dao.selectOrdListDetailList(modelPdf);
    		
    		setReportParameterOrdInitList(parameters);
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}
			
			if(model.get("vrBarCodeGb").equals("1")){
				//1D 바코드 일 경우 (Code128)
				if(model.get("vrPageGb").equals("0")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}else if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_11);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}else if(model.get("vrBarCodeGb").equals("2")){
				//2D 이미지바코드 일 경우 (DataMatrix)
				if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_11);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}
			
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    /**
     *  Method ID  : pickingSearch_godo 
     *  Method 설명  : 피킹리스트발행
     *  작성자             : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatePickingGodo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // String vrOrdId = "";
            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i] = (String)model.get("ORD_ID" + i);
                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updatePicking(modelIns);
            ServiceUtil.isValidReturnCode("WMSRP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           

            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();
            /*
	            for(int i = 0 ; i < ordId.length ; i++){
	                if(ordId.length - i == 1){
	                    vrOrdId += "'" + ordId[i] + "'";
	                }else{
	                    vrOrdId += "'" + ordId[i] + "',";
	                }
	            }
            */
            modelPdf.put("vrOrdId"		, ordId);
            modelPdf.put("vrOrdSeq"		, ordSeq);
            modelPdf.put("USER_NAME"	, (String)model.get(ConstantIF.SS_USER_NAME));
            modelPdf.put("vrOrderByGb"	, (String)model.get("vrOrderByGb"));
            modelPdf.put("SS_SVC_NO"	, (String)model.get(ConstantIF.SS_SVC_NO));
            
    		Map<String, Object> parameters = dao.selectPickingHeaderMap(modelPdf);
    		List<Map<String, Object>> parameterList = dao.selectPickingDetailList(modelPdf);
	    	
	    	setReportParameter(parameters);
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}
			
			if(model.get("vrBarCodeGb").equals("1")){
				//1D 바코드 일 경우 (Code128)
				if(model.get("vrPageGb").equals("0")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_GODO);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}/*else if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V2);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}
			}else if(model.get("vrBarCodeGb").equals("2")){
				//2D 이미지바코드 일 경우 (DataMatrix)
				if(model.get("vrPageGb").equals("1")){
					JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_3_V3);
					JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
					String createFile = pdfFilepath + File.separatorChar + pdfFileName;
		    		JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
				}*/
			}
			
    		    	    	
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    
    /**
     *  Method ID  : updatePickingHtml 
     *  Method 설명  : 피킹리스트발행 오즈 미사용, HTML 출력
     *  작성자             : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatePickingHtml(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            // String vrOrdId = "";
            String[] ordId = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i] = (String)model.get("ORD_ID" + i);
                ordSeq[i] = (String)model.get("ORD_SEQ" + i);
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId", ordId);
            modelIns.put("ordSeq", ordSeq);

            // session 및 등록정보
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            // dao
            modelIns = (Map<String, Object>)dao.updatePicking(modelIns);

            //조회
            Map<String, Object> modelPdf = new HashMap<String, Object>();
            
            modelPdf.put("vrUserId"			, (String)model.get("vrUserId"));
            modelPdf.put("vrLcId"				, (String)model.get("vrLcId"));
            modelPdf.put("vrCustId"			, (String)model.get("vrCustId"));
            modelPdf.put("vrSrchOrderType" , (String)model.get("vrSrchOrderType"));
            
    		List<Map<String, Object>> parameterList = dao.selectPickingDetailListHtml(modelPdf);
    		
    		m.put("detailList", parameterList);
	    	
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
}
