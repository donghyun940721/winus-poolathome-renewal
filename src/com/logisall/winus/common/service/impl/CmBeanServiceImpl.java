package com.logisall.winus.common.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.common.service.CmBeanService;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("CmBeanService")
public class CmBeanServiceImpl extends AbstractServiceImpl implements CmBeanService {

	protected Log logger = LogFactory.getLog(CmBeanServiceImpl.class);

	@Resource(name = "CmBeanDao")
	private CmBeanDao dao;

	/**
	 * 대체 Method ID : selectMngCode 대체 Method 설명 : 코드자료를 조회한다. - Popup용
	 * (거래처,고객사,주선사,착지 등등) 작성자 : chSong srchKey : 명명 :구현: 파라미터 명
	 * -------------------------------------------------------------------------
	 * --------- CAR : 차량 : O : vrSrchCarCd , vrSrchCarNm(X) CENTER : 물류센터 : O :
	 * vrSrchCenterCd, vrSrchCenterNm CLIENT : 고객사 : O : vrSrchCustCd,
	 * vrSrchCustNm, srchSubCode3, srchSubCode4, srchSubCode5(화주정보)
	 * (srchSubCode3 => 100 : 고객사, 104 : 물류센터, 108 : 거래처 ) CUST : 화주 : O :
	 * vrSrchCustCd, vrSrchCustNm DEPT : 부서 : O : vrSrchDeptCd, vrSrchDeptNm
	 * DOCK : 도크 : O : vrSrchDockCd, vrSrchDockNo, vrSrchDockNm EMPLOY : 사원 : O
	 * : vrSrchEmployCd, vrSrchEmployNm ITEM : 상품 : O : vrSrchItemCd,
	 * vrSrchItemNm LOCATION : 로케이션 : O : vrSrchLocCd, vrSrchLocId UOM : UOM : O
	 * : vrSrchUomCd, vrSrchUomNm USER : 사용자 : O : vrSrchUserId, vrSrchUserNm,
	 * srchSubCode3, srchSubCode5 WH : 창고 : O : vrSrchWhCd, vrSrchWhNm ZONE : 존
	 * : O : vrSrchZoneId, vrSrchZoneNm POOL : 물류용기 : O : vrSrchPoolId,
	 * vrSrchPoolNm -- 추가 (2015-07-27 기드온) ITEMGRP : 상품군 : O : vrSrchItemGrpCd,
	 * vrSrchItemGrpNm -- 추가 (2015-07-29 기드온) ADMIN : ADMIN : O : vrSrchCustCd,
	 * vrSrchCustNm, srchSubCode3
	 * -------------------------------------------------------------------------
	 * ---------
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectMngCode(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("CAR")) {
			map.put("CAR", dao.selectMngCodeCAR(model));
		} else if (srchKey.equals("CENTER")) {
			map.put("CENTER", dao.selectMngCodeCENTER(model));
		} else if (srchKey.equals("CLIENT")) {
			map.put("CLIENT", dao.selectMngCodeCLIENT(model));
		} else if (srchKey.equals("CUST")) {
			if ("OK".equals(model.get("custId"))) {
				map.put("CUST", dao.selectMngCodeStore(model));
			} else {
				map.put("CUST", dao.selectMngCodeCUST(model));
			}
		} else if (srchKey.equals("DEPT")) {
			map.put("DEPT", dao.selectMngCodeDEPT(model));
		} else if (srchKey.equals("DOCK")) {
			map.put("DOCK", dao.selectMngCodeDOCK(model));
		} else if (srchKey.equals("EMPLOY")) {
			map.put("EMPLOY", dao.selectMngCodeEMPLOY(model));
		} else if (srchKey.equals("ITEM")) {
			map.put("ITEM", dao.selectMngCodeITEM(model));
		} else if (srchKey.equals("ITEMEQUALS")) {
			map.put("ITEM", dao.selectMngCodeITEMEQUALS(model));
		} else if (srchKey.equals("LOCATION")) {
			map.put("LOCATION", dao.selectMngCodeLOCATION(model));
		} else if (srchKey.equals("LOCATION_ONLY")) {
			map.put("LOCATION", dao.selectMngCodeLOCATIONONLY(model));
		} else if (srchKey.equals("UOM")) {
			map.put("UOM", dao.selectMngCodeUOM(model));
		} else if (srchKey.equals("USER")) {
			map.put("USER", dao.selectMngCodeUSER(model));
		} else if (srchKey.equals("WH")) {
			map.put("WH", dao.selectMngCodeWH(model));
		} else if (srchKey.equals("ZONE")) {
			map.put("ZONE", dao.selectMngCodeZONE(model));
		} else if (srchKey.equals("POOL")) {
			map.put("POOL", dao.selectMngCodePOOL(model));
		} else if (srchKey.equals("ITEMGRP")) {
			map.put("ITEMGRP", dao.selectMngCodeITEMGRP(model));
		} else if (srchKey.equals("ADMIN")) {
			map.put("ADMIN", dao.selectMngCodeADMIN(model));
		} else if (srchKey.equals("DLVCUST")) {
			map.put("DLVCUST", dao.selectMngCodeDLVCUST(model));
		} else if (srchKey.equals("PARTNER")) {
			map.put("PARTNER", dao.selectMngCodePARTNER(model));
		} else if (srchKey.equals("TEL")) {
			map.put("TEL", dao.selectMngCodeTEL(model));
		} else if (srchKey.equals("ITEMIF")) {
			map.put("ITEMIF", dao.selectMngCodeITEMIF(model));
		} else if (srchKey.equals("DRIVER")) {
			map.put("DRIVER", dao.selectMngCodeDRIVER(model));
		}
		return map;
	}
	
	/**
	 * -------------------------------------------------------------------------
	 * FNC_CODE :: 
	 * -------------------------------------------------------------------------
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectFncCode(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String fcnKey     = (String)model.get("fcnKey");
		String text       = (String)model.get("text");
		String paramKey01 = (String)model.get("paramKey01");
		String paramKey02 = (String)model.get("paramKey02");
		
		Map<String, Object> modelDt = new HashMap<String, Object>();
		modelDt.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO));
		modelDt.put("TEXT"     , text);
		modelDt.put("PARAM_01" , paramKey01);
		modelDt.put("PARAM_02" , paramKey02);
		
		//FNC_USER_CODE
		if(fcnKey.equals("FNC_USER_CODE")){
			
		}
				
		//FNC_USER_CODE2
		if(fcnKey.equals("FNC_USER_CODE2")){
			if(paramKey01.equals("RITEMID")){
				modelDt = (Map<String, Object>)dao.selectFncCode2(modelDt);
				map.put("O_REF", modelDt.get("O_REF"));
			}
		}
		return map;
	}
}
