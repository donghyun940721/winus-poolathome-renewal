package com.logisall.winus.common.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.common.service.CmBeanService;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class CmBeanController {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "CmBeanService")
	private CmBeanService service;

	@RequestMapping("/selectMngCode.action")
	public ModelAndView selectMngCode(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.selectMngCode(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/selectFncCode.action")
	public ModelAndView selectFncCode(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.selectFncCode(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
