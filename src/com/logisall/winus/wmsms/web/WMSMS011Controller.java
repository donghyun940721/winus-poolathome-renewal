package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS011Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS011Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	static final String[] COLUMN_NAME_WMSMS011 = {
		  "TRUST_CUST_ID", "CUST_CD", "CUST_NM", "REP_NM", "TEL"
		, "FAX", "MOBILE_NO", "ZIP", "ADDR", "E_MAIL"
		, "HTTP", "BIZ_COND", "BIZ_TYPE", "CREDIT_GRADE", "DLV_CNTL"
		, "REPRESENT_CUST_CD", "CUST_STAT", "VAT_YN", "CUST_EPC_CD", "TAG_PREFIX"
		, "SGLN_CD", "CUST_INOUT_TYPE", "BEST_DATE_YN", "ASN_YN", "ASN_IN_LC_ID"
		, "ASN_IN_TRUST_CUST_ID", "ASN_IN_CUST_ID", "ASN_TYPE"
		, "DEL_YN", "WORK_IP", "REG_NO", "UPD_NO"
	};	
	
	static final String[] COLUMN_NAME_WMSMS011_PROCEDURE = {
		  "I_TRANS_CUST_CD",
		  "I_TRANS_CUST_NM",
		  "I_TRANS_CUST_INOUT_TYPE",
		  "I_TRANS_LC_ID",
		  "I_ASN_SET_YN",
		  "I_ZIP",
		  "I_ADDR",
		  "I_REP_NM",
		  "I_MOBILE_NO",
		  "I_E_MAIL",
	};	
	
	static final String[] COLUMN_NAME_WMSMS011_PROCEDURE_SIMPLE = {
		"I_TRANS_CUST_CD",
		"I_TRANS_CUST_NM",
		"I_ZIP",
		"I_ADDR",
		"I_REP_NM",
		"I_MOBILE_NO",
		"I_E_MAIL",
	};	
	
	@Resource(name = "WMSMS011Service")
	private WMSMS011Service service;

 
    /*-
     * Method ID : mn
     * Method 설명 : 매출거래처관리 화면
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSMS011.action")
    public ModelAndView mn(Map<String, Object> model){
        return new ModelAndView("winus/wmsms/WMSMS011");
    }
    
    /*-
	 * Method ID : mn2
	 * Method 설명 : 화주 조회 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS011Q1.action")
	public ModelAndView mn2(Map<String, Object> model) {
		return new ModelAndView("winus/wmsms/WMSMS011Q1");
	}
	
    /*-
     * Method ID : list
     * Method 설명 : 매출거래처관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSMS011/list.action")
    public ModelAndView list(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
        }
        return mav;
    }
    
    /*-
	 * Method ID : listE1
	 * Method 설명 : 화주 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS011Q1/list.action")
	public ModelAndView listQ1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listQ1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
    /*-
     * Method ID : save
     * Method 설명 : 매출거래처관리 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS011/save.action")
    public ModelAndView save(Map<String, Object> model) throws Exception{
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = null;
        try{
            m = service.save(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m = new HashMap<String, Object>();
            //m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("MSG", e.getMessage());
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID : listExcel
     * Method 설명 : 매출거래처관리 엑셀다운
     * 작성자 : 기드온
     * @param request
     * @param response
     * @param model
     */
    @RequestMapping("/WMSMS011/excel.action")
    public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcel(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelDown(response, grs);
            }
        }catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
        }
    }
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("코드"), "0", "0", "0", "0", "200"},
                                   {"매출처명", "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("생성일"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("생성자"), "3", "3", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"   , "S"},
                                    {"CUST_NM"  , "S"},
                                    {"REG_DT"  , "S"},
                                    {"REG_NM"  , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("거래처정보관리");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
            	log.error("fail download Excel file...", e);
            }
        }
    }



	/*-
	 * Method ID  : wmsms011E2
	 * Method 설명  : 매출거래처등록 팝업
	 * 작성자             : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS011E2.action")
	public ModelAndView wmsms011E2(Map<String, Object> model) {
		return new ModelAndView("winus/wmsms/WMSMS011E2");
	}

	/*-
	 * Method ID : wmsms120E2 Method 설명 : 엑셀업로드 화면 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS011E3.action")
	public ModelAndView wmsms011E3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS011E3");
	}
	
	/*-
	 * Method ID 		: wmsms011E4 
	 * Method 설명 	: 엑셀업로드 (프로시저) 화면 
	 * 작성자 			: KSJ
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS011E4.action")
	public ModelAndView wmsms011E4(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS011E4");
	}
	
	/*-
	 * Method ID 		: wmsms011E5
	 * Method 설명 	: 엑셀 간편 업로드 (프로시저) 화면 
	 * 작성자 			: kimzero
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS011E5.action")
	public ModelAndView wmsms011E5(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS011E5");
	}

	/*-
	 * Method ID : uploadInfo Method 설명 : Excel 파일 읽기 작성자 : kwt
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS011/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS011, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID : saveLcSync 
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS011/saveLcSync.action")
	public ModelAndView saveLcSync(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveLcSync(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID 		: uploadInfoProcedure
	 * Method 설명 	: 거래처 엑셀 업로드 (프로시저) 
	 * 작성자 			: KSJ
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS011/uploadInfoProcedure.action")
	public ModelAndView uploadInfoProcedure(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			model.put("WORK_IP", request.getRemoteAddr());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			String uploadType = (String) model.get("uploadType");
			
			List<Map> list = new ArrayList<Map>();
			if(uploadType.equals("NORMAL")){
				//excelLimitRowRead(File file, String[] cellName, int startSheet, int startRow, int endRow, int startCell)
				list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS011_PROCEDURE, 0, startRow, 10000, 0);
			}else if (uploadType.equals("SIMPLE")){
				list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS011_PROCEDURE_SIMPLE, 0, startRow, 10000, 0);
			}
			m = service.saveUploadDataProcedure(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	/*-
	 * Method ID    : saveCopyTransCust
	 * Method 설명      : 거래처 복사
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS011/saveCopyTransCust.action")
	public ModelAndView saveCopyTransCust(@RequestParam Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCopyTransCust(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}