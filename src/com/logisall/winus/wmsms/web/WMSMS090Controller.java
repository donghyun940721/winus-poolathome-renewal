package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS090Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS090Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS090Service")
    private WMSMS090Service service;
    
	static final String[] COLUMN_NAME_WMSMS090 = {
			"ITEM_KOR_NM", "ITEM_ENG_NM", "ITEM_SHORT_NM", "ITEM_EPC_CD", "ITEM_BAR_CD", //5 line
			"DEL_YN", "WORK_IP", "LC_ID", "CUST_ID", "ITEM_CODE",//5 line
			"STOCK_UOM_ID", "BUY_UNIT", "ITEM_TYPE", "ITEM_WGT", "ITEM_GRP_ID",//5 line
			"PACKING_BOX_TYPE", "PROP_STOCK_DAY", "TIME_PERIOD_DAY", "SHIP_ABC", "SET_ITEM_YN",//5 line
			"SET_ITEM_TYPE", "ITEM_CAPACITY",//2 line
			"WEIGHT_CLASS", "ORD_CUST_ID", "ITEM_SIZE", "KAN_CD",//4 line
			"VAT_YN", "ITEM_CLASS", "OP_QTY", "RS_QTY", "EA_YN",//5 line
			"ITEM_EPC_CD", "IN_WH_ID", "IN_ZONE", "PLT_YN", "REP_UOM_ID",//5 line
			"MIN_UOM_ID", "ITEM_NM", "QTY",  "OP_UOM_ID", "CUST_BARCODE",//5 line
			"CUST_ITEM_CD", "UNIT_PRICE", "REMARK", "PROP_QTY", "EXPIRY_DATE",//5 line
			"RFID", "TAG_PREFIX", "CUST_EPC_CD", "USE_YN", "BEST_DATE_TYPE",//5 line
			"BEST_DATE_NUM", "BEST_DATE_UNIT", "OUT_BEST_DATE_NUM", "OUT_BEST_DATE_UNIT", "IN_BEST_DATE_NUM",//5 line
			"IN_BEST_DATE_UNIT", "USE_DATE_NUM", "USE_DATE_UNIT", "COLOR", "SIZE_W",//5 line
			"SIZE_H", "SIZE_L", "ITF_CD", "TYPE_ST", "BOX_EPC_CD",//5 line
			"BOX_BAR_CD", "CUST_LEGACY_ITEM_CD", "MAKER_NM", "LOT_USE_YN", "REG_NO",//5 line
			"UPD_NO","UOM_QTY_STAN", "UOM_CD_STAN", "UOM_CD_TARGET",//4 line
			"AUTO_OUT_ORD_YN","AUTO_CAL_PLT_YN","OUT_LOC_REC","ITEM_DEVISION_CD","WORKING_TIME"//5 line
	};
	
	static final String[] COLUMN_NAME_WMSMS090_PK = {
		 "ITEM_LOCAL_NM"
		,"ITEM_ENG_NM"
		,"ITEM_BAR_CD"
		,"ITEM_CODE"
		,"UOM_CD"

		,"ITEM_WGT"
		,"SET_ITEM_YN"
		,"ITEM_SIZE"
		,"PLT_YN"
		,"ITEM_NM"

		,"CUST_ITEM_CD"
		,"UNIT_PRICE"
		,"PROP_QTY"
		,"EXPIRY_DATE"
		,"COLOR"

		,"SIZE_W"
		,"SIZE_H"
		,"SIZE_L"
		,"BOX_BAR_CD"
		,"CUST_LEGACY_ITEM_CD"

		,"MAKER_NM"
		,"ITEM_CAPACITY"
	};

	/**
	 * Method ID : wmsms090 Method 설명 : 상품정보관리 화면 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSMS090.action")
	public ModelAndView wmsms090(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090", service.selectData(model));
	}
	
	/**
	 * Method ID : wmsms090pop 
	 * Method 설명 : 상품정보관리 화면 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090pop.action")
	public ModelAndView wmsms090pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090pop");
	}

	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/list.action")
	public ModelAndView listItem(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : saveItem Method 설명 : 상품정보관리 저장수정 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/save.action")
	public ModelAndView saveItem(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveItem(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : listUom Method 설명 : UOM환산이력 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/listUom.action")
	public ModelAndView listUom(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listUom(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list uom :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : saveSub Method 설명 : UOM환산이력 저장 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/saveSub.action")
	public ModelAndView saveSub(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveUom(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : saveSub2 Method 설명 : UOM환산이력 저장2 Ritem_cd를 db에서 찾아서 처리 작성자 :
	 * chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/saveSub2.action")
	public ModelAndView saveSub2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveUom2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save sub2 :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : listExcel Method 설명 : 엑셀다운로드 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download Excel file :", e);
			}
		}
	}
	
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
					            		  {MessageResolver.getText("화주")      		, "0", "0", "0", "0", "200"}
					            		, {MessageResolver.getText("상품군")      		, "1", "1", "0", "0", "200"}
					            		, {MessageResolver.getText("상품코드")      	, "2", "2", "0", "0", "200"}
					            		, {MessageResolver.getText("상품명")      		, "3", "3", "0", "0", "200"}
					            		, {MessageResolver.getText("대표UOM")      	, "4", "4", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("기본입고존")      	, "5", "5", "0", "0", "200"}
					            		, {MessageResolver.getText("기본출고존")      	, "6", "6", "0", "0", "200"}
					            		, {MessageResolver.getText("상품중량")      	, "7", "7", "0", "0", "200"}
					            		, {MessageResolver.getText("세로")      		, "8", "8", "0", "0", "200"}
					            		, {MessageResolver.getText("높이")      		, "9", "9", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("가로")      		, "10", "10", "0", "0", "200"}
					            		, {MessageResolver.getText("임가공상품여부")     , "11", "11", "0", "0", "200"}
					            		, {MessageResolver.getText("임가공상품타입")     , "12", "12", "0", "0", "200"}
					            		, {MessageResolver.getText("용적")     		, "13", "13", "0", "0", "200"}
					            		, {MessageResolver.getText("상품바코드")      	, "14", "14", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("화주상품코드")		, "15", "15", "0", "0", "200"}
					            		, {MessageResolver.getText("박스바코드")      	, "16", "16", "0", "0", "200"}
					            		, {MessageResolver.getText("상품설명")      	, "17", "17", "0", "0", "200"}
					            		, {MessageResolver.getText("유효기간")      	, "18", "18", "0", "0", "200"}
					            		, {MessageResolver.getText("적정재고수량")		, "19", "19", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("적정재고일수")		, "20", "20", "0", "0", "200"}
					            		, {MessageResolver.getText("MAKER이름")		, "21", "21", "0", "0", "200"}
					            		, {MessageResolver.getText("단가")      		, "22", "22", "0", "0", "200"}
					            		, {MessageResolver.getText("통화명")      		, "23", "23", "0", "0", "200"}
					            		, {MessageResolver.getText("UOM_YN")		, "24", "24", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("입고LOT사용여부")	, "25", "25", "0", "0", "200"}
					            		, {MessageResolver.getText("DEL_YN")      	, "26", "26", "0", "0", "200"}
					            		, {MessageResolver.getText("품온구분")			, "27", "27", "0", "0", "200"} 
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
					            		  {"CUST_NM"			, "S"}
					            		, {"ITEM_GRP_NM"		, "S"}
					            		, {"ITEM_CODE"			, "S"}
					            		, {"ITEM_KOR_NM"		, "S"}
					            		, {"REP_UOM_CD"			, "S"}
					            		
					            		, {"IN_ZONE_NM"			, "S"}
					            		, {"OUT_ZONE_NM"		, "S"}
					            		, {"ITEM_WGT"			, "S"}
					            		, {"SIZE_H"				, "S"}
					            		, {"SIZE_L"				, "S"}
					            		
					            		, {"SIZE_W"				, "S"}
					            		, {"SET_ITEM_YN"		, "S"}
					            		, {"SET_ITEM_TYPE"		, "S"}
					            		, {"ITEM_CAPACITY"		, "S"}
					            		, {"ITEM_BAR_CD"		, "S"}
					            		
					            		, {"CUST_ITEM_CD"		, "S"}
					            		, {"BOX_BAR_CD"			, "S"}
					            		, {"ITEM_DESC"			, "S"}
					            		, {"BEST_DATE_NUM"		, "S"}
					            		, {"PROP_QTY"			, "S"}
					            		
					            		, {"PROP_STOCK_DAY"		, "S"}
					            		, {"MAKER_NM"			, "S"}
					            		, {"UNIT_PRICE"			, "S"}
					            		, {"CURRENCY_NAME"		, "S"}
					            		, {"UOM_YN"				, "S"}
					            		
					            		, {"LOT_USE_YN"			, "S"}
					            		, {"DEL_YN"				, "S"}
					            		, {"TEMP_TYPE_NAME"			, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("상품정보관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/**
	 * Method ID : wmsms0902 Method 설명 : 상품정보관리 화면 sample 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSMS0902.action")
	public ModelAndView wmsms0902(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS0902", service.selectData(model));
	}

	/**
	 * Method ID : WMSMS090E4 Method 설명 : 엑셀업로드 화면 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090E4.action")
	public ModelAndView wmsms090E4(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090E4");
	}

	/**
	 * Method ID : uploadInfo Method 설명 : Excel 파일 읽기 작성자 : kwt
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}     

	/**
	 * Method ID : 기존 등록 ITEM_CODE 중복 체크
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/overapCheck.action")
	public ModelAndView overapCheck(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.overapCheck(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : save
	 * Method 설명 : 통합 HelpDesk 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/saveItemImg.action")
	public ModelAndView saveItemImg(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveItemImg(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSMS090/insertValidate.action")
	public ModelAndView insertValidate(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.insertValidate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : uploadInfo Method 설명 : Excel 파일 읽기 작성자 : kwt
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/uploadInfoPk.action")
	public ModelAndView uploadInfoPk(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090_PK, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("chkDuplicationYn"	,model.get("chkDuplicationYn"));
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			//run
			m = service.saveUploadDataPk(mapBody);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}  
	
	/*-
	 * Method ID : saveLcSync 
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090/saveLcSync.action")
	public ModelAndView saveLcSync(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveLcSync(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : checkUomChange
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090/checkUomChange.action")
	public ModelAndView checkUomChange(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.checkUomChange(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : wmsms090pop2 
	 * Method 설명 : 상품정보관리 화면 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS090pop2.action")
	public ModelAndView wmsms090pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS090pop2");
	}
	

	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/deletedList.action")
	public ModelAndView deletedList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.deletedList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/restoreItem.action")
	public ModelAndView restoreItem(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.restoreItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : selectDuplicateBarcd
	 * Method 설명 : 중복바코드 조회(상품관리메뉴)
	 * 작성자 : ykim
	 * 작성일 : 21.11.19
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS090/selectDuplicateBarcd.action")
	public ModelAndView selectDuplicateBarcd(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.selectDuplicateBarcd(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}
}
