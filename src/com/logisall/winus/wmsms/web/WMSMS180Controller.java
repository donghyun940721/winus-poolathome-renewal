package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.FSUtil;
import com.logisall.winus.wmsms.service.WMSMS180Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS180Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	final static String[] COLUMN_NAME_WMSMS180 = {
		"ZONE_NM", "LC_ID", "LOC_ID"		
		, "DEL_YN", "WORK_IP", "REG_NO", "UPD_NO"
	};
	
	@Resource(name = "WMSMS180Service")
	private WMSMS180Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : ZONE정보등록 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSMS180.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmsms/WMSMS180");
	}

	/*-
	 * Method ID : sublist
	 * Method 설명 : ZONE정보등록 서브 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS180/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list(sub) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : saveSub
	 * Method 설명 : ZONE정보등록 서브 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS180/sub_save.action")
	public ModelAndView saveSub(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveSub(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save(sub) :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : saveSub_T1
	 * Method 설명 : 거래처별 권역 저장
	 * 작성자 : sing09 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS180/saveSub_T1.action")
	public ModelAndView saveSub_T1(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveSub_T1(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save(sub) :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : saveSub
	 * Method 설명 : ZONE정보등록 서브 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS180/sub_delete.action")
	public ModelAndView deleteSub(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.deleteSub(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save(sub) :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
