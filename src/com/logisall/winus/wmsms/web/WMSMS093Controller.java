package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS093Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS093Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS093Service")
    private WMSMS093Service service;
    
    
	static final String[] COLUMN_NAME_WMSMS093 = {
			"ITEM_GRP_ID", "WGT_ARROW_RATE", "BEST_DATE_NUM", "BEST_DATE_UNIT"
	};

	/*-
	 * Method ID    : wmsms100
	 * Method 설명  : 상품별물류기기관리 화면
	 * 작성자       : kwt
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSMS093.action")
	public ModelAndView wmsms093(Map<String, Object> model) throws Exception {
		Map<String, Object> m = null;
		ModelAndView mav = new ModelAndView("winus/wmsms/WMSMS093");
		/*
		try {
			m = service.getItemList(model);
			if (m != null) {
				mav.addAllObjects(m);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		*/
		return mav;
	}
	/*-
	 * Method ID    : list
	 * Method 설명  : 상품별물류기기 조회
	 * 작성자       : kwt
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS093/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : save
	 * Method 설명  : 상품별물류기기정보 저장
	 * 작성자       : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS093/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : autoOrderInsert.action
	 * Method 설명      : 주문입력
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSMS093/autoOrderInsert.action")
	public ModelAndView autoOrderInsert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoOrderInsert(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
