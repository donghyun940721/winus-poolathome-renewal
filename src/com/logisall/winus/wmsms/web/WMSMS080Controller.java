package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS080Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS080Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	static final String[] COLUMN_NAME_WMSMS080 = {
			"LC_ID", "LOC_CD", "ROWCNT", "COL", "DAN"
			, "LOC_TYPE", "LOC_STAT", "WH_ID", "CUST_ID", "SHIP_ABC"
			, "ITEM_GRP", "ITEM_ID", "CELL_W", "CELL_H", "CELL_L"
			, "OP_QTY", "RS_QTY", "CARRY_TYPE", "CARRY_QTY", "MULTI_ITEM_YN"
			, "WM_LINE", "WORK_RANK", "MAX_DAN", "DLV_CNTL", "PLTBOX_YN"
			, "PLTBOX_TYPE", "CARRY_PLT_QTY", "SYS_RSV_YN", "TAG_PREFIX", "LOC_BARCODE_CD"
			, "LOC_GLN_CD", "POSITION_X", "POSITION_Y", "POSITION_Z", "RACK_DIRECTION"
			, "DEL_YN", "WORK_IP", "REG_NO", "UPD_NO"};

	static final String[] COLUMN_NAME_WMSMS080_SIMPLE = {
			"WH_ID", "LOC_CD", "LOC_BARCODE_CD", "LOC_TYPE", "LOC_STAT", "CARRY_PLT_QTY"
//			,"LOC_ABLE_PLT_QTY"
			, "CARRY_TYPE", "OP_QTY", "CUST_ID"};
	
	static final String[] COLUMN_NAME_WMSMS080_SIMPLE_V2 = {
		"WH_ID", "LOC_CD", "LOC_BARCODE_CD", "LOC_TYPE", "LOC_STAT", "CARRY_PLT_QTY"
		, "CARRY_TYPE", "OP_QTY", "CUST_ID", "ROWCNT", "COL", "DAN"};
	
	@Resource(name = "WMSMS080Service")
	private WMSMS080Service service;

	/*-
	 * Method ID : mn 
	 * Method 설명 : 로케이션정보 화면 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSMS080.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS080", service.selectBox(model));
	}

	/*-
	 * Method ID : mn 
	 * Method 설명 : 출고통제 화면(공지) 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS080E2.action")
	public ModelAndView popmn(Map<String, Object> model) {
		return new ModelAndView("winus/wmsms/WMSMS080E2"); 
	}

	/*-
	 * Method ID : save 
	 * Method 설명 : 출고통제 저장 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS080E2/save.action")
	public ModelAndView sub_save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveSub(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : list 
	 * Method 설명 : 로케이션정보 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS080/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list info :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : delList 
	 * Method 설명 : 삭제된로케이션정보 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS080/delList.action")
	public ModelAndView delList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.delList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list info :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : save 
	 * Method 설명 : 로케이션정보 저장 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS080/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID : restore 
	 * Method 설명 : 로케이션 복구
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS080/restore.action")
	public ModelAndView restore(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.restore(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to restore :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("update.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID : update 
	 * Method 설명 : 로케이션정보 수정(출고통제 값) 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS080/update.action")
	public ModelAndView update(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.update(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("update.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : sublist 
	 * Method 설명 : 출고통제 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS080/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get sub list info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : listExcel 
	 * Method 설명 : 로케이션정보 엑셀다운 
	 * 작성자 : 기드온
	 * 
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSMS080/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download Excel :", e);
			}
		}
	}

	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			String[][] headerEx = {
					  {MessageResolver.getMessage("로케이션")   , "0", "0", "0", "0", "200"}
					, {MessageResolver.getMessage("행")       , "1", "1", "0", "0", "200"}
					, {MessageResolver.getMessage("열")       , "2", "2", "0", "0", "200"}
					, {MessageResolver.getMessage("단")       , "3", "3", "0", "0", "200"}
					, {MessageResolver.getMessage("LOC구분")   , "4", "4", "0", "0", "200"}
					, {MessageResolver.getMessage("사용상태")   , "5", "5", "0", "0", "200"}
					, {MessageResolver.getMessage("상품군")    , "6", "6", "0", "0", "200"}
					, {MessageResolver.getMessage("가로")     , "7", "7", "0", "0", "200"}
					, {MessageResolver.getMessage("높이")     , "8", "8", "0", "0", "200"}
					, {MessageResolver.getMessage("세로")     , "9", "9", "0", "0", "200"}
					, {MessageResolver.getMessage("적재구분")   , "10", "10", "0", "0", "200"}
					, {MessageResolver.getMessage("통제여부")   , "11", "11", "0", "0", "200"}
					, {MessageResolver.getMessage("바코드")    , "12", "12", "0", "0", "200"}
					, {MessageResolver.getMessage("상품용기구분"), "13", "13", "0", "0", "200"}
			};
			// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시..
			// / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {
						{"LOC_CD", "S"}
			  	      , {"ROWCNT", "S"}
			  	      , {"COL", "S"}
			  	      , {"DAN", "S"}
			  	      , {"LOC_TYPE_NM", "S"}
					  , {"LOC_STAT_NM", "S"}
					  , {"ITEM_GRP", "S"}
					  , {"CELL_W", "S"}
					  , {"CELL_H", "S"}
					  , {"CELL_L", "S"}
					  , {"CARRY_TYPE_NM", "S"}
					  , {"DLV_CNTL", "S"}
					  , {"LC_BARCODE_CD", "S"}
					  , {"LOC_ITEM_TYPE", "S"}
			};

			// 파일명
			String FileName = MessageResolver.getText("로케이션정보관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String Etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, FileName, sheetName, marCk, Etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download Excel :", e);
			}
		}
	}

	/*-
	 * Method ID : WMSMS080E3 Method 설명 : 엑셀업로드 화면 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS080E3.action")
	public ModelAndView wmsms080E3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS080E3");
	}

	/*-
	 * Method ID : WMSMS080E4 Method
	 * 설명 : 로케이션업로드 간편
	 * 작성자 : kimzero
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS080E4.action")
	public ModelAndView wmsms080E4(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS080E4");
	}
	
	/*-
	 * Method ID : WMSMS080E4 Method
	 * 설명 : 로케이션업로드 간편 , 행열단
	 * 작성자 : sing09
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS080E5.action")
	public ModelAndView wmsms080E5(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS080E5");
	}
	
	/*-
	 * Method ID : inExcelFileUpload Method 설명 : Excel 파일 읽기 작성자 : chSong
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS080/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			String uploadType = (String) model.get("uploadType");
			
			List<Map> list = new ArrayList<Map>();
			if(uploadType.equals("NORMAL")){
				//excelLimitRowRead(File file, String[] cellName, int startSheet, int startRow, int endRow, int startCell)
				list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS080, 0, startRow, 10000, 0);
			}else if (uploadType.equals("SIMPLE")){
				list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS080_SIMPLE, 0, startRow, 10000, 0);
			}else if (uploadType.equals("SIMPLE_V2")){
				list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS080_SIMPLE_V2, 0, startRow, 10000, 0);
			}
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID : WMSMS080pop Method 설명 : 로케이션재등록 pop-up window open function
	 * 
	 * @param model
	 * @return model
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS080pop.action")
	public ModelAndView WMSMS080pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS080pop");
	}
}
