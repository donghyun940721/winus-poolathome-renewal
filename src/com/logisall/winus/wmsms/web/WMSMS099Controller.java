package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS099Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS099Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS099Service")
    private WMSMS099Service service;
    
	static final String[] COLUMN_NAME_WMSMS099 = {
			"ITEM_KOR_NM", "ITEM_ENG_NM", "ITEM_SHORT_NM", "ITEM_EPC_CD", "ITEM_BAR_CD", //5 line
			"DEL_YN", "WORK_IP", "LC_ID", "CUST_ID", "ITEM_CODE",//5 line
			"STOCK_UOM_ID", "BUY_UNIT", "ITEM_TYPE", "ITEM_WGT", "ITEM_GRP_ID",//5 line
			"PACKING_BOX_TYPE", "PROP_STOCK_DAY", "TIME_PERIOD_DAY", "SHIP_ABC", "SET_ITEM_YN",//5 line
			"SET_ITEM_TYPE", "ITEM_CAPACITY",//2 line
			"WEIGHT_CLASS", "ORD_CUST_ID", "ITEM_SIZE", "KAN_CD",//4 line
			"VAT_YN", "ITEM_CLASS", "OP_QTY", "RS_QTY", "EA_YN",//5 line
			"ITEM_EPC_CD", "IN_WH_ID", "IN_ZONE", "PLT_YN", "REP_UOM_ID",//5 line
			"MIN_UOM_ID", "ITEM_NM", "QTY",  "OP_UOM_ID", "CUST_BARCODE",//5 line
			"CUST_ITEM_CD", "UNIT_PRICE", "REMARK", "PROP_QTY", "EXPIRY_DATE",//5 line
			"RFID", "TAG_PREFIX", "CUST_EPC_CD", "USE_YN", "BEST_DATE_TYPE",//5 line
			"BEST_DATE_NUM", "BEST_DATE_UNIT", "OUT_BEST_DATE_NUM", "OUT_BEST_DATE_UNIT", "IN_BEST_DATE_NUM",//5 line
			"IN_BEST_DATE_UNIT", "USE_DATE_NUM", "USE_DATE_UNIT", "COLOR", "SIZE_W",//5 line
			"SIZE_H", "SIZE_L", "ITF_CD", "TYPE_ST", "BOX_EPC_CD",//5 line
			"BOX_BAR_CD", "CUST_LEGACY_ITEM_CD", "MAKER_NM", "LOT_USE_YN", "REG_NO",//5 line
			"UPD_NO","UOM_QTY_STAN", "UOM_CD_STAN", "UOM_CD_TARGET",//4 line
			"AUTO_OUT_ORD_YN","AUTO_CAL_PLT_YN","OUT_LOC_REC","ITEM_DEVISION_CD","WORKING_TIME"//5 line
	};
	
	static final String[] COLUMN_NAME_WMSMS099_PK = {
		 "ITEM_LOCAL_NM"
		,"ITEM_ENG_NM"
		,"ITEM_BAR_CD"
		,"ITEM_CODE"
		,"UOM_CD"

		,"ITEM_WGT"
		,"SET_ITEM_YN"
		,"ITEM_SIZE"
		,"PLT_YN"
		,"ITEM_NM"

		,"CUST_ITEM_CD"
		,"UNIT_PRICE"
		,"PROP_QTY"
		,"EXPIRY_DATE"
		,"COLOR"

		,"SIZE_W"
		,"SIZE_H"
		,"SIZE_L"
		,"BOX_BAR_CD"
		,"CUST_LEGACY_ITEM_CD"

		,"MAKER_NM"
		,"ITEM_CAPACITY"
	};

	/**
	 * Method ID : wmsms099 Method 설명 : 상품정보관리 화면 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSMS099.action")
	public ModelAndView wmsms099(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS099", service.selectData(model));
	}
	
	
	@RequestMapping("/WMSMS099pop.action")
	public ModelAndView wmscm091q7(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS099pop");
	}
	
	
	
	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS099/list.action")
	public ModelAndView listItem(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}
	
	
	/**
	 * Method ID :listDetailItem 이력 조회
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS099/Detail_list.action")
	public ModelAndView listDetailItem(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listDetailItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}


	
	/*-
	 * Method ID : save
	 * Method 설명 : 통합 HelpDesk 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS099/saveItemImg.action")
	public ModelAndView saveItemImg(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveItemImg(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSMS099/insertValidate.action")
	public ModelAndView insertValidate(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.insertValidate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : saveLcSync 
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS099/saveLcSync.action")
	public ModelAndView saveLcSync(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveLcSync(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
}
