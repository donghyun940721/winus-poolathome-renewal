package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.FSUtil;
import com.logisall.winus.wmsms.service.WMSMS081Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS081Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	final static String[] COLUMN_NAME_WMSMS081 = {
		"ZONE_NM", "LOC_CD", "WORK_SEQ", "DEL_YN", "WORK_IP", "UPD_NO"
	};
	
	@Resource(name = "WMSMS081Service")
	private WMSMS081Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : ZONE정보등록 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSMS081.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmsms/WMSMS081");
	}

	/*-
	 * Method ID : list
	 * Method 설명 : ZONE정보등록  조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS081/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : sublist
	 * Method 설명 : ZONE정보등록 서브 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS081/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list(sub) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : saveSub
	 * Method 설명 : ZONE정보등록 서브 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS081/sub_save.action")
	public ModelAndView saveSub(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveSub(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save(sub) :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			if (e.getMessage().equals(MessageResolver.getMessage("chek.ritemid"))) {
				String[] name = {MessageResolver.getMessage("로케이션")};
				m.put("MSG", MessageResolver.getMessage("need.duplicate.check", name));
			}
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : ZONE정보등록  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS081/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : WMSMS081E2
	 * Method 설명      : 엑셀업로드 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS081E2.action")
	public ModelAndView WMSMS081E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS081E2");
	}

	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : chSong
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS081/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			//log.info("START===================file");
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS081, 0, startRow, 10000, 0);
			//log.info("START===================file1111");
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel file :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
}
