package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS190Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS190Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS190Service")
	private WMSMS190Service service;

	static final String[] COLUMN_NAME_WMSMS190 = {
		"POOL_GRP_CD", "CUST_ID", "POOL_CODE", "POOL_NM", "STOCK_UOM_CD", 
		"PROP_QTY", "PROP_STOCK_DAY", "POOL_WGT", "POOL_CAPACITY", "REMARK", 
		"IN_WH_ID", "IN_ZONE_CD", "UNIT_PRICE", "ASSET_TYPE_CD", "COMP_EPC_CD", 
		"POOL_WGT_ARROW_RANGE", "POOL_BAR_CD", "SIZE_W", "SIZE_L", "SIZE_H", 
		"ITF_CD", "COLOR", "AUTO_PLT_TRANS_YN", "PLT_STAT", "PARCEL_BOX_GB"
	};
	static final String[] COLUMN_NAME_WMSMS190_SIMPLE = {
		"POOL_GRP_CD", "POOL_CODE", "POOL_NM", "STOCK_UOM_CD", "POOL_BAR_CD",
		"PLT_STAT", "OUT_LOC_REC", "PROP_QTY", "PROP_STOCK_DAY", "POOL_WGT", 
		"POOL_CAPACITY", "REMARK", "UNIT_PRICE"
	};

	/*-
	 * Method ID : mn Method 설명 : 물류용기관리 화면 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSMS190.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS190", service.selectBox(model));
	}

	/*-
	 * Method ID : list Method 설명 : 물류용기관리 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS190/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save Method 설명 : 물류용기관리 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS190/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcel Method 설명 : 엑셀다운로드 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS190/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download Excel File :", e);
			}
		}
	}

	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
			String[][] headerEx = {{MessageResolver.getText("부자재코드")	, "0", "0", "0", "0", "100"}
								 , {MessageResolver.getText("부자재명")	, "1", "1", "0", "0", "100"}
								 , {MessageResolver.getText("부자재군")	, "2", "2", "0", "0", "100"}
								 , {MessageResolver.getText("입고창고코드")	, "3", "3", "0", "0", "100"}
								 , {MessageResolver.getText("유통기간")	, "4", "4", "0", "0", "100"}
								 , {MessageResolver.getText("유효기간일수")	, "5", "5", "0", "0", "100"}
								 };

			String[][] valueName = {{"POOL_CODE"		, "S"}
								  , {"POOL_NM"			, "S"}
								  , {"POOL_GRP_ID"		, "S"}
								  , {"IN_WH_CD"			, "S"}
								  , {"EXPIRY_DATE"		, "S"}
								  , {"TIME_PERIOD_DAY"	, "S"}
								  };

			// 파일명
			String fileName = MessageResolver.getText("포장부자재관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download Excel File :", e);
			}
		}
	}

	/*-
	 * Method ID : wmsms190E2 Method 설명 : 물류용기 팝업 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS190E2.action")
	public ModelAndView wmsms190E2(Map<String, Object> model) {
		return new ModelAndView("winus/wmsms/WMSMS190E2");
	}

	/*-
	 * Method ID : orderPoolList Method 설명 : 물류용기 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS190E2/list.action")
	public ModelAndView orderPoolList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listOrderPool(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list info :", e);
			}
		}
		return mav;
	}


	/*-
	 * Method ID : wmsms190E2
	 * Method 설명 : 엑셀업로드
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS190E3.action")
	public ModelAndView wmsms190E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS190E3");
	}
	/*-
	 * Method ID : WMSMS190E4
	 * Method 설명 : 엑셀업로드(간편)
	 * 작성자 : sing09
	 * @param model
	 * @param response
	 */
	@RequestMapping("/WMSMS190E4.action")
	public ModelAndView WMSMS190E4(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS190E4");
	}


	/*-
	 * Method ID : uploadInfo
	 * Method 설명 : 엑셀파일업로드
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param txtFile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS190/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS190, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID : uploadInfo_simple
	 * Method 설명 : 엑셀파일업로드_간편
	 * 작성자 : sing09
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSMS190/uploadInfo_simple.action")
	public ModelAndView uploadInfo_simple(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS190_SIMPLE, 0, startRow, 10000, 0);
			m = service.saveUploadData_simple(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID : save
	 * Method 설명 : 통합 HelpDesk 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS190/saveItemImg.action")
	public ModelAndView saveItemImg(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveItemImg(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}