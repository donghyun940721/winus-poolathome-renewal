package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS150Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS150Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS150Service")
	private WMSMS150Service service;

	static final String[] COLUMN_NAME_WMSMS150 = {
			"CUST_ID", "LC_ID", "LOC_ID", "CYCL_STOCK_TYPE", "TYPE_ST", "WORK_DAY_NUM", "RITEM_ID"
			, "DEL_YN", "WORK_IP", "REG_NO", "UPD_NO"
	};

	/*-
     * Method ID : mn
     * Method 설명 : 순환재고조사설정 화면
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSMS150.action")
    public ModelAndView mn(Map<String, Object> model)  throws Exception{
        return new ModelAndView("winus/wmsms/WMSMS150", service.selectBox(model));
    }
    
    /*-
     * Method ID : list
     * Method 설명 : 순환재고조사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSMS150/list.action")
    public ModelAndView list(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
        }
        return mav;
    }
    
    /*-
     * Method ID    : save
     * Method 설명      : 순환재고조사 저장
     * 작성자                 : 기드온
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSMS150/save.action")
    public ModelAndView save(Map<String, Object> model){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();
        try{
            m = service.save(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    } 
    
    

    /*-
     * Method ID : listExcel
     * Method 설명 : 순환재고설정 엑셀다운
     * 작성자 : 기드온
     * @param request
     * @param response
     * @param model
     */
    @RequestMapping("/WMSMS150/excel.action")
    public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcel(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelDown(response, grs);
            }
        }catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
        }
    }
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 엑셀다운로드
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주"), "0", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("순환재고정리방식"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("개체총량구분"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("대상일수"), "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품"), "6", "7", "0", "0", "200"}                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_CD"   , "S"},
                                    {"CUST_NM"   , "S"},
                                    {"CYCL_STOCK_TYPE_EXCEL"  , "S"},
                                    {"TYPE_ST"  , "S"},
                                    {"WORK_DAY_NUM"  , "S"},
                                    {"LOC_CD"  , "S"},
                                    {"RITEM_CD"  , "S"},
                                    {"RITEM_NM"  , "S"}                                   
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("재고조사설정");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}


	/*-
	 * Method ID : wmsms150E2
	 * Method 설명 : 엑셀업로드
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS150E2.action")
	public ModelAndView wmsms150E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS150E2");
	}

	/*-
	 * Method ID : uploadInfo 
	 * Method 설명 : Excel 파일 읽기 
	 * 작성자 : kwt
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS150/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS150, 0, startRow, 10000, 0);
			
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : itemPop
	 * Method 설명      : 상품조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM150/itemPop.action")
	public ModelAndView itemPop(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.itemPop(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : wmscm091
	 * Method 설명      : 상품조회 POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS150.action")
	public ModelAndView wmsms150(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS150Q1", service.itemPop(model));
	}
}
