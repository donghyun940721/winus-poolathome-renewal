package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS095Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS095Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	static final String[] COLUMN_NAME_WMSMS095 = {
		"RITEM_ID", "ITEM_WGT_ARROW_RANGE", "ITEM_WGT_ARROW_RATE", "WGT_PRIORITY", "BEST_DATE_NUM", "BEST_DATE_UNIT" 
	};
	

	@Resource(name = "WMSMS095Service")
	private WMSMS095Service service;

 
    /*-
     * Method ID : mn
     * Method 설명 : 화주별거래처상품관리 화면
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSMS095.action")
    public ModelAndView mn(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsms/WMSMS095", service.selectBox(model));        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
    
   
    
    /*-
     * Method ID : itemList
     * Method 설명 : 화주별거래처상품관리 상품조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSMS095/itemList.action")
    public ModelAndView itemList(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listItem(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
        }
        return mav;
    }
    /*-
     * Method ID : itemGrpList
     * Method 설명 : 화주별거래처상품관리 상품군조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSMS095/itemGrpList.action")
    public ModelAndView itemGrpList(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listItemGrp(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("fail to get item group list...", e);
			}
        }
        return mav;
    }
    
    /*-
     * Method ID : save
     * Method 설명 : 화주별거래처상품관리 상품저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS095/itemsave.action")
    public ModelAndView save(Map<String, Object> model) throws Exception{
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = null;
        try{
            m = service.saveItem(model);
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
            m = new HashMap<String, Object>();
            m.put("MSG", MessageResolver.getMessage("save.error"));
            if(e.getMessage().equals(MessageResolver.getMessage("chek.ritemid"))){
                m.put("MSG", MessageResolver.getMessage("ritemid.chek"));
            }          
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID : itemGrpsave
     * Method 설명 : 화주별거래처상품관리 상품저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS095/itemGrpsave.action")
    public ModelAndView itemGrpsave(Map<String, Object> model) throws Exception{
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = null;
        try{
            m = service.saveItemGrp(model);
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("fail to save item group...", e);
			}
            m = new HashMap<String, Object>();
            m.put("MSG", MessageResolver.getMessage("save.error"));     
            if(e.getMessage().equals(MessageResolver.getMessage("chek.itemgrp"))){
                m.put("MSG", MessageResolver.getMessage("itemgrp.chek"));
            }
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID    : listExcel
     * Method 설명      : 화주별거래처상품관리 엑셀다운로드
     * 작성자                 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS095/excel.action")
    public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcelItem(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelDown(response, grs);
            }
        }catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("fail to download Excel file...", e);
			}
        }
    }
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("상품군")     , "0", "1", "0", "0", "100"}
				                    ,{MessageResolver.getText("무게오차허용범위(%)")     , "2", "2", "0", "0", "400"}
				                    ,{MessageResolver.getText("출하최소유효기간")       , "3", "3", "0", "0", "400"}
				                    ,{MessageResolver.getText("출하최소유효기간단위")        , "4", "4", "0", "0", "250"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                     {"ITEM_CODE"  , "S"}
                                    ,{"ITEM_KOR_NM"    , "S"}
                                    ,{"ITEM_WGT_ARROW_RANGE"    , "N"}
                                    ,{"BEST_DATE_NUM"     , "S"}
                                    ,{"BEST_DATE_UNIT_EXCEL"     , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("상품군목록");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
            	log.error("fail download Excel file...", e);
            }
        }
    } 
    
    
    /*-
     * Method ID    : listExcel2
     * Method 설명      : 화주별거래처상품군관리 엑셀다운로드
     * 작성자                 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS095/excel2.action")
    public void listExcel2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcelItemGrp(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelDown2(response, grs);
            }
        }catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
        }
    }
    
    
    /*-
     * Method ID : doExcelDown2
     * Method 설명 : 아이템그룹 엑셀 다운로드
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                    {MessageResolver.getText("상품군")     , "0", "1", "0", "0", "100"}
                                   ,{MessageResolver.getText("무게오차허용범위(%)")     , "2", "2", "0", "0", "400"}
                                   ,{MessageResolver.getText("출하최소유효기간")       , "3", "3", "0", "0", "400"}
                                   ,{MessageResolver.getText("출하최소유효기간단위")        , "4", "4", "0", "0", "250"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                     {"ITEM_GRP_CD"  , "S"}
                                    ,{"ITEM_GRP_NM"    , "S"}
                                    ,{"WGT_ARROW_RATE"    , "N"}
                                    ,{"BEST_DATE_NUM"     , "S"}
                                    ,{"BEST_DATE_UNIT_EXCEL"     , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("상품목록");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
            	log.error("fail download Excel file...", e);
            }
        }
    } 
    

    /*-
     * Method ID : wmsms095E2
     * Method 설명 : 엑셀업로드 화면
     * 작성자 : kwt
     *
     * @param model
     * @param request
     * @param response
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS095E2.action")
    public ModelAndView wmsms095E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
    	return new ModelAndView("winus/wmsms/WMSMS095E2");     
    }
    
    /*-
     * Method ID : uploadItemInfo
     * Method 설명 : 엑셀파일 업로드 
     * 작성자 : kwt
     *
     * @param request
     * @param response
     * @param model
     * @param txtFile
     * @return
     * @throws Exception
     */
    @RequestMapping("/WMSMS095/uploadItemInfo.action")
    public ModelAndView uploadItemInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model,  @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = null;
        try {
            request.setCharacterEncoding("utf-8");
            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");
            String fileName = file.getOriginalFilename();
            String filePaths = ConstantIF.TEMP_PATH;
            
            if(!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);
            }            
            File destinationDir = new File(filePaths);            
            File destination = File.createTempFile("excelTemp", fileName, destinationDir); //두개의 문자값으로 조합...하여 파일이름을 변경저장
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));         
            
            int startRow = Integer.parseInt((String)model.get("startRow"));            
            List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS095, 0, startRow, 10000, 0);//로우제한sheet idx,시작행 idx,끝행(시작행에서 몇번째),시작열 idx)            
            m = service.saveCsvItem(model, list);
            
            destination.deleteOnExit();
            mav.addAllObjects(m);          
            
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("fail upload Excel file...", e);
			}
            m = new HashMap<String, Object>();
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("errCnt", "1");
            mav.addAllObjects(m);  
        }
        return mav;
    }           
     
}