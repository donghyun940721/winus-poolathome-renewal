package com.logisall.winus.wmsms.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsms.service.WMSMS092Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSMS092Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS092Service")
	private WMSMS092Service service;

	static final String[] COLUMN_NAME_WMSMS092 = {
		"CUST_CODE", "SET_ITEM_CODE", "PART_ITEM_CODE", "PART_ITEM_QTY", "UOM_CODE"
	};

	/*-
	 * Method ID    : wmsms092
	 * Method 설명      : 세트상품구성정보 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSMS092.action")
	public ModelAndView wmsms092(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS092", service.selectData(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 세트상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSMS092/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listE5
	 * Method 설명      : 세트상품구성내역조회 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSMS092/listE5.action")
	public ModelAndView listE5(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE5(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listSub
	 * Method 설명      : 구성상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSMS092/listSub.action")
	public ModelAndView listSub(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List Sub :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveSub
	 * Method 설명      : 구성상품 저장수정
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSMS092/saveSub.action")
	public ModelAndView saveItem(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSub(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS092/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listSubExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to Excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("상품코드")   , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")     , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("수량")      , "2", "2", "0", "0", "100"},
                                   {"UOM"                               , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("생성일")     , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("생성자")     , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("수정일")     , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("수정자")     , "7", "7", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"ITEM_CODE"        , "S"},
                                    {"PART_RITEM_NM"    , "S"},
                                    {"QTY"              , "N"},
                                    {"UOM_ID"           , "S"},
                                    {"REG_DT"           , "S"},
                                    {"REG_NM"           , "S"},
                                    {"UPD_DT"           , "S"},
                                    {"UPD_NM"           , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("세트상품구성정보");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isWarnEnabled()) {
				log.warn("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS092/excelE5.action")
	public void listExcelE5(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listSubExcelE5(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownE5(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to Excel :", e);
			}
		}
	}
	protected void doExcelDownE5(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("셋트상품코드")		, "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("셋트상품명")		, "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("파트상품코드")		, "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("파트상품명")		, "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("수량")			, "4", "4", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"SET_RITEM_CD"		, "S"},
                                    {"SET_RITEM_NM"		, "S"},
                                    {"PART_RITEM_CD"	, "S"},
                                    {"PART_RITEM_NM"	, "S"},
                                    {"QTY"           	, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("세트상품구성내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isWarnEnabled()) {
				log.warn("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : wmsms092E2
	 * Method 설명      : 엑셀업로드 화면
	 * 작성자                 : kwt
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSMS092E2.action")
	public ModelAndView wmsms092E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS092E2");
	}

	/*-
	 * Method ID  : uploadInfo
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS092/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS092, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID : 
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSMS092E3.action")
	public ModelAndView wmsms092E3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsms/WMSMS092E3");
	}
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : chSong
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS092E3/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int colSize = Integer.parseInt((String) model.get("colSize"));
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}

			int startRow = Integer.parseInt((String) model.get("startRow"));

			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 1000, 0);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("LIST", list);

			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", map);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID   : getCustIdByLcId
	 * Method 설명 : LC_ID로 해당 코드의 화주ID 가져오기
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSMS092E3/getItemSubGrid.action")
	public ModelAndView getItemSubGrid(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getItemSubGrid(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE6
	 * Method 설명   : 매칭정보관리 조회
	 * 작성자         : kcr
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSMS092/listE6.action")
	public ModelAndView listE6(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE6(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listSub
	 * Method 설명      : 구성상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSMS092/listE7.action")
	public ModelAndView listE7(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE7(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List Sub :", e);
			}
		}
		return mav;
	}
	

	

	/*-
	 * Method ID    : saveSub
	 * Method 설명      : 구성상품 저장수정
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
//	@RequestMapping("/WMSMS092/saveSub.action")
//	public ModelAndView saveItem(Map<String, Object> model) {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.saveSub(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save Sub :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
	
	
	/*-
	 * Method ID    : saveE7
	 * Method 설명      : 매칭정보관리 저장
	 * 작성자                 : KCR
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSMS092/saveE6.action")
	public ModelAndView saveE6(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE6(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSMS092/excelE6.action")
	public void listExcelE6(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listSubExcelE6(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to Excel :", e);
			}
		}
	}	
}
