package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS011Service {

    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveLcSync(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadDataProcedure(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveCopyTransCust(Map<String, Object> model) throws Exception;
}
