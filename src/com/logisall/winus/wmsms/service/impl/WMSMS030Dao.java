package com.logisall.winus.wmsms.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS030Dao")
public class WMSMS030Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
	 * Method ID  : selectLc
	 * Method 설명  : selectLc
	 * 작성자             : chsong
	 * @param model
	 * @return
	 */
	public Object selectLc(Map<String, Object> model){
		return executeQueryForList("tmsys091.selectLc", model);
	}
	
    /**
     * Method ID : list
     * Method 설명 :
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms030.searchlist", model);
    }   
    
    
    /**
     * Method ID : insert
     * Method 설명 : 물류센터 사업자 정보 등록 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model){
        return executeInsert("wmsms010.insert", model);
    }
    
    /**
     * Method ID : updateCust
     * Method 설명 : 물류센터 사업자 정보 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object updateCust(Map<String, Object> model){
        return executeUpdate("wmsms010.update", model);
    }
    /**
     * Method ID : updateLc
     * Method 설명 :  물류센터 정보 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object updateLc(Map<String, Object> model){
        return executeUpdate("wmsms030.update", model);
    }
    /**
     * Method ID : delete
     * Method 설명 : 물류센터 정보 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object delete(Map<String, Object> model){
        return executeUpdate("wmsms030.delete", model);
    }
    
    /**
     * Method ID  : insertLc
     * Method 설명  : 물류센터 정보 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public String[]  insertLc(Map<String, Object> model){
        String[] val = {"", "0"};
        executeUpdate("wmsms030.pk_wmsms030.sp_new_lc", model);
        int err = Integer.parseInt(model.get("O_MSG_CODE").toString());
        if(err != 0){
            val[0] = model.get("O_MSG_NAME").toString();
            val[1] = model.get("O_MSG_CODE").toString();
        }
        return val;
    }
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }
    
}
