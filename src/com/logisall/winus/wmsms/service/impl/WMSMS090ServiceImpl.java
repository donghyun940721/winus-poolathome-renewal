package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsms.service.WMSMS090Service;
//import com.m2m.jdfw5x.egov.exception.BizException;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS090Service")
public class WMSMS090ServiceImpl implements WMSMS090Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS090Dao")
	private WMSMS090Dao dao;

	private final static String[] CHECK_VALIDATE_WMSMS090 = { "CUST_ID", "REP_UOM_ID" };

	/**
	 * 대체 Method ID : selectData 대체 Method 설명 : 상품 목록 필요 데이타셋 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ITEMGRP", dao.selectItemGrp(model));
//		map.put("ITEMGRPLV1", dao.selectItemGrpLv1(model));
//		map.put("ITEMGRPLV2", dao.selectItemGrpLv2(model));
//		map.put("ITEMGRPLV3", dao.selectItemGrpLv3(model));
		map.put("UOM", dao.selectUom(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listItem 대체 Method 설명 : 상품 목록 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listItem(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.listItem(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : saveItem 대체 Method 설명 : 상품관리저장(저장,수정,삭제) 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveItem(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {

			int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
			int iuCnt = Integer.parseInt(model.get("I_selectIds").toString());
			if (iuCnt > 0) {
				// 저장, 수정
				String[] itemIdTemp = new String[iuCnt]; // 상품ID
				String[] unitPrice = new String[iuCnt];
				String[] custBarcode = new String[iuCnt]; // 화주 바코드
				String[] ritemId = new String[iuCnt]; // 센터화주상품ID
				String[] custId = new String[iuCnt]; // 화주ID

				String[] stockUomId = new String[iuCnt]; // 재고단위
				String[] buyUnit = new String[iuCnt]; // 구매단위
				String[] itemType = new String[iuCnt]; // 상품구분
				String[] itemWgt = new String[iuCnt]; // 상품중량
				String[] itemGrpId = new String[iuCnt]; // 상품군

				String[] itemGrpType = new String[iuCnt]; // 상품군 type
				String[] packingBoxType = new String[iuCnt]; // 포장box종류
				String[] propStockDay = new String[iuCnt]; // 적정재고일수
				String[] timePeriodDay = new String[iuCnt]; // 유효기간일수
				String[] shipAbc = new String[iuCnt]; // 출하abc

				String[] setItemYn = new String[iuCnt]; // 임가공상품여부
				String[] weightClass = new String[iuCnt]; // 적재무게등급
				String[] ordCustId = new String[iuCnt]; // 발주거래처ID
				String[] itemBarCd = new String[iuCnt]; // 상품바코드
				String[] itemSize = new String[iuCnt]; // 상품크기

				String[] kanCd = new String[iuCnt]; // KAN코드
				String[] vatYn = new String[iuCnt]; // 부가세여부
				String[] itemClass = new String[iuCnt]; // 상품등급
				String[] opQty = new String[iuCnt]; // OP수량
				String[] rsQty = new String[iuCnt]; // RACK보충수량

				String[] eaYn = new String[iuCnt]; // 낱개관리여부
				String[] itemEpcCd = new String[iuCnt]; // RFID태그
				String[] inWhId = new String[iuCnt]; // 입고창고코드
				String[] inZone = new String[iuCnt]; // 입고존
				String[] outZone = new String[iuCnt]; // 입고존
				String[] pltYn = new String[iuCnt]; // plt관리여부

				String[] repUomIdTemp = new String[iuCnt]; // uomid
				String[] opUomId = new String[iuCnt]; // op_uomid
				String[] propQty = new String[iuCnt]; // 적정재고
				String[] expiryDate = new String[iuCnt]; // 유통기한
				String[] tagPrefix = new String[iuCnt]; // tag_prefix

				String[] custEpcCd = new String[iuCnt]; // 회사 epc코드
				String[] itemCode = new String[iuCnt]; // 상품코드
				String[] custItemCd = new String[iuCnt]; // 화주상품코드
				String[] itemEngNm = new String[iuCnt]; // 상품명(영문)
				String[] itemKorNm = new String[iuCnt]; // 상품명(한글)

				String[] itemShortNm = new String[iuCnt]; // 상품명(약어)
				String[] itemNm = new String[iuCnt]; // 상품명

				// 추가된부분
				String[] bestDateType = new String[iuCnt]; // 유효기간통제기준
				String[] bestDateNum = new String[iuCnt]; // 유효기간
				String[] bestDateUnit = new String[iuCnt]; // 유효기간단위
				String[] useDateNum = new String[iuCnt]; // 소비기한
				String[] useDateUnit = new String[iuCnt]; // 소비기한단위

				String[] outBestDateNum = new String[iuCnt]; // 출하최소유효기간
				String[] outBestDateUnit = new String[iuCnt]; // 출하최소유효기간단위
				String[] inBestDateNum = new String[iuCnt]; // 입고최소유효기간
				String[] inBestDateUnit = new String[iuCnt]; // 입고최소유효기간단위
				
				//String[] insertOrdTime = new String[iuCnt]; // 자동주문시간

				String[] color = new String[iuCnt]; // 색상
				String[] itfCd = new String[iuCnt]; // ITF_CD
				String[] sizeH = new String[iuCnt]; // 세로
				String[] sizeL = new String[iuCnt]; // 높이
				String[] sizeW = new String[iuCnt]; // 가로

				String[] boxEpcCd = new String[iuCnt]; // 박스EPC코드
				String[] typeSt = new String[iuCnt]; // 상품관리TYPE
				String[] boxBarCd = new String[iuCnt]; // 박스바코드

				String[] custLegacyItemCd = new String[iuCnt]; // 박스바코드
				String[] makerNm = new String[iuCnt]; // 상품명(한글)
				String[] lotUseYn = new String[iuCnt];
				String[] autoOutOrdYn = new String[iuCnt];
				String[] autoCalPltYn = new String[iuCnt];
				String[] outLocRec = new String[iuCnt];
				String[] inLocRec = new String[iuCnt];
				
				String[] itemDevisionCd = new String[iuCnt]; // 상품명(한글)
				String[] lotPrefix = new String[iuCnt];
				String[] reworkItemYn = new String[iuCnt];
				
				String[] inOutBound = new String[iuCnt];
				
				String[] inCustZone = new String[iuCnt];
				String[] outCustZone = new String[iuCnt];
				String[] itemDesc = new String[iuCnt];
				String[] workingTime = new String[iuCnt];
				String[] currencyName = new String[iuCnt];
				
				String[] itemCapaCity = new String[iuCnt];
				String[] setItemType = new String[iuCnt];
				String[] useYn = new String[iuCnt];
				String[] itemTmp = new String[iuCnt];
				String[] fixLocId = new String[iuCnt];
				String[] unitQty = new String[iuCnt];
				String[] unitNm = new String[iuCnt];
				
				String[] salesPrice = new String[iuCnt];
				
				String lcId = ""; // 센터ID
				String workIp = "";
				String userNo = "";

				for (int i = 0; i < iuCnt; i++) {
					itemIdTemp[i] = (String) model.get("I_ITEM_ID" + i);
					unitPrice[i] = (String) model.get("I_UNIT_PRICE" + i);
					custBarcode[i] = (String) model.get("I_CUST_BARCODE" + i);
					ritemId[i] = (String) model.get("I_RITEM_ID" + i);
					custId[i] = (String) model.get("I_CUST_ID" + i);

					stockUomId[i] = (String) model.get("I_STOCK_UOM_ID" + i);
					buyUnit[i] = (String) model.get("I_BUY_UNIT" + i);
					itemType[i] = (String) model.get("I_ITEM_TYPE" + i);
					itemWgt[i] = (String) model.get("I_ITEM_WGT" + i);
					itemGrpId[i] = (String) model.get("I_ITEM_GRP_ID" + i);

					itemGrpType[i] = (String) model.get("I_ITEM_GRP_TYPE" + i);
					packingBoxType[i] = (String) model.get("I_PACKING_BOX_TYPE" + i);
					propStockDay[i] = (String) model.get("I_PROP_STOCK_DAY" + i);
					timePeriodDay[i] = (String) model.get("I_TIME_PERIOD_DAY" + i);
					shipAbc[i] = (String) model.get("I_SHIP_ABC" + i);

					setItemYn[i] = (String) model.get("I_SET_ITEM_YN" + i);
					weightClass[i] = (String) model.get("I_WEIGHT_CLASS" + i);
					ordCustId[i] = (String) model.get("I_ORD_CUST_ID" + i);
					itemBarCd[i] = (String) model.get("I_ITEM_BAR_CD" + i);
					itemSize[i] = (String) model.get("I_ITEM_SIZE" + i);
 
					kanCd[i] = (String) model.get("I_KAN_CD" + i);
					vatYn[i] = (String) model.get("I_VAT_YN" + i);
					itemClass[i] = (String) model.get("I_ITEM_CLASS" + i);
					opQty[i] = (String) model.get("I_OP_QTY" + i);
					rsQty[i] = (String) model.get("I_RS_QTY" + i);

					eaYn[i] = (String) model.get("I_EA_YN" + i);
					itemEpcCd[i] = (String) model.get("I_ITEM_EPC_CD" + i);
					inWhId[i] = (String) model.get("I_IN_WH_ID" + i);
					inZone[i] = (String) model.get("I_IN_ZONE" + i);
					outZone[i] = (String) model.get("I_OUT_ZONE" + i);
					pltYn[i] = (String) model.get("I_PLT_YN" + i);

					repUomIdTemp[i] = (String) model.get("I_REP_UOM_ID" + i);
					opUomId[i] = (String) model.get("I_OP_UOM_ID" + i);
					propQty[i] = (String) model.get("I_PROP_QTY" + i);
					expiryDate[i] = (String) model.get("I_EXPIRY_DATE" + i);
					tagPrefix[i] = (String) model.get("I_TAG_PREFIX" + i);

					custEpcCd[i] = (String) model.get("I_CUST_EPC_CD" + i);
					itemCode[i] = (String) model.get("I_ITEM_CODE" + i);
					custItemCd[i] = (String) model.get("I_CUST_ITEM_CD" + i);
					itemEngNm[i] = (String) model.get("I_ITEM_ENG_NM" + i).toString().replaceAll("&amp;", "&");
					itemKorNm[i] = (String) model.get("I_ITEM_KOR_NM" + i).toString().replaceAll("&amp;", "&");
					itemShortNm[i] = (String) model.get("I_ITEM_SHORT_NM" + i);
					itemNm[i] = (String) model.get("I_ITEM_NM" + i).toString().replaceAll("&amp;", "&");

					// 추가된부분
					bestDateType[i] = (String) model.get("I_BEST_DATE_TYPE" + i);
					bestDateNum[i] = (String) model.get("I_BEST_DATE_NUM" + i);
					bestDateUnit[i] = (String) model.get("I_BEST_DATE_UNIT" + i);
					useDateNum[i] = (String) model.get("I_USE_DATE_NUM" + i);
					useDateUnit[i] = (String) model.get("I_USE_DATE_UNIT" + i);

					outBestDateNum[i] = (String) model.get("I_OUT_BEST_DATE_NUM" + i);
					outBestDateUnit[i] = (String) model.get("I_OUT_BEST_DATE_UNIT" + i);
					inBestDateNum[i] = (String) model.get("I_IN_BEST_DATE_NUM" + i);
					inBestDateUnit[i] = (String) model.get("I_IN_BEST_DATE_UNIT" + i);

					//insertOrdTime[i] = (String) model.get("I_INSERT_ORD_TIME" + i);
					
					color[i] = (String) model.get("I_COLOR" + i);
					itfCd[i] = (String) model.get("I_ITF_CD" + i);
					sizeH[i] = (String) model.get("I_SIZE_H" + i);
					sizeL[i] = (String) model.get("I_SIZE_L" + i);
					sizeW[i] = (String) model.get("I_SIZE_W" + i);

					boxEpcCd[i] = (String) model.get("I_BOX_EPC_CD" + i);
					typeSt[i] = (String) model.get("I_TYPE_ST" + i);
					boxBarCd[i] = (String) model.get("I_BOX_BAR_CD" + i);

					custLegacyItemCd[i] = (String) model.get("I_CUST_LEGACY_ITEM_CD" + i);
					makerNm[i] = (String) model.get("I_MAKER_NM" + i);
					lotUseYn[i] = (String) model.get("I_LOT_USE_YN" + i);
					autoOutOrdYn[i] = (String) model.get("I_AUTO_OUT_ORD_YN" + i);
					autoCalPltYn[i] = (String) model.get("I_AUTO_CAL_PLT_YN" + i);
					outLocRec[i] = (String) model.get("I_OUT_LOC_REC" + i);
					inLocRec[i] = (String) model.get("I_IN_LOC_REC" + i);
					
					itemDevisionCd[i] = (String) model.get("I_ITEM_DEVISION_CD" + i);
					lotPrefix[i] = (String) model.get("I_LOT_PREFIX" + i);
					reworkItemYn[i] = (String) model.get("I_REWORK_ITEM_YN" + i);
					
					inOutBound[i] = (String) model.get("I_IN_OUT_BOUND" + i);
					
					inCustZone[i] = (String) model.get("I_IN_CUST_ZONE" + i);
					outCustZone[i] = (String) model.get("I_OUT_CUST_ZONE" + i);
					itemDesc[i] = (String) model.get("I_ITEM_DESC" + i);
					workingTime[i] = (String) model.get("I_WORKING_TIME" + i);
					currencyName[i] = (String) model.get("I_CURRENCY_NAME" + i);
					
					itemCapaCity[i] = (String) model.get("I_ITEM_CAPACITY" + i);
					setItemType[i] = (String) model.get("I_SET_ITEM_TYPE" + i);
					useYn[i] = (String) model.get("I_USE_YN" + i);
					itemTmp[i] = (String) model.get("I_ITEM_TMP" + i);
					fixLocId[i] = (String) model.get("I_FIX_LOC_ID" + i);
					unitQty[i] = (String) model.get("I_UNIT_QTY" + i);
					unitNm[i] = (String) model.get("I_UNIT_NM" + i);
					salesPrice[i] = (String) model.get("I_SALES_PRICE");
				}

				lcId = (String) model.get(ConstantIF.SS_SVC_NO);
				workIp = (String) model.get(ConstantIF.SS_CLIENT_IP);
				userNo = (String) model.get(ConstantIF.SS_USER_NO);

				Map<String, Object> modelIu = new HashMap<String, Object>();
				modelIu.put("ITEM_ID", itemIdTemp); // varchar2 20 not null
				modelIu.put("UNIT_PRICE", unitPrice); // number (15 (소수점포함 38))
				modelIu.put("CUST_BARCODE", custBarcode); // varchar2 20
				modelIu.put("RITEM_ID", ritemId); // varchar2 20 not null
				modelIu.put("CUST_ID", custId); // varchar2 20 not null

				modelIu.put("STOCK_UOM_ID", stockUomId); // varchar2 20
				modelIu.put("BUY_UNIT", buyUnit); // varchar2 20
				modelIu.put("ITEM_TYPE", itemType); // varchar2 20
				modelIu.put("ITEM_WGT", itemWgt); // number(10, 3)
				modelIu.put("ITEM_GRP_ID", itemGrpId); // varchar2 20

				modelIu.put("ITEM_GRP_TYPE", itemGrpType); // varchar2 20
				modelIu.put("PACKING_BOX_TYPE", packingBoxType); // varchar2 20
				modelIu.put("PROP_STOCK_DAY", propStockDay); // number 3
				modelIu.put("TIME_PERIOD_DAY", timePeriodDay); // number 10
				modelIu.put("SHIP_ABC", shipAbc); // varchar2 20

				modelIu.put("SET_ITEM_YN", setItemYn); // char 1
				modelIu.put("WEIGHT_CLASS", weightClass); // varchar2 20
				modelIu.put("ORD_CUST_ID", ordCustId); // varchar2 20
				modelIu.put("ITEM_BAR_CD", itemBarCd); // varchar2 20
				modelIu.put("ITEM_SIZE", itemSize); // varchar2 20

				modelIu.put("KAN_CD", kanCd); // varchar2 20
				modelIu.put("VAT_YN", vatYn); // char 1
				modelIu.put("ITEM_CLASS", itemClass); // varchar2 20
				modelIu.put("OP_QTY", opQty); // number 10
				modelIu.put("RS_QTY", rsQty); // number 10

				modelIu.put("EA_YN", eaYn); // char 1
				modelIu.put("ITEM_EPC_CD", itemEpcCd); // varchar2 100
				modelIu.put("IN_WH_ID", inWhId); // varchar2 20
				modelIu.put("IN_ZONE", inZone); // varchar2 20
				modelIu.put("OUT_ZONE", outZone); // varchar2 20
				modelIu.put("PLT_YN", pltYn); // char 1

				modelIu.put("REP_UOM_ID", repUomIdTemp); // varchar2 20 not null
				modelIu.put("OP_UOM_ID", opUomId); // varchar2 20
				modelIu.put("PROP_QTY", propQty); // number (15 (소수점포함 38))
				modelIu.put("EXPIRY_DATE", expiryDate); // varchar2 8
				modelIu.put("TAG_PREFIX", tagPrefix); // varchar2 20

				modelIu.put("CUST_EPC_CD", custEpcCd); // varchar2 20
				modelIu.put("ITEM_CODE", itemCode); // varchar2 200
				modelIu.put("CUST_ITEM_CD", custItemCd);// varchar2 200
				modelIu.put("ITEM_ENG_NM", itemEngNm); // varchar2 200
				modelIu.put("ITEM_KOR_NM", itemKorNm); // varchar2 200

				modelIu.put("ITEM_SHORT_NM", itemShortNm); // varchar2 200
				modelIu.put("ITEM_NM", itemNm); // varchar2 100
				modelIu.put("LC_ID", lcId); // varchar2 20 not null

				modelIu.put("WORK_IP", workIp);
				modelIu.put("USER_NO", userNo);

				modelIu.put("BEST_DATE_TYPE", bestDateType); // 유효기간통제기준
				modelIu.put("BEST_DATE_NUM", bestDateNum); // 유효기간
				modelIu.put("BEST_DATE_UNIT", bestDateUnit); // 유효기간단위
				modelIu.put("USE_DATE_NUM", useDateNum); // 소비기한
				modelIu.put("USE_DATE_UNIT", useDateUnit); // 소비기한단위

				modelIu.put("OUT_BEST_DATE_NUM", outBestDateNum); // 출하최소유효기간
				modelIu.put("OUT_BEST_DATE_UNIT", outBestDateUnit); // 출하최소유효기간단위
				modelIu.put("IN_BEST_DATE_NUM", inBestDateNum); // 입고최소유효기간
				modelIu.put("IN_BEST_DATE_UNIT", inBestDateUnit); // 입고최소유효기간단위
				
				//modelIu.put("INSERT_ORD_TIME", insertOrdTime); // 입고최소유효기간

				// 추가된부분(프로시져에서는 아직안씀)
				modelIu.put("COLOR", color); // 색상
				modelIu.put("ITF_CD", itfCd); // ITF_CD
				modelIu.put("SIZE_H", sizeH); // 사이즈_높이
				modelIu.put("SIZE_L", sizeL); // 사이즈_세로
				modelIu.put("SIZE_W", sizeW); // 사이즈_가로

				modelIu.put("BOX_EPC_CD", boxEpcCd); // 박스EPC코드
				modelIu.put("TYPE_ST", typeSt); // 상품관리TYPE
				modelIu.put("BOX_BAR_CD", boxBarCd); // 상품관리TYPE

				modelIu.put("CUST_LEGACY_ITEM_CD", custLegacyItemCd); // 상품관리TYPE
				modelIu.put("MAKER_NM", makerNm); // varchar2 200
				modelIu.put("LOT_USE_YN", lotUseYn); // char 1
				modelIu.put("AUTO_OUT_ORD_YN", autoOutOrdYn); // char 1
				modelIu.put("AUTO_CAL_PLT_YN", autoCalPltYn); // char 1
				modelIu.put("OUT_LOC_REC", outLocRec); // char 1
				modelIu.put("IN_LOC_REC", inLocRec); // char 1
				

				modelIu.put("ITEM_DEVISION_CD", itemDevisionCd); // varchar2 200
				modelIu.put("LOT_PREFIX", lotPrefix); // varchar2 50
				modelIu.put("REWORK_ITEM_YN", reworkItemYn); // char 1
				
				modelIu.put("IN_OUT_BOUND", inOutBound); // char 3
				
				modelIu.put("IN_CUST_ZONE", inCustZone); // char 20
				modelIu.put("OUT_CUST_ZONE", outCustZone); // char 20
				modelIu.put("ITEM_DESC", itemDesc); // varchar2 1000
				modelIu.put("WORKING_TIME", workingTime); // number (15 (소수점포함 38))
				modelIu.put("CURRENCY_NAME", currencyName); // VARCHAR2(3)
				
				modelIu.put("ITEM_CAPACITY", itemCapaCity); // NUMBER
				modelIu.put("SET_ITEM_TYPE", setItemType); // CHAR(1)
				modelIu.put("USE_YN", useYn); // CHAR(1)
				modelIu.put("ITEM_TMP", itemTmp); // CHAR(1)
				modelIu.put("FIX_LOC_ID", fixLocId); // CHAR(1)
				modelIu.put("UNIT_QTY", unitQty); // CHAR(1)
				modelIu.put("UNIT_NM", unitNm); // CHAR(1)
				modelIu.put("SALES_PRICE", salesPrice); // CHAR(1)
				
				ServiceUtil.checkInputValidation2(modelIu, CHECK_VALIDATE_WMSMS090);

				// dao
				dao.saveItem(modelIu);
				ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIu.get("O_MSG_CODE")),
						(String) modelIu.get("O_MSG_NAME"));

			}
			// 등록 수정 끝

			// 삭제
			if (delCnt > 0) {
				for (int i = 0; i < Integer.parseInt(model.get("D_selectIds").toString()); i++) {
					
					Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "RITEM");
					modelSP.put("I_CODE", model.get("D_RITEM_ID" + i));				
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }
					
					Map<String, Object> modelDt = new HashMap<String, Object>();
					modelDt.put("USER_ID", model.get(ConstantIF.SS_USER_ID));
					modelDt.put("ITEM_ID", model.get("D_ITEM_ID" + i));
					modelDt.put("RITEM_ID", model.get("D_RITEM_ID" + i));
					if ("DELETE".equals(model.get("D_ST_GUBUN" + i))) {
						dao.deleteItem(modelDt);
						dao.deleteRitem(modelDt);
					} else {
						errCnt++;
						m.put("errCnt", errCnt);
						throw new BizException(MessageResolver.getMessage("save.error"));
					}
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	// /**
	// *
	// * 대체 Method ID : deleteItem
	// * 대체 Method 설명 : 상품 목록 삭제 (x)
	// * 작성자 : chsong
	// * @param model
	// * @return
	// * @throws Exception
	// */
	// @Override
	// public Map<String, Object> deleteItem(Map<String, Object> model) throws
	// Exception {
	// Map<String, Object> m = new HashMap<String, Object>();
	// int errCnt = 0;
	// try{
	// dao.deleteItem(model);
	// dao.deleteRitem(model);
	// } catch(Exception e){
	// throw e;
	// }
	// return m;
	// }

	/**
	 * 
	 * 대체 Method ID : listUom 대체 Method 설명 : UOM 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listUom(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.listUom(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : saveUom 대체 Method 설명 : UOM저장 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveUom(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("USER_ID", model.get(ConstantIF.SS_USER_ID));
				modelDt.put("ST_GUBUN", model.get("ST_GUBUN" + i));
				modelDt.put("APPLY_FR_DT", model.get("APPLY_FR_DT" + i));
				modelDt.put("APPLY_TO_DT", model.get("APPLY_TO_DT" + i));
				modelDt.put("CLOSE_DT", model.get("CLOSE_DT" + i));
				modelDt.put("RITEM_ID", model.get("RITEM_ID" + i));
				modelDt.put("UOM1_CD", model.get("UOM1_CD" + i));
				modelDt.put("UOM2_CD", model.get("UOM2_CD" + i));
				modelDt.put("QTY", model.get("QTY" + i));
				modelDt.put("UOM_SEQ", model.get("UOM_SEQ" + i));

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insertUom(modelDt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.updateUom(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					dao.deleteUom(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
				
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());
			
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * 대체 Method ID : listExcel 대체 Method 설명 : 상품목록 엑셀. 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		model.put("pageIndex", "1");
		model.put("pageSize", "60000");

		map.put("LIST", dao.listItem(model));

		return map;
	}

	/**
	 * 
	 * 대체 Method ID : saveUom2 대체 Method 설명 : UOM저장2 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> saveUom2(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("USER_ID", model.get(ConstantIF.SS_USER_ID));
				modelDt.put("ST_GUBUN", model.get("ST_GUBUN" + i));
				modelDt.put("APPLY_FR_DT", model.get("APPLY_FR_DT" + i));
				modelDt.put("APPLY_TO_DT", model.get("APPLY_TO_DT" + i));
				modelDt.put("CLOSE_DT", model.get("CLOSE_DT" + i));
				modelDt.put("UOM1_CD", model.get("UOM1_CD" + i));
				modelDt.put("UOM2_CD", model.get("UOM2_CD" + i));
				modelDt.put("QTY", model.get("QTY" + i));
				modelDt.put("UOM_SEQ", model.get("UOM_SEQ" + i));

				modelDt.put("vrCustCd", model.get("vrCustCd"));
				modelDt.put("vrCustNm", model.get("vrCustNm"));
				modelDt.put("vrItemCd", model.get("vrItemCd"));

				modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insertUom2(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
			
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : saveCsvWh Method 설명 : CSV 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[] { String.valueOf(insertCnt) }));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);

		} catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : 기존 등록 ITEM_CODE 중복 체크
	 * 
	 * @param model
	 * @return
	 */
	public Map<String, Object> overapCheck(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		
		if (srchKey.equals("ITEM_CODE")) {
			map.put("ITEM_CODE", dao.overapCheck(model));
		}
		return map;
	}
	
	/**
	 * Method ID : save Method 설명 : 통합 HelpDesk 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveItemImg(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
				Map<String, Object> modelDt = new HashMap<String, Object>();
				// 확장자 잘라내기
				String str = (String) model.get("D_ATCH_FILE_NAME");
				String ext = "." + str.substring((str.lastIndexOf('.') + 1));
				// 파일 ID 만들기
				int fileSeq = 1;
				String fileId = "";
				fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
				// 저장준비
				modelDt.put("FILE_ID", fileId);
				modelDt.put("ATTACH_GB", "ITEM"); // 통합 HELPDESK 업로드
				modelDt.put("FILE_VALUE", "wmsms090"); // 기존코드 "tmsba230" 로
														// 하드코딩되어 있음 파일구분값?
				modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE")); // 서버저장경로
				modelDt.put("ORG_FILENAME", model.get("D_ATCH_FILE_NAME")); // 원본파일명
				modelDt.put("FILE_EXT", ext); // 파일 확장자
				modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));// 파일 사이즈
				// 저장
				String fileMngNo = (String) dao.fileUpload(modelDt);
				// 리턴값 파일 id랑 파일경로
				model.put("IMAGE_ID", fileId);
				model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));
				
				dao.insertInfo(model);
			}

			map.put("MSG", MessageResolver.getMessage("insert.success"));
		} catch (Exception e) {
			throw e;
		}
		return map;
	}
	
	public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("RST", dao.insertValidate(model));
		return map;
	}
	
	/**
     * 
     * 대체 Method ID   : saveUploadDataPk
     * 대체 Method 설명    : 템플릿 상품 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveUploadDataPk(Map<String, Object> model) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		int listBodyCnt   = listBody.size();
		
    	Map<String, Object> m = new HashMap<String, Object>();
        try{
            if(listBodyCnt > 0){
            	String[] itemLocalNm 	= new String[listBodyCnt];
            	String[] itemEngNm 		= new String[listBodyCnt];
            	String[] itemBarCd 		= new String[listBodyCnt];
            	String[] itemCode 		= new String[listBodyCnt];
            	String[] uomCd 			= new String[listBodyCnt];
            	
            	String[] itemWgt 		= new String[listBodyCnt];
            	String[] setItemYn 		= new String[listBodyCnt];
            	String[] itemSize		= new String[listBodyCnt];
            	String[] pltYn 			= new String[listBodyCnt];
            	String[] itemNm 		= new String[listBodyCnt];
            	
            	String[] custItemCd 	= new String[listBodyCnt];
            	String[] unitPrice 		= new String[listBodyCnt];
            	String[] propQty 		= new String[listBodyCnt];
            	String[] expiryDate 	= new String[listBodyCnt];
            	String[] color 			= new String[listBodyCnt];
            	
            	String[] sizeW 			= new String[listBodyCnt];
            	String[] sizeH 			= new String[listBodyCnt];
            	String[] sizeL 			= new String[listBodyCnt];
            	String[] boxBarCd 		= new String[listBodyCnt];
            	String[] custLegacyItemCd = new String[listBodyCnt];
            	
            	String[] makerNm 		= new String[listBodyCnt];
            	String[] itemCapacity 	= new String[listBodyCnt];
                
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	JsonObject object = (JsonObject) listBody.get(i);
                	
                	itemLocalNm[i] 		= (String)object.get("ITEM_LOCAL_NM").toString().replaceAll("\"", "");
                	itemEngNm[i] 		= (String)object.get("ITEM_ENG_NM").toString().replaceAll("\"", "");
                	itemBarCd[i] 		= (String)object.get("ITEM_BAR_CD").toString().replaceAll("\"", "");
                	itemCode[i] 		= (String)object.get("ITEM_CODE").toString().replaceAll("\"", "");
                	uomCd[i] 			= (String)object.get("UOM_CD").toString().replaceAll("\"", "");
                	
                	itemWgt[i]			= (String)object.get("ITEM_WGT").toString().replaceAll("\"", "");
                	setItemYn[i] 		= (String)object.get("SET_ITEM_YN").toString().replaceAll("\"", "");
                	itemSize[i] 		= (String)object.get("ITEM_SIZE").toString().replaceAll("\"", "");
                	pltYn[i] 			= (String)object.get("PLT_YN").toString().replaceAll("\"", "");
                	itemNm[i] 			= (String)object.get("ITEM_NM").toString().replaceAll("\"", "");
                	
                	custItemCd[i] 		= (String)object.get("CUST_ITEM_CD").toString().replaceAll("\"", "");
                	unitPrice[i] 		= (String)object.get("UNIT_PRICE").toString().replaceAll("\"", "");
                	propQty[i] 			= (String)object.get("PROP_QTY").toString().replaceAll("\"", "");
                	expiryDate[i] 		= (String)object.get("EXPIRY_DATE").toString().replaceAll("\"", "");
                	color[i] 			= (String)object.get("COLOR").toString().replaceAll("\"", "");
                	
                	sizeW[i] 			= (String)object.get("SIZE_W").toString().replaceAll("\"", "");
                	sizeH[i] 			= (String)object.get("SIZE_H").toString().replaceAll("\"", "");
                	sizeL[i] 			= (String)object.get("SIZE_L").toString().replaceAll("\"", "");
                	boxBarCd[i] 		= (String)object.get("BOX_BAR_CD").toString().replaceAll("\"", "");
                	custLegacyItemCd[i] = (String)object.get("CUST_LEGACY_ITEM_CD").toString().replaceAll("\"", "");
                	
                	makerNm[i] 			= (String)object.get("MAKER_NM").toString().replaceAll("\"", "");
                	itemCapacity[i]		= (String)object.get("ITEM_CAPACITY").toString().replaceAll("\"", "");
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("I_ITEM_LOCAL_NM"		, itemLocalNm);
                modelIns.put("I_ITEM_ENG_NM"		, itemEngNm);
                modelIns.put("I_ITEM_BAR_CD"		, itemBarCd);
                modelIns.put("I_ITEM_CODE"			, itemCode);
                modelIns.put("I_UOM_CD"				, uomCd);
                
                modelIns.put("I_ITEM_WGT"			, itemWgt);
                modelIns.put("I_SET_ITEM_YN"		, setItemYn);
                modelIns.put("I_ITEM_SIZE"			, itemSize);
                modelIns.put("I_PLT_YN"				, pltYn);
                modelIns.put("I_ITEM_NM"			, itemNm);
                
                modelIns.put("I_CUST_ITEM_CD"		, custItemCd);
                modelIns.put("I_UNIT_PRICE"			, unitPrice);
                modelIns.put("I_PROP_QTY"			, propQty);
                modelIns.put("I_EXPIRY_DATE"		, expiryDate);
                modelIns.put("I_COLOR"				, color);
                
                modelIns.put("I_SIZE_W"				, sizeW);
                modelIns.put("I_SIZE_H"				, sizeH);
                modelIns.put("I_SIZE_L"				, sizeL);
                modelIns.put("I_BOX_BAR_CD"			, boxBarCd);
                modelIns.put("I_CUST_LEGACY_ITEM_CD", custLegacyItemCd);
                
                modelIns.put("I_MAKER_NM"			, makerNm);
                modelIns.put("I_ITEM_CAPACITY"		, itemCapacity);
                
                modelIns.put("I_DUP_YN"    			, model.get("chkDuplicationYn"));
                modelIns.put("I_CUST_ID"    		, model.get("vrCustId"));
                modelIns.put("I_LC_ID"    			, model.get("SS_SVC_NO"));   
                modelIns.put("I_WORK_IP"  			, model.get("SS_CLIENT_IP"));  
                modelIns.put("I_USER_NO"  			, model.get("SS_USER_NO"));

                System.out.println(">>>" + model.get("chkDuplicationYn") + " // " + model.get("vrCustId"));
                //dao                
                modelIns = (Map<String, Object>)dao.saveUploadDataPk(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveLcSync
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveLcSync(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String[] spLcSyncArrData = model.get("MULIT_LC_ID_ARR").toString().split(",");
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            Map<String, Object> modelIns = new HashMap<String, Object>();
            if(tmpCnt > 0){
                for(int i = 0 ; i < tmpCnt ; i ++){
                    modelIns.put("I_MAKE_LC_ID"		, spLcSyncArrData);
                    modelIns.put("I_ITEM_CODE"		, (String)model.get("ITEM_CODE"+i));
                    modelIns.put("I_CUST_ID"		, (String)model.get("CUST_ID"+i));
                    modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
                    modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                    //dao
                    modelIns = (Map<String, Object>)dao.saveLcSync(modelIns);
                }
            }
            ServiceUtil.isValidReturnCode("WMSMS090", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : checkUomChange
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> checkUomChange(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
		
			Map<String, Object> modelSP = new HashMap<String, Object>();
			modelSP.put("I_TYPE", "RITEM");
			modelSP.put("I_CODE", model.get("RITEM_ID"));				
			String checkExistData = dao.checkExistData(modelSP);
			
			//System.out.println(model.get("RITEM_ID") + " ::: " + checkExistData);
			if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
				throw new BizException("재고가 존재합니다.");
            }
		
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("재고가 존재하지 않습니다."));
	
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
	            log.info(be.getMessage());
	        }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());
	
		} catch (Exception e) {
			throw e;
		}
		return m;
    }

	/**
	 * 
	 * 대체 Method ID : deletedList
	 * 대체 Method 설명 : 삭제 상품 목록 조회 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> deletedList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LIST", dao.deletedList(model));
		return map;
	}
	
	/**
	 * 
	 * 대체 Method ID : restoreItem 
	 * 설명 : 삭제 품목 복구 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> restoreItem(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("ITEM_ID", model.get("ITEM_ID_" + i));

                modelDt.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO)   );
                modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                dao.restoreItem(modelDt);
                dao.restoreItemDetail(modelDt);
                
			}
			m.put("errCnt", 0);
			m.put("MSG", MessageResolver.getMessage("save.success"));
				
		} catch (Exception e) {
			if (log.isInfoEnabled()) {
				log.info(e.getMessage());
			}
			m.put("errCnt", 1);
			m.put("MSG", e.getMessage());
			
			throw e;
		}
		return m;
	}
	
	/**
	 * 
	 * 대체 Method ID : selectDuplicateBarcd 
	 * 설명 : 중복 바코드 조회
	 * 작성자 : ykim
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectDuplicateBarcd(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap();
		int errCnt = 0;
		try {
			m.put("LIST", dao.selectDuplicateBarcd(model));
			
		} catch (Exception e) {
			if (log.isInfoEnabled()) {
				log.info(e.getMessage());
			}
			throw e;
		}
		return m;
	}
}
