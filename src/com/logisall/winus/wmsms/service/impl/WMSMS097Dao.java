package com.logisall.winus.wmsms.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS097Dao")
public class WMSMS097Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 상품군 데이터셋
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
}
