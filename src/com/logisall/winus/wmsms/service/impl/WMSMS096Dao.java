package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS096Dao")
public class WMSMS096Dao extends SqlMapAbstractDAO{   
    /**
     * Method ID    : list
     * Method 설명      : 상품별물류기기 목록 조회
     * 작성자                 : kwt
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms096.list", model);
    }    
    /**
     * Method ID    : insert
     * Method 설명      : 상품별물류기기 등록
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsms096.insert", model);
    }    
    
    /**
     * Method ID    : delet
     * Method 설명      : 상품별물류기기 삭제
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object delete(Map<String, Object> model) {
        return executeInsert("wmsms096.delete", model);
    } 
    
    /**
     * Method ID    : update
     * Method 설명      : 상품별물류기기 수정
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsms096.update", model);
    }   
    
    /**
     * Method ID  : listItem
     * Method 설명  : 자료 조회 
     * 작성자       : kwt
     * @param model
     * @return
     */
    public GenericResultSet listItem(Map<String, Object> model) {
        return executeQueryPageWq("wmsms096.itemlist", model);
    }
    
//    public List<Map<String, Object>> getItemList(Map<String, Object> model) {
//        return executeQueryForList("wmsms096.itemlist", model);
//    }
    

    /**
     * Method ID    : listPool
     * Method 설명      : 상품의 물류기기 조회
     * 작성자                 : chSong
     * @param   model
     * @return
     */
    public GenericResultSet listPool(Map<String, Object> model) {
        return executeQueryPageWq("wmspl010.listPool", model);
    }  
    
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 상품유형 가져오기
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemType", model);
    }
    
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 물류기기명 가져오기
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object getItemList(Map<String, Object> model){
        return executeQueryForList("wmsms096.itemlist", model);
    }
    
    /**
     * Method ID  : getCustList
     * Method 설명  : 거래처 가져오기
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object getCustList(Map<String, Object> model){
        return executeQueryForList("wmsms096.custList", model);
    }
    
    /**
     * Method ID  : chekCount
     * Method 설명  : insert, update 구분
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Integer chekCount(Map<String, Object> model){
        return (Integer)executeQueryForObject("wmsms096.chekCount", model);
    }
    
    /**
     * Method ID  : chekCountDel
     * Method 설명  : delete 구분
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Integer chekCountDel(Map<String, Object> model){
        return (Integer)executeQueryForObject("wmsms096.chekCountDel", model);
    }
    
    /**
     * Method ID : insertCsvItemGroup
     * Method 설명 : 상품군 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertCsvItemGroup(Map<String, Object> model, List list) throws Exception {
    		
    		SqlMapClient sqlMapClient = getSqlMapClient();
    		try {
    			sqlMapClient.startTransaction();
    			Map<String, Object> paramMap = null;
	    		for (int i=0;i<list.size();i++) {
	    			paramMap = (Map)list.get(i);
	    			
	    			paramMap.put("TRUST_CUST_ID", 	model.get("TRUST_CUST_ID"));
	    			paramMap.put("CUST_ID", 		model.get("CUST_ID"));
	    			    			
	    			if ( 
	    					(paramMap.get("TRUST_CUST_ID") != null && StringUtils.isNotEmpty( paramMap.get("TRUST_CUST_ID").toString()) )
	    					&& (paramMap.get("CUST_ID") != null && StringUtils.isNotEmpty( paramMap.get("CUST_ID").toString()) ) 
	    					&& (paramMap.get("ITEM_GRP_ID") != null && StringUtils.isNotEmpty( paramMap.get("ITEM_GRP_ID").toString()) ) 
	    					
	    					) {
	    				paramMap.put("SS_USER_NO", 		model.get("SS_USER_NO"));
		    			paramMap.put("SS_SVC_NO", 		model.get("SS_SVC_NO"));
		    			paramMap.put("SS_CLIENT_IP", 	model.get("SS_CLIENT_IP"));
		    			
		    			sqlMapClient.insert("wmsms095.itemGrpInsert", paramMap);

	    			}
	    		}
	    		sqlMapClient.endTransaction();
	    		
    		} catch(Exception e) {
    			e.printStackTrace();
    			throw e;
    			
    		} finally {
    			if (sqlMapClient != null) {
    				sqlMapClient.endTransaction();
    			}
    		}
    }       
}
