package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS085Dao")
public class WMSMS085Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : list Method 설명 : ZONE정보등록 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listOrg(Map<String, Object> model) {
		return executeQueryPageWq("wmsms085.listZoneOrg", model);
	}
	
	/**
	 * Method ID : list Method 설명 : ZONE정보등록 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsms085.listZone", model);
	}
	
	/**
	 * Method ID : listNoneGrid Method 설명 : ZONE정보등록 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object listNoneGrid(Map<String, Object> model){
        return executeQueryForList("wmsms085.listZoneNoneGrid", model);
    }

	/**
	 * Method ID : sublist Method 설명 : ZONE정보등록 서브 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet sublist(Map<String, Object> model) {
		return executeQueryPageWq("wmsms085.listDetail", model);
	}

	/**
	 * Method ID : insert Method 설명 : ZONE정보등록 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmsms085.insert", model);
	}

	/**
	 * Method ID : update Method 설명 : ZONE정보 수정 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmsms085.update", model);
	}

	/**
	 * Method ID : delete Method 설명 : ZONE정보 삭제 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmsms085.delete", model);
	}

	/**
	 * Method ID : insertSub Method 설명 : ZONE정보 서브 등록 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object insertSub(Map<String, Object> model) {
		return executeInsert("wmsms085.insertDetail", model);
	}

	/**
	 * Method ID : updateSub Method 설명 : ZONE정보 서브 수정 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object updateSub(Map<String, Object> model) {
		return executeUpdate("wmsms085.updateDetail", model);
	}

	/**
	 * Method ID : deleteSub Method 설명 : ZONE정보 서브 삭제 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteSub(Map<String, Object> model) {
		return executeDelete("wmsms085.deleteDetail", model);
	}

	/**
	 * Method ID : selectlocchek Method 설명 : 로케이션 (중복검사) 작성자 : 기드온
	 * 
	 * @param model
	 * @return Object
	 */
	public Integer selectlocchek(Map<String, Object> model) {
		return (Integer) executeView("wmsms085.chekCount", model);
	}

	/**
	 * Method ID : saveUploadData Method 
	 * 설명 : 업로드데이터등록시
	 * 작성자 : 기드온
	 * 2022.12.23 수정 SUMMER HYUN  -WORK_IP 자동입력으로 변경
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			
			String preCustZoneNm  = "NONE";
			String custZoneId = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			if ( paramMap.get("CUST_ZONE_NM") != null && StringUtils.isNotEmpty( (String)paramMap.get("CUST_ZONE_NM")) 
    					//&& paramMap.get("LC_ID") != null && StringUtils.isNotEmpty( (String)paramMap.get("LC_ID"))
    				) {
    				
    				paramMap.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
    		    	paramMap.put("REG_NO", model.get(ConstantIF.SS_USER_NO));
    		    	paramMap.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
    		    	
    				if ( !preCustZoneNm.equals((String)paramMap.get("CUST_ZONE_NM")) ) {
    					custZoneId = (String) sqlMapClient.insert("wmsms085.insertUploadData", paramMap);
    				}	   
    				
    				if ( paramMap.get("CUST_ID") != null && StringUtils.isNotEmpty( (String)paramMap.get("CUST_ID")) 
	    		    		&& custZoneId != null ) {
	    		    	paramMap.put("CUST_ZONE_ID", custZoneId);
	    		    	sqlMapClient.insert("wmsms085.insertUploadDataDetail", paramMap);
	    		    }
	    		  
	    		    preCustZoneNm = (String)paramMap.get("CUST_ZONE_NM");
    			}
    		}
    		sqlMapClient.endTransaction();
    		
		} catch(Exception e) {
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }

	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }

	public GenericResultSet listExcel(Map<String, Object> model) {
		return executeQueryPageWq("wmsms085.listDetailExcel", model);
	}	
}
