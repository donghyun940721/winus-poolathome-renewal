package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsms.service.WMSMS120Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS120Service")
public class WMSMS120ServiceImpl implements WMSMS120Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS120Dao")
    private WMSMS120Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : Dock 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : Dock 저장
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" , model.get("selectIds"));
                
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));//접속자 ID
                modelDt.put("SS_SVC_NO" , model.get(ConstantIF.SS_SVC_NO));//물류센터코드
                
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));     //상태 stgubun
                
                modelDt.put("DOCK_NO"   , model.get("DOCK_NO"+i));      //도크번호
                modelDt.put("DOCK_CD"   , model.get("DOCK_CD"+i));      //도크코드
                modelDt.put("SEQ"       , model.get("SEQ"+i));          //순번
                modelDt.put("DOCK_NM"   , model.get("DOCK_NM"+i));      //도크장명
                modelDt.put("IN_CAR_CNT", model.get("IN_CAR_CNT"+i));   //진입가능댓스
                modelDt.put("IN_TON"    , model.get("IN_TON"+i));       //진입가능톤수
                modelDt.put("REMARK"    , model.get("REMARK"+i));       //비고
                modelDt.put("CAR_ID"    , model.get("CAR_ID"+i));       //차량id
                modelDt.put("WH_ID"     , model.get("WH_ID"+i));        //창고코드
                
//                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS100);
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.delete(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
                
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }

    /**
     * 대체 Method ID   : listWhPop
     * 대체 Method 설명 : 도크장정보 창고코드 팝업 자동조회
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listWhPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listWhPop(model));
        
        return map;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 상품목록 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
	/**
	 * Method ID : saveUploadData 
	 * Method 설명 : 엑셀파일 저장 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[] { String.valueOf(insertCnt) }));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);

		} catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}    
}
