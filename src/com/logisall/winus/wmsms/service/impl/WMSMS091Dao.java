package com.logisall.winus.wmsms.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS091Dao")
public class WMSMS091Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : selectItemGrp
     * Method 설명  : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }

    /**
     * 대체 Method ID   : wmsms091.addItemList
     * 대체 Method 설명 : 기준관리 > 상품정보추가등록 > 조회
     * 작성자      : KCR
     * @param model
     * @return
     * @throws Exception
     */
    public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsms091.addItemList", model);
	}
    
    /**
     * 대체 Method ID   : wmsms091.update
     * 대체 Method 설명 : 기준관리 > 상품정보추가등록 > 저장
     * 작성자      : KCR
     * @param model
     * @return
     * @throws Exception
     */
    public Object update(Map<String, Object> model) {
		return executeUpdate("wmsms091.update", model);
	}

    
}
