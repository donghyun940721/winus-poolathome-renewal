package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;
import com.logisall.winus.frm.common.util.ConstantIF;

@Repository("WMSMS092Dao")
public class WMSMS092Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectUom
     * Method 설명  : UOM 데이터셋
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectUom(Map<String, Object> model){
        return executeQueryForList("wmsms100.selectUom", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 세트상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms090.list", model);
    }
    
    /**
     * Method ID  : listE5
     * Method 설명  : 세트상품구성내역조회 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listE5(Map<String, Object> model) {
        return executeQueryPageWq("wmsms090.listE5", model);
    }
    
    /**
     * Method ID  : list6
     * Method 설명  : 매칭정보관리
     * 작성자        : kcr
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listE6(Map<String, Object> model) {
        return executeQueryPageWq("wmsms090.listE6", model);
    }
    
    /**
     * Method ID  : listE7
     * Method 설명  : 매칭정보관리_detail
     * 작성자             : kcr
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listE7(Map<String, Object> model) {
        return executeQueryPageWq("wmsms090.listE7", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 매칭정보관리
     * 작성자                 : kcr
     * @param   model
     * @return  Object
     */
    public Object insertE6(Map<String, Object> model) {
        return executeInsert("wmsms090.insertE6", model);
    } 
    
    /**
     * Method ID  : listSub
     * Method 설명  : 구성상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmsms092.list", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명   : 구성상품 등록
     * 작성자         : kcr
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsms092.insert", model);
    }    
    
    /**
     * Method ID    : delete
     * Method 설명   : 구성상품 삭제
     * 작성자         : kcr
     * @param   model
     * @return  Object
     */
    public Object deleteE6(Map<String, Object> model) {
        return executeUpdate("wmsms090.deleteE6", model);
    }
    
    
    /**
     * Method ID    : update
     * Method 설명      : 구성상품 수정
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsms092.update", model);
    }  
    
    /**
     * Method ID    : update
     * Method 설명      : 구성상품 삭제
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmsms092.delete", model);
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀파일등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void saveUploadData(Map<String, Object> model, List list) throws Exception {
    		
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			    			
    			if (   paramMap.get("CUST_CODE") != null && StringUtils.isNotEmpty( (String)paramMap.get("CUST_CODE")) 
    			    && paramMap.get("SET_ITEM_CODE") != null && StringUtils.isNotEmpty( (String)paramMap.get("SET_ITEM_CODE"))
    			    && paramMap.get("PART_ITEM_CODE") != null && StringUtils.isNotEmpty( (String)paramMap.get("PART_ITEM_CODE"))
    			    && paramMap.get("PART_ITEM_QTY") != null && StringUtils.isNotEmpty( (String)paramMap.get("PART_ITEM_QTY"))
    			    && paramMap.get("UOM_CODE") != null && StringUtils.isNotEmpty( (String)paramMap.get("UOM_CODE"))
    			    ) {		
    				
    				paramMap.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
    				paramMap.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    				paramMap.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	    			sqlMapClient.insert("wmsms092.insertUploadData", paramMap);
    			}
    		}
    		sqlMapClient.endTransaction();
    		
		} catch(Exception e) {
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }      
    
    /**
     * Method ID  : getItemSubGrid
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getItemSubGrid(Map<String, Object> model){
        return executeQueryForList("wmsms092e3.getItemSubGrid", model);
    }
}
