package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsms.service.WMSMS096Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS096Service")
public class WMSMS096ServiceImpl implements WMSMS096Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSMS096Dao")
    private WMSMS096Dao dao;
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명 : 상품별물류기기 조회
     * 작성자           : kwt
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("ITEM_GRP_TYPE", "G");
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명 : 상품별물류기기 저장
     * 작성자           : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int chek = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("GOODS_RITEM_ID"		, model.get("GOODS_RITEM_ID"+i));
                modelDt.put("POOL_RITEM_ID"			, model.get("POOL_RITEM_ID"+i));
                modelDt.put("GOODS_QTY"				, model.get("GOODS_QTY"+i));
                modelDt.put("POOL_QTY"				, model.get("POOL_QTY"+i));
                modelDt.put("COVER_QTY"				, model.get("COVER_QTY"+i));
                modelDt.put("SS_USER_NO"			, model.get(ConstantIF.SS_USER_NO));//접속자 ID
                modelDt.put("ORD_TYPE"				, model.get("ORD_TYPE"+i));
                modelDt.put("TRANS_CUST_ID"			, model.get("TRANS_CUST_ID"+i));
                modelDt.put("VIEW_ORG_POOL_RITEM_ID", model.get("VIEW_ORG_POOL_RITEM_ID"+i));
                modelDt.put("GUBUN"					, model.get("GUBUN"+i));
                modelDt.put("LOT_PREFIX"			, model.get("LOT_PREFIX"+i));
                
                if(model.get("ORD_TYPE"+i).equals("D")){
                	chek = dao.chekCountDel(modelDt);
                    if(chek == 1){ // insert
                        dao.delete(modelDt);
                    }else {
                      errCnt++;
                      m.put("errCnt", errCnt);
                      throw new BizException(MessageResolver.getMessage("save.error"));
                    }
                }else{
                	chek = dao.chekCount(modelDt);
                    if(chek == 0){ // insert
                        dao.insert(modelDt);
                    }else if(chek == 1){// update
                        dao.update(modelDt);
                    }else {
                      errCnt++;
                      m.put("errCnt", errCnt);
                      throw new BizException(MessageResolver.getMessage("save.error"));
                    }
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));

        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());

        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명 : 상품별물류기기 저장
     * 작성자           : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int chek = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("GOODS_RITEM_ID", model.get("GOODS_RITEM_ID"+i));
                modelDt.put("POOL_RITEM_ID", model.get("POOL_RITEM_ID"+i));
                modelDt.put("GOODS_QTY", model.get("GOODS_QTY"+i));
                modelDt.put("POOL_QTY", model.get("POOL_QTY"+i));
                modelDt.put("COVER_QTY", model.get("COVER_QTY"+i));
                modelDt.put("ORD_TYPE", model.get("ORD_TYPE"+i));
                modelDt.put("GUBUN", model.get("GUBUN"+i));
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));//접속자 ID
                
                chek = dao.chekCountDel(modelDt);
                if(chek == 1){ // insert
                    dao.delete(modelDt);
                }else {
                  errCnt++;
                  m.put("errCnt", errCnt);
                  throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));

        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());

        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : getItemList
     * 대체 Method 설명 : 화면 셀렉트 박스 만들기
     * 작성자           : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object>  getItemList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        try {   
            map.put("ITEMGRP", dao.selectItemGrp(model));
            model.put("ITEM_GRP_TYPE", "P");
            map.put("ITEM_TYPE_NAME", dao.getItemList(model));
            map.put("CUST_ID", dao.getCustList(model));
//            List<Map<String, Object>> list = dao.getItemList(model);
//            if ( list != null ) {
//                String strComboInfo =  ServiceUtil.getComboOptionStr(list, "POOL_RITEM_ID", "POOL_KOR_NM1");
//                map.put("ITEM_TYPE_NAME", strComboInfo);
//            }
        } catch(Exception e) {
            throw e;
        }
        return map;
    }

    /**
     * 대체 Method ID   : listItem
     * 대체 Method 설명 : 상품별물류기기 자료조회
     * 작성자           : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listItem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("ITEM_GRP_TYPE", "P");
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listItem(model));
        
        return map;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 상품목록 엑셀.
     * 작성자           : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("ITEM_GRP_TYPE", "G");
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listPool
     * 대체 Method 설명 : 상품의 물류기기 조회
     * 작성자           : chSong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listPool(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("ITEM_GRP_TYPE", "G");
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listPool(model));
        return map;
    }

    /**
     * Method ID : saveCsvItemGrp
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCsvItemGroup(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.insertCsvItemGroup(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }    
    
}
