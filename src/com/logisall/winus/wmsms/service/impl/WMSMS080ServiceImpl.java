package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS080Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS080Service")
public class WMSMS080ServiceImpl implements WMSMS080Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS080Dao")
    private WMSMS080Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 로케이션정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : delList
     * Method 설명 : 로케이션정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> delList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.delList(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : sublist
     * Method 설명 : 출고통제 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.sublist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : update
     * Method 설명 : 로케이션정보 출고통제 수정
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> update(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            dao.updateDlvCntl(model);
            
        }catch (Exception e) {
            throw e;
        }
        return m;
    }
    /**
     * Method ID : save
     * Method 설명 : 로케이션정보 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{    
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
              
                modelDt.put("selectIds"         , model.get("selectIds"));
                modelDt.put("LC_ID"             , model.get("SS_SVC_NO"));
                modelDt.put("WORK_IP"			, model.get("SS_CLIENT_IP"));
                
                modelDt.put("LOC_ID"            , model.get("LOC_ID" + i));
                modelDt.put("LOC_CD"            , model.get("LOC_CD" + i));
                modelDt.put("ROWCNT"            , model.get("ROWCNT" + i));
                modelDt.put("COL"               , model.get("COL" + i));
                modelDt.put("DAN"               , model.get("DAN" + i));
                modelDt.put("MAX_DAN"           , ""); // 입력받는 곳 없음
                modelDt.put("LOC_TYPE"          , model.get("LOC_TYPE" + i));
                modelDt.put("LOC_STAT"          , model.get("LOC_STAT" + i));
                modelDt.put("LOC_ITEM_TYPE"     , model.get("LOC_ITEM_TYPE" + i));
                modelDt.put("WH_ID"             , model.get("WH_ID" + i));
                modelDt.put("CUST_ID"           , model.get("CUST_ID" + i));
                modelDt.put("ITEM_ID"           , ""); // 입력받는 곳 없음
                modelDt.put("SHIP_ABC"          , model.get("SHIP_ABC" + i));
                modelDt.put("CELL_W"            , model.get("CELL_W" + i));
                modelDt.put("CELL_H"            , model.get("CELL_H" + i));
                modelDt.put("CELL_L"            , model.get("CELL_L" + i));
                modelDt.put("OP_QTY"            , model.get("OP_QTY" + i));
                modelDt.put("RS_QTY"            , model.get("RS_QTY" + i));
                modelDt.put("ITEM_GRP"          , model.get("ITEM_GRP" + i));
                modelDt.put("CARRY_TYPE"        , model.get("CARRY_TYPE" + i));
                modelDt.put("WORK_RANK"         , ""); // 입력받는 곳 없음
                modelDt.put("CARRY_PLT_QTY"     , model.get("CARRY_PLT_QTY" + i));
                
                modelDt.put("TAG_PREFIX"     	, model.get("TAG_PREFIX" + i));
                modelDt.put("LC_RFID_CD"    	, model.get("LC_RFID_CD" + i));
                modelDt.put("LC_BARCODE_CD"     , model.get("LC_BARCODE_CD" + i));
                modelDt.put("LC_GLN_CD"     	, model.get("LC_GLN_CD" + i));
                
                modelDt.put("POSITION_X"     	, model.get("POSITION_X" + i));
                modelDt.put("POSITION_Y"     	, model.get("POSITION_Y" + i));
                modelDt.put("POSITION_Z"     	, model.get("POSITION_Z" + i));
                
                modelDt.put("RACK_DIRECTION"    , model.get("RACK_DIRECTION" + i));
                
                modelDt.put("REG_NO"            , model.get("SS_USER_NO"));
                modelDt.put("UPD_NO"            , model.get("SS_USER_NO"));
                modelDt.put("DLV_CNTL"          , model.get("DLV_CNTL"));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    
                    dao.deleteDown(modelDt);
                    dao.insert(modelDt);
                    
                    m.put("MSG", MessageResolver.getMessage("insert.success"));
                    m.put("errCnt", errCnt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    
                    dao.update(modelDt);
                    
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	
                	Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "LOC");
					modelSP.put("I_CODE", model.get("LOC_ID" + i));				
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }                    
                    dao.deleteUp(modelDt);
                    
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);                    
                    throw new Exception(MessageResolver.getMessage("save.error"));                
                }
            }
        } catch (BizException be) {
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : restore
     * Method 설명 : 로케이션 복구
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> restore(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
            try{
            	 for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                     Map<String, Object> modelDt = new HashMap<String, Object>();
                     modelDt.put("UPD_NO"            , model.get("SS_USER_NO"));
                     modelDt.put("LOC_ID"            , model.get("LOC_ID_"+i));
                     dao.deleteRestore(modelDt);
            	}
                m.put("MSG", MessageResolver.getMessage("update.success"));
                m.put("errCnt", errCnt);
            } catch(Exception e){
                throw e;
            }
        return m;
    }
    
    /**
     * Method ID : saveSub
     * Method 설명 : 출고통제 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
            try{
                if("Y".equals(model.get("DLV_CNTL"))){
                    dao.insertSubDlv(model);
                    
                }else if("N".equals(model.get("DLV_CNTL"))){
                    dao.insertSub(model);
                    
                }else{
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                    m.put("errCnt", "1");
                }
                
                m.put("MSG", MessageResolver.getMessage("insert.success"));
                m.put("errCnt", errCnt);
            } catch(Exception e){
                throw e;
            }
        return m;
    }
    /**
     * Method ID : listExcel
     * Method 설명 : 물류센터정보관리 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
     
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            //상품군
            map.put("ITEMGRP", dao.selectItem(model));
            //적재구분
            model.put("inKey", "ITM05");
            map.put("CarryType", dao.selectCarryType(model));
            //loc구분 
            model.put("inKey", "LOC01");
            map.put("LocType", dao.selectLocType(model));
            //사용상태 
            model.put("inKey", "LOC04");
            map.put("LocStat", dao.selectStat(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : saveCsv
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{
            	if(insertCnt > 0){
            		String[] locCd         = new String[insertCnt];
                	String[] rowCnt         = new String[insertCnt];
                	String[] col         = new String[insertCnt];
                	String[] dan         = new String[insertCnt];
                	String[] maxDan         = new String[insertCnt];
                	
                	String[] locType         = new String[insertCnt];
                	String[] locStat         = new String[insertCnt];
                	String[] locItemType         = new String[insertCnt];
                	String[] whId         = new String[insertCnt];
                	String[] custId         = new String[insertCnt];
                	
                	String[] itemId         = new String[insertCnt];
                	String[] shipAbc         = new String[insertCnt];
                	String[] cellW         = new String[insertCnt];
                	String[] cellH         = new String[insertCnt];
                	String[] cellL         = new String[insertCnt];
                	
                	String[] opQty         = new String[insertCnt];
                	String[] rsQty         = new String[insertCnt];
                	String[] ItemGrp         = new String[insertCnt];
                	String[] carryType         = new String[insertCnt];
                	String[] workRank         = new String[insertCnt];
                	
                	String[] carryPltQty         = new String[insertCnt];
                	String[] locAblePltQty         = new String[insertCnt];
                	String[] delYn         = new String[insertCnt];
                	String[] dlvCntl         = new String[insertCnt];
                	String[] tagPrefix         = new String[insertCnt];
                	
                	String[] locGlnCd         = new String[insertCnt];
                	String[] locBarcodeCd         = new String[insertCnt];
                	String[] positionX         = new String[insertCnt];
                	String[] positionY         = new String[insertCnt];
                	String[] positionZ         = new String[insertCnt];
                	
                	String[] rackDirection         = new String[insertCnt];
                	
                	if(String.valueOf(model.get("uploadType")).equals("SIMPLE")){
                		// 간편 업로드
                		for (int i=0;i<list.size();i++) {
                			Map<String, String> temp = (Map)list.get(i);
                			locCd[i] = (String)temp.get("LOC_CD");
                        	rowCnt[i] = null;
                        	col[i] = null;
                        	dan[i] = null;
                        	maxDan[i] = null;
                        	
                        	locType[i] = (String)temp.get("LOC_TYPE");
                        	locStat[i] = (String)temp.get("LOC_STAT");
                        	locItemType[i] = (String)temp.get("LOC_ITEM_TYPE");
                        	// 기존 : WMSMS040에서 LC_ID로 WH_CD를 찾은 리스트와 엑셀의 WH_ID가 있는지 비교 후 없으면 continue,
                        	//      있으면 FNC_USER_CODE2('WHID', #LC_ID#, #WH_CD#)로 insert
                        	//      -> SIMPLE은 WH_CD를 입력 하기 때문에, ID를 검색해서 찾아야함.
                        	temp.put("vrLcId",(String)model.get("SS_SVC_NO"));
                        	String WH_ID = dao.selectwhcdtoid(temp);
                        	if(WH_ID == null || WH_ID.equals("")){
                        		m.put("errCnt", 1);
                            	m.put("MSG_ORA", "");
                                m.put("MSG", MessageResolver.getMessage("엑셀저장실패 Exception: 유효하지않은 창고코드 값이 있습니다.", new String[]{String.valueOf(insertCnt)}) );
                                return m;
                        	}
                        	whId[i] = WH_ID;
                        	custId[i] = (String)temp.get("CUST_ID");
                        	
                        	itemId[i] = null;
                        	shipAbc[i] = null;
                        	cellW[i] = null;
                        	cellH[i] = null;
                        	cellL[i] = null;
                        	
                        	opQty[i] = (String)temp.get("OP_QTY");
                        	rsQty[i] = null;
                        	ItemGrp[i] = null;
                        	carryType[i] = (String)temp.get("CARRY_TYPE");
                        	workRank[i] = null;
                        	
                        	//기존 : LOC_ABLE_PLT_QTY, CARRY_PLT_QTY가 엑셀의 CARRY_PLT_QTY에서 같은 값 insert
                        	carryPltQty[i] = (String)temp.get("CARRY_PLT_QTY");
                        	locAblePltQty[i] = (String)temp.get("CARRY_PLT_QTY");
                        	delYn[i] = "N";
                        	dlvCntl[i] = null;
                        	tagPrefix[i] = null;
                        	
                        	locGlnCd[i] = null;
                        	locBarcodeCd[i] = (String)temp.get("LOC_BARCODE_CD");
                        	positionX[i] = null;
                        	positionY[i] = null;
                        	positionZ[i] = null;
                        	
                        	rackDirection[i] = null;
                		}
                		
                	}else if(String.valueOf(model.get("uploadType")).equals("SIMPLE_V2")){
                		// 간편 업로드
                		for (int i=0;i<list.size();i++) {
                			Map<String, String> temp = (Map)list.get(i);
                			locCd[i] = (String)temp.get("LOC_CD");
                			rowCnt[i] = (String)temp.get("ROWCNT");
                        	col[i] = (String)temp.get("COL");
                        	dan[i] = (String)temp.get("DAN");
                        	maxDan[i] = null;
                        	
                        	locType[i] = (String)temp.get("LOC_TYPE");
                        	locStat[i] = (String)temp.get("LOC_STAT");
                        	locItemType[i] = (String)temp.get("LOC_ITEM_TYPE");
                        	// 기존 : WMSMS040에서 LC_ID로 WH_CD를 찾은 리스트와 엑셀의 WH_ID가 있는지 비교 후 없으면 continue,
                        	//      있으면 FNC_USER_CODE2('WHID', #LC_ID#, #WH_CD#)로 insert
                        	//      -> SIMPLE은 WH_CD를 입력 하기 때문에, ID를 검색해서 찾아야함.
                        	temp.put("vrLcId",(String)model.get("SS_SVC_NO"));
                        	String WH_ID = dao.selectwhcdtoid(temp);
                        	if(WH_ID == null || WH_ID.equals("")){
                        		m.put("errCnt", 1);
                            	m.put("MSG_ORA", "");
                                m.put("MSG", MessageResolver.getMessage("엑셀저장실패 Exception: 유효하지않은 창고코드 값이 있습니다.", new String[]{String.valueOf(insertCnt)}) );
                                return m;
                        	}
                        	whId[i] = WH_ID;
                        	custId[i] = (String)temp.get("CUST_ID");
                        	
                        	itemId[i] = null;
                        	shipAbc[i] = null;
                        	cellW[i] = null;
                        	cellH[i] = null;
                        	cellL[i] = null;
                        	
                        	opQty[i] = (String)temp.get("OP_QTY");
                        	rsQty[i] = null;
                        	ItemGrp[i] = null;
                        	carryType[i] = (String)temp.get("CARRY_TYPE");
                        	workRank[i] = null;
                        	
                        	//기존 : LOC_ABLE_PLT_QTY, CARRY_PLT_QTY가 엑셀의 CARRY_PLT_QTY에서 같은 값 insert
                        	carryPltQty[i] = (String)temp.get("CARRY_PLT_QTY");
                        	locAblePltQty[i] = (String)temp.get("CARRY_PLT_QTY");
                        	delYn[i] = "N";
                        	dlvCntl[i] = null;
                        	tagPrefix[i] = null;
                        	
                        	locGlnCd[i] = null;
                        	locBarcodeCd[i] = (String)temp.get("LOC_BARCODE_CD");
                        	positionX[i] = null;
                        	positionY[i] = null;
                        	positionZ[i] = null;
                        	
                        	rackDirection[i] = null;
                		}
                		
                	}else{
                		for (int i=0;i<list.size();i++) {
                			Map<String, String> temp = (Map)list.get(i);
                			locCd[i] = (String)temp.get("LOC_CD");
                        	rowCnt[i] = (String)temp.get("ROWCNT");
                        	col[i] = (String)temp.get("COL");
                        	dan[i] = (String)temp.get("DAN");
                        	maxDan[i] = (String)temp.get("MAX_DAN");
                        	
                        	locType[i] = (String)temp.get("LOC_TYPE");
                        	locStat[i] = (String)temp.get("LOC_STAT");
                        	locItemType[i] = (String)temp.get("LOC_ITEM_TYPE");
                        	whId[i] = (String)temp.get("WH_ID");
                        	custId[i] = (String)temp.get("CUST_ID");
                        	
                        	itemId[i] = (String)temp.get("ITEM_ID");
                        	shipAbc[i] = (String)temp.get("SHIP_ABC");
                        	cellW[i] = (String)temp.get("CELL_W");
                        	cellH[i] = (String)temp.get("CELL_H");
                        	cellL[i] = (String)temp.get("CELL_L");
                        	
                        	opQty[i] = (String)temp.get("OP_QTY");
                        	rsQty[i] = (String)temp.get("RS_QTY");
                        	ItemGrp[i] = (String)temp.get("ITEM_GRP");
                        	carryType[i] = (String)temp.get("CARRY_TYPE");
                        	workRank[i] = (String)temp.get("WORK_RANK");
                        	
                        	carryPltQty[i] = (String)temp.get("CARRY_PLT_QTY");
                        	locAblePltQty[i] = (String)temp.get("CARRY_PLT_QTY");
                        	delYn[i] = "N";
                        	dlvCntl[i] = (String)temp.get("DLV_CNTL");
                        	tagPrefix[i] = (String)temp.get("TAG_PREFIX");
                        	
                        	locGlnCd[i] = (String)temp.get("LOC_GLN_CD");
                        	locBarcodeCd[i] = (String)temp.get("LOC_BARCODE_CD");
                        	positionX[i] = (String)temp.get("POSITION_X");
                        	positionY[i] = (String)temp.get("POSITION_Y");
                        	positionZ[i] = (String)temp.get("POSITION_Z");
                        	
                        	rackDirection[i] = (String)temp.get("RACK_DIRECTION");
                		}
                		
                	}
                	m.put("vrLcId", model.get("SS_SVC_NO"));
                	
                	m.put("locCd", locCd);
                	m.put("rowCnt", rowCnt);
                	m.put("col", col);
                	m.put("dan", dan);
                	m.put("maxDan", maxDan);
                	
                	m.put("locType", locType);
                	m.put("locStat", locStat);
                	m.put("locItemType", locItemType);
                	m.put("whId", whId);
                	m.put("custId", custId);
                	
                	m.put("itemId", itemId);
                	m.put("shipAbc", shipAbc);
                	m.put("cellW", cellW);
                	m.put("cellH", cellH);
                	m.put("cellL", cellL);
                	
                	m.put("opQty", opQty);
                	m.put("rsQty", rsQty);
                	m.put("ItemGrp", ItemGrp);
                	m.put("carryType", carryType);
                	m.put("workRank", workRank);
                	
                	m.put("carryPltQty", carryPltQty);
                	m.put("locAblePltQty", locAblePltQty);
                	m.put("delYn", delYn);
                	m.put("dlvCntl", dlvCntl);
                	m.put("tagPrefix", tagPrefix);
                	
                	m.put("locGlnCd", locGlnCd);
                	m.put("locBarcodeCd", locBarcodeCd);
                	m.put("positionX", positionX);
                	m.put("positionY", positionY);
                	m.put("positionZ", positionZ);
                	
                	m.put("rackDirection", rackDirection);
                	
                	m.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                m.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                
                	m = (Map<String, Object>)dao.saveLocProc(m);
                	
                	ServiceUtil.isValidReturnCode("WMSMS080", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
            	}
            	
            	m.put("errCnt", 0);
            	m.put("MSG_ORA", "");
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }    
}
