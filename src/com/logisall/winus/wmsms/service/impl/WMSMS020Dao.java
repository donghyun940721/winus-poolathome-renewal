package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS020Dao")
public class WMSMS020Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 센터별작업정책
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public List<Map<String, Object>> listE1(Map<String, Object> model) {
        return executeQueryForList("wmsms020.search.e1", model);
    }  
    
    
    public String getPropertInfo(Map<String, Object> model){
        return  (String)executeQueryForObject("wmsms020.searchProperty", model);
    }
    
    public Object updatePropertInfo(Map<String, Object> model){
        return  executeQueryForObject("wmsms020.pk_wmsms020.sp_set_manage_plt", model);
    }
    
    public Object updateConfig(Map<String, Object> model){
        return  executeQueryForObject("wmsms020.pk_wmsms020.sp_set_config", model);
    }
    
    /**
     * Method ID : update
     * Method 설명 : 센터별작업정책 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public int update(Map<String, Object> model) {
        return executeUpdate("wmsms020.update", model);
    }
    
   
}
