package com.logisall.winus.wmsms.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS130Dao")
public class WMSMS130Dao extends SqlMapAbstractDAO{

    /**
     * Method ID : list 
     * Method 설명 : 마감관리 조회 
     * 작성자 : kwt
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms130.list", model);
    }    
    /**
     * Method ID : insert 
     * Method 설명 : 마감관리 등록
     * 작성자 : kwt
     * @param   model
     * @return
     */
    public String[] saveCloseInfo(Map<String, Object> model) {
        String[] val = {"", "0"};
        executeUpdate("wmsms130.pk_wmsms130.sp_save_close", model);
        int err = Integer.parseInt(model.get("O_MSG_CODE").toString());
        if(err != 0){
            val[0] = model.get("O_MSG_NAME").toString();
            val[1] = model.get("O_MSG_CODE").toString();
        }
        return val;
    }
    
    /**
     * Method ID : delete 
     * Method 설명 : 마감관리 삭제 
     * 작성자 : kwt
     * @param   model
     * @return
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmsms130.delete", model);
    }
    
}
