package com.logisall.winus.wmsms.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS084Dao")
public class WMSMS084Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 로케이션별 입고상품등록  조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
//    public GenericResultSet list(Map<String, Object> model) {
//        return executeQueryPageWq("wmsms084.search", model);
//    }   
	/**
     * Method ID : list
     * Method 설명 : 로케이션별 입고상품등록  조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
	public GenericResultSet list(Map<String, Object> model) {
	      return executeQueryPageWq("wmsms140.list", model);
	  }   
    /**
     * Method ID : insert
     * Method 설명 : 로케이션별 입고상품등록 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model){
        return executeInsert("wmsms084.insert", model);
    }
    
    /**
     * Method ID : update
     * Method 설명 :  로케이션별 입고상품등록 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model){
        return executeUpdate("wmsms084.update", model);
    }
    
    

    /**
     * Method ID : delete
     * Method 설명 :  로케이션별 입고상품등록 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object delete(Map<String, Object> model){
        return executeUpdate("wmsms084.delete", model);
    }
    
    
}
