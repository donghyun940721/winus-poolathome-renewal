package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSPL020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSPL020Service")
public class WMSPL020ServiceImpl implements WMSPL020Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSPL020Dao")
    private WMSPL020Dao dao;
    
    /**
     * 대체 Method ID    : selectBox
     * 대체 Method 설명      : 물류용기관리 화면 데이타셋
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ZONE", dao.selectZone(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID    : list
     * 대체 Method 설명      : 물류용기관리 조회
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
      
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 물류용기관리 저장
     * 작성자                      : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                //
                String itemGrpId = (String)model.get("ITEM_GRP_ID"+i);
                
                //상품군화
                Map<String, Object> modelItem = new HashMap<String, Object>();
                modelItem.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));
                modelItem.put("SS_CLIENT_IP"  , model.get(ConstantIF.SS_CLIENT_IP));    
                modelItem.put("SS_SVC_NO"     , model.get(ConstantIF.SS_SVC_NO));    
                
                modelItem.put("ITEM_GRP_NM"   , model.get("POOL_GRP_NM"+i));
                modelItem.put("ITEM_GRP_CD"   , model.get("POOL_GRP_CD"+i));
                modelItem.put("IN_ZONE_ID"    , model.get("IN_ZONE_ID"+i));
                modelItem.put("ITEM_GRP_TYPE" , model.get("vrItemGrpType"));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    itemGrpId = (String)dao.insertIg(modelItem);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    modelItem.put("ITEM_GRP_ID", itemGrpId);
                    dao.updateIg(modelItem);
                }
                
                //용기군저장
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));    
                modelDt.put("SS_CLIENT_IP"  , model.get(ConstantIF.SS_CLIENT_IP));    
                modelDt.put("SS_SVC_NO"     , model.get(ConstantIF.SS_SVC_NO));     //LC_ID
                
                modelDt.put("selectIds"     , model.get("selectIds"));
                modelDt.put("ST_GUBUN"      , model.get("ST_GUBUN"+i));
                modelDt.put("POOL_GRP_ID"   , model.get("POOL_GRP_ID"+i));        //INSERT -> SEQ   
                modelDt.put("POOL_GRP_CD"   , model.get("POOL_GRP_CD"+i));           
                modelDt.put("POOL_GRP_NM"   , model.get("POOL_GRP_NM"+i));
                modelDt.put("POOL_WGT_ARROW_RATE"   , model.get("POOL_WGT_ARROW_RATE"+i)); 
                modelDt.put("IN_ZONE_ID"    , model.get("IN_ZONE_ID"+i));         
                
                modelDt.put("ITEM_GRP_ID"   , itemGrpId);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	
                	Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "RITEMGRP");
					modelSP.put("I_CODE", itemGrpId); 			
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }
                	
                    dao.delete(modelDt);
                    dao.deleteIg(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
                
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 엑셀다운
     * 작성자                      : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
	/**
	 * Method ID : saveUploadData 
	 * Method 설명 : 엑셀파일 저장 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[] { String.valueOf(insertCnt) }));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);

		} catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}     
}
