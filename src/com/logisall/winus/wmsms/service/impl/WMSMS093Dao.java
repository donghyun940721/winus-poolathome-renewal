package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS093Dao")
public class WMSMS093Dao extends SqlMapAbstractDAO{   
    /**
     * Method ID    : list
     * Method 설명      : 상품별물류기기 목록 조회
     * 작성자                 : kwt
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms093.list", model);
    }    
    /**
     * Method ID    : insert
     * Method 설명      : 상품별물류기기 등록
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object start_insert(Map<String, Object> model) {
        return executeInsert("wmsms093.start_insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 상품별물류기기 수정
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object start_update(Map<String, Object> model) {
        return executeUpdate("wmsms093.start_update", model);
    }   
    
    /**
     * Method ID    : update
     * Method 설명      : 상품별물류기기 수정
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object stop_update(Map<String, Object> model) {
        return executeUpdate("wmsms093.stop_update", model);
    }
    
    /**
     * Method ID    : update
     * Method 설명      : 상품별물류기기 수정
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object end_update(Map<String, Object> model) {
        return executeUpdate("wmsms093.end_update", model);
    }
    
    /**
     * Method ID    : autoOrderInsert
     * Method 설명      : 주문입력
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object autoOrderInsert(Map<String, Object> model){
        executeUpdate("wmsms093.PK_WMSOP030.SP_OUT_AUTO_ORDER_INSERT", model);
        return model;
    }
}
