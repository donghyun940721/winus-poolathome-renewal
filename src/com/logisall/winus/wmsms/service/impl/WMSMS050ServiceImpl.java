package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DESUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS050Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS050Service")
public class WMSMS050ServiceImpl implements WMSMS050Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS050Dao")
    private WMSMS050Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 매출거래처정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {          
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
  
    /**
     * Method ID : save
     * Method 설명 : 매출거래처정보 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save_bk(Map<String, Object> model) throws Exception {
    	
    	log.info(model);
    	
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            if(iuCnt > 0){
                
                String[] carId = new String[iuCnt];
                String[] carCd = new String[iuCnt];
                String[] carTon = new String[iuCnt];
                String[] carGb = new String[iuCnt];
                String[] carType = new String[iuCnt];
                
                String[] transCd = new String[iuCnt];
                String[] drvTel = new String[iuCnt];
                String[] dockNo = new String[iuCnt];
                String[] lcId = new String[iuCnt];
                String[] useYn = new String[iuCnt];
                
                String[] delYn = new String[iuCnt];
                String[] transNm = new String[iuCnt];
                String[] drvNm = new String[iuCnt];
                
                String[] carBarCd = new String[iuCnt];

                String workIp = "";
                String userNo = "";
                
                workIp = (String)model.get(ConstantIF.SS_CLIENT_IP);
                userNo = (String)model.get(ConstantIF.SS_USER_NO);
                
                for(int i = 0 ; i < iuCnt ; i ++){
                    carId[i]      = (String)model.get("CAR_ID"+i);
                    carCd[i]      = (String)model.get("CAR_CD"+i);
                    carTon[i]     = (String)model.get("CAR_TON"+i);
                    carGb[i]      = (String)model.get("CAR_GB"+i);
                    carType[i]    = (String)model.get("CAR_TYPE"+i);
                    
                    transCd[i]    = (String)model.get("TRANS_CD"+i);
                    drvTel[i]     = (String)model.get("DRV_TEL"+i);
                    dockNo[i]     = (String)model.get("DOCK_NO"+i);
                    lcId[i]       = (String)model.get("SS_SVC_NO");
                    useYn[i]      = (String)model.get("USE_YN"+i);
                    
                    transNm[i]    = (String)model.get("TRANS_NM"+i);
                    drvNm[i]      = (String)model.get("DRV_NM"+i);
                    carBarCd[i]   = (String)model.get("CAR_BAR_CD"+i);
                    
                    if("DELETE".equals(model.get("ST_GUBUN"+i))){
                        delYn[i]       = "Y";
                        
                        Map<String, Object> modelSP = new HashMap<String, Object>();
    					modelSP.put("I_TYPE", "CAR");
    					modelSP.put("I_CODE", model.get("CAR_ID"+i));				
    					String checkExistData = dao.checkExistData(modelSP);
    					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
    						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
    								MessageResolver.getMessage("delete.exist." + checkExistData)
    						}) );
                        }
                    }else{
                        delYn[i]       = (String)model.get("DEL_YN"+i);
                    }
                    
                    if( "INSERT".equals(model.get("ST_GUBUN"+i)) ){
                        model.put("CAR_CODE", carCd[i]);
                        int chek = dao.selectCarchek(model);
                        if(chek == 1){
                            // 중복값중에 삭제된게 아니면 중복이라고 메시지 띠우기
                            errCnt++;
                            m.put("errCnt", errCnt);
                            throw new BizException(MessageResolver.getMessage("chek.ritemid"));
                        }
                    }
                }
                Map<String, Object> modelSP = new HashMap<String, Object>();
                
                modelSP.put("I_CAR_ID"		, carId);
                modelSP.put("I_CAR_CD"		, carCd);
                modelSP.put("I_CAR_TON"		, carTon);
                modelSP.put("I_CAR_GB"		, carGb);
                modelSP.put("I_CAR_TYPE"	, carType);
                
                modelSP.put("I_TRANS_CD"	, transCd);
                modelSP.put("I_DRV_TEL"		, drvTel);
                modelSP.put("I_DOCK_NO"		, dockNo);
                modelSP.put("I_LC_ID"		, lcId);
                modelSP.put("I_USE_YN"		, useYn);
                
                modelSP.put("I_DEL_YN"		, delYn);
                modelSP.put("I_TRANS_NM"	, transNm);
                modelSP.put("I_DRV_NM"		, drvNm);
                modelSP.put("I_CAR_BAR_CD"	, carBarCd);
                
                modelSP.put("I_WORK_IP"		, workIp);
                modelSP.put("I_USER_NO"		, userNo);
                
                dao.saveCar(modelSP);
                m.put("MSG", MessageResolver.getMessage("save.success"));
                m.put("errCnt", errCnt);
            }
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 차량정보 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
     
    
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트박스 생성
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            model.put("inKey", "CAR02");
            map.put("CAR02", dao.selectCar02(model));   
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
	/**
	 * Method ID : saveUploadData 
	 * Method 설명 : 엑셀파일 저장 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[] { String.valueOf(insertCnt) }));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);

		} catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}    
	
	/**
     * Method ID : driver list
     * Method 설명 : 기사님 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> driverList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {    
        	map.put("LIST", dao.driverList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : batchSave
     * Method 설명 : 배차 저장
     * 작성자 : khkim
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> dspSave(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
	        try{
	            
	        	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	            if(tmpCnt > 0){
		        	
		            String[] ordId = new String[tmpCnt];
		            String[] ordSeq = new String[tmpCnt];

		            String[] ordType = new String[tmpCnt];
		            String[] ritemId = new String[tmpCnt];
		            String[] uomId = new String[tmpCnt];
		            String[] ordQty = new String[tmpCnt];
		            String[] transCd = new String[tmpCnt];
		            
		            String[] dspRout = new String[tmpCnt];
		            String[] carTon = new String[tmpCnt];
		            String[] drvNm = new String[tmpCnt];
		            String[] drvTel = new String[tmpCnt];
		            String[] carCd = new String[tmpCnt];
		           		            
		            String[] dspPayCharge = new String[tmpCnt];
		            String[] expPltQty = new String[tmpCnt];
		            String[] upAmt = new String[tmpCnt];
		            String[] downAmt = new String[tmpCnt];
		            String[] etcAmt = new String[tmpCnt];
		            
		            String[] transAmt = new String[tmpCnt];
		            String[] waitAmt = new String[tmpCnt];
		            String[] deviceAmt = new String[tmpCnt];
		            String[] dspMemo = new String[tmpCnt];
		            String[] delYn = new String[tmpCnt];
		            
		            for(int i = 0; i < tmpCnt; i++){
		            	ordId[i]    = (String)model.get("ORD_ID_" + i);
		            	ordSeq[i]    = (String)model.get("ORD_SEQ_" + i);
		            	
		            	ordType[i] = (String)model.get("ORD_TYPE_" + i);
			            ritemId[i] = (String)model.get("RITEM_ID_" + i);
			            uomId[i] = (String)model.get("UOM_ID_" + i);
			            ordQty[i] = (String)model.get("ORD_QTY_" + i);
			            transCd[i] = (String)model.get("TRANS_CUST_CD_" + i);
			            
			            dspRout[i] = (String)model.get("DSP_ROUT_" + i);
			            carTon[i] = (String)model.get("CAR_TON_" + i);
			            drvNm[i] = (String)model.get("DRV_NM_" + i);
			            drvTel[i] = (String)model.get("DRV_TEL_" + i);
			            carCd[i] = (String)model.get("CAR_CD_" + i);
			           		            
			            dspPayCharge[i] = (String)model.get("DSP_PAY_CHARGE_" + i);
			            expPltQty[i] = (String)model.get("EXP_PLT_QTY_" + i);
			            upAmt[i] = (String)model.get("UP_AMT_" + i);
			            downAmt[i] = (String)model.get("DOWN_AMT_" + i);
			            etcAmt[i] = (String)model.get("ETC_AMT_" + i);
			            
			            transAmt[i] = (String)model.get("TRANS_AMT_" + i);
			            waitAmt[i] = (String)model.get("WAIT_AMT_" + i);
			            deviceAmt[i] = (String)model.get("DEVICE_AMT_" + i);
			            dspMemo[i] = (String)model.get("DSP_MEMO_" + i);
		            	delYn[i]    = (String)model.get("DEL_YN_" + i);
		            }
		            // 프로시져에 보낼것들 다담는다
		            Map<String, Object> modelIns = new HashMap<String, Object>();
	
		            modelIns.put("ordId", ordId);
		            modelIns.put("ordSeq", ordSeq);
	
		            modelIns.put("ordType", ordType);
		            modelIns.put("ritemId", ritemId);
		            modelIns.put("uomId", uomId);
		            modelIns.put("ordQty", ordQty);
		            modelIns.put("transCd", transCd);
		            
		            modelIns.put("dspRout", dspRout);
		            modelIns.put("carTon", carTon);
		            modelIns.put("drvNm", drvNm);
		            modelIns.put("drvTel", drvTel);
		            modelIns.put("carCd", carCd);
		            
		            modelIns.put("dspPayCharge", dspPayCharge);
		            modelIns.put("expPltQty", expPltQty);
		            modelIns.put("upAmt", upAmt);
		            modelIns.put("downAmt", downAmt);
		            modelIns.put("etcAmt", etcAmt);
		            
		            modelIns.put("transAmt", transAmt);
		            modelIns.put("waitAmt", waitAmt);
		            modelIns.put("deviceAmt", deviceAmt);
		            modelIns.put("dspMemo", dspMemo);
		            modelIns.put("delYn", delYn);
		            
		            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
		            modelIns.put("CUST_ID", "");
	                modelIns.put("DSP_ID", "");
		            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
		            // dao
		            modelIns = (Map<String, Object>)dao.dspSave(modelIns);
		            ServiceUtil.isValidReturnCode("WMSMS050", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
	            }
	  	    	
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("list.success"));
	            
	        } catch(BizException be) {
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", be);
				}       	
	            m.put("errCnt", 1);
	            m.put("MSG", be.getMessage() );
	            
	        }catch(Exception e){
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", e);
				} 
	            m.put("errCnt", 1);
	            m.put("MSG", MessageResolver.getMessage("save.error") );
	        }
	        return m;
    }
    
    /**
     * Method ID : dspCancel
     * Method 설명 : 배차 저장
     * 작성자 : khkim
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> dspCancel(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
	        try{
	            
	        	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
	            if(tmpCnt > 0){
		        	
		            String[] ordId = new String[tmpCnt];
		            String[] ordSeq = new String[tmpCnt];

		            String[] ordType = new String[tmpCnt];
		            String[] ritemId = new String[tmpCnt];
		            String[] uomId = new String[tmpCnt];
		            String[] ordQty = new String[tmpCnt];
		            String[] transCd = new String[tmpCnt];
		            
		            String[] dspRout = new String[tmpCnt];
		            String[] carTon = new String[tmpCnt];
		            String[] drvNm = new String[tmpCnt];
		            String[] drvTel = new String[tmpCnt];
		            String[] carCd = new String[tmpCnt];
		           		            
		            String[] dspPayCharge = new String[tmpCnt];
		            String[] expPltQty = new String[tmpCnt];
		            String[] upAmt = new String[tmpCnt];
		            String[] downAmt = new String[tmpCnt];
		            String[] etcAmt = new String[tmpCnt];
		            
		            String[] transAmt = new String[tmpCnt];
		            String[] waitAmt = new String[tmpCnt];
		            String[] deviceAmt = new String[tmpCnt];
		            String[] dspMemo = new String[tmpCnt];
		            String[] delYn = new String[tmpCnt];
		            
		            for(int i = 0; i < tmpCnt; i++){
		            	ordId[i]    = (String)model.get("ORD_ID_" + i);
		            	ordSeq[i]    = (String)model.get("ORD_SEQ_" + i);
		            	
		            	ordType[i] = null;
			            ritemId[i] = null;
			            uomId[i] = null;
			            ordQty[i] = null;
			            transCd[i] = null;
			            
			            dspRout[i] = null;
			            carTon[i] = null;
			            drvNm[i] = null;
			            drvTel[i] = null;
			            carCd[i] = null;
			           		            
			            dspPayCharge[i] = null;
			            expPltQty[i] = null;
			            upAmt[i] = null;
			            downAmt[i] = null;
			            etcAmt[i] = null;
			            
			            transAmt[i] = null;
			            waitAmt[i] = null;
			            deviceAmt[i] = null;
			            dspMemo[i] = null;
		            	delYn[i]    = (String)model.get("DEL_YN");
		            }
			            // 프로시져에 보낼것들 다담는다
		            Map<String, Object> modelIns = new HashMap<String, Object>();
	
		            modelIns.put("ordId", ordId);
		            modelIns.put("ordSeq", ordSeq);
	
		            modelIns.put("ordType", ordType);
		            modelIns.put("ritemId", ritemId);
		            modelIns.put("uomId", uomId);
		            modelIns.put("ordQty", ordQty);
		            modelIns.put("transCd", transCd);
		            
		            modelIns.put("dspRout", dspRout);
		            modelIns.put("carTon", carTon);
		            modelIns.put("drvNm", drvNm);
		            modelIns.put("drvTel", drvTel);
		            modelIns.put("carCd", carCd);
		            
		            modelIns.put("dspPayCharge", dspPayCharge);
		            modelIns.put("expPltQty", expPltQty);
		            modelIns.put("upAmt", upAmt);
		            modelIns.put("downAmt", downAmt);
		            modelIns.put("etcAmt", etcAmt);
		            
		            modelIns.put("transAmt", transAmt);
		            modelIns.put("waitAmt", waitAmt);
		            modelIns.put("deviceAmt", deviceAmt);
		            modelIns.put("dspMemo", dspMemo);
		            modelIns.put("delYn", delYn);
	
		            
		            modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
		            modelIns.put("CUST_ID", "");
	                modelIns.put("DSP_ID", "");
		            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
		            // dao
		            modelIns = (Map<String, Object>)dao.dspSave(modelIns);
		            ServiceUtil.isValidReturnCode("WMSMS050", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
		            
	            }
	            
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("list.success"));
	            
	        } catch(BizException be) {
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", be);
				}       	
	            m.put("errCnt", 1);
	            m.put("MSG", be.getMessage() );
	            
	        }catch(Exception e){
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", e);
				} 
	            m.put("errCnt", 1);
	            m.put("MSG", MessageResolver.getMessage("save.error") );
	        }
	        return m;
    }
    
    /**
     * Method ID : dspLoad
     * Method 설명 : 배차정보 로드
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> dspLoad(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {          
            map.put("LIST", dao.dspLoad(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     * Method ID : save
     * Method 설명 : 차량정보 저장 시 사용자 자동 등록 여부 Y일 경우 사용자 등록 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	
    	log.info(model);
    	
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            if(iuCnt > 0){
                
                String[] carId = new String[iuCnt];
                String[] carCd = new String[iuCnt];
                String[] carTon = new String[iuCnt];
                String[] carGb = new String[iuCnt];
                String[] carType = new String[iuCnt];
                
                String[] transCd = new String[iuCnt];
                String[] drvTel = new String[iuCnt];
                String[] dockNo = new String[iuCnt];
                String[] lcId = new String[iuCnt];
                String[] useYn = new String[iuCnt];
                
                String[] delYn = new String[iuCnt];
                String[] transNm = new String[iuCnt];
                String[] drvNm = new String[iuCnt];
                
                String[] carBarCd = new String[iuCnt];
                String[] userSaveYn = new String[iuCnt];
                

                String workIp = "";
                String userNo = "";
                
                workIp = (String)model.get(ConstantIF.SS_CLIENT_IP);
                userNo = (String)model.get(ConstantIF.SS_USER_NO);
                
                for(int i = 0 ; i < iuCnt ; i ++){
                    carId[i]      = (String)model.get("CAR_ID"+i);
                    carCd[i]      = (String)model.get("CAR_CD"+i);
                    carTon[i]     = (String)model.get("CAR_TON"+i);
                    carGb[i]      = (String)model.get("CAR_GB"+i);
                    carType[i]    = (String)model.get("CAR_TYPE"+i);
                    
                    transCd[i]    = (String)model.get("TRANS_CD"+i);
                    drvTel[i]     = (String)model.get("DRV_TEL"+i);
                    dockNo[i]     = (String)model.get("DOCK_NO"+i);
                    lcId[i]       = (String)model.get("SS_SVC_NO");
                    useYn[i]      = (String)model.get("USE_YN"+i);
                    
                    transNm[i]    = (String)model.get("TRANS_NM"+i);
                    drvNm[i]      = (String)model.get("DRV_NM"+i);
                    carBarCd[i]   = (String)model.get("CAR_BAR_CD"+i);
                    
                    if("DELETE".equals(model.get("ST_GUBUN"+i))){
                        delYn[i]       = "Y";
                        
                        Map<String, Object> modelSP = new HashMap<String, Object>();
    					modelSP.put("I_TYPE", "CAR");
    					modelSP.put("I_CODE", model.get("CAR_ID"+i));				
    					String checkExistData = dao.checkExistData(modelSP);
    					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
    						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
    								MessageResolver.getMessage("delete.exist." + checkExistData)
    						}) );
                        }
                    }else{
                        delYn[i]       = (String)model.get("DEL_YN"+i);
                    }
                    
                    if( "INSERT".equals(model.get("ST_GUBUN"+i)) ){
                        model.put("CAR_CODE", carCd[i]);
                        int chek = dao.selectCarchek(model);
                        if(chek == 1){
                            // 중복값중에 삭제된게 아니면 중복이라고 메시지 띠우기
                            errCnt++;
                            m.put("errCnt", errCnt);
                            throw new BizException(MessageResolver.getMessage("chek.ritemid"));
                        }
                    }
                   
                }
                Map<String, Object> modelSP = new HashMap<String, Object>();
                
                modelSP.put("I_CAR_ID"		, carId);
                modelSP.put("I_CAR_CD"		, carCd);
                modelSP.put("I_CAR_TON"		, carTon);
                modelSP.put("I_CAR_GB"		, carGb);
                modelSP.put("I_CAR_TYPE"	, carType);
                
                modelSP.put("I_TRANS_CD"	, transCd);
                modelSP.put("I_DRV_TEL"		, drvTel);
                modelSP.put("I_DOCK_NO"		, dockNo);
                modelSP.put("I_LC_ID"		, lcId);
                modelSP.put("I_USE_YN"		, useYn);
                
                modelSP.put("I_DEL_YN"		, delYn);
                modelSP.put("I_TRANS_NM"	, transNm);
                modelSP.put("I_DRV_NM"		, drvNm);
                modelSP.put("I_CAR_BAR_CD"	, carBarCd);
                
                modelSP.put("I_WORK_IP"		, workIp);
                modelSP.put("I_USER_NO"		, userNo);
                
                dao.saveCar(modelSP);
                for(int i = 0 ; i < iuCnt ; i ++){

                    userSaveYn[i]   = (String)model.get("USER_SAVE_YN"+i);
//                    System.out.println(userSaveYn[i]);
                    
                    if("Y".equals(model.get(("USER_SAVE_YN"+i)))) {
//	                if("Y".equals(model.get(("USER_SAVE_YN"+i))) && "0000001262".equals(model.get(("D_LC_ID"+i))) || "0000002940".equals(model.get(("D_LC_ID"+i)))) {
	                	System.out.println("여기1----");
	                	
	                	Map<String, Object> map = new HashMap<String, Object>();
	                	
	                	if(log.isInfoEnabled()){
	                    	log.info(model);
	                    }
	                    try {
	                        for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
	                        	model.put("USER_PASSWORD", DESUtil.dataEncrypt("a"+model.get("USER_ID"+j)));
	                        	model.put("USER_ID", "a"+model.get("USER_ID"+j));
	                        	model.put("USER_NM", "a"+model.get("USER_NM"+j));
	                        }
	                        
//	                        System.out.println("a"+model.get("USER_ID"));
	                        String isuser = dao.userIdCheck(model);
	                        
	                        if ("0".equals(isuser.substring(0, 1))) {
//	                             1.사용자정보를 등록한다.
	            	            	System.out.println("1.사용자정보를 등록한다.");
	            	            	
	            	        		model.put("AUTH_CD"			, "MA-210");
	            	        		model.put("CLIENT_ID"		, "0000281249");
	            	        		model.put("USER_GB"			, "40");
	            	        		model.put("MULTI_USE_GB"	, "N");
	            	        		
	            	        		dao.insert(model);
	                            
//	                             2.사용자 물류센터를 등록한다.
	                              for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
	                                  Map<String, Object> modelDt = new HashMap<String, Object>();
	                                  modelDt.put("USER_NO"			, model.get("USER_NO"));
	                                  modelDt.put("D_LC_ID"			, model.get("D_LC_ID" +j));
	                                  
	                                  modelDt.put("D_CONNECT_YN"	, "Y");
	                                  modelDt.put("D_USER_LC_SEQ"	, "1");
	                                  
	                                  modelDt.put("SS_CLIENT_IP"	, model.get("SS_CLIENT_IP"));
	                                  modelDt.put("SS_USER_NO"		, model.get("SS_USER_NO"));
	                                  
	                                  System.out.println("2.사용자 물류센터를 등록한다.");
//	                                  System.out.println(modelDt.get("SS_USER_NO"));
	                                  
	                                  modelDt.put("D_ITEM_GRP_ID"	, "1");
	                                  dao.insertDC(modelDt);
	                              }

	                            map.put("MSG", MessageResolver.getMessage("insert.success"));
	                            map.put("USER_NO", model.get("USER_NO"));
	                            map.put("USER_ID", model.get("USER_ID"));
	            //
	                        } else {
	                            map.put("MSG", MessageResolver.getMessage("userid.check"));
	                        }
	                    } catch (Exception e) {
	                        throw e;
	                    }
	                    return map;
	                }
                }
                
                
                m.put("MSG", MessageResolver.getMessage("save.success"));
                m.put("errCnt", errCnt);
            }
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
  
}
