package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS011Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS011Service")
public class WMSMS011ServiceImpl implements WMSMS011Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS011Dao")
    private WMSMS011Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 매출거래처정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {          
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listQ1
     * Method 설명 : 화주 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listQ1(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
  
    /**
     * Method ID : save
     * Method 설명 : 매출거래처정보 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("TRUST_CUST_CD", model.get("TRUST_CUST_CD"+i));
                modelDt.put("TRUST_CUST_ID", model.get("TRUST_CUST_ID"+i));
                modelDt.put("TRUST_CUST_NM", model.get("TRUST_CUST_NM"+i));
                modelDt.put("CUST_CD", model.get("CUST_CD"+i));
                modelDt.put("CUST_NM", model.get("CUST_NM"+i));
                modelDt.put("CUST_ID", model.get("CUST_ID"+i));
                modelDt.put("REP_NM", model.get("REP_NM"+i));
                modelDt.put("BIZ_NO", model.get("BIZ_NO"+i));
                modelDt.put("TEL", model.get("TEL"+i));
                modelDt.put("FAX", model.get("FAX"+i));
                modelDt.put("MOBILE_NO", model.get("MOBILE_NO"+i));
                modelDt.put("ZIP", model.get("ZIP"+i));
                modelDt.put("TAG_PREFIX", model.get("TAG_PREFIX"+i));
                modelDt.put("SGLN_CD", model.get("SGLN_CD"+i));
                modelDt.put("ADDR", model.get("ADDR"+i));
                modelDt.put("E_MAIL", model.get("E_MAIL"+i));
                modelDt.put("E_MAIL_DOMAIN", model.get("E_MAIL_DOMAIN"+i));
                modelDt.put("HTTP", model.get("HTTP"+i));
                modelDt.put("BIZ_COND", model.get("BIZ_COND"+i));
                modelDt.put("BIZ_TYPE", model.get("BIZ_TYPE"+i));
                modelDt.put("CREDIT_GRADE", model.get("CREDIT_GRADE"+i));
                modelDt.put("DLV_CNTL", model.get("DLV_CNTL"+i));
                modelDt.put("REPRESENT_CUST_CD", model.get("REPRESENT_CUST_CD"+i));
                modelDt.put("CUST_STAT", model.get("CUST_STAT"+i));
                modelDt.put("VAT_YN", model.get("VAT_YN"+i));
                modelDt.put("CUST_EPC_CD", model.get("CUST_EPC_CD"+i));                
                modelDt.put("CUST_INOUT_TYPE", model.get("CUST_INOUT_TYPE"+i));                
                
                modelDt.put("DUTY_NM", model.get("DUTY_NM"+i));
                modelDt.put("DUTY_MOBILE", model.get("DUTY_MOBILE"+i));
                modelDt.put("INOUT_ZONE_ID", model.get("INOUT_ZONE_ID"+i));
                modelDt.put("CUST_BAR_CD", model.get("CUST_BAR_CD"+i));
                modelDt.put("CUST_TYPE", model.get("CUST_TYPE"+i));
                modelDt.put("DELIVERY_TIME", model.get("DELIVERY_TIME"+i));
                
                String bestDateYn = (String)model.get("BEST_DATE_YN"+i);          
                if ( StringUtils.isEmpty(bestDateYn)) {
                	bestDateYn = "N";
                }
                modelDt.put("BEST_DATE_YN", bestDateYn);
                modelDt.put("CUST_GRP_ID", model.get("CUST_GRP_ID"+i));
                modelDt.put("BIZ_MEMO", model.get("BIZ_MEMO"+i));
                String asnYn = (String)model.get("ASN_YN"+i);
                if ( !StringUtils.isEmpty(asnYn) && "Y".equals(asnYn) ) {
	                modelDt.put("ASN_YN", model.get("ASN_YN"+i));
	                modelDt.put("ASN_IN_CUST_ID", model.get("ASN_IN_CUST_ID"+i));
	                modelDt.put("ASN_IN_LC_ID", model.get("ASN_IN_LC_ID"+i));
	                modelDt.put("ASN_IN_TRUST_CUST_ID", model.get("ASN_IN_TRUST_CUST_ID"+i));                
	                modelDt.put("ASN_TYPE", model.get("ASN_TYPE"+i));
	                modelDt.put("ASN_MAKE_TYPE", model.get("ASN_MAKE_TYPE"+i));
	                
	                int checkCnt1 = dao.checkTrustCustId(modelDt);
                    if ( checkCnt1 < 1 ){
                        throw new BizException( MessageResolver.getMessage("등록값없음", new String[]{MessageResolver.getMessage("ASN입고화주ID")}) );
                    }
                    int checkCnt2 = dao.checkCustId(modelDt);
                    if ( checkCnt2 < 1 ){
                        throw new BizException( MessageResolver.getMessage("등록값없음", new String[]{MessageResolver.getMessage("ASN입고거래처ID")}) );
                    }
                    int checkCnt3 = dao.checkLcId(modelDt);
                    if ( checkCnt3 < 1 ){
                        throw new BizException( MessageResolver.getMessage("등록값없음", new String[]{MessageResolver.getMessage("ASN입고물류센터ID")}) );
                    }	                
	                
                } else {
                	modelDt.put("ASN_YN", "N");
                }
                
                modelDt.put("LC_ID", model.get("SS_SVC_NO"));
                modelDt.put("REG_NO", model.get("SS_USER_NO"));
                modelDt.put("UPD_NO", model.get("SS_USER_NO"));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    
                	int checkCdCnt = dao.checkTxtCustId(modelDt);
                    if(checkCdCnt > 0){
                    	throw new BizException("해당거래처 코드 값 존재");
                    }
                	
                    String wmsms010CustId = (String) dao.insertWmsms010(modelDt);
                    // System.out.println(modelDt);
                    dao.insertWmsms011(modelDt);
                    // dao.wmsms012_insert(modelDt); // 2015.10.08 물류센터별화주에는 INSERT할 필요 없음
                    
                    //** 거래처ZONE정보관리 연동 저장 1:1
                    Map<String, Object> modelDtMs085Header = new HashMap<String, Object>();
                    modelDtMs085Header.put("SS_SVC_NO"		, model.get("SS_SVC_NO"));
                    modelDtMs085Header.put("CUST_ZONE_NM"	, model.get("CUST_NM"+i));
                    modelDtMs085Header.put("SS_USER_NO"		, model.get("SS_USER_NO"));
                    
                    String checkLcClientOrdYn = dao.checkLcClientOrdYn(modelDtMs085Header);
                    //System.out.println("checkLcClientOrdYn : " + checkLcClientOrdYn);
                    if(checkLcClientOrdYn.equals("Y")){
                    	//** 거래처ZONE정보관리 연동 저장 1:1 wmsms085 header insert
                    	String custZoneId = (String) dao.insertMs085Header(modelDtMs085Header);
                    	//System.out.println("custZoneId : " + custZoneId);
                        Map<String, Object> modelDtMs085Detail = new HashMap<String, Object>();
                        modelDtMs085Detail.put("WORK_IP"		, model.get("SS_CLIENT_IP"));
                        modelDtMs085Detail.put("CUST_ID"		, wmsms010CustId);
                        modelDtMs085Detail.put("CUST_ZONE_ID"	, custZoneId);
                        modelDtMs085Detail.put("SS_USER_NO"		, model.get("SS_USER_NO"));
                        
                        //** 거래처ZONE정보관리 연동 저장 1:1 wmsms085 detail insert
                        dao.insertMs085Detail(modelDtMs085Detail);
                    }
                    
                    m.put("MSG", MessageResolver.getMessage("insert.success"));
                    m.put("errCnt", errCnt);
                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    dao.updateWmsms011(modelDt);
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.deleteWmsms010(modelDt);
                    dao.deleteWmsms011(modelDt);
                    // dao.delete_wmsms012(modelDt);	// 2015.10.08 물류센터별화주에는 INSERT할 필요 없음
                    
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);
                    
                }else{
                    errCnt++;
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);                    
                    throw new Exception(MessageResolver.getMessage("save.error"));                
                }
            }
        } catch (BizException be) {
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 매출거래처정보 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
     
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀업로드 저장
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.saveUploadData(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
    
    /**
     * Method ID : saveLcSync
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveLcSync(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String[] spLcSyncArrData = model.get("MULIT_LC_ID_ARR").toString().split(",");
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            Map<String, Object> modelIns = new HashMap<String, Object>();
            if(tmpCnt > 0){
                for(int i = 0 ; i < tmpCnt ; i ++){
                    modelIns.put("I_MULIT_LC_ID"	, spLcSyncArrData);
                    modelIns.put("I_CUST_ID"		, (String)model.get("CUST_ID"+i));
                    modelIns.put("I_CUST_CD"		, (String)model.get("CUST_CD"+i));
                    modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
                    modelIns.put("I_ASN_SET_YN"		, (String)model.get("ASN_SET_YN"+i));
                    modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                    //dao
                    modelIns = (Map<String, Object>)dao.saveLcSync(modelIns);
                }
            }
            ServiceUtil.isValidReturnCode("WMSMS011", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    /**
     * Method ID		: saveCopyTransCust
     * Method 설명 	: 거래처복사
     * 작성자 			: sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCopyTransCust(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();
    		modelDt.put("I_CUST_ID"			, model.get("I_CUST_ID"));
    		modelDt.put("I_CHANGE_CUST_ID"  , model.get("I_CHANGE_CUST_ID"));
    		modelDt.put("I_LC_ID"   		, model.get("I_LC_ID"));
    		modelDt.put("I_USER_NO"			, model.get("I_USER_NO"));
    		modelDt.put("I_WORK_IP"			, model.get("I_WORK_IP"));
    		
    		modelDt = (Map<String, Object>)dao.copyTransCust(modelDt);
    		ServiceUtil.isValidReturnCode("WMSMS011", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    /**
     * Method ID		: saveUploadDataProcedure
     * Method 설명 	: 거래처 엑셀 업로드 (프로시저)
     * 작성자 			: KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadDataProcedure(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.saveUploadDataProcedure(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
}
