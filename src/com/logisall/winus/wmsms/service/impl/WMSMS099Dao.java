package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS099Dao")
public class WMSMS099Dao extends SqlMapAbstractDAO {

    /**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model) {
	return executeQueryForList("wmsms094.selectItemGrp", model);
    }

    

    /**
     * Method ID : listItem Method 설명 : 상품정보 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listItem(Map<String, Object> model) {
	return executeQueryPageWq("wmsms099.listItem", model);
    }
    
    
    /**
     * Method ID : listItem Method 설명 : 상품정보 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listDetailItem(Map<String, Object> model) {
	return executeQueryPageWq("wmsms099.listDetailItem", model);
    }


    /**
     * Method ID : fileUpload
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileUpload(Map<String, Object> model) {
        return executeInsert("wmsms099.fileInsert", model);
    }
    
    
    /**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertInfo(Map<String, Object> model) {
        return executeInsert("wmsms099.itemImgInfoSave", model);
    }
    
    /**
     * Method ID  : insertValidate
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertValidate(Map<String, Object> model){
        return executeQueryForList("wmsms099.insertValidate", model);
    }
    
   
    
    /**
     * Method ID    : saveLcSync
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveLcSync(Map<String, Object> model){
        executeUpdate("wmsms099.pk_wmsms099.sp_save_multi_lc_item", model);
        return model;
    }

    
    
}
