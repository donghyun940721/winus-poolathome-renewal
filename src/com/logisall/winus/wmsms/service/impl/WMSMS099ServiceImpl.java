package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsms.service.WMSMS099Service;
//import com.m2m.jdfw5x.egov.exception.BizException;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS099Service")
public class WMSMS099ServiceImpl implements WMSMS099Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS099Dao")
	private WMSMS099Dao dao;

	private final static String[] CHECK_VALIDATE_WMSMS099 = { "CUST_ID", "REP_UOM_ID" };

	
	/**
     * 대체 Method ID   : selectData
     * 대체 Method 설명    : 상품 목록 필요 데이타셋
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }

	
	/**
	 * 
	 * 대체 Method ID : listItem 대체 Method 설명 : 상품 목록 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listItem(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.listItem(model));
		return map;
	}
	
	
	/**
	 * 
	 * 대체 Method ID : listItem 대체 Method 설명 : 상품 목록 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listDetailItem(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.listDetailItem(model));
		return map;
	}

	
	/**
	 * Method ID : save Method 설명 : 통합 HelpDesk 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveItemImg(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
				Object attachFileInfo = model.get("D_ATCH_FILE_NAME");

				for (int i=0; i< ((String[])attachFileInfo).length; i++){
					Map<String, Object> modelDt = new HashMap<String, Object>();
					
					if(((String[])model.get("D_ATCH_FILE_NAME"))[i].length() > 0){
						// 확장자 잘라내기
						//String str          = (String) model.get("D_ATCH_FILE_NAME");
						//String tranFilename = (String) model.get("TRAN_FILENAME");

						String fileRoute = ((String[])model.get("D_ATCH_FILE_ROUTE"))[i];
						String fileName  = ((String[])model.get("D_ATCH_FILE_NAME"))[i];
						String fileSize  = ((String[])model.get("FILE_SIZE"))[i];
						
						String fileNewNm = ((String[])model.get("FILE_NEW_NAME"))[i];
						String ritem_id = (String) model.get("RITEM_ID");
						
						String ext    = "." + fileName.substring((fileName.lastIndexOf('.') + 1));
						// 파일 ID 만들기
						int fileSeq = 1;
						String fileId = "";
						fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
						// 저장준비
						modelDt.put("FILE_ID", fileId);
						modelDt.put("ATTACH_GB", "ITEM"); // 통합 HELPDESK 업로드
						modelDt.put("FILE_VALUE", "wmsms099"); // 기존코드 "tmsba230" 로
						modelDt.put("FILE_PATH",  fileRoute); // 서버저장경로
						modelDt.put("ORG_FILENAME", fileNewNm + ext); // 원본파일명
						modelDt.put("FILE_EXT", ext); // 파일 확장자
						modelDt.put("FILE_SIZE", fileSize);// 파일 사이즈
						modelDt.put("WORK_ORD_ID", ritem_id); // WORK_ORD_ID == RITEM_ID
						// 저장
						String fileMngNo = (String) dao.fileUpload(modelDt);
						// 리턴값 파일 id랑 파일경로
						model.put("IMAGE_ID", fileId);
						model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));
						
						dao.insertInfo(model);
					}
				}
			}
			map.put("MSG", MessageResolver.getMessage("insert.success"));
		} catch (Exception e) {
			throw e;
		}
		return map;
	}
	
	public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("RST", dao.insertValidate(model));
		return map;
	}
	
	
    
    /**
     * Method ID : saveLcSync
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveLcSync(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String[] spLcSyncArrData = model.get("MULIT_LC_ID_ARR").toString().split(",");
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            Map<String, Object> modelIns = new HashMap<String, Object>();
            if(tmpCnt > 0){
                for(int i = 0 ; i < tmpCnt ; i ++){
                    modelIns.put("I_MAKE_LC_ID"		, spLcSyncArrData);
                    modelIns.put("I_ITEM_CODE"		, (String)model.get("ITEM_CODE"+i));
                    modelIns.put("I_CUST_ID"		, (String)model.get("CUST_ID"+i));
                    modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
                    modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                    //dao
                    modelIns = (Map<String, Object>)dao.saveLcSync(modelIns);
                }
            }
            ServiceUtil.isValidReturnCode("WMSMS099", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
   
}
