package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsms.service.WMSMS160Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS160Service")
public class WMSMS160ServiceImpl implements WMSMS160Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS160Dao")
    private WMSMS160Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSMS160 = {"MOBILE_DEV_NO", "MOBILE_PHONE_NO"};
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 고객관리 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 고객관리 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));

                modelDt.put("MOBILE_DEV_ID"     , model.get("MOBILE_DEV_ID"+i));
                modelDt.put("MOBILE_DEV_NO"     , model.get("MOBILE_DEV_NO"+i));
                modelDt.put("MOBILE_DEV_NM"     , model.get("MOBILE_DEV_NM"+i));
                modelDt.put("MOBILE_PHONE_NO"   , model.get("MOBILE_PHONE_NO"+i));
                modelDt.put("MOBILE_SERIAL_NO"  , model.get("MOBILE_SERIAL_NO"+i));
                modelDt.put("ETC1"    			, model.get("ETC1"+i));
                modelDt.put("REG_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    		    , model.get(ConstantIF.SS_SVC_NO));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS160);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	/*
                	Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "UOM");
					modelSP.put("I_CODE", model.get("UOM_ID"+i));			
					String checkExistData = dao.checkExistData(modelSP);
					if (checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }
					*/
                    dao.delete(modelDt);
                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 고객관리 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
}
