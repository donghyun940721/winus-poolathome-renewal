package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS050Dao")
public class WMSMS050Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 차량정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms050.listCarPop", model);
    }   
    
    /**
     * Method ID : selectCar02
     * Method 설명 : 차량정보 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectCar02(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID  : saveCar
     * Method 설명  : 차량정보 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object saveCar(Map<String, Object> model){
        return  executeUpdate("wmsms050.pk_wmsms050.sp_save_car", model);
    }
    
    /**
     * Method ID    : selectCarchek
     * Method 설명      : 차량 (중복검사)
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Integer selectCarchek(Map<String, Object> model){
        return (Integer) executeView("wmsms050.chekCount", model);
    }
    
    
    
   
    /**
     * Method ID : insertWmsms010
     * Method 설명 : 화주등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertWmsms010(Map<String, Object> model){
        return executeInsert("wmsms010.insert2", model);
    }
    
    /**
     * Method ID : insertWmsms011
     * Method 설명 : 화주등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertWmsms011(Map<String, Object> model){
        return executeInsert("wmsms011.insert", model);
    }
    
    /**
     * Method ID : wmsms012_insert
     * Method 설명 : 화주등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertWmsms012(Map<String, Object> model){
        return executeInsert("wmsms012.insert", model);
    }
    
    
    /**
     * Method ID : update
     * Method 설명 :  화주 정보 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model){
        return executeUpdate("wmsms010.update2", model);
    }
    
    

    /**
     * Method ID : deleteWmsms010
     * Method 설명 :  화주정보 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object deleteWmsms010(Map<String, Object> model){
        return executeUpdate("wmsms010.delete", model);
    }
    
    /**
     * Method ID : deleteWmsms011
     * Method 설명 :  화주정보 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object deleteWmsms011(Map<String, Object> model){
        return executeUpdate("wmsms011.delete", model);
    }
    /**
     * Method ID : deleteWmsms012
     * Method 설명 :  화주정보 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object deleteWmsms012(Map<String, Object> model){
        return executeUpdate("wmsms012.delete2", model);
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀파일등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void saveUploadData(Map<String, Object> model, List list) throws Exception {
    		
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			    			
    			if ( paramMap.get("CAR_CD") != null && StringUtils.isNotEmpty( (String)paramMap.get("CAR_CD")) 
    					&& paramMap.get("CAR_TON") != null && StringUtils.isNotEmpty( (String)paramMap.get("CAR_TON"))
    					&& paramMap.get("LC_ID") != null && StringUtils.isNotEmpty( (String)paramMap.get("LC_ID")) 
    				) {		    			
	    			sqlMapClient.insert("wmsms050.insertUploadData", paramMap);
    			}
    		}
    		sqlMapClient.endTransaction();
    		
		} catch(Exception e) {
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }    
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }

	public Object driverList(Map<String, Object> model) {
		List driverList = executeQueryForList("wmsms050.driverList", model);
		GenericResultSet genericResultSet = new GenericResultSet();
		genericResultSet.setList(driverList);
		return genericResultSet;
	}

	public Object dspSave(Map<String, Object> model) {
		executeUpdate("wmsms050.pk_wmsop050.sp_multi_dsp_complete", model);
        return model;
	}

	public Object dspLoad(Map<String, Object> model) {
		List driverList = executeQueryForList("wmsms050.dspLoad", model);
		GenericResultSet genericResultSet = new GenericResultSet();
		genericResultSet.setList(driverList);
		return genericResultSet;
	}
	
	public String userIdCheck(Map<String, Object> model) {
		return (String)executeView("TMSYS090.userIdCheck", model);
	}
	
	public Object insert(Map<String, Object> model) {
		return executeInsert("tmsys090.userInsert", model);
	}
	
	public Object insertDC(Map<String, Object> model) {
        return executeInsert("tmsys091.insert", model);
    }
}
