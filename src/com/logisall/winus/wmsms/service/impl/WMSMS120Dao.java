package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS120Dao")
public class WMSMS120Dao extends SqlMapAbstractDAO{

    /**
     * Method ID    : list
     * Method 설명      : 도크장 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms120.list", model);
    }    
    /**
     * Method ID    : insert
     * Method 설명      : 도크장 등록
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsms120.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 도크장 수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsms120.update", model);
    }   
    
    /**
     * Method ID    : delete
     * Method 설명      : 도크장 삭제 (DEL_YN 를 Y로 수정)
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmsms120.delete", model);
    }
    
    /**
     * Method ID  : listWhPop
     * Method 설명  : 창고 팝업 조회 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listWhPop(Map<String, Object> model) {
        return executeQueryPageWq("wmsms040.poplist", model);
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀파일등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void saveUploadData(Map<String, Object> model, List list) throws Exception {
    		
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			    			
    			if ( paramMap.get("DOCK_CD") != null && StringUtils.isNotEmpty( (String)paramMap.get("DOCK_CD")) 
    					&& paramMap.get("DOCK_NM") != null && StringUtils.isNotEmpty( (String)paramMap.get("DOCK_NM"))
    					&& paramMap.get("LC_ID") != null && StringUtils.isNotEmpty( (String)paramMap.get("LC_ID")) 
    				) {		    			
	    			sqlMapClient.insert("wmsms120.insertUploadData", paramMap);
    			}
    		}
    		sqlMapClient.endTransaction();
    		
		} catch(Exception e) {
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }    
}
