package com.logisall.winus.wmsms.service.impl;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS080Dao")
public class WMSMS080Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 로케이션정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms080.search", model);
    }   
    /**
     * Method ID : delList
     * Method 설명 : 삭제된 로케이션정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet delList(Map<String, Object> model) {
        return executeQueryPageWq("wmsms080.delSearch", model);
    }   
    /**
     * Method ID : sublist
     * Method 설명 : 출고통제 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublist(Map<String, Object> model) {
        return executeQueryPageWq("wmsms083.search", model);
    }  
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    
    /**
     * Method ID : selectCarryType
     * Method 설명 : 적재구분  셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectCarryType(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectLocType
     * Method 설명 : loc구분  셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectLocType(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectStat
     * Method 설명 : 사용상태  셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectStat(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
   
    /**
     * Method ID : insert
     * Method 설명 : 로케이션정보 등록 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model){
        return executeInsert("wmsms080.insert", model);
    }
        
    /**
     * Method ID : update
     * Method 설명 : 로케이션정보 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model){
        return executeUpdate("wmsms080.update", model);
    }
    
    /**
     * Method ID : delete_D
     * Method 설명 :  로케이션정보 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object deleteDown(Map<String, Object> model){
        return executeDelete("wmsms080.delete_D", model);
    }
    
    /**
     * Method ID : deleteUp
     * Method 설명 :  로케이션정보 삭제(업데이트)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object deleteUp(Map<String, Object> model){
        return executeUpdate("wmsms080.delete_U", model);
    }
    
    /**
     * Method ID : deleteRestore
     * Method 설명 :  로케이션정보 복구(업데이트)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object deleteRestore(Map<String, Object> model){
        return executeUpdate("wmsms080.delete_R", model);
    }
    
    /**
     * Method ID : sub_insert
     * Method 설명 : 출고통제 등록 (시작)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertSub(Map<String, Object> model){
        return executeInsert("wmsms083.insert", model);
    }
    
    /**
     * Method ID : insertSubDlv
     * Method 설명 : 출고통제 등록 (종료)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertSubDlv(Map<String, Object> model){
        return executeInsert("wmsms083.insert2", model);
    }
    
    /**
     * Method ID : updateSub
     * Method 설명 :  출고통제 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object updateSub(Map<String, Object> model){
        return executeUpdate("wmsms083.update", model);
    }
    
    /**
     * Method ID : updateDlvCntl
     * Method 설명 :  로케이션 출고통제값 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object updateDlvCntl(Map<String, Object> model){
        return executeUpdate("wmsms080.updateDlvCntl", model);
    }
    /**
     * Method ID    	: saveLocProc
     * Method 설명       : 로케이션 템플릿 저장
     * 작성자              : schan
     * @param   model
     * @return
     */
    public Object saveLocProc(Map<String, Object> model){
        executeUpdate("wmsms080.pk_wmsms080.sp_save_loc_info", model);
        return model;
    }
    
    /**
     * Method ID : selectwhcdtoid
     * Method 설명 : 창고ID체크
     * 작성자 : sing09
     * @param model
     * @return
     */
    public String selectwhcdtoid(Map<String, String> model) throws Exception {
    	String res = "";
    	SqlMapClient sqlMapClient = getSqlMapClient();
    	try{
    		List<String> whList = sqlMapClient.queryForList("wmsms080.selectwhcdtoid",model);
    		if(whList.size() >= 1){
        		res = whList.get(0);
        	}
    	}
    	catch (Exception e){
    		throw e;
    	}
    	return res;
    }
    
    /**
     * Method ID : insertCsv
     * Method 설명 : 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertCsv(Map<String, Object> model, List list) throws Exception {
    		
    		SqlMapClient sqlMapClient = getSqlMapClient();
    		try {
    			sqlMapClient.startTransaction();
    			Map paramMap = null;
	    		for (int i=0;i<list.size();i++) {
	    			paramMap = (Map)list.get(i);
	    			
	    			if (paramMap.get("LOC_CD") != null && StringUtils.isNotEmpty((String) paramMap.get("LOC_CD")) 
	    					&& paramMap.get("WH_ID") != null && StringUtils.isNotEmpty((String) paramMap.get("WH_ID"))
	    					//&& paramMap.get("CARRY_QTY") != null && StringUtils.isNotEmpty((String) paramMap.get("CARRY_QTY"))
	    			) {	    				
		    			
	    				sqlMapClient.insert("wmsms080.insert_many", paramMap);
	    			}
	    		}
	    		sqlMapClient.endTransaction();
	    		
    		} catch(Exception e) {
    			e.printStackTrace();
    			throw e;
    			
    		} finally {
    			if (sqlMapClient != null) {
    				sqlMapClient.endTransaction();
    			}
    		}
    }    
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }	    
 
}
