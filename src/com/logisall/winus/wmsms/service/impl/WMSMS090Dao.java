package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS090Dao")
public class WMSMS090Dao extends SqlMapAbstractDAO {

    /**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model) {
	return executeQueryForList("wmsms094.selectItemGrp", model);
    }

    /**
     * Method ID : selectUom Method 설명 : 신규생성시 LCID마다 다른 UOM 필요데이터 조회 작성자 :
     * chsong
     * 
     * @param model
     * @return
     */
    public Object selectUom(Map<String, Object> model) {
	return executeQueryForList("wmsms100.selectUom", model);
    }

    /**
     * Method ID : listItem Method 설명 : 상품정보 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listItem(Map<String, Object> model) {
	return executeQueryPageWq("wmsms091.listItem", model);
    }

    /**
     * Method ID : saveItem Method 설명 : 상품정보 저장 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object saveItem(Map<String, Object> model) {
	return executeUpdate("wmsms090.pk_wmsms090e.sp_save_item", model);
    }

    /**
     * Method ID : deleteItem Method 설명 : 상품정보삭제시 wmsms090 삭제 (실질적은 delyn -> y 로
     * 변경) 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object deleteItem(Map<String, Object> model) {
	return executeUpdate("wmsms090.delete", model);
    }

    /**
     * Method ID : deleteRitem Method 설명 : 상품정보삭제시 wmsms091 삭제 (실질적은 delyn -> y
     * 로 변경) 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object deleteRitem(Map<String, Object> model) {
	return executeUpdate("wmsms091.delete", model);
    }

    /**
     * Method ID : listUom Method 설명 : UOM환산이력 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listUom(Map<String, Object> model) {
	return executeQueryPageWq("wmsms101.listUom", model);
    }

    /**
     * Method ID : insertUom Method 설명 : UOM환산이력 등록 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object insertUom(Map<String, Object> model) {
	return executeInsert("wmsms101.insert", model);
    }

    /**
     * Method ID : updateUom Method 설명 : UOM환산이력 수정 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object updateUom(Map<String, Object> model) {
	return executeUpdate("wmsms101.update", model);
    }

    /**
     * Method ID : deleteUom Method 설명 : UOM환산이력 삭제 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object deleteUom(Map<String, Object> model) {
	return executeUpdate("wmsms101.delete", model);
    }

    /**
     * Method ID : insertUom2 Method 설명 : UOM환산이력 등록 Ritem을 db쿼리에서 찾아서 쓰는방법 작성자
     * : chsong
     * 
     * @param model
     * @return
     */
    public Object insertUom2(Map<String, Object> model) {
	return executeInsert("wmsms101.insert2", model);
    }

    /**
     * Method ID : insertCsv Method 설명 : 대용량등록시 작성자 : 기드온
     * 
     * @param model
     * @return
     */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if (paramMap.get("ITEM_KOR_NM") != null 
				 && StringUtils.isNotEmpty((String) paramMap.get("ITEM_KOR_NM")) 
				 && paramMap.get("ITEM_CODE") != null 
				 && StringUtils.isNotEmpty((String) paramMap.get("ITEM_CODE"))
				 && paramMap.get("CUST_ID") != null 
				 && StringUtils.isNotEmpty((String) paramMap.get("CUST_ID")) 
				 && paramMap.get("REP_UOM_ID") != null 
				 && StringUtils.isNotEmpty((String) paramMap.get("REP_UOM_ID"))
				 && paramMap.get("SET_ITEM_YN") != null 
				 && StringUtils.isNotEmpty((String) paramMap.get("SET_ITEM_YN"))
				 && paramMap.get("UOM_QTY_STAN") != null 
				 && StringUtils.isNotEmpty((String) paramMap.get("UOM_QTY_STAN"))
				 && paramMap.get("UOM_CD_STAN") != null 
				 && StringUtils.isNotEmpty((String) paramMap.get("UOM_CD_STAN"))
				 && paramMap.get("UOM_CD_TARGET") != null 
				 && StringUtils.isNotEmpty((String) paramMap.get("UOM_CD_TARGET"))
				 ) {
					/* ITEM_CODE 중복체크가(chkDuplicationYn) Y 이면 : 조회 체크 */
					if ("Y".equals(model.get("chkDuplicationYn"))) {
						String existRsSet = (String)sqlMapClient.queryForObject("wmsms090.selectExistItemCodeDup", paramMap);
        				if(existRsSet == null && StringUtils.isEmpty(existRsSet)){
        					/* LC_ID, CUST_ID, ITEM_CODE 기준 조회값이 없으면 : 중복이 없으면 : 신규등록 */
    						String itemId = (String) sqlMapClient.insert("wmsms090.insertUploadData", paramMap);
    						paramMap.put("ITEM_ID", itemId);

    						String ritemId = (String) sqlMapClient.insert("wmsms091.insertUploadData", paramMap);
    						paramMap.put("RITEM_ID", ritemId);
    						
    						sqlMapClient.insert("wmsms090.insertUploadUomData", paramMap);
            			}else{
            				System.out.println(">> duplication itemcode : " + existRsSet);
            			}
					}else{
						String itemId = (String) sqlMapClient.insert("wmsms090.insertUploadData", paramMap);
						paramMap.put("ITEM_ID", itemId);

						String ritemId = (String) sqlMapClient.insert("wmsms091.insertUploadData", paramMap);
						paramMap.put("RITEM_ID", ritemId);
						
						sqlMapClient.insert("wmsms090.insertUploadUomData", paramMap);
					}
				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }	    
    
    /**
     * Method ID  : overapCheck
     * 작성자             : wdy
     * @param model
     * @return
     */
    public Object overapCheck(Map<String, Object> model){
    	return executeQueryForList("wmsms090.overapCheck", model);
    }
    
    /**
     * Method ID : fileUpload
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileUpload(Map<String, Object> model) {
        return executeInsert("tmsys900.fileInsert", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertInfo(Map<String, Object> model) {
        return executeInsert("wmsms090.itemImgInfoSave", model);
    }
    
    /**
     * Method ID  : insertValidate
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertValidate(Map<String, Object> model){
        return executeQueryForList("wmsms090.insertValidate", model);
    }
    
    /**
     * Method ID    : saveUploadDataPk
     * Method 설명      : 템플릿 상품 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveUploadDataPk(Map<String, Object> model){
        executeUpdate("wmsms090.pk_wmsms090e.sp_save_excel_simp_item", model);
        return model;
    }
    
    /**
     * Method ID    : saveLcSync
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveLcSync(Map<String, Object> model){
        executeUpdate("wmsms090.pk_wmsms090.sp_save_multi_lc_item", model);
        return model;
    }

	public Object deletedList(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsms090.deletedList", model);
		genericResultSet.setList(list);
		return genericResultSet;
	}
	
    
    /**
     * Method ID    : restoreItem
     * Method 설명      : 삭제된 상품 재등록
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object restoreItem(Map<String, Object> model){
        executeUpdate("wmsms090.restoreItem", model);
        return model;
    }
    
    /**
     * Method ID    : restoreItem
     * Method 설명      : 삭제된 상품 재등록
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object restoreItemDetail(Map<String, Object> model){
        executeUpdate("wmsms090.restoreItemDetail", model);
        return model;
    }
    
    /**
     * Method ID    : selectDuplicateBarcd
     * Method 설명      : 중복 바코드 조회
     * 작성자                 : ykim
     * @param   model
     * @return
     */
    public GenericResultSet selectDuplicateBarcd(Map<String, Object> model){
    	return executeQueryWq("wmsms090.selectDuplicateBarcd", model);
    }
}
