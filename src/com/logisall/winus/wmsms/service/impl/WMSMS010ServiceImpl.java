package com.logisall.winus.wmsms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS010Service")
public class WMSMS010ServiceImpl implements WMSMS010Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS010Dao")
    private WMSMS010Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 화주정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 화주정보 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
            
            int iuCnt = Integer.parseInt(model.get("I_selectIds").toString());
            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            
            if(iuCnt > 0){
                String custType = "";
                String lcId = "";
                
                String[] trustCustId = new String[iuCnt];  //
                String[] custId = new String[iuCnt];  // 거래처id
                String[] custCd = new String[iuCnt];  // 거래처cd
                String[] bizNo = new String[iuCnt];  // 사업자등록번호
                
                String[] mobileNo = new String[iuCnt];  // 핸드폰번호 x
                String[] dutyId = new String[iuCnt];  // 담당자id x
                String[] tel = new String[iuCnt];  // 전화번호
                String[] creLimitDay = new String[iuCnt];  // 신용한도일수 x
                String[] settleTermDay = new String[iuCnt];  // 결제조건일수 x
                
                String[] zip = new String[iuCnt];  // 우편번호
                String[] fax = new String[iuCnt];  // 팩스
                String[] creditGrade = new String[iuCnt];  // 신용등급
                String[] dlvCntl = new String[iuCnt];  // 출고통제여부
                String[] representCustCd = new String[iuCnt];  // 대표거래처코드 x
                
                String[] currCd = new String[iuCnt];  // 통화코드 x
                String[] creSaleAmt = new String[iuCnt];  // 외상매출금 x
                String[] creLimitAmt = new String[iuCnt];  // 신용한도금액 x
                String[] ordAmt = new String[iuCnt];  // 입력오더금액 x
                String[] unReceptAmt = new String[iuCnt];  // 미수어음 x
                
                String[] salePerNo = new String[iuCnt];  // 담당영업사원번호 x
                String[] custStat = new String[iuCnt];  // 거래처상태 x
                String[] vatYn = new String[iuCnt];  // 부가세여부 x
                String[] trUpdownYn = new String[iuCnt];  // 상하차가능여부 x
                String[] inTon = new String[iuCnt];  // 진입톤수 x
                
                String[] calcSeq = new String[iuCnt];  // 할당우선순위 x
                String[] dlvReqDt = new String[iuCnt];  // 배송요청시간 x
                String[] dlvCntlComment = new String[iuCnt];  // 출고통제사유
                String[] custEpcCd = new String[iuCnt];  //  x
                String[] custNm = new String[iuCnt];  // 거래처명
                
                String[] tradeNm = new String[iuCnt];  // 상호
                String[] eMail = new String[iuCnt];  // 이메일
                String[] repNm = new String[iuCnt];  // 대표자명
                String[] bizCond = new String[iuCnt];  // 업태
                String[] bizType = new String[iuCnt];  // 업종
                
                String[] http = new String[iuCnt];  // 홈페이지
                String[] addr = new String[iuCnt];  // 주소
                String[] e_mail = new String[iuCnt];  // 주소
                String[] repCustRepNm = new String[iuCnt];  // 대표거래처 대표자성명 x
                String[] custType2 = new String[iuCnt];  // 화주타입x
                String[] inOutCustType2 = new String[iuCnt];  // 거래처타입x
                
                String[] lotNoUseYn = new String[iuCnt];  // 입고lotNo사용여부
                String[] clientNecYn = new String[iuCnt];  // 입고lotNo사용여부
                String[] rcvRcmTy = new String[iuCnt];  // 입고lotNo사용여부
                String[] typicalCustGb = new String[iuCnt];  // 입고lotNo사용여부
                String[] sapUseYn = new String[iuCnt];  // SAP주문사용여부
                
                String[] calType = new String[iuCnt];  // 정산유형
                
                String[] dutyMobile = new String[iuCnt];  // 이메일
                String[] dutyNm 	= new String[iuCnt];  // 이메일
                String[] dutyPhone  = new String[iuCnt];  // 이메일
                
                String[] interfaceKey  = new String[iuCnt];  // interfaceKey
                String[] partnerKey  = new String[iuCnt];  // partnerKey
          
                String[] dealType = new String[iuCnt];
                String[] majorProduct = new String[iuCnt];
                String[] manageEvent = new String[iuCnt];
                
                String[] manageBizCont = new String[iuCnt];
                String[] manageGrade = new String[iuCnt];
                String[] settleTerm = new String[iuCnt];
                
                String[] nowUseYn = new String[iuCnt];
                String[] accountCd1 = new String[iuCnt];
                String[] accountNo1 = new String[iuCnt];
                String[] useCalYn = new String[iuCnt];
                String[] safeStockTy = new String[iuCnt];
                
                String[] dlvTraceMsgYn = new String[iuCnt];
                String[] dlvTraceGrade = new String[iuCnt];
                String[] dlvCsatMsgYn = new String[iuCnt];
                
                String workIp = "";
                String userNo = "";
                
                for(int i = 0 ; i < iuCnt ; i ++){
                    trustCustId[i]      = (String)model.get("TRUST_CUST_ID"+i);
                    custId[i]      = (String)model.get("CUST_ID"+i);
                    custCd[i]      = (String)model.get("CUST_CD"+i);
                    bizNo[i]       = (String)model.get("BIZ_NO"+i);
                    mobileNo[i]    = (String)model.get("MOBILE_NO"+i);
                    dutyId[i]      = (String)model.get("DUTY_ID"+i);
                    
                    tel[i]             = (String)model.get("TEL"+i);
                    creLimitDay[i]     = (String)model.get("CRE_LIMIT_DAY"+i);
                    settleTermDay[i]   = (String)model.get("SETTLE_TERM_DAY"+i);
                    zip[i]             = (String)model.get("ZIP"+i);
                    fax[i]             = (String)model.get("FAX"+i);
                    
                    creditGrade[i]      = (String)model.get("CREDIT_GRADE"+i);
                    dlvCntl[i]          = (String)model.get("DLV_CNTL"+i);
                    representCustCd[i]  = (String)model.get("REPRESENT_CUST_CD"+i);
                    currCd[i]           = (String)model.get("CURR_CD"+i);
                    creSaleAmt[i]       = (String)model.get("CRE_SALE_AMT"+i);
                    
                    creLimitAmt[i]      = (String)model.get("CRE_LIMIT_AMT"+i);
                    ordAmt[i]           = (String)model.get("ORD_AMT"+i);
                    unReceptAmt[i]      = (String)model.get("UN_RECEPT_AMT"+i);
                    salePerNo[i]        = (String)model.get("SALE_PER_NO"+i);
                    custStat[i]         = (String)model.get("CUST_STAT"+i);

                    vatYn[i]            = (String)model.get("VAT_YN"+i);
                    trUpdownYn[i]       = (String)model.get("TR_UPDOWN_YN"+i);
                    inTon[i]            = (String)model.get("IN_TON"+i);
                    calcSeq[i]          = (String)model.get("CALC_SEQ"+i);
                    dlvReqDt[i]         = (String)model.get("DLV_REQ_DT"+i);
                    
                    dlvCntlComment[i]   = (String)model.get("DLV_CNTL_COMMENT"+i);
                    custEpcCd[i]        = (String)model.get("CUST_EPC_CD"+i);
                    custNm[i]           = (String)model.get("CUST_NM"+i);
                    tradeNm[i]          = (String)model.get("TRADE_NM"+i);
                    eMail[i]            = (String)model.get("E_MAIL"+i);
                    
                    repNm[i]            = (String)model.get("REP_NM"+i);
                    bizCond[i]          = (String)model.get("BIZ_COND"+i);
                    bizType[i]          = (String)model.get("BIZ_TYPE"+i);
                    http[i]             = (String)model.get("HTTP"+i);
                    addr[i]             = (String)model.get("ADDR"+i);
                    e_mail[i]             = (String)model.get("E_MAIL"+i);
                    
                    repCustRepNm[i]     = (String)model.get("REP_CUST_REP_NM"+i);
                    custType2[i]        = (String)model.get("CUST_TYPE"+i);
                    inOutCustType2[i]   = (String)model.get("CUST_INOUT_TYPE"+i);
                    lotNoUseYn[i]       = (String)model.get("LOT_NO_USE_YN"+i);
                    clientNecYn[i]      = (String)model.get("CLIENT_NEC_YN"+i);
                    typicalCustGb[i]    = (String)model.get("TYPICAL_CUST_GB"+i);
                    
                    rcvRcmTy[i]         = (String)model.get("RCV_RCM_TY"+i);
                    sapUseYn[i]         = (String)model.get("SAP_USE_YN"+i);
                    calType[i]          = (String)model.get("CAL_TYPE"+i);
                    
                    dutyMobile[i]       = (String)model.get("DUTY_MOBILE"+i);
                    dutyNm[i]       	= (String)model.get("DUTY_NM"+i);
                    dutyPhone[i]        = (String)model.get("DUTY_PHONE"+i);
                    
                    interfaceKey[i]    	= (String)model.get("INTERFACE_KEY"+i);
                    partnerKey[i]       = (String)model.get("PARTNER_KEY"+i);
              
                    dealType[i]    		= (String)model.get("DEAL_TYPE"+i);
                    majorProduct[i]     = (String)model.get("MAJOR_PRODUCTS"+i);
                    manageEvent[i]      = (String)model.get("MANAGE_EVENT"+i);
                    
                    manageBizCont[i]    = (String)model.get("MANAGE_BIZ_CONT"+i);
                    manageGrade[i]      = (String)model.get("MANAGE_GRADE"+i);
                    settleTerm[i]       = (String)model.get("SETTLE_TERM"+i);
                    
                    nowUseYn[i]    		= (String)model.get("NOW_USE_YN"+i);
                    accountCd1[i]       = (String)model.get("ACCOUNT_CD1"+i);
                    accountNo1[i]       = (String)model.get("ACCOUNT_NO1"+i);
                    useCalYn[i]       	= (String)model.get("USE_CAL_YN"+i);
                    safeStockTy[i]      = (String)model.get("SAFE_STOCK_TY"+i);

                    dlvTraceMsgYn[i]    = (String)model.get("DLV_TRACE_MSG_YN"+i);
                    dlvTraceGrade[i]    = (String)model.get("DLV_TRACE_GRADE"+i);
                    dlvCsatMsgYn[i]    = (String)model.get("DLV_CSAT_MSG_YN"+i);
                    String calYn = model.get("USE_CAL_YN").toString();
                    
                    if(calYn.equals("Y")){
                    
	                    /* WMSAC pk 시작 */
	                    /* WMSAC pk 시작 */
	                    String[] contDetailId  = new String[1];                
	                    String[] serviceCd     = new String[1];      
	                    String[] ritemId       = new String[1];     
	                    String[] uomId         = new String[1];     
	                    String[] freeTimeDay   = new String[1];     
	                    String[] unitAmt       = new String[1];
	                    String[] tansCustId    = new String[1];
	                    
	                    contDetailId[0]    = (String)model.get("CONT_DETAIL_ID"+i);     
	                    serviceCd[0]       = "120";   
	                    ritemId[0]         = "";
	                    uomId[0]           = "";
	                    freeTimeDay[0]     = "";
	                    unitAmt[0]         = "";
	                    tansCustId[0]      = "";
	                    //프로시져에 보낼것들 다담는다
	                    Map<String, Object> modelIns = new HashMap<String, Object>();
	                    
	                    //main
	                    modelIns.put("contId"          	, (String)model.get("CONT_ID"+i));
	                    modelIns.put("gvLcId"          	, (String)model.get(ConstantIF.SS_SVC_NO));
	                    modelIns.put("custId"         	, (String)model.get("CUST_ID"+i));
	                    modelIns.put("saleBuyGbn"     	, "");
	                    modelIns.put("applyFrDt"       	, (String)model.get("APPLY_FR_DT"+i));
	                    
	                    modelIns.put("applyToDt"       	, (String)model.get("APPLY_TO_DT"+i));
	                    modelIns.put("accStd"          	, "");
	                    modelIns.put("contDeptId"      	, (String)model.get("CONT_DEPT_ID"+i));
	                    modelIns.put("contEmployeeId"  	, (String)model.get("CONT_EMPLOYEE_ID"+i));
	                    modelIns.put("realDeptId"      	, "");
	                    
	                    modelIns.put("realEmployeeId"  	, (String)model.get("REAL_EMPLOYEE_ID"+i));
	                    modelIns.put("collectEmployeeId", (String)model.get("COLLECT_EMPLOYEE_ID"+i));
	                    modelIns.put("approvalEmpId"	, (String)model.get("APPROVAL_EMP_ID"+i));
	                    modelIns.put("groupComEmpId"	, (String)model.get("GROUP_COM_EMP_ID"+i));
	                    modelIns.put("payDay"          	, (String)model.get("PAY_DAY"+i));
	                    modelIns.put("payCd"           	, "");
	                    modelIns.put("roundCd"         	, "");
	                    modelIns.put("endCutCd"        	, "");
	                    
	                    modelIns.put("confYn"          	, "");
	                    modelIns.put("closingDay"       , (String)model.get("CLOSING_DAY"+i));

	                    //sub
	                    modelIns.put("contDetailId"    , contDetailId);
	                    modelIns.put("serviceCd"       , serviceCd);
	                    modelIns.put("ritemId"         , ritemId);
	                    modelIns.put("uomId"           , uomId);
	                    modelIns.put("freeTimeDay"     , freeTimeDay);
	                    modelIns.put("unitAmt"         , unitAmt);
	                    modelIns.put("tansCustId"      , tansCustId);
	                    
	                    //session 정보
	                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	
	                    //dao                
	                    modelIns = (Map<String, Object>)dao.saveSub(modelIns);
	                    ServiceUtil.isValidReturnCode("WMSAC010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	                    /* WMSAC pk 끝 */
                    }
                }

                custType = "C";
                lcId = (String)model.get(ConstantIF.SS_SVC_NO);
                workIp = (String)model.get(ConstantIF.SS_CLIENT_IP);
                userNo = (String)model.get(ConstantIF.SS_USER_NO);
                
                Map<String, Object> modelSP = new HashMap<String, Object>();
                
                modelSP.put("I_CUST_TYPE", custType);
                modelSP.put("I_LC_ID", lcId);
                
                modelSP.put("I_TRUST_CUST_ID", trustCustId);
                modelSP.put("I_CUST_ID", custId);
                modelSP.put("I_CUST_CD", custCd);
                modelSP.put("I_BIZ_NO", bizNo);
                modelSP.put("I_MOBILE_NO", mobileNo);
                
                modelSP.put("I_DUTY_ID", dutyId);
                modelSP.put("I_TEL", tel);
                modelSP.put("I_CRE_LIMIT_DAY", creLimitDay);
                modelSP.put("I_SETTLE_TERM_DAY", settleTermDay);
                modelSP.put("I_ZIP", zip);
                
                modelSP.put("I_FAX", fax);
                modelSP.put("I_CREDIT_GRADE", creditGrade);
                modelSP.put("I_DLV_CNTL", dlvCntl);
                modelSP.put("I_REPRESENT_CUST_CD", representCustCd);
                modelSP.put("I_CURR_CD", currCd);
                
                modelSP.put("I_CRE_SALE_AMT", creSaleAmt);
                modelSP.put("I_CRE_LIMIT_AMT", creLimitAmt);
                modelSP.put("I_ORD_AMT", ordAmt);
                modelSP.put("I_UN_RECEPT_AMT", unReceptAmt);
                modelSP.put("I_SALE_PER_NO", salePerNo);
                
                modelSP.put("I_CUST_STAT", custStat);
                modelSP.put("I_VAT_YN", vatYn);
                modelSP.put("I_TR_UPDOWN_YN", trUpdownYn);
                modelSP.put("I_IN_TON", inTon);
                modelSP.put("I_CALC_SEQ", calcSeq);
                
                modelSP.put("I_DLV_REQ_DT", dlvReqDt);
                modelSP.put("I_DLV_CNTL_COMMENT", dlvCntlComment);
                modelSP.put("I_CUST_EPC_CD", custEpcCd);
                modelSP.put("I_CUST_PART_TYPE", custType2);
                modelSP.put("I_CUST_INOUT_TYPE", inOutCustType2);
                
                modelSP.put("I_LOT_NO_USE_YN", lotNoUseYn);
                modelSP.put("I_CLIENT_NEC_YN", clientNecYn);
                modelSP.put("I_RCV_RCM_TY", rcvRcmTy);
                modelSP.put("I_TYPICAL_CUST_GB", typicalCustGb);
                modelSP.put("I_SAP_USE_YN", sapUseYn);
                
                modelSP.put("I_CAL_TYPE", calType);
                modelSP.put("I_CUST_NM", custNm);
                modelSP.put("I_TRADE_NM", tradeNm);
                modelSP.put("I_EMAIL", eMail);
                modelSP.put("I_REP_NM", repNm);
                
                modelSP.put("I_BIZ_COND", bizCond);
                modelSP.put("I_BIZ_TYPE", bizType);
                modelSP.put("I_HTTP", http);
                modelSP.put("I_ADDR", addr);
                modelSP.put("I_E_MAIL", e_mail);
                
                modelSP.put("I_REP_CUST_REP_NM", repCustRepNm);
                modelSP.put("I_DUTY_MOBILE", dutyMobile);
                modelSP.put("I_DUTY_NM", 	 dutyNm);
                modelSP.put("I_DUTY_PHONE",  dutyPhone);
                modelSP.put("I_INTERFACE_KEY",  interfaceKey);
                
                modelSP.put("I_PARTNER_KEY",  partnerKey);
                modelSP.put("I_DEAL_TYPE",  dealType);
                modelSP.put("I_MAJOR_PRODUCTS",  majorProduct);
                modelSP.put("I_MANAGE_EVENT",  manageEvent);
                modelSP.put("I_MANAGE_BIZ_CONT",  manageBizCont);
                
                modelSP.put("I_MANAGE_GRADE",  manageGrade);
                modelSP.put("I_SETTLE_TERM",  settleTerm);
                modelSP.put("I_NOW_USE_YN",  nowUseYn);
                modelSP.put("I_ACCOUNT_CD1",  accountCd1);
                modelSP.put("I_ACCOUNT_NO1",  accountNo1);
                
                modelSP.put("I_USE_CAL_YN",  useCalYn);
                modelSP.put("I_SAFE_STOCK_TY",  safeStockTy);
                
                modelSP.put("I_DLV_TRACE_MSG_YN",  dlvTraceMsgYn);
                modelSP.put("I_DLV_TRACE_GRADE",  dlvTraceGrade);
                modelSP.put("I_DLV_CSAT_MSG_YN",  dlvCsatMsgYn);
                
                modelSP.put("I_WORK_IP", workIp);
                modelSP.put("I_USER_NO", userNo);                                       
          
                String[] val = dao.saveCust(modelSP);  
                
                if(!val[1].equals("0")){
                        errCnt++;
                        m.put("MSG", val[0]);
                }
            }
            
            if(delCnt > 0){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                for(int i = 0 ; i < delCnt ; i ++){
                	
                	Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "CUST");
					modelSP.put("I_CODE", model.get("CUST_ID" + i));				
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    } 
                	
                    modelDt.put("CUST_ID"       , model.get("CUST_ID" + i));
                    modelDt.put("ST_GUBUN"		, model.get("D_ST_GUBUN" + i));
                    modelDt.put("UPD_NO", model.get("SS_USER_NO"));
                    modelDt.put("LC_ID", model.get("SS_SVC_NO"));
                    if("DELETE".equals(modelDt.get("ST_GUBUN"))){
                        dao.deleteWmsms010(modelDt);
                        dao.deleteWmsms012(modelDt);                           
                    }
                }
            }
            
            if(errCnt == 0){
                m.put("MSG", MessageResolver.getMessage("save.success"));
                m.put("errCnt", errCnt);
            }else{                
                if(m.get("MSG").equals("")){
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                }
                m.put("errCnt", errCnt);
            }     
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 화주정보 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
     
    
    /**
     * Method ID : saveCsv
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCsv(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.insertCsv(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }    
    
    /**
     * Method ID : listQ1
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listQ1(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : listEmployeeQ1
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listEmployeeQ1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listEmployeeQ1(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : saveLcSync
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveLcSync(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String[] spLcSyncArrData = model.get("MULIT_LC_ID_ARR").toString().split(",");
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            Map<String, Object> modelIns = new HashMap<String, Object>();
            if(tmpCnt > 0){
                for(int i = 0 ; i < tmpCnt ; i ++){
                    modelIns.put("I_MULIT_LC_ID"	, spLcSyncArrData);
                    modelIns.put("I_CUST_CD"		, (String)model.get("CUST_CD"+i));
                    modelIns.put("I_LC_ID"			, (String)model.get(ConstantIF.SS_SVC_NO));
                    modelIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                    //dao
                    modelIns = (Map<String, Object>)dao.saveLcSync(modelIns);
                }
            }
            ServiceUtil.isValidReturnCode("WMSMS010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
