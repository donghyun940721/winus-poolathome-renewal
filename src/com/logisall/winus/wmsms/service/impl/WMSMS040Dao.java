package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS040Dao")
public class WMSMS040Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 창고정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms040.list", model);
    }   
    
    /**
     * Method ID : poplist
     * Method 설명 : 팝업 창고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet poplist(Map<String, Object> model) {
        return executeQueryPageWq("wmsms040.poplist", model);
    }   
    /**
     * Method ID : insert
     * Method 설명 : 창고정보 등록 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model){
        return executeInsert("wmsms040.insert", model);
    }
    
    /**
     * Method ID : delete
     * Method 설명 : 창고정보 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model){
        return executeUpdate("wmsms040.update", model);
    }
    
    /**
     * Method ID : delete
     * Method 설명 : 창고정보 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object delete(Map<String, Object> model){
        return executeUpdate("wmsms040.delete", model);
    }
    
    /**
     * Method ID : selectWhType
     * Method 설명 : 창고정보 타입 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectWhType(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectWhGB
     * Method 설명 : 창고정보 구분 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectWhGB(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : insertCsvWh
     * Method 설명 : 상품군 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertCsvWh(Map<String, Object> model, List list) throws Exception {
    		
    		SqlMapClient sqlMapClient = getSqlMapClient();
    		try {
    			sqlMapClient.startTransaction();
    			Map<String, Object> paramMap = null;
	    		for (int i=0;i<list.size();i++) {
	    			paramMap = (Map)list.get(i);
	    			    			
	    			if ( (paramMap.get("WH_CD") != null && StringUtils.isNotEmpty( paramMap.get("WH_CD").toString())) 
	    					&& (paramMap.get("WH_NM") != null && StringUtils.isNotEmpty( paramMap.get("WH_NM").toString()))
	    					&& (paramMap.get("LC_ID") != null && StringUtils.isNotEmpty( paramMap.get("LC_ID").toString()))
	    					) {	    				
	    				paramMap.put("REG_NO", 			model.get("SS_USER_NO"));
	    				
		    			sqlMapClient.insert("wmsms040.insert", paramMap);

	    			}
	    		}
	    		sqlMapClient.endTransaction();
	    		
    		} catch(Exception e) {
    			e.printStackTrace();
    			throw e;
    			
    		} finally {
    			if (sqlMapClient != null) {
    				sqlMapClient.endTransaction();
    			}
    		}
    }
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }
}
