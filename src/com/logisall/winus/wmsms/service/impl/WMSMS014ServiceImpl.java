package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsms.service.WMSMS014Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.logisall.winus.frm.common.util.DESUtil;

@Service("WMSMS014Service")
public class WMSMS014ServiceImpl implements WMSMS014Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS014Dao")
    private WMSMS014Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSMS014 = {"DEPT_ID", "DEPT_CD"};
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 고객관리 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listDetail
     * 대체 Method 설명    : 직원관리 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listDetail(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 고객관리 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));
                modelDt.put("DEPT_ID"     		, model.get("DEPT_ID"+i));
                modelDt.put("DEPT_CD"     		, model.get("DEPT_CD"+i));
                modelDt.put("DEPT_NM"     		, model.get("DEPT_NM"+i));
                modelDt.put("IN_DEPT_CD"    	, model.get("IN_DEPT_CD"+i));
                modelDt.put("UP_DEPT_CD"     	, model.get("UP_DEPT_CD"+i));
                modelDt.put("UP_IN_DEPT_CD"     , model.get("UP_IN_DEPT_CD"+i));
                modelDt.put("DEPT_LEVEL"   		, model.get("DEPT_LEVEL"+i));
                modelDt.put("REMARK"   			, model.get("REMARK"+i));
                modelDt.put("CUST_ID"   		, model.get("CUST_ID"+i));
                modelDt.put("COST_CD"   		, model.get("COST_CD"+i));
                modelDt.put("COST_NM"   		, model.get("COST_NM"+i));
                modelDt.put("REG_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("WORK_IP"    		, model.get(ConstantIF.SS_CLIENT_IP));

                //ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS014);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	dao.insert(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.delete(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveDetail
     * 대체 Method 설명    : 고객관리 저장 saveDetail
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));
                
                modelDt.put("EMPLOYEE_ID"     	, model.get("EMPLOYEE_ID"+i));

                modelDt.put("EMPLOYEE_NO"     	, model.get("EMPLOYEE_NO"+i));
                modelDt.put("EMPLOYEE_NM"     	, model.get("EMPLOYEE_NM"+i));
                modelDt.put("MOBILE_NO"     	, model.get("MOBILE_NO"+i));
                modelDt.put("REMARK"    		, model.get("REMARK"+i));
                
                modelDt.put("DEPT_ID"     		, model.get("DEPT_ID"+i));
              
                modelDt.put("REG_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("WORK_IP"    		, model.get(ConstantIF.SS_CLIENT_IP));

                //ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS014);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	dao.insertDetail(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.updateDetail(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.deleteDetail(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 고객관리 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("RST", dao.insertValidate(model));
		return map;
	}
    
    public Map<String, Object> userIdValidate(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("RST", dao.userIdValidate(model));
		return map;
	}
}
