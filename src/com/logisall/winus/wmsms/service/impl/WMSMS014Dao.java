package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS014Dao")
public class WMSMS014Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : list 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("WMSMS014.list", model);
	}
	
	/**
	 * Method ID : listDetail 
	 * Method 설명 : 직원관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listDetail(Map<String, Object> model) {
		return executeQueryPageWq("WMSMS014.listDetail", model);
	}
	
	/**
	 * Method ID : insert 
	 * Method 설명 : 고객관리 등록 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("WMSMS014.insert", model);
	}
	
	/**
	 * Method ID : update 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("WMSMS014.update", model);
	}
	
	/**
	 * Method ID : UpadteMobileUserInfo 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object UpadteMobileUserInfo(Map<String, Object> model) {
		return executeUpdate("WMSMS014.UpadteMobileUserInfo", model);
	}
	
	/**
	 * Method ID : delete 
	 * Method 설명 : 고객관리 삭제 (DEL_YN 를 Y로 수정) 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("WMSMS014.delete", model);
	}
    
	/**
	 * Method ID : deleteUserInfo 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteUserInfo(Map<String, Object> model) {
		return executeUpdate("WMSMS014.deleteUserInfo", model);
	}
	
    /**
     * Method ID  : insertValidate
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertValidate(Map<String, Object> model){
        return executeQueryForList("WMSMS014.insertValidate", model);
    }
    
    /**
     * Method ID  : userIdValidate
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object userIdValidate(Map<String, Object> model){
        return executeQueryForList("WMSMS014.userIdValidate", model);
    }
    
    /**
	 * Method ID : userIdCheck
	 * Method 설명 : 사용자ID 중복 체크
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public String userIdCheck(Map<String, Object> model) {
		return (String)executeView("TMSYS090.userIdCheck", model);
	}
	
	/**
	 * Method ID : insert
	 * Method 설명 : 사용자관리 등록
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object insertUserId(Map<String, Object> model) {
		return executeInsert("tmsys090.userInsert", model);
	}
	
	/**
     * Method ID : insertDC
     * Method 설명 : 사용자물류센터 추가
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertDC(Map<String, Object> model) {
        return executeInsert("tmsys091.insert", model);
    }
    
    /**
	 * Method ID : insertDetail
	 * Method 설명 : 고객관리 등록  Detail
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object insertDetail(Map<String, Object> model) {
		return executeInsert("WMSMS014.insertDetail", model);
	}
	
	/**
	 * Method ID : update Detail
	 * Method 설명 : 고객관리 수정 Detail
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateDetail(Map<String, Object> model) {
		return executeUpdate("WMSMS014.updateDetail", model);
	}
	
	/**
	 * Method ID : delete Detail
	 * Method 설명 : 고객관리 삭제 (DEL_YN 를 Y로 수정) Detail
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteDetail(Map<String, Object> model) {
		return executeUpdate("WMSMS014.deleteDetail", model);
	}
}
