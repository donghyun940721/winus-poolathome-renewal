package com.logisall.winus.wmsms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS180Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS180Service")
public class WMSMS180ServiceImpl implements WMSMS180Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS180Dao")
    private WMSMS180Dao dao;

    /**
     * Method ID : listSub
     * Method 설명 : ZONE정보등록 서브 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.sublist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : saveSub
     * Method 설명 : ZONE정보등록 서브 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	// test
        	// 현재 선택된 subList들 list화
        	List checking = new ArrayList();
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("CUST_ID"		,model.get("CUST_ID" + i));
                modelDt.put("CODE_VALUE"	,model.get("CODE_VALUE" + i));
                modelDt.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID" + i));
                modelDt.put("LC_ID"			,model.get("SS_SVC_NO"));
                modelDt.put("SS_USER_NO"	,model.get("SS_USER_NO"));
                modelDt.put("DLV_TYPE"		,model.get("DLV_TYPE"));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	checking.add(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            }
        	
        	// 이전 선택되어있던 subList들 list화
        	Map<String, Object> listsDt = new HashMap<String, Object>();
        	listsDt.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
        	listsDt.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID"));
        	listsDt.put("CUST_ID", model.get("SEL_CUST_ID"));
        	listsDt.put("pageIndex", "1");
        	listsDt.put("pageSize", model.get("girdRecord"));
        	List checked = dao.sublist(listsDt).getList();
        	        	
        	// 해당 subList 요소 전체삭제
        	Map<String, Object> modelDtM = new HashMap<String, Object>();
        	modelDtM.put("SEL_CUST_ID"		,model.get("SEL_CUST_ID"));
        	modelDtM.put("LC_ID"			,model.get("SS_SVC_NO"));
        	modelDtM.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID"));
        	
        	dao.saveInitDelete(modelDtM);
        	
        	for (int i = 0 ; i < checked.size() ; i++){
        		Map<String, Object>object = (Map<String, Object>)checked.get(i);
        		
        		if(object != null && !object.isEmpty() && object.containsKey("CHK_BOX") && object.containsKey("CODE_VALUE")){
        			// 이전 선택되어있던 subList row 선택
        			if(object.get("CHK_BOX").toString().equals("Yes")){
        				int idx = -1;
        				for(int j = 0 ; j < checking.size() ; j++){
        					Map<String, Object>object2 = (Map<String, Object>)checking.get(j);
        					// 현재 선택되어있던 subList중 같은 CITY_CD 찾기
        					if(object2.containsKey("CODE_VALUE") && object2.get("CODE_VALUE").toString().equals(object.get("CODE_VALUE").toString())){
        						idx = j;
        						break;
        					}
        				}
        				// 현재 선택되어있던 subList중 같은 CITY_CD 선택
        				if(idx != -1){
        					// 현재 선택된 subList data 불러오기
        					Map<String, Object>object2 = (Map<String, Object>)checking.get(idx);
        					// 이전 선택된  DLV_TYPE이 DLV/AS 이면 기존 데이터 그대로 삽입
                            if (object.get("DLV_TYPE").toString().equals("DLV/AS")){
                            	object.put("LC_ID",model.get("SS_SVC_NO"));
            					object.put("CUST_ID",model.get("SEL_CUST_ID"));
            					object.put("TRUST_CUST_ID",model.get("TRUST_CUST_ID"));
            					object.put("SS_USER_NO",model.get("SS_USER_NO"));
                            	dao.saveInsert(object);
                            }
                            // 이전 선택된 subList와 현재 선택된 subList의 DLV_TYPE이 다르면 DLV/AS를 삽입.
                            else if(!object2.get("DLV_TYPE").toString().equals(object.get("DLV_TYPE").toString())){
                            	object2.put("DLV_TYPE","DLV/AS");
                            	dao.saveInsert(object2);
                            }
                            // 이전 선택된 subList와 현재 선택된 subList의 DLV_TYPE이 같으면 현재 데이터 그대로 삽입
                            else{
                            	dao.saveInsert(object2);
                            }
        					
        				}
        				// 현재 선택되어있던 subList중 같은 CITY_CD 없음 => 현재 체크가 안되어있음.
        				else{
        					object.put("LC_ID",model.get("SS_SVC_NO"));
        					object.put("CUST_ID",model.get("SEL_CUST_ID"));
        					object.put("TRUST_CUST_ID",model.get("TRUST_CUST_ID"));
        					object.put("SS_USER_NO",model.get("SS_USER_NO"));
        					// 이전 선택된 subList의 DLV_TYPE이 DLV/AS 이면 현재와 다른 DLV_TYPE만 삽입.
        					if(object.get("DLV_TYPE").toString().equals("DLV/AS")){
        						if(model.get("DLV_TYPE").toString().equals("DLV")){
        							object.put("DLV_TYPE", "AS");
        							dao.saveInsert(object);
        						}
        						else{
        							object.put("DLV_TYPE", "DLV");
        							dao.saveInsert(object);
        						}
        					}
        					// 이전 선택된 subList의 DLV_TYPE이 현재 DLV_TYPE과 다르면 이전 데이터 그대로 삽입.
        					else if(!object.get("DLV_TYPE").toString().equals(model.get("DLV_TYPE").toString())){
        						dao.saveInsert(object);
        					}
        				}
        			}
        			// 이전 선택되어있지 않았던 subList row 선택
        			else{
        				int idx = -1;
        				for(int j = 0 ; j < checking.size() ; j++){
        					Map<String, Object>object2 = (Map<String, Object>)checking.get(j);
        					// 현재 선택되어있던 subList중 같은 CITY_CD 찾기
        					if(object2.containsKey("CODE_VALUE") && object2.get("CODE_VALUE").toString().equals(object.get("CODE_VALUE").toString())){
        						idx = j;
        						break;
        					}
        				}
        				// 현재 선택되어있던 subList중 같은 CITY_CD 선택
        				if(idx != -1){
        					// 현재 선택된 subList data 삽입
        					Map<String, Object>object2 = (Map<String, Object>)checking.get(idx);
        					dao.saveInsert(object2);
        				}
        			}
        		}
        		else{
        			errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
        		}
        	}
        	m.put("MSG", MessageResolver.getMessage("update.success"));
            m.put("errCnt", errCnt);
     
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveSub_T1 
     * Method 설명 : 거래처별 권역설정 수정
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveSub_T1(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		// 해당 subList 요소 전체삭제
        	Map<String, Object> modelDtM = new HashMap<String, Object>();
        	modelDtM.put("SEL_CUST_ID"		,model.get("SEL_CUST_ID"));
        	modelDtM.put("LC_ID"			,model.get("SS_SVC_NO"));
        	modelDtM.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID"));
        	modelDtM.put("DLV_TYPE"			,model.get("DLV_TYPE"));
        	
        	dao.saveInitDelete0419(modelDtM);
        	
    		List checking = new ArrayList();
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			modelDt.put("CUST_ID"		,model.get("CUST_ID" + i));
    			modelDt.put("CODE_VALUE"	,model.get("CODE_VALUE" + i));
    			modelDt.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID" + i));
    			modelDt.put("LC_ID"			,model.get("SS_SVC_NO"));
    			modelDt.put("SS_USER_NO"	,model.get("SS_USER_NO"));
    			modelDt.put("DLV_TYPE"		,model.get("DLV_TYPE"));
    			
    			dao.saveInsert(modelDt);
    		}
        	
    		m.put("MSG", MessageResolver.getMessage("update.success"));
    		m.put("errCnt", errCnt);
    		
    	} catch (Exception be) {
    		if (log.isInfoEnabled()) {
    			log.info(be.getMessage());
    		}
    		m.put("MSG", be.getMessage());
    	} 
    	return m;
    }
    
    /**
     * Method ID : saveSub
     * Method 설명 : ZONE정보등록 서브 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> deleteSub(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            if(1 == 1){
            	Map<String, Object> modelDtM = new HashMap<String, Object>();
            	modelDtM.put("SEL_CUST_ID"		,model.get("SEL_CUST_ID"));
            	modelDtM.put("LC_ID"			,model.get("SS_SVC_NO"));
            	modelDtM.put("TRUST_CUST_ID"	,model.get("TRUST_CUST_ID"));
            	dao.saveInitDelete(modelDtM);
            	m.put("MSG", MessageResolver.getMessage("delete.success"));
                m.put("errCnt", errCnt);
            }else{
                errCnt++;
                m.put("errCnt", errCnt);
                throw new Exception(MessageResolver.getMessage("save.error"));
            }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
