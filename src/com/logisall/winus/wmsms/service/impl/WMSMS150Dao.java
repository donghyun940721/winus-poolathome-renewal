package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS150Dao")
public class WMSMS150Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : list
     * Method 설명  : 순환재고조사 조회
     * 작성자             : 기드온
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms150.list", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 순환재고조사 등록
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsms150.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 순환재고조사 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsms150.update", model);
    }  
    
    /**
     * Method ID    : delete
     * Method 설명      : 순환재고조사 삭제
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object delete_WMSST121(Map<String, Object> model) {
        return executeDelete("wmsms150.delete_WMSST121", model);
    }
    public Object delete_WMSST120(Map<String, Object> model) {
        return executeDelete("wmsms150.delete_WMSST120", model);
    }
    public Object delete_WMSMS150(Map<String, Object> model) {
        return executeDelete("wmsms150.delete_WMSMS150", model);
    }
    
    
    /**
     * Method ID : selectCYCL01
     * Method 설명 : 순환재고조사방식 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectCYCL01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀파일등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void saveUploadData(Map<String, Object> model, List list) throws Exception {
    		
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			
    			if ( paramMap.get("CUST_ID") != null && StringUtils.isNotEmpty( (String)paramMap.get("CUST_ID"))
    					&& paramMap.get("LC_ID") != null && StringUtils.isNotEmpty( (String)paramMap.get("LC_ID"))
    					&& paramMap.get("CYCL_STOCK_TYPE") != null && StringUtils.isNotEmpty( (String)paramMap.get("CYCL_STOCK_TYPE"))    					    					
    				) {
    				
    				String stockType = (String)paramMap.get("CYCL_STOCK_TYPE");
    				if ( "1".equals(stockType) 
    					|| "2".equals(stockType) &&  paramMap.get("WORK_DAY_NUM") != null && StringUtils.isNotEmpty( (String)paramMap.get("WORK_DAY_NUM"))
    					|| "3".equals(stockType) &&  paramMap.get("LOC_ID") != null && StringUtils.isNotEmpty( (String)paramMap.get("LOC_ID")) 
    					|| "4".equals(stockType) &&  paramMap.get("RITEM_ID") != null && StringUtils.isNotEmpty( (String)paramMap.get("RITEM_ID")) 	
    				) {		    					
    					sqlMapClient.insert("wmsms150.insert_many", paramMap);	  
    				}
	    			
    			}
    		}
    		sqlMapClient.endTransaction();
    		
		} catch(Exception e) {
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }     
    
    /**
     * Method ID  : itemPop
     * Method 설명  : 상품조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet itemPop(Map<String, Object> model) {
        return executeQueryPageWq("wmsms150.itemPop", model);
    }  
}
