package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS081Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS081Service")
public class WMSMS081ServiceImpl implements WMSMS081Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS081Dao")
    private WMSMS081Dao dao;


    /**
     * Method ID : list
     * Method 설명 : ZONE정보등록 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : listSub
     * Method 설명 : ZONE정보등록 서브 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.sublist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : save
     * Method 설명 : ZONE정보등록 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                    modelDt.put("SS_SVC_NO", model.get("SS_SVC_NO"));
                    modelDt.put("SS_USER_NO", model.get("SS_USER_NO"));
                    modelDt.put("ZONE_ID", model.get("ZONE_ID" + i));
                    modelDt.put("ZONE_NM", model.get("ZONE_NM" + i));
                    modelDt.put("COLOR",   model.get("COLOR" + i));
          
     
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                    m.put("MSG", MessageResolver.getMessage("insert.success"));
                    m.put("errCnt", errCnt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	
                	Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "ZONE");
					modelSP.put("I_CODE", model.get("ZONE_ID" + i));				
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }					
                    dao.delete(modelDt);
                    
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveSub
     * Method 설명 : ZONE정보등록 서브 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                    modelDt.put("SS_USER_NO", model.get("SS_USER_NO"));
                    modelDt.put("WORK_IP", model.get("SS_CLIENT_IP"));
                    
                    modelDt.put("LOC_ID", model.get("LOC_ID" + i));
                    modelDt.put("ZONE_ID", model.get("ZONE_ID" + i));
                    modelDt.put("O_LOC_ID", model.get("OLD_LOC_ID" + i));
                    modelDt.put("WORK_SEQ", model.get("WORK_SEQ" + i));
                    System.out.println("AA : " + model.get("WORK_SEQ" + i));
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    int chek = dao.selectlocchek(modelDt);
                    if(chek == 1){
                        // 중복값중에 삭제된게 아니면 중복이라고 메시지 띠우기
                        errCnt++;
                        m.put("errCnt", errCnt);
                        throw new Exception(MessageResolver.getMessage("chek.ritemid"));
                    }else{
                        dao.insertSub(modelDt);
                        m.put("MSG", MessageResolver.getMessage("insert.success"));
                        m.put("errCnt", errCnt);     
                    }
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.updateSub(modelDt);
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.deleteSub(modelDt);
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[] { String.valueOf(insertCnt) }));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);

		} catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}       
}
