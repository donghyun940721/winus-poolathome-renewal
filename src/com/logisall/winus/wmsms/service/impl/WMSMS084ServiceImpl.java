package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS084Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS084Service")
public class WMSMS084ServiceImpl implements WMSMS084Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS084Dao")
    private WMSMS084Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 로케이션별 입고상품등록 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {          
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
//            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
  
    /**
     * Method ID : save
     * Method 설명 : 로케이션별 입고상품등록 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("RITEM_ID", model.get("RITEM_ID"+i));
                modelDt.put("LOC_ID", model.get("LOC_ID"+i));
                modelDt.put("OL_RITEM_ID", model.get("OL_RITEM_ID"+i));
                modelDt.put("OL_LOC_ID", model.get("OL_LOC_ID"+i));
                modelDt.put("REMARK", model.get("REMARK"+i));
         
                
                modelDt.put("LC_ID", model.get("SS_SVC_NO"));
                modelDt.put("REG_NO", model.get("SS_USER_NO"));
                modelDt.put("UPD_NO", model.get("SS_USER_NO"));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                    
                    m.put("MSG", MessageResolver.getMessage("insert.success"));
                    m.put("errCnt", errCnt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.delete(modelDt);
                    
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);                    
                    throw new Exception(MessageResolver.getMessage("save.error"));                
                }
            }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 로케이션별 입고상품등록 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
//        map.put("LIST", dao.list(model));
        
        return map;
    }
     
    
    
}
