package com.logisall.winus.wmsms.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsms.service.WMSMS170Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.logisall.winus.frm.common.util.DESUtil;

@Service("WMSMS170Service")
public class WMSMS170ServiceImpl implements WMSMS170Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS170Dao")
    private WMSMS170Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSMS170 = {"DRIVER_NM", "MOBILE_NO"};
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 고객관리 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 고객관리 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 	, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  	, model.get("ST_GUBUN"+i));
                modelDt.put("DRIVER_ID"     , model.get("DRIVER_ID"+i));
                modelDt.put("DRIVER_NM"     , model.get("DRIVER_NM"+i));
                modelDt.put("MOBILE_NO"     , model.get("MOBILE_NO"+i));
                modelDt.put("LICENSE_NO"    , model.get("LICENSE_NO"+i));
                modelDt.put("REMARK"     	, model.get("REMARK"+i));
                modelDt.put("USER_ID"     	, model.get("USER_ID"+i));
                modelDt.put("KEY_USER_NO"   , model.get("KEY_USER_NO"+i));
                modelDt.put("REG_NO"    	, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    		, model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("WORK_IP"    	, model.get(ConstantIF.SS_CLIENT_IP));
                
                Map<String, Object> modelUserIdDt = new HashMap<String, Object>();
                modelUserIdDt.put("USER_PASSWORD"	, DESUtil.dataEncrypt((String)model.get("USER_PW"+i)));
                modelUserIdDt.put("USER_NO"     	, model.get("KEY_USER_NO"+i));
                modelUserIdDt.put("USER_ID"     	, model.get("USER_ID"+i));
                modelUserIdDt.put("USER_NM"     	, model.get("DRIVER_NM"+i));
                modelUserIdDt.put("AUTH_CD"     	, model.get("AUTH_CD"+i));
                modelUserIdDt.put("MOBILE_NO"     	, model.get("MOBILE_NO"+i));
                modelUserIdDt.put("CLIENT_ID"     	, model.get("CLIENT_CD"+i));
                modelUserIdDt.put("USER_GB"     	, model.get("USER_GB"+i));
                modelUserIdDt.put("INSERT_NO"     	, model.get(ConstantIF.SS_USER_NO));
                modelUserIdDt.put("MULTI_USE_GB"	, model.get("MULTI_USE_GB"+i));
                modelUserIdDt.put("LANG"     		, model.get("LANG"+i));
                modelUserIdDt.put("USER_GRID_NUM"   , "");
                modelUserIdDt.put("SS_CLIENT_IP"    , model.get(ConstantIF.SS_CLIENT_IP));
                modelUserIdDt.put("SS_USER_NO"     	, model.get(ConstantIF.SS_USER_NO));

                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS170);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	String getDriverId = (String)dao.insert(modelDt);
                	modelDt.put("DRIVER_ID",	getDriverId);
                	dao.insertWMSDV010(modelDt);
                	
                	
                	String isuser = dao.userIdCheck(modelUserIdDt);
                    if ("0".equals(isuser.substring(0, 1))) {
                        // 1.사용자정보를 등록한다.
                    	String getUserNo = (String)dao.insertUserId(modelUserIdDt);
                        
                        // 2.사용자 물류센터를 등록한다.
                        Map<String, Object> modelUserLcDt = new HashMap<String, Object>();
                        modelUserLcDt.put("USER_NO"			, getUserNo);
                        modelUserLcDt.put("D_LC_ID"			, model.get(ConstantIF.SS_SVC_NO));
                        modelUserLcDt.put("D_CONNECT_YN"	, "Y");
                        modelUserLcDt.put("D_USER_LC_SEQ"	, "1");
                        modelUserLcDt.put("SS_CLIENT_IP"	, model.get(ConstantIF.SS_CLIENT_IP));
                        modelUserLcDt.put("SS_USER_NO"		, model.get(ConstantIF.SS_USER_NO));
	                    dao.insertDC(modelUserLcDt);
                    } else {
                        m.put("MSG", MessageResolver.getMessage("userid.check"));
                    }
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    dao.updateWMSDV010(modelDt);
                    
                    
                    dao.UpadteMobileUserInfo(modelUserIdDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){

                    dao.delete(modelDt);
                    dao.deleteWMSDV010(modelDt);
                    
                    
                    dao.deleteUserInfo(modelUserIdDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : save2
     * 대체 Method 설명    : 고객관리 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 	, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  	, model.get("ST_GUBUN"+i));
                modelDt.put("DRIVER_ID"     , model.get("DRIVER_ID"+i));
                modelDt.put("DRIVER_NM"     , model.get("DRIVER_NM"+i));
                modelDt.put("MOBILE_NO"     , model.get("MOBILE_NO"+i));
                modelDt.put("LICENSE_NO"    , "");
                modelDt.put("REMARK"     	, "");
                modelDt.put("REG_NO"    	, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    		, model.get("LC_ID"+i));
                modelDt.put("WORK_IP"    	, model.get(ConstantIF.SS_CLIENT_IP));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS170);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	String getDriverId = (String)dao.insert(modelDt);
                	modelDt.put("DRIVER_ID",	getDriverId);
                	dao.insertWMSDV010(modelDt);
                	
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    dao.updateWMSDV010(modelDt);
                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.delete(modelDt);
                    dao.deleteWMSDV010(modelDt);
                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 고객관리 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("RST", dao.insertValidate(model));
		return map;
	}
    
    
    public Map<String, Object> mobileValidate(Map<String, Object> model) throws Exception {
	   	Map<String, Object> m = new HashMap<String, Object>();
    	int CheckCnt = 0;
    	String CheckMsg = "";
		try{
	  
				Map<String, Object> modelIns = new HashMap<String, Object>();
				
							
				modelIns.put("vrSrchMobileNo"    	, model.get("vrSrchMobileNo"));					
				String orgCheck = dao.mobileValidate(modelIns);
				
				// 모바일 번호가 존재하는 경우
				if(orgCheck.equals("1")){
					CheckCnt = 1;
					m.put("MSG_CHK", CheckCnt);
				}else if(!orgCheck.equals("1")){
					CheckCnt = 2;
					m.put("MSG_CHK", CheckCnt);
					}
	
				
	    		m.put("MSG", MessageResolver.getMessage("save.success"));
	    		m.put("MSG_ORA", model.get("vrSrchMobileNo"));
	    		m.put("errCnt", 0);
	    		
		    	} catch(Exception e) {
		    		throw e;
		    	}
		
		return m;
		
	}
    
    
    public Map<String, Object> userIdValidate(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("RST", dao.userIdValidate(model));
		return map;
	}
}
