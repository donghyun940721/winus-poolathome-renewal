package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPL010Dao")
public class WMSPL010Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : insert Method 설명 : 물류용기관리 등록 작성자 : 기드온
	 * 
	 * @param model
	 * @return Object
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmspl010.insert", model);
	}

	/**
	 * Method ID : update Method 설명 : 물류용기관리 수정 작성자 : 기드온
	 * 
	 * @param model
	 * @return Object
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmspl010.update", model);
	}

	/**
	 * Method ID : delete Method 설명 : 물류용기관리 삭제 작성자 : 기드온
	 * 
	 * @param model
	 * @return Object
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmspl010.delete", model);
	}

	/**
	 * Method ID : list Method 설명 : 물류용기관리 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return GenericResultSet
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmspl010.list", model);
	}

	/**
	 * Method ID : selectUSEYN Method 설명 : 사용유무 셀렉트 박스 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object selectUSEYN(Map<String, Object> model) {
		return executeQueryForList("tmsyms030.selectBox", model);
	}

	/**
	 * Method ID : selectPoolGrp Method 설명 : 용기군 셀렉트 박스 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object selectPoolGrp(Map<String, Object> model) {
		return executeQueryForList("wmspl020.selectPoolGrp", model);
	}

	/**
	 * Method ID : orderPoolList Method 설명 : 물류용기팝업 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return GenericResultSet
	 */
	public GenericResultSet orderPoolList(Map<String, Object> model) {
		return executeQueryPageWq("wmspl010.orderPoolSearch", model);
	}

	/**
	 * Method ID : listGrp Method 설명 : 상품군 조회 (물류용기 상품화위해서 조회) 작성자 : chsong
	 * 
	 * @param model
	 * @return GenericResultSet
	 */
	public GenericResultSet listGrp(Map<String, Object> model) {
		return executeQueryPageWq("wmsms094.list", model);
	}

	/**
	 * Method ID : saveItem Method 설명 : 상품정보 저장 (물류용기 상품화위해서 넣음) 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object saveItem(Map<String, Object> model) {
		executeUpdate("wmsms090.pk_wmsms090e.sp_save_item", model);
		return model;
	}

	/**
	 * Method ID : deleteItem Method 설명 : 상품정보삭제시 wmsms090 삭제 (실질적은 delyn -> y 로
	 * 변경) 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteItem(Map<String, Object> model) {
		return executeUpdate("wmsms090.delete", model);
	}

	/**
	 * Method ID : deleteRitem Method 설명 : 상품정보삭제시 wmsms091 삭제 (실질적은 delyn -> y
	 * 로 변경) 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteRitem(Map<String, Object> model) {
		return executeUpdate("wmsms091.delete", model);
	}

	/**
	 * Method ID : saveUploadData Method 설명 : 엑셀파일등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;

			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				if (paramMap.get("POOL_NM") != null && StringUtils.isNotEmpty((String) paramMap.get("POOL_NM")) && paramMap.get("POOL_CODE") != null && StringUtils.isNotEmpty((String) paramMap.get("POOL_CODE"))
						&& paramMap.get("CUST_ID") != null && StringUtils.isNotEmpty((String) paramMap.get("CUST_ID")) && paramMap.get("REP_UOM_ID") != null && StringUtils.isNotEmpty((String) paramMap.get("REP_UOM_ID"))) {

					paramMap.put("ITEM_KOR_NM", paramMap.get("POOL_NM"));
					paramMap.put("ITEM_EPC_CD", paramMap.get("COMP_EPC_CD"));
					paramMap.put("ITEM_BAR_CD", paramMap.get("POOL_BAR_CD"));

					String itemId = (String) sqlMapClient.insert("wmsms090.insertUploadData", paramMap);
					paramMap.put("ITEM_ID", itemId);
					paramMap.put("ITEM_CODE", paramMap.get("POOL_CODE"));
					paramMap.put("ITEM_NM", paramMap.get("POOL_NM"));
					paramMap.put("ITEM_GRP_ID", paramMap.get("ITEM_GRP_ID"));
					paramMap.put("ITEM_WGT", paramMap.get("POOL_WGT"));
					paramMap.put("IN_ZONE", paramMap.get("IN_ZONE_ID"));
					
					if ( paramMap.get("OP_QTY") == null ) {
						paramMap.put("OP_QTY", 0);
					}
					if ( paramMap.get("SET_ITEM_YN") == null ) {
						paramMap.put("SET_ITEM_YN", "N");
					}
					if ( paramMap.get("TIME_PERIOD_DAY") == null ) {
						paramMap.put("TIME_PERIOD_DAY", 0);
					}
					if ( paramMap.get("PLT_YN") == null ) {
						paramMap.put("PLT_YN", "Y");
					}

					String rItemID = (String) sqlMapClient.insert("wmsms091.insertUploadData", paramMap);

					paramMap.put("RITEM_ID", rItemID);

					sqlMapClient.insert("wmspl010.insertUploadData", paramMap);
				}

				sqlMapClient.endTransaction();
			}

		} catch (Exception e) {
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }	
	
    /**
     * Method ID : fileUpload
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileUpload(Map<String, Object> model) {
        return executeInsert("tmsys900.fileInsert", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertInfo(Map<String, Object> model) {
        return executeInsert("wmspl010.itemImgInfoSave", model);
    }
}
