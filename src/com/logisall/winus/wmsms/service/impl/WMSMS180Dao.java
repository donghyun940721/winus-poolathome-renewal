package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS180Dao")
public class WMSMS180Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : sublist Method 설명 : ZONE정보등록 서브 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet sublist(Map<String, Object> model) {
		return executeQueryPageWq("wmsms180.sublist", model);
	}

	/**
	 * Method ID : updateSub Method 설명 : ZONE정보 서브 수정 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object saveInitDelete(Map<String, Object> model) {
		return executeUpdate("wmsms180.saveInitDelete", model);
	}
	
	public Object saveInitDelete0419(Map<String, Object> model) {
		return executeUpdate("wmsms180.saveInitDelete0419", model);
	}
	
	/**
	 * Method ID : insert Method 설명 : ZONE정보등록 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object saveInsert(Map<String, Object> model) {
		return executeInsert("wmsms180.saveInsert", model);
	}
}

