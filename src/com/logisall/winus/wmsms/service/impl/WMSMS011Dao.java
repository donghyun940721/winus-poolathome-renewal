package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS011Dao")
public class WMSMS011Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : list 
	 * Method 설명 : 매출거래처 정보 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsms011.search", model);
	}

	/**
	 * Method ID : listQ1
	 * Method 설명 : 화주 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet listQ1(Map<String, Object> model) {
		return executeQueryPageWq("wmsms011.listQ1", model);
	}
	
	/**
	 * Method ID : insertWmsms010 
	 * Method 설명 : 화주등록 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object insertWmsms010(Map<String, Object> model) {
		return executeInsert("wmsms010.insert2", model);
	}

	/**
	 * Method ID : insertWmsms011 
	 * Method 설명 : 화주등록 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object insertWmsms011(Map<String, Object> model) {
		return executeInsert("wmsms011.insert", model);
	}

	/**
	 * Method ID : wmsms012_insert 
	 * Method 설명 : 화주등록 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object insertWmsms012(Map<String, Object> model) {
		return executeInsert("wmsms012.insert", model);
	}

	/**
	 * Method ID : update 
	 * Method 설명 : 화주 정보 수정 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmsms010.update2", model);
	}

	/**
	 * Method ID : updateWmsms011 
	 * Method 설명 : 화주 정보 수정(유효기간통제, 화주EPC) 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object updateWmsms011(Map<String, Object> model) {
		return executeUpdate("wmsms011.update", model);
	}

	/**
	 * Method ID : deleteWmsms010 
	 * Method 설명 : 화주정보 삭제 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteWmsms010(Map<String, Object> model) {
		return executeUpdate("wmsms010.delete", model);
	}

	/**
	 * Method ID : deleteWmsms011 
	 * Method 설명 : 화주정보 삭제 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteWmsms011(Map<String, Object> model) {
		return executeUpdate("wmsms011.delete", model);
	}
	/**
	 * Method ID : deleteWmsms012 
	 * Method 설명 : 화주정보 삭제 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteWmsms012(Map<String, Object> model) {
		return executeUpdate("wmsms012.delete2", model);
	}

	public int checkTrustCustId(Map<String, Object> model) {
		// 중복검사
		return (Integer) executeQueryForObject("wmsms010.checkTrustCustId", model);
	}

	public int checkCustId(Map<String, Object> model) {
		// 중복검사
		return (Integer) executeQueryForObject("wmsms010.checkCustId", model);
	}

	public int checkLcId(Map<String, Object> model) {
		// 중복검사
		return (Integer) executeQueryForObject("wmsms010.checkLcId", model);
	}

	/**
	 * Method ID : saveUploadData 
	 * Method 설명 : 엑셀파일등록시 
	 * 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if (paramMap.get("CUST_TYPE") == null || StringUtils.isEmpty((String) paramMap.get("CUST_TYPE"))) {
					paramMap.put("CUST_TYPE", "C");
				}
				if (paramMap.get("CUST_CD") != null && StringUtils.isNotEmpty((String) paramMap.get("CUST_CD"))) {

					String custId = (String) sqlMapClient.insert("wmsms010.insertCsv", paramMap);
					paramMap.put("CUST_ID", custId);
					
					sqlMapClient.insert("wmsms011.insertCsv2", paramMap);
				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	/**
     * Method ID    : saveLcSync
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveLcSync(Map<String, Object> model){
        executeUpdate("wmsms011.pk_wmsms010.sp_sav_multi_lc_trans_cust", model);
        return model;
    }
    
    /**
     * Method ID    : copyTransCust
     * Method 설명      : 거래처복사
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object copyTransCust(Map<String, Object> model){
    	executeUpdate("wmsms011.pk_wmsms010.sp_sav_copy_trans_cust", model);
    	return model;
    }
    
    
	/**
     * Method ID    : uploadExcel
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object uploadExcel(Map<String, Object> model){
        executeUpdate("wmsms011.pk_wmsms010.sp_sav_trans_cust", model);
        return model;
    }

    public int checkTxtCustId(Map<String, Object> model) {
    	// 중복검사
    	return (Integer) executeQueryForObject("wmsms010.checkTxtCustId", model);
    }
    
    /*-
     * Method ID : checkLcClientOrdYn
     * Method 설명 : 
     * @param model
     * @return
     */
    public String checkLcClientOrdYn(Map<String, Object> model) {
        return (String)executeView("wmsms011.checkLcClientOrdYn", model);
    }
    
    /**
	 * Method ID : insert Method 설명 : ZONE정보등록 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object insertMs085Header(Map<String, Object> model) {
		return executeInsert("wmsms085.insert", model);
	}
	
	/**
	 * Method ID : insertSub Method 설명 : ZONE정보 서브 등록 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object insertMs085Detail(Map<String, Object> model) {
		return executeInsert("wmsms085.insertDetail", model);
	}
	
	/**
	 * Method ID 		: saveUploadDataProcedure
	 * Method 설명 	: 거래처 엑셀 업로드 (프로시저)
	 * 작성자 			: KSJ
	 * @param model
	 * @return
	 */
	public void saveUploadDataProcedure(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				paramMap.put("I_CUST_ID", model.get("SS_CUST_ID"));
				paramMap.put("I_LC_ID", model.get("SS_SVC_NO"));
				paramMap.put("I_WORK_IP", "127.0.0.1");
				paramMap.put("I_USER_NO", model.get("SS_USER_NO"));
				
				if(model.get("I_TRANS_CUST_INOUT_TYPE") == null){
					paramMap.put("I_TRANS_CUST_INOUT_TYPE", "2");
				}
				
				
				if (paramMap.get("I_TRANS_CUST_CD") != null && StringUtils.isNotEmpty((String) paramMap.get("I_TRANS_CUST_CD"))
						&& paramMap.get("I_CUST_ID") != null && StringUtils.isNotEmpty((String) paramMap.get("I_CUST_ID"))) {

					sqlMapClient.insert("wmsms011.pk_wmsms010.sp_sav_trans_cust", paramMap);
				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
}
