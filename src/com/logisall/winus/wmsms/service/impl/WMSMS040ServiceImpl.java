package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS040Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS040Service")
public class WMSMS040ServiceImpl implements WMSMS040Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS040Dao")
    private WMSMS040Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 창고정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : poplist
     * Method 설명 : 팝업 창고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.poplist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
  
    /**
     * Method ID : save
     * Method 설명 : 창고정보 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
           
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
           
                modelDt.put("selectIds"         , model.get("selectIds"));
                modelDt.put("ST_GUBUN"          , model.get("ST_GUBUN" + i));
                
                modelDt.put("WH_ID"             , model.get("WH_ID" + i));
                modelDt.put("WH_CD"             , model.get("WH_CD" + i));
                modelDt.put("WH_NM"             , model.get("WH_NM" + i));
                modelDt.put("WH_TYPE"           , model.get("WH_TYPE" + i));
                modelDt.put("WH_GB"             , model.get("WH_GB" + i));
                modelDt.put("MAX_LEN_W"         , model.get("MAX_LEN_W" + i));
                modelDt.put("MAX_LEN_L"         , model.get("MAX_LEN_L" + i));
                // 기존 로직 IN_SCH, OUT_SCH 입력받는 곳이 없음. 추후 수정해야됨.
                
                modelDt.put("LC_ID"             , model.get("SS_SVC_NO"));
                modelDt.put("REG_NO"            , model.get("SS_USER_NO"));
                modelDt.put("UPD_NO"            , model.get("SS_USER_NO"));


                if("INSERT".equals(model.get("ST_GUBUN"+i))){
     
                    dao.insert(modelDt);

             
                
                    m.put("MSG", MessageResolver.getMessage("insert.success"));
                    m.put("errCnt", errCnt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                  
                    dao.update(modelDt);

                    
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	
                	Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "WH");
					modelSP.put("I_CODE", model.get("WH_ID" + i));					
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }
					
                    dao.delete(modelDt);
                    
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("MSG", MessageResolver.getMessage("delete.success"));
                    m.put("errCnt", errCnt);                    
                    throw new Exception(MessageResolver.getMessage("save.error"));
                
                }
            }
        } catch (BizException be) {
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 창고정보 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트박스 생성
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            model.put("inKey", "COM08");
            map.put("WhType", dao.selectWhType(model));
   
            model.put("inKey", "COM03");
            map.put("WhGB", dao.selectWhGB(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID   : listClient
     * Method 설명 : 창고정보  팝업 자동조회
     * 작성자      : 기드온
     * @param 
     * @return
     */
    public Map<String, Object> listClient(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.poplist(model));
        
        return map;
    }
    
    /**
     * Method ID : saveCsvWh
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCsvWh(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.insertCsvWh(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }      
}
