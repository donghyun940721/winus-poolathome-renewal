package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS190Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS190Service")
public class WMSMS190ServiceImpl implements WMSMS190Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS190Dao")
	private WMSMS190Dao dao;

	/**
	 * 대체 Method ID : selectBox 대체 Method 설명 : 물류용기관리 화면 데이타셋 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("POOLGRP", dao.selectPoolGrp2(model));
		model.put("inKey", "USEYN");
		map.put("USEYN", dao.selectUSEYN(model));
		map.put("UOM", dao.selectUom(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : list 대체 Method 설명 : 물류용기관리 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.list(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : save 대체 Method 설명 : 물류용기관리 저장 작성자 : 기드온 수정 : 상품화를 위한 저장 추가
	 * (상품화, 물류용기등록) chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> save(Map<String, Object> model) throws Exception {
		
		Map<String, Object> m = new HashMap<String, Object>();		
		int errCnt = 0;
		try {
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				String stGubun = (String) model.get("I_ST_GUBUN" + i);
				String[] itemId = {(String) model.get("I_ITEM_ID" + i)};
				String[] custId = {(String) model.get("I_CUST_ID" + i)};
				String[] itemCode = {(String) model.get("I_ITEM_CODE" + i)};
				String[] itemKorNm = {(String) model.get("I_ITEM_KOR_NM" + i)};
				String[] repUomId = {(String) model.get("I_REP_UOM_ID" + i)};
				String[] ritemId = {(String) model.get("I_RITEM_ID" + i)};
				String[] itemGrpId = {""};

				String[] compEpcCd = {(String) model.get("COMP_EPC_CD" + i)};
				String[] poolBarCd = {(String) model.get("POOL_BAR_CD" + i)};
				String[] inWhId = {(String) model.get("IN_WH_ID" + i)};
				String[] inZoneId = {(String) model.get("IN_ZONE_ID" + i)};
				String[] tagPreFix = {(String) model.get("TAG_PREFIX" + i)};
				String[] weight = {(String) model.get("POOL_WGT" + i)};
				String[] unitPrice = {(String) model.get("UNIT_PRICE" + i)};
				String[] color = {(String) model.get("COLOR" + i)};
				String[] itfCd = {(String) model.get("ITF_CD" + i)};
				String[] sizeH = {(String) model.get("SIZE_H" + i)};
				String[] sizeL = {(String) model.get("SIZE_L" + i)};
				String[] sizeW = {(String) model.get("SIZE_W" + i)};
				String[] typeSt = {(String) model.get("TYPE_ST" + i)};
				
				String[] inCustZone  = {(String) model.get("IN_CUST_ZONE" + i)};
				String[] outCustZone = {(String) model.get("OUT_CUST_ZONE" + i)};
				
				String[] lotCustZone = {(String) model.get("LOT_USE_YN" + i)};
				String[] outLocRec 	 = {(String) model.get("OUT_LOC_REC" + i)};
				String[] inLocRec 	 = {(String) model.get("IN_LOC_REC" + i)};
				String[] itemTmp 	 = {(String) model.get("ITEM_TMP" + i)};
				String[] useYn 		 = {(String) model.get("USE_YN_B" + i)};
				
				String[] salesPrice = {(String) model.get("SALES_PRICE" + i)};
				
				if ("".equals(model.get("I_ITEM_GRP_ID" + i)) || StringUtils.isEmpty((String) model.get("I_ITEM_GRP_ID" + i))) {
					GenericResultSet grpRs = dao.listGrp(model);
					List grpList = grpRs.getList();
					for (int j = 0; j < grpList.size(); j++) {
						Map<String, Object> grpMap = (Map<String, Object>) grpList.get(j);
						if (grpMap.get("ITEM_GRP_TYPE").equals("P")) {
							itemGrpId[0] = (String) grpMap.get("ITEM_GRP_ID");
							break;
						}
					}
				} else {
					itemGrpId[0] = (String) model.get("I_ITEM_GRP_ID" + i);
				}
				String[] dummy = {""};
				String[] dummyN = {"N"};
				String[] dummyY = {"Y"};

				String lcId = (String) model.get(ConstantIF.SS_SVC_NO);
				String workIp = (String) model.get(ConstantIF.SS_CLIENT_IP);
				String userNo = (String) model.get(ConstantIF.SS_USER_NO);

				String ritemIdTemp = "";

				Map<String, Object> modelItem = new HashMap<String, Object>();

				if (stGubun.equals("INSERT") || stGubun.equals("UPDATE")) {
					modelItem.put("ITEM_ID", itemId);
					modelItem.put("UNIT_PRICE", unitPrice);
					modelItem.put("CUST_BARCODE", dummy);
					modelItem.put("RITEM_ID", ritemId); // 얘가없네
					modelItem.put("CUST_ID", custId);
					
					modelItem.put("STOCK_UOM_ID", dummy);
					modelItem.put("BUY_UNIT", dummy);
					modelItem.put("ITEM_TYPE", dummy);
					modelItem.put("ITEM_WGT", weight);
					modelItem.put("ITEM_GRP_ID", itemGrpId);
					
					modelItem.put("ITEM_GRP_TYPE", dummy);
					modelItem.put("PACKING_BOX_TYPE", dummy);
					modelItem.put("PROP_STOCK_DAY", dummy);
					modelItem.put("TIME_PERIOD_DAY", dummy);
					modelItem.put("SHIP_ABC", dummy);
					
					modelItem.put("SET_ITEM_YN", dummyN);
					modelItem.put("WEIGHT_CLASS", dummy);
					modelItem.put("ORD_CUST_ID", dummy);
					modelItem.put("ITEM_BAR_CD", poolBarCd);
					modelItem.put("ITEM_SIZE", dummy);
					
					modelItem.put("KAN_CD", dummy);
					modelItem.put("VAT_YN", dummy);
					modelItem.put("ITEM_CLASS", dummy);
					modelItem.put("OP_QTY", dummy);
					modelItem.put("RS_QTY", dummy);					
					
					modelItem.put("EA_YN", dummy);
					modelItem.put("ITEM_EPC_CD", compEpcCd);
					modelItem.put("IN_WH_ID", inWhId);
					modelItem.put("IN_ZONE", inZoneId);
					modelItem.put("OUT_ZONE", dummy);
					modelItem.put("PLT_YN", dummyY);
					
					modelItem.put("REP_UOM_ID", repUomId);
					modelItem.put("OP_UOM_ID", dummy);
					modelItem.put("PROP_QTY", dummy);
					modelItem.put("EXPIRY_DATE", dummy);
					modelItem.put("TAG_PREFIX", tagPreFix);
					
					modelItem.put("CUST_EPC_CD", dummy);
					modelItem.put("BEST_DATE_TYPE", dummy);
					modelItem.put("BEST_DATE_NUM", dummy);
					modelItem.put("BEST_DATE_UNIT", dummy);
					modelItem.put("USE_DATE_NUM", dummy);
					
					modelItem.put("USE_DATE_UNIT", dummy);
					modelItem.put("OUT_BEST_DATE_NUM", dummy);
					modelItem.put("OUT_BEST_DATE_UNIT", dummy);
					modelItem.put("IN_BEST_DATE_NUM", dummy);
					modelItem.put("IN_BEST_DATE_UNIT", dummy);
					
					// 추가된부분(프로시져에서는 아직안씀)
					modelItem.put("COLOR", color); // 색상
					modelItem.put("ITF_CD", itfCd); // ITF_CD
					modelItem.put("SIZE_H", sizeH); // 사이즈_높이
					modelItem.put("SIZE_L", sizeL); // 사이즈_세로
					modelItem.put("SIZE_W", sizeW); // 사이즈_가로
					
					modelItem.put("BOX_BAR_CD", dummy); // 박스EPC코드
					modelItem.put("BOX_EPC_CD", dummy); // 박스EPC코드
					modelItem.put("TYPE_ST", typeSt); // 상품관리TYPE
					modelItem.put("CUST_LEGACY_ITEM_CD", dummy); // 사이즈_세로
					modelItem.put("ITEM_CODE", itemCode);
					
					modelItem.put("CUST_ITEM_CD", dummy);
					modelItem.put("ITEM_ENG_NM", dummy);
					modelItem.put("ITEM_KOR_NM", itemKorNm);
					modelItem.put("ITEM_SHORT_NM", dummy);
					modelItem.put("ITEM_NM", dummy);
					
					
					modelItem.put("MAKER_NM", dummy); // 사이즈_가로
					modelItem.put("LOT_USE_YN", lotCustZone); // 입고LOT 사용여부 (상품등록 공통컬럼)
					modelItem.put("AUTO_OUT_ORD_YN", dummy);
					modelItem.put("AUTO_CAL_PLT_YN", dummy);
					modelItem.put("OUT_LOC_REC", outLocRec);
					modelItem.put("IN_LOC_REC", inLocRec);
					
					modelItem.put("ITEM_DEVISION_CD", dummy);
					modelItem.put("LOT_PREFIX", dummy);
					modelItem.put("REWORK_ITEM_YN", dummy);
					modelItem.put("IN_OUT_BOUND", dummy);
					
					modelItem.put("IN_CUST_ZONE", inCustZone);
					modelItem.put("OUT_CUST_ZONE", outCustZone);
					modelItem.put("ITEM_DESC", dummy);
					modelItem.put("WORKING_TIME", dummy);
					modelItem.put("CURRENCY_NAME", dummy);
					
					modelItem.put("ITEM_CAPACITY", dummy);
					modelItem.put("SET_ITEM_TYPE", dummy);
					modelItem.put("ITEM_TMP", itemTmp);
					modelItem.put("FIX_LOC_ID", dummy);
					modelItem.put("UNIT_QTY", dummy);
					modelItem.put("UNIT_NM", dummy);
					modelItem.put("USE_YN", useYn);
					
					modelItem.put("SALES_PRICE", dummy);
					
					modelItem.put("LC_ID", lcId); // varchar2 20 not null
					modelItem.put("WORK_IP", workIp);
					
					modelItem.put("USER_NO", userNo);

					modelItem = (Map<String, Object>) dao.saveItem(modelItem);
					if (stGubun.equals("INSERT")) {
						ritemIdTemp = modelItem.get("O_MSG_NAME").toString();
					}
					if (Integer.parseInt(modelItem.get("O_MSG_CODE").toString()) != 0) {
						throw new Exception(MessageResolver.getMessage("save.error"));
					}
				} else {
					modelItem.put("USER_ID", model.get(ConstantIF.SS_USER_ID));
					modelItem.put("ITEM_ID", model.get("I_ITEM_ID" + i));
					modelItem.put("RITEM_ID", model.get("I_RITEM_ID" + i));
				}

				Map<String, Object> modelDt = new HashMap<String, Object>();
				modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
				modelDt.put("SS_CLIENT_IP", model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO)); // LC_ID
				modelDt.put("selectIds", model.get("selectIds"));
				modelDt.put("ST_GUBUN", model.get("ST_GUBUN" + i));

				modelDt.put("POOL_ID", model.get("POOL_ID" + i));
				modelDt.put("POOL_GRP_ID", model.get("POOL_GRP_ID" + i));
				modelDt.put("POOL_CODE", model.get("POOL_CODE" + i));
				modelDt.put("POOL_WGT", model.get("POOL_WGT" + i));
				modelDt.put("CUST_ID", model.get("CUST_ID" + i));

				modelDt.put("POOL_SIZE", model.get("POOL_SIZE" + i));
				modelDt.put("POOL_NM", model.get("POOL_NM" + i));
				modelDt.put("REMARK", model.get("REMARK" + i));
				modelDt.put("PROP_QTY", model.get("PROP_QTY" + i));
				modelDt.put("USE_YN", model.get("USE_YN" + i));

				modelDt.put("EXPIRY_DATE", model.get("EXPIRY_DATE" + i));
				modelDt.put("PROP_STOCK_DAY", model.get("PROP_STOCK_DAY" + i));
				modelDt.put("TIME_PERIOD_DAY", model.get("TIME_PERIOD_DAY" + i));
				modelDt.put("IN_WH_ID", model.get("IN_WH_ID" + i));
				modelDt.put("IN_ZONE_ID", model.get("IN_ZONE_ID" + i));

				modelDt.put("UNIT_PRICE", model.get("UNIT_PRICE" + i));
				modelDt.put("ASSET_TYPE_CD", model.get("ASSET_TYPE_CD" + i));
				modelDt.put("TAG_PREFIX", model.get("TAG_PREFIX" + i));
				modelDt.put("COMP_EPC_CD", model.get("COMP_EPC_CD" + i));
				modelDt.put("POOL_BAR_CD", model.get("POOL_BAR_CD" + i));

				modelDt.put("COLOR", model.get("COLOR" + i));
				modelDt.put("ITF_CD", model.get("ITF_CD" + i));
				modelDt.put("SIZE_H", model.get("SIZE_H" + i));
				modelDt.put("SIZE_L", model.get("SIZE_L" + i));
				modelDt.put("SIZE_W", model.get("SIZE_W" + i));
				modelDt.put("REP_UOM_ID", model.get("REP_UOM_ID" + i));
				modelDt.put("TYPE_ST", model.get("TYPE_ST" + i));
				modelDt.put("PLT_STAT", model.get("PLT_STAT" + i));
				modelDt.put("AUTO_PLT_TRANS_YN", model.get("AUTO_PLT_TRANS_YN" + i));
				
				modelDt.put("POOL_WGT_ARROW_RANGE", model.get("POOL_WGT_ARROW_RANGE" + i));
				
				modelDt.put("APPLICATION_MODEL", model.get("APPLICATION_MODEL" + i));
				modelDt.put("MATERIAL", model.get("MATERIAL" + i));
				modelDt.put("MIN_RCV_QTY", model.get("MIN_RCV_QTY" + i));
				modelDt.put("RCV_INSPECTION_POINT", model.get("RCV_INSPECTION_POINT" + i));
				modelDt.put("RCV_LEAD_TIME", model.get("RCV_LEAD_TIME" + i));

				if (stGubun.equals("INSERT") || stGubun.equals("UPDATE")) {
					modelDt.put("RITEM_ID", ritemIdTemp);
				} else {
					modelDt.put("RITEM_ID", model.get("RITEM_ID" + i));
				}

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert(modelDt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.update(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "RITEM");
					modelSP.put("I_CODE", model.get("I_RITEM_ID" + i));			
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }
					dao.delete(modelDt);
					dao.deleteItem(modelItem);
					dao.deleteRitem(modelItem);
					
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));
			
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());
			
		} catch (Exception e) {
			throw e;
		}
		
		return m;
	}

	/**
	 * 대체 Method ID : listExcel 대체 Method 설명 : 엑셀다운 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		model.put("pageIndex", "1");
		model.put("pageSize", "60000");

		map.put("LIST", dao.list(model));

		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listOrderPool 대체 Method 설명 : 물류용기팝업 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listOrderPool(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.orderPoolList(model));
		return map;
	}

	/**
	 * Method ID : saveUploadData Method 설명 : 엑셀파일 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData(model, list);

			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}
	
	/**
	 * Method ID : saveUploadData_simple 
	 * Method 설명 : 엑셀파일 저장 
	 * 작성자 : sing09
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveUploadData_simple(Map<String, Object> model, List list) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		int insertCnt = (list != null) ? list.size() : 0;
		try {
			dao.saveUploadData_simple(model, list);
			
			m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}));
			m.put("MSG_ORA", "");
			m.put("errCnt", errCnt);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save upload data :", e);
			}
			throw e;
		}
		return m;
	}
	
	/**
	 * Method ID : save Method 설명 : 통합 HelpDesk 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveItemImg(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
				Map<String, Object> modelDt = new HashMap<String, Object>();
				// 확장자 잘라내기
				String str = (String) model.get("D_ATCH_FILE_NAME");
				String ext = "." + str.substring((str.lastIndexOf('.') + 1));
				// 파일 ID 만들기
				int fileSeq = 1;
				String fileId = "";
				fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
				// 저장준비
				modelDt.put("FILE_ID", fileId);
				modelDt.put("ATTACH_GB", "POOL"); // 통합 HELPDESK 업로드
				modelDt.put("FILE_VALUE", "wmsms190"); // 기존코드 "tmsba230" 로
														// 하드코딩되어 있음 파일구분값?
				modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE")); // 서버저장경로
				modelDt.put("ORG_FILENAME", model.get("D_ATCH_FILE_NAME")); // 원본파일명
				modelDt.put("FILE_EXT", ext); // 파일 확장자
				modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));// 파일 사이즈
				// 저장
				String fileMngNo = (String) dao.fileUpload(modelDt);
				// 리턴값 파일 id랑 파일경로
				model.put("IMAGE_ID", fileId);
				model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));
				
				dao.insertInfo(model);
			}

			map.put("MSG", MessageResolver.getMessage("insert.success"));
		} catch (Exception e) {
			throw e;
		}
		return map;
	}
}
