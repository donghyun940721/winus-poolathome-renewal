package com.logisall.winus.wmsms.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMS010Dao")
public class WMSMS010Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : list Method 설명 : 화주 정보 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsms010.search", model);
	}

	/**
	 * Method ID : saveCust Method 설명 : 화주 정보 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public String[] saveCust(Map<String, Object> model) {
		String[] val = {"", "0"};
		executeUpdate("wmsms010.pk_wmsms010.sp_sav_cust", model);
		int err = Integer.parseInt(model.get("O_MSG_CODE").toString());
		if (err != 0) {
			val[0] = model.get("O_MSG_NAME").toString();
			val[1] = model.get("O_MSG_CODE").toString();
		}
		return val;
	}

	/**
	 * Method ID : delete_wmsms010 Method 설명 : 화주정보 삭제 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteWmsms010(Map<String, Object> model) {
		return executeUpdate("wmsms010.delete", model);
	}

	/**
	 * Method ID : delete_wmsms012 Method 설명 : 화주정보 삭제 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteWmsms012(Map<String, Object> model) {
		return executeUpdate("wmsms012.delete", model);
	}

	/**
	 * Method ID : insertCsv Method 설명 : 대용량등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void insertCsv(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if (paramMap.get("CUST_TYPE") == null || StringUtils.isEmpty((String) paramMap.get("CUST_TYPE"))) {
					paramMap.put("CUST_TYPE", "C");
				}
				if (paramMap.get("CUST_CD") != null && StringUtils.isNotEmpty((String) paramMap.get("CUST_CD"))) {

					String custId = (String) sqlMapClient.insert("wmsms010.insertCsv", paramMap);
					paramMap.put("CUST_ID", custId);

					if ("C".equals(paramMap.get("CUST_TYPE"))) {
						sqlMapClient.insert("wmsms012.insertCsv", paramMap);
					} else if ("D".equals(paramMap.get("CUST_TYPE"))) {
						sqlMapClient.insert("wmsms011.insertCsv", paramMap);
					}
				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }		
    
    /**
	 * Method ID : listQ1
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet listQ1(Map<String, Object> model) {
		return executeQueryPageWq("wmsms010.listQ1", model);
	}
	
	/**
	 * Method ID : listEmployeeQ1
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet listEmployeeQ1(Map<String, Object> model) {
		return executeQueryPageWq("wmsms010.listEmployeeQ1", model);
	}
	
	/**
     * Method ID    : saveSub
     * Method 설명      : 청구단가계약 등록
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSub(Map<String, Object> model){
        executeUpdate("wmsac010.pk_wmsac010.sp_save_cont", model);
        return model;
    }
    
    /**
     * Method ID    : saveLcSync
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveLcSync(Map<String, Object> model){
        executeUpdate("wmsms010.pk_wmsms010.sp_sav_multi_lc_cust", model);
        return model;
    }
	
	/**
     * Method ID    : saveSubPlt
     * Method 설명      : 정산용 물류용기 생성
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object saveSubPlt(Map<String, Object> model){
        executeUpdate("wmspl010.insert", model);
        return model;
    }
}
