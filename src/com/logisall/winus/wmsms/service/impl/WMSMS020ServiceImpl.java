package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS020Service")
public class WMSMS020ServiceImpl implements WMSMS020Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMS020Dao")
    private WMSMS020Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 화주정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if ( model.get("S_CODE_TYPE_CD") == null ) {
                map.put("PROCESSINFO", dao.listE1(model));
                map.put("PLT_MANAGE_YN", dao.getPropertInfo(model));
            } else {
                map.put("LIST", dao.listE1(model));
            }
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     * Method ID : savePropertyInfo
     * Method 설명 : 물류센터정보관리 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> savePropertyInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            
            Map<String, Object> modelSP = new HashMap<String, Object>();
            
            int iuCnt = Integer.parseInt(model.get("SAVE_COUNT").toString());
            String[] lcId = new String[iuCnt];
            String[] managePltYn = new String[iuCnt];            
            for(int i = 0 ; i < iuCnt ; i ++){
                lcId[0] = (String)model.get("SS_SVC_NO");
                managePltYn[0] = (String)model.get("PLT_MANAGE_YN");
            }
            modelSP.put("I_LC_ID",          lcId);
            modelSP.put("I_MANAGE_PLT_YN",  managePltYn);
            modelSP.put("I_USER_NO",        model.get("SS_USER_ID"));
            modelSP.put("I_WORK_IP",        model.get("SS_CLIENT_IP"));
            
            // SP실행 - 기존에 없는 항목일 경우 O_CONFIG_ID 에 키값 반환
            dao.updatePropertInfo(modelSP);
            
            String workName = "WMSMS020.SP_SET_MANAGE_PLT";
            String retCode = (String)modelSP.get("O_MSG_CODE");
            String retMsg = (String)modelSP.get("O_MSG_NAME");
            
            // SP실행결과 판별(정상이 아닐 경우, BizException 발생)
            ServiceUtil.isValidReturnCode(workName, retCode, retMsg);
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", "0");
           
        // SP실행결과가 정상이 아닐 경우, BizException 발생 
        } catch (BizException be) {
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("errCnt", "1");
            
        } catch(Exception e) {
            throw e;
        }
        return m;
    }
    
  
    /**
     * Method ID : save
     * Method 설명 : 물류센터정보관리 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int updateCnt = 0;
        try{
            
            Map<String, Object> modelSP = new HashMap<String, Object>();
            
            modelSP.put("I_LC_ID",          model.get("SS_SVC_NO"));
            // modelSP.put("I_LC_ID",          "0000020023");
            modelSP.put("I_CONFIG_NM",      model.get("CODE_TYPE_CD"));
            modelSP.put("I_CODE_TYPE_CD",   model.get("CODE_TYPE_CD"));
            modelSP.put("I_USER_ID",        model.get("SS_USER_ID"));
            
            // SP실행 - 기존에 없는 항목일 경우 O_CONFIG_ID 에 키값 반환
            dao.updateConfig(modelSP);
            
            // O_CONFIG_ID 키값이 있을 경우, 이하 UPDATE에 사용
            String configId = null;
            if ( modelSP.get("O_CONFIG_ID") != null ) {
                configId = (String)modelSP.get("O_CONFIG_ID");
            }
            boolean isNewConfigId = !StringUtils.isEmpty(configId);
            
            int iuCnt = Integer.parseInt(model.get("SAVE_COUNT").toString());
            for(int i = 0 ; i < iuCnt; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("CODE_TYPE_CD", model.get("CODE_TYPE_CD"));
                
                // SP실행결과 O_CONFIG_ID 값이 번환되었을 경우
                if ( isNewConfigId ) {
                    modelDt.put("CONFIG_ID",    configId);
                    
                // SP실행결과 O_CONFIG_ID 값이 null일 경우, 기존의 값을 사용
                } else {
                    modelDt.put("CONFIG_ID",    model.get("CONFIG_ID" +i));
                }
                modelDt.put("USE_YN",       model.get("USE_YN" +i));
                modelDt.put("BA_CODE_CD",   model.get("BA_CODE_CD" +i));
                modelDt.put("SS_USER_NO",   model.get("SS_USER_NO"));
                
                int cnt = dao.update(modelDt);
                updateCnt = updateCnt + cnt;
            }
            
            if ( updateCnt < 1 ) {
                throw new BizException("UPDATE ERROR");
            }
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", "0");
        } catch (BizException be) {
        	log.info(be.getMessage());
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("errCnt", "1");
             
        } catch(Exception e) {
        	log.info(e.getMessage());
            throw e;
        }
        return m;
    }
    
}
