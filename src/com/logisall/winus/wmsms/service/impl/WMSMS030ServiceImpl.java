package com.logisall.winus.wmsms.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.WMSMS030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMS030Service")
public class WMSMS030ServiceImpl implements WMSMS030Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS030Dao")
	private WMSMS030Dao dao;

	/**
	 * 
	 * 대체 Method ID   : selectLc
	 * 대체 Method 설명    : selectLc
	 * 작성자                      : chsong
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> selectLc(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		map.put("LCLIST", dao.selectLc(model));

		return map;
	}
	
	/**
	 * Method ID : list Method 설명 : 물류센터정보관리 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if (model.get("page") == null) {
				model.put("pageIndex", "1");
			} else {
				model.put("pageIndex", model.get("page"));
			}
			if (model.get("rows") == null) {
				model.put("pageSize", "20");
			} else {
				model.put("pageSize", model.get("rows"));
			}
			map.put("LIST", dao.list(model));
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}

	/**
	 * Method ID : save Method 설명 : 물류센터정보관리 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> save(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {

			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();
				Map<String, Object> modelSP = new HashMap<String, Object>();

				modelDt.put("selectIds"	, model.get("selectIds"));
				modelDt.put("LC_ID"		, model.get("LC_ID" + i));
				modelDt.put("CUST_ID"	, model.get("CUST_ID" + i));

				modelDt.put("ST_GUBUN"	, model.get("ST_GUBUN" + i));
				modelDt.put("LC_NM"		, model.get("LC_NM" + i));
				modelDt.put("REG_DT"	, model.get("REG_DT" + i));
				modelDt.put("REG_NM"	, model.get("REG_NM" + i));
				modelDt.put("LC_CD"		, model.get("LC_CD" + i));
				modelDt.put("DOCK_CNT"	, model.get("DOCK_CNT" + i));

				modelDt.put("CUST_CD"	, model.get("CUST_CD" + i));
				modelDt.put("CUST_NM"	, model.get("CUST_NM" + i));
				modelDt.put("BIZ_NO"	, model.get("BIZ_NO" + i));
				modelDt.put("REP_NM"	, model.get("REP_NM" + i));

				modelDt.put("TRADE_NM"	, model.get("TRADE_NM" + i));
				modelDt.put("ZIP"		, model.get("ZIP" + i));
				modelDt.put("ADDR"		, model.get("ADDR" + i));
				modelDt.put("TEL"		, model.get("TEL" + i));
				modelDt.put("FAX"		, model.get("FAX" + i));

				modelDt.put("I_RFID_YN"	, model.get("RFID_YN" + i));
				modelDt.put("I_WGT_YN"	, model.get("WGT_YN" + i));
				modelDt.put("I_BEST_DATE_YN", model.get("BEST_DATE_YN" + i));
				modelDt.put("I_LC_TIME_ZONE", model.get("LC_TIME_ZONE" + i));
				modelDt.put("I_WGT_TYPE"	, model.get("WGT_TYPE" + i));

				if (modelDt.get("I_RFID_YN") == null || StringUtils.isEmpty((String) modelDt.get("I_RFID_YN"))) {
					modelDt.put("I_RFID_YN", "N");
				}
				if (modelDt.get("I_WGT_YN") == null || StringUtils.isEmpty((String) modelDt.get("I_WGT_YN"))) {
					modelDt.put("I_WGT_YN", "N");
				}
				if (modelDt.get("I_BEST_DATE_YN") == null
						|| StringUtils.isEmpty((String) modelDt.get("I_BEST_DATE_YN"))) {
					modelDt.put("I_BEST_DATE_YN", "N");
				}
				if (modelDt.get("I_LC_TIME_ZONE") == null
						|| StringUtils.isEmpty((String) modelDt.get("I_LC_TIME_ZONE"))) {
					modelDt.put("I_LC_TIME_ZONE", "01");
				}
				if (modelDt.get("I_WGT_TYPE") == null
						|| StringUtils.isEmpty((String) modelDt.get("I_WGT_TYPE"))) {
					modelDt.put("I_WGT_TYPE", "KG");
				}

				modelDt.put("I_TAG_PREFIX", model.get("TAG_PREFIX" + i));
				modelDt.put("I_GLN_CD", model.get("GLN_CD" + i));
				
				modelDt.put("I_LATITUDE"		, model.get("LATITUDE" + i));
				modelDt.put("I_LONGITUDE"		, model.get("LONGITUDE" + i));
				modelDt.put("I_RACK_USE_YN"		, model.get("RACK_USE_YN" + i));
				modelDt.put("I_AUTO_RCV_STEP"	, model.get("AUTO_RCV_STEP" + i));
				modelDt.put("I_AUTO_SND_STEP"	, model.get("AUTO_SND_STEP" + i));
				modelDt.put("I_MULTI_SHIP_YN"	, model.get("MULTI_SHIP_YN" + i));
				
				modelDt.put("I_RACK_TYPE"		, model.get("RACK_TYPE" + i));
				
				modelDt.put("I_LC_TYPE"			, model.get("LC_TYPE" + i));
				modelDt.put("LC_ENG_NM"			, model.get("LC_ENG_NM" + i));
				modelDt.put("I_MANAGE_PLT_YN"	, model.get("MANAGE_PLT_YN" + i));
				modelDt.put("I_LC_USE_TGT"		, model.get("LC_USE_TGT" + i));
				modelDt.put("I_LC_USE_TY"		, model.get("LC_USE_TY" + i));
				modelDt.put("I_STOCK_ALERT_YN"	, model.get("STOCK_ALERT_YN" + i));
				modelDt.put("I_CLIENT_ORD_YN"	, model.get("CLIENT_ORD_YN" + i));
				
				modelDt.put("I_RECEIVING_LC_ID"	, model.get("RECEIVING_LC_ID" + i));
				modelDt.put("I_SHIPPING_LC_ID"	, model.get("SHIPPING_LC_ID" + i));
				//modelDt.put("I_OUTLOC", model.get("OUTLOC" + i));
				
				modelDt.put("WORK_IP", model.get("SS_CLIENT_IP"));
				modelDt.put("REG_NO", model.get("SS_USER_NO"));

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert(modelDt);

					// lc 프로시저 시작
					modelSP.put("I_CUST_ID"			, modelDt.get("CUST_ID_I"));
					modelSP.put("I_LC_CD"			, modelDt.get("LC_CD"));
					modelSP.put("I_LC_NM"			, modelDt.get("LC_NM"));
					modelSP.put("I_DOCK_CNT"		, modelDt.get("DOCK_CNT"));

					modelSP.put("I_RFID_YN"			, modelDt.get("I_RFID_YN"));
					modelSP.put("I_WGT_YN"			, modelDt.get("I_WGT_YN"));
					modelSP.put("I_BEST_DATE_YN"	, modelDt.get("I_BEST_DATE_YN"));
					modelSP.put("I_LC_TIME_ZONE"	, modelDt.get("I_LC_TIME_ZONE"));
					modelSP.put("I_WGT_TYPE"		, modelDt.get("I_WGT_TYPE"));
					
					modelSP.put("I_TAG_PREFIX"		, modelDt.get("I_TAG_PREFIX"));
					modelSP.put("I_GLN_CD"			, modelDt.get("I_GLN_CD"));
					
					modelSP.put("I_LATITUDE"		, modelDt.get("I_LATITUDE"));
					modelSP.put("I_LONGITUDE"		, modelDt.get("I_LONGITUDE"));
					modelSP.put("I_RACK_USE_YN"		, modelDt.get("I_RACK_USE_YN"));
					modelSP.put("I_AUTO_RCV_STEP"	, modelDt.get("I_AUTO_RCV_STEP"));
					modelSP.put("I_AUTO_SND_STEP"	, modelDt.get("I_AUTO_SND_STEP"));
					modelSP.put("I_MULTI_SHIP_YN"	, modelDt.get("I_MULTI_SHIP_YN"));
					
					modelSP.put("I_RACK_TYPE"		, modelDt.get("I_RACK_TYPE"));
					
					modelSP.put("I_LC_TYPE"			, modelDt.get("I_LC_TYPE"));
					modelSP.put("I_LC_ENG_NM"		, modelDt.get("LC_ENG_NM"));
					modelSP.put("I_MANAGE_PLT_YN"	, modelDt.get("I_MANAGE_PLT_YN"));
					modelSP.put("I_LC_USE_TGT"		, modelDt.get("I_LC_USE_TGT"));
					modelSP.put("I_LC_USE_TY"		, modelDt.get("I_LC_USE_TY"));
					modelSP.put("I_STOCK_ALERT_YN"	, modelDt.get("I_STOCK_ALERT_YN"));
					modelSP.put("I_CLIENT_ORD_YN"	, modelDt.get("I_CLIENT_ORD_YN"));
					
					modelSP.put("I_RECEIVING_LC_ID"	, modelDt.get("I_RECEIVING_LC_ID"));
					modelSP.put("I_SHIPPING_LC_ID"	, modelDt.get("I_SHIPPING_LC_ID"));
					//modelSP.put("I_OUTLOC", modelDt.get("I_OUTLOC"));
					
					modelSP.put("WORK_IP", modelDt.get("WORK_IP"));
					modelSP.put("USER_NO", modelDt.get("REG_NO"));

					String[] val = dao.insertLc(modelSP);
					m.put("MSG", MessageResolver.getMessage("insert.success"));
					m.put("errCnt", errCnt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {

					dao.updateCust(modelDt);
					dao.updateLc(modelDt);

					m.put("MSG", MessageResolver.getMessage("update.success"));
					m.put("errCnt", errCnt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					
					modelSP.put("I_TYPE", "LC");
					modelSP.put("I_CODE", model.get("LC_ID" + i));					
					String checkExistData = dao.checkExistData(modelSP);
					if ( checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
								MessageResolver.getMessage("delete.exist." + checkExistData)
						}) );
                    }
					
					dao.delete(modelDt);
					
					m.put("MSG", MessageResolver.getMessage("delete.success"));
					m.put("errCnt", errCnt);
				} else {
					errCnt++;
					m.put("MSG", MessageResolver.getMessage("delete.success"));
					m.put("errCnt", errCnt);
					throw new Exception(MessageResolver.getMessage("save.error"));

				}
			}
		} catch (BizException be) {
			m.put("MSG", be.getMessage());
			
		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	/**
	 * Method ID : listExcel Method 설명 : 물류센터정보관리 엑셀다운 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		model.put("pageIndex", "1");
		model.put("pageSize", "60000");

		map.put("LIST", dao.list(model));

		return map;
	}

}
