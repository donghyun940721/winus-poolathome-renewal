package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;


public interface WMSMS014Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception;
    public Map<String, Object> userIdValidate(Map<String, Object> model) throws Exception;
}
