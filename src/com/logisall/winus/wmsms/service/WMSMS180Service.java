package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS180Service {
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSub_T1(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteSub(Map<String, Object> model) throws Exception;
}
