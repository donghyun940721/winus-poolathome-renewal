package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS092Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE7(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE6(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubExcelE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubExcelE6(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> getItemSubGrid(Map<String, Object> model) throws Exception;
}

