package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS096Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> getItemList(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listPool(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveCsvItemGroup(Map<String, Object> model, List list) throws Exception;
    
}
