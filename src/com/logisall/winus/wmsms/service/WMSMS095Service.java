package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS095Service {

    public Map<String, Object> listItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveCsvItem(Map<String, Object> model, List list) throws Exception;

}
