package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS099Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetailItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveItemImg(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveLcSync(Map<String, Object> model) throws Exception;

}