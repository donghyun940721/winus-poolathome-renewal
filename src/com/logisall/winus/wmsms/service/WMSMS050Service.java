package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS050Service {

    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> driverList(Map<String, Object> model) throws Exception;
	public Map<String, Object> dspSave(Map<String, Object> model) throws Exception;
	public Map<String, Object> dspCancel(Map<String, Object> model) throws Exception;
	public Map<String, Object> dspLoad(Map<String, Object> model) throws Exception;
}
