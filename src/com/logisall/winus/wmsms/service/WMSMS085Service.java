package com.logisall.winus.wmsms.service;

import java.util.List;
import java.util.Map;

public interface WMSMS085Service {
	public Map<String, Object> listOrg(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listNoneGrid(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;	
}
