package com.logisall.winus.tmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.tmscm.service.TMSCM070Service;

@Service("TMSCM070Service")
public class TMSCM070ServiceImpl implements TMSCM070Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSCM070Dao")
    private TMSCM070Dao dao;
    
    /**
     * Method ID   : selectAuthBtn
     * Method 설명    : 버튼권한조회
     * 작성자               : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectAuthBtn(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        try {
            model.put("AUTH_CD",(String)model.get(ConstantIF.SS_USER_TYPE));
            model.put("PROGRAM_ID", model.get("vrProgramId"));
            map.put("AuthBtn", dao.selectAuthBtn(model));
        } catch (Exception e) {
            log.error(e.toString());
        }
        return map;
    }
}
