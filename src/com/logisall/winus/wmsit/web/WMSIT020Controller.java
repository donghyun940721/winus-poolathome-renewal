package com.logisall.winus.wmsit.web;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsit.service.WMSIT020Service;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIT020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT020Service")
	private WMSIT020Service service;
	
	@Resource(name = "WMSOP642Service")
	private WMSOP642Service WMSOP642_service;
	
	/*-
	 * Method ID : mn
	 * Method 설명 : B2C검수 화면
	 * 작성자 : 구연희
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSIT020.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsit/WMSIT020", service.selectItemGrp(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : mn
	 * Method 설명 : B2C검수 화면(비즈)
	 * 작성자 : 구연희
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSIT021.action")
	public ModelAndView mn2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsit/WMSIT021", service.selectItemGrp(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : mn
	 * Method 설명 : 이노서브 검수화면
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSIT030.action")
	public ModelAndView wmsit030(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsit/WMSIT030", service.selectItemGrp(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list_T1
	 * Method 설명 : B2C검수 조회
	 * 작성자 : 구연희
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT020/list_T1.action")
	public ModelAndView list_T1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID    : listSub_T1
	 * Method 설명      : B2C검수 조회
	 * 작성자                 : 구연희
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSIT020/listSub_T1.action")
	public ModelAndView listSub_T1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub_T1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	/*-
	 * Method ID     : workPickingComp
	 * Method 설명      	 : 검수완료
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIT020/workPickingComp.action")
	public ModelAndView workPickingComp(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.workPickingComp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/*-
	 * Method ID    : ordChkConfirm
	 * Method 설명      : B2C 검수 리스트 조회 (우측 그리드)
	 * 작성자                 : 구연희
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSIT020/ordChkConfirm.action")
	public ModelAndView ordChkConfirm(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.ordChkConfirm(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : listSingle
	 * Method 설명 : 이노서브검수 조회 (단포)
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT030/listSingle.action")
	public ModelAndView listSingle(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSingle(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listMulti
	 * Method 설명 : 이노서브검수 조회 (합포)
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT030/listMulti.action")
	public ModelAndView listMulti(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String,Object> m = new HashMap<String, Object>();
		try {
			m = service.listMulti(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
			m.put("MSG", e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : poolBoxList
	 * Method 설명 : 이노서브검수 조회 (박스정보)
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT030/poolBoxList.action")
	public ModelAndView poolBoxList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.poolBoxList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID     : workPickingComp
	 * Method 설명      	 : 이노서브 검수완료 -> 시리얼발행 -> 송장발행 -> 출력
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIT030/workPickingComp.action")
	public ModelAndView WMSIT030_workPickingComp(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		String errLog = "";
		try {
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			if(model.get("PARCEL_COM_TY").equals("04") || model.get("PARCEL_COM_TY").equals("06")){
				// time set
				//long beforeTime = System.currentTimeMillis();
				Map<String, Object> modelIns = new HashMap<String,Object>();
				int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
				
				errLog = "택배 송장 발행";
				//택배 송장 발행
				Map<String, Object> WMSOP642Model = new HashMap<String, Object>();
				for(int i = 0 ; i < tmpCnt; i++){
					WMSOP642Model.put("LC_ID"+i, (String)model.get("LC_ID"));
					WMSOP642Model.put("CUST_ID"+i, (String)model.get("CUST_ID"));
					WMSOP642Model.put("TRACKING_NO"+i, (String)model.get("BOX_NO"));
					WMSOP642Model.put("DENSE_FLAG"+i, (String)model.get("DENSE_FLAG"+i));
					WMSOP642Model.put("ORD_QTY"+i, (String)model.get("CHK_QTY"+i));
					WMSOP642Model.put("ORD_ID"+i, (String)model.get("ORD_ID"+i));
					WMSOP642Model.put("ORD_SEQ"+i, (String)model.get("ORD_SEQ"+i));
					WMSOP642Model.put("INVC_NO"+i, (String)model.get("INVC_NO"+i));
				}
				WMSOP642Model.put("selectIds", (String)model.get("selectIds"));
				WMSOP642Model.put("PARCEL_SEQ_YN", (String)model.get("PARCEL_SEQ_YN"));
				WMSOP642Model.put("PARCEL_COM_TY", (String)model.get("PARCEL_COM_TY"));
				WMSOP642Model.put("PARCEL_COM_TY_SEQ", (String)model.get("PARCEL_COM_TY_SEQ"));
				WMSOP642Model.put("PARCEL_ORD_TY", (String)model.get("PARCEL_ORD_TY"));
				WMSOP642Model.put("PARCEL_PAY_TY", (String)model.get("PARCEL_PAY_TY"));
				WMSOP642Model.put("PARCEL_BOX_TY", (String)model.get("PARCEL_BOX_TY"));
				WMSOP642Model.put("PARCEL_ETC_TY", (String)model.get("PARCEL_ETC_TY"));
				WMSOP642Model.put("PARCEL_PAY_PRICE", (String)model.get("PARCEL_PAY_PRICE"));
				WMSOP642Model.put("ADD_FLAG", (String)model.get("ADD_FLAG"));
				WMSOP642Model.put("DIV_FLAG", (String)model.get("DIV_FLAG"));
				
				WMSOP642Model.put("SS_CLIENT_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				WMSOP642Model.put("SS_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
				String hostUrl = request.getServerName();
				WMSOP642Model.put("hostUrl", hostUrl);
				
				m = WMSOP642_service.DlvInvcNoOrderComm(WMSOP642Model);
                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                //beforeTime = System.currentTimeMillis();
				if(!m.get("header").toString().equals("Y")){
					m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(1) : " + m.get("message"));
             		mav.addAllObjects(m);
            		return mav;
				}
				
				errLog = "택배 송장 조회";
				modelIns = new HashMap<String,Object>();
				modelIns.put("LC_ID", (String)model.get("LC_ID"));
				modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
				modelIns.put("GUBUN", (String)model.get("BOX_NO"));
				modelIns.put("ORD_ID", (String)model.get("ORD_ID0"));
				
				m = service.WMSIT030_workInvcNo(modelIns);
                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                //beforeTime = System.currentTimeMillis();
				GenericResultSet grs = (GenericResultSet)m.get("LIST");
				List list = grs.getList();
				if(list == null || list.isEmpty() == true || list.size() != 1){
					m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(2) : LIST=" + list);
             		mav.addAllObjects(m);
            		return mav;
				}
				
				String invcNo = (String)((Map<String,Object>)list.get(0)).get("INVC_NO");
				
				errLog = "검수완료";
				//검수완료
				modelIns = new HashMap<String,Object>();
				String[] ordId = new String[tmpCnt];
				String[] ordSeq = new String[tmpCnt];
				String[] ritemId = new String[tmpCnt];
				String[] uomId = new String[tmpCnt];
				String[] ordQty = new String[tmpCnt];
				String[] chkQty = new String[tmpCnt];
				for(int i = 0 ; i < tmpCnt ; i++){
					ordId[i] = (String)model.get("ORD_ID"+i);
					ordSeq[i] = (String)model.get("ORD_SEQ"+i);
					ritemId[i] = (String)model.get("RITEM_ID"+i);
					uomId[i] = (String)model.get("UOM_ID"+i);
					ordQty[i] = (String)model.get("ORD_QTY"+i);
					chkQty[i] = (String)model.get("CHK_QTY"+i);
				}
				modelIns.put("ORD_ID", ordId);
				modelIns.put("ORD_SEQ", ordSeq);
				modelIns.put("RITEM_ID", ritemId);
				modelIns.put("UOM_ID", uomId);
				modelIns.put("ORD_QTY", ordQty);
				modelIns.put("CHK_QTY", chkQty);
				modelIns.put("LC_ID", (String)model.get("LC_ID"));
				modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
				modelIns.put("ORD_TYPE", (String)model.get("ORD_TYPE"));
				modelIns.put("CHK_BOX_NO", (String)model.get("BOX_NO"));
				modelIns.put("CHK_BOX_ID", (String)model.get("BOX_ID"));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				
				m = service.WMSIT030_workPickingComp(modelIns);
                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
                //beforeTime = System.currentTimeMillis();
             	if(Integer.parseInt(m.get("errCnt").toString()) != 0 ){
             		WMSOP642Model = new HashMap<String, Object>();
             		WMSOP642Model.put("INVC_NO", invcNo);
             		WMSOP642_service.wmsdf110DelYnUpdate(WMSOP642Model);
             		m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(3) : " + m.get("MSG"));
             		mav.addAllObjects(m);
            		return mav;
             	}
             	
             	errLog = "시리얼번호 발행";
             	modelIns = new HashMap<String,Object>();
				ArrayList<String> serialOrdId = new ArrayList<String>();
				ArrayList<String> serialOrdSeq = new ArrayList<String>();
				ArrayList<String> serialRitemId = new ArrayList<String>();
				ArrayList<String> serialUomId = new ArrayList<String>();
				ArrayList<String> serialOrdQty = new ArrayList<String>();
				ArrayList<String> serialChkQty = new ArrayList<String>();
				ArrayList<String> serialNo = new ArrayList<String>();
				int serialCount = 0;
				for(int i = 0 ; i < tmpCnt ; i++){
					if(model.get("SERIAL_CHK_YN"+i).equals("Y")){
						String tempOrdId = (String)model.get("ORD_ID"+i);
						String tempOrdSeq = (String)model.get("ORD_SEQ"+i);
						String tempRitemId = (String)model.get("RITEM_ID"+i);
						String tempUomId = (String)model.get("UOM_ID"+i);
						String tempOrdQty = (String)model.get("ORD_QTY"+i);
						String tempChkQty = (String)model.get("CHK_QTY"+i);
						String[] tmepSerialNo = model.get("SERIAL_NO"+i).toString().split(",");
						for(int j = 0 ; j < tmepSerialNo.length ; j++){
							serialOrdId.add(tempOrdId);
							serialOrdSeq.add(tempOrdSeq);
							serialRitemId.add(tempRitemId);
							serialUomId.add(tempUomId);
							serialOrdQty.add(tempOrdQty);
							serialChkQty.add(tempChkQty);
							serialNo.add(tmepSerialNo[j]);
							serialCount++;
						}
					}
				}
				if(serialCount > 0){
					modelIns.put("ORD_ID", serialOrdId.toArray(new String[serialOrdId.size()]));
					modelIns.put("ORD_SEQ", serialOrdSeq.toArray(new String[serialOrdSeq.size()]));
					modelIns.put("RITEM_ID", serialRitemId.toArray(new String[serialRitemId.size()]));
					modelIns.put("UOM_ID", serialUomId.toArray(new String[serialUomId.size()]));
					modelIns.put("ORD_QTY", serialOrdQty.toArray(new String[serialOrdQty.size()]));
					modelIns.put("CHK_QTY", serialChkQty.toArray(new String[serialChkQty.size()]));
					modelIns.put("SERIAL_NO", serialNo.toArray(new String[serialNo.size()]));
					modelIns.put("LC_ID", (String)model.get("LC_ID"));
					modelIns.put("CUST_ID", (String)model.get("CUST_ID"));
					modelIns.put("ORD_TYPE", (String)model.get("ORD_TYPE"));
					modelIns.put("CHK_BOX_NO", (String)model.get("BOX_NO"));
					modelIns.put("CHK_BOX_ID", (String)model.get("BOX_ID"));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	             	m = service.WMSIT030_workSerialComp(modelIns);
	                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
	                //beforeTime = System.currentTimeMillis();
	             	if(Integer.parseInt(m.get("errCnt").toString()) != 0 ){
	             		m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(4) : " + m.get("MSG"));
	             		mav.addAllObjects(m);
	            		return mav;
	             	}
	             	
				}
				
				errLog = "택배 송장 출력";
				modelIns = new HashMap<String,Object>();
				for(int i = 0 ; i < tmpCnt ; i++){
					modelIns.put("ORD_ID"+i, (String)model.get("ORD_ID"+i));
					modelIns.put("ORD_SEQ"+i, (String)model.get("ORD_SEQ"+i));
					modelIns.put("INVC_NO"+i, invcNo);
				}
				modelIns.put("PARCEL_COM_TY", (String)model.get("PARCEL_COM_TY"));
				modelIns.put("ORDER_FLAG", "01");
				modelIns.put("selectIds", tmpCnt);
				
				
				modelIns.put(ConstantIF.SS_SVC_NO, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put(ConstantIF.SS_USER_NO, (String)model.get(ConstantIF.SS_USER_NO));
				m = WMSOP642_service.dlvPrintPoiNoUpdate(modelIns);
                //System.out.println(errLog+" Time : "+(System.currentTimeMillis()-beforeTime)+"ms");
				if(!m.get("POI_NO_YN").equals("Y") || m.get("POI_NO") == null){
					m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(5) : " + m.get("MSG"));
             		mav.addAllObjects(m);
            		return mav;
				}
				
			}else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : "+e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID     : pickigCompCancel
	 * Method 설명      	 : 이노서브 검수삭제
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIT030/pickigCompCancel.action")
	public ModelAndView pickigCompCancel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		String errLog = "검수 삭제";
		try {	
			m = service.pickigCompCancel(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", errLog + "과정에서 오류가 발생하였습니다.(0) : "+e.getMessage());
		}
		mav.addAllObjects(m);
		return mav;
	}
}