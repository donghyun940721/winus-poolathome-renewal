package com.logisall.winus.wmsit.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsit.service.WMSIT010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIT010Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSIT010Service")
	private WMSIT010Service service;
	
	/*-
	 * Method ID : mn
	 * Method 설명 : B2B검수 화면
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSIT010.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsit/WMSIT010", service.selectItemGrp(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : ordRitemList
	 * Method 설명 : B2B검수 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT010/ordRitemList.action")
	public ModelAndView ordRitemList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.ordRitemList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID 	: getBoxNum
	 * Method 설명  : B2B검수 box no 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIT010/getBoxNum.action")
	public ModelAndView getBoxNum(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getBoxNum(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : chkConfirmB2B
	 * Method 설명 : B2B검수
	 * 작성자 : sing09
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSIT010/chkConfirmB2B.action")
	public ModelAndView chkConfirmB2B(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.chkConfirmB2B(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : ordChkConfirm
	 * Method 설명      : B2B 검수 리스트 조회 (우측 그리드)
	 * 작성자                 : 이성중
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSIT010/ordChkConfirm.action")
	public ModelAndView ordChkConfirm(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.ordChkConfirm(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
}