package com.logisall.winus.wmsit.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsit.service.WMSIT010Service;
import com.logisall.winus.wmsop.service.impl.WMSOP520Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIT010Service")
public class WMSIT010ServiceImpl extends AbstractServiceImpl implements WMSIT010Service {
    
    @Resource(name = "WMSIT010Dao")
    private WMSIT010Dao dao;


    @Resource(name = "WMSOP520Dao")
    private WMSOP520Dao dao1;
    
    /**
     * Method ID   : selectItemGrp
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao1.selectItemGrp(model));
//        map.put("ITEMGRP98", dao1.selectItemGrp98(model));
//        map.put("ITEMGRP99", dao1.selectItemGrp99(model));
        return map;
    }
    
    /**
     * Method ID : ordRitemList
     * Method 설명 : B2B검수 조회
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> ordRitemList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.ordRitemList(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID : chkConfirmB2B
     * Method 설명 : B2B검수
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> chkConfirmB2B(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	try{
    		int boxCnt = Integer.parseInt(model.get("boxCnt").toString());
    		int totalCnt = Integer.parseInt(model.get("totalCnt").toString());
    		if(boxCnt > 0){
    			//box count 수
    			for(int i = 1 ; i <= boxCnt ; i ++){
    				int tempCnt = 0;
    				
    				for(int j = 0; j < totalCnt; j ++){
    					if(i == Integer.parseInt((String) model.get("BOX_NO_"+j))){
    						//box당 String Array 배열 수 체크
    						tempCnt++;
    					}
    				}
    				if(tempCnt > 0){
	    				String[] COMP_ORD_ID    = new String[tempCnt];
	    				String[] COMP_ORD_SEQ  	= new String[tempCnt];
	    				String[] COMP_RITEM_ID  = new String[tempCnt];
	    				String[] COMP_UOM_ID 	= new String[tempCnt];
	    				String[] COMP_ORD_QTY 	= new String[tempCnt];
	    				String[] COMP_CHK_QTY 	= new String[tempCnt];
	    				String BOX_NO = "";
	    				
	    				// 배열 사이즈 체크 우측 그리드 row는 모두 돌리면서 배열 compCnt는 배열수에 맞게 처리..
	    				int compCnt = 0;
	    				for(int j = 0; j < totalCnt; j ++){
	    					//for문  box와  total count 박스 일치 시 담아줌
	    					if(i == Integer.parseInt((String) model.get("BOX_NO_"+j))){
		    					COMP_ORD_ID[compCnt]    	= (String)model.get("COMP_ORD_ID_"	+j);
		    					COMP_ORD_SEQ[compCnt] 	= (String)model.get("COMP_ORD_SEQ_"	+j);
		    					COMP_RITEM_ID[compCnt]    = (String)model.get("COMP_RITEM_ID_"+j);
		    					COMP_UOM_ID[compCnt] 		= (String)model.get("COMP_UOM_ID_"	+j);
		    					COMP_ORD_QTY[compCnt] 	= (String)model.get("COMP_ORD_QTY_"	+j);
		    					COMP_CHK_QTY[compCnt] 	= (String)model.get("COMP_CHK_QTY_"	+j);
		    					BOX_NO 				= (String)model.get("BOX_NO_"		+j);
		    					compCnt++;
	    					}
	    				}
	    				//프로시져 담을것 
	    				Map<String, Object> modelIns = new HashMap<String, Object>();
	    				//ARRAY 형
	    				modelIns.put("COMP_ORD_ID"	, COMP_ORD_ID);
	    				modelIns.put("COMP_ORD_SEQ"	, COMP_ORD_SEQ);
	    				modelIns.put("COMP_RITEM_ID", COMP_RITEM_ID);
	    				modelIns.put("COMP_UOM_ID"	, COMP_UOM_ID);
	    				modelIns.put("COMP_ORD_QTY"	, COMP_ORD_QTY);
	    				modelIns.put("COMP_CHK_QTY"	, COMP_CHK_QTY);
	    				
	    				//VARCHAR BOX
	    				modelIns.put("COMP_CHK_BOX_NO"	, BOX_NO);
	    				
	    				//VARCHAR 형 고정
	    				modelIns.put("COMP_LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	    				modelIns.put("COMP_CUST_ID", (String)model.get("I_CUST_ID"));
	    				modelIns.put("COMP_ORD_TYPE", (String)model.get("I_ORD_TYPE"));
	    				modelIns.put("COMP_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	    				modelIns.put("COMP_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	    				
	    				
	    				//프로시저에 던지고 box count for문 계속.. 
		    			modelIns = (Map<String, Object>)dao.chkConfirmB2B(modelIns);
		    			ServiceUtil.isValidReturnCode("WMSIT010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	    			}
    			}
    		}
    		m.put("errCnt", 0);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(BizException be) {
    		m.put("errCnt", -1);
    		m.put("MSG", be.getMessage() );
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * Method ID : ordChkConfirm
     * Method 설명 : B2B 검수 리스트 조회 (우측 그리드)
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> ordChkConfirm(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.ordChkConfirm(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
    	return map;
    }   
    
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            model.put("inKey", "ORD01");
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : getBoxNum
     * Method 설명 : box no 조회
     * 작성자 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getBoxNum(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("BOXNUM", dao.getBoxNum(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
}
