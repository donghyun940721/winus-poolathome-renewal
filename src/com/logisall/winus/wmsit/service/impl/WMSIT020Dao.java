package com.logisall.winus.wmsit.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIT020Dao")
public class WMSIT020Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list_T1
     * Method 설명 : B2B검수 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet list_T1(Map<String, Object> model) {
        return executeQueryPageWq("wmsit020.list_T1", model);
    }   
    
    /**
     * Method ID : listSub_T1
     * Method 설명 : B2B 박스검수  조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listSub_T1(Map<String, Object> model) {
        return executeQueryPageWq("wmsit020.listSub_T1", model);
    }   
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    
    /**
     * Method ID    : saveOutB2BChkConfirm
     * Method 설명      : 출고 B2B 검수(PK_WMSST010)
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object saveOutB2BChkConfirm(Map<String, Object> model){
        executeUpdate("wmsit020.pk_wmsit020.sp_out_b2b_chk_confirm", model);
        return model;
    }
    
    
    /**
     * Method ID : ordChkConfirm
     * Method 설명 : B2B 검수 리스트 조회 (우측 그리드)
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet ordChkConfirm(Map<String, Object> model) {
        return executeQueryPageWq("wmsit020.ordChkConfirm", model);
    }   
    /**
     * Method ID : listSingle
     * Method 설명 : 이노서브 검수조회(단포)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet listSingle(Map<String, Object> model) {
        return executeQueryWq("wmsit030.listSingle", model);
    }   
    /**
     * Method ID : listMulti
     * Method 설명 : 이노서브 검수조회(합포)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet listMulti(Map<String, Object> model) {
        return executeQueryWq("wmsit030.listMulti", model);
    }
    /**
     * Method ID : chkOrd
     * Method 설명 : 이노서브 주문 리스트 플래그 조회(합포)
     * 작성자 : schan
     * @param model
     * @return
     */
    public Object chkOrd(Map<String, Object> model){
    	executeUpdate("wmsit030.pk_wmsdf010.sp_get_check_confirm_ord", model);
    	return model;
    }
    
    /**
     * Method ID : poolBoxList
     * Method 설명 : 이노서브 검수조회(박스마스터)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet poolBoxList(Map<String, Object> model) {
        return executeQueryWq("wmsit030.poolBoxList", model);
    }
    
    /**
     * Method ID    : WMSIT030_workPickingComp
     * Method 설명      : 이노서브 출고검수 
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public Object WMSIT030_workPickingComp(Map<String, Object> model){
        executeUpdate("wmsit030.pk_wmsdf010.sp_parcel_chk_confirm", model);
        return model;
    }
    /**
     * Method ID    : WMSIT030_workSerialComp
     * Method 설명      : 이노서브 시리얼 등록
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public Object WMSIT030_workSerialComp(Map<String, Object> model){
        executeUpdate("wmsit030.pk_wmsdf010.sp_parcel_chk_serial", model);
        return model;
    }
    /**
     * Method ID    : WMSIT030_workSerialComp
     * Method 설명      : 이노서브 송장 조회
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public GenericResultSet WMSIT030_workInvcNo(Map<String, Object> model){
    	return executeQueryWq("wmsit030.listInvcNo", model);
    }
    /**
     * Method ID    : pickigCompCancel
     * Method 설명      : 이노서브 검수취소
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public Object pickigCompCancel(Map<String, Object> model){
    	executeUpdate("wmsit030.pk_wmsdf010.sp_check_confirm_cancel", model);
    	return model;
    }
}

