package com.logisall.winus.wmsit.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIT010Dao")
public class WMSIT010Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : ordRitemList
     * Method 설명 : B2B검수 조회(좌측 그리드)
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet ordRitemList(Map<String, Object> model) {
        return executeQueryPageWq("wmsit010.ordRitemList", model);
    }   
    
    /**
     * Method ID : ordChkConfirm
     * Method 설명 : B2B 검수 리스트 조회 (우측 그리드)
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet ordChkConfirm(Map<String, Object> model) {
        return executeQueryPageWq("wmsit010.ordChkConfirm", model);
    }   
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : chkConfirmB2B
     * Method 설명 : B2B 검수
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object chkConfirmB2B(Map<String, Object> model){
    	executeUpdate("wmsit010.pk_wmsst010.sp_out_b2b_chk_confirm", model);
    	return model;
    }
    
    
    /**
     * Method ID  	 	: getBoxNum
     * Method 설명  	: 검수완료된 box no 조회 
     * 작성자           : KSJ
     * @param model
     * @return
     */
	public int getBoxNum(Map<String, Object> model) {
		return (int) executeView("wmsit010.getBoxNum", model);
	}
}

