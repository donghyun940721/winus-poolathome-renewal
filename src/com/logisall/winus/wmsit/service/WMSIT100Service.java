package com.logisall.winus.wmsit.service;

import java.util.Map;


public interface WMSIT100Service {
    public Map<String, Object> invcItemList(Map<String, Object> model) throws Exception;    
    public Map<String, Object> ordChkConfirm(Map<String, Object> model) throws Exception;    
    public Map<String, Object> chkConfirmB2B(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> getBoxNum(Map<String, Object> model) throws Exception;
}
