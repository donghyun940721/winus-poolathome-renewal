package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSLG001Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
