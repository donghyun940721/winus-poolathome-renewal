package com.logisall.winus.wmsop.service;

import java.util.Map;


public interface WMSOP700Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;    
    public Map<String, Object> list3(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}
