package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP021Service {
    public Map<String, Object> selectPoolGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
}
