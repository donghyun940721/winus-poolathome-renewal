package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

public interface WMSOP100Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE5(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE7(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveExcelInfo(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveExcelInfo_CS(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveExcelInfo_CT(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveExcelInfo_HD(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveExcelInfo_AS(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveExcelInfoMulti(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelDownCustom(Map<String, Object> model) throws Exception;
    public Map<String, Object> keyInSaveInfo(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> keyInSaveInfo_CS(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> keyInSaveInfo_CT(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> keyInSaveInfo_HD(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> keyInSaveInfo_AS(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> keyInSaveInfoMulti(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> checkInsertPossible(Map<String, Object> model) throws Exception;
    public Map<String, Object> spInsertKeyIn_DLV(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> spInsertExcel_DLV(Map<String, Object> model, List list, String[] cellName) throws Exception;
    public Map<String, Object> spInsertKeyIn_CT(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> spInsertExcel_CT(Map<String, Object> model, List list, String[] cellName) throws Exception;
    public Map<String, Object> spInsertKeyIn_CS(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> spInsertExcel_CS(Map<String, Object> model, List list, String[] cellName) throws Exception;
    public Map<String, Object> spInsertKeyIn_AS(Map<String, Object> model, String[] cellName) throws Exception;
    public Map<String, Object> spInsertExcel_AS(Map<String, Object> model, List list, String[] cellName) throws Exception;
  
}