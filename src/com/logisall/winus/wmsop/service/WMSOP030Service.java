package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

import com.m2m.jdfw5x.egov.database.GenericResultSet;


public interface WMSOP030Service {
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listNew(Map<String, Object> model) throws Exception;
    public Map<String, Object> asnList(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listByCust(Map<String, Object> model) throws Exception;
    public Map<String, Object> listByCustCJ(Map<String, Object> model) throws Exception;
    public Map<String, Object> listByCustSF(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteOrderV2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listExcelPlt(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveAlloc(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveOrdPick(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveCancelPick(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveOutComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> cancelOutReReceiving(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSimpleOut(Map<String, Object> model) throws Exception;    
    public Map<String, Object> outUpdateZero(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> getSampleExcelDown(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveExcelOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveExcelOrderSAP(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listRackSearch(Map<String, Object> model) throws Exception;
    public Map<String, Object> autoBestLocSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> ordDelSetReordInsert(Map<String, Object> model) throws Exception;
    public Map<String, Object> asnSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> WorkUpdateOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveWorkOrder(Map<String, Object> model) throws Exception;

    public Map<String, Object> changeMapping(Map<String, Object> model) throws Exception;
    public Map<String, Object> adminOrderDelete(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> getExcelDown2(Map<String, Object> model) throws Exception;
    public Map<String, Object> outOrderCntInit(Map<String, Object> model) throws Exception;
    public Map<String, Object> outWorkingCntInit(Map<String, Object> model) throws Exception;
    public Map<String, Object> customerInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> ifOutOrd(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveDlvNo(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelDlvNoTemp(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> getResult(Map<String, Object> model) throws Exception;
    public List<Map<String, Object>> getTemplate(Map<String, Object> model) throws Exception;
    public List<Map<String, Object>> getTemplateALL(Map<String, Object> model) throws Exception;
    public List<Map<String, Object>> getTemplateInfoV2(Map<String, Object> model) throws Exception;
    public List<Map<String, Object>> getTemplateType(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> saveExcelOrderJava(Map<String, Object> model, Map<String, Object> model2) throws Exception;
	public Map<String, Object> saveExcelOrderJavaALL(Map<String, Object> model, Map<String, Object> model2) throws Exception;
	public Map<String, Object> saveExcelOrderJavaALLCheck(Map<String, Object> model, Map<String, Object> model2) throws Exception;
	public Map<String, Object> beforeSavecheckOrgOrdNum(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveExcelOrderJavaCust(Map<String, Object> model, Map<String, Object> model2) throws Exception;
	public Map<String, Object> saveExcelOrderJavaAllowBlank(Map<String, Object> model, Map<String, Object> model2) throws Exception;//공란허용 공통로직
	public Map<String, Object> saveExcelOrderJavaAllowBlankALL(Map<String, Object> model, Map<String, Object> model2) throws Exception;//공란허용 공통로직
	public Map<String, Object> saveExcelOrderJavaB2C(Map<String, Object> model, Map<String, Object> model2) throws Exception;
	public Map<String, Object> saveExcelOrderJavaB2T(Map<String, Object> model, Map<String, Object> model2) throws Exception;
	public Map<String, Object> saveExcelOrderJavaB2O(Map<String, Object> model, Map<String, Object> model2) throws Exception;
	public Map<String, Object> saveExcelOrderJavaB2D(Map<String, Object> model, Map<String, Object> model2) throws Exception;
	
	public Map<String, Object> saveExcelOrderJavaCommonB2C_TS(Map<String, Object> model, Map<String, Object> model2) throws Exception;

	public Map<String, Object> saveListRtnOrderJavaB2TS(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveListRtnOrderJavaB2AS(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> autoDeleteLocSave(Map<String, Object> model) throws Exception;
	public Map<String, Object> autoBestLocSaveMulti(Map<String, Object> model) throws Exception;
	public Map<String, Object> autoBestLocSaveMultiV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> autoBestLocSaveMultiPart(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> listExcelB2C(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveCjDataInsert(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustCJ_setParam(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustCJ_ordInvcNoConf(Map<String, Object> model) throws Exception;
	public Map<String, Object> cjConfListInsertToWMSDF020(Map<String, Object> model) throws Exception;
	public Map<String, Object> cjConfListInsertToV_RCPT_WINUS010(Map<String, Object> model) throws Exception;
	public Map<String, Object> cjConfListUpdateToWMSDF020(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> godomallOrderUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> cjAddressInformationByValue(String xml) throws Exception;
	
	public Map<String, Object> saveSfDataInsert(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustCJReturn_setParam(Map<String, Object> model) throws Exception;
	public Map<String, Object> cjReturnListInsertToWMSDF020(Map<String, Object> model) throws Exception;
	public Map<String, Object> cjReturnListUpdateToWMSDF020(Map<String, Object> model) throws Exception;
	public Map<String, Object> invoicPdfDownloadSF(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> saveSfDataCancel(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustSummary(Map<String, Object> model) throws Exception;
	public Map<String, Object> autoBestLocSaveMultiV3(Map<String, Object> model) throws Exception;
	public Map<String, Object> autoDeleteLocSaveV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveOutCompleteV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> listCountByCust(Map<String, Object> model) throws Exception;
	public Map<String, Object> updatePickingTotalV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> batch(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustNoCount(Map<String, Object> model) throws Exception;
	public Map<String, Object> pickingListExcel(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> outInvalidView(Map<String, Object> model) throws Exception;
	public Map<String, Object> confirmPickingTotal(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> saveFile(Map<String, Object> model) throws Exception;
	public Map<String, Object> workingUnlock(Map<String, Object> model) throws Exception;
	public Map<String, Object> totalWorkingUnlock(Map<String, Object> model) throws Exception;
	public Map<String, Object> getPickingLog(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByOlive(Map<String, Object> model) throws Exception;
	public Map<String, Object> productOliveSoldOut(Map<String, Object> model) throws Exception;
	public Map<String, Object> cancelProductOliveSoldOut(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByOliveSummary(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByOliveDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> listCountByOlive(Map<String, Object> model) throws Exception;
	public Map<String, Object> autoBestLocSaveMultiOlive(Map<String, Object> model)  throws Exception;
	public Map<String, Object> updatePickingTotalOlive(Map<String, Object> model)  throws Exception;
	public Map<String, Object> updateAddressOlive(Map<String, Object> model) throws Exception;
	public Map<String, Object> searchAddressOlive(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> noneBillingFlag(Map<String, Object> model) throws Exception;
	public Map<String, Object> updateInvalidAddressOlive(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> outByListOm(Map<String, Object> model) throws Exception;
    public Map<String, Object> outListByDelivery(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveOutCompleteV3(Map<String, Object> model) throws Exception;
    public Map<String, Object> outPickingCancel(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> outListByKcc(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> inspectionList(Map<String, Object> model) throws Exception;
    public Map<String, Object> outTallyCancel(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listByHeader(Map<String, Object> model) throws Exception;
    public Map<String, Object> listByKccHeader(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByKccDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByKccCount(Map<String, Object> model) throws Exception;
    
	public Map<String, Object> listExcelOM(Map<String, Object> model) throws Exception;
	public Map<String, Object> nCodeExcelList(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> deleteOrderNcode(Map<String, Object> model) throws Exception;
	public Map<String, Object> delreCreAsn(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> pickingWorkOrder(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> combineOrdDegree_B2B(Map<String, Object> model) throws Exception;
	public Map<String, Object> decomposeOrdDegree_B2B(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> getCutomerOrderInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> custInfoSave(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> saveBoxRecom(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveExcelOrderJavaAllowBlank_AS(Map<String, Object> model, Map<String, Object> model2) throws Exception; //공란허용 공통로직 + 입고유형


}
