package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

import com.m2m.jdfw5x.egov.database.GenericResultSet;


public interface WMSOP338Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
	public Map<String, Object> updatePickingTotalKakao(Map<String, Object> model) throws Exception;
	public Map<String, Object> confirmPickingTotal(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveOutCompleteV2(Map<String, Object> model) throws Exception;

    public Map<String, Object> listByHeaderKakao(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustDetailKakao(Map<String, Object> model) throws Exception;
	public Map<String, Object> listCountByCust(Map<String, Object> model) throws Exception;

	public Map<String, Object> pickingListPoiNo(Map<String, Object> model) throws Exception;
}
