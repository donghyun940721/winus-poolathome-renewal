package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP210Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateAutoLabel(Map<String, Object> model) throws Exception;
    

}
