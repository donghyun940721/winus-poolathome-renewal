package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP070Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
}
