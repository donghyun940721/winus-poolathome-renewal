package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

public interface WMSOP020Service {
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> lcinfolist(Map<String, Object> model) throws Exception;
    public Map<String, Object> inOrderCntInit(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveInvcNoOrder(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelPlt(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveConfirmGrn(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveCancelGrn(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveInComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveInCompleteOutOrder(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveCancelInComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSimpleIn(Map<String, Object> model) throws Exception;    
    public Map<String, Object> checkTest(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listInOrderItemASN(Map<String, Object> model) throws Exception;    
    public Map<String, Object> saveAsnOrder(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> ifInOrdLocMapp(Map<String, Object> model) throws Exception;
    public Map<String, Object> getExcelDown2(Map<String, Object> model) throws Exception;
    public Map<String, Object> autoBestLocSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> asnOutOrd(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> autoFixLocSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> simpleInWorkingReset(Map<String, Object> model) throws Exception;
}