package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

public interface WMSOP035Service {

	public byte[] pdfDown(Map<String, Object> model) throws Exception;

	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> list2DHL(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> request2EMS(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> list2(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> outSaveComplete(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> outSaveCompleteV2(Map<String, Object> model) throws Exception;

	public Map<String, Object> searchUnshippedDHL(Map<String, Object> model) throws Exception;

	public Map<String, Object> dhlExcel(Map<String, Object> model) throws Exception;

	public Map<String, Object> saveList2DHL(Map<String, Object> model) throws Exception;

	public Map<String, Object> list2EMS(Map<String, Object> model) throws Exception;

	public Map<String, Object> emsAddress(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> addressHistory(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> addressOM010(Map<String, Object> model) throws Exception;

	public Map<String, Object> export2EMS(Map<String, Object> model) throws Exception;

	public Map<String, Object> saveExport2EMS(List list) throws Exception;

	public String getOrdId(String orgOrdId) throws Exception;

	public boolean updateBlNo(List list) throws Exception;

	public Map<String, Object> checkBlNo(Map<String, Object> model) throws Exception;

	public Map<String, Object> listE5(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> listE6(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> selectPrintDt(Map<String, Object> mode) throws Exception;
	
	public Map<String, Object> updatetPrintDt(Map<String, Object> mode) throws Exception;
}
