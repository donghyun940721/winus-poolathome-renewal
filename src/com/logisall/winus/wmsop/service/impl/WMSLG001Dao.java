package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSLG001Dao")
public class WMSLG001Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 입출고현황 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmslg001.list", model);
    }
    
    /**
     * Method ID    : listExcel
     * Method 설명      : 입출고현황 엑셀용조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listExcel(Map<String, Object> model) {
        return executeQueryPageWq("wmslg001.list", model);
    }
    
    
}
