package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Repository("WMSOP103Dao")
public class WMSOP103Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop103.list", model);
    }
//    
//    /**
//     * Method ID : insert
//     * Method 설명 : 게시판 등록
//     * 작성자 : 기드온
//     * @param model
//     * @return
//     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsop100.insert_wmsei010", model);
    }
    
    /*-
     * Method ID : updateFileRow
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public int updateFileRow(Map<String, Object> model) {
        return executeUpdate("wmsop100.updateFileRow", model);
    }
    
    /**
     * Method ID : insertExcelInfo013
     * Method 설명 : 상품군 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertExcelInfo103(Map<String, Object> model, List list) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			paramMap.put("LC_ID"     	, model.get("LC_ID"));
    			paramMap.put("REG_NO"    	, model.get("REG_NO"));
    			paramMap.put("UPLOAD_ID" 	, model.get("uploadId"));
    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));
    			paramMap.put("O_DLV_ORD_TYPE", "AS출고");
    			
    			if (   paramMap.get("O_DLV_COM_ID")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
    			    && paramMap.get("O_BUYED_DT")     != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
//    				if(paramMap.get("O_DLV_ORD_TYPE") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_TYPE"))){
//    					String existRsSet = (String)sqlMapClient.queryForObject("wmsop103.selectExistSalesOrdId", paramMap);
    				
    					// 1.고객관리 입력 WMSCT010
            			String getSalesCustId = (String)sqlMapClient.insert("wmsop103.insertExcelInfoSalesCust", paramMap);
            			
            			// 2. WMSCT010 입력 후 채번 된 SALES_CUST_ID 참조
            			paramMap.put("RS_SALES_CUST_ID", getSalesCustId);
            			
            			// 3. 배송관리 입력 WMSSP010
            			String getRstDlvOrdId = (String)sqlMapClient.insert("wmsop103.insertExcelInfoDlvCust", paramMap);
            			
            			paramMap.put("RS_DLV_ORD_ID", getRstDlvOrdId);
            			sqlMapClient.insert("wmsop103.updateExcelInfoSalesCust", paramMap);       			
            			            			
            			
            			// AS출고 주문 일 경우 : 주문등록과 함께 AS내역 입력
//            			if(paramMap.get("O_DLV_ORD_TYPE").equals("AS출고")){
            				paramMap.put("vrSalesCustId"			, getSalesCustId);
        					paramMap.put("vrSrchAsCsType"			, "AS");
        					paramMap.put("vrSrchRitemId"			, paramMap.get("O_PRODUCT"));
        					paramMap.put("vrSrchAsCsLc"				, model.get("LC_ID"));
        					paramMap.put("vrSrchBuyDt"				, paramMap.get("O_BUYED_DT"));
        					paramMap.put("vrSrchReqId"				, model.get("REG_NO"));
        					
        					paramMap.put("vrPayReqYn"				, paramMap.get("O_PAY_REQ_YN"));	//처리유형(접수), C 유상, F무상
        					paramMap.put("vrSrchEtc"				, paramMap.get("O_ETC1"));		//내역상세(인수증메모)
        					paramMap.put("vrSrchMemo"				, paramMap.get("O_MEMO"));		//처리요청내역(처리내역_배송메모)
        					paramMap.put("TextReqContents"			, paramMap.get("O_REG_CONTENTS")); //접수내역
        					
        					paramMap.put("AS_SALES_CUST_ID"			, getSalesCustId);
        					sqlMapClient.insert("wms01103.asUpload"	, paramMap);
//            			}
//        			}else{
//        				throw new Exception("Exception:엑셀 데이터 중 등록구분이 없습니다. 등록오류.");
//        			}
    			}else{
    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
    			}
    		}
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }
    
    /**
     * Method ID : insertExcelInfo
     * Method 설명 : 상품군 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertExcelInfoUpdate(Map<String, Object> model, List list) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			paramMap.put("LC_ID"     	, model.get("LC_ID"));
    			paramMap.put("REG_NO"    	, model.get("REG_NO"));
    			paramMap.put("UPLOAD_ID" 	, model.get("uploadId"));
    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));
    			System.out.println(paramMap.get("O_DLV_ORD_STAT") + " // " + paramMap.get("O_DLV_ORD_TYPE"));
    			if (   paramMap.get("O_DLV_COM_ID") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
    			    && paramMap.get("O_BUYED_DT")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
    				if(   paramMap.get("O_DLV_ORD_STAT") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_STAT"))
    				   && paramMap.get("O_DLV_ORD_TYPE") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_TYPE"))){
    					String existRsSet = (String)sqlMapClient.queryForObject("wmsop100.selectExistSalesOrdId", paramMap);
        				if(existRsSet == null && StringUtils.isEmpty(existRsSet)){
        					throw new Exception("Exception:등록된 주무번호가 없습니다.("+paramMap.get("O_ORG_ORD_ID")+") 등록오류.");
            			}else{
            				// WINUS에 동일한 고객사주문정보가 있는 데이터 일 경우 주문 업데이트 (기존주문 상태변경 작업)
        					// 등록구분이 [출고] 가 아닐경우, 과거데이터 업데이트 할 경우  : 고객사주문번호가 있고 배송상태가 배송완료가 아닐 경우만
            				// 특정 값만 업데이트 (아래 적요)
            				// 배송완료시 작동
            				
        					//existRsSet : SALES_CUST_ID||RST_DLV_ORD_ID
        					paramMap.put("RS_EXIST_SALES_CUST_ID", existRsSet.substring(0,10));
            				paramMap.put("RS_EXIST_DLV_ORD_ID"   , existRsSet.substring(10));

            				// 배송상태, 등록구분, 배송일자 업데이트
            				sqlMapClient.insert("wmsop100.updateSalesCust" , paramMap);
                			sqlMapClient.insert("wmsop100.updateDlvCust"   , paramMap);
            			}
        			}else{
        				throw new Exception("Exception:엑셀 데이터 중 배송상태 또는 등록구분 값이 없습니다. 등록오류.");
        			}
    			}else{
    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
    			}
    		}
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }
    
    /**
	 * Method ID : deleteUpdateWmssp010 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteUpdateWmssp010(Map<String, Object> model) {
		return executeUpdate("wmsop103.deleteUpdateWmssp010", model);
	}
	
	/**
	 * Method ID : deleteUpdateWmsct010 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteUpdateWmsct010(Map<String, Object> model) {
		return executeUpdate("wmsop103.deleteUpdateWmsct010", model);
	}
//	
    /**
	 * Method ID : delete 
	 * Method 설명 : 고객관리 삭제 (DEL_YN 를 Y로 수정) 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmsop103.delete", model);
	}
//	
//	/**
//     * Method ID : excelDownCustom
//     * Method 설명 : Type별 주문정보 엑셀다운로드
//     * @param model
//     * @return
//     */
//    public GenericResultSet excelDownCustom(Map<String, Object> model) {
//        return executeQueryPageWq("wmsop100.excelDownCustom", model);
//    }
//    
//    /**
//     * Method ID : insertExcelInfoMulti
//     * Method 설명 : 배송주문등록 > 엑셀주무등록 다중
//     * 작성자 : 기드온
//     * @param model
//     * @return
//     */
//    public void insertExcelInfoMulti(Map<String, Object> model, List list) throws Exception {
//		SqlMapClient sqlMapClient = getSqlMapClient();
//		try {
//			sqlMapClient.startTransaction();
//			Map<String, Object> paramMap = null;
//    		for (int i=0;i<list.size();i++) {
//    			paramMap = (Map)list.get(i);
//    			paramMap.put("LC_ID"     	, model.get("LC_ID"));
//    			paramMap.put("REG_NO"    	, model.get("REG_NO"));
//    			paramMap.put("UPLOAD_ID" 	, model.get("uploadId"));
//    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
//    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));
//    			
//    			if (   paramMap.get("O_DLV_COM_ID")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
//    			    && paramMap.get("O_BUYED_DT")     != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
//    				if(paramMap.get("O_DLV_ORD_TYPE") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_TYPE"))){
//    					
//    					String existRsSet = (String)sqlMapClient.queryForObject("wmsop100.selectExistOrdId", paramMap);
//    					
//    					
//    					if(existRsSet.equals("0")) {
//    						// 1.고객관리 입력 WMSCT010
//                			String getSalesCustId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoSalesCust", paramMap);
//                			
//                			// 2. WMSCT010 입력 후 채번 된 SALES_CUST_ID 참조
//                			paramMap.put("RS_SALES_CUST_ID", getSalesCustId);
//                			
//                			// 3. 배송관리 입력 WMSSP010
////                			String getRstDlvOrdId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoDlvCust", paramMap);		//기존
//                			String getRstDlvOrdId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoDlvCustMulti", paramMap);
//                			paramMap.put("RS_DLV_ORD_ID", getRstDlvOrdId);
//                			
//                			sqlMapClient.insert("wmsop100.updateExcelInfoSalesCust", paramMap);
//                			
//                			// 4. 배송관리 디테일 입력 WMSSP011
//                			sqlMapClient.insert("wmsop100.insertExcelInfoSp011", paramMap);
//                			
//                			// AS출고 주문 일 경우 : 주문등록과 함께 AS내역 입력
//                			if(paramMap.get("O_DLV_ORD_TYPE").equals("AS출고")){
//                				paramMap.put("vrSalesCustId"			, getSalesCustId);
//            					paramMap.put("vrSrchAsCsType"			, "AS");
//            					paramMap.put("vrSrchRitemId"			, paramMap.get("O_PRODUCT"));
//            					paramMap.put("vrSrchAsCsLc"				, model.get("LC_ID"));
//            					paramMap.put("vrSrchBuyDt"				, paramMap.get("O_BUYED_DT"));
//            					paramMap.put("vrSrchReqId"				, model.get("REG_NO"));
//            					paramMap.put("TextReqContents"			, paramMap.get("O_ETC1"));
//            					paramMap.put("AS_SALES_CUST_ID"			, getSalesCustId);
//            					sqlMapClient.insert("wms01100.asUpload"	, paramMap);
//                			}
//                			
//    					}else{
//    						String existDlvOrdId = (String)sqlMapClient.queryForObject("wmsop100.selectDlvOrdId", paramMap);
//    						paramMap.put("DLV_ORD_ID", existDlvOrdId);
//    						System.out.println("DLV_ORD_ID : "+paramMap.get("DLV_ORD_ID"));
//    						// 4. 배송관리 디테일 입력 WMSSP011
//                			sqlMapClient.insert("wmsop100.insertExcelInfoSp011", paramMap);
//    					}
//    				
//            			
//        			}else{
//        				throw new Exception("Exception:엑셀 데이터 중 등록구분이 없습니다. 등록오류.");
//        			}
//    			}else{
//    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
//    			}
//    		}
//    		sqlMapClient.endTransaction();
//		} catch(Exception e) {
//			e.printStackTrace();
//			throw e;
//			
//		} finally {
//			if (sqlMapClient != null) {
//				sqlMapClient.endTransaction();
//			}
//		}
//    }
}
