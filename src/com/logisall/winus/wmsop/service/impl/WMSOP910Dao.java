package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP910Dao")
public class WMSOP910Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID    : listE1
     * Method 설명      : 입고관리 조회
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listE1(Map<String, Object> model) {
    	String LC_ID = (String)model.get("LC_ID");
    	if (LC_ID != null && StringUtils.isNotEmpty(LC_ID) && LC_ID.equals("0000002940")){//kcc
    		return executeQueryPageWq("wmsop910.listE1Kcc", model);
    	}else{
    		return executeQueryPageWq("wmsop910.listE1", model);
    	}
    }
    
    /**
     * Method ID    : listE2
     * Method 설명      : 출고관리 조회
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	if(model.get("SS_SVC_NO").equals("0000003620")){ // 비즈컨설팅 물류센터일 경우 (임시)
    		return executeQueryPageWq("wmsop910.listE2Biz", model);
    	}else{
    		return executeQueryPageWq("wmsop910.listE2", model);
    	}
    }
    
    /**
     * Method ID    : listE3
     * Method 설명    : 화주별 출고관리  조회(OM)
     * 작성자               : KCR
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
    	if(model.get("SS_SVC_NO").equals("0000003540")){ // 링티 물류센터일 경우 (임시)
    		return executeQueryPageWq("wmsop910.listE3LingTea", model);
    	}else if(model.get("SS_SVC_NO").equals("0000002860")){
    		return executeQueryPageWq("wmsop910.listE3Olive", model);
    	}else if(model.get("SS_SVC_NO").equals("0000003620")){ // 비즈컨설팅 물류센터일 경우 (임시)
    		return executeQueryPageWq("wmsop910.listE3Biz", model);
    	}else{
    		return executeQueryPageWq("wmsop910.listE3", model);
    	}
    }
    
    /**
     * Method ID    : listE4Header
     * Method 설명    : 출고관리 조회 헤더
     * 작성자               : KCR
     * @param   model
     * @return
     */
	public Object listE4Header(Map<String, Object> model) {
		List custs = list("wmsop910.listE4Header", model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	 /**
     * Method ID    : listE4Detail
     * Method 설명    : 출고관리 조회 디테일
     * 작성자               : KCR
     * @param   model
     * @return
     */
	public Object listE4Detail(Map<String, Object> model) {
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop910.listE4Detail", model));
    	return genericResultSet;
	}

	 /**
     * Method ID   		: listE1SummaryCount
     * Method 설명      : 입출고통합관리 -> 입고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE1SummaryCount(Map<String, Object> model) {
		return executeView("wmsop910.listE1SummaryCount", model);
	}
	
	 /**
     * Method ID   		: listE2SummaryCount
     * Method 설명      : 입출고통합관리 -> B2B 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE2SummaryCount(Map<String, Object> model) {
		if(model.get("SS_SVC_NO").equals("0000003620")){
			return executeView("wmsop910.listE2BizSummaryCount", model);
		}else{
			return executeView("wmsop910.listE2SummaryCount", model);
		}
	}
	
	 /**
     * Method ID   		: listE3SummaryCount
     * Method 설명      : 입출고통합관리 -> B2C 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listE3SummaryCount(Map<String, Object> model) {
		if(model.get("SS_SVC_NO").equals("0000003620")){
			return executeView("wmsop910.listE3BizSummaryCount", model);
		}else{
			return executeView("wmsop910.listE3SummaryCount", model);
		}
	}
	
}
