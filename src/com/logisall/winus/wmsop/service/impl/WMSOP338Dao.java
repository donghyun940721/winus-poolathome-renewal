package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP338Dao")
public class WMSOP338Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    public List rawlistByHeaderKakao(Map<String, Object> model) {
		List custs = list("wmsop338.listByHeaderKakao", model);
    	return custs;
	}
    
	public Object listByHeaderKakao(Map<String, Object> model) {
		List custs = rawlistByHeaderKakao(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
    public Object listByCustDetailKakao(Map<String, Object> model) {
		
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop338.listByCustDetailKakao", model));
    	return genericResultSet;
	}

    public List rawListByCustSummaryWorkKaKao(Map<String, Object> model) {
		List custs = list("wmsop338.rawListByCustSummaryWorkKaKao", model);
    	return custs;
	}
    
    public Object updatePickingTotal(Map<String, Object> model){
        executeUpdate("wmsop0338.pk_wmsop030.sp_total_print_picking", model);
        return model;
    }
    
    public Object confirmPickingTotal(Map<String, Object> model){
        executeUpdate("wmsop338.pk_wmsop030.SP_PICKING", model);
        return model;
    }
    
    public Object saveOutComplete(Map<String, Object> model){
        executeUpdate("wmsop338.pk_wmsop030.sp_out_complete", model);
        return model;
    }
    
    public Object listCountByCust(Map<String, Object> model) {
		return executeView("wmsop338.listCountByCust", model);
	}

    public Object pickingListPoiNo(Map<String, Object> model) {
    	return executeUpdate("wmsop338.pickingListPoiNo", model);
    }
    
}
