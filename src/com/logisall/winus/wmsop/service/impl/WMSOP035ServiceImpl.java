package com.logisall.winus.wmsop.service.impl;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP035Service;
import com.logisall.winus.wmsop.vo.WMSOP035VO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import org.apache.pdfbox.multipdf.PDFMergerUtility;

@Service("WMSOP035Service")
public class WMSOP035ServiceImpl implements WMSOP035Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP035Dao")
    private WMSOP035Dao dao;
    
  
    
    /**
     * Method ID   : list
     * Method 설명    : 출고관리  조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.list(model));
        return map;
    }
    /**
     * Method ID   : searchUnshippedDHL
     * Method 설명    : 올리브영 미발행내역 조회
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> searchUnshippedDHL(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
		List list = dao.searchUnshippedDHL(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		
    		String decryptdTel = "";
    		if(mapp.get("D_TEL") != null && !((String)mapp.get("D_TEL")).trim().equals("")){
    			decryptdTel = OliveAes256.decrpyt((String) mapp.get("D_TEL")); 
    		}
    		
    		String decryptdPost = "";
    		if(mapp.get("D_POST") != null && !((String)mapp.get("D_POST")).trim().equals("")){
    			decryptdPost = OliveAes256.decrpyt((String) mapp.get("D_POST")); 
    		}
    		
    		String decryptdAddress = "";
    		if(mapp.get("D_ADDRESS") != null && !((String)mapp.get("D_ADDRESS")).trim().equals("")){
    			decryptdAddress = OliveAes256.decrpyt((String) mapp.get("D_ADDRESS")); 
    		}
    		
    		String decryptdAddress2 = "";
    		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
    			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
    		}
    		
    		String decryptdCity = (String) mapp.get("D_CITY");
    		String decryptdProvince = (String) mapp.get("D_PROVINCE");
    		
    		String decryptjTel = "";
    		if(mapp.get("J_TEL") != null && !((String)mapp.get("J_TEL")).trim().equals("")){
    			decryptjTel = OliveAes256.decrpyt((String) mapp.get("J_TEL")); 
    		}
    		 
    		String decryptjPost = "";
    		if(mapp.get("J_POST") != null && !((String)mapp.get("J_POST")).trim().equals("")){
    			decryptjPost = OliveAes256.decrpyt((String) mapp.get("J_POST")); 
    		}
    		
    		String decryptjAddress = "";
    		if(mapp.get("J_ADDRESS") != null && !((String)mapp.get("J_ADDRESS")).trim().equals("")){
    			decryptjAddress = OliveAes256.decrpyt((String) mapp.get("J_ADDRESS")); 
    		}

    		String decryptjAddress2 = "";
    		if(mapp.get("J_ADDRESS2") != null && !((String)mapp.get("J_ADDRESS2")).trim().equals("")){
    			decryptjAddress2 = OliveAes256.decrpyt((String)mapp.get("J_ADDRESS2"));
    		}
    		
    		String jCity = (String)mapp.get("J_CITY");
    		String jProvince = (String)mapp.get("J_PROVINCE");
    		
    		mapp.put("D_TEL", decryptdTel);
    		mapp.put("D_POST", decryptdPost);
    		mapp.put("D_PROVINCE", decryptdProvince);
    		mapp.put("D_ADDRESS", decryptdAddress);
    		mapp.put("D_ADDRESS2", decryptdAddress2);
    		

    		mapp.put("J_TEL", decryptjTel);
    		mapp.put("J_POST", decryptjPost);
    		mapp.put("J_ADDRESS", decryptjAddress);
    		mapp.put("J_ADDRESS2", decryptjAddress2);
    		// String ordId = (String)model.get("ordId");
    		// mapp.put("ORD_ID", ordId);
    		
    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", genericResultSet);
    	
    	return map;
    }
   
    @Override
    public Map<String, Object> dhlExcel(Map<String, Object> model) {
    	// TODO Auto-generated method stub
    	return null;
    }
    
    
    /*-
	 * Method ID   : getCustIdByLcId
	 * Method 설명 : LC_ID로 해당 코드의 화주ID 가져오기
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("NODATA")) {
			map.put("NODATA", dao.list2(model));
		}
		return map;
	}
    
    /**
     * Method ID : selectPopExcel
     * Method 설명 : 세트상품 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	@Override
    public byte[] pdfDown(Map<String, Object> model) throws Exception {
    	byte[] pdfBinary;
    	
    	List<WMSOP035VO> pdfList = dao.pdfDown(model);
    	if(pdfList.size() > 1){
    		PDFMergerUtility pdfMergerUtility = new PDFMergerUtility();
    		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    		pdfMergerUtility.setDestinationStream(outputStream);
    		
    		for(WMSOP035VO vo : pdfList){
    			ByteArrayInputStream inputStream = new ByteArrayInputStream(vo.getGraphicImage());
    			pdfMergerUtility.addSource(inputStream);
    			inputStream.close();
    		}
    		pdfMergerUtility.mergeDocuments(null);
    		pdfBinary = outputStream.toByteArray();
    		outputStream.close();
    		
    	}else if(pdfList.size() == 1){
    		pdfBinary = pdfList.get(0).getGraphicImage();
    		
    	}else{
    		pdfBinary = new byte[0];
    	}
    	
    	
    	return pdfBinary;
    }
    
	/**
     * 
     * 대체 Method ID   : outSaveComplete
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> outSaveComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){    
                String[] ordId   = new String[tmpCnt];
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)model.get("I_ORD_ID"+i);   
                	//System.out.println("I_ORD_ID : " + (String)model.get("I_ORD_ID"+i));
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);

                //session 및 등록정보
                modelIns.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
                //dao                
                modelIns = (Map<String, Object>)dao.outSaveComplete(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP035", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
	     * 
	     * 대체 Method ID   : outSaveComplete
	     * 대체 Method 설명    : 
	     * 작성자                      : chsong
	     * @param model
	     * @return
	     * @throws Exception
	     */
		 @Override
	    public Map<String, Object> outSaveCompleteV2(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();

	        try{
	            
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                String[] ordId = new String[]{(String) model.get("ordId")};
                
                modelIns.put("ordId", ordId);

                //session 및 등록정보
                modelIns.put("lcId", (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
                //dao                
                modelIns = (Map<String, Object>)dao.outSaveComplete(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP035", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
	            
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            
	        } catch(BizException be) {
	            m.put("errCnt", -1);
	            m.put("MSG", be.getMessage() );
	            
	        } catch(Exception e){
	            throw e;
	        }
	        return m;
	    }
	 
	 /**
     * 
     * 대체 Method ID   : list2DHL
     * 대체 Method 설명    : 수출신고정보
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2DHL(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Object vrSrchOrdIds2 = model.get("vrSrchOrdIds2");
        if(vrSrchOrdIds2 != null && !"".equals(((String)vrSrchOrdIds2).trim())){
        	List<String> vrSrchOrdIdsArray = new ArrayList(Arrays.asList(((String)vrSrchOrdIds2).trim().split(";")));
        	for(int i = vrSrchOrdIdsArray.size() - 1; i >= 0 ; i--){
        		if(vrSrchOrdIdsArray.get(i).trim().equals("")){
        			vrSrchOrdIdsArray.remove(i);
        			
        		}
        		
        	}
        	
        	model.put("vrSrchOrdIdsArray", vrSrchOrdIdsArray);
        }
    	
        map.put("LIST", dao.list2DHL(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : list2EMS
     * 대체 Method 설명    : 수출신고정보
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2EMS(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Object vrSrchOrdIds2 = model.get("vrSrchOrdIds2");
    	if(vrSrchOrdIds2 != null && !"".equals(((String)vrSrchOrdIds2).trim())){
    		List<String> vrSrchOrdIdsArray = new ArrayList(Arrays.asList(((String)vrSrchOrdIds2).trim().split(";")));
    		for(int i = vrSrchOrdIdsArray.size() - 1; i >= 0 ; i--){
    			if(vrSrchOrdIdsArray.get(i).trim().equals("")){
    				vrSrchOrdIdsArray.remove(i);
    				
    			}
    			
    		}
    		
    		model.put("vrSrchOrdIdsArray", vrSrchOrdIdsArray);
    	}
    	
    	map.put("LIST", dao.list2EMS(model));
    	return map;
    }
    /**
     * 
     * 대체 Method ID   : list2DHL
     * 대체 Method 설명    : 수출신고정보
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> request2EMS(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Object vrSrchOrdIds2 = model.get("vrSrchOrdIds3");
        if(vrSrchOrdIds2 != null && !"".equals(((String)vrSrchOrdIds2).trim())){
        	List<String> vrSrchOrdIdsArray = new ArrayList(Arrays.asList(((String)vrSrchOrdIds2).trim().split(";")));
        	for(int i = vrSrchOrdIdsArray.size() - 1; i >= 0 ; i--){
        		if(vrSrchOrdIdsArray.get(i).trim().equals("")){
        			vrSrchOrdIdsArray.remove(i);
        			
        		}
        		
        	}
        	
        	model.put("vrSrchOrdIdsArray", vrSrchOrdIdsArray);
        }
    	
    	List list  = dao.request2EMS(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		Map<String, Object> smap = new HashMap<>();
    		String orgOrdId = (String) mapp.get("ORG_ORD_ID");
    		smap.put("orgOrdId", orgOrdId);
    		smap.put("SS_SVC_NO", (String)model.get("SS_SVC_NO"));
    		List detailList = dao.request2EMSDetail(smap);
    		StringBuilder nmBuilder = new StringBuilder();
    		StringBuilder qtyBuilder = new StringBuilder();
    		StringBuilder weightBuilder = new StringBuilder();
    		StringBuilder totBuilder = new StringBuilder();
    		StringBuilder hsBuilder = new StringBuilder();
    		StringBuilder fromBuilder = new StringBuilder();
    		
    		int sum = 0;
    		for(int j = 0; j < detailList.size(); j++){
    			
    			Map<String, Object> mapq = (Map)detailList.get(j);
    			String ordDesc = ((String)mapq.get("ORD_DESC"))
    					.replaceAll("#", "")
    					.replaceAll("\\+", "")
    					.replaceAll("'", "")
    					.replaceAll("-", "")
    					.replaceAll("!", "")
    					.replaceAll("\\?", "");
    			String ritemNm = ordDesc.length() > 32 ? ordDesc.substring(0, 32) : ordDesc;
    			nmBuilder.append(ritemNm + ";");
    			
    			String ordQty = String.valueOf(mapq.get("ORD_QTY"));
    			int qty = Integer.parseInt(ordQty);
    			sum += qty;
    			qtyBuilder.append(ordQty + ";");
    			totBuilder.append(String.valueOf(mapq.get("TOT")) + ";");
    			hsBuilder.append("3304999000;");
    			fromBuilder.append("KR;");
    			weightBuilder.append("450;");
    			
    		}
    		
    		//String width = "0";
    		//String height = "0";
    		//String length = "0";
    		String totalWeight = "0";
    		
    		
    		if(sum >= 1 && sum <= 4){
    			//width = "21";
    			//height = "11.5";
    			//length = "12";
    			totalWeight = "1000";
    		}else if(sum >= 5 && sum <= 7){
    			//width = "25";
    			//height = "16";
    			//length = "15";
    			totalWeight = "1500";
    			
    		}else if(sum >= 8 && sum <= 10){
    			//width = "22";
    			//height = "17";
    			//length = "28";
    			totalWeight = "2000";
    			
    		}else if(sum >= 11 && sum <= 12){
    			//width = "34";
    			//height = "22";
    			//length = "28";
    			totalWeight = "3200";
    			
    		}else if(sum >= 13){
    			//width = "40";
    			//height = "20";
    			//length = "15";
    			totalWeight = "3500";
    			
    		}
    		
    		//mapp.put("WIDTH", width);
    		//mapp.put("HEIGHT", height);
    		//mapp.put("LENGTH", length);
    		mapp.put("TOTAL_WEIGHT", totalWeight);
    		
    		mapp.put("RITEM_NM", nmBuilder.toString());
    		mapp.put("QTY", qtyBuilder.toString());
    		mapp.put("WEIGHT", weightBuilder.toString());
    		mapp.put("TOT", totBuilder.toString());
    		mapp.put("HS_CODE", hsBuilder.toString());
    		mapp.put("FROM", fromBuilder.toString());
    		mapp.put("UNIT", "");
    		mapp.put("INSURANCE_YN", "");
    		mapp.put("INSURANCE_PRICE", "");
    		
    		String dTel = (String)mapp.get("D_TEL");
    		String decryptdTel = OliveAes256.decrpyt(dTel);
    		
    		String dPost = (String)mapp.get("D_POST");
    		String decryptdPost = OliveAes256.decrpyt(dPost);
    		
    		String dAddress = (String)mapp.get("D_ADDRESS");
    		String decryptdAddress = OliveAes256.decrpyt(dAddress);
    		
    		String dCity = (String)mapp.get("D_CITY");
    		String dProvince = (String)mapp.get("D_PROVINCE");
    		
    		String decryptdAddress2 = "";
    		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
    			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
    		}
    		
    		mapp.put("D_TEL", decryptdTel);
    		mapp.put("D_POST", decryptdPost);
    		mapp.put("D_ADDRESS", decryptdAddress + " " + decryptdAddress2 + ", " + dCity + ", " + dProvince);
    		mapp.put("D_ADDRESS2", "");
    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", genericResultSet);
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveList2DHL
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveList2DHL(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ORG_ORD_NO"    , model.get("ORG_ORD_NO"+i));
                modelDt.put("ORD_DETAIL_NO" , model.get("ORD_DETAIL_NO"+i));
                modelDt.put("SS_USER_NO"	, (String)model.get(ConstantIF.SS_USER_NO));
                modelDt.put("SS_LC_ID"     	, (String)model.get(ConstantIF.SS_SVC_NO));
                
                dao.saveList2DHL(modelDt);
                
                m.put("errCnt", 0);
                m.put("MSG", MessageResolver.getMessage("save.success"));
            }
            
        } catch(Exception e){
        	m.put("errCnt", -1);
            throw e;
            
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : list2DHL
     * 대체 Method 설명    : 수출신고정보
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> emsAddress(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	Map<String, Object> mapp = dao.emsAddress(model);
		
		String dTel = (String)mapp.get("D_TEL");
		String decryptdTel = OliveAes256.decrpyt(dTel);
		
		String dPost = (String)mapp.get("D_POST");
		String decryptdPost = OliveAes256.decrpyt(dPost);
		
		String dAddress = (String)mapp.get("D_ADDRESS");
		String decryptdAddress = OliveAes256.decrpyt(dAddress);
		
		String decryptdAddress2 = "";
		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
		}
		String dCity = (String)mapp.get("D_CITY");
		String dProvince = (String)mapp.get("D_PROVINCE");
		
		String dAddressTotal = decryptdAddress + " " + decryptdAddress2 + ", " + dCity + ", " + dProvince;

		String jTel = (String)mapp.get("J_TEL");
		String decryptjTel = OliveAes256.decrpyt(jTel);
		
		String jPost = (String)mapp.get("J_POST");
		String decryptjPost = OliveAes256.decrpyt(jPost);
		
		String jAddress = (String)mapp.get("J_ADDRESS");
		String decryptjAddress = OliveAes256.decrpyt(jAddress);
		
		String decryptjAddress2 = "";
		if(mapp.get("J_ADDRESS2") != null && !((String)mapp.get("J_ADDRESS2")).trim().equals("")){
			decryptjAddress2 = OliveAes256.decrpyt((String)mapp.get("J_ADDRESS2"));
		}
		String jCity = (String)mapp.get("J_CITY");
		String jProvince = (String)mapp.get("J_PROVINCE");
		
		String jAddressTotal = decryptjAddress + " " + decryptjAddress2 + ", " + jCity + ", " + jProvince;
		
		
		map.put("D_TEL", decryptdTel);
		map.put("D_POST", decryptdPost);
		map.put("D_ADDRESS", dAddressTotal);

		map.put("J_TEL", decryptjTel);
		map.put("J_POST", decryptjPost);
		map.put("J_ADDRESS", jAddressTotal);
		String ordId = (String)model.get("ordId");
		map.put("ORD_ID", ordId);
		checkShipmentPublish(map);
		
    	return map;
    	    	
    }
    
    @Override
    public Map<String, Object> addressHistory(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List list  = dao.addressHistory(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		
    		String decryptdTel = "";
    		if(mapp.get("D_TEL") != null && !((String)mapp.get("D_TEL")).trim().equals("")){
    			decryptdTel = OliveAes256.decrpyt((String) mapp.get("D_TEL")); 
    		}
    		
    		String decryptdPost = "";
    		if(mapp.get("D_POST") != null && !((String)mapp.get("D_POST")).trim().equals("")){
    			decryptdPost = OliveAes256.decrpyt((String) mapp.get("D_POST")); 
    		}
    		
    		String decryptdAddress = "";
    		if(mapp.get("D_ADDRESS") != null && !((String)mapp.get("D_ADDRESS")).trim().equals("")){
    			decryptdAddress = OliveAes256.decrpyt((String) mapp.get("D_ADDRESS")); 
    		}
    		
    		String decryptdAddress2 = "";
    		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
    			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
    		}
    		
    		String decryptdCity = (String) mapp.get("D_CITY");
    		String decryptdProvince = (String) mapp.get("D_PROVINCE");
    		
    		String decryptjTel = "";
    		if(mapp.get("J_TEL") != null && !((String)mapp.get("J_TEL")).trim().equals("")){
    			decryptjTel = OliveAes256.decrpyt((String) mapp.get("J_TEL")); 
    		}
    		 
    		String decryptjPost = "";
    		if(mapp.get("J_POST") != null && !((String)mapp.get("J_POST")).trim().equals("")){
    			decryptjPost = OliveAes256.decrpyt((String) mapp.get("J_POST")); 
    		}
    		
    		String decryptjAddress = "";
    		if(mapp.get("J_ADDRESS") != null && !((String)mapp.get("J_ADDRESS")).trim().equals("")){
    			decryptjAddress = OliveAes256.decrpyt((String) mapp.get("J_ADDRESS")); 
    		}

    		String decryptjAddress2 = "";
    		if(mapp.get("J_ADDRESS2") != null && !((String)mapp.get("J_ADDRESS2")).trim().equals("")){
    			decryptjAddress2 = OliveAes256.decrpyt((String)mapp.get("J_ADDRESS2"));
    		}
    		
    		String jCity = (String)mapp.get("J_CITY");
    		String jProvince = (String)mapp.get("J_PROVINCE");
    		
    		mapp.put("D_TEL", decryptdTel);
    		mapp.put("D_POST", decryptdPost);
    		mapp.put("D_PROVINCE", decryptdProvince);
    		mapp.put("D_ADDRESS", decryptdAddress);
    		mapp.put("D_ADDRESS2", decryptdAddress2);
    		

    		mapp.put("J_TEL", decryptjTel);
    		mapp.put("J_POST", decryptjPost);
    		mapp.put("J_ADDRESS", decryptjAddress);
    		mapp.put("J_ADDRESS2", decryptjAddress2);
    		String ordId = (String)model.get("ordId");
    		mapp.put("ORD_ID", ordId);
    		
    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", list);
    	
    	return map;
    }
    
    @Override
    public Map<String, Object> addressOM010(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List list  = dao.addressHistory(model);
    	
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> mapp = (Map)list.get(i);
    		
    		String decryptdTel = "";
    		if(mapp.get("D_TEL") != null && !((String)mapp.get("D_TEL")).trim().equals("")){
    			decryptdTel = OliveAes256.decrpyt((String) mapp.get("D_TEL")); 
    		}
    		
    		String decryptdPost = "";
    		if(mapp.get("D_POST") != null && !((String)mapp.get("D_POST")).trim().equals("")){
    			decryptdPost = OliveAes256.decrpyt((String) mapp.get("D_POST")); 
    		}
    		
    		String decryptdAddress = "";
    		if(mapp.get("D_ADDRESS") != null && !((String)mapp.get("D_ADDRESS")).trim().equals("")){
    			decryptdAddress = OliveAes256.decrpyt((String) mapp.get("D_ADDRESS")); 
    		}
    		
    		String decryptdAddress2 = "";
    		if(mapp.get("D_ADDRESS2") != null && !((String)mapp.get("D_ADDRESS2")).trim().equals("")){
    			decryptdAddress2 = OliveAes256.decrpyt((String)mapp.get("D_ADDRESS2"));
    		}
    		
    		String decryptdCity = (String) mapp.get("D_CITY");
    		String decryptdProvince = (String) mapp.get("D_PROVINCE");
    		
    		String decryptjTel = "";
    		if(mapp.get("J_TEL") != null && !((String)mapp.get("J_TEL")).trim().equals("")){
    			decryptjTel = OliveAes256.decrpyt((String) mapp.get("J_TEL")); 
    		}
    		 
    		String decryptjPost = "";
    		if(mapp.get("J_POST") != null && !((String)mapp.get("J_POST")).trim().equals("")){
    			decryptjPost = OliveAes256.decrpyt((String) mapp.get("J_POST")); 
    		}
    		
    		String decryptjAddress = "";
    		if(mapp.get("J_ADDRESS") != null && !((String)mapp.get("J_ADDRESS")).trim().equals("")){
    			decryptjAddress = OliveAes256.decrpyt((String) mapp.get("J_ADDRESS")); 
    		}

    		String decryptjAddress2 = "";
    		if(mapp.get("J_ADDRESS2") != null && !((String)mapp.get("J_ADDRESS2")).trim().equals("")){
    			decryptjAddress2 = OliveAes256.decrpyt((String)mapp.get("J_ADDRESS2"));
    		}
    		
    		String jCity = (String)mapp.get("J_CITY");
    		String jProvince = (String)mapp.get("J_PROVINCE");
    		
    		mapp.put("D_TEL", decryptdTel);
    		mapp.put("D_POST", decryptdPost);
    		mapp.put("D_PROVINCE", decryptdProvince);
    		mapp.put("D_ADDRESS", decryptdAddress);
    		mapp.put("D_ADDRESS2", decryptdAddress2);
    		

    		mapp.put("J_TEL", decryptjTel);
    		mapp.put("J_POST", decryptjPost);
    		mapp.put("J_ADDRESS", decryptjAddress);
    		mapp.put("J_ADDRESS2", decryptjAddress2);
    		String ordId = (String)model.get("ordId");
    		mapp.put("ORD_ID", ordId);
    		
    	}
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list);
    	map.put("LIST", list);
    	
    	return map;
    }
    
    private void checkShipmentPublish(Map<String, Object> model){
    	int count = dao.selectShipmentPublish(model);
    	if(count > 0){
    		dao.updateShipmentPublish(model);
    	}else{
    		dao.insertShipmentPublish(model);
    		
    	}
    	
    }
    
    @Override
    public Map<String, Object> export2EMS(Map<String, Object> model)
    		throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("LIST", dao.export2EMS(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveExcelOrderJava
     * 대체 Method 설명    : 템플릿 주문 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveExport2EMS(List list) throws Exception {
        Map<String, Object> m = new HashMap<>();
    	dao.saveExport2EMS(list);
        
        m.put("MSG", MessageResolver.getMessage("save.success"));
        m.put("MSG_ORA", "");
        m.put("errCnt", 0);
            
        return m;
    } 
    
    @Override
    public String getOrdId(String orgOrdId) throws Exception {
    	return dao.getOrdId(orgOrdId);
    }
    
    @Override
    public boolean updateBlNo(List list) {
    	for(int i = 0; i < list.size(); i++){
    		Map<String, Object> obj = (Map)list.get(i);
    		String blNo = (String)obj.get("NEW_BLNO");
    		if(blNo.startsWith("EG")){
    			dao.updateEmsBlNo(obj);
    			dao.deleteEmsTracking(obj);
    			
    		}else{
    			dao.updateDhlBlNo(obj);
    			dao.deleteDhlTracking(obj);
    			
    		}
    		dao.updateCommonBlNo(obj);
    		dao.deleteCommonTracking(obj);
    	}
    	return Boolean.valueOf(true);
    }
    
    @Override
    public Map<String, Object> checkBlNo(Map<String, Object> model)
    		throws Exception {
    	List<String> orgOrdIds = Arrays.asList("", ""); //개수만큼
    	List<String> blNos = Arrays.asList("", ""); //개수만큼
    	
    	System.out.println(orgOrdIds.size() + "  " + blNos.size());
    	int count = orgOrdIds.size();
    	
    	List<String> resultList = new ArrayList<>();
    	for(int i = 0; i < count; i++){
    		List<Map> result = dao.checkBlNo(orgOrdIds.get(i));
    		if(result.size() > 0){
    			String blNo = (String)result.get(0).get("BL_NO");
    			if(blNos.get(i).equals(blNo)){
    				resultList.add("이상없음 또는 UNIPASS 조회안됨");
    				//System.out.println("이상없음 또는 UNIPASS 확인 필요");
    			}else{
    				resultList.add("특송장번호 다름");
    				//System.out.println("특송장번호 다름");
    			}
    		}else{
    			resultList.add("접수내역 존재하지 않음");
    			//System.out.println("WMS전산에 접수내역 존재하지 않음");
    		}
    	}
    	
    	System.out.println(resultList.toString());
    	return null;
    }
    
    /**
     * Method ID   : listE5
     * Method 설명    : 올리브영 수출신고번호 or 운송장번호 누락 조회
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE5(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.listE5(model));
        return map;
    }
    
    /**
     * Method ID   : listE6
     * Method 설명    : 올리브영 운송장 출력 여부 조회
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE6(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        if(model.get("vrSrchShippingCompany6").equals("10")){
        	map.put("LIST", dao.listE6EMS(model));
        }else{
        	map.put("LIST", dao.listE6DHL(model));
        }
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   	: selectPrintDt
     * 대체 Method 설명    : 올리브영 송장 출력 시간 조회
     * 작성자                    : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> selectPrintDt(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("RESULT", dao.selectPrintDt(model));
        return map;
    } 
    
    /**
     * 
     * 대체 Method ID   : updatetPrintDt
     * 대체 Method 설명    : 올리브영 송장 출력 시간 업데이트
     * 작성자                      : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatetPrintDt(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<>();
    	m = (Map<String, Object>) dao.updatetPrintDt(model);
        
        m.put("MSG", MessageResolver.getMessage("save.success"));
        m.put("MSG_ORA", "");
        m.put("errCnt", 0);
            
        return m;
    } 
}
