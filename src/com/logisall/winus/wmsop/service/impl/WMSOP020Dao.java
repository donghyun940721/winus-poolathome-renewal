package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP020Dao")
public class WMSOP020Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID  : selectItemGrp
     * Method �ㅻ�  : ��硫대�� ������ ����援� 媛��몄�ㅺ린
     * ���깆��             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 엑셀다운로드
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	
    	String LC_ID = (String)model.get("LC_ID");
    	if (LC_ID != null && StringUtils.isNotEmpty(LC_ID) && LC_ID.equals("0000002940")){//kcc
    		
    		String  vrSrchOrdSubtype = (String) model.get("vrSrchOrdSubtype");
            if(vrSrchOrdSubtype != null && StringUtils.isNotEmpty(vrSrchOrdSubtype) ){
            	model.put("vrSrchOrdSubtype", Integer.parseInt(vrSrchOrdSubtype));
            }
            
    		return executeQueryPageWq("wmsop010.inListKcc", model);
    	}else{
    		return executeQueryPageWq("wmsop010.inList", model);
    	}
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 엑셀다운로드(PLT)
     * 작성자                 : smics
     * @param   model
     * @return
     */
    public GenericResultSet listPlt(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.inPoolList", model);
    }    
    
    /**
     * Method ID    : lcinfolist
     * Method �ㅻ�      : ��怨�愿�由� �⑷�
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object lcinfolist(Map<String, Object> model) {
        return executeView("wmsop020.lcinfolist", model);
    }
    
    /**
     * Method ID    : inOrderCntInit
     * Method �ㅻ�      : 
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object inOrderCntInit(Map<String, Object> model) {
        return executeView("wmsop020.inOrderCntInit", model);
    }
    
    /**
     * Method ID    : deleteOrder
     * Method �ㅻ�      : 二쇰Ц����
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object deleteOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_delete_order", model);
        return model;
    }

    /**
     * Method ID    : saveConfirmGrn
     * Method �ㅻ�      : ��������
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object saveConfirmGrn(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_confirm_grn", model);
        return model;
    }
    
    /**
     * Method ID    : saveCancelGrn
     * Method �ㅻ�      : ��������
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object saveCancelGrn(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_cancel_grn", model);
        return model;
    }

    /**
     * Method ID    : saveInComplete
     * Method �ㅻ�      : ��怨�����
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object saveInComplete(Map<String, Object> model){
        try{
            executeUpdate("wmsop020.pk_wmsop020.sp_in_complete", model);
        }catch (Exception e ){
            model.put("O_MSG_NAME", e.getCause().getCause().getMessage());
            model.put("O_MSG_CODE", -1);
        }
        return model;
    }
    
    public Object saveInCompleteOutOrder(Map<String, Object> model){
        try{
        	executeUpdate("wmsop020.pk_wmsop020.sp_in_complete_after_ship_ord", model);
        }catch (Exception e ){
            model.put("O_MSG_NAME", e.getCause().getCause().getMessage());
            model.put("O_MSG_CODE", -1);
        }
        return model;
    }

    /**
     * Method ID   : saveCancelInComplete
     * Method 내용    : 입고확정취소
     * 작성자                      : MonkeySeok
     * 날 짜 		: 2020-06-14
     * @param   model
     * @return
     */
    public Object saveCancelInComplete(Map<String, Object> model){
        try{
            executeUpdate("wmsop020.pk_wmsop020.sp_cancel_in_complete", model);
        }catch (Exception e ){
            model.put("O_MSG_NAME", e.getCause().getCause().getMessage());
            model.put("O_MSG_CODE", -1);
        }
        return model;
    }

    /**
     * Method ID    : saveSimpleIn
     * Method �ㅻ�      : 媛��몄��怨�
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object saveSimpleIn(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_simple_in", model);
        return model;
    }
    
    /**
     * Method ID    : chkTest
     * Method �ㅻ�      : ����由우���⑥�깆껜��
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object chkTest(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020b.sp_chk_template", model);
        return model;
    }

    
    /**
     * Method ID    : listInOrderItemASN
     * Method �ㅻ�      : ��怨�愿�由�ASN ���� 二쇰Ц ���� 議고��
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listInOrderItemASN(Map<String, Object> model) {
        return executeQueryPageWq("wmsop011.listInOrderItemASN", model);
    }
    
    /**
     * Method ID    : saveAsnOrder
     * Method �ㅻ�      : ��怨�ASN二쇰Ц �깅�
     * ���깆��                 : chsong
     * @param   model
     * @return
     */
    public Object saveAsnOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_asn_insert_order", model);
        return model;
    }
    
    /*-
     * Method ID : checkWorkStatData
     * Method 설명 : 
     * 작성자 : 
     *
     * @param model
     * @return
     */
    public String checkWorkStatData(Map<String, Object> model) {
        return (String)executeView("wmsop020.checkWorkStatData", model);
    }
    
    /**
     * Method ID    : ifInOrdLocMapp
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object ifInOrdLocMapp(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsif100.sp_in_ord_loc_mapping", model);
        return model;
    }
    
    /**
     * Method ID    : autoBestLocSave
     * Method 설명      : 출고관리 출고주문 등록,수정
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object autoBestLocSave(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_auto_location", model);
        return model;
    }
    
    /**
	 * Method ID : saveUploadData 
	 * Method 설명 : 엑셀업로드 저장
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			
			String[] ordId  = new String[list.size()];
            String[] ordSeq = new String[list.size()];
            String[] locCd  = new String[list.size()];
            
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if ( (paramMap.get("I_ORD_ID")  != null && StringUtils.isNotEmpty(paramMap.get("I_ORD_ID").toString()))
				   &&(paramMap.get("I_ORD_SEQ") != null && StringUtils.isNotEmpty(paramMap.get("I_ORD_SEQ").toString()))
				   &&(paramMap.get("I_LOC_CD")  != null && StringUtils.isNotEmpty(paramMap.get("I_LOC_CD").toString())) ) {
					
					ordId[i]  = (String)paramMap.get("I_ORD_ID");
                	ordSeq[i] = (String)paramMap.get("I_ORD_SEQ");
                	locCd[i]  = (String)paramMap.get("I_LOC_CD");
				}
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("ordId" , ordId);
            modelIns.put("ordSeq", ordSeq);
            modelIns.put("locCd" , locCd);

            //session 및 등록정보
            modelIns.put("LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

            //dao    
            
            /*
            //modelIns = (Map<String, Object>)dao.autoBestLocSave(modelIns);
            executeUpdate("wmsop001_sable7.pk_wmsop001_sable7.sp_excel_loc", modelIns);
            //ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            */
          
            // 프로시저 호출 분기(SHP : 출고 / RCV : 입고)
            String spName = ((String)model.get("target")).equals("RCV") ? "sp_excel_loc" : "sp_excel_out_loc";
            executeUpdate("wmsop001_sable7.pk_wmsop001_sable7."+ spName, modelIns);
            sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	/**
     * Method ID    : asnOutOrd
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object asnOutOrd(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop030.sp_make_asn_first_work", model);
        return model;
    }
    
    /**
     * Method ID	: autoFixLocSave
     * Method 설명	: 입고 Fix 로케이션 추천지정
     * 작성자			: chsong
     * @param model
     * @return
     */
    public Object autoFixLocSave(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop001_sable7.sp_fix_auto_loc", model);
        return model;
    }
    
    /**
     * Method ID : saveInvcNoOrder
     * Method 설명 : 반품배송정보 내역 업데이트
     * 작성자 : yhku (11.25이관) 
     * @param model
     * @return
     */
	public void saveInvcNoOrder(Map<String, Object> model) throws Exception {
		executeUpdate("wmsop020.saveInvcNoOrder", model);
	}
	

    /**
     * Method ID : simpleInWorkingReset
     * Method 설명 : 운영관리 > 입고관리 > 작업초기화
     * 작성자 : sing09
     * @param model
     * @return
     */
    public Object simpleInWorkingReset(Map<String, Object> model){
        executeUpdate("wmsop020.simpleInWorkingReset", model);
        return model;
    }   
    
}
