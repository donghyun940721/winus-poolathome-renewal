package com.logisall.winus.wmsop.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsct.service.impl.WMSCT000Dao;
import com.logisall.winus.wmsop.service.WMSOP100Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSOP100Service")
public class WMSOP100ServiceImpl extends AbstractServiceImpl implements WMSOP100Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP100Dao")
    private WMSOP100Dao dao;
    
    @Resource(name = "WMSCT000Dao")
    private WMSCT000Dao dao000;
    
    private final static String[] CHECK_VALIDATE_WMSOP100 = {"UPLOAD_ID"};

   /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID  : saveE2
     * 대체 Method 설명    : 엑셀목록 저장
     * 작성자                        : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	/*
	       	//주문등록시간 제한체크
	       	Map<String, Object> modelDt = new HashMap<String, Object>();
            String checkData = dao.checkInsertPossible(modelDt);
            if(checkData.equals("N")){ //:N
            	//등록제한, 화면에서 Break return
	           	m.put("MSG", "");
	           	m.put("RST", "BREAK");
            }else{
            	//정상처리
            	model.put("UPLOAD_TITLE", model.get("D_FILE_NAME"));
    			model.put("FILE_SIZE"   , model.get("D_FILE_SIZE"));				
    			model.put("LC_ID"       , model.get(ConstantIF.SS_SVC_NO));
    			model.put("REG_NO"      , model.get(ConstantIF.SS_USER_NO));
    			model.put("CUST_ID"     , model.get("D_CUST_ID"));
    			
                String fileUploadId = (String) dao.insert(model);
    			m.put("MSG", MessageResolver.getMessage("insert.success"));
    			m.put("RST", fileUploadId);
            }
            */
            //정상처리
        	model.put("UPLOAD_TITLE", model.get("D_FILE_NAME"));
			model.put("FILE_SIZE"   , model.get("D_FILE_SIZE"));				
			model.put("LC_ID"       , model.get(ConstantIF.SS_SVC_NO));
			model.put("REG_NO"      , model.get(ConstantIF.SS_USER_NO));
			model.put("CUST_ID"     , model.get("D_CUST_ID"));
			
            String fileUploadId = (String) dao.insert(model);
			m.put("MSG", MessageResolver.getMessage("insert.success"));
			m.put("RST", fileUploadId);
        } catch (Exception e) {
        	throw e;
        }
	return m;
    }
    
    /**
     * 
     * 대체 Method ID  : saveE5
     * 대체 Method 설명    : 엑셀목록 HD
     * 작성자                        : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveE5(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	try{
    		//정상처리
    		model.put("UPLOAD_TITLE", model.get("D_FILE_NAME"));
    		model.put("FILE_SIZE"   , model.get("D_FILE_SIZE"));				
    		model.put("LC_ID"       , model.get(ConstantIF.SS_SVC_NO));
    		model.put("REG_NO"      , model.get(ConstantIF.SS_USER_NO));
    		model.put("CUST_ID"     , model.get("D_CUST_ID"));
    		
    		String fileUploadId = (String) dao.insert(model);
    		m.put("MSG", MessageResolver.getMessage("insert.success"));
    		m.put("RST", fileUploadId);
    	} catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID  : saveE7
     * 대체 Method 설명    : AS엑셀목록 저장
     * 작성자                        : sing
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveE7(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	try{
    		//정상처리
    		model.put("UPLOAD_TITLE", model.get("D_FILE_NAME"));
    		model.put("FILE_SIZE"   , model.get("D_FILE_SIZE"));				
    		model.put("LC_ID"       , model.get(ConstantIF.SS_SVC_NO));
    		model.put("REG_NO"      , model.get(ConstantIF.SS_USER_NO));
    		model.put("CUST_ID"     , model.get("D_CUST_ID"));
    		
    		String fileUploadId = (String) dao.insert(model);
    		m.put("MSG", MessageResolver.getMessage("insert.success"));
    		m.put("RST", fileUploadId);
    	} catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    
	/**
     * Method ID : saveExcelInfo
     * Method 설명 : 엑셀읽기저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{
            	model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
				model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
				model.put("FILE_ROW" , insertCnt);
				
				// 1.고객관리 입력 및 수정 WMSCT010
				if (model.get("vrSaveGb").equals("INSERT")){
					dao.insertExcelInfo(model, list);
				}else if(model.get("vrSaveGb").equals("UPDATE")){
					dao.insertExcelInfoUpdate(model, list);
				}
				
				//WMSEI010 insert rowcount update : SET FILE_ROW  = #FILE_ROW#
                dao.updateFileRow(model);
                
                m.put("MSG", MessageResolver.getMessage("save.success"));
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                m.put("MSG", MessageResolver.getMessage("save.error"));
    			m.put("MSG_ORA", e.getMessage());
    			m.put("errCnt", "1");
                throw e;
            }
        return m;
    }
    
    /**
     * Method ID : saveExcelInfo_HD
     * Method 설명 : 엑셀읽기저장 HD
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo_HD(Map<String, Object> model, List list) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	int insertCnt = (list != null)?list.size():0;
    	try{
    		model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
    		model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
    		model.put("FILE_ROW" , insertCnt);
    		
			// 1.고객관리 입력 및 수정 WMSCT010
			 int cnt = dao.insertExcelInfo_HD(model, list);
			 model.put("FILE_ROW_HD"    , cnt);
    		//WMSEI010 insert rowcount update : SET FILE_ROW  = #FILE_ROW#
    		dao.updateFileRow_HD(model);
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", errCnt);
    		
    	} catch(Exception e){
    		m.put("MSG", MessageResolver.getMessage("save.error"));
    		m.put("MSG_ORA", e.getMessage());
    		m.put("errCnt", "1");
    		throw e;
    	}
    	return m;
    }
    
    /**
     * Method ID : saveExcelInfo_CT
     * Method 설명 : 엑셀읽기저장 고객정보
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo_CT(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{
                model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
                model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
                model.put("FILE_ROW" , insertCnt);
                
                // 1.고객관리 입력 및 수정 WMSCT010
                dao.insertExcelInfo_CT(model, list);
                dao.updateFileRow(model);
                
                m.put("MSG", MessageResolver.getMessage("save.success"));
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                m.put("MSG", MessageResolver.getMessage("save.error"));
                m.put("MSG_ORA", e.getMessage());
                m.put("errCnt", "1");
                throw e;
            }
        return m;
    }
    /**
     * Method ID : saveExcelInfo_CS
     * Method 설명 : 엑셀읽기저장 CS정보
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo_CS(Map<String, Object> model, List list) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	int insertCnt = (list != null)?list.size():0;
    	try{
    		model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
    		model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
    		model.put("FILE_ROW" , insertCnt);
    		
    		// 1.CS관리 입력 및 수정 WMSCT010
    		int cnt = dao.insertExcelInfo_CS(model, list);
			model.put("FILE_ROW_HD"    , cnt);
			dao.updateFileRow_HD(model);
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", errCnt);
    		
    	} catch(Exception e){
    		m.put("MSG", MessageResolver.getMessage("save.error"));
    		m.put("MSG_ORA", e.getMessage());
    		m.put("errCnt", "1");
    		throw e;
    	}
    	return m;
    }
    /**
     * Method ID : saveExcelInfo_AS
     * Method 설명 : 엑셀읽기저장 AS
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo_AS(Map<String, Object> model, List list) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	int insertCnt = (list != null)?list.size():0;
    	try{
    		model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
    		model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
    		model.put("FILE_ROW" , insertCnt);
    		// 1.고객관리 입력 및 수정 WMSCT010
			dao.insertExcelInfo_AS(model, list);
    		//WMSEI010 insert rowcount update : SET FILE_ROW  = #FILE_ROW#
			dao.updateFileRow(model);
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", errCnt);
    		
    	} catch(Exception e){
    		m.put("MSG", MessageResolver.getMessage("save.error"));
    		m.put("MSG_ORA", e.getMessage());
    		m.put("errCnt", "1");
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));
                modelDt.put("UPLOAD_ID"     	, model.get("UPLOAD_ID"+i));
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSOP100);
                
                if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	dao.deleteUpdateWmssp011_upload(modelDt);
                	dao.deleteUpdateWmssp011(modelDt);
                	dao.deleteUpdateWmssp010(modelDt);
                	dao.deleteUpdateWmsas010(modelDt);
                	dao.deleteUpdateWmsct010(modelDt);
                	
					dao.delete(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : delete
     * 대체 Method 설명    :
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			modelDt.put("I_UPLOAD_ID"     	, model.get("UPLOAD_ID"+i));
    			modelDt.put("I_WORK_IP"    		, model.get(ConstantIF.SS_CLIENT_IP));
    			modelDt.put("I_USER_NO"    		, model.get(ConstantIF.SS_USER_NO));

    			modelDt = (Map<String, Object>)dao000.spCustInfoDelete(modelDt);
        		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
        		 
    		}
    		m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch (BizException be) {
    		if (log.isInfoEnabled()) {
    			log.info(be.getMessage());
    		}
    		m.put("MSG", be.getMessage());
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * Method ID  : excelDownCustom
     * Method 설명    : Type별 주문정보 엑셀다운로드
     * 작성자                 : chsong
     * @param   model
     * @return 
     * @throws Exception 
     */
     @Override
     public Map<String, Object> excelDownCustom(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         model.put("pageIndex", "1");
         model.put("pageSize", "60000");
         map.put("LIST", dao.excelDownCustom(model));
         return map;
     }
     
     /**
      * 
      * 대체 Method ID   : keyInSaveInfo
      * 대체 Method 설명    : 배송주문 일반저장
      * 작성자                      : chsong
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> keyInSaveInfo(Map<String, Object> model, String[] cellName) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         int lastCell = cellName.length;
         int errCnt = 0;
         
         List list = new ArrayList();
         Map rowMap = null;
         
         try{
        	 for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	 rowMap = new LinkedHashMap();
            	 
        		 for(int k = 0 ; k < lastCell; k ++){
        			 rowMap.put(cellName[k], model.get(cellName[k]+i));
        		 }
        		 list.add(rowMap);
        	 }
        	 
        	 int insertCnt = (list != null)?list.size():0;
 
             model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
             model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
             model.put("FILE_ROW" , insertCnt);
 				
             // 1.고객관리 입력 및 수정 WMSCT010
             if (model.get("vrSaveGb").equals("INSERT")){
            	 dao.insertExcelInfo(model, list);
             }else if(model.get("vrSaveGb").equals("UPDATE")){
            	 dao.insertExcelInfoUpdate(model, list);
             }
             dao.updateFileRow(model);
             
             m.put("MSG", MessageResolver.getMessage("save.success"));
             m.put("MSG_ORA", "");
             m.put("errCnt", errCnt);
                 
         } catch(Exception e){
        	 m.put("MSG", MessageResolver.getMessage("save.error"));
        	 m.put("MSG_ORA", e.getMessage());
        	 m.put("errCnt", "1");
        	 throw e;
         }
         return m;
     }
     
     /**
      * 
      * 대체 Method ID   : keyInSaveInfo_HD
      * 대체 Method 설명    : 배송주문 일반저장 HD
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> keyInSaveInfo_HD(Map<String, Object> model, String[] cellName) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 int lastCell = cellName.length;
    	 int errCnt = 0;
    	 
    	 List list = new ArrayList();
    	 Map rowMap = null;
    	 
    	 try{
    		 for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			 rowMap = new LinkedHashMap();
    			 
    			 for(int k = 0 ; k < lastCell; k ++){
    				 rowMap.put(cellName[k], model.get(cellName[k]+i));
    			 }
    			 list.add(rowMap);
    		 }
    		 
    		 int insertCnt = (list != null)?list.size():0;
    		 
    		 model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
    		 model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
    		 model.put("FILE_ROW" , insertCnt);
    		 
    		 // 1.고객관리 입력 및 수정 WMSCT010
			 int cnt = dao.insertExcelInfo_HD(model, list);
			 model.put("FILE_ROW_HD"    , cnt);
			 dao.updateFileRow_HD(model);
    		 
    		 m.put("MSG", MessageResolver.getMessage("save.success"));
    		 m.put("MSG_ORA", "");
    		 m.put("errCnt", errCnt);
    		 
    	 } catch(Exception e){
    		 m.put("MSG", MessageResolver.getMessage("save.error"));
    		 m.put("MSG_ORA", e.getMessage());
    		 m.put("errCnt", "1");
    		 throw e;
    	 }
    	 return m;
     }
     
     /**
      * 
      * 대체 Method ID   : keyInSaveInfo_CT
      * 대체 Method 설명    : 고객주문일반업로드
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> keyInSaveInfo_CT(Map<String, Object> model, String[] cellName) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         int lastCell = cellName.length;
         int errCnt = 0;
         List list = new ArrayList();
         Map rowMap = null;
         try{
             for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                 rowMap = new LinkedHashMap();
                 for(int k = 0 ; k < lastCell; k ++){
                     rowMap.put(cellName[k], model.get(cellName[k]+i));
                 }
                 list.add(rowMap);
             }
             int insertCnt = (list != null)?list.size():0;
             
             model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
             model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
             model.put("FILE_ROW" , insertCnt);
             
             dao.insertExcelInfo_CT(model, list);
             dao.updateFileRow(model);
             
             m.put("MSG", MessageResolver.getMessage("save.success"));
             m.put("MSG_ORA", "");
             m.put("errCnt", errCnt);
             
         } catch(Exception e){
             m.put("MSG", MessageResolver.getMessage("save.error"));
             m.put("MSG_ORA", e.getMessage());
             m.put("errCnt", "1");
             throw e;
         }
         return m;
     }
     
     /**
      * 
      * 대체 Method ID   : keyInSaveInfo_CS
      * 대체 Method 설명    : CS주문일반업로드
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> keyInSaveInfo_CS(Map<String, Object> model, String[] cellName) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 int lastCell = cellName.length;
    	 int errCnt = 0;
    	 List list = new ArrayList();
    	 Map rowMap = null;
    	 try{
    		 for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			 rowMap = new LinkedHashMap();
    			 for(int k = 0 ; k < lastCell; k ++){
    				 rowMap.put(cellName[k], model.get(cellName[k]+i));
    			 }
    			 list.add(rowMap);
    		 }
    		 int insertCnt = (list != null)?list.size():0;
    		 
    		 model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
    		 model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
    		 model.put("FILE_ROW" , insertCnt);
    		 
    		 dao.insertExcelInfo_CS(model, list);
    		 dao.updateFileRow(model);
    		 
    		 m.put("MSG", MessageResolver.getMessage("save.success"));
    		 m.put("MSG_ORA", "");
    		 m.put("errCnt", errCnt);
    		 
    	 } catch(Exception e){
    		 m.put("MSG", MessageResolver.getMessage("save.error"));
    		 m.put("MSG_ORA", e.getMessage());
    		 m.put("errCnt", "1");
    		 throw e;
    	 }
    	 return m;
     }
     
     /**
      * 
      * 대체 Method ID   : keyInSaveInfo_AS
      * 대체 Method 설명    : AS주문 일반저장 
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> keyInSaveInfo_AS(Map<String, Object> model, String[] cellName) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 int lastCell = cellName.length;
    	 int errCnt = 0;
    	 
    	 List list = new ArrayList();
    	 Map rowMap = null;
    	 
    	 try{
    		 for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			 rowMap = new LinkedHashMap();
    			 
    			 for(int k = 0 ; k < lastCell; k ++){
    				 rowMap.put(cellName[k], model.get(cellName[k]+i));
    			 }
    			 list.add(rowMap);
    		 }
    		 
    		 int insertCnt = (list != null)?list.size():0;
    		 
             model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
             model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
             model.put("FILE_ROW" , insertCnt);
 				
             // 1.고객관리 입력 및 수정 WMSCT010
        	 dao.insertExcelInfo_AS(model, list);
             dao.updateFileRow(model);
    		 
    		 m.put("MSG", MessageResolver.getMessage("save.success"));
    		 m.put("MSG_ORA", "");
    		 m.put("errCnt", errCnt);
    		 
    	 } catch(Exception e){
    		 m.put("MSG", MessageResolver.getMessage("save.error"));
    		 m.put("MSG_ORA", e.getMessage());
    		 m.put("errCnt", "1");
    		 throw e;
    	 }
    	 return m;
     }
     
     /**
      * 
      * 대체 Method ID   : keyInSaveInfoMulti
      * 대체 Method 설명    : 배송주문(H/D) 일반저장
      * 작성자                      : chsong
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> keyInSaveInfoMulti(Map<String, Object> model, String[] cellName) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         int lastCell = cellName.length;
         int errCnt = 0;
         
         List list = new ArrayList();
         Map rowMap = null;
         
         try{
        	 for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	 rowMap = new LinkedHashMap();
            	 
        		 for(int k = 0 ; k < lastCell; k ++){
        			 rowMap.put(cellName[k], model.get(cellName[k]+i));
        		 }
        		 list.add(rowMap);
        	 }
        	 
        	 int insertCnt = (list != null)?list.size():0;
 
             model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
             model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
             model.put("FILE_ROW" , insertCnt);
 				
             // 1.고객관리 입력 및 수정 WMSCT010
             if (model.get("vrSaveGb").equals("INSERT")){
//            	 dao.insertExcelInfo(model, list);
            	 dao.insertExcelInfoMulti(model, list);
             }else if(model.get("vrSaveGb").equals("UPDATE")){
            	 dao.insertExcelInfoUpdate(model, list);
             }
             dao.updateFileRow(model);
             
             m.put("MSG", MessageResolver.getMessage("save.success"));
             m.put("MSG_ORA", "");
             m.put("errCnt", errCnt);
                 
         } catch(Exception e){
        	 m.put("MSG", MessageResolver.getMessage("save.error"));
        	 m.put("MSG_ORA", e.getMessage());
        	 m.put("errCnt", "1");
        	 throw e;
         }
         return m;
     }
     
     /**
      * Method ID : saveExcelInfoMulti
      * Method 설명 : 엑셀읽기저장
      * 작성자 : 기드온
      * @param model
      * @return
      * @throws Exception
      */
     public Map<String, Object> saveExcelInfoMulti(Map<String, Object> model, List list) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         int errCnt = 0;
         int insertCnt = (list != null)?list.size():0;
             try{
             	model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
 				model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
 				model.put("FILE_ROW" , insertCnt);
 				
 				// 1.고객관리 입력 및 수정 WMSCT010
 				if (model.get("vrSaveGb").equals("INSERT")){
 					dao.insertExcelInfoMulti(model, list);
 				}else if(model.get("vrSaveGb").equals("UPDATE")){
 					dao.insertExcelInfoUpdate(model, list);
 				}
                 dao.updateFileRow(model);
                 
                 m.put("MSG", MessageResolver.getMessage("save.success"));
                 m.put("MSG_ORA", "");
                 m.put("errCnt", errCnt);
                 
             } catch(Exception e){
                 m.put("MSG", MessageResolver.getMessage("save.error"));
     			m.put("MSG_ORA", e.getMessage());
     			m.put("errCnt", "1");
                 throw e;
             }
         return m;
     }
     
     /*-
 	 * Method ID	: checkInsertPossible
 	 * Method 설명	: 주문등록 시간체크
 	 * 작성자			: 
 	 * @param 
 	 * @return
 	 */
     public Map<String, Object> checkInsertPossible(Map<String, Object> model) throws Exception {
 		Map<String, Object> map = new HashMap<String, Object>();
 		map.put("DS", dao.checkInsertPossible(model));
 		return map;
 	}
     
	public String replaceStr(Map<String, Object> obj, int idx, String bindColNm){
    	String strResult = String.valueOf(obj.get("S_" + idx));
    	
    	if(strResult.equals("null")){
    		strResult = String.valueOf(obj.get(bindColNm));
    		if(strResult.equals("null")){
    			strResult = "";
    		}
    	}
    	
    	return strResult;
    }

	 public String replaceStr(JsonObject obj, int idx, String bindColNm){
	 	String strResult = String.valueOf(obj.get("S_" + idx));
	 	
	 	if(strResult.equals("null")){
	 		strResult = String.valueOf(obj.get(bindColNm)).replaceAll("\"", "");
	 	}
	 	
	 	return strResult;
	 }
     
     public String replaceStrMap(Map<String, Object> obj, int idx, String bindColNm){
     	String strResult = String.valueOf(obj.get("S_" + idx));
     	
     	if(strResult.equals("null")){
     		strResult = String.valueOf(obj.get(bindColNm));
     		if(strResult.equals("null")){
     			strResult = "";
     		}
     	}
     	
     	return strResult;
     }

     /**
      * 
      * 대체 Method ID   : spInsertKeyIn_DLV
      * 대체 Method 설명    : 배송주문일반업로드  Package
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> spInsertKeyIn_DLV(Map<String, Object> model, String[] cellName) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 int insHead = cellName.length;
    	 int insBody = Integer.parseInt(model.get("selectIds").toString()) ;
    	 try{
    		 if(insBody < 1){
    			 m.put("MSG", MessageResolver.getMessage("save.error"));
    			 m.put("MSG_ORA", "데이터가 없습니다.");
    			 m.put("errCnt", 1);
    			 return m;
    		 }
    		 
    		 String[] oseq            = new String[insBody];
    		 String[] odlvcomid       = new String[insBody];
    		 String[] osalescompanyid = new String[insBody];
    		 String[] oorgordid       = new String[insBody];
    		 String[] ocustomernm     = new String[insBody];
    		 String[] ophone1         = new String[insBody];
    		 String[] ophone2         = new String[insBody];
    		 String[] ozip            = new String[insBody];
    		 String[] oaddr           = new String[insBody];
    		 String[] oproduct        = new String[insBody];
    		 String[] oqty            = new String[insBody];
    		 String[] oproductarr     = new String[insBody];
    		 String[] oqtyarr         = new String[insBody];
    		 String[] odlvordtype     = new String[insBody];
    		 String[] odlvsetdt       = new String[insBody];
    		 String[] oetc1           = new String[insBody];
    		 String[] omemo           = new String[insBody];
    		 String[] obuyeddt        = new String[insBody];
    		 String[] ocase1          = new String[insBody];
    		 String[] obuycustnm      = new String[insBody];
    		 String[] obuyphone1      = new String[insBody];
    		 String[] obuyphone2	  = new String[insBody];
    		 
    		 for(int i=0; i<insBody; i++){
    			 
    			 String O_SEQ               = "";
    			 String O_DLV_COM_ID        = "";
    			 String O_SALES_COMPANY_ID  = "";
    			 String O_ORG_ORD_ID        = "";
    			 String O_CUSTOMER_NM       = "";
    			 String O_PHONE_1           = "";
    			 String O_PHONE_2           = "";
    			 String O_ZIP               = "";
    			 String O_ADDR              = "";
    			 String O_PRODUCT           = "";
    			 String O_QTY               = "";
    			 String O_PRODUCT_ARR       = "";
    			 String O_QTY_ARR           = "";
    			 String O_DLV_ORD_TYPE      = "";
    			 String O_DLV_SET_DT        = "";
    			 String O_ETC1              = "";
    			 String O_MEMO              = "";
    			 String O_BUYED_DT          = "";
    			 String O_CASE1             = "";
    			 String O_BUY_CUST_NM       = "";
    			 String O_BUY_PHONE_1       = "";
    			 String O_BUY_PHONE_2       = "";
    			 String productLength		= "";
    			 String qtyLength			= "";
    			 
    			 for(int k = 0 ; k < insHead; k ++){
    				 String bindColNm = String.valueOf(cellName[k]);
    				 switch (bindColNm) {
    				 case "O_SEQ"               :  O_SEQ              = (String)model.get(bindColNm+i); break;
    				 case "O_DLV_COM_ID"        :  O_DLV_COM_ID       = (String)model.get(bindColNm+i); break;
    				 case "O_SALES_COMPANY_ID"  :  O_SALES_COMPANY_ID = (String)model.get(bindColNm+i); break;
    				 case "O_ORG_ORD_ID"        :  O_ORG_ORD_ID       = (String)model.get(bindColNm+i); break;
    				 case "O_CUSTOMER_NM"       :  O_CUSTOMER_NM      = (String)model.get(bindColNm+i); break;
    				 case "O_PHONE_1"           :  O_PHONE_1          = (String)model.get(bindColNm+i); break;
    				 case "O_PHONE_2"           :  O_PHONE_2          = (String)model.get(bindColNm+i); break;
    				 case "O_ZIP"               :  O_ZIP              = (String)model.get(bindColNm+i); break;
    				 case "O_ADDR"              :  O_ADDR             = (String)model.get(bindColNm+i); break;
    				 case "O_PRODUCT"           :  O_PRODUCT          = (String)model.get(bindColNm+i); break;
    				 case "O_QTY"               :  O_QTY              = (String)model.get(bindColNm+i); break;
    				 case "O_PRODUCT_D0"        :  productLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_QTY_D0"            :  qtyLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_QTY_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_PRODUCT_D1"        :  productLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_QTY_D1"            :  qtyLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_QTY_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_PRODUCT_D2"        :  productLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_QTY_D2"            :  qtyLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_QTY_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_PRODUCT_D3"        :  productLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_QTY_D3"            :  qtyLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_QTY_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_PRODUCT_D4"        :  productLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_QTY_D4"            :  qtyLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_QTY_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_PRODUCT_D5"        :  productLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_QTY_D5"            :  qtyLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_QTY_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_PRODUCT_D6"        :  productLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_QTY_D6"            :  qtyLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_QTY_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_PRODUCT_D7"        :  productLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_QTY_D7"            :  qtyLength = ((String)model.get(bindColNm+i)).length() != 0 ? O_QTY_ARR+="*"+(String)model.get(bindColNm+i) : ""; break;
    				 case "O_DLV_ORD_TYPE"      :  O_DLV_ORD_TYPE     = (String)model.get(bindColNm+i); break;
    				 case "O_DLV_SET_DT"        :  O_DLV_SET_DT       = (String)model.get(bindColNm+i); break;
    				 case "O_ETC1"              :  O_ETC1             = (String)model.get(bindColNm+i); break;
    				 case "O_MEMO"              :  O_MEMO             = (String)model.get(bindColNm+i); break;
    				 case "O_BUYED_DT"          :  O_BUYED_DT         = (String)model.get(bindColNm+i); break;
    				 case "O_CASE1"             :  O_CASE1            = (String)model.get(bindColNm+i); break;
    				 case "O_BUY_CUST_NM"       :  O_BUY_CUST_NM      = (String)model.get(bindColNm+i); break;
    				 case "O_BUY_PHONE_1"       :  O_BUY_PHONE_1      = (String)model.get(bindColNm+i); break;
    				 case "O_BUY_PHONE_2"       :  O_BUY_PHONE_2      = (String)model.get(bindColNm+i); break;
    				 }
    			 }
    			 
    			 oseq            [i] = O_SEQ             ;
    			 odlvcomid       [i] = model.get("SS_SVC_NO").equals("0000003700") ? "JOA" : O_DLV_COM_ID ;
    			 osalescompanyid [i] = O_SALES_COMPANY_ID;
    			 oorgordid       [i] = O_ORG_ORD_ID      ;
    			 ocustomernm     [i] = O_CUSTOMER_NM     ;
    			 ophone1         [i] = O_PHONE_1         ;
    			 ophone2         [i] = O_PHONE_2         ;
    			 ozip            [i] = O_ZIP             ;
    			 oaddr           [i] = O_ADDR            ;
    			 oproduct        [i] = O_PRODUCT         ;
    			 oqty            [i] = O_QTY             ;
    			 oproductarr     [i] = O_PRODUCT_ARR     ;
    			 oqtyarr         [i] = O_QTY_ARR         ;
    			 odlvordtype     [i] = O_DLV_ORD_TYPE    ;
    			 odlvsetdt       [i] = O_DLV_SET_DT      ;
    			 oetc1           [i] = O_ETC1            ;
    			 omemo           [i] = O_MEMO            ;
    			 obuyeddt        [i] = O_BUYED_DT        ;
    			 ocase1          [i] = O_CASE1           ;
    			 obuycustnm      [i] = O_BUY_CUST_NM     ;
    			 obuyphone1      [i] = O_BUY_PHONE_1     ;
    			 obuyphone2		 [i] = O_BUY_PHONE_2     ; 
    			 
    		 }
    		 Map<String, Object> modelDt = new HashMap<String, Object>();
    		 modelDt.put("I_SEQ"              , oseq            );
    		 modelDt.put("I_DLV_COM_ID"       , odlvcomid       );
    		 modelDt.put("I_SALES_COMPANY_ID" , osalescompanyid );
    		 modelDt.put("I_ORG_ORD_ID"       , oorgordid       );
    		 modelDt.put("I_CUSTOMER_NM"      , ocustomernm     );
    		 modelDt.put("I_PHONE_1"          , ophone1         );
    		 modelDt.put("I_PHONE_2"          , ophone2         );
    		 modelDt.put("I_ZIP"              , ozip            );
    		 modelDt.put("I_ADDR"             , oaddr           );
    		 modelDt.put("I_PRODUCT"          , oproduct        );
    		 modelDt.put("I_QTY"              , oqty            );
    		 modelDt.put("I_PRODUCT_ARR"      , oproductarr     );
    		 modelDt.put("I_QTY_ARR"          , oqtyarr         );
    		 modelDt.put("I_DLV_ORD_TYPE"     , odlvordtype     );
    		 modelDt.put("I_DLV_SET_DT"       , odlvsetdt       );
    		 modelDt.put("I_ETC1"             , oetc1           );
    		 modelDt.put("I_MEMO"             , omemo           );
    		 modelDt.put("I_BUYED_DT"         , obuyeddt        );
    		 modelDt.put("I_CASE1"            , ocase1          );
    		 modelDt.put("I_BUY_CUST_NM"      , obuycustnm      );
    		 modelDt.put("I_BUY_PHONE_1"      , obuyphone1      );
    		 modelDt.put("I_BUY_PHONE_2"      , obuyphone2	    );
    		 modelDt.put("I_TRUST_CUST_ID"    , model.get("vrTrustCustId"));
    		 modelDt.put("I_TRUST_CUST_CD"    , model.get("vrTrustCustCd"));
    		 modelDt.put("I_UPLOAD_ID"   	 , model.get("uploadId"));
    		 modelDt.put("I_LC_ID"    		 , model.get(ConstantIF.SS_SVC_NO));
    		 modelDt.put("I_USER_NO"   		 , model.get(ConstantIF.SS_USER_NO));
    		 
    		 modelDt = (Map<String, Object>)dao000.spDlvCustInfoInsert(modelDt);
    		 ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
    		 m.put("O_CUR", modelDt.get("O_CUR"));       
    		 
    		 if(modelDt.get("O_MSG_CODE").equals("2")) {
    			 m.put("MSG", MessageResolver.getMessage("save.error"));
    			 m.put("MSG_ORA", "");
    			 m.put("errCnt", modelDt.get("O_MSG_CODE"));
    		 }else{
    			 m.put("MSG", MessageResolver.getMessage("save.success"));
    			 m.put("MSG_ORA", "");
    			 m.put("errCnt", modelDt.get("O_MSG_CODE"));
    		 }
    	 } catch(Exception e){
    		 throw e;
    	 }
    	 return m;
     }
     
     /**
      * 대체 Method ID   : spInsertExcel_DLV
      * 대체 Method 설명    : 배송주문 엑셀 업로드 Package
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> spInsertExcel_DLV(Map<String, Object> model, List list, String[] cellName) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 List<Map<String, Object>> listBody = list;
    	 int insBody = (listBody != null)?listBody.size():0;
    	 int insHead = cellName.length;
    	 try{
    		 if(insBody < 1){
    			 m.put("MSG", MessageResolver.getMessage("save.error"));
    			 m.put("MSG_ORA", "데이터가 없습니다.");
    			 m.put("errCnt", 1);
    			 return m;
    		 }
    		 
    		 String[] oseq            = new String[insBody];
    		 String[] odlvcomid       = new String[insBody];
    		 String[] osalescompanyid = new String[insBody];
    		 String[] oorgordid       = new String[insBody];
    		 String[] ocustomernm     = new String[insBody];
    		 String[] ophone1         = new String[insBody];
    		 String[] ophone2         = new String[insBody];
    		 String[] ozip            = new String[insBody];
    		 String[] oaddr           = new String[insBody];
    		 String[] oproduct        = new String[insBody];
    		 String[] oqty            = new String[insBody];
    		 String[] oproductarr     = new String[insBody];
    		 String[] oqtyarr         = new String[insBody];
    		 String[] odlvordtype     = new String[insBody];
    		 String[] odlvsetdt       = new String[insBody];
    		 String[] oetc1           = new String[insBody];
    		 String[] omemo           = new String[insBody];
    		 String[] obuyeddt        = new String[insBody];
    		 String[] ocase1          = new String[insBody];
    		 String[] obuycustnm      = new String[insBody];
    		 String[] obuyphone1      = new String[insBody];
    		 String[] obuyphone2	  = new String[insBody];
    		 
    		 for(int i=0; i<insBody; i++){
    			 
    			 String O_SEQ               = "";
    			 String O_DLV_COM_ID        = "";
    			 String O_SALES_COMPANY_ID  = "";
    			 String O_ORG_ORD_ID        = "";
    			 String O_CUSTOMER_NM       = "";
    			 String O_PHONE_1           = "";
    			 String O_PHONE_2           = "";
    			 String O_ZIP               = "";
    			 String O_ADDR              = "";
    			 String O_PRODUCT           = "";
    			 String O_QTY               = "";
    			 String O_PRODUCT_ARR       = "";
    			 String O_QTY_ARR           = "";
    			 String O_DLV_ORD_TYPE      = "";
    			 String O_DLV_SET_DT        = "";
    			 String O_ETC1              = "";
    			 String O_MEMO              = "";
    			 String O_BUYED_DT          = "";
    			 String O_CASE1             = "";
    			 String O_BUY_CUST_NM       = "";
    			 String O_BUY_PHONE_1       = "";
    			 String O_BUY_PHONE_2       = "";
    			 String productLength		= "";
    			 String qtyLength			= "";
    			 
    			 for(int j=0; j<insHead; j++){
    				 Map<String, Object> object = listBody.get(i);
    				 String bindColNm = String.valueOf(cellName[j]);
    				 switch (bindColNm) {
    				 case "O_SEQ"               :  O_SEQ              = (String)object.get(bindColNm); break;
    				 case "O_DLV_COM_ID"        :  O_DLV_COM_ID       = (String)object.get(bindColNm); break;
    				 case "O_SALES_COMPANY_ID"  :  O_SALES_COMPANY_ID = (String)object.get(bindColNm); break;
    				 case "O_ORG_ORD_ID"        :  O_ORG_ORD_ID       = (String)object.get(bindColNm); break;
    				 case "O_CUSTOMER_NM"       :  O_CUSTOMER_NM      = (String)object.get(bindColNm); break;
    				 case "O_PHONE_1"           :  O_PHONE_1          = (String)object.get(bindColNm); break;
    				 case "O_PHONE_2"           :  O_PHONE_2          = (String)object.get(bindColNm); break;
    				 case "O_ZIP"               :  O_ZIP              = (String)object.get(bindColNm); break;
    				 case "O_ADDR"              :  O_ADDR             = (String)object.get(bindColNm); break;
    				 case "O_PRODUCT"           :  O_PRODUCT          = (String)object.get(bindColNm); break;
    				 case "O_QTY"               :  O_QTY              = (String)object.get(bindColNm); break;
    				 case "O_PRODUCT_D0"        :  productLength = ((String)object.get(bindColNm)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_QTY_D0"            :  qtyLength = ((String)object.get(bindColNm)).length() != 0 ? O_QTY_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_PRODUCT_D1"        :  productLength = ((String)object.get(bindColNm)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_QTY_D1"            :  qtyLength = ((String)object.get(bindColNm)).length() != 0 ? O_QTY_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_PRODUCT_D2"        :  productLength = ((String)object.get(bindColNm)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_QTY_D2"            :  qtyLength = ((String)object.get(bindColNm)).length() != 0 ? O_QTY_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_PRODUCT_D3"        :  productLength = ((String)object.get(bindColNm)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_QTY_D3"            :  qtyLength = ((String)object.get(bindColNm)).length() != 0 ? O_QTY_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_PRODUCT_D4"        :  productLength = ((String)object.get(bindColNm)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_QTY_D4"            :  qtyLength = ((String)object.get(bindColNm)).length() != 0 ? O_QTY_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_PRODUCT_D5"        :  productLength = ((String)object.get(bindColNm)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_QTY_D5"            :  qtyLength = ((String)object.get(bindColNm)).length() != 0 ? O_QTY_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_PRODUCT_D6"        :  productLength = ((String)object.get(bindColNm)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_QTY_D6"            :  qtyLength = ((String)object.get(bindColNm)).length() != 0 ? O_QTY_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_PRODUCT_D7"        :  productLength = ((String)object.get(bindColNm)).length() != 0 ? O_PRODUCT_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_QTY_D7"            :  qtyLength = ((String)object.get(bindColNm)).length() != 0 ? O_QTY_ARR+="*"+(String)object.get(bindColNm) : ""; break;
    				 case "O_DLV_ORD_TYPE"      :  O_DLV_ORD_TYPE     = (String)object.get(bindColNm); break;
    				 case "O_DLV_SET_DT"        :  O_DLV_SET_DT       = (String)object.get(bindColNm); break;
    				 case "O_ETC1"              :  O_ETC1             = (String)object.get(bindColNm); break;
    				 case "O_MEMO"              :  O_MEMO             = (String)object.get(bindColNm); break;
    				 case "O_BUYED_DT"          :  O_BUYED_DT         = (String)object.get(bindColNm); break;
    				 case "O_CASE1"             :  O_CASE1            = (String)object.get(bindColNm); break;
    				 case "O_BUY_CUST_NM"       :  O_BUY_CUST_NM      = (String)object.get(bindColNm); break;
    				 case "O_BUY_PHONE_1"       :  O_BUY_PHONE_1      = (String)object.get(bindColNm); break;
    				 case "O_BUY_PHONE_2"       :  O_BUY_PHONE_2      = (String)object.get(bindColNm); break;
    				 }
    			 }
    			 oseq            [i] = O_SEQ             ;
    			 odlvcomid       [i] = model.get("SS_SVC_NO").equals("0000003700") ? "JOA" : O_DLV_COM_ID ;
    			 osalescompanyid [i] = O_SALES_COMPANY_ID;
    			 oorgordid       [i] = O_ORG_ORD_ID      ;
    			 ocustomernm     [i] = O_CUSTOMER_NM     ;
    			 ophone1         [i] = O_PHONE_1         ;
    			 ophone2         [i] = O_PHONE_2         ;
    			 ozip            [i] = O_ZIP             ;
    			 oaddr           [i] = O_ADDR            ;
    			 oproduct        [i] = O_PRODUCT         ;
    			 oqty            [i] = O_QTY             ;
    			 oproductarr     [i] = O_PRODUCT_ARR     ;
    			 oqtyarr         [i] = O_QTY_ARR         ;
    			 odlvordtype     [i] = O_DLV_ORD_TYPE    ;
    			 odlvsetdt       [i] = O_DLV_SET_DT      ;
    			 oetc1           [i] = O_ETC1            ;
    			 omemo           [i] = O_MEMO            ;
    			 obuyeddt        [i] = O_BUYED_DT        ;
    			 ocase1          [i] = O_CASE1           ;
    			 obuycustnm      [i] = O_BUY_CUST_NM     ;
    			 obuyphone1      [i] = O_BUY_PHONE_1     ;
    			 obuyphone2		 [i] = O_BUY_PHONE_2     ; 
    			 
    		 }
    		 Map<String, Object> modelDt = new HashMap<String, Object>();
    		 modelDt.put("I_SEQ"              , oseq            );
    		 modelDt.put("I_DLV_COM_ID"       , odlvcomid       );
    		 modelDt.put("I_SALES_COMPANY_ID" , osalescompanyid );
    		 modelDt.put("I_ORG_ORD_ID"       , oorgordid       );
    		 modelDt.put("I_CUSTOMER_NM"      , ocustomernm     );
    		 modelDt.put("I_PHONE_1"          , ophone1         );
    		 modelDt.put("I_PHONE_2"          , ophone2         );
    		 modelDt.put("I_ZIP"              , ozip            );
    		 modelDt.put("I_ADDR"             , oaddr           );
    		 modelDt.put("I_PRODUCT"          , oproduct        );
    		 modelDt.put("I_QTY"              , oqty            );
    		 modelDt.put("I_PRODUCT_ARR"      , oproductarr     );
    		 modelDt.put("I_QTY_ARR"          , oqtyarr         );
    		 modelDt.put("I_DLV_ORD_TYPE"     , odlvordtype     );
    		 modelDt.put("I_DLV_SET_DT"       , odlvsetdt       );
    		 modelDt.put("I_ETC1"             , oetc1           );
    		 modelDt.put("I_MEMO"             , omemo           );
    		 modelDt.put("I_BUYED_DT"         , obuyeddt        );
    		 modelDt.put("I_CASE1"            , ocase1          );
    		 modelDt.put("I_BUY_CUST_NM"      , obuycustnm      );
    		 modelDt.put("I_BUY_PHONE_1"      , obuyphone1      );
    		 modelDt.put("I_BUY_PHONE_2"      , obuyphone2	    );
    		 modelDt.put("I_TRUST_CUST_ID"    , model.get("vrTrustCustId"));
    		 modelDt.put("I_TRUST_CUST_CD"    , model.get("vrTrustCustCd"));
    		 modelDt.put("I_UPLOAD_ID"   	 , model.get("uploadId"));
    		 modelDt.put("I_LC_ID"    		 , model.get(ConstantIF.SS_SVC_NO));
    		 modelDt.put("I_USER_NO"   		 , model.get(ConstantIF.SS_USER_NO));
    		 
    		 modelDt = (Map<String, Object>)dao000.spDlvCustInfoInsert(modelDt);
    		 ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
    		 m.put("O_CUR", modelDt.get("O_CUR"));       
    		 
    		 if(modelDt.get("O_MSG_CODE").equals("2")) {
    			 m.put("MSG", MessageResolver.getMessage("save.error"));
    			 m.put("MSG_ORA", "");
    			 m.put("errCnt", modelDt.get("O_MSG_CODE"));
    		 }else{
    			 m.put("MSG", MessageResolver.getMessage("save.success"));
    			 m.put("MSG_ORA", "");
    			 m.put("errCnt", modelDt.get("O_MSG_CODE"));
    		 }
    	 } catch(Exception e){
			 m.put("MSG", e.getMessage());
			 m.put("MSG_ORA", "");
			 m.put("errCnt", 2);
    		 throw e;
    	 }
    	 return m;
     }
     /**
      * 
      * 대체 Method ID   : spInsertKeyIn_CT
      * 대체 Method 설명    : 고객주문일반업로드  Package
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> spInsertKeyIn_CT(Map<String, Object> model, String[] cellName) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         int insHead = cellName.length;
         int insBody = Integer.parseInt(model.get("selectIds").toString()) ;
         try{
     		if(insBody < 1){
     			m.put("MSG", MessageResolver.getMessage("save.error"));
     			m.put("MSG_ORA", "데이터가 없습니다.");
     			m.put("errCnt", 1);
     			return m;
     		}

     		String[] oseq             = new String[insBody];
     		String[] odlvcomid        = new String[insBody];
     		String[] osalescompanyid  = new String[insBody];
     		String[] oorgordid        = new String[insBody];
     		String[] ofirstinsdt      = new String[insBody];
     		String[] ocustomernm      = new String[insBody];
     		String[] ophone1          = new String[insBody];
     		String[] ophone2          = new String[insBody];
     		String[] ozip             = new String[insBody];
     		String[] oaddr            = new String[insBody];
     		String[] oproduct         = new String[insBody];
     		String[] oqty             = new String[insBody];
     		String[] obuyeddt         = new String[insBody];
     		String[] obuycustnm       = new String[insBody];
     		String[] obuyphone1       = new String[insBody];
     		String[] obuyphone2       = new String[insBody];
     		
             for(int i = 0 ; i < insBody ; i ++){
     			
     			String O_SEQ               = "" ;
     			String O_DLV_COM_ID        = "" ;
     			String O_SALES_COMPANY_ID  = "" ;
     			String O_ORG_ORD_ID        = "" ;
     			String O_FIRST_INS_DT      = "" ;
     			String O_CUSTOMER_NM       = "" ;
     			String O_PHONE_1           = "" ;
     			String O_PHONE_2           = "" ;
     			String O_ZIP               = "" ;
     			String O_ADDR              = "" ;
     			String O_PRODUCT           = "" ;
     			String O_QTY               = "" ;
     			String O_BUYED_DT          = "" ;
     			String O_BUY_CUST_NM       = "" ;
     			String O_BUY_PHONE_1       = "" ;
     			String O_BUY_PHONE_2       = "" ;
     			
                 for(int k = 0 ; k < insHead; k ++){
     				String bindColNm = String.valueOf(cellName[k]);
     				switch (bindColNm) {
     					case "O_SEQ"              : O_SEQ                 = (String)model.get(bindColNm+i); break;
     					case "O_DLV_COM_ID"       : O_DLV_COM_ID          = (String)model.get(bindColNm+i); break;
     					case "O_SALES_COMPANY_ID" : O_SALES_COMPANY_ID    = (String)model.get(bindColNm+i); break;
     					case "O_ORG_ORD_ID"       : O_ORG_ORD_ID          = (String)model.get(bindColNm+i); break;
     					case "O_FIRST_INS_DT"     : O_FIRST_INS_DT        = (String)model.get(bindColNm+i); break;
     					case "O_CUSTOMER_NM"      : O_CUSTOMER_NM         = (String)model.get(bindColNm+i); break;
     					case "O_PHONE_1"          : O_PHONE_1             = (String)model.get(bindColNm+i); break;
     					case "O_PHONE_2"          : O_PHONE_2             = (String)model.get(bindColNm+i); break;
     					case "O_ZIP"              : O_ZIP                 = (String)model.get(bindColNm+i); break;
     					case "O_ADDR"             : O_ADDR                = (String)model.get(bindColNm+i); break;
     					case "O_PRODUCT"          : O_PRODUCT             = (String)model.get(bindColNm+i); break;
     					case "O_QTY"              : O_QTY                 = (String)model.get(bindColNm+i); break;
     					case "O_BUYED_DT"         : O_BUYED_DT            = (String)model.get(bindColNm+i); break;
     					case "O_BUY_CUST_NM"      : O_BUY_CUST_NM         = (String)model.get(bindColNm+i); break;
     					case "O_BUY_PHONE_1"      : O_BUY_PHONE_1         = (String)model.get(bindColNm+i); break;
     					case "O_BUY_PHONE_2"      : O_BUY_PHONE_2         = (String)model.get(bindColNm+i); break;
     				}
                 }

     			oseq           [i] = O_SEQ             ;
     			odlvcomid      [i] = O_DLV_COM_ID      ;
     			osalescompanyid[i] = O_SALES_COMPANY_ID;
     			oorgordid      [i] = O_ORG_ORD_ID      ;
     			ofirstinsdt    [i] = O_FIRST_INS_DT    ;
     			ocustomernm    [i] = O_CUSTOMER_NM     ;
     			ophone1        [i] = O_PHONE_1         ;
     			ophone2        [i] = O_PHONE_2         ;
     			ozip           [i] = O_ZIP             ;
     			oaddr          [i] = O_ADDR            ;
     			oproduct       [i] = O_PRODUCT         ;
     			oqty           [i] = O_QTY             ;
     			obuyeddt       [i] = O_BUYED_DT        ;
     			obuycustnm     [i] = O_BUY_CUST_NM     ;
     			obuyphone1     [i] = O_BUY_PHONE_1     ;
     			obuyphone2     [i] = O_BUY_PHONE_2     ;
             }
             
             Map<String, Object> modelDt = new HashMap<String, Object>();
     		modelDt.put("I_SEQ"              , oseq           );
     		modelDt.put("I_DLV_COM_ID"       , odlvcomid      );
     		modelDt.put("I_SALES_COMPANY_ID" , osalescompanyid);
     		modelDt.put("I_ORG_ORD_ID"       , oorgordid      );
     		modelDt.put("I_FIRST_INS_DT"     , ofirstinsdt    );
     		modelDt.put("I_CUSTOMER_NM"      , ocustomernm    );
     		modelDt.put("I_PHONE_1"          , ophone1        );
     		modelDt.put("I_PHONE_2"          , ophone2        );
     		modelDt.put("I_ZIP"              , ozip           );
     		modelDt.put("I_ADDR"             , oaddr          );
     		modelDt.put("I_PRODUCT"          , oproduct       );
     		modelDt.put("I_QTY"              , oqty           );
     		modelDt.put("I_BUYED_DT"         , obuyeddt       );
     		modelDt.put("I_BUY_CUST_NM"      , obuycustnm     );
     		modelDt.put("I_BUY_PHONE_1"      , obuyphone1     );
     		modelDt.put("I_BUY_PHONE_2"      , obuyphone2     );
     		modelDt.put("I_TRUST_CUST_ID"    , model.get("vrTrustCustId"));
     		modelDt.put("I_TRUST_CUST_CD"    , model.get("vrTrustCustCd"));
     		modelDt.put("I_UPLOAD_ID"   	 , model.get("uploadId"));
     		modelDt.put("I_LC_ID"    		 , model.get(ConstantIF.SS_SVC_NO));
     		modelDt.put("I_USER_NO"   		 , model.get(ConstantIF.SS_USER_NO));
     		
     		modelDt = (Map<String, Object>)dao000.spCustInfoInsert(modelDt);
     		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
     		m.put("O_CUR", modelDt.get("O_CUR"));       
     		
     		if(modelDt.get("O_MSG_CODE").equals("2")) {
     			m.put("MSG", MessageResolver.getMessage("save.error"));
     			m.put("MSG_ORA", "");
     			m.put("errCnt", modelDt.get("O_MSG_CODE"));
     		}else{
     			m.put("MSG", MessageResolver.getMessage("save.success"));
     			m.put("MSG_ORA", "");
     			m.put("errCnt", modelDt.get("O_MSG_CODE"));
     		}
         } catch(Exception e){
             throw e;
         }
         return m;
     }
     
     /**
      * 대체 Method ID   : spInsertExcel_CT
      * 대체 Method 설명    : 고객주문 엑셀 업로드 Package
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> spInsertExcel_CT(Map<String, Object> model, List list, String[] cellName) throws Exception {
     	Map<String, Object> m = new HashMap<String, Object>();
     	List<Map<String, Object>> listBody = list;
         int insBody = (listBody != null)?listBody.size():0;
         int insHead = cellName.length;
     	try{
             if(insBody < 1){
             	m.put("MSG", MessageResolver.getMessage("save.error"));
                 m.put("MSG_ORA", "데이터가 없습니다.");
                 m.put("errCnt", 1);
             	return m;
             }

             String[] oseq             = new String[insBody];
             String[] odlvcomid        = new String[insBody];
             String[] osalescompanyid  = new String[insBody];
             String[] oorgordid        = new String[insBody];
             String[] ofirstinsdt      = new String[insBody];
             String[] ocustomernm      = new String[insBody];
             String[] ophone1          = new String[insBody];
             String[] ophone2          = new String[insBody];
             String[] ozip             = new String[insBody];
             String[] oaddr            = new String[insBody];
             String[] oproduct         = new String[insBody];
             String[] oqty             = new String[insBody];
             String[] obuyeddt         = new String[insBody];
             String[] obuycustnm       = new String[insBody];
             String[] obuyphone1       = new String[insBody];
             String[] obuyphone2       = new String[insBody];
             
     		for(int i=0; i<insBody; i++){
     			
     			String O_SEQ               = "" ;
     			String O_DLV_COM_ID        = "" ;
     			String O_SALES_COMPANY_ID  = "" ;
     			String O_ORG_ORD_ID        = "" ;
     			String O_FIRST_INS_DT      = "" ;
     			String O_CUSTOMER_NM       = "" ;
     			String O_PHONE_1           = "" ;
     			String O_PHONE_2           = "" ;
     			String O_ZIP               = "" ;
     			String O_ADDR              = "" ;
     			String O_PRODUCT           = "" ;
     			String O_QTY               = "" ;
     			String O_BUYED_DT          = "" ;
     			String O_BUY_CUST_NM       = "" ;
     			String O_BUY_PHONE_1       = "" ;
     			String O_BUY_PHONE_2       = "" ;
     			
     			for(int j=0; j<insHead; j++){
         			Map<String, Object> object = listBody.get(i);
         			String bindColNm = String.valueOf(cellName[j]);
         			switch (bindColNm) {
 	        			case "O_SEQ"              : O_SEQ                 = (String)object.get(bindColNm); break;
 	        			case "O_DLV_COM_ID"       : O_DLV_COM_ID          = (String)object.get(bindColNm); break;
 	        			case "O_SALES_COMPANY_ID" : O_SALES_COMPANY_ID    = (String)object.get(bindColNm); break;
 	        			case "O_ORG_ORD_ID"       : O_ORG_ORD_ID          = (String)object.get(bindColNm); break;
 	        			case "O_FIRST_INS_DT"     : O_FIRST_INS_DT        = (String)object.get(bindColNm); break;
 	        			case "O_CUSTOMER_NM"      : O_CUSTOMER_NM         = (String)object.get(bindColNm); break;
 	        			case "O_PHONE_1"          : O_PHONE_1             = (String)object.get(bindColNm); break;
 	        			case "O_PHONE_2"          : O_PHONE_2             = (String)object.get(bindColNm); break;
 	        			case "O_ZIP"              : O_ZIP                 = (String)object.get(bindColNm); break;
 	        			case "O_ADDR"             : O_ADDR                = (String)object.get(bindColNm); break;
 	        			case "O_PRODUCT"          : O_PRODUCT             = (String)object.get(bindColNm); break;
 	        			case "O_QTY"              : O_QTY                 = (String)object.get(bindColNm); break;
 	        			case "O_BUYED_DT"         : O_BUYED_DT            = (String)object.get(bindColNm); break;
 	        			case "O_BUY_CUST_NM"      : O_BUY_CUST_NM         = (String)object.get(bindColNm); break;
 	        			case "O_BUY_PHONE_1"      : O_BUY_PHONE_1         = (String)object.get(bindColNm); break;
 	        			case "O_BUY_PHONE_2"      : O_BUY_PHONE_2         = (String)object.get(bindColNm); break;
         			}
     			}
     			
     			oseq           [i] = O_SEQ             ;
     			odlvcomid      [i] = O_DLV_COM_ID      ;
     			osalescompanyid[i] = O_SALES_COMPANY_ID;
     			oorgordid      [i] = O_ORG_ORD_ID      ;
     			ofirstinsdt    [i] = O_FIRST_INS_DT    ;
     			ocustomernm    [i] = O_CUSTOMER_NM     ;
     			ophone1        [i] = O_PHONE_1         ;
     			ophone2        [i] = O_PHONE_2         ;
     			ozip           [i] = O_ZIP             ;
     			oaddr          [i] = O_ADDR            ;
     			oproduct       [i] = O_PRODUCT         ;
     			oqty           [i] = O_QTY             ;
     			obuyeddt       [i] = O_BUYED_DT        ;
     			obuycustnm     [i] = O_BUY_CUST_NM     ;
     			obuyphone1     [i] = O_BUY_PHONE_1     ;
     			obuyphone2     [i] = O_BUY_PHONE_2     ;
     			
     		}
 			Map<String, Object> modelDt = new HashMap<String, Object>();
 			modelDt.put("I_SEQ"              , oseq           );
 			modelDt.put("I_DLV_COM_ID"       , odlvcomid      );
 			modelDt.put("I_SALES_COMPANY_ID" , osalescompanyid);
 			modelDt.put("I_ORG_ORD_ID"       , oorgordid      );
 			modelDt.put("I_FIRST_INS_DT"     , ofirstinsdt    );
 			modelDt.put("I_CUSTOMER_NM"      , ocustomernm    );
 			modelDt.put("I_PHONE_1"          , ophone1        );
 			modelDt.put("I_PHONE_2"          , ophone2        );
 			modelDt.put("I_ZIP"              , ozip           );
 			modelDt.put("I_ADDR"             , oaddr          );
 			modelDt.put("I_PRODUCT"          , oproduct       );
 			modelDt.put("I_QTY"              , oqty           );
 			modelDt.put("I_BUYED_DT"         , obuyeddt       );
 			modelDt.put("I_BUY_CUST_NM"      , obuycustnm     );
 			modelDt.put("I_BUY_PHONE_1"      , obuyphone1     );
 			modelDt.put("I_BUY_PHONE_2"      , obuyphone2     );
 			modelDt.put("I_TRUST_CUST_ID"    , model.get("vrTrustCustId"));
 			modelDt.put("I_TRUST_CUST_CD"    , model.get("vrTrustCustCd"));
 			modelDt.put("I_UPLOAD_ID"   	 , model.get("uploadId"));
 			modelDt.put("I_LC_ID"    		 , model.get(ConstantIF.SS_SVC_NO));
 			modelDt.put("I_USER_NO"   		 , model.get(ConstantIF.SS_USER_NO));
 			
 			modelDt = (Map<String, Object>)dao000.spCustInfoInsert(modelDt);
     		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
     		m.put("O_CUR", modelDt.get("O_CUR"));       
     		
     		if(modelDt.get("O_MSG_CODE").equals("2")) {
 	            m.put("MSG", MessageResolver.getMessage("save.error"));
 	            m.put("MSG_ORA", "");
 	            m.put("errCnt", modelDt.get("O_MSG_CODE"));
     		}else{
 	            m.put("MSG", MessageResolver.getMessage("save.success"));
 	            m.put("MSG_ORA", "");
 	            m.put("errCnt", modelDt.get("O_MSG_CODE"));
     		}
     	} catch(Exception e){
			 m.put("MSG", e.getMessage());
			 m.put("MSG_ORA", "");
			 m.put("errCnt", 2);
   		 throw e;
     	}
     	return m;
     }
     
     /**
      * 
      * 대체 Method ID   : spInsertKeyIn_CS
      * 대체 Method 설명    : CS주문일반업로드  Package
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> spInsertKeyIn_CS(Map<String, Object> model, String[] cellName) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 int insHead = cellName.length;
    	 int insBody = Integer.parseInt(model.get("selectIds").toString()) ;
    	 try{
    		 if(insBody < 1){
    			 m.put("MSG", MessageResolver.getMessage("save.error"));
    			 m.put("MSG_ORA", "데이터가 없습니다.");
    			 m.put("errCnt", 1);
    			 return m;
    		 }
    		 
    		 String[] oseq             = new String[insBody];
    		 String[] odlvcomid        = new String[insBody];
    		 String[] osalescompanyid  = new String[insBody];
    		 String[] oorgordid        = new String[insBody];
    		 String[] ofirstinsdt      = new String[insBody];
    		 String[] ocustomernm      = new String[insBody];
    		 String[] ophone1          = new String[insBody];
    		 String[] ophone2          = new String[insBody];
    		 String[] ozip             = new String[insBody];
    		 String[] oaddr            = new String[insBody];
    		 String[] oproduct         = new String[insBody];
    		 String[] oqty             = new String[insBody];
    		 String[] obuyeddt         = new String[insBody];
    		 String[] obuycustnm       = new String[insBody];
    		 String[] obuyphone1       = new String[insBody];
    		 String[] obuyphone2       = new String[insBody];
    		 String[] ocsregcontents   = new String[insBody];
    		 String[] ocsmemo          = new String[insBody];
    		 
    		 for(int i = 0 ; i < insBody ; i ++){
    			 
    			 String O_SEQ               = "" ;
    			 String O_DLV_COM_ID        = "" ;
    			 String O_SALES_COMPANY_ID  = "" ;
    			 String O_ORG_ORD_ID        = "" ;
    			 String O_FIRST_INS_DT      = "" ;
    			 String O_CUSTOMER_NM       = "" ;
    			 String O_PHONE_1           = "" ;
    			 String O_PHONE_2           = "" ;
    			 String O_ZIP               = "" ;
    			 String O_ADDR              = "" ;
    			 String O_PRODUCT           = "" ;
    			 String O_QTY               = "" ;
    			 String O_BUYED_DT          = "" ;
    			 String O_BUY_CUST_NM       = "" ;
    			 String O_BUY_PHONE_1       = "" ;
    			 String O_BUY_PHONE_2       = "" ;
    			 String O_CS_REG_CONTENTS   = "" ;
    			 String O_CS_MEMO       	= "" ;
    			 
    			 for(int k = 0 ; k < insHead; k ++){
    				 String bindColNm = String.valueOf(cellName[k]);
    				 switch (bindColNm) {
    				 case "O_SEQ"              : O_SEQ                 = (String)model.get(bindColNm+i); break;
    				 case "O_DLV_COM_ID"       : O_DLV_COM_ID          = (String)model.get(bindColNm+i); break;
    				 case "O_SALES_COMPANY_ID" : O_SALES_COMPANY_ID    = (String)model.get(bindColNm+i); break;
    				 case "O_ORG_ORD_ID"       : O_ORG_ORD_ID          = (String)model.get(bindColNm+i); break;
    				 case "O_FIRST_INS_DT"     : O_FIRST_INS_DT        = (String)model.get(bindColNm+i); break;
    				 case "O_CUSTOMER_NM"      : O_CUSTOMER_NM         = (String)model.get(bindColNm+i); break;
    				 case "O_PHONE_1"          : O_PHONE_1             = (String)model.get(bindColNm+i); break;
    				 case "O_PHONE_2"          : O_PHONE_2             = (String)model.get(bindColNm+i); break;
    				 case "O_ZIP"              : O_ZIP                 = (String)model.get(bindColNm+i); break;
    				 case "O_ADDR"             : O_ADDR                = (String)model.get(bindColNm+i); break;
    				 case "O_PRODUCT"          : O_PRODUCT             = (String)model.get(bindColNm+i); break;
    				 case "O_QTY"              : O_QTY                 = (String)model.get(bindColNm+i); break;
    				 case "O_BUYED_DT"         : O_BUYED_DT            = (String)model.get(bindColNm+i); break;
    				 case "O_BUY_CUST_NM"      : O_BUY_CUST_NM         = (String)model.get(bindColNm+i); break;
    				 case "O_BUY_PHONE_1"      : O_BUY_PHONE_1         = (String)model.get(bindColNm+i); break;
    				 case "O_BUY_PHONE_2"      : O_BUY_PHONE_2         = (String)model.get(bindColNm+i); break;
    				 case "O_CS_REG_CONTENTS"  : O_CS_REG_CONTENTS     = (String)model.get(bindColNm+i); break;
    				 case "O_CS_MEMO"          : O_CS_MEMO             = (String)model.get(bindColNm+i); break;
    				 }
    			 }
    			 
    			 oseq           [i] = O_SEQ             ;
    			 odlvcomid      [i] = O_DLV_COM_ID      ;
    			 osalescompanyid[i] = O_SALES_COMPANY_ID;
    			 oorgordid      [i] = O_ORG_ORD_ID      ;
    			 ofirstinsdt    [i] = O_FIRST_INS_DT    ;
    			 ocustomernm    [i] = O_CUSTOMER_NM     ;
    			 ophone1        [i] = O_PHONE_1         ;
    			 ophone2        [i] = O_PHONE_2         ;
    			 ozip           [i] = O_ZIP             ;
    			 oaddr          [i] = O_ADDR            ;
    			 oproduct       [i] = O_PRODUCT         ;
    			 oqty           [i] = O_QTY             ;
    			 obuyeddt       [i] = O_BUYED_DT        ;
    			 obuycustnm     [i] = O_BUY_CUST_NM     ;
    			 obuyphone1     [i] = O_BUY_PHONE_1     ;
    			 obuyphone2     [i] = O_BUY_PHONE_2     ;
    			 ocsregcontents [i] = O_CS_REG_CONTENTS ;
    			 ocsmemo        [i] = O_CS_MEMO         ;
    		 }
    		 
    		 Map<String, Object> modelDt = new HashMap<String, Object>();
    		 modelDt.put("I_SEQ"              , oseq           );
    		 modelDt.put("I_DLV_COM_ID"       , odlvcomid      );
    		 modelDt.put("I_SALES_COMPANY_ID" , osalescompanyid);
    		 modelDt.put("I_ORG_ORD_ID"       , oorgordid      );
    		 modelDt.put("I_FIRST_INS_DT"     , ofirstinsdt    );
    		 modelDt.put("I_CUSTOMER_NM"      , ocustomernm    );
    		 modelDt.put("I_PHONE_1"          , ophone1        );
    		 modelDt.put("I_PHONE_2"          , ophone2        );
    		 modelDt.put("I_ZIP"              , ozip           );
    		 modelDt.put("I_ADDR"             , oaddr          );
    		 modelDt.put("I_PRODUCT"          , oproduct       );
    		 modelDt.put("I_QTY"              , oqty           );
    		 modelDt.put("I_BUYED_DT"         , obuyeddt       );
    		 modelDt.put("I_BUY_CUST_NM"      , obuycustnm     );
    		 modelDt.put("I_BUY_PHONE_1"      , obuyphone1     );
    		 modelDt.put("I_BUY_PHONE_2"      , obuyphone2     );
    		 modelDt.put("I_CS_REG_CONTENTS"  , ocsregcontents );
    		 modelDt.put("I_CS_MEMO"          , ocsmemo        );
    		 modelDt.put("I_CS_COMPANY"    	  , model.get(ConstantIF.SS_CMPY_NAME));
    		 modelDt.put("I_TRUST_CUST_ID"    , model.get("vrTrustCustId"));
    		 modelDt.put("I_TRUST_CUST_CD"    , model.get("vrTrustCustCd"));
    		 modelDt.put("I_UPLOAD_ID"   	  , model.get("uploadId"));
    		 modelDt.put("I_LC_ID"    		  , model.get(ConstantIF.SS_SVC_NO));
    		 modelDt.put("I_USER_NO"   		  , model.get(ConstantIF.SS_USER_NO));
    		 
    		 modelDt = (Map<String, Object>)dao000.spCsCustInfoInsert(modelDt);
    		 ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
    		 m.put("O_CUR", modelDt.get("O_CUR"));       
    		 
    		 if(modelDt.get("O_MSG_CODE").equals("2")) {
    			 m.put("MSG", MessageResolver.getMessage("save.error"));
    			 m.put("MSG_ORA", "");
    			 m.put("errCnt", modelDt.get("O_MSG_CODE"));
    		 }else{
    			 m.put("MSG", MessageResolver.getMessage("save.success"));
    			 m.put("MSG_ORA", "");
    			 m.put("errCnt", modelDt.get("O_MSG_CODE"));
    		 }
    	 } catch(Exception e){
    		 throw e;
    	 }
    	 return m;
     }
     
     /**
      * 대체 Method ID   : spInsertExcel_CS
      * 대체 Method 설명    : CS주문 엑셀 업로드 Package
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> spInsertExcel_CS(Map<String, Object> model, List list, String[] cellName) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 List<Map<String, Object>> listBody = list;
    	 int insBody = (listBody != null)?listBody.size():0;
    	 int insHead = cellName.length;
    	 try{
    		 if(insBody < 1){
    			 m.put("MSG", MessageResolver.getMessage("save.error"));
    			 m.put("MSG_ORA", "데이터가 없습니다.");
    			 m.put("errCnt", 1);
    			 return m;
    		 }
    		 
    		 String[] oseq             = new String[insBody];
    		 String[] odlvcomid        = new String[insBody];
    		 String[] osalescompanyid  = new String[insBody];
    		 String[] oorgordid        = new String[insBody];
    		 String[] ofirstinsdt      = new String[insBody];
    		 String[] ocustomernm      = new String[insBody];
    		 String[] ophone1          = new String[insBody];
    		 String[] ophone2          = new String[insBody];
    		 String[] ozip             = new String[insBody];
    		 String[] oaddr            = new String[insBody];
    		 String[] oproduct         = new String[insBody];
    		 String[] oqty             = new String[insBody];
    		 String[] obuyeddt         = new String[insBody];
    		 String[] obuycustnm       = new String[insBody];
    		 String[] obuyphone1       = new String[insBody];
    		 String[] obuyphone2       = new String[insBody];
    		 String[] ocsregcontents   = new String[insBody];
    		 String[] ocsmemo          = new String[insBody];
    		 
    		 for(int i=0; i<insBody; i++){

    			 String O_SEQ               = "" ;
    			 String O_DLV_COM_ID        = "" ;
    			 String O_SALES_COMPANY_ID  = "" ;
    			 String O_ORG_ORD_ID        = "" ;
    			 String O_FIRST_INS_DT      = "" ;
    			 String O_CUSTOMER_NM       = "" ;
    			 String O_PHONE_1           = "" ;
    			 String O_PHONE_2           = "" ;
    			 String O_ZIP               = "" ;
    			 String O_ADDR              = "" ;
    			 String O_PRODUCT           = "" ;
    			 String O_QTY               = "" ;
    			 String O_BUYED_DT          = "" ;
    			 String O_BUY_CUST_NM       = "" ;
    			 String O_BUY_PHONE_1       = "" ;
    			 String O_BUY_PHONE_2       = "" ;
    			 String O_CS_REG_CONTENTS   = "" ;
    			 String O_CS_MEMO       	= "" ;
    			 
    			 for(int j=0; j<insHead; j++){
    				 Map<String, Object> object = listBody.get(i);
    				 String bindColNm = String.valueOf(cellName[j]);
    				 switch (bindColNm) {
    				 case "O_SEQ"              : O_SEQ                 = (String)object.get(bindColNm); break;
    				 case "O_DLV_COM_ID"       : O_DLV_COM_ID          = (String)object.get(bindColNm); break;
    				 case "O_SALES_COMPANY_ID" : O_SALES_COMPANY_ID    = (String)object.get(bindColNm); break;
    				 case "O_ORG_ORD_ID"       : O_ORG_ORD_ID          = (String)object.get(bindColNm); break;
    				 case "O_FIRST_INS_DT"     : O_FIRST_INS_DT        = (String)object.get(bindColNm); break;
    				 case "O_CUSTOMER_NM"      : O_CUSTOMER_NM         = (String)object.get(bindColNm); break;
    				 case "O_PHONE_1"          : O_PHONE_1             = (String)object.get(bindColNm); break;
    				 case "O_PHONE_2"          : O_PHONE_2             = (String)object.get(bindColNm); break;
    				 case "O_ZIP"              : O_ZIP                 = (String)object.get(bindColNm); break;
    				 case "O_ADDR"             : O_ADDR                = (String)object.get(bindColNm); break;
    				 case "O_PRODUCT"          : O_PRODUCT             = (String)object.get(bindColNm); break;
    				 case "O_QTY"              : O_QTY                 = (String)object.get(bindColNm); break;
    				 case "O_BUYED_DT"         : O_BUYED_DT            = (String)object.get(bindColNm); break;
    				 case "O_BUY_CUST_NM"      : O_BUY_CUST_NM         = (String)object.get(bindColNm); break;
    				 case "O_BUY_PHONE_1"      : O_BUY_PHONE_1         = (String)object.get(bindColNm); break;
    				 case "O_BUY_PHONE_2"      : O_BUY_PHONE_2         = (String)object.get(bindColNm); break;
    				 case "O_CS_REG_CONTENTS"  : O_CS_REG_CONTENTS     = (String)object.get(bindColNm); break;
    				 case "O_CS_MEMO"          : O_CS_MEMO             = (String)object.get(bindColNm); break;
    				 }
    			 }

    			 oseq           [i] = O_SEQ             ;
    			 odlvcomid      [i] = O_DLV_COM_ID      ;
    			 osalescompanyid[i] = O_SALES_COMPANY_ID;
    			 oorgordid      [i] = O_ORG_ORD_ID      ;
    			 ofirstinsdt    [i] = O_FIRST_INS_DT    ;
    			 ocustomernm    [i] = O_CUSTOMER_NM     ;
    			 ophone1        [i] = O_PHONE_1         ;
    			 ophone2        [i] = O_PHONE_2         ;
    			 ozip           [i] = O_ZIP             ;
    			 oaddr          [i] = O_ADDR            ;
    			 oproduct       [i] = O_PRODUCT         ;
    			 oqty           [i] = O_QTY             ;
    			 obuyeddt       [i] = O_BUYED_DT        ;
    			 obuycustnm     [i] = O_BUY_CUST_NM     ;
    			 obuyphone1     [i] = O_BUY_PHONE_1     ;
    			 obuyphone2     [i] = O_BUY_PHONE_2     ;
    			 ocsregcontents [i] = O_CS_REG_CONTENTS ;
    			 ocsmemo        [i] = O_CS_MEMO         ;
    		 }
    		 
    		 Map<String, Object> modelDt = new HashMap<String, Object>();
    		 modelDt.put("I_SEQ"              , oseq           );
    		 modelDt.put("I_DLV_COM_ID"       , odlvcomid      );
    		 modelDt.put("I_SALES_COMPANY_ID" , osalescompanyid);
    		 modelDt.put("I_ORG_ORD_ID"       , oorgordid      );
    		 modelDt.put("I_FIRST_INS_DT"     , ofirstinsdt    );
    		 modelDt.put("I_CUSTOMER_NM"      , ocustomernm    );
    		 modelDt.put("I_PHONE_1"          , ophone1        );
    		 modelDt.put("I_PHONE_2"          , ophone2        );
    		 modelDt.put("I_ZIP"              , ozip           );
    		 modelDt.put("I_ADDR"             , oaddr          );
    		 modelDt.put("I_PRODUCT"          , oproduct       );
    		 modelDt.put("I_QTY"              , oqty           );
    		 modelDt.put("I_BUYED_DT"         , obuyeddt       );
    		 modelDt.put("I_BUY_CUST_NM"      , obuycustnm     );
    		 modelDt.put("I_BUY_PHONE_1"      , obuyphone1     );
    		 modelDt.put("I_BUY_PHONE_2"      , obuyphone2     );
    		 modelDt.put("I_CS_REG_CONTENTS"  , ocsregcontents );
    		 modelDt.put("I_CS_MEMO"          , ocsmemo        );
    		 modelDt.put("I_CS_COMPANY"    	  , model.get(ConstantIF.SS_CMPY_NAME));
    		 modelDt.put("I_TRUST_CUST_ID"    , model.get("vrTrustCustId"));
    		 modelDt.put("I_TRUST_CUST_CD"    , model.get("vrTrustCustCd"));
    		 modelDt.put("I_UPLOAD_ID"   	  , model.get("uploadId"));
    		 modelDt.put("I_LC_ID"    		  , model.get(ConstantIF.SS_SVC_NO));
    		 modelDt.put("I_USER_NO"   		  , model.get(ConstantIF.SS_USER_NO));
    		 
    		 modelDt = (Map<String, Object>)dao000.spCsCustInfoInsert(modelDt);
    		 ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
    		 m.put("O_CUR", modelDt.get("O_CUR"));       
    		 
    		 if(modelDt.get("O_MSG_CODE").equals("2")) {
    			 m.put("MSG", MessageResolver.getMessage("save.error"));
    			 m.put("MSG_ORA", "");
    			 m.put("errCnt", modelDt.get("O_MSG_CODE"));
    		 }else{
    			 m.put("MSG", MessageResolver.getMessage("save.success"));
    			 m.put("MSG_ORA", "");
    			 m.put("errCnt", modelDt.get("O_MSG_CODE"));
    		 }
    	 } catch(Exception e){
			 m.put("MSG", e.getMessage());
			 m.put("MSG_ORA", "");
			 m.put("errCnt", 2);
    		 throw e;
    	 }
    	 return m;
     }

     /**
      * 
      * 대체 Method ID   : spInsertKeyIn_AS
      * 대체 Method 설명    : AS주문일반업로드  Package
      * 작성자                      : sing09
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> spInsertKeyIn_AS(Map<String, Object> model, String[] cellName) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         int insHead = cellName.length;
         int insBody = Integer.parseInt(model.get("selectIds").toString()) ;
         try{
     		if(insBody < 1){
     			m.put("MSG", MessageResolver.getMessage("save.error"));
     			m.put("MSG_ORA", "데이터가 없습니다.");
     			m.put("errCnt", 1);
     			return m;
     		}

     		String[] oseq              = new String[insBody];
     		String[] odlvcomid         = new String[insBody];
     		String[] osalescompanyid   = new String[insBody];
     		String[] oorgordid         = new String[insBody];
     		String[] ocustomernm       = new String[insBody];
     		String[] ophone1           = new String[insBody];
     		String[] ophone2           = new String[insBody];
     		String[] ozip              = new String[insBody];
     		String[] oaddr             = new String[insBody];
     		String[] oproduct          = new String[insBody];
     		String[] oqty              = new String[insBody];
     		String[] odlvsetdt         = new String[insBody];
     		String[] ofirstinsdt       = new String[insBody];
     		String[] opayreqyn         = new String[insBody];
     		String[] oreqcost          = new String[insBody];
     		String[] oetc1             = new String[insBody];
     		String[] omemo             = new String[insBody];
     		String[] obuyeddt          = new String[insBody];
     		String[] oascostmemo       = new String[insBody];
     		String[] oasetc1           = new String[insBody];
     		String[] oasetc2           = new String[insBody];
     		
             for(int i = 0 ; i < insBody ; i ++){
     			
     			String O_SEQ               = "" ;
     			String O_DLV_COM_ID        = "" ;
     			String O_SALES_COMPANY_ID  = "" ;
     			String O_ORG_ORD_ID        = "" ;
     			String O_CUSTOMER_NM       = "" ;
     			String O_PHONE_1           = "" ;
     			String O_PHONE_2           = "" ;
     			String O_ZIP               = "" ;
     			String O_ADDR              = "" ;
     			String O_PRODUCT           = "" ;
     			String O_QTY               = "" ;
     			String O_DLV_SET_DT        = "" ;
     			String O_FIRST_INS_DT      = "" ;
     			String O_PAY_REQ_YN        = "" ;
     			String O_REQ_COST          = "" ;
     			String O_ETC1              = "" ;
     			String O_MEMO              = "" ;
     			String O_BUYED_DT          = "" ;
     			String O_AS_COST_MEMO      = "" ;
     			String O_AS_ETC1           = "" ;
     			String O_AS_ETC2           = "" ;
     			
                 for(int k = 0 ; k < insHead; k ++){
     				String bindColNm = String.valueOf(cellName[k]);
        			switch (bindColNm) {
		    			case "O_SEQ"               : O_SEQ               = (String)model.get(bindColNm+i); break;
		    			case "O_DLV_COM_ID"        : O_DLV_COM_ID        = (String)model.get(bindColNm+i); break;
		    			case "O_SALES_COMPANY_ID"  : O_SALES_COMPANY_ID  = (String)model.get(bindColNm+i); break;
		    			case "O_ORG_ORD_ID"        : O_ORG_ORD_ID        = (String)model.get(bindColNm+i); break;
		    			case "O_CUSTOMER_NM"       : O_CUSTOMER_NM       = (String)model.get(bindColNm+i); break;
		    			case "O_PHONE_1"           : O_PHONE_1           = (String)model.get(bindColNm+i); break;
		    			case "O_PHONE_2"           : O_PHONE_2           = (String)model.get(bindColNm+i); break;
		    			case "O_ZIP"               : O_ZIP               = (String)model.get(bindColNm+i); break;
		    			case "O_ADDR"              : O_ADDR              = (String)model.get(bindColNm+i); break;
		    			case "O_PRODUCT"           : O_PRODUCT           = (String)model.get(bindColNm+i); break;
		    			case "O_QTY"               : O_QTY               = (String)model.get(bindColNm+i); break;
		    			case "O_DLV_SET_DT"        : O_DLV_SET_DT        = (String)model.get(bindColNm+i); break;
		    			case "O_FIRST_INS_DT"      : O_FIRST_INS_DT      = (String)model.get(bindColNm+i); break;
		    			case "O_PAY_REQ_YN"        : O_PAY_REQ_YN        = (String)model.get(bindColNm+i); break;
		    			case "O_REQ_COST"          : O_REQ_COST          = (String)model.get(bindColNm+i); break;
		    			case "O_ETC1"              : O_ETC1              = (String)model.get(bindColNm+i); break;
		    			case "O_MEMO"              : O_MEMO              = (String)model.get(bindColNm+i); break;
		    			case "O_BUYED_DT"          : O_BUYED_DT          = (String)model.get(bindColNm+i); break;
		    			case "O_AS_COST_MEMO"      : O_AS_COST_MEMO      = (String)model.get(bindColNm+i); break;
		    			case "O_AS_ETC1"           : O_AS_ETC1           = (String)model.get(bindColNm+i); break;
		    			case "O_AS_ETC2"           : O_AS_ETC2           = (String)model.get(bindColNm+i); break;
    				}
                 }
     			
     			oseq              [i] = O_SEQ             ;
     			odlvcomid         [i] = O_DLV_COM_ID      ;
     			osalescompanyid   [i] = O_SALES_COMPANY_ID;
     			oorgordid         [i] = O_ORG_ORD_ID      ;
     			ocustomernm       [i] = O_CUSTOMER_NM     ;
     			ophone1           [i] = O_PHONE_1         ;
     			ophone2           [i] = O_PHONE_2         ;
     			ozip              [i] = O_ZIP             ;
     			oaddr             [i] = O_ADDR            ;
     			oproduct          [i] = O_PRODUCT         ;
     			oqty              [i] = O_QTY             ;
     			odlvsetdt         [i] = O_DLV_SET_DT      ;
     			ofirstinsdt       [i] = O_FIRST_INS_DT    ;
     			opayreqyn         [i] = O_PAY_REQ_YN      ;
     			oreqcost          [i] = O_REQ_COST        ;
     			oetc1             [i] = O_ETC1            ;
     			omemo             [i] = O_MEMO            ;
     			obuyeddt          [i] = O_BUYED_DT        ;
     			oascostmemo       [i] = O_AS_COST_MEMO    ;
     			oasetc1           [i] = O_AS_ETC1         ;
     			oasetc2           [i] = O_AS_ETC2         ;
             }
     		
 			Map<String, Object> modelDt = new HashMap<String, Object>();
 			modelDt.put("I_SEQ"              , oseq            );
 			modelDt.put("I_DLV_COM_ID"       , odlvcomid       );
 			modelDt.put("I_SALES_COMPANY_ID" , osalescompanyid );
 			modelDt.put("I_ORG_ORD_ID"       , oorgordid       );
 			modelDt.put("I_CUSTOMER_NM"      , ocustomernm     );
 			modelDt.put("I_PHONE_1"          , ophone1         );
 			modelDt.put("I_PHONE_2"          , ophone2         );
 			modelDt.put("I_ZIP"              , ozip            );
 			modelDt.put("I_ADDR"             , oaddr           );
 			modelDt.put("I_PRODUCT"          , oproduct        );
 			modelDt.put("I_QTY"              , oqty            );
 			modelDt.put("I_DLV_SET_DT"       , odlvsetdt       );
 			modelDt.put("I_FIRST_INS_DT"     , ofirstinsdt     );
 			modelDt.put("I_PAY_REQ_YN"       , opayreqyn       );
 			modelDt.put("I_REQ_COST"         , oreqcost        );
 			modelDt.put("I_ETC1"             , oetc1           );
 			modelDt.put("I_MEMO"             , omemo           );
 			modelDt.put("I_BUYED_DT"         , obuyeddt        );
 			modelDt.put("I_AS_COST_MEMO"     , oascostmemo     );
 			modelDt.put("I_AS_ETC1"          , oasetc1         );
 			modelDt.put("I_AS_ETC2"          , oasetc2         );
 			modelDt.put("I_TRUST_CUST_ID"    , model.get("vrTrustCustId"));
 			modelDt.put("I_TRUST_CUST_CD"    , model.get("vrTrustCustCd"));
 			modelDt.put("I_UPLOAD_ID"   	 , model.get("uploadId"));
 			modelDt.put("I_LC_ID"    		 , model.get(ConstantIF.SS_SVC_NO));
 			modelDt.put("I_USER_NO"   		 , model.get(ConstantIF.SS_USER_NO));
     		
     		modelDt = (Map<String, Object>)dao000.spAsCustInfoInsert(modelDt);
     		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
     		m.put("O_CUR", modelDt.get("O_CUR"));       
     		
     		if(modelDt.get("O_MSG_CODE").equals("2")) {
     			m.put("MSG", MessageResolver.getMessage("save.error"));
     			m.put("MSG_ORA", "");
     			m.put("errCnt", modelDt.get("O_MSG_CODE"));
     		}else{
     			m.put("MSG", MessageResolver.getMessage("save.success"));
     			m.put("MSG_ORA", "");
     			m.put("errCnt", modelDt.get("O_MSG_CODE"));
     		}
         } catch(Exception e){
             throw e;
         }
         return m;
     }
     
     /**
      * Method ID : spInsertExcel_AS
      * Method 설명 : AS주문 엑셀 업로드 Package
      * 작성자 : sing09
      * @param model
      * @return
      * @throws Exception
      */
     public Map<String, Object> spInsertExcel_AS(Map<String, Object> model, List list, String[] cellName) throws Exception {
     	Map<String, Object> m = new HashMap<String, Object>();
     	List<Map<String, Object>> listBody = list;
        int insBody = (listBody != null)?listBody.size():0;
        int insHead = cellName.length;
     	try{
     		if(insBody < 1){
	         	m.put("MSG", MessageResolver.getMessage("save.error"));
	            m.put("MSG_ORA", "데이터가 없습니다.");
	            m.put("errCnt", 1);
	            return m;
     		}
     		
     		String[] oseq              = new String[insBody];
     		String[] odlvcomid         = new String[insBody];
     		String[] osalescompanyid   = new String[insBody];
     		String[] oorgordid         = new String[insBody];
     		String[] ocustomernm       = new String[insBody];
     		String[] ophone1           = new String[insBody];
     		String[] ophone2           = new String[insBody];
     		String[] ozip              = new String[insBody];
     		String[] oaddr             = new String[insBody];
     		String[] oproduct          = new String[insBody];
     		String[] oqty              = new String[insBody];
     		String[] odlvsetdt         = new String[insBody];
     		String[] ofirstinsdt       = new String[insBody];
     		String[] opayreqyn         = new String[insBody];
     		String[] oreqcost          = new String[insBody];
     		String[] oetc1             = new String[insBody];
     		String[] omemo             = new String[insBody];
     		String[] obuyeddt          = new String[insBody];
     		String[] oascostmemo       = new String[insBody];
     		String[] oasetc1           = new String[insBody];
     		String[] oasetc2           = new String[insBody];
            
    		for(int i=0; i<insBody; i++){
    			
    			String O_SEQ               = "" ;
    			String O_DLV_COM_ID        = "" ;
    			String O_SALES_COMPANY_ID  = "" ;
    			String O_ORG_ORD_ID        = "" ;
    			String O_CUSTOMER_NM       = "" ;
    			String O_PHONE_1           = "" ;
    			String O_PHONE_2           = "" ;
    			String O_ZIP               = "" ;
    			String O_ADDR              = "" ;
    			String O_PRODUCT           = "" ;
    			String O_QTY               = "" ;
    			String O_DLV_SET_DT        = "" ;
    			String O_FIRST_INS_DT      = "" ;
    			String O_PAY_REQ_YN        = "" ;
    			String O_REQ_COST          = "" ;
    			String O_ETC1              = "" ;
    			String O_MEMO              = "" ;
    			String O_BUYED_DT          = "" ;
    			String O_AS_COST_MEMO      = "" ;
    			String O_AS_ETC1           = "" ;
    			String O_AS_ETC2           = "" ;
    			
    			for(int j=0; j<insHead; j++){
        			Map<String, Object> object = listBody.get(i);
        			String bindColNm = String.valueOf(cellName[j]);
        			switch (bindColNm) {
	        			case "O_SEQ"               : O_SEQ               = (String)object.get(bindColNm); break;
	        			case "O_DLV_COM_ID"        : O_DLV_COM_ID        = (String)object.get(bindColNm); break;
	        			case "O_SALES_COMPANY_ID"  : O_SALES_COMPANY_ID  = (String)object.get(bindColNm); break;
	        			case "O_ORG_ORD_ID"        : O_ORG_ORD_ID        = (String)object.get(bindColNm); break;
	        			case "O_CUSTOMER_NM"       : O_CUSTOMER_NM       = (String)object.get(bindColNm); break;
	        			case "O_PHONE_1"           : O_PHONE_1           = (String)object.get(bindColNm); break;
	        			case "O_PHONE_2"           : O_PHONE_2           = (String)object.get(bindColNm); break;
	        			case "O_ZIP"               : O_ZIP               = (String)object.get(bindColNm); break;
	        			case "O_ADDR"              : O_ADDR              = (String)object.get(bindColNm); break;
	        			case "O_PRODUCT"           : O_PRODUCT           = (String)object.get(bindColNm); break;
	        			case "O_QTY"               : O_QTY               = (String)object.get(bindColNm); break;
	        			case "O_DLV_SET_DT"        : O_DLV_SET_DT        = (String)object.get(bindColNm); break;
	        			case "O_FIRST_INS_DT"      : O_FIRST_INS_DT      = (String)object.get(bindColNm); break;
	        			case "O_PAY_REQ_YN"        : O_PAY_REQ_YN        = (String)object.get(bindColNm); break;
	        			case "O_REQ_COST"          : O_REQ_COST          = (String)object.get(bindColNm); break;
	        			case "O_ETC1"              : O_ETC1              = (String)object.get(bindColNm); break;
	        			case "O_MEMO"              : O_MEMO              = (String)object.get(bindColNm); break;
	        			case "O_BUYED_DT"          : O_BUYED_DT          = (String)object.get(bindColNm); break;
	        			case "O_AS_COST_MEMO"      : O_AS_COST_MEMO      = (String)object.get(bindColNm); break;
	        			case "O_AS_ETC1"           : O_AS_ETC1           = (String)object.get(bindColNm); break;
	        			case "O_AS_ETC2"           : O_AS_ETC2           = (String)object.get(bindColNm); break;
        			}
    			}
    			
    			oseq              [i] = O_SEQ             ;
    			odlvcomid         [i] = O_DLV_COM_ID      ;
    			osalescompanyid   [i] = O_SALES_COMPANY_ID;
    			oorgordid         [i] = O_ORG_ORD_ID      ;
    			ocustomernm       [i] = O_CUSTOMER_NM     ;
    			ophone1           [i] = O_PHONE_1         ;
    			ophone2           [i] = O_PHONE_2         ;
    			ozip              [i] = O_ZIP             ;
    			oaddr             [i] = O_ADDR            ;
    			oproduct          [i] = O_PRODUCT         ;
    			oqty              [i] = O_QTY             ;
    			odlvsetdt         [i] = O_DLV_SET_DT      ;
    			ofirstinsdt       [i] = O_FIRST_INS_DT    ;
    			opayreqyn         [i] = O_PAY_REQ_YN      ;
    			oreqcost          [i] = O_REQ_COST        ;
    			oetc1             [i] = O_ETC1            ;
    			omemo             [i] = O_MEMO            ;
    			obuyeddt          [i] = O_BUYED_DT        ;
    			oascostmemo       [i] = O_AS_COST_MEMO    ;
    			oasetc1           [i] = O_AS_ETC1         ;
    			oasetc2           [i] = O_AS_ETC2         ;
    			
    		}
    		
			Map<String, Object> modelDt = new HashMap<String, Object>();
			modelDt.put("I_SEQ"              , oseq            );
			modelDt.put("I_DLV_COM_ID"       , odlvcomid       );
			modelDt.put("I_SALES_COMPANY_ID" , osalescompanyid );
			modelDt.put("I_ORG_ORD_ID"       , oorgordid       );
			modelDt.put("I_CUSTOMER_NM"      , ocustomernm     );
			modelDt.put("I_PHONE_1"          , ophone1         );
			modelDt.put("I_PHONE_2"          , ophone2         );
			modelDt.put("I_ZIP"              , ozip            );
			modelDt.put("I_ADDR"             , oaddr           );
			modelDt.put("I_PRODUCT"          , oproduct        );
			modelDt.put("I_QTY"              , oqty            );
			modelDt.put("I_DLV_SET_DT"       , odlvsetdt       );
			modelDt.put("I_FIRST_INS_DT"     , ofirstinsdt     );
			modelDt.put("I_PAY_REQ_YN"       , opayreqyn       );
			modelDt.put("I_REQ_COST"         , oreqcost        );
			modelDt.put("I_ETC1"             , oetc1           );
			modelDt.put("I_MEMO"             , omemo           );
			modelDt.put("I_BUYED_DT"         , obuyeddt        );
			modelDt.put("I_AS_COST_MEMO"     , oascostmemo     );
			modelDt.put("I_AS_ETC1"          , oasetc1         );
			modelDt.put("I_AS_ETC2"          , oasetc2         );
			modelDt.put("I_TRUST_CUST_ID"    , model.get("vrTrustCustId"));
			modelDt.put("I_TRUST_CUST_CD"    , model.get("vrTrustCustCd"));
			modelDt.put("I_UPLOAD_ID"   	 , model.get("uploadId"));
			modelDt.put("I_LC_ID"    		 , model.get(ConstantIF.SS_SVC_NO));
			modelDt.put("I_USER_NO"   		 , model.get(ConstantIF.SS_USER_NO));
     		
			modelDt = (Map<String, Object>)dao000.spAsCustInfoInsert(modelDt);
     		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));
     		m.put("O_CUR", modelDt.get("O_CUR"));       
     		
     		if(modelDt.get("O_MSG_CODE").equals("2")) {
 	            m.put("MSG", MessageResolver.getMessage("save.error"));
 	            m.put("MSG_ORA", "");
 	            m.put("errCnt", modelDt.get("O_MSG_CODE"));
     		}else{
 	            m.put("MSG", MessageResolver.getMessage("save.success"));
 	            m.put("MSG_ORA", "");
 	            m.put("errCnt", modelDt.get("O_MSG_CODE"));
     		}
     	} catch(Exception e){
			 m.put("MSG", e.getMessage());
			 m.put("MSG_ORA", "");
			 m.put("errCnt", 2);
     		throw e;
     	}
     	return m;
     }
}
