package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.components.headertoolbar.actions.MoveElementCommand;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsop.service.WMSOP135Service;
import com.logisall.winus.wmsop.vo.WMSOP135VO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;



@Service("WMSOP135Service")
public class WMSOP135ServiceImpl implements WMSOP135Service{

    protected Log log = LogFactory.getLog(this.getClass());
    
    
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service wmsif000service;

    
    @Resource(name = "WMSOP135Dao")
    private WMSOP135Dao dao;

    
    
    
	
	/**
	 * Method ID   : getOrdSeqInfo
	 * Method 설명    : 출고주문  상세 리스트
	 * 작성자               : yhku
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public WMSOP135VO getOrdSeqInfo(Map<String, Object> model) throws Exception {
	    Map<String, Object> map = new HashMap<String, Object>();
	   
	    
	    map.put("ORD_ID", (String)model.get("vrOrdId"));
		map.put("ORD_SEQ",(String) model.get("vrOrdSeq"));
		//session 및 등록정보
		map.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
		map.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		map.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		    
	    return dao.getOrdSeqInfo(map);
	}
	

    
	 /**
     * Method ID   : getOrdList
     * Method 설명    : 출고주문  상세 리스트
     * 작성자               : yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public List<WMSOP135VO> getOrdList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
       
        int tmpCnt = Integer.parseInt(model.get("selectIds").toString()); //selectIds

        if(tmpCnt > 0){           
            String[] ordIds   = new String[tmpCnt];
            
            for(int i = 0 ; i < tmpCnt ; i ++){
            	ordIds[i]    = (String)model.get("I_ORD_ID"+i);  
            }
            
            map.put("ORD_IDs", ordIds);
            //session 및 등록정보
            map.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
            map.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            map.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
           
        }
        
        //map.put("LIST",  dao.getOrdList(map));
        List<WMSOP135VO> list = dao.getOrdList(map);
        return list;
   
    }
   
    /**
    * Method ID   : getOrdSeqList
    * Method 설명    : 출고주문  상세 리스트
    * 작성자               : yhku
    * @param   model
    * @return
    * @throws  Exception
    */
    @Override
    public List<WMSOP135VO> getOrdSeqList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();

    	int tmpCnt = Integer.parseInt(model.get("selectIds").toString()); //selectIds
		
		List<Map<String,String>> twoParamsMapList = new ArrayList<Map<String,String>>();
		if(tmpCnt > 0){           
			String[] ordIds   = new String[tmpCnt];
			String[] ordSeqs   = new String[tmpCnt];
			
			for(int i = 0 ; i < tmpCnt ; i ++){
				ordIds[i]    = (String)model.get("I_ORD_ID"+i);  
				ordSeqs[i]    = (String)model.get("I_ORD_SEQ"+i);  
				
				HashMap<String, String> twoParamsMap= new HashMap<String, String>();
		        twoParamsMap.put("ORD_ID", ordIds[i]);
		        twoParamsMap.put("ORD_SEQ", ordSeqs[i]);
		        twoParamsMapList.add(twoParamsMap);
			}
			
			map.put("ORD_ID_SEQs", twoParamsMapList);
			//session 및 등록정보
			map.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
			map.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			map.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		
		}
		
	    List<WMSOP135VO> list = dao.getOrdSeqList(map);
	    return list;
    }
   
   
   /**
    * 
    * 대체 Method ID   : OrdSeqInfoTraceEAICall
    * 대체 Method 설명    : 
    * 작성자                      : yhku
    * @param model
    * @return
    * @throws Exception
    */
	 @Override
	 public Map<String, Object> OrdSeqInfoTraceEAICall(Map<String, Object> model, WMSOP135VO vo) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       Map<String, Object> map = new HashMap<String, Object>();
       try{
    	   
    	   if(vo != null){
				String original_WorkStat = "";
				String change_WorkStat = "";
				original_WorkStat = (String)model.get("vrWorkStat");//기존 작업상태 
				change_WorkStat = vo.getWorkStat();//바뀐 작업상태

				/*System.out.println("original_WorkStat : " + original_WorkStat);
				System.out.println("change_WorkStat : " + change_WorkStat);
				System.out.println("vo.getWorkStat(): " + vo.getWorkStat());*/
				
				
				if(!original_WorkStat.equals(change_WorkStat) ){ //작업상태가 변경됬을때만 처리. 
					
				
					JSONObject jsonObject = new JSONObject();
					JSONArray req_array = new JSONArray();
					JSONArray req_array_online = new JSONArray();
					JSONArray req_sj_array = new JSONArray();
					
					
					JSONObject data = new JSONObject();
					data.put("ORD_ID", vo.getOrdId());
					data.put("ORD_SEQ", vo.getOrdSeq());
					data.put("WORK_STAT", vo.getWorkStat()); // STS02 : 출고작업상태 (230 로케이션지정, 240 로케이션지정취소)
					//data.put("ORD_TYPE", vo.getOrdType());
					
					/*String ordSubtype = vo.getOrdSubtype();
					if(ordSubtype =="134" || ordSubtype =="143"){  //  온라인 :  143 택배배송(온라인) , 134 이동출고   
						req_array_online.add(data);
					}else{//  오프라인 : 30 정상출고 ,  141 일반/세대반입배송 , 142 택배배송(오프라인)
						req_array.add(data);
					}*/
				
					
					String orgOrdId = vo.getOrgOrdId();
					if(orgOrdId != null && orgOrdId !=""){
						String[] orgOrdIds = orgOrdId.split("-");
						String ordType= orgOrdIds[1];  //WMDCTO (SL : 온라인  / 그외, )
						String ordKcoo= orgOrdIds[2];  //WMKCOO (00001 : jde / 00002 : 상재 )
						
						if (ordKcoo != null && ordKcoo.equals("00002")){ 
							req_sj_array.add(data);
						}else{
							if(ordType != null && ordType.equals("SL")){  //  온라인 :  SL
								req_array_online.add(data);
							}else{
								req_array.add(data);
							}
						}
						
	    			}
			
				
					//eai call
					String cMethod		= "POST";
					String cUrl ="";
					/*String jsonString	= request.getParameter("formData");
					String data		= request.getParameter("data");
					model.put("jsonString"	, jsonString);*/
					model.put("cMethod"		, cMethod);
					String dataJson ="";
					
					
					//오프라인  trace_ADD(상품준비중/주문등록)  eai call
					if(req_array.size()>0){
				        jsonObject.put("input", req_array);
				        cUrl = "KCCHCC/SPC_KCC_TRACE/TRACE_STAUS_ADD/OFFLINE";
				        model.put("cUrl"		, cUrl);
				        dataJson = jsonObject.toJSONString();
				        model.put("data", dataJson);
				        
				        map = wmsif000service.crossDomainHttpWsKcc(model);
					}
		
					//온라인  trace_ADD(상품준비중/주문등록)  eai call
			        if(req_array_online.size()>0){
			        	jsonObject = new JSONObject();
			        	jsonObject.put("input", req_array_online);
			        	cUrl = "KCCHCC/SPC_KCC_TRACE/TRACE_STAUS_ADD/ONLINE";
			        	model.put("cUrl"		, cUrl);
			        	dataJson = jsonObject.toJSONString();
			        	model.put("data", dataJson);
			        	
			        	map = wmsif000service.crossDomainHttpWsKcc(model);
				        
					}
			        
			        
			        //오프라인 ( 상재  )  trace_ADD(상품준비중/주문등록)  eai call
					if(req_sj_array.size()>0){
				        jsonObject.put("input", req_sj_array);
				        cUrl = "KCCHCC/SPC_KCC_TRACE/TRACE_STAUS_ADD/SJ";
				        model.put("cUrl"		, cUrl);
				        dataJson = jsonObject.toJSONString();
				        model.put("data", dataJson);
				        
				        map = wmsif000service.crossDomainHttpWsKcc(model);
					}
				}
	        
			}
           
           m.put("errCnt", 0);
           m.put("MSG", MessageResolver.getMessage("save.success"));
           
       } catch(BizException be) {
           m.put("errCnt", -1);
           m.put("MSG", be.getMessage() );
           
       } catch(Exception e){
           throw e;
       }
       return m;
   }
	 
	 
	 

  /**
    * 
    * 대체 Method ID   : OrdSeqListTraceEAICall
    * 대체 Method 설명    : 
    * 작성자                      : yhku
    * @param model
    * @return
    * @throws Exception
    */
	 @Override
   public Map<String, Object> OrdSeqListTraceEAICall(Map<String, Object> model, List<WMSOP135VO> list) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       Map<String, Object> map = new HashMap<String, Object>();
       try{
    	   
			//List<WMSOP135VO> ordList = (List<WMSOP135VO>) map.get("LIST");
			JSONObject jsonObject = new JSONObject();
			JSONArray req_array = new JSONArray();
	        JSONArray req_array_online = new JSONArray();
	        JSONArray req_sj_array = new JSONArray();
	        
	        
			if(list.size() > 0){
				for(WMSOP135VO vo : list){
			        JSONObject data = new JSONObject();
			        data.put("ORD_ID", vo.getOrdId());
			        data.put("ORD_SEQ", vo.getOrdSeq());
			        data.put("WORK_STAT", vo.getWorkStat());
			        //data.put("ORD_TYPE", vo.getOrdType());
			        
			        
			        String orgOrdId = vo.getOrgOrdId();
					if(orgOrdId != null && orgOrdId !=""){
						String[] orgOrdIds = orgOrdId.split("-");
						String ordType= orgOrdIds[1];  //WMDCTO (SL : 온라인  / 그외, )
						String ordKcoo= orgOrdIds[2];  //WMKCOO (00001 : jde / 00002 : 상재 )
						
						if (ordKcoo != null && ordKcoo.equals("00002")){ 
							req_sj_array.add(data);
						}else{
							if(ordType != null && ordType.equals("SL")){  //  온라인 :  SL
								req_array_online.add(data);
							}else{
								req_array.add(data);
							}
						}
						
	    			}
				}
				
				//eai call
				String cMethod		= "POST";
				String cUrl ="";
				model.put("cMethod"		, cMethod);
				String dataJson ="";
				
				
				//오프라인  trace_ADD(상품준비중/주문등록)  eai call
				if(req_array.size()>0){
			        jsonObject.put("input", req_array);
			        cUrl = "KCCHCC/SPC_KCC_TRACE/TRACE_STAUS_ADD/OFFLINE";
			        model.put("cUrl"		, cUrl);
			        dataJson = jsonObject.toJSONString();
			        model.put("data", dataJson);
			        
			        map = wmsif000service.crossDomainHttpWsKcc(model);
				}


				//온라인  trace_ADD(상품준비중/주문등록)  eai call
		        if(req_array_online.size()>0){
		        	jsonObject = new JSONObject();
		        	jsonObject.put("input", req_array_online);
		        	cUrl = "KCCHCC/SPC_KCC_TRACE/TRACE_STAUS_ADD/ONLINE";
		        	model.put("cUrl"		, cUrl);
		        	dataJson = jsonObject.toJSONString();
		        	model.put("data", dataJson);
		        	
		        	map = wmsif000service.crossDomainHttpWsKcc(model);
			        
				}
		        
		        
		        //오프라인 ( 상재  )  trace_ADD(상품준비중/주문등록)  eai call
				if(req_sj_array.size()>0){
			        jsonObject.put("input", req_sj_array);
			        cUrl = "KCCHCC/SPC_KCC_TRACE/TRACE_STAUS_ADD/SJ";
			        model.put("cUrl"		, cUrl);
			        dataJson = jsonObject.toJSONString();
			        model.put("data", dataJson);
			        
			        map = wmsif000service.crossDomainHttpWsKcc(model);
				}
				
			}
	       
           m.put("errCnt", 0);
           m.put("MSG", MessageResolver.getMessage("save.success"));
           
       } catch(BizException be) {
           m.put("errCnt", -1);
           m.put("MSG", be.getMessage() );
           
       } catch(Exception e){
           throw e;
       }
       return m;
   }	 
	 
	 /**
	 * Method ID   : updateOrdDesc
	 * Method 설명    : 출고관리KCC 비고1, 비고2 저장
	 * 작성자               : Seongjun Kwon
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> updateOrdDesc(Map<String, Object> model) throws Exception {
	    Map<String, Object> map = new HashMap<String, Object>();
	    Map<String, Object> m = new HashMap<String, Object>();
	    try{
	    	String [] ordIds   = model.get("vrOrdIds").toString().split(",");
	    	String [] ordSeqs  = model.get("vrOrdSeqs").toString().split(",");
	    	String [] ordDescs = model.get("vrOrdDescs").toString().split(",");
	    	String [] etc2s    = model.get("vrEtc2s").toString().split(",");
	    	
	    	for(int i = 0; i < ordIds.length; ++i){
	    		map.put("vrOrdId", ordIds[i]);
	    		map.put("vrOrdSeq", ordSeqs[i]);
	    		map.put("vrOrdDesc", ordDescs[i]);
	    		map.put("vrEtc2", etc2s[i]);
	    		
	    		dao.updateOrdDesc(map);
	    	}
	    
	    	m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
	    }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
	    }
	    
	    return m; 
	}
	
	
 /**
	 * Method ID   : autoBestLocSaveMultiQtyList
	 * Method 설명    : 로케이션추천지정(다중주문일괄/주문번호) 재고 수량 파악
	 * 작성자               : yhku
	 * @param   model
	 * @return
	 * @throws  Exception
	 */
	@Override
	public Map<String, Object> autoBestLocSaveMultiQtyList(Map<String, Object> model) throws Exception {
	    Map<String, Object> mav = new HashMap<String, Object>();
       
    	try{
    		int tmpCnt = Integer.parseInt(model.get("selectIds").toString()); //selectIds
    		String ordSeqYn = model.get("ordSeqYn").toString(); //ordSeqYn
 	    	
 	    	if(tmpCnt > 0){           
 	    		 
 	    		//주문번호단위 
 	    		if(ordSeqYn !=null && ordSeqYn.equals("N")){
 	    			 String[] ordIds   = new String[tmpCnt];
 	    			 
 	    			 for(int i = 0 ; i < tmpCnt ; i ++){
 	 				 	ordIds[i]    = (String)model.get("I_ORD_ID"+i);  
 	 				 }
 	    			 model.put("ORD_IDs", ordIds);
 	    			 
 	    			
 	    	    //주문seq단위
 	    		}else{
 	    			 String ordId   = (String)model.get("I_ORD_ID0");  
 	    			 String[] ordSeqs   = new String[tmpCnt];
 	    			 
 	    			 for(int i = 0 ; i < tmpCnt ; i ++){
 	 					ordSeqs[i]    = (String)model.get("I_ORD_SEQ"+i);  
 	 				 }
 	 				 
 	    			 model.put("ORD_ID", ordId);
 	 				 model.put("ORD_SEQs", ordSeqs);
 	    		}
 				
 				 //session 및 등록정보
 				model.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
 				model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
 				model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
 	        }
	    
 	    	mav.put("LIST", dao.autoBestLocSaveMultiQtyList(model));
 	    	mav.put("errCnt", 0);
 	    	mav.put("MSG", MessageResolver.getMessage("save.success"));
	    }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	mav.put("errCnt", 1);
        	mav.put("MSG", MessageResolver.getMessage("save.error") );
	    }
        return mav;
        
	}
	
	  /**
     * 
     * Method ID   : listExcel
     * Method 설명    : 입출고현황 엑셀용조회
     * 작성자                      : yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        
        if(model.get("vrSrchOrdSubtype").equals("148")) {//B2C
            
            model.put("vrGubun1", "134"); //이동출고
            model.put("vrGubun2", "143"); //택배출고(온라인)
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(model.get("vrSrchOrdSubtype").equals("149")) {//B2P
            
            model.put("vrGubun1", "30"); //정상출고
            model.put("vrGubun2", "141"); //세대반입
            model.put("vrGubun3", "142"); //택배출고(오프라인)
            model.put("vrGubun4", "144"); //고객자차
            model.put("vrGubun5", "145"); //택배출고(착불)
            
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            String subtypeGubun3 = (String)model.get("vrGubun3");
            String subtypeGubun4 = (String)model.get("vrGubun4");
            String subtypeGubun5 = (String)model.get("vrGubun5");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2, subtypeGubun3, subtypeGubun4, subtypeGubun5};
            model.put("chekbox", chekbox);
            
        }else if(!model.get("vrSrchOrdSubtype").equals("")){
          	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
           	 String chekbox[] = {subtypeGubun3};
           	 
           	 model.put("chekbox", chekbox);
        }
        
        
        map.put("LIST", dao.listExcel(model));
        return map;
    }
    
    
    /**
     * 
     * 대체 Method ID   : deliveryComplete
     * 대체 Method 설명    : 배송완료(이동출고)
     * 작성자                      : yhku
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> deliveryComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
        	
        	
            if(tmpCnt > 0){       
                
                for(int i = 0 ; i < tmpCnt ; i ++){
					String dlvOrdId   =  (String)model.get("I_DLV_ORD_ID"+i);
					String outDt   = (String)model.get("I_OUT_DT"+i);
					String custId   =(String)model.get("I_CUST_ID"+i);

                	//System.out.println(" I_DLV_ORD_ID: " + dlvOrdId);
                	//System.out.println(" I_OUT_DT: " + outDt.replaceAll("-",""));
                	//System.out.println(" I_CUST_ID: " + custId);
                	
                	//프로시져에 보낼것들 다담는다
                    Map<String, Object> modelIns = new HashMap<String, Object>();
                    
                    modelIns.put("dlvOrdId" , dlvOrdId);
                    modelIns.put("outDt" , outDt.replaceAll("-",""));
                    modelIns.put("custId" , custId);

                    //session 및 등록정보
                    modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                    modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                    modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                    //dao                
                    modelIns = (Map<String, Object>)dao.deliveryComplete(modelIns);
                    ServiceUtil.isValidReturnCode("WMSOP135", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                }
              
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }	 
	
	
}
