package com.logisall.winus.wmsop.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP642Dao")
public class WMSOP642Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID	: listByDlvSummary
     * Method 설명	: 택배주문조회
     * 작성자			: chsong
     * @param model
     * @return
     */
    public List rawListByDlvSummary(Map<String, Object> model) {
    	List custs = new ArrayList(); 
    	if(model.get("chkOptionType1") == null || "01".equals(model.get("chkOptionType1"))){
    		custs = list("wmsop642.listByDlvSummary", model);
    		
    	}else if("02".equals(model.get("chkOptionType1"))){/*박스추천*/
    		custs = list("wmsop642.listByDlvSummary02", model);
    	}
    	return custs;
	}
	public Object listByDlvSummary(Map<String, Object> model) {
		List custs = rawListByDlvSummary(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
     * Method ID	: listByDlvSummaryDiv
     * Method 설명	: 택배주문조회
     * 작성자			: chsong
     * @param model
     * @return
     */
    public List rawListByDlvSummaryDiv(Map<String, Object> model) {
		List custs = list("wmsop641.listByDlvSummary", model);
    	return custs;
	}
	public Object listByDlvSummaryDiv(Map<String, Object> model) {
		List custs = rawListByDlvSummaryDiv(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
     * Method ID	: list2ByDlvHistory
     * Method 설명	: 택배이력엑셀등록조회
     * 작성자			: chsong
     * @param model
     * @return
     */
	public List rawList2ByDlvHistory(Map<String, Object> model) {
		List custs = list("wmsop642.list2ByDlvHistory", model);
    	return custs;
	}
    
	public Object list2ByDlvHistory(Map<String, Object> model) {
		List custs = rawList2ByDlvHistory(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
     * Method ID	: list3ByDlvHistory
     * Method 설명	: 택배이력엑셀등록조회
     * 작성자			: SUMMER HYUN
     * @param model
     * @return
     */
	public List rawList3ByDlvHistory(Map<String, Object> model) {
		List custs = list("wmsop642.list3ByDlvHistory", model);		
    	return custs;
	}
	
	public Object list3ByDlvHistory(Map<String, Object> model) {
		List custs = rawList3ByDlvHistory(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
    /**
     * Method ID	: getRarcelAPIInvoicIsYn
     * Method 설명 	: 택배접수 API 송장접수한 주문건인지 여부 확인 
     * @param model
     * @return
     */
   /* public String getRarcelAPIInvcNoIsYn(Map<String, Object> model){
        return (String) executeView("wmsop642.getRarcelAPIInvcNoIsYn", model);
    }*/
	
	
	/**
	 * Method ID : insertCsv Method 설명 : 대용량등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void insertCsv(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				String LC_ID = (String) model.get("strLcId");
				String CUST_ID = (String) model.get("strCustId");
				
				if (paramMap.get("ORD_ID") 		!= null && StringUtils.isNotEmpty((String) paramMap.get("ORD_ID"))
    			 && paramMap.get("DLV_COMP_CD") != null && StringUtils.isNotEmpty((String) paramMap.get("DLV_COMP_CD"))
    			 && paramMap.get("INVC_NO") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("INVC_NO"))
    			 && StringUtils.isNotEmpty(LC_ID)
    			 && StringUtils.isNotEmpty(CUST_ID)
    			){
					//String custId = (String) sqlMapClient.insert("wmsms010.insertCsv", paramMap);
					paramMap.put("LC_ID"	, LC_ID);
					paramMap.put("CUST_ID"	, CUST_ID);
					
					String invcIsYn = (String) executeView("wmsop642.getRarcelAPIInvcNoIsYn", paramMap);
					
					if(invcIsYn != null && !invcIsYn.equals("Y") && invcIsYn != "Y"){//택배연동 송장 접수건이 아니면 등록, yhku (2021-12-24) 추가 
						sqlMapClient.insert("wmsop642.insertCsv", paramMap);
					}
					
    			}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}

/**
	 * Method ID : dlvInfoInsert /insertDF110 
	 * Method 설명 : 택배이력등록 (출고관리 - 용인) 
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 */
	public Object dlvInfoInsert(Map<String, Object> model )throws Exception {
		return executeInsert("wmsop642.insertCsv", model);
	}
	
	
	/**
     * Method ID  : dlvDlvCompInfo
     * Method 설명  : 택배배송 송화인정보
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object dlvDlvCompInfo(Map<String, Object> model){
    	System.out.println("dao - in");
        return executeQueryForList("wmsop642.dlvDlvCompInfo", model);
    }
    
	
	/**
     * Method ID  : dlvSenderInfo
     * Method 설명  : 택배배송 송화인정보
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object dlvSenderInfo(Map<String, Object> model){
        return executeQueryForList("wmsop642.dlvSenderInfo", model);
    }
    
	/**
     * Method ID  : dlvSenderInfoDetail
     * Method 설명  : 택배배송 세분화 송화인정보 
     * 작성자             : dhkim
     * @param model
     * @return
     */
    public Object dlvSenderInfoDetail(Map<String, Object> model){
        return executeQueryForList("wmsop642.dlvSenderInfoDetail", model);
    }
    
    /**
	 * Method ID	: boxNoUpdate 
	 * Method 설명	: boxNoUpdate코드 수정 
	 * 작성자			: chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object boxNoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop642.boxNoUpdate", model);
	}
	
	/*-
     * Method ID : getOracleSysTimeStamp
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String getOracleSysTimeStamp(Map<String, Object> model) {
        return (String)executeView("wmsop642.getOracleSysTimeStamp", model);
    }
    
    /**
	 * Method ID : dlvPrintPoiNoUpdate 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
    public Object dlvPrintPoiNoUpdate(Map<String, Object> model){
        executeUpdate("wmsop642.pk_wmsdf010.sp_print_invc_update", model);
        return model;
    }
	
	public Object invcNoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop642.invcNoUpdate", model);
	}
	
	/**
	 * Method ID : invcNoInfoUpdate 
	 * Method 설명 : 택배송장업로드 수정 및 삭제
	 * 작성자 : dhkim
	 * 
	 * @param model
	 * @return
	 */
	public Object invcNoInfoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop642.invcNoInfoUpdate", model);
	}
	
	/**
	 * Method ID : invcNoInfoDelete 
	 * Method 설명 : 택배송장업로드 이력삭제
	 * 작성자 : dhkim
	 * 
	 * @param model
	 * @return
	 */
	public Object invcNoInfoDelete(Map<String, Object> model) {
		return executeUpdate("wmsop642.invcNoInfoDelete", model);
	}
	
	/**
	 * Method ID	: wmsdf110DelYnUpdate 
	 * Method 설명	: wmsdf110DelYnUpdate코드 수정 
	 * 작성자			: chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object wmsdf110DelYnUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop642.wmsdf110DelYnUpdate", model);
	}
	
	/**
     * Method ID	: getCustOrdDegree
     * Method 설명 	: 화주별 주문차수
     * @param model
     * @return
     */
    public Object getCustOrdDegree(Map<String, Object> model){
        return executeQueryForList("wmsop642.getCustOrdDegree", model);
    }
    

    public GenericResultSet listByDlvSummaryExcel(Map<String, Object> model) {
//		return executeQueryPageWq("wmsop642.listByDlvSummary", model);
		
		return executeQueryPageWq("wmsop642.listByDlvSummaryExcel", model);
		/*
		GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop030.pickingList", model));
    	return genericResultSet;
    	*/
	}
    
    /**
     * Method ID	: getCustInfo
     * Method 설명 	: 고객정보별 원주문번호 조회
     * @param model
     * @return
     */
    public Object getCustInfo(Map<String, Object> model){
        return executeQueryForList("wmsop642.getCustInfo", model);
    }
    
    /**
    * Method ID	: getCustInfoInvcInFo
    * Method 설명 	: 고객정보별 원주문번호 조회 OM, OP, DF 조인
    * @param model
    * @return
    */
   public Object getCustInfoInvcInFo(Map<String, Object> model){
       return executeQueryForList("wmsop642.getCustInfoInvcInFo", model);
   }
    
    /**
     * Method ID	: getCustInfoV2
     * Method 설명 	: 고객정보별 원주문번호 조회 om010. op010 조인
     * @param model
     * @return
     */
    public Object getCustInfoV2(Map<String, Object> model){
        return executeQueryForList("wmsop642.getCustInfoV2", model);
    }
    
    /**
	 * Method ID	: custInfoUpdate 
	 * Method 설명	: custInfoUpdate
	 * 작성자			: chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object custInfoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop642.custInfoUpdate", model);
	}
	
	/**
     * Method ID  : dlvInvcItemNmSetInfo
     * Method 설명  : 택배 송장별 상품명 명칭 정보
     * 작성자             : yhku
     * @param model
     * @return
     */
    public Object dlvInvcItemNmSetInfo(Map<String, Object> model){
        return executeQueryForList("wmsop642.dlvInvcItemNmSetInfo", model);
    }
    
    /**
     * Method ID  : rtnOrderInfo
     * Method 설명  : 택배 송장별 상품명 명칭 정보
     * 작성자             : yhku
     * @param model
     * @return
     */
    public List getRtnOrderInfo(Map<String, Object> model) {
        return executeQueryForList("wmsop642.getRtnOrderInfo", model);
    }
    
}
