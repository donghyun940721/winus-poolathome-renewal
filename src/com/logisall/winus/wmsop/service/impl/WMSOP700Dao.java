package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP700Dao")
public class WMSOP700Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 통합조회(입/출고) 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop700.list", model);
    }   
    /**
     * Method ID : list2
     * Method 설명 : 통합조회(재고) 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet list2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop700.list2", model);
    }   
    /**
     * Method ID : list3
     * Method 설명 : 통합조회(센터별재고) 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet list3(Map<String, Object> model) {
    	return executeQueryPageWq("wmsop700.list3", model);
    }   
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
}

