package com.logisall.winus.wmsop.service.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP100Dao")
public class WMSOP100Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop100.list", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 게시판 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsop100.insert_wmsei010", model);
    }
    
    /*-
     * Method ID : updateFileRow
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public int updateFileRow(Map<String, Object> model) {
        return executeUpdate("wmsop100.updateFileRow", model);
    }
    
    /*-
     * Method ID : updateFileRow_HD
     * Method 설명 : 
     * 작성자 : sing09
     *
     * @param model
     * @return
     */
    public int updateFileRow_HD(Map<String, Object> model) {
    	return executeUpdate("wmsop100.updateFileRow_HD", model);
    }
    
    /**
     * Method ID : insertExcelInfo
     * Method 설명 : 상품군 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertExcelInfo(Map<String, Object> model, List list) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			
			Map<String, Object> modelchk = new HashMap<String, Object>();
			modelchk.put("LC_ID", "NONE");
			modelchk.put("USER_NO", model.get("REG_NO"));
			String checkInsertPossible = (String)executeView("wmsop100.checkInsertPossible", modelchk);
	        
			//정상처리
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			paramMap.put("LC_ID"     	, model.get("LC_ID"));
    			paramMap.put("REG_NO"    	, model.get("REG_NO"));
    			paramMap.put("UPLOAD_ID" 	, model.get("uploadId"));
    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));
    			
    			//거래배송사가 KPP이고 주문데이터 중 주문일자가 당일 이전 데이터가 있으면 등록 제한
    			if(model.get("REG_NO").equals("0000017461") || model.get("REG_NO").equals("0000002460") || model.get("REG_NO").equals("0000017340")){
    				SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
    				Date now = new Date();
    			    Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
    			    Date date2 = nDate.parse(nDate.format(now));
    			    if(checkInsertPossible.equals("N")){
    			        throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
    			    }
    			    if(date1.before(date2)){
    			        throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
    			    }
    			}else{
    				if (paramMap.get("O_DLV_COM_ID").equals("KPP")){
        				SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
        				Date now = new Date();
        			    Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
        			    Date date2 = nDate.parse(nDate.format(now));
        			    if(checkInsertPossible.equals("N")){
        			        throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
        			    }
        			    if(date1.before(date2)){
        			        throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
        			    }
        			}
    			}
//    			if (paramMap.get("O_DLV_COM_ID").equals("KPP")){
//    				SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
//    				Date now = new Date();
//    			    Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
//    			    Date date2 = nDate.parse(nDate.format(now));
//    			    if(checkInsertPossible.equals("N")){
//    			        throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
//    			    }
//    			    if(date1.before(date2)){
//    			        throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
//    			    }
//    			}
    			
    			if (   paramMap.get("O_DLV_COM_ID")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
    			    && paramMap.get("O_BUYED_DT")     != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
    				if(paramMap.get("O_DLV_ORD_TYPE") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_TYPE"))){
    					//String existRsSet = (String)sqlMapClient.queryForObject("wmsop100.selectExistSalesOrdId", paramMap);
    					// 1.고객관리 입력 WMSCT010
            			String getSalesCustId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoSalesCust", paramMap);
            			// 2. WMSCT010 입력 후 채번 된 SALES_CUST_ID 참조
            			paramMap.put("RS_SALES_CUST_ID", getSalesCustId);
            			// 3. 배송관리 입력 WMSSP010
            			String getRstDlvOrdId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoDlvCust", paramMap);
            			paramMap.put("RS_DLV_ORD_ID", getRstDlvOrdId);
            			sqlMapClient.insert("wmsop100.updateExcelInfoSalesCust", paramMap);
            			
            			// AS출고 주문 일 경우 : 주문등록과 함께 AS내역 입력
            			if(paramMap.get("O_DLV_ORD_TYPE").equals("AS출고")){
            				paramMap.put("vrSalesCustId"			, getSalesCustId);
        					paramMap.put("vrSrchAsCsType"			, "AS");
        					paramMap.put("vrSrchRitemId"			, paramMap.get("O_PRODUCT"));
        					paramMap.put("vrSrchAsCsLc"				, model.get("LC_ID"));
        					paramMap.put("vrSrchBuyDt"				, paramMap.get("O_BUYED_DT"));
        					paramMap.put("vrSrchReqId"				, model.get("REG_NO"));
        					paramMap.put("TextReqContents"			, paramMap.get("O_ETC1"));
        					paramMap.put("AS_SALES_CUST_ID"			, getSalesCustId);
        					sqlMapClient.insert("wms01100.asUpload"	, paramMap);
            			}
        			}else{
        				throw new Exception("Exception:엑셀 데이터 중 등록구분이 없습니다. 등록오류.");
        			}
    			}else{
    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
    			}
    		}
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }
    
    /**
     * Method ID : insertExcelInfo
     * Method 설명 : 상품군 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertExcelInfoUpdate(Map<String, Object> model, List list) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			paramMap.put("LC_ID"     	, model.get("LC_ID"));
    			paramMap.put("REG_NO"    	, model.get("REG_NO"));
    			paramMap.put("UPLOAD_ID" 	, model.get("uploadId"));
    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));
    			if (   paramMap.get("O_DLV_COM_ID") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
    			    && paramMap.get("O_BUYED_DT")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
    				if(   paramMap.get("O_DLV_ORD_STAT") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_STAT"))
    				   && paramMap.get("O_DLV_ORD_TYPE") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_TYPE"))){
    					String existRsSet = (String)sqlMapClient.queryForObject("wmsop100.selectExistSalesOrdId", paramMap);
        				if(existRsSet == null && StringUtils.isEmpty(existRsSet)){
        					throw new Exception("Exception:등록된 주무번호가 없습니다.("+paramMap.get("O_ORG_ORD_ID")+") 등록오류.");
            			}else{
            				// WINUS에 동일한 고객사주문정보가 있는 데이터 일 경우 주문 업데이트 (기존주문 상태변경 작업)
        					// 등록구분이 [출고] 가 아닐경우, 과거데이터 업데이트 할 경우  : 고객사주문번호가 있고 배송상태가 배송완료가 아닐 경우만
            				// 특정 값만 업데이트 (아래 적요)
            				// 배송완료시 작동
            				
        					//existRsSet : SALES_CUST_ID||RST_DLV_ORD_ID
        					paramMap.put("RS_EXIST_SALES_CUST_ID", existRsSet.substring(0,10));
            				paramMap.put("RS_EXIST_DLV_ORD_ID"   , existRsSet.substring(10));

            				// 배송상태, 등록구분, 배송일자 업데이트
            				sqlMapClient.insert("wmsop100.updateSalesCust" , paramMap);
                			sqlMapClient.insert("wmsop100.updateDlvCust"   , paramMap);
            			}
        			}else{
        				throw new Exception("Exception:엑셀 데이터 중 배송상태 또는 등록구분 값이 없습니다. 등록오류.");
        			}
    			}else{
    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
    			}
    		}
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }
    
    /**
     * Method ID : deleteUpdateWmssp011_upload 
     * Method 설명 : 
     * 작성자 : sing09
     * 
     * @param model
     * @return
     */
    public Object deleteUpdateWmssp011_upload(Map<String, Object> model) {
    	return executeUpdate("wmsop100.deleteUpdateWmssp011_upload", model);
    }
    
    /**
     * Method ID : deleteUpdateWmssp011 
     * Method 설명 : 
     * 작성자 : sing09
     * 
     * @param model
     * @return
     */
    public Object deleteUpdateWmssp011(Map<String, Object> model) {
    	return executeUpdate("wmsop100.deleteUpdateWmssp011", model);
    }
    
    /**
	 * Method ID : deleteUpdateWmssp010 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteUpdateWmssp010(Map<String, Object> model) {
		return executeUpdate("wmsop100.deleteUpdateWmssp010", model);
	}
	
	/**
	 * Method ID : deleteUpdateWmsct010 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteUpdateWmsct010(Map<String, Object> model) {
		return executeUpdate("wmsop100.deleteUpdateWmsct010", model);
	}

	/**
	 * Method ID : deleteUpdateWmsas010 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteUpdateWmsas010(Map<String, Object> model) {
		return executeUpdate("wmsop100.deleteUpdateWmsas010", model);
	}
	
    /**
	 * Method ID : delete 
	 * Method 설명 : 고객관리 삭제 (DEL_YN 를 Y로 수정) 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmsop100.delete", model);
	}
	
	/**
     * Method ID : excelDownCustom
     * Method 설명 : Type별 주문정보 엑셀다운로드
     * @param model
     * @return
     */
    public GenericResultSet excelDownCustom(Map<String, Object> model) {
        return executeQueryPageWq("wmsop100.excelDownCustom", model);
    }
    
    /**
     * Method ID : insertExcelInfoMulti
     * Method 설명 : 배송주문등록 > 엑셀주무등록 다중
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertExcelInfoMulti(Map<String, Object> model, List list) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			
			Map<String, Object> modelchk = new HashMap<String, Object>();
			modelchk.put("LC_ID", "NONE");
			modelchk.put("USER_NO", model.get("REG_NO"));
			String checkInsertPossible = (String)executeView("wmsop100.checkInsertPossible", modelchk);
			
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			paramMap.put("LC_ID"     	, model.get("LC_ID"));
    			paramMap.put("REG_NO"    	, model.get("REG_NO"));
    			paramMap.put("UPLOAD_ID" 	, model.get("uploadId"));
    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));
    			
    			//거래배송사가 KPP이고 주문데이터 중 주문일자가 당일 이전 데이터가 있으면 등록 제한
    			if(model.get("REG_NO").equals("0000017461") || model.get("REG_NO").equals("0000002460") || model.get("REG_NO").equals("0000017340")){
    				SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
    				Date now = new Date();
    				Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
    				Date date2 = nDate.parse(nDate.format(now));
    				if(checkInsertPossible.equals("N")){
    					throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
    				}
    				if(date1.before(date2)){
    					throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
    				}
    			}else{
	    			if (paramMap.get("O_DLV_COM_ID").equals("KPP")){
	    				SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
	    				Date now = new Date();
	    			    Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
	    			    Date date2 = nDate.parse(nDate.format(now));
	    			    if(checkInsertPossible.equals("N")){
	    			        throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
	    			    }
	    			    if(date1.before(date2)){
	    			        throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
	    			    }
	    			}
    			}
    			
    			if (   paramMap.get("O_DLV_COM_ID")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
    			    && paramMap.get("O_BUYED_DT")     != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
    				if(paramMap.get("O_DLV_ORD_TYPE") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_TYPE"))){
    					
    					String existRsSet = (String)sqlMapClient.queryForObject("wmsop100.selectExistOrdId", paramMap);
    					
    					
    					if(existRsSet.equals("0")) {
    						// 1.고객관리 입력 WMSCT010
                			String getSalesCustId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoSalesCust", paramMap);
                			
                			// 2. WMSCT010 입력 후 채번 된 SALES_CUST_ID 참조
                			paramMap.put("RS_SALES_CUST_ID", getSalesCustId);
                			
                			// 3. 배송관리 입력 WMSSP010
//                			String getRstDlvOrdId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoDlvCust", paramMap);		//기존
                			String getRstDlvOrdId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoDlvCustMulti", paramMap);
                			paramMap.put("RS_DLV_ORD_ID", getRstDlvOrdId);
                			
                			sqlMapClient.insert("wmsop100.updateExcelInfoSalesCust", paramMap);
                			
                			// 4. 배송관리 디테일 입력 WMSSP011
                			sqlMapClient.insert("wmsop100.insertExcelInfoSp011", paramMap);
                			
                			// AS출고 주문 일 경우 : 주문등록과 함께 AS내역 입력
                			if(paramMap.get("O_DLV_ORD_TYPE").equals("AS출고")){
                				paramMap.put("vrSalesCustId"			, getSalesCustId);
            					paramMap.put("vrSrchAsCsType"			, "AS");
            					paramMap.put("vrSrchRitemId"			, paramMap.get("O_PRODUCT"));
            					paramMap.put("vrSrchAsCsLc"				, model.get("LC_ID"));
            					paramMap.put("vrSrchBuyDt"				, paramMap.get("O_BUYED_DT"));
            					paramMap.put("vrSrchReqId"				, model.get("REG_NO"));
            					paramMap.put("TextReqContents"			, paramMap.get("O_ETC1"));
            					paramMap.put("AS_SALES_CUST_ID"			, getSalesCustId);
            					sqlMapClient.insert("wms01100.asUpload"	, paramMap);
                			}
                			
    					}else{
    						String existDlvOrdId = (String)sqlMapClient.queryForObject("wmsop100.selectDlvOrdId", paramMap);
    						paramMap.put("DLV_ORD_ID", existDlvOrdId);
    						// 4. 배송관리 디테일 입력 WMSSP011
                			sqlMapClient.insert("wmsop100.insertExcelInfoSp011", paramMap);
    					}
    				
            			
        			}else{
        				throw new Exception("Exception:엑셀 데이터 중 등록구분이 없습니다. 등록오류.");
        			}
    			}else{
    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
    			}
    		}
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }
    
    /**
     * Method ID : insertExcelInfo_HD
     * Method 설명 : 배송주문등록 > 일반/엑셀주문 HD
     * 작성자 : sing09
     * @param model
     * @return
     */
    public int insertExcelInfo_HD(Map<String, Object> model, List list) throws Exception {
    	SqlMapClient sqlMapClient = getSqlMapClient();
    	int returnCnt = 0;
    	try {
    		sqlMapClient.startTransaction();
    		Map<String, Object> paramMap = null;
    		
    		Map<String, Object> modelchk = new HashMap<String, Object>();
    		modelchk.put("LC_ID", "NONE");
    		modelchk.put("USER_NO", model.get("REG_NO"));
    		String checkInsertPossible = (String)executeView("wmsop100.checkInsertPossible", modelchk);
			
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			paramMap.put("LC_ID"     	, model.get("LC_ID"));
    			paramMap.put("REG_NO"    	, model.get("REG_NO"));
    			paramMap.put("UPLOAD_ID" 	, model.get("uploadId"));
    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));
    			
    			//거래배송사 정상 체크 
    			String stCheckDlvCompId = checkDlvCompId(paramMap);
    			if(stCheckDlvCompId.equals("N")){
					throw new Exception("Exception:주문데이터 중 거래배송사가 잘못 기입된 주문이 있습니다. [수취인 : "+paramMap.get("O_CUSTOMER_NM")+"]");
				}
    			
    			//거래배송사가 KPP이고 주문데이터 중 주문일자가 당일 이전 데이터가 있으면 등록 제한
    			//사용자 박미소프로 일땐 예외 X 등록가능 O
    			if(model.get("REG_NO").equals("0000017461") || model.get("REG_NO").equals("0000002460") || model.get("REG_NO").equals("0000017340") || model.get("REG_NO").equals("0000000001")){
    				SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
    				Date now = new Date();
    				Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
    				Date date2 = nDate.parse(nDate.format(now));
    				if(checkInsertPossible.equals("N")){
    					throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
    				}
    				if(date1.before(date2)){
    					throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
    				}
    			}else{
    				if (paramMap.get("O_DLV_COM_ID").equals("KPP")){
    					SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
    					Date now = new Date();
    					Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
    					Date date2 = nDate.parse(nDate.format(now));
    					if(checkInsertPossible.equals("N")){
    						throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
    					}
    					if(date1.before(date2)){
    						throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
    					}
    				}
    			}
    			
    			//데이터 등록 START 
    			if (   paramMap.get("O_DLV_COM_ID")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
    					&& paramMap.get("O_BUYED_DT")     != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
    				if(paramMap.get("O_DLV_ORD_TYPE") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_TYPE"))){
    					
    					//원주문번호 중복체크
    					String existRsSet = (String)sqlMapClient.queryForObject("wmsop100.selectExistOrdId_HD", paramMap);
    					
    					if(existRsSet.equals("0")) {
    						returnCnt++;
    						String getSalesCustId = "";
    						// 1.고객관리 입력 WMSCT010
    						if("0000003780".equals(model.get("LC_ID")) || "0000003820".equals(model.get("LC_ID"))){
    							getSalesCustId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoSalesCust_ZOIT", paramMap);
    						}else{
    							getSalesCustId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoSalesCust", paramMap);
    						}
    						// 2. WMSCT010 입력 후 채번 된 SALES_CUST_ID 참조
    						paramMap.put("RS_SALES_CUST_ID", getSalesCustId);
    						// 3. 배송관리 입력 WMSSP010
    						String getRstDlvOrdId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoDlvCustMulti_HD", paramMap);
    						paramMap.put("RS_DLV_ORD_ID", getRstDlvOrdId);
    						sqlMapClient.insert("wmsop100.updateExcelInfoSalesCust", paramMap);
    						// 4. 배송관리 디테일 입력 WMSSP011 아이템코드 체크
    						sqlMapClient.insert("wmsop100.insertExcelInfoSp015", paramMap);
    						
    						for(int j = 0; j<8; j++){
    							if(((String) paramMap.get("O_PRODUCT_D"+j)).length() != 0){
    								paramMap.put("O_PRODUCT", paramMap.get("O_PRODUCT_D"+j));
    								paramMap.put("O_QTY", paramMap.get("O_QTY_D"+j));
    								String itemCodeCK = (String)sqlMapClient.queryForObject("wmsop100.CheckItem", paramMap);
    								if("Y".equals(itemCodeCK)){
    									returnCnt++;
    									sqlMapClient.insert("wmsop100.insertExcelInfoSp015", paramMap);
    								}else{
    									throw new Exception("Exception:등록되지 않은 상품코드입니다. 상품코드  : " + paramMap.get("O_PRODUCT_D"+j));
    								}
    							}
    						}
    						// AS출고 주문 일 경우 : 주문등록과 함께 AS내역 입력
    						if(paramMap.get("O_DLV_ORD_TYPE").equals("AS출고")){
    							paramMap.put("vrSalesCustId"			, getSalesCustId);
    							paramMap.put("vrSrchAsCsType"			, "AS");
    							paramMap.put("vrSrchRitemId"			, paramMap.get("O_PRODUCT"));
    							paramMap.put("vrSrchAsCsLc"				, model.get("LC_ID"));
    							paramMap.put("vrSrchBuyDt"				, paramMap.get("O_BUYED_DT"));
    							paramMap.put("vrSrchReqId"				, model.get("REG_NO"));
    							paramMap.put("TextReqContents"			, paramMap.get("O_ETC1"));
    							paramMap.put("AS_SALES_CUST_ID"			, getSalesCustId);
    							sqlMapClient.insert("wms01100.asUpload"	, paramMap);
    						}
    					}else{
    						String existDlvOrdId = (String)sqlMapClient.queryForObject("wmsop100.selectDlvOrdId", paramMap);
    						paramMap.put("DLV_ORD_ID", existDlvOrdId);
    						returnCnt++;
    						sqlMapClient.insert("wmsop100.insertExcelInfoSp015", paramMap);
    						
    						for(int j = 0; j<8; j++){
    							if(((String) paramMap.get("O_PRODUCT_D"+j)).length() != 0){
    								paramMap.put("O_PRODUCT", paramMap.get("O_PRODUCT_D"+j));
    								paramMap.put("O_QTY", paramMap.get("O_QTY_D"+j));
    								String itemCodeCK = (String)sqlMapClient.queryForObject("wmsop100.CheckItem", paramMap);
    								if("Y".equals(itemCodeCK)){
    									returnCnt++;
    									sqlMapClient.insert("wmsop100.insertExcelInfoSp015", paramMap);
    								}else{
    									throw new Exception("Exception:등록되지 않은 상품코드입니다. 상품코드  : " + paramMap.get("O_PRODUCT_D"+j));
    								}
    							}
    						}
    					}
    				}else{
    					throw new Exception("Exception:엑셀 데이터 중 등록구분이 없습니다. 등록오류.");
    				}
    			}else{
    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
    			}
    		}
    		sqlMapClient.endTransaction();
    	} catch(Exception e) {
    		e.printStackTrace();
    		throw e;
    		
    	} finally {
    		if (sqlMapClient != null) {
    			sqlMapClient.endTransaction();
    		}
    	}
		return returnCnt;
    }
    
    /**
     * Method ID : insertExcelInfo_CT
     * Method 설명 : 배송주문등록 > 고객정보업로드
     * 작성자 : sing09
     * @param model
     * @return
     */
    public void insertExcelInfo_CT(Map<String, Object> model, List list) throws Exception {
        SqlMapClient sqlMapClient = getSqlMapClient();
        try {
            sqlMapClient.startTransaction();
            Map<String, Object> paramMap = null;
			
            for (int i=0;i<list.size();i++) {
                paramMap = (Map)list.get(i);
                paramMap.put("LC_ID"        , model.get("LC_ID"));
                paramMap.put("REG_NO"       , model.get("REG_NO"));
                paramMap.put("UPLOAD_ID"    , model.get("uploadId"));
                paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
                paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));

    			//거래배송사 정상 체크 
    			String stCheckDlvCompId = checkDlvCompId(paramMap);
    			if(stCheckDlvCompId.equals("N")){
					throw new Exception("Exception:주문데이터 중 거래배송사가 잘못 기입된 주문이 있습니다. [수취인 : "+paramMap.get("O_CUSTOMER_NM")+"]");
				}
    			
                //거래배송사가 KPP이고 주문데이터 중 주문일자가 당일 이전 데이터가 있으면 등록 제한
                if(model.get("REG_NO").equals("0000017461") || model.get("REG_NO").equals("0000002460") || model.get("REG_NO").equals("0000017340")){
                    SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
                    Date now = new Date();
                    Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
                    Date date2 = nDate.parse(nDate.format(now));
                    //if(checkInsertPossible.equals("N")){
                    //    throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
                    //}
                    if(date1.before(date2)){
                        throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
                    }
                }else{
                    if (paramMap.get("O_DLV_COM_ID").equals("KPP")){
                        SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
                        Date now = new Date();
                        Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
                        Date date2 = nDate.parse(nDate.format(now));
                        //if(checkInsertPossible.equals("N")){
                        //   throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
                        //}
                        if(date1.before(date2)){
                            throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
                        }
                    }
                }
                
                //WMSCT010 실제 업로드 로직
                if (   paramMap.get("O_DLV_COM_ID")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
                    && paramMap.get("O_BUYED_DT")     != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
                    String existRsSet = (String)sqlMapClient.queryForObject("wmsop100.selectExistOrdId", paramMap);
                    if(existRsSet.equals("0")) {
                        // 1.고객관리 입력 WMSCT010
                        sqlMapClient.insert("wmsop100.insertExcelInfoSalesCust_CT", paramMap);
                    }
                }else{
                    throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
                }
            }
            sqlMapClient.endTransaction();
        } catch(Exception e) {
            e.printStackTrace();
            throw e;
            
        } finally {
            if (sqlMapClient != null) {
                sqlMapClient.endTransaction();
            }
        }
    }

    /**
     * Method ID : insertExcelInfo_AS
     * Method 설명 : AS주문등록
     * 작성자 : sing09
     * @param model
     * @return
     */
    public void insertExcelInfo_AS(Map<String, Object> model, List list) throws Exception {
    	SqlMapClient sqlMapClient = getSqlMapClient();
    	try {
    		sqlMapClient.startTransaction();
    		Map<String, Object> paramMap = null;
    		
    		Map<String, Object> modelchk = new HashMap<String, Object>();
    		modelchk.put("LC_ID", "NONE");
    		modelchk.put("USER_NO", model.get("REG_NO"));
    		String checkInsertPossible = (String)executeView("wmsop100.checkInsertPossible", modelchk);
			
    		//정상처리
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			paramMap.put("LC_ID"     	, model.get("LC_ID"));
    			paramMap.put("REG_NO"    	, model.get("REG_NO"));
    			paramMap.put("UPLOAD_ID" 	, model.get("uploadId"));
    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));

    			//거래배송사 정상 체크 
    			String stCheckDlvCompId = checkDlvCompId(paramMap);
    			if(stCheckDlvCompId.equals("N")){
					throw new Exception("Exception:주문데이터 중 거래배송사가 잘못 기입된 주문이 있습니다. [수취인 : "+paramMap.get("O_CUSTOMER_NM")+"]");
				}
    			// 등록구분 AS고정
    			paramMap.put("O_DLV_ORD_TYPE", "AS출고");
    			
    			//거래배송사가 KPP이고 주문데이터 중 주문일자가 당일 이전 데이터가 있으면 등록 제한
    			if(model.get("REG_NO").equals("0000017461") || model.get("REG_NO").equals("0000002460") || model.get("REG_NO").equals("0000017340")){
    				SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
    				Date now = new Date();
    				Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
    				Date date2 = nDate.parse(nDate.format(now));
    				if(checkInsertPossible.equals("N")){
    					throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
    				}
    				if(date1.before(date2)){
    					throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
    				}
    			}else{
    				if (paramMap.get("O_DLV_COM_ID").equals("KPP")){
    					SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
    					Date now = new Date();
    					Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
    					Date date2 = nDate.parse(nDate.format(now));
    					if(checkInsertPossible.equals("N")){
    						throw new Exception("Exception:주문데이터 중 거래배송사가 [KPP(풀앳홈)] 인 경우  등록시간은 00:00 ~ 16:00으로 제한됩니다.");
    					}
    					if(date1.before(date2)){
    						throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
    					}
    				}
    			}
    			
    			if (   paramMap.get("O_DLV_COM_ID")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
    					&& paramMap.get("O_BUYED_DT")     != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
    				if(paramMap.get("O_DLV_ORD_TYPE") != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_ORD_TYPE"))){
    					// 1.고객관리 입력 WMSCT010
    					String getSalesCustId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoSalesCust", paramMap);
    					// 2. WMSCT010 입력 후 채번 된 SALES_CUST_ID 참조
    					paramMap.put("RS_SALES_CUST_ID", getSalesCustId);
    					// 3. 배송관리 입력 WMSSP010
    					String getRstDlvOrdId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoDlvCust", paramMap);
    					paramMap.put("RS_DLV_ORD_ID", getRstDlvOrdId);
    					sqlMapClient.insert("wmsop100.updateExcelInfoSalesCust", paramMap);
    					
    					// AS출고 주문 일 경우 : 주문등록과 함께 AS내역 입력
    					if(paramMap.get("O_DLV_ORD_TYPE").equals("AS출고")){
    						paramMap.put("vrSalesCustId"			, getSalesCustId);
    						paramMap.put("vrSrchAsCsType"			, "AS");
    						paramMap.put("vrSrchRitemId"			, paramMap.get("O_PRODUCT"));
    						paramMap.put("vrSrchAsCsLc"			, model.get("LC_ID"));
    						paramMap.put("vrSrchBuyDt"				, paramMap.get("O_BUYED_DT"));
    						paramMap.put("vrSrchReqId"				, model.get("REG_NO"));
    						paramMap.put("TextReqContents"			, paramMap.get("O_ETC1"));
    						paramMap.put("vrSrchOMemo"				, paramMap.get("O_MEMO"));
    						paramMap.put("AS_SALES_CUST_ID"		, getSalesCustId);
    						paramMap.put("vrFirstInsDt"			, paramMap.get("O_FIRST_INS_DT"));
    						paramMap.put("vrPayReqYn"				, paramMap.get("O_PAY_REQ_YN"));
    						paramMap.put("vrReqCost"				, paramMap.get("O_REQ_COST"));
    						paramMap.put("vrAsCostMemo"			, paramMap.get("O_AS_COST_MEMO"));
    						paramMap.put("vrAsEtc1"				, paramMap.get("O_AS_ETC1"));
    						paramMap.put("vrAsEtc2"				, paramMap.get("O_AS_ETC2"));
    						sqlMapClient.insert("wms01100.asUpload"	, paramMap);
    					}
    				}else{
    					throw new Exception("Exception:엑셀 데이터 중 등록구분이 없습니다. 등록오류.");
    				}
    			}else{
    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
    			}
    		}
    		sqlMapClient.endTransaction();
    	} catch(Exception e) {
    		e.printStackTrace();
    		throw e;
    		
    	} finally {
    		if (sqlMapClient != null) {
    			sqlMapClient.endTransaction();
    		}
    	}
    }
    
    /**
     * Method ID : insertExcelInfo_CS
     * Method 설명 : 배송주문등록 > CS정보업로드
     * 작성자 : sing09
     * @param model
     * @return
     */
    public int insertExcelInfo_CS(Map<String, Object> model, List list) throws Exception {
    	SqlMapClient sqlMapClient = getSqlMapClient();
    	int returnCnt = 0;
    	try {
    		sqlMapClient.startTransaction();
    		Map<String, Object> paramMap = null;
    		
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			paramMap.put("LC_ID"        , model.get("LC_ID"));
    			paramMap.put("REG_NO"       , model.get("REG_NO"));
    			paramMap.put("UPLOAD_ID"    , model.get("uploadId"));
    			paramMap.put("TRUST_CUST_CD", model.get("vrTrustCustCd"));
    			paramMap.put("TRUST_CUST_ID", model.get("vrTrustCustId"));

    			//거래배송사 정상 체크 
    			String stCheckDlvCompId = checkDlvCompId(paramMap);
    			if(stCheckDlvCompId.equals("N")){
					throw new Exception("Exception:주문데이터 중 거래배송사가 잘못 기입된 주문이 있습니다. [수취인 : "+paramMap.get("O_CUSTOMER_NM")+"]");
				}
    			
    			//거래배송사가 KPP이고 주문데이터 중 주문일자가 당일 이전 데이터가 있으면 등록 제한
    			if(model.get("REG_NO").equals("0000017461") || model.get("REG_NO").equals("0000002460") || model.get("REG_NO").equals("0000017340")){
    				SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
    				Date now = new Date();
    				Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
    				Date date2 = nDate.parse(nDate.format(now));
    				if(date1.before(date2)){
    					throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
    				}
    			}else{
    				if (paramMap.get("O_DLV_COM_ID").equals("KPP")){
    					SimpleDateFormat nDate = new SimpleDateFormat("yyyyMMdd");
    					Date now = new Date();
    					Date date1 = nDate.parse(paramMap.get("O_BUYED_DT").toString().replace("-", ""));
    					Date date2 = nDate.parse(nDate.format(now));
    					if(date1.before(date2)){
    						throw new Exception("Exception:주문데이터 중 주문일이 당일 이전 데이터가 있습니다. [등록제한 수취인: "+paramMap.get("O_CUSTOMER_NM")+"]");
    					}
    				}
    			}
    			
    			//WMSCT010 실제 업로드 로직
    			if (   paramMap.get("O_DLV_COM_ID")   != null && StringUtils.isNotEmpty((String)paramMap.get("O_DLV_COM_ID"))
    					&& paramMap.get("O_BUYED_DT")     != null && StringUtils.isNotEmpty((String)paramMap.get("O_BUYED_DT"))){
    				//CT010 고객주문번호로 존재여부 체크
    				//있으면 WMSAS010 채번 CS주문등록 
    				//없으면 WMSCT010 등록 후 CS주문까지 등록
    				String existRsSet = (String)sqlMapClient.queryForObject("wmsop100.selectExistOrdId_CS", paramMap);
    				if(existRsSet.equals("0")) {
						returnCnt++;
						
						String getSalesCustId = "";
                        // 1.고객관리 입력 WMSCT010
						getSalesCustId = (String)sqlMapClient.insert("wmsop100.insertExcelInfoSalesCust_CT", paramMap);
						// 2. WMSCT010 입력 후 채번 된 SALES_CUST_ID 참조
						paramMap.put("vrSalesCustId"			, getSalesCustId);
						paramMap.put("vrSrchAsCsType"			, "CS");
						paramMap.put("vrSrchRitemId"			, paramMap.get("O_PRODUCT"));
						paramMap.put("vrSrchAsCsLc"				, model.get("LC_ID"));
						paramMap.put("vrSrchBuyDt"				, paramMap.get("O_BUYED_DT"));
						paramMap.put("vrSrchReqId"				, model.get("REG_NO"));
						paramMap.put("vrFirstInsDt"				, paramMap.get("O_FIRST_INS_DT"));
						paramMap.put("TextReqContents"			, paramMap.get("O_CS_REG_CONTENTS"));
						paramMap.put("vrSrchOMemo"				, paramMap.get("O_CS_MEMO"));
						paramMap.put("vrCsCompany"				, model.get(ConstantIF.SS_CMPY_NAME));
						paramMap.put("AS_SALES_CUST_ID"			, (String)sqlMapClient.queryForObject("wmsop100.chkSalestData", paramMap));
						sqlMapClient.insert("wmsop100.csUpload"	, paramMap);
                    }else{
						returnCnt++;
						
						String getSalesCustId = (String)sqlMapClient.queryForObject("wmsop100.selectSalesCustId", paramMap);
						// 2. WMSCT010 입력 후 채번 된 SALES_CUST_ID 참조
						paramMap.put("vrSalesCustId"			, getSalesCustId);
						paramMap.put("vrSrchAsCsType"			, "CS");
						paramMap.put("vrSrchRitemId"			, paramMap.get("O_PRODUCT"));
						paramMap.put("vrSrchAsCsLc"				, model.get("LC_ID"));
						paramMap.put("vrSrchBuyDt"				, paramMap.get("O_BUYED_DT"));
						paramMap.put("vrSrchReqId"				, model.get("REG_NO"));
						paramMap.put("vrFirstInsDt"				, paramMap.get("O_FIRST_INS_DT"));
						paramMap.put("TextReqContents"			, paramMap.get("O_CS_REG_CONTENTS"));
						paramMap.put("vrSrchOMemo"				, paramMap.get("O_CS_MEMO"));
						paramMap.put("vrCsCompany"				, model.get(ConstantIF.SS_CMPY_NAME));
						paramMap.put("AS_SALES_CUST_ID"			, (String)sqlMapClient.queryForObject("wmsop100.chkSalestData", paramMap));
						sqlMapClient.insert("wmsop100.csUpload"	, paramMap);
    					
    				}
    			}else{
    				throw new Exception("Exception:엑셀 데이터 중 거래배송사명 또는 등록일이 없습니다. 등록오류.");
    			}
    		}
    		sqlMapClient.endTransaction();
    	} catch(Exception e) {
    		e.printStackTrace();
    		throw e;
    		
    	} finally {
    		if (sqlMapClient != null) {
    			sqlMapClient.endTransaction();
    		}
    	}
		return returnCnt;
    }
    
    /* 주문등록 시간체크 */
    public String checkInsertPossible(Map<String, Object> model) {
    	model.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
    	model.put("USER_NO", model.get(ConstantIF.SS_USER_NO));
    	model.put("USE_TGT", model.get(ConstantIF.SS_SVC_USE_TGT));
        return (String)executeView("wmsop100.checkInsertPossible", model);
    }
    
    /* 발주 거래배송사 체크 */
    public String checkDlvCompId(Map<String, Object> model) {
    	return (String)executeView("wmsop100.CheckDlvCompId", model);
    }
    
    /* IF_테이블 등록여부 */
    public String ifCheckUpdate(Map<String, Object> model) {
    	return (String)executeView("wmsop100.ifCheckUpdate", model);
    }
    
    /* IF_테이블 업데이트 */
	public Object ifUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop100.ifUpdate", model);
	}
}
