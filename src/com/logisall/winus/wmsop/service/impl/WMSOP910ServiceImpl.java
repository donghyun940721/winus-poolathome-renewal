package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.components.headertoolbar.actions.MoveElementCommand;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsms.service.impl.WMSMS090Dao;
import com.logisall.winus.wmsop.service.WMSOP910Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;






















/*SF*/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONObject;
import org.json.XML;
import org.json.simple.JSONArray;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.OrderWebService;

@Service("WMSOP910Service")
public class WMSOP910ServiceImpl implements WMSOP910Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP910Dao")
    private WMSOP910Dao dao;
    
	@Resource(name = "WMSMS090Dao")
	private WMSMS090Dao dao090;
    
    /**
	 * 대체 Method ID : selectData 대체 Method 설명 : 필요 데이타셋 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ITEMGRP", dao090.selectItemGrp(model));
		return map;
	}
	
	
	
    /**
     * Method ID	: listE1
     * Method 설명	: 입고관리 조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        model.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
        
        GenericResultSet res = dao.listE1(model);
        List list = res.getList();
        
        int len = list.size();
		if(len > 0){
			JsonParser jsonParser = new JsonParser();
			for(int i = 0 ; i < len ; i++){
				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
	        	String tempMemo = tempMap.get("USER_MEMO");
	        	if(tempMemo != null && !tempMemo.equals("")){
	        		//tempMemo = org.springframework.web.util.HtmlUtils.htmlUnescape(tempMemo);
	        		try{
	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
		        		if(!object.isJsonNull()){
		        			System.out.println(object.get("1").toString());
		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
		        			
		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
		        		}
	        		}catch(Exception e){
	        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	        		}
	        	}
	        }
		}
        res.setList(list);
        map.put("LIST", res);
        return map;
    }
    
    /**
     * Method ID	: listE2
     * Method 설명	: 출고관리 조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        //등록차수
        if(model.get("selectCnt") != null && !model.get("selectCnt").equals("")){
        	int selectCnt = Integer.parseInt(model.get("selectCnt").toString());
  	        String[] ordDegreeValuesStrs = new String[selectCnt];
  	        
  	        if(selectCnt == 1){
  	        	ordDegreeValuesStrs[0] =  String.valueOf(model.get("vrSrchOrdDegreeSelectBox"));
  	        }else{
  	        	 ordDegreeValuesStrs =  (String[]) model.get("vrSrchOrdDegreeSelectBox");
  	        }
  	        
  	        if(selectCnt > 0){
  	        	List<String> vrOrdInsDtOrdDegreeList   = new ArrayList<String>();
  	        	List<String> vrOrdInsDtList   = new ArrayList<String>();
  	            for(int i = 0 ; i < selectCnt ; i ++){
  		        	String valueStr = (String) ordDegreeValuesStrs[i];
  		        	if(valueStr != null && !valueStr.equals("")){
  			        	String[] strArr = valueStr.split("_");
  			        	if(strArr.length>1){
  			        		//날짜 + 차주
  			        		vrOrdInsDtOrdDegreeList.add(valueStr);
  			        	}else{
  			        		//날짜
  			        		vrOrdInsDtList.add(valueStr);
  			        	}
  		        	}
  		        }
  	            
  	            if(vrOrdInsDtList.size() > 0 && vrOrdInsDtOrdDegreeList.size() > 0 ){
  	            	model.put("vrOrdInsDtOrdDegreeType","ALL");
  	            }else{
  	            	model.put("vrOrdInsDtOrdDegreeType", null);
  	            }
  	        	
  	        	model.put("vrOrdInsDtOrdDegreeList", vrOrdInsDtOrdDegreeList);
  	        	model.put("vrOrdInsDtList", vrOrdInsDtList);
  	        }
        }
      
        GenericResultSet res = dao.listE2(model);
        List list = res.getList();
        
        int len = list.size();
		if(len > 0){
			// 통합 출고관리(B2B) 결과 확인용 print 추후 삭제예정
			System.out.println("resList : " + (Map<String, String>)list.get(0));
			//System.out.println("REC_LOC : " + ((Map<String, String>)list.get(0)).get("P_REC_LOC"));
			//System.out.println("STOCK_UNIT_NO : " + ((Map<String, String>)list.get(0)).get("P_STOCK_UNIT_NO"));
			JsonParser jsonParser = new JsonParser();
			for(int i = 0 ; i < len ; i++){
				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
				//System.out.println("REC_LOC : " + tempMap.get("REC_LOC"));
	        	String tempMemo = tempMap.get("USER_MEMO");
	        	if(tempMemo != null && !tempMemo.equals("")){
	        		try{
	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
		        		if(!object.isJsonNull()){
		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
		        			
		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
		        			
		        		}
	        		}catch(Exception e){
	        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	        		}
	        	}
	        }
		}
        res.setList(list);
        map.put("LIST", res);
        return map;
    }

    /**
     * Method ID	: listE3
     * Method 설명	: 출고관리 조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        if(model.get("vrSrchOrdSubtype").equals("140")) {
            
            model.put("vrGubun1", "50"); //재고이동
            model.put("vrGubun2", "132"); //유통점출고
            
            String subtypeGubun1 = (String)model.get("vrGubun1");
            String subtypeGubun2 = (String)model.get("vrGubun2");
            
            String chekbox[] = {subtypeGubun1, subtypeGubun2};
            model.put("chekbox", chekbox);
            
        }else if(!model.get("vrSrchOrdSubtype").equals("")){
       	 String subtypeGubun3 = (String)model.get("vrSrchOrdSubtype");
       	 String chekbox[] = {subtypeGubun3};
       	 
       	 model.put("chekbox", chekbox);
       } 
        
        if("on".equals(model.get("chkEmptyStock"))){
            model.put("chkEmptyStock", "1");
        }
        
        if("on".equals(model.get("chkBackOrder"))){
            model.put("chkBackOrder", "Y");
        }else{
        	model.put("chkBackOrder", "N");
        }
        
        List<String> vrSrchMulOrdIdArr = new ArrayList();
        String[] spVrSrchMulOrdId = model.get("vrSrchMulOrdId").toString().split(",");
        for (String keyword : spVrSrchMulOrdId ){
        	vrSrchMulOrdIdArr.add(keyword);
        }
        model.put("vrSrchMulOrdIdArr", vrSrchMulOrdIdArr);
        
        GenericResultSet res = dao.listE3(model);
        List list = res.getList();
        
        int len = list.size();
		if(len > 0){
			JsonParser jsonParser = new JsonParser();
			for(int i = 0 ; i < len ; i++){
				Map<String, String> tempMap = ((Map<String, String>)list.get(i));
	        	String tempMemo = tempMap.get("USER_MEMO");
	        	if(tempMemo != null && !tempMemo.equals("")){
	        		//org.springframework.web.util.HtmlUtils.htmlUnescape()
	        		try{
	        			JsonObject object = (JsonObject)jsonParser.parse(tempMemo);
		        		if(!object.isJsonNull()){
		        			System.out.println(object.get("1").toString());
		        			String memo1 = (!object.has("1")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("1").toString());
		        			String memo2 = (!object.has("2")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("2").toString());
		        			String memo3 = (!object.has("3")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("3").toString());
		        			String memo4 = (!object.has("4")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("4").toString());
		        			String memo5 = (!object.has("5")) ? "" : org.springframework.web.util.HtmlUtils.htmlUnescape(object.get("5").toString());
		        			
		        			tempMap.put("USER_MEMO_1", memo1.substring(1,memo1.length()-1));
		        			tempMap.put("USER_MEMO_2", memo2.substring(1,memo2.length()-1));
		        			tempMap.put("USER_MEMO_3", memo3.substring(1,memo3.length()-1));
		        			tempMap.put("USER_MEMO_4", memo4.substring(1,memo4.length()-1));
		        			tempMap.put("USER_MEMO_5", memo5.substring(1,memo5.length()-1));
		        		}
	        		}catch(Exception e){
	        			System.out.println("USER_MEMO ERROR : NOT JSONParser STRING");
	        		}
	        	}
	        }
		}
		
		res.setList(list);
        map.put("LIST", res);
        return map;
    }
    
    /**
     * Method ID   : listE4Header
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4Header(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("vrSrchOrdSubtype").equals("140")){
    		List<String> arrList = new ArrayList();
    		arrList.add("50");
    		arrList.add("132");
	        model.put("vrSrchOrdSubtypeArr", arrList);
    	}
    
        map.put("LIST", dao.listE4Header(model));
        return map;
    }
    
    /**
     * Method ID   : listE4Detail
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4Detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE4Detail(model));
        return map;
    }
    
    
    /**
     * Method ID   		: listE1SummaryCount
     * Method 설명      : 입출고통합관리 -> 입고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE1SummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE1SummaryCount(model));
        return map;
    }
    
    /**
     * Method ID   		: listE2SummaryCount
     * Method 설명      : 입출고통합관리 -> B2B 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2SummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE2SummaryCount(model));
        return map;
    }
    
    
    /**
     * Method ID   		: listE3SummaryCount
     * Method 설명      : 입출고통합관리 -> B2C 출고관리 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3SummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listE3SummaryCount(model));
        return map;
    }
    
    
}