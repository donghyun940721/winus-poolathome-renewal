
package com.logisall.winus.wmsop.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP000Service")
public class WMSOP000ServiceImpl implements WMSOP000Service {

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP000Dao")
    private WMSOP000Dao dao;

    /**
     * Method ID    : selectData
     * Method 설명      : 데이타셋
     * 작성자                 : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("UOM", dao.selectUom(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : getCenterRule
     * 대체 Method 설명    : 센터별정책항목
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> getCenterRule(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("RULE", dao.centerRule(model));
        return map;
    }
    
    /**
     *  Method ID : getAssertRule 
     *  Method 설명 : 정책검사
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> getAssertRule(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String returnResult = "Y";
        try {
            int cnt = Integer.parseInt(model.get("selectIds").toString());

            for (int i = 0; i < cnt; i++) {
                Map<String, Object> modelAss = new HashMap<String, Object>();
                modelAss.put("vrRuleType", model.get("vrRuleType"));
                modelAss.put("vrSpName", model.get("vrSpName"));
                modelAss.put("vrChainYn", model.get("vrChainYn"));
                modelAss.put("vrOrdId", model.get("ORD_ID" + i));
                modelAss.put("vrOrdSeq", model.get("ORD_SEQ" + i));
                modelAss = (Map<String, Object>)dao.assertRule(modelAss);

                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelAss.get("O_MSG_CODE")), (String)modelAss.get("O_MSG_NAME"));               
            }
            m.put("RESULT", returnResult);
            
        } catch(BizException be) {
        	m.put("RESULT", "N");
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }

    /**
     *  Method ID : listCarPop 
     *  Method 설명 : 배차작업 차량목록 
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listCarPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listCarPop(model));
        return map;
    }

    /**
     *  Method ID : searchDsp 
     *  Method 설명 : 배차작업 배차내역 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listDsp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        if (model.get("vrOrdId") != null && !"".equals(model.get("vrOrdId"))) {
            String[] arrOrdId = model.get("vrOrdId").toString().split(",");
            /*
	            String vrOrdId = "";
	            for (int i = 0; i < arrOrdId.length; i++) {
	                if (arrOrdId.length - i == 1) {
	                    vrOrdId += "'" + arrOrdId[i] + "'";
	                } else {
	                    vrOrdId += "'" + arrOrdId[i] + "',";
	                }
	            }
            */
            model.put("vrOrdId", arrOrdId);
        }
        map.put("LIST", dao.listDsp(model));
        return map;
    }

    /**
     *  Method ID : searchNewDsp 
     *  Method 설명 : 미배차내역 조회 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listNewDsp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        if (model.get("vrOrdId") != null && !"".equals(model.get("vrOrdId"))) {
            String[] arrOrdId = model.get("vrOrdId").toString().split(",");
            
            /* 2016.03.02 Security 대응 */
            /*
	            String vrOrdId = "";
	            for (int i = 0; i < arrOrdId.length; i++) {
	                if (arrOrdId.length - i == 1) {
	                    vrOrdId += "'" + arrOrdId[i] + "'";
	                } else {
	                    vrOrdId += "'" + arrOrdId[i] + "',";
	                }
	            }
            */
            model.put("vrOrdId", arrOrdId);
            
        }
        map.put("LIST", dao.listNewDsp(model));
        return map;
    }

//    /**
//     *  Method ID : saveDsp 
//     *  Method 설명 : 배차내역(저장,수정) 
//     * 작성자 : chsong
//     * 
//     * @param model
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public Map<String, Object> saveDsp(Map<String, Object> model) throws Exception {
//        Map<String, Object> m = new HashMap<String, Object>();
//        int errCnt = 0;
//        String errMsg = MessageResolver.getMessage("save.error");
//        try {
//
//            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
//           
//            // 저장, 수정
//            String[] dspId      = new String[tmpCnt];
//            String[] carId      = new String[tmpCnt];
//            String[] dutyId     = new String[tmpCnt];
//            String[] dockNo     = new String[tmpCnt];
//            String[] transCd    = new String[tmpCnt];
//
//            String[] drvNm      = new String[tmpCnt];
//            String[] drvTel     = new String[tmpCnt];
//            String[] dlvType    = new String[tmpCnt];
//            String[] transReqDt = new String[tmpCnt];
//            String[] kinOutYn   = new String[tmpCnt];
//
//            String[] carConfYn  = new String[tmpCnt];
//            String[] dlvKm      = new String[tmpCnt];
//            String[] outConfDt  = new String[tmpCnt];
//            String[] upAmt      = new String[tmpCnt];
//            String[] downAmt    = new String[tmpCnt];
//
//            String[] transAmt   = new String[tmpCnt];
//            String[] etcAmt     = new String[tmpCnt];
//            String[] arrN       = new String[tmpCnt];
//            String[] ordId      = new String[tmpCnt];
//            String[] ordSeq     = new String[tmpCnt];
//
//            String[] ritemId    = new String[tmpCnt];
//            String[] ordUomId   = new String[tmpCnt];
//            String[] ordQty     = new String[tmpCnt];
//            String[] dlvZone    = new String[tmpCnt];
//            String[] dlvBlock   = new String[tmpCnt];
//
//            String[] dlvSeq         = new String[tmpCnt];
//            String[] transCustCd    = new String[tmpCnt];
//            String[] transCustAddr  = new String[tmpCnt];
//            String[] dlvReqDt       = new String[tmpCnt];
//            String[] expBoxQty      = new String[tmpCnt];
//
//            String[] expPltQty  = new String[tmpCnt];
//            String[] capaTot    = new String[tmpCnt];
//            String[] dlvConfQty = new String[tmpCnt];
//            String[] carCd      = new String[tmpCnt];
//
//            for (int i = 0; i < tmpCnt; i++) {
//                dspId[i]        = (String)model.get("DSP_ID" + i);
//                carId[i]        = (String)model.get("CAR_ID" + i);
//                dutyId[i]       = (String)model.get("DUTY_ID" + i);
//                dockNo[i]       = (String)model.get("DOCK_NO" + i);
//                transCd[i]      = (String)model.get("TRANS_CD" + i);
//
//                drvNm[i]        = (String)model.get("DRV_NM" + i);
//                drvTel[i]       = (String)model.get("DRV_TEL" + i);
//                dlvType[i]      = (String)model.get("DLV_TYPE" + i);
//                transReqDt[i]   = (String)model.get("TRANS_REQ_DT" + i);
//                kinOutYn[i]     = (String)model.get("KIN_OUT_YN" + i);
//
//                carConfYn[i]    = (String)model.get("CAR_CONF_YN" + i);
//                dlvKm[i]        = (String)model.get("DLV_KM" + i);
//                outConfDt[i]    = (String)model.get("OUT_CONF_DT" + i);
//                upAmt[i]        = (String)model.get("UP_AMT" + i);
//                downAmt[i]      = (String)model.get("DOWN_AMT" + i);
//
//                transAmt[i]     = (String)model.get("TRANS_AMT" + i);
//                etcAmt[i]       = (String)model.get("ETC_AMT" + i);
//                arrN[i]         = "N";
//                ordId[i]        = (String)model.get("ORD_ID" + i);
//                ordSeq[i]       = (String)model.get("ORD_SEQ" + i);
//
//                ritemId[i]      = (String)model.get("RITEM_ID" + i);
//                ordUomId[i]     = (String)model.get("ORD_UOM_ID" + i);
//                ordQty[i]       = (String)model.get("ORD_QTY" + i);
//                dlvZone[i]      = (String)model.get("DLV_ZONE" + i);
//                dlvBlock[i]     = (String)model.get("DLV_BLOCK" + i);
//
//                dlvSeq[i]           = (String)model.get("DLV_SEQ" + i);
//                transCustCd[i]      = (String)model.get("TRANS_CUST_CD" + i);
//                transCustAddr[i]    = (String)model.get("TRANS_CUST_ADDR" + i);
//                dlvReqDt[i]         = (String)model.get("DLV_REQ_DT" + i);
//                expBoxQty[i]        = (String)model.get("EXP_BOX_QTY" + i);
//
//                expPltQty[i]    = (String)model.get("EXP_PLT_QTY" + i);
//                capaTot[i]      = (String)model.get("CAPA_TOT" + i);
//                dlvConfQty[i]   = (String)model.get("DLV_CONF_QTY" + i);
//                carCd[i]        = (String)model.get("CAR_CD" + i);
//            }
//            // 프로시져에 보낼것들 다담는다
//            Map<String, Object> modelIns = new HashMap<String, Object>();
//
//            modelIns.put("dspId"        , dspId);
//            modelIns.put("carId"        , carId);
//            modelIns.put("dutyId"       , dutyId);
//            modelIns.put("dockNo"       , dockNo);
//            modelIns.put("transCd"      , transCd);
//
//            modelIns.put("drvNm"        , drvNm);
//            modelIns.put("drvTel"       , drvTel);
//            modelIns.put("dlvType"      , dlvType);
//            modelIns.put("transReqDt"   , transReqDt);
//            modelIns.put("kinOutYn"     , kinOutYn);
//
//            modelIns.put("carConfYn"    , carConfYn);
//            modelIns.put("dlvKm"        , dlvKm);
//            modelIns.put("outConfDt"    , outConfDt);
//            modelIns.put("upAmt"        , upAmt);
//            modelIns.put("downAmt"      , downAmt);
//
//            modelIns.put("transAmt"     , transAmt);
//            modelIns.put("etcAmt"       , etcAmt);
//            modelIns.put("arrN"         , arrN);
//            modelIns.put("ordId"        , ordId);
//            modelIns.put("ordSeq"       , ordSeq);
//
//            modelIns.put("ritemId"      , ritemId);
//            modelIns.put("ordUomId"     , ordUomId);
//            modelIns.put("ordQty"       , ordQty);
//            modelIns.put("dlvZone"      , dlvZone);
//            modelIns.put("dlvBlock"     , dlvBlock);
//
//            modelIns.put("dlvSeq"       , dlvSeq);
//            modelIns.put("transCustCd"  , transCustCd);
//            modelIns.put("transCustAddr", transCustAddr);
//            modelIns.put("dlvReqDt"     , dlvReqDt);
//            modelIns.put("expBoxQty"    , expBoxQty);
//
//            modelIns.put("expPltQty"    , expPltQty);
//            modelIns.put("capaTot"      , capaTot);
//            modelIns.put("dlvConfQty"   , dlvConfQty);
//            modelIns.put("carCd"        , carCd);
//
//            // session 및 등록정보
//            modelIns.put("vrOrdType"    , model.get("vrOrdType"));
//            modelIns.put("USER_NO"      , (String)model.get(ConstantIF.SS_USER_NO));
//            modelIns.put("WORK_IP"      , (String)model.get(ConstantIF.SS_CLIENT_IP));
//
//            // dao
//            modelIns = (Map<String, Object>)dao.saveDsp(modelIns);
//            errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
//            if(errCnt == 0){
//                errMsg = MessageResolver.getMessage("save.success");
//            }else{
//                errMsg = modelIns.get("O_MSG_NAME").toString();
//            }
//            m.put("errCnt", errCnt);
//            m.put("MSG", errMsg);
//        } catch (Exception e) {
//            throw e;
//        }
//        return m;
//    }
//    
    /**
     *  Method ID : deleteDsp 
     *  Method 설명 : 배차내역(삭제) 
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteDsp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("delete.error");
        try {

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
           
            // 삭제
            String[] ordId  = new String[tmpCnt];
            String[] ordSeq = new String[tmpCnt];
            String[] dspId  = new String[tmpCnt];
            String[] ordQty = new String[tmpCnt];

            for (int i = 0; i < tmpCnt; i++) {
                ordId[i]    = (String)model.get("ORD_ID" + i);
                ordSeq[i]   = (String)model.get("ORD_SEQ" + i);
                dspId[i]    = (String)model.get("DSP_ID" + i);
                ordQty[i]   = (String)model.get("ORG_ORD_QTY" + i);
            }
            
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelDel = new HashMap<String, Object>();
            modelDel.put("ordId"    , ordId);
            modelDel.put("ordSeq"   , ordSeq);
            modelDel.put("dspId"    , dspId);
            modelDel.put("ordQty"   , ordQty);
            modelDel.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO));
            modelDel.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));

            // dao
            modelDel = (Map<String, Object>)dao.deleteDsp(modelDel);
            errCnt = Integer.parseInt(modelDel.get("O_MSG_CODE").toString());
            if(errCnt == 0){
                errMsg = MessageResolver.getMessage("delete.success");
            }else{
                errMsg = modelDel.get("O_MSG_NAME").toString();
            }
            m.put("errCnt", errCnt);
            m.put("MSG", errMsg);
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     *  Method ID : saveDsp 
     *  Method 설명 : 배차내역(저장,수정,삭제) 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveDsp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if (insCnt > 0) {
                // 저장, 수정
                String[] dspId = new String[insCnt];
                String[] carId = new String[insCnt];
                String[] dutyId = new String[insCnt];
                String[] dockNo = new String[insCnt];
                String[] transCd = new String[insCnt];

                String[] drvNm = new String[insCnt];
                String[] drvTel = new String[insCnt];
                String[] dlvType = new String[insCnt];
                String[] transReqDt = new String[insCnt];
                String[] kinOutYn = new String[insCnt];

                String[] carConfYn = new String[insCnt];
                String[] dlvKm = new String[insCnt];
                String[] outConfDt = new String[insCnt];
                String[] upAmt = new String[insCnt];
                String[] downAmt = new String[insCnt];

                String[] transAmt = new String[insCnt];
                String[] etcAmt = new String[insCnt];
                String[] arrN = new String[insCnt];
                String[] ordId = new String[insCnt];
                String[] ordSeq = new String[insCnt];

                String[] ritemId = new String[insCnt];
                String[] ordUomId = new String[insCnt];
                String[] ordQty = new String[insCnt];
                String[] dlvZone = new String[insCnt];
                String[] dlvBlock = new String[insCnt];

                String[] dlvSeq = new String[insCnt];
                String[] transCustCd = new String[insCnt];
                String[] transCustAddr = new String[insCnt];
                String[] dlvReqDt = new String[insCnt];
                String[] expBoxQty = new String[insCnt];

                String[] expPltQty = new String[insCnt];
                String[] capaTot = new String[insCnt];
                String[] dlvConfQty = new String[insCnt];
                String[] carCd = new String[insCnt];
                
                //추가
                String[] adjuctCarTon = new String[insCnt];
                String[] dspMemoTon = new String[insCnt];
                
                for (int i = 0; i < insCnt; i++) {
                    dspId[i] = (String)model.get("I_DSP_ID" + i);
                    carId[i] = (String)model.get("I_CAR_ID" + i);
                    dutyId[i] = (String)model.get("I_DUTY_ID" + i);
                    dockNo[i] = (String)model.get("I_DOCK_NO" + i);
                    transCd[i] = (String)model.get("I_TRANS_CD" + i);

                    drvNm[i] = (String)model.get("I_DRV_NM" + i);
                    drvTel[i] = (String)model.get("I_DRV_TEL" + i);
                    dlvType[i] = (String)model.get("I_DLV_TYPE" + i);
                    transReqDt[i] = (String)model.get("I_TRANS_REQ_DT" + i);
                    kinOutYn[i] = (String)model.get("I_KIN_OUT_YN" + i);

                    carConfYn[i] = (String)model.get("I_CAR_CONF_YN" + i);
                    dlvKm[i] = (String)model.get("I_DLV_KM" + i);
                    outConfDt[i] = (String)model.get("I_OUT_CONF_DT" + i);
                    upAmt[i] = (String)model.get("I_UP_AMT" + i);
                    downAmt[i] = (String)model.get("I_DOWN_AMT" + i);

                    transAmt[i] = (String)model.get("I_TRANS_AMT" + i);
                    etcAmt[i] = (String)model.get("I_ETC_AMT" + i);
                    arrN[i] = "N";
                    ordId[i] = (String)model.get("I_ORD_ID" + i);
                    ordSeq[i] = (String)model.get("I_ORD_SEQ" + i);

                    ritemId[i] = (String)model.get("I_RITEM_ID" + i);
                    ordUomId[i] = (String)model.get("I_ORD_UOM_ID" + i);
                    ordQty[i] = (String)model.get("I_ORD_QTY" + i);
                    dlvZone[i] = (String)model.get("I_DLV_ZONE" + i);
                    dlvBlock[i] = (String)model.get("I_DLV_BLOCK" + i);

                    dlvSeq[i] = (String)model.get("I_DLV_SEQ" + i);
                    transCustCd[i] = (String)model.get("I_TRANS_CUST_CD" + i);
                    transCustAddr[i] = (String)model.get("I_TRANS_CUST_ADDR" + i);
                    dlvReqDt[i] = (String)model.get("I_DLV_REQ_DT" + i);
                    expBoxQty[i] = (String)model.get("I_EXP_BOX_QTY" + i);

                    expPltQty[i] = (String)model.get("I_EXP_PLT_QTY" + i);
                    capaTot[i] = (String)model.get("I_CAPA_TOT" + i);
                    dlvConfQty[i] = (String)model.get("I_DLV_CONF_QTY" + i);
                    carCd[i] = (String)model.get("I_CAR_CD" + i);
                    
                    //추가
                    adjuctCarTon[i] = (String)model.get("I_ADJUCT_CAR_TON" + i);
                    dspMemoTon[i] = (String)model.get("I_DSP_MEMO_TON" + i);
                
                }
                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();

                modelIns.put("dspId", dspId);
                modelIns.put("carId", carId);
                modelIns.put("dutyId", dutyId);
                modelIns.put("dockNo", dockNo);
                modelIns.put("transCd", transCd);

                modelIns.put("drvNm", drvNm);
                modelIns.put("drvTel", drvTel);
                modelIns.put("dlvType", dlvType);
                modelIns.put("transReqDt", transReqDt);
                modelIns.put("kinOutYn", kinOutYn);

                modelIns.put("carConfYn", carConfYn);
                modelIns.put("dlvKm", dlvKm);
                modelIns.put("outConfDt", outConfDt);
                modelIns.put("upAmt", upAmt);
                modelIns.put("downAmt", downAmt);

                modelIns.put("transAmt", transAmt);
                modelIns.put("etcAmt", etcAmt);
                modelIns.put("arrN", arrN);
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);

                modelIns.put("ritemId", ritemId);
                modelIns.put("ordUomId", ordUomId);
                modelIns.put("ordQty", ordQty);
                modelIns.put("dlvZone", dlvZone);
                modelIns.put("dlvBlock", dlvBlock);

                modelIns.put("dlvSeq", dlvSeq);
                modelIns.put("transCustCd", transCustCd);
                modelIns.put("transCustAddr", transCustAddr);
                modelIns.put("dlvReqDt", dlvReqDt);
                modelIns.put("expBoxQty", expBoxQty);

                modelIns.put("expPltQty", expPltQty);
                modelIns.put("capaTot", capaTot);
                modelIns.put("dlvConfQty", dlvConfQty);
                modelIns.put("carCd", carCd);

                //추가
                modelIns.put("adjuctCarTon", adjuctCarTon);
                modelIns.put("dspMemoTon", dspMemoTon);

                // session 및 등록정보
                modelIns.put("vrOrdType", model.get("vrOrdType"));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));

                // dao
                modelIns = (Map<String, Object>)dao.saveDsp(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }
            // 등록 수정 끝
            if (delCnt > 0) {
                String[] ordId = new String[delCnt];
                String[] ordSeq = new String[delCnt];
                String[] dspId = new String[delCnt];
                String[] ordQty = new String[delCnt];
                for (int i = 0; i < delCnt; i++) {
                    ordId[i] = (String)model.get("D_ORD_ID" + i);
                    ordSeq[i] = (String)model.get("D_ORD_SEQ" + i);
                    dspId[i] = (String)model.get("D_DSP_ID" + i);
                    ordQty[i] = (String)model.get("D_ORG_ORD_QTY" + i);
                }

                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelDel = new HashMap<String, Object>();
                modelDel.put("ordId", ordId);
                modelDel.put("ordSeq", ordSeq);
                modelDel.put("dspId", dspId);
                modelDel.put("ordQty", ordQty);
                modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDel.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));

                modelDel = (Map<String, Object>)dao.deleteDsp(modelDel);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));                
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );            
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    /**
     * 
     * 대체 Method ID   : listExcelDsp
     * 대체 Method 설명    : 배차내역 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcelDsp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        /*
        if (!model.get("vrOrdId").equals("")) {        	
            String[] arrOrdId = model.get("vrOrdId").toString().split(",");
            String vrOrdId = "";
            for (int i = 0; i < arrOrdId.length; i++) {
                if (arrOrdId.length - i == 1) {
                    vrOrdId += "'" + arrOrdId[i] + "'";
                } else {
                    vrOrdId += "'" + arrOrdId[i] + "',";
                }
            }
            model.put("vrOrdId", vrOrdId);
        }
        */
        
        if (model.get("vrOrdId") != null && !"".equals(model.get("vrOrdId"))) {
            String[] arrOrdId = model.get("vrOrdId").toString().split(",");
            model.put("vrOrdId", arrOrdId);
        }
        map.put("LIST", dao.listDsp(model));
        return map;
    } 
    
    /**
     *  Method ID : listConfirmDsp 
     *  Method 설명 : 입차확인 내역 
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listConfirmDsp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        /*
	        if (!model.get("vrOrdId").equals("")) {
	            String[] arrOrdId = model.get("vrOrdId").toString().split(",");
	            String vrOrdId = "";
	            for (int i = 0; i < arrOrdId.length; i++) {
	                if (arrOrdId.length - i == 1) {
	                    vrOrdId += "'" + arrOrdId[i] + "'";
	                } else {
	                    vrOrdId += "'" + arrOrdId[i] + "',";
	                }
	            }
	            model.put("vrOrdId", vrOrdId);
	        }
        */
        if (model.get("vrOrdId") != null && !"".equals((String)model.get("vrOrdId"))) {
            String[] arrOrdId = model.get("vrOrdId").toString().split(",");
            model.put("vrOrdId", arrOrdId);
        }
        map.put("LIST", dao.listConfirmDsp(model));
        return map;
    }

    
    
    
    
    /**
     *  Method ID : saveConfirmCarIn 
     *  Method 설명 : 입차내역 승인/취소 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveConfirmCarIn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if (tmpCnt > 0) {
                String[] ordId = new String[tmpCnt];
                String[] ordSeq = new String[tmpCnt];
                String[] dspId = new String[tmpCnt];

                for (int i = 0; i < tmpCnt; i++) {
                    ordId[i] = (String)model.get("ORD_ID" + i);
                    ordSeq[i] = (String)model.get("ORD_SEQ" + i);
                    dspId[i] = (String)model.get("DSP_ID" + i);
                }
                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();

                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("dspId", dspId);

                // session 및 등록정보
                modelIns.put("vrApproveYn", model.get("vrApproveYn"));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));

                // dao
                modelIns = (Map<String, Object>)dao.saveConfirmCarIn(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }

            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }

    /**
     *  Method ID : saveSetDock 
     *  Method 설명 : dock장 지정/해제 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSetDock(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if (tmpCnt > 0) {
                String[] dockNo = new String[tmpCnt];
                String[] carId = new String[tmpCnt];
                String[] dspId = new String[tmpCnt];

                for (int i = 0; i < tmpCnt; i++) {
                    dockNo[i] = (String)model.get("DOCK_NO" + i);
                    carId[i] = (String)model.get("CAR_ID" + i);
                    dspId[i] = (String)model.get("DSP_ID" + i);
                }
                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();

                modelIns.put("dockNo", dockNo);
                modelIns.put("carId", carId);
                modelIns.put("dspId", dspId);

                // session 및 등록정보
                modelIns.put("vrDockUseYn", model.get("vrDockUseYn"));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));

                // dao
                modelIns = (Map<String, Object>)dao.saveSetDock(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }  
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }

    /**
     *  Method ID : listLoc 
     *  Method 설명 : Location지정 목록 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listLoc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listLoc(model));
        return map;
    }
    
    /**
     *  Method ID : listLocV2
     *  Method 설명 : Location 다중 지정 목록 
     * 작성자 : schan
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listLocV2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        // 필요 param : vrOrdId, vrOrdSeq
        int tmpCnt = Integer.parseInt(model.get("vrRowCount").toString());
        
        String[] arrOrdId = new String[tmpCnt];
        String[] arrOrdSeq = new String[tmpCnt];
        String[] arrWorkId = new String[tmpCnt];
        String[] arrWorkSeq = new String[tmpCnt];
        String[] arrWorkType = new String[tmpCnt];
        ArrayList arrOrd = new ArrayList();
        for(int i = 0; i< tmpCnt; i++){
        	arrOrdId[i] = (String) model.get("arrOrdId_"+i);
        	arrOrdSeq[i] = (String) model.get("arrOrdSeq_"+i);
        	String tempWorkId = (String) model.get("arrWorkId_"+i);
        	if(tempWorkId!=null){
        		arrWorkId[i] = tempWorkId;
        	}
        	else {
        		arrWorkId[i] = "";
        	}
        	String tempWorkSeq = (String) model.get("arrWorkSeq_"+i);
        	if(tempWorkSeq!=null){
        		arrWorkSeq[i] = tempWorkSeq;
        	}
        	else {
        		arrWorkSeq[i] = "";
        	}
        	arrWorkType[i] = (String) model.get("arrWorkType_"+i);
        	
        	Map<String,Object>tempMap = new HashMap<String,Object>();
        	tempMap.put("vrOrdId", arrOrdId[i]);
        	tempMap.put("vrOrdSeq", arrOrdSeq[i]);
        	tempMap.put("vrWorkId", arrWorkId[i]);
        	tempMap.put("vrWorkSeq", arrWorkSeq[i]);
        	tempMap.put("vrWorkType", arrWorkType[i]);
        	arrOrd.add(tempMap);
        }
        // vrOrdId=, vrOrdSeq=, vrWorkId=, vrWorkSeq
        Map<String, Object> tempModel = new HashMap<String, Object>();
        tempModel.put("arrOrdId", arrOrdId);
        tempModel.put("arrOrd", arrOrd);
        tempModel.put("vrOrdType",model.get("vrOrdType"));
        tempModel.put("sidx",model.get("sidx"));
        tempModel.put("sord",model.get("sord"));
        tempModel.put("SS_SVC_NO", String.valueOf(model.get(ConstantIF.SS_SVC_NO)));
        tempModel.put("vrCustId", model.get("vrCustId"));

        if (model.get("page") == null) {
        	tempModel.put("pageIndex", "1");
        } else {
        	tempModel.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
        	tempModel.put("pageSize", "20");
        } else {
        	tempModel.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listLocV2(tempModel));
        return map;
    }

    /**
     *  Method ID : listLocSub 
     *  Method 설명 : Location지정 Sub 목록 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listLocSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listLocSub(model));
        return map;
    }

    /**
     *  Method ID : saveLocInitType 
     *  Method 설명 : 로케이션 추천을위한?? 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("PMD")
    public Map<String, Object> saveLocInitType(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        int errCnt = 0;
        
        try {
            model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            model = (Map<String, Object>)dao.saveLocInitType(model);
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(model.get("O_MSG_CODE")), (String)model.get("O_MSG_NAME"));

            map.put("errCnt", errCnt);
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listLoc(model));

        } catch(BizException be) {
        	throw new Exception(be.getMessage());        
            
        } catch (Exception e) {
            throw e;
        }
        return map;
    }
    
    /**
     *  Method ID : saveLocInitTypeV2
     *  Method 설명 : 로케이션 추천 list init 다중파라미터ver 
     * 작성자 : schan
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    @SuppressWarnings("PMD")
    public Map<String, Object> saveLocInitTypeV2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        int errCnt = 0;
        
        try {
            model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            // 필요 param : vrOrdId, vrOrdSeq
            int tmpCnt = Integer.parseInt(model.get("vrRowCount").toString());
            
            String[] arrOrdId = new String[tmpCnt];
            String[] arrOrdSeq = new String[tmpCnt];
            String[] arrWorkId = new String[tmpCnt];
            String[] arrWorkSeq = new String[tmpCnt];
            String[] arrWorkType = new String[tmpCnt];
            List arrOrd = new ArrayList();
            for(int i = 0; i< tmpCnt; i++){
            	arrOrdId[i] = (String) model.get("arrOrdId_"+i);
            	arrOrdSeq[i] = (String) model.get("arrOrdSeq_"+i);
            	String tempWorkId = (String) model.get("arrWorkId_"+i);
            	if(tempWorkId!=null){
            		arrWorkId[i] = tempWorkId;
            	}
            	else {
            		arrWorkId[i] = "";
            	}
            	String tempWorkSeq = (String) model.get("arrWorkSeq_"+i);
            	if(tempWorkSeq!=null){
            		arrWorkSeq[i] = tempWorkSeq;
            	}
            	else {
            		arrWorkSeq[i] = "";
            	}
            	arrWorkType[i] = (String) model.get("arrWorkType_"+i);

                
            	Map<String,Object>tempMap = new HashMap<String,Object>();
            	tempMap.put("vrOrdId", arrOrdId[i]);
            	tempMap.put("vrOrdSeq", arrOrdSeq[i]);
            	tempMap.put("vrWorkId", arrWorkId[i]);
            	tempMap.put("vrWorkSeq", arrWorkSeq[i]);
                tempMap.put("vrWorkType", arrWorkType[i]);
            	arrOrd.add(tempMap);
            	
            }
            
            for(int i = 0; i < tmpCnt; i++){
            	Map<String, Object> tempMap = new HashMap<String, Object>();
            	tempMap.put("vrOrdId", arrOrdId[i]);
            	tempMap.put("vrOrdSeq", arrOrdSeq[i]);
            	tempMap.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            	tempMap.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            	tempMap.put("vrOrdType",(String)model.get("vrOrdType"));
                tempMap.put("vrWorkType", arrWorkType[i]);
            	tempMap = (Map<String, Object>)dao.saveLocInitType(tempMap);
            	ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(tempMap.get("O_MSG_CODE")), (String)tempMap.get("O_MSG_NAME"));
            }
            map.put("errCnt", errCnt);
            // vrOrdId=, vrOrdSeq=, vrWorkId=, vrWorkSeq
            Map<String, Object> tempModel = new HashMap<String, Object>();
            tempModel.put("arrOrdId", arrOrdId);
            tempModel.put("arrOrd", arrOrd);
            tempModel.put("vrOrdType",(String)model.get("vrOrdType"));
            tempModel.put("sidx",model.get("sidx"));
            tempModel.put("sord",model.get("sord"));
            tempModel.put("SS_SVC_NO", String.valueOf(model.get(ConstantIF.SS_SVC_NO)));
            tempModel.put("vrCustId", model.get("vrCustId"));
            
            if (model.get("page") == null) {
            	tempModel.put("pageIndex", "1");
            } else {
            	tempModel.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
            	tempModel.put("pageSize", "20");
            } else {
            	tempModel.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.listLocV2(tempModel));

        } catch(BizException be) {
        	throw new Exception(be.getMessage());        
            
        } catch (Exception e) {
            throw e;
        }
        return map;
    }
    
    
//    /**
//     *  Method ID : saveLoc 
//     *  Method 설명 : 로케이션 저장 
//     * 작성자 : chsong
//     * @param model
//     * @return
//     * @throws Exception
//     */
//    @Override
//    public Map<String, Object> saveLoc(Map<String, Object> model) throws Exception {
//        Map<String, Object> m = new HashMap<String, Object>();
//        int errCnt = 0;
//        String errMsg = MessageResolver.getMessage("save.error");
//        try {
//
//            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
//            if (tmpCnt > 0) {
//                // 저장, 수정
//                String[] workId     = new String[tmpCnt];
//                String[] workSeq    = new String[tmpCnt];
//                String[] workSubseq = new String[tmpCnt];
//                String[] pdaCd      = new String[tmpCnt];
//                String[] employeeId = new String[tmpCnt];
//
//                String[] locId      = new String[tmpCnt];
//                String[] ritemId    = new String[tmpCnt];
//                String[] workQty    = new String[tmpCnt];
//                String[] uomId      = new String[tmpCnt];
//                String[] confQty    = new String[tmpCnt];
//
//                String[] subLotId   = new String[tmpCnt];
//                String[] stockId    = new String[tmpCnt];
//                String[] realPltQty = new String[tmpCnt];
//                String[] realBoxQty = new String[tmpCnt];
//                
//                for (int i = 0; i < tmpCnt; i++) {
//                    workId[i]       = (String)model.get("WORK_ID" + i);
//                    workSeq[i]      = (String)model.get("WORK_SEQ" + i);
//                    workSubseq[i]   = (String)model.get("WORK_SUBSEQ" + i);
//                    pdaCd[i]        = (String)model.get("PDA_CD" + i);
//                    employeeId[i]   = (String)model.get("EMPLOYEE_ID" + i);
//
//                    locId[i]        = (String)model.get("LOC_ID" + i);
//                    ritemId[i]      = (String)model.get("RITEM_ID" + i);
//                    workQty[i]      = (String)model.get("WORK_QTY" + i);
//                    uomId[i]        = (String)model.get("UOM_ID" + i);
//                    confQty[i]      = (String)model.get("CONF_QTY" + i);
//
//                    subLotId[i]     = (String)model.get("SUB_LOT_ID" + i);
//                    stockId[i]      = (String)model.get("STOCK_ID" + i);
//                    realPltQty[i]   = (String)model.get("REAL_PLT_QTY" + i);
//                    realBoxQty[i]   = (String)model.get("REAL_BOX_QTY" + i);
//                }
//                // 프로시져에 보낼것들 다담는다
//                Map<String, Object> modelIns = new HashMap<String, Object>();
//
//                modelIns.put("workId"       , workId);
//                modelIns.put("workSeq"      , workSeq);
//                modelIns.put("workSubseq"   , workSubseq);
//                modelIns.put("pdaCd"        , pdaCd);
//                modelIns.put("employeeId"   , employeeId);
//
//                modelIns.put("locId"        , locId);
//                modelIns.put("ritemId"      , ritemId);
//                modelIns.put("workQty"      , workQty);
//                modelIns.put("uomId"        , uomId);
//                modelIns.put("confQty"      , confQty);
//
//                modelIns.put("subLotId"     , subLotId);
//                modelIns.put("stockId"      , stockId);
//                modelIns.put("realPltQty"   , realPltQty);
//                modelIns.put("realBoxQty"   , realBoxQty);
//
//                // session 및 등록정보
//                modelIns.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));
//                modelIns.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO));
//
//                // dao
//                modelIns = (Map<String, Object>)dao.saveLoc(modelIns);
//                errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
//                errMsg = modelIns.get("O_MSG_NAME").toString();
//            }
//            m.put("errCnt", errCnt);
//            if (errCnt == 0) {
//                errMsg = MessageResolver.getMessage("save.success");
//            }
//            m.put("MSG", errMsg);  
//            
//        } catch (Exception e) {
//            throw e;
//        }
//        return m;
//    }

    /**
     *  Method ID : deleteLoc 
     *  Method 설명 : 로케이션 삭제 
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteLoc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
           
            String[] workId     = new String[tmpCnt];
            String[] workSeq    = new String[tmpCnt];
            String[] workSubseq = new String[tmpCnt];
            for (int i = 0; i < tmpCnt; i++) {
                workId[i]       = (String)model.get("WORK_ID" + i);
                workSeq[i]      = (String)model.get("WORK_SEQ" + i);
                workSubseq[i]   = (String)model.get("WORK_SUBSEQ" + i);
            }

            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelDel = new HashMap<String, Object>();
            modelDel.put("workId"       , workId);
            modelDel.put("workSeq"      , workSeq);
            modelDel.put("workSubseq"   , workSubseq);

            modelDel.put("WORK_IP"      , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelDel.put("USER_NO"      , (String)model.get(ConstantIF.SS_USER_NO));

            modelDel = (Map<String, Object>)dao.deleteLoc(modelDel);
            ServiceUtil.isValidReturnCode("WMSOP000", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("delete.success"));
            
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }    
    
    
    /**
     *  Method ID : saveLoc 
     *  Method 설명 : 로케이션 저장 
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveLoc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try {

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if (insCnt > 0) {
                // 저장, 수정
                String[] workId = new String[insCnt];
                String[] workSeq = new String[insCnt];
                String[] workSubseq = new String[insCnt];
                String[] pdaCd = new String[insCnt];
                String[] employeeId = new String[insCnt];

                String[] locId = new String[insCnt];
                String[] ritemId = new String[insCnt];
                String[] workQty = new String[insCnt];
                String[] uomId = new String[insCnt];
                String[] confQty = new String[insCnt];

                String[] subLotId = new String[insCnt];
                String[] stockId = new String[insCnt];
                String[] realPltQty = new String[insCnt];
                String[] realBoxQty = new String[insCnt];
                
                String[] itemDateEnd = new String[insCnt];
                
                for (int i = 0; i < insCnt; i++) {
                    workId[i] = (String)model.get("I_WORK_ID" + i);
                    workSeq[i] = (String)model.get("I_WORK_SEQ" + i);
                    workSubseq[i] = (String)model.get("I_WORK_SUBSEQ" + i);
                    pdaCd[i] = (String)model.get("I_PDA_CD" + i);
                    employeeId[i] = (String)model.get("I_EMPLOYEE_ID" + i);

                    locId[i] = (String)model.get("I_LOC_ID" + i);
                    ritemId[i] = (String)model.get("I_RITEM_ID" + i);
                    workQty[i] = (String)model.get("I_WORK_QTY" + i);
                    uomId[i] = (String)model.get("I_UOM_ID" + i);
                    confQty[i] = (String)model.get("I_CONF_QTY" + i);

                    subLotId[i] = (String)model.get("I_SUB_LOT_ID" + i);
                    stockId[i] = (String)model.get("I_STOCK_ID" + i);
                    realPltQty[i] = (String)model.get("I_REAL_PLT_QTY" + i);
                    realBoxQty[i] = (String)model.get("I_REAL_BOX_QTY" + i);
                    
                    if ( model.get("I_ITEM_DATE_END" +i) != null ) { 
                    	itemDateEnd[i] = ((String)model.get("I_ITEM_DATE_END" +i)).trim().replaceAll("-", "");
                    } else {
                    	itemDateEnd[i] = (String)model.get("I_ITEM_DATE_END" +i);
                    }
                }
                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();

                modelIns.put("workId", workId);
                modelIns.put("workSeq", workSeq);
                modelIns.put("workSubseq", workSubseq);
                modelIns.put("pdaCd", pdaCd);
                modelIns.put("employeeId", employeeId);

                modelIns.put("locId", locId);
                modelIns.put("ritemId", ritemId);
                modelIns.put("workQty", workQty);
                modelIns.put("uomId", uomId);
                modelIns.put("confQty", confQty);

                modelIns.put("subLotId", subLotId);
                modelIns.put("stockId", stockId);
                modelIns.put("realPltQty", realPltQty);
                modelIns.put("realBoxQty", realBoxQty);

                modelIns.put("itemDateEnd", itemDateEnd);
                // session 및 등록정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                // dao
                modelIns = (Map<String, Object>)dao.saveLoc(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            // 등록 수정 끝

            if (delCnt > 0) {
                String[] workId = new String[delCnt];
                String[] workSeq = new String[delCnt];
                String[] workSubseq = new String[delCnt];
                for (int i = 0; i < delCnt; i++) {
                    workId[i] = (String)model.get("D_WORK_ID" + i);
                    workSeq[i] = (String)model.get("D_WORK_SEQ" + i);
                    workSubseq[i] = (String)model.get("D_WORK_SUBSEQ" + i);
                }

                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelDel = new HashMap<String, Object>();
                modelDel.put("workId", workId);
                modelDel.put("workSeq", workSeq);
                modelDel.put("workSubseq", workSubseq);

                modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                modelDel = (Map<String, Object>)dao.deleteLoc(modelDel);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelDel.get("O_MSG_CODE")), (String)modelDel.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );   
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   : updateLocAutoIn
     * Method 설명    : 입고로케이션 추천 
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateLocAutoIn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int arrCnt          = 1; //우선 1건같아서 1로함
        int lotNoLongChkCnt = 0; //CUST_LOT_NO 길이 체크
        
        try {
            String[] ordId      = new String[arrCnt];
            String[] ordSeq     = new String[arrCnt];
            String[] ritemId    = new String[arrCnt];
            String[] no         = new String[arrCnt];
            String[] custLotNo  = new String[arrCnt];

            String[] workDt     = new String[arrCnt];
            String[] makeDt     = new String[arrCnt];
            String[] uomId      = new String[arrCnt];
            String[] ordQty     = new String[arrCnt];
            String[] workQty    = new String[arrCnt];

            String[] realPltQty = new String[arrCnt];
            String[] realBoxQty = new String[arrCnt];
            String[] itemStat   = new String[arrCnt];
            String[] workStat   = new String[arrCnt];
            String[] unitAmt    = new String[arrCnt];
            
            String[] eaCapa     = new String[arrCnt];

            String[] itemDateEnd = new String[arrCnt];
            String[] ownerCd = new String[arrCnt];
            
            for (int i = 0; i < arrCnt; i++) {
            	// ordId[i] = (String)model.get("ordId" + i);    
            	// 화면에서 뒤에 번호붙이면 이렇게 처리해야됨
                // 지금은 한건이라고 생각해서 보냈으니 i를 빼도록하겠음 (for문은 그대로두겠음)
            	
                ordId[i]        = (String)model.get("ordId");
                ordSeq[i]       = (String)model.get("ordSeq");
                ritemId[i]      = (String)model.get("ritemId");
                no[i]           = (String)model.get("no");
                custLotNo[i]    = (String)model.get("custLotNo");
                
                workDt[i]       = (String)model.get("workDt");
                makeDt[i]       = (String)model.get("makeDt");
                uomId[i]        = (String)model.get("uomId");
                ordQty[i]       = (String)model.get("ordQty");
                workQty[i]      = (String)model.get("workQty");
                
                realPltQty[i]   = (String)model.get("realPltQty");
                realBoxQty[i]   = (String)model.get("realBoxQty");
                itemStat[i]     = (String)model.get("itemStat");
                workStat[i]     = (String)model.get("workStat");
                unitAmt[i]      = (String)model.get("unitAmt");
                
                eaCapa[i]       = (String)model.get("eaCapa");
                itemDateEnd[i]  = (String)model.get("itemDateEnd");
                ownerCd[i]  = (String)model.get("ownerCd");
                
                if(model.get("custLotNo").toString().length() > 20){
                	lotNoLongChkCnt++;
                }
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId"        , ordId     );
            modelIns.put("ordSeq"       , ordSeq    );
            modelIns.put("ritemId"      , ritemId   );
            modelIns.put("no"           , no        );
            modelIns.put("custLotNo"    , custLotNo );

            modelIns.put("workDt"       , workDt    );
            modelIns.put("makeDt"       , makeDt    );
            modelIns.put("uomId"        , uomId     );
            modelIns.put("ordQty"       , ordQty    );
            modelIns.put("workQty"      , workQty   );

            modelIns.put("realPltQty"   , realPltQty);
            modelIns.put("realBoxQty"   , realBoxQty);
            modelIns.put("itemStat"     , itemStat  );
            modelIns.put("workStat"     , workStat  );
            modelIns.put("unitAmt"      , unitAmt   );
            
            modelIns.put("eaCapa"       , eaCapa    );
            modelIns.put("itemDateEnd"  , itemDateEnd);
            modelIns.put("ownerCd"  	, ownerCd);
            
            // session 및 필요정보
            modelIns.put("vrLocKeepYn"  , model.get("vrlocKeepYn")  );
            modelIns.put("vrPutawayType", model.get("vrPutawayType"));
            modelIns.put("vrStartLoc"   , model.get("vrStartLoc")   );
            modelIns.put("vrEndLoc"     , model.get("vrEndLoc")     );
            
            modelIns.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO)   );
            modelIns.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO)  );

            // dao
            if(lotNoLongChkCnt > 0){
            	modelIns = (Map<String, Object>)dao.locAutoInLoc_2048(modelIns);
            }else{
            	modelIns = (Map<String, Object>)dao.locAutoInLoc(modelIns);
            }
            
            ServiceUtil.isValidReturnCode("WMSST001", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            m.put("O_REF", modelIns.get("O_REF"));
            m.put("O_LOC", modelIns.get("O_LOC"));
            m.put("errCnt", 0);
            
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
  
    /**
     * Method ID   : updateLocAutoOut
     * Method 설명    : 출고로케이션 추천 
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateLocAutoOut(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int arrCnt          = 1; //우선 1건같아서 1로함
        int lotNoLongChkCnt = 0; //CUST_LOT_NO 길이 체크
        try {
            String[] ordId      = new String[arrCnt];
            String[] ordSeq     = new String[arrCnt];
            String[] ritemId    = new String[arrCnt];
            String[] custLotNo  = new String[arrCnt];
            String[] workDt     = new String[arrCnt];
            
            String[] makeDt     = new String[arrCnt];
            String[] periodDay  = new String[arrCnt];
            String[] uomId      = new String[arrCnt];
            String[] ordQty     = new String[arrCnt];            
            String[] workQty    = new String[arrCnt];
            
            String[] realPltQty = new String[arrCnt];
            String[] realBoxQty = new String[arrCnt];
            String[] itemStat   = new String[arrCnt];
            String[] workStat   = new String[arrCnt];
            
            for (int i = 0; i < arrCnt; i++) {
//                    ordId[i] = (String)model.get("ordId" + i);    // 화면에서 뒤에 번호붙이면 이렇게 처리해야됨
                //지금은 한건이라고 생각해서 보냈으니 i를 빼도록하겠음 (for문은 그대로두겠음)
                ordId[i]        = (String)model.get("ordId");
                ordSeq[i]       = (String)model.get("ordSeq");
                ritemId[i]      = (String)model.get("ritemId");
                custLotNo[i]    = (String)model.get("custLotNo");                
                workDt[i]       = (String)model.get("workDt");
                
                makeDt[i]       = (String)model.get("makeDt");
                periodDay[i]    = (String)model.get("periodDay");
                uomId[i]        = (String)model.get("uomId");
                ordQty[i]       = (String)model.get("ordQty");                
                workQty[i]      = (String)model.get("workQty");
                
                realPltQty[i]   = (String)model.get("realPltQty");
                realBoxQty[i]   = (String)model.get("realBoxQty");
                itemStat[i]     = (String)model.get("itemStat");
                workStat[i]     = (String)model.get("workStat");
                                
                if(model.get("custLotNo").toString().length() > 20){
                	lotNoLongChkCnt++;
                }
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId"        , ordId     );
            modelIns.put("ordSeq"       , ordSeq    );
            modelIns.put("ritemId"      , ritemId   );
            modelIns.put("custLotNo"    , custLotNo );
            modelIns.put("workDt"       , workDt    );
            
            modelIns.put("makeDt"       , makeDt    );
            modelIns.put("periodDay"    , periodDay );
            modelIns.put("uomId"        , uomId     );
            modelIns.put("ordQty"       , ordQty    );            
            modelIns.put("workQty"      , workQty   );
            
            modelIns.put("realPltQty"   , realPltQty);
            modelIns.put("realBoxQty"   , realBoxQty);
            modelIns.put("itemStat"     , itemStat  );
            modelIns.put("workStat"     , workStat  );
            
            // session 및 필요정보
            modelIns.put("vrPickingType"    , model.get("vrPickingType"));
            modelIns.put("vrStartLoc"       , model.get("vrStartLoc")   );
            modelIns.put("vrEndLoc"         , model.get("vrEndLoc")     );
            
            modelIns.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO)   );
            modelIns.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO)  );

            // dao
            if(lotNoLongChkCnt > 0){
            	modelIns = (Map<String, Object>)dao.locAutoOutLoc_2048(modelIns);
            }else{
            	modelIns = (Map<String, Object>)dao.locAutoOutLoc(modelIns);
            }
            
            ServiceUtil.isValidReturnCode("WMSST001", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("O_REF", modelIns.get("O_REF"));
            m.put("O_LOC", modelIns.get("O_LOC"));
            m.put("errCnt", 0);
            
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }    
    
    /**
     *  Method ID : saveLocChangeWorkQty 
     *  Method 설명 : 입고수량변경
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveLocChangeWorkQty(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try {
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if (tmpCnt > 0) {
                // 저장, 수정
                String[] workId = new String[tmpCnt];
                String[] workSeq = new String[tmpCnt];
                String[] workQty = new String[tmpCnt];
                for (int i = 0; i < tmpCnt; i++) {
                    workId[i] = (String)model.get("WORK_ID" + i);
                    workSeq[i] = (String)model.get("WORK_SEQ" + i);
                    workQty[i] = (String)model.get("WORK_QTY" + i);
                }
                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();

                modelIns.put("workId", workId);
                modelIns.put("workSeq", workSeq);
                modelIns.put("workQty", workQty);
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                // dao
                modelIns = (Map<String, Object>)dao.saveLocChangeWorkQty(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveSetWorkDate 
     * Method 설명 : 작업일 변경 
     * 작성자 : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSetWorkDate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = "";
        
        try {
             String[] workId = model.get("vrWorkId").toString().split(",");
             
             String vrWorkDt = model.get("vrWorkDt").toString().replace("-", "");
             String[] workDt = new String[workId.length];
             
             String vrReason1 = model.get("vrReason1").toString();
             String[] reason1 = new String[workId.length];
             
             
             String vrReason2 = model.get("vrReason2").toString();
             String[] reason2 = new String[workId.length];
             
             for(int i = 0 ; i < workDt.length ; i++){
                 workDt[i] = vrWorkDt;               
                 reason1[i] = vrReason1;
                 reason2[i] = vrReason2;
             }
             
             Map<String, Object> modelIns = new HashMap<String, Object>();

             modelIns.put("workId", workId);
             modelIns.put("workDt", workDt);
             
             modelIns.put("reason1", reason1);
             modelIns.put("reason2", reason2);

             // session 및 등록정보
             modelIns.put("LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
             modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
             modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
    
            // dao 
//            modelIns = (Map<String, Object>)dao.saveSetWorkDate(modelIns);
//            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
             
            //dao 날짜변경시 변경사유 프로시저
            modelIns = (Map<String, Object>)dao.saveSetWorkDateMemo(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
           
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );            
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   : setLocAutoInLoc
     * Method 설명    : 입고로케이션 추천 2
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateSetLocAutoIn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int arrCnt = 1; //우선 1건같아서 1로함
        
        try {
            String[] ordId      = new String[arrCnt];
            String[] ordSeq     = new String[arrCnt];
            String[] ritemId    = new String[arrCnt];
            String[] no         = new String[arrCnt];
            String[] custLotNo  = new String[arrCnt];

            String[] workDt     = new String[arrCnt];
            String[] makeDt     = new String[arrCnt];
            String[] uomId      = new String[arrCnt];
            String[] ordQty     = new String[arrCnt];
            String[] workQty    = new String[arrCnt];

            String[] realPltQty = new String[arrCnt];
            String[] realBoxQty = new String[arrCnt];
            String[] itemStat   = new String[arrCnt];
            String[] workStat   = new String[arrCnt];
            String[] unitAmt    = new String[arrCnt];
            
            String[] eaCapa     = new String[arrCnt];
            
            String[] itemDateEnd = new String[arrCnt];
            
            for (int i = 0; i < arrCnt; i++) {
//                    ordId[i] = (String)model.get("ordId" + i);    // 화면에서 뒤에 번호붙이면 이렇게 처리해야됨
                //지금은 한건이라고 생각해서 보냈으니 i를 빼도록하겠음 (for문은 그대로두겠음)
                ordId[i]        = (String)model.get("ordId");
                ordSeq[i]       = (String)model.get("ordSeq");
                ritemId[i]      = (String)model.get("ritemId");
                no[i]           = (String)model.get("no");
                custLotNo[i]    = (String)model.get("custLotNo");
                
                workDt[i]       = (String)model.get("workDt");
                makeDt[i]       = (String)model.get("makeDt");
                uomId[i]        = (String)model.get("uomId");
                ordQty[i]       = (String)model.get("ordQty");
                workQty[i]      = (String)model.get("workQty");
                
                realPltQty[i]   = (String)model.get("realPltQty");
                realBoxQty[i]   = (String)model.get("realBoxQty");
                itemStat[i]     = (String)model.get("itemStat");
                workStat[i]     = (String)model.get("workStat");
                unitAmt[i]      = (String)model.get("unitAmt");
                
                eaCapa[i]       = (String)model.get("eaCapa");
                itemDateEnd[i]  = (String)model.get("itemDateEnd");
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId"        , ordId     );
            modelIns.put("ordSeq"       , ordSeq    );
            modelIns.put("ritemId"      , ritemId   );
            modelIns.put("no"           , no        );
            modelIns.put("custLotNo"    , custLotNo );

            modelIns.put("workDt"       , workDt    );
            modelIns.put("makeDt"       , makeDt    );
            modelIns.put("uomId"        , uomId     );
            modelIns.put("ordQty"       , ordQty    );
            modelIns.put("workQty"      , workQty   );

            modelIns.put("realPltQty"   , realPltQty);
            modelIns.put("realBoxQty"   , realBoxQty);
            modelIns.put("itemStat"     , itemStat  );
            modelIns.put("workStat"     , workStat  );
            modelIns.put("unitAmt"      , unitAmt   );
            
            modelIns.put("eaCapa"       , eaCapa    );
            modelIns.put("itemDateEnd"  , itemDateEnd);
            
            // session 및 필요정보
            modelIns.put("vrLocKeepYn"  , model.get("vrlocKeepYn")  );
            modelIns.put("vrPutawayType", model.get("vrPutawayType"));
            modelIns.put("vrStartLoc"   , model.get("vrStartLoc")   );
            modelIns.put("vrEndLoc"     , model.get("vrEndLoc")     );
            
            modelIns.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO)   );
            modelIns.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO)  );

            // dao
            modelIns = (Map<String, Object>)dao.setLocAutoInLoc(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));            

            m.put("O_REF", modelIns.get("O_REF"));
            m.put("O_LOC", modelIns.get("O_LOC"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
  
    /**
     * Method ID   : updateSetLocAutoOut
     * Method 설명    : 출고로케이션 추천 2
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateSetLocAutoOut(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // int errCnt = 0;
        // String errMsg = "";
        int arrCnt = 1; //우선 1건같아서 1로함
        
        try {
            String[] ordId      = new String[arrCnt];
            String[] ordSeq     = new String[arrCnt];
            String[] ritemId    = new String[arrCnt];
            String[] custLotNo  = new String[arrCnt];
            String[] workDt     = new String[arrCnt];
            
            String[] makeDt     = new String[arrCnt];
            String[] periodDay  = new String[arrCnt];
            String[] uomId      = new String[arrCnt];
            String[] ordQty     = new String[arrCnt];            
            String[] workQty    = new String[arrCnt];
            
            String[] realPltQty = new String[arrCnt];
            String[] realBoxQty = new String[arrCnt];
            String[] itemStat   = new String[arrCnt];
            String[] workStat   = new String[arrCnt];
            
            for (int i = 0; i < arrCnt; i++) {
//                    ordId[i] = (String)model.get("ordId" + i);    // 화면에서 뒤에 번호붙이면 이렇게 처리해야됨
                //지금은 한건이라고 생각해서 보냈으니 i를 빼도록하겠음 (for문은 그대로두겠음)
                ordId[i]        = (String)model.get("ordId");
                ordSeq[i]       = (String)model.get("ordSeq");
                ritemId[i]      = (String)model.get("ritemId");
                custLotNo[i]    = (String)model.get("custLotNo");                
                workDt[i]       = (String)model.get("workDt");
                
                makeDt[i]       = (String)model.get("makeDt");
                periodDay[i]    = (String)model.get("periodDay");
                uomId[i]        = (String)model.get("uomId");
                ordQty[i]       = (String)model.get("ordQty");                
                workQty[i]      = (String)model.get("workQty");
                
                realPltQty[i]   = (String)model.get("realPltQty");
                realBoxQty[i]   = (String)model.get("realBoxQty");
                itemStat[i]     = (String)model.get("itemStat");
                workStat[i]     = (String)model.get("workStat");
            }
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("ordId"        , ordId     );
            modelIns.put("ordSeq"       , ordSeq    );
            modelIns.put("ritemId"      , ritemId   );
            modelIns.put("custLotNo"    , custLotNo );
            modelIns.put("workDt"       , workDt    );
            
            modelIns.put("makeDt"       , makeDt    );
            modelIns.put("periodDay"    , periodDay );
            modelIns.put("uomId"        , uomId     );
            modelIns.put("ordQty"       , ordQty    );            
            modelIns.put("workQty"      , workQty   );
            
            modelIns.put("realPltQty"   , realPltQty);
            modelIns.put("realBoxQty"   , realBoxQty);
            modelIns.put("itemStat"     , itemStat  );
            modelIns.put("workStat"     , workStat  );
            
            // session 및 필요정보
            modelIns.put("vrPickingType"    , model.get("vrPickingType"));
            modelIns.put("vrStartLoc"       , model.get("vrStartLoc")   );
            modelIns.put("vrEndLoc"         , model.get("vrEndLoc")     );
            
            modelIns.put("LC_ID"    , (String)model.get(ConstantIF.SS_SVC_NO)   );
            modelIns.put("WORK_IP"  , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO"  , (String)model.get(ConstantIF.SS_USER_NO)  );

            // dao
            modelIns = (Map<String, Object>)dao.setLocAutoOutLoc(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            m.put("O_REF", modelIns.get("O_REF"));
            m.put("O_LOC", modelIns.get("O_LOC"));
            m.put("errCnt", 0);
            
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }      
    
    
    
    /**
     *  Method ID : listTransporDsp 
     *  Method 설명 : 운송사 거래처 조회  
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listTransporDsp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.listTransporDsp(model));
        return map;
    }
    
    
    /**
     *  Method ID : listTransCustIdDsp 
     *  Method 설명 : 운송사 설정 내역 조회  
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listTransCustIdDsp(Map<String, Object> model) throws Exception {
    	 Map<String, Object> map = new HashMap<String, Object>();
         if (model.get("page") == null) {
             model.put("pageIndex", "1");
         } else {
             model.put("pageIndex", model.get("page"));
         }
         if (model.get("rows") == null) {
             model.put("pageSize", "20");
         } else {
             model.put("pageSize", model.get("rows"));
         }

         map.put("LIST", dao.listTransCustIdDsp(model));
        return map;
    }
    
    
    
    
    /**
     *  Method ID : saveTransDsp 
     *  Method 설명 : 운송사 내역(저장,수정,삭제) 
     * 작성자 : chsong
     * 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveTransDsp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
       
        try {

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            
            if (insCnt > 0) { // (신규, 기존) 수정
                for (int i = 0; i < insCnt; i++) {
					String ordId = (String)model.get("I_ORD_ID" + i);
					String transCustId = (String)model.get("I_TRANS_CUST_ID" + i);
					String transCustCd =(String)model.get("I_TRANS_CUST_CD" + i);
                	
                	 // 데이터에 보낼것들 다담는다
                    Map<String, Object> modelIns = new HashMap<String, Object>();
					modelIns.put("ORD_ID", ordId);
					modelIns.put("TRANS_CUST_ID", transCustId);
					modelIns.put("TRANS_CUST_CD", transCustCd);
                	
					//session 및 등록정보
					modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
					modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
					
					 // dao
	                modelIns = (Map<String, Object>)dao.saveTransDsp(modelIns);
                }
            }
            
            if (delCnt > 0) {// (삭제) 수정 
            	for (int i = 0; i < delCnt; i++) {
					String ordId = (String)model.get("D_ORD_ID" + i);
                	
                	 // 데이터에 보낼것들 다담는다
                    Map<String, Object> modelDel = new HashMap<String, Object>();
                    modelDel.put("ORD_ID", ordId);
                    //session 및 등록정보
                    modelDel.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                    modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                	
					 // dao
                    modelDel = (Map<String, Object>)dao.updateDelTransDsp(modelDel);
                }
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
            m.put("errCnt", -1);
            m.put("MSG", MessageResolver.getMessage("save.error"));       
            
        }
        return m;
    }
}
