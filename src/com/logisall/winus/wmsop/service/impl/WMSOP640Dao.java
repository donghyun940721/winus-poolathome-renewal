package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP640Dao")
public class WMSOP640Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID	: listByDlvSummary
     * Method 설명	: 택배주문조회
     * 작성자			: chsong
     * @param model
     * @return
     */
    public List rawListByDlvSummary(Map<String, Object> model) {
		List custs = list("wmsop640.listByDlvSummary", model);
    	return custs;
	}
    
	public Object listByDlvSummary(Map<String, Object> model) {
		List custs = rawListByDlvSummary(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
     * Method ID	: list2ByDlvHistory
     * Method 설명	: 택배이력엑셀등록조회
     * 작성자			: chsong
     * @param model
     * @return
     */
	public List rawList2ByDlvHistory(Map<String, Object> model) {
		List custs = list("wmsop640.list2ByDlvHistory", model);
    	return custs;
	}
    
	public Object list2ByDlvHistory(Map<String, Object> model) {
		List custs = rawList2ByDlvHistory(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
	 * Method ID : insertCsv Method 설명 : 대용량등록시 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void insertCsv(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				String LC_ID = (String) model.get("strLcId");
				String CUST_ID = (String) model.get("strCustId");
				
				if (paramMap.get("ORD_ID") 		!= null && StringUtils.isNotEmpty((String) paramMap.get("ORD_ID"))
    			 && paramMap.get("DLV_COMP_CD") != null && StringUtils.isNotEmpty((String) paramMap.get("DLV_COMP_CD"))
    			 && paramMap.get("INVC_NO") 	!= null && StringUtils.isNotEmpty((String) paramMap.get("INVC_NO"))
    			 && StringUtils.isNotEmpty(LC_ID)
    			 && StringUtils.isNotEmpty(CUST_ID)
    			){
					//String custId = (String) sqlMapClient.insert("wmsms010.insertCsv", paramMap);
					paramMap.put("LC_ID"	, LC_ID);
					paramMap.put("CUST_ID"	, CUST_ID);
					
					sqlMapClient.insert("wmsop640.insertCsv", paramMap);
    			}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	/**
     * Method ID  : dlvSenderInfo
     * Method 설명  : 택배배송 송화인정보
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object dlvSenderInfo(Map<String, Object> model){
        return executeQueryForList("wmsop640.dlvSenderInfo", model);
    }
    
    /**
	 * Method ID	: boxNoUpdate 
	 * Method 설명	: boxNoUpdate코드 수정 
	 * 작성자			: chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object boxNoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.boxNoUpdate", model);
	}
	
	/*-
     * Method ID : getOracleSysTimeStamp
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String getOracleSysTimeStamp(Map<String, Object> model) {
        return (String)executeView("wmsop640.getOracleSysTimeStamp", model);
    }
    
    /**
	 * Method ID : dlvPrintPoiNoUpdate 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object dlvPrintPoiNoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.dlvPrintPoiNoUpdate", model);
	}
	
	public Object invcNoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.invcNoUpdate", model);
	}
	
	public Object invcNoInfoUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.invcNoInfoUpdate", model);
	}
	
	public Object invcNoInfoDelete(Map<String, Object> model) {
		return executeUpdate("wmsop640.invcNoInfoDelete", model);
	}
	
	public List getValidData(Map<String, Object> model) {
		return executeQueryForList("wmsop640.getValidData", model);
	}
	
	/**
	 * Method ID	: wmsdf110DelYnUpdate 
	 * Method 설명	: wmsdf110DelYnUpdate코드 수정 
	 * 작성자			: chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object wmsdf110DelYnUpdate(Map<String, Object> model) {
		return executeUpdate("wmsop640.wmsdf110DelYnUpdate", model);
	}
	
	/**
     * Method ID	: getCustOrdDegree
     * Method 설명 	: 화주별 주문차수
     * @param model
     * @return
     */
    public Object getCustOrdDegree(Map<String, Object> model){
        return executeQueryForList("wmsop640.getCustOrdDegree", model);
    }
}
