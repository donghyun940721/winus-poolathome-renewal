package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP999Dao")
public class WMSOP999Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }

    /**
     * Method ID    : listOrderDetail
     * Method 설명      : 입출고관리 상세 주문 정보 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object listOrderDetail(Map<String, Object> model) {
        return executeView("wmsop010.listOrderDetail", model);
    }

    /**
     * Method ID   		: listOrderDetailOm
     * Method 설명      : 출고관리 OM 상세 조회화면
     * 작성자           : KSJ
     * @param   model
     * @return
     */
    public Object listOrderDetailOm(Map<String, Object> model) {
        return executeView("wmsop010.listOrderDetailOm", model);
    }
    
    /**
     * Method ID    : listInOrderItem
     * Method 설명      : 입고관리 상세 주문 상품 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listInOrderItem(Map<String, Object> model) {
        return executeQueryPageWq("wmsop011.listInOrderItem", model);
    }
    
    /**
     * Method ID    : saveInOrder
     * Method 설명      : 입고관리 입고주문 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveInOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_insert_order", model);
        return model;
    }
    
    /**
     * Method ID    : deleteInOrder
     * Method 설명      : 입고관리 입고주문 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deleteInOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_delete_order", model);
        return model;
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 간편입고주문 상품 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms091.selectSimpleList", model);
    }
    
    /**
     * Method ID    : saveSimpleInOrder
     * Method 설명      : 간편입고주문 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSimpleInOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_simple_insert_order", model);
        return model;
    }
    
    /**
     * Method ID    : listOutOrderItem
     * Method 설명      : 입고관리 상세 주문 상품 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listOutOrderItem(Map<String, Object> model) {
        return executeQueryPageWq("wmsop011.listOutOrderItem", model);
    }
    
    /**
     * Method ID    : listLocSearch
     * Method 설명      : 출고관리 상세 상품 로케이션조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listLocSearch(Map<String, Object> model) {
        return executeQueryPageWq("wmsst502.listLocSearch", model);
    }   
    
    /**
     * Method ID    : saveOutOrder
     * Method 설명      : 출고관리 출고주문 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOutOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_insert_order", model);
        return model;
    }
    
    
    
    /**
     * Method ID    		: saveOutOrderOm
     * Method 설명       : 출고관리 출고주문(OM) 등록,수정
     * 작성자                : KSJ
     * @param   model
     * @return
     */
    public Object saveOutOrderOm(Map<String, Object> model){
        executeUpdate("wmsop031.pk_wmsop031.sp_insert_order_2048array_om", model);
        return model;
    }
    
    
    
    /**
     * Method ID    : etcSaveOutOrder
     * Method 설명      : 아산물류센터 원주문, BL번호 출고관리 출고주문 수정
     * 작성자                 : wdy
     * @param   model
     * @return
     */
    public Object etcSaveOutOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_etc_out_update", model);
        return model;
    }
    
    /**
     * Method ID    : etcSaveInOrder
     * Method 설명      : 아산물류센터 입고일자 수정
     * 작성자                 : wdy
     * @param   model
     * @return
     */
    public Object etcSaveInOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_etc_in_update", model);
        return model;
    }
    
    /**
     * Method ID    : deleteOutOrder
     * Method 설명      : 출고관리 출고주문 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deleteOutOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_delete_order", model);
        return model;
    }
    
    /**
     * Method ID    : saveSimpleOutOrder
     * Method 설명      : 간편출고주문 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSimpleOutOrder(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_simple_insert_order", model);
        return model;
    }
    
    /**
     * Method ID    : etcSaveV2
     * Method 설명      : 아산물류센터 원주문, BL번호 출고관리 출고주문 수정
     * 작성자                 : wdy
     * @param   model
     * @return
     */
	public Object etcSaveV2(Map<String, Object> model) {
		executeUpdate("wmsop030.etc_save_v2", model);
		return model;
	}

	/**
     * Method ID    : saveInOrderV2
     * Method 설명      : 입고관리 입고주문 등록,수정V2
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object saveInOrderV2(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_insert_order_2048array", model);
        return model;
    }
}
