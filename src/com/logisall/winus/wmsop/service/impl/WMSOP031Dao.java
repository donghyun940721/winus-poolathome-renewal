package com.logisall.winus.wmsop.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP031Dao")
public class WMSOP031Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID  : selectPoolGrp
     * Method 설명  : 화면내 필요한 용기군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectPoolGrp(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 물류용기출고관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outPoolList", model);
    }
    
    /**
	 * Method ID   	: getRetryOrder
	 * Method 설명 	: 재접수 대상 주문 조회
	 * 작성자 		: KSJ
	 * 
	 * @param model
	 * @return
	 */
	public List<Map<String, Object>> getReOrderList(Map<String, Object> model) {
		return (List<Map<String, Object>>) executeQueryForList("wmsop031.getReOrderList", model);
	}
    
	/**
     * Method ID    		: saveExcelOrderB2T
     * Method 설명       	: 템플릿 주문 저장 (pk_wmsop030T)
     * 작성자              	: KSJ
     * @param   model
     * @return
     */
    public Object saveExcelOrderB2Cts(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030ts.sp_insert_template", model);
        return model;
    }
    
//    /**
//     * Method ID    : sum
//     * Method 설명      : 물류용기출고관리 합계
//     * 작성자                 : chsong
//     * @param   model
//     * @return
//     */
//    public Object sum(Map<String, Object> model) {
//        return executeView("wmsop010.outPoolListSum", model);
//    }
    
 
}
