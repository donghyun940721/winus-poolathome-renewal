package com.logisall.winus.wmsop.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.wmsop.vo.WMSOP035VO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP035Dao")
public class WMSOP035Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 출고관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop035.list", model);
    }
   
    /**
     * Method ID  : list2
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object list2(Map<String, Object> model){
        return executeQueryForList("wmsop035.list2", model);
    }
    
	public List<WMSOP035VO> pdfDown(Map<String, Object> model) {
		return executeQueryForList("wmsop035.pdfDown", model);
	}
	
	/**
     * Method ID    : outSaveComplete
     * Method 설명      : 
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object outSaveComplete(Map<String, Object> model){
        executeUpdate("pk_wmsif060.sp_ord_id_out_complete", model);
        return model;
    }

	public List searchUnshippedDHL(Map<String, Object> model) {
		List list = executeQueryForList("wmsop035.searchUnshippedDHL", model);;
		return list;
	}
    
	/**
	 * Method ID : list2DHL
	 * Method 설명 : 수출신고정보
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list2DHL(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.list2DHL", model);;
		genericResultSet.setList(list);
		return genericResultSet;
	}
	
	public List request2EMS(Map<String, Object> model) {
		List list = executeQueryForList("wmsop035.request2EMS", model);
		return list;
	}

	public List request2EMSDetail(Map<String, Object> smap) {
		List list = executeQueryForList("wmsop035.request2EMSDetail", smap);
		return list;
	}
	
	/**
	 * Method ID : saveList2DHL 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object saveList2DHL(Map<String, Object> model) {
		return executeUpdate("wmsop035.saveList2DHL", model);
	}

	public GenericResultSet list2EMS(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.list2EMS", model);;
		genericResultSet.setList(list);
		return genericResultSet;
	}

	public Map<String, Object> emsAddress(Map<String, Object> model) {
		return (Map<String, Object>)executeQueryForObject("wmsop035.emsAddress", model);
	}
	
	public List addressHistory(Map<String, Object> model) {
		List list = executeQueryForList("wmsop035.addressHistory", model);;
		return list;
	}
	
	public List addressOM010(Map<String, Object> model) {
		List list = executeQueryForList("wmsop035.addressOM010", model);;
		return list;
	}

	public int selectShipmentPublish(Map<String, Object> model) {
		return (int)executeView("wmsop035.selectShipmentPublish", model);
	}

	public void updateShipmentPublish(Map<String, Object> model) {
		executeUpdate("wmsop035.updateShipmentPublish", model);
		
	}

	public void insertShipmentPublish(Map<String, Object> model) {
		executeUpdate("wmsop035.insertShipmentPublish", model);
		
	}

	public Object export2EMS(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.export2EMS", model);;
		genericResultSet.setList(list);
		return genericResultSet;
	}

	public Object saveExport2EMS(List list) {
		return executeInsert("wmsop035.saveExport2EMS", list);
		
	}

	public String getOrdId(String orgOrdId) {
		return (String)executeView("wmsop035.getOrdId", orgOrdId);
	}

	public List<Map> checkBlNo(String obj) {
		return executeQueryForList("wmsop035.checkBlNo", obj);
	}

	public void updateEmsBlNo(Map<String, Object> obj) {
		executeUpdate("wmsop035.updateEmsBlNo", obj);
		
	}

	public void updateDhlBlNo(Map<String, Object> obj) {
		executeUpdate("wmsop035.updateDhlBlNo", obj);
		
	}

	public void updateCommonBlNo(Map<String, Object> obj) {
		executeUpdate("wmsop035.updateCommonBlNo", obj);
		
	}
	public void deleteEmsTracking(Map<String, Object> obj) {
		executeUpdate("wmsop035.deleteEmsTracking", obj);
		
	}
	
	public void deleteDhlTracking(Map<String, Object> obj) {
		executeUpdate("wmsop035.deleteDhlTracking", obj);
		
	}
	
	public void deleteCommonTracking(Map<String, Object> obj) {
		executeUpdate("wmsop035.deleteCommonTracking", obj);
		
	}
	
	 /**
     * Method ID  : listE5
     * Method 설명  : 올리브영 수출신고번호 or 운송장번호 누락 조회
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public GenericResultSet listE5(Map<String, Object> model){
    	if(model.get("vrSrchShippingCompany5").equals("10")){
    		return executeQueryPageWq("wmsop035.listE5EMS", model);
    	}else{
    		return executeQueryPageWq("wmsop035.listE5DHL", model);	
    	}
    }
    
    /**
     * Method ID  : listE6EMS
     * Method 설명  : 올리브영 운송장 출력 여부 조회 (EMS)
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public Object listE6EMS(Map<String, Object> model){
    	GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.listE6EMS", model);;
		genericResultSet.setList(list);
		return genericResultSet;
    }
    
    /**
     * Method ID  : listE6DHL
     * Method 설명  : 올리브영 운송장 출력 여부 조회 (DHL)
     * 작성자             : KSJ
     * @param model
     * @return
     */
    public Object listE6DHL(Map<String, Object> model){
    	GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsop035.listE6DHL", model);;
		genericResultSet.setList(list);
		return genericResultSet;
    }
    
    /**
     * Method ID  	: selectPrintDt
     * Method 설명  	: 올리브영 운송장 출력 시간 조회
     * 작성자            : KSJ
     * @param model
     * @return
     */
    public Object selectPrintDt(Map<String, Object> model){
		return executeQueryForObject("wmsop035.selectPrintDt", model);
    }
    
    /**
     * Method ID  	 : updatetPrintDt
     * Method 설명  	 : 올리브영 운송장 출력 시간 업데이트 
     * 작성자             : KSJ
     * @param model
     * @return
     */
	public Object updatetPrintDt(Map<String, Object> model) {
		executeUpdate("wmsop035.updatePrintDt", model);
		
		if(model.get("shippingCompany").equals("10")){
			executeUpdate("wmsop035.updatePrintDtEms", model);	
		}else{
			executeUpdate("wmsop035.updatePrintDtDhl", model);	
		}
		
		return model;
	}
}
