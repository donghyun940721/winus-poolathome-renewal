package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.components.headertoolbar.actions.MoveElementCommand;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.ctc.wstx.util.StringUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP640Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;










/*SF*/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONObject;
import org.json.XML;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.OrderWebService;

@Service("WMSOP640Service")
public class WMSOP640ServiceImpl implements WMSOP640Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP640Dao")
    private WMSOP640Dao dao;
    
    /**
     * Method ID	: selectItemGrp
     * Method 설명	: 출고관리 화면에서 필요한 데이터
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * Method ID	: listByDlvSummary
     * Method 설명	: 택배접수조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByDlvSummary(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByDlvSummary(model));
        return map;
    }
    
    /**
     * Method ID	: list2ByDlvHistory
     * Method 설명	: 택배접수조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2ByDlvHistory(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.list2ByDlvHistory(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: DlvShipment
     * 대체 Method 설명	: 택배접수
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvShipment(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                try{
            		//이미 송장번호가 있으면 CJ접수 시 삭제 처리 (DEL_YN = Y)
            		if (model.get("INVC_NO"+i).toString().length() != 0 && !StringUtils.isEmpty((String)model.get("INVC_NO"+i))){
            			System.out.println("INVC_NO가 NULL이 아님");
            			
            			Map<String, Object> modelDel = new HashMap<String, Object>();
            			System.out.println("ORD_ID:"+model.get("ORD_ID"+i));
            			System.out.println("ORD_SEQ:"+model.get("ORD_SEQ"+i));
            			System.out.println("INVC_NO:"+model.get("INVC_NO"+i));
            			
            			modelDel.put("ORD_ID"    	, model.get("ORD_ID"+i));
            			modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
            			modelDel.put("INVC_NO"		, model.get("INVC_NO"+i));
            			modelDel.put("DLV_COMP_CD"	, "04");
                        
                        dao.wmsdf110DelYnUpdate(modelDel);
            		}
                } catch(Exception e){
                    throw e;
                }
            }
        	
        	/*
        	System.out.println("PARCEL_SEQ_YN:"+model.get("PARCEL_SEQ_YN").toString());
    		System.out.println("PARCEL_COM_TY:"+model.get("PARCEL_COM_TY").toString());
    		System.out.println("PARCEL_COM_TY_SEQ:"+model.get("PARCEL_COM_TY_SEQ").toString());
    		System.out.println("PARCEL_ORD_TY:"+model.get("PARCEL_ORD_TY").toString());
    		System.out.println("PARCEL_PAY_TY:"+model.get("PARCEL_PAY_TY").toString());
    		System.out.println("PARCEL_BOX_TY:"+model.get("PARCEL_BOX_TY").toString());
    		System.out.println("PARCEL_ETC_TY:"+model.get("PARCEL_ETC_TY").toString());
    		System.out.println("FR_DATE:"+model.get("FR_DATE").toString());
    		System.out.println("TO_DATE:"+model.get("TO_DATE").toString());
    		System.out.println("////////////////////////////////////////////////");
    		*/
        	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                String sUrl = "https://172.31.10.253:5201/PARCEL/CJLOGIS/ORDER";
                
                try{
                	//PROC_FLAG = 1 일 경우만  : (고객명/주소/전화번호가 동일 한 1row만 처리) 
                	if("1".equals(model.get("PROC_FLAG"+i))){
                		/*
                		System.out.println("PROC_FLAG:"+(String)model.get("PROC_FLAG"+i));
                    	System.out.println("LC_ID:"+(String)model.get("LC_ID"+i));
                    	System.out.println("CUST_ID:"+(String)model.get("CUST_ID"+i));
                    	System.out.println("RCVR_NM:"+(String)model.get("RCVR_NM"+i));
                    	System.out.println("RCVR_ADDR:"+(String)model.get("RCVR_ADDR"+i));
                    	System.out.println("RCVR_DETAIL_ADDR:"+(String)model.get("RCVR_DETAIL_ADDR"+i));
                    	System.out.println("==========================================");
                    	*/
                		
                    	//Json Data
            			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
    				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
    				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
            									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
            									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
            									+					",\"PARCEL_BOX_TY\":\""		+model.get("PARCEL_BOX_TY").toString()+"\""
            									+					",\"PARCEL_ETC_TY\":\""		+model.get("PARCEL_ETC_TY").toString()+"\""
            									+					",\"FR_DATE\":\""			+model.get("FR_DATE").toString()+"\""
            									+					",\"TO_DATE\":\""			+model.get("TO_DATE").toString()+"\""
            									
            									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
            									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
            									+					",\"RCVR_NM\":\""			+(String)model.get("RCVR_NM"+i)+"\""
            									+					",\"RCVR_ADDR\":\""			+(String)model.get("RCVR_ADDR"+i)+"\""
            									+					",\"RCVR_DETAIL_ADDR\":\""	+(String)model.get("RCVR_DETAIL_ADDR"+i)+"\"}}";
            			//System.out.println(jsonInputString);
            			
                    	//ssl disable
                    	disableSslVerification();
            	        //System.out.println("sUrl : " + sUrl);
            	        
            	        URL url = null; 
            	        url = new URL(sUrl);
            	        
            	        HttpsURLConnection con = null;
                    	con = (HttpsURLConnection) url.openConnection();
                    	
                    	//웹페이지 로그인 권한 적용
                    	String userpass = "eaiuser01" + ":" + "eaiuser01";
                    	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

                    	con.setRequestProperty ("Authorization", basicAuth);
                    	con.setDoInput(true);
                    	con.setDoOutput(true);
                    	con.setRequestMethod("POST");
                    	con.setConnectTimeout(0);
                    	con.setReadTimeout(0);
                    	con.setRequestProperty("Content-Type", "application/json");
            			con.setRequestProperty("Accept", "application/json");
            			
            			//JSON 보내는 Output stream
            	        try(OutputStream os = con.getOutputStream()) {
            	            byte[] input = jsonInputString.getBytes("utf-8");
            	            os.write(input, 0, input.length);
            	        }
            	        
            	        //Response data 받는 부분
            	        
            	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            	            StringBuilder response = new StringBuilder();
            	            String responseLine = null;
            	            while ((responseLine = br.readLine()) != null) {
            	                response.append(responseLine.trim());
            	            }
            	            
            	            //System.out.println(response.toString());
            	            
            	            JSONObject jsonData = new JSONObject(response.toString());
            	            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
            	            m.put("header"	, docResponse.get("header")); // Y, E
            	            m.put("message"	, docResponse.get("message"));// msg
            	            
                            //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
                            //m.put("rst"	, response.toString());
            	        }
                    	con.disconnect();
                	}
                	
                } catch(Exception e){
                    throw e;
                }
            }
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID	: disableSslVerification
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	/**
     * Method ID : saveCsv
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCsv(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.insertCsv(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
    
    /*-
	 * Method ID   : dlvSenderInfo
	 * Method 설명 : 택배배송 송화인정보
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> dlvSenderInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DLV_SENDER_INFO", dao.dlvSenderInfo(model));
		return map;
	}
    
    /**
     * 
     * 대체 Method ID		: boxNoUpdate
     * 대체 Method 설명		: boxNoUpdate코드 저장
     * 작성자				: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> boxNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("ORD_ID"    	, model.get("ORD_ID"));
            modelDt.put("ORD_SEQ"    	, model.get("ORD_SEQ"));
            modelDt.put("TRACKING_NO"	, model.get("TRACKING_NO"));
            
            dao.boxNoUpdate(modelDt);
            m.put("errCnt", "0");
            
        } catch(Exception e){
        	m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    public Map<String, Object> dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelSP = new HashMap<String, Object>();
        
        String getTimeStamp = dao.getOracleSysTimeStamp(modelSP);
		if (getTimeStamp != null && !StringUtils.isEmpty(getTimeStamp)) {
			try{
	            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	                Map<String, Object> modelDt = new HashMap<String, Object>();
	                modelDt.put("POI_NO"	, getTimeStamp);
	                modelDt.put("ORD_ID"	, model.get("ORD_ID"+i));
	                modelDt.put("ORD_SEQ"	, model.get("ORD_SEQ"+i));
	                
	                dao.dlvPrintPoiNoUpdate(modelDt);
	            }
	            
	            m.put("MSG"			, MessageResolver.getMessage("save.success"));
	            m.put("POI_NO_YN"	, "Y");
	            m.put("POI_NO"		, getTimeStamp);
	            
	        } catch(Exception e){
	        	m.put("MSG"			, MessageResolver.getMessage("save.error"));
	            m.put("POI_NO_YN"	, "N");
	            m.put("POI_NO"		, "");
	            throw e;
	        }
        }
        return m;
    }
    
    

    @Override
    public Map<String, Object> invcNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                //WHERE 조건
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("LC_ID"     , model.get("LC_ID"+i));
                modelDt.put("CUST_ID"    , model.get("CUST_ID"+i));
                
                modelDt.put("ORD_ID"    	  , model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"     	  , model.get("ORD_SEQ"+i));
                modelDt.put("DLV_COMP_CD"     , model.get("DLV_COMP_CD"+i));
                modelDt.put("ORG_INVC_NO"     , model.get("ORG_INVC_NO"+i));
                modelDt.put("SWEET_TRACKER_IF_YN"     	, model.get("SWEET_TRACKER_IF_YN"+i));
                
                //UPDATE 조건
                modelDt.put("INVC_NO"     	  , model.get("INVC_NO"+i));
                
                
//                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS100);
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.invcNoUpdate(modelDt);                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    @Override
    public Map<String, Object> invcNoInfoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        String strGubun = "Y";        
        
        try{
        	// Element Validation 
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	
            	Map<String, Object> modelDt = new HashMap<String, Object>();
            	
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                //WHERE 조건
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("LC_ID"     , model.get("LC_ID"+i));
                modelDt.put("CUST_ID"    , model.get("CUST_ID"+i));
                
                modelDt.put("ORD_ID"    	  			, model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"     	  			, model.get("ORD_SEQ"+i));
                modelDt.put("DLV_COMP_CD"     			, model.get("DLV_COMP_CD"+i));
                modelDt.put("SWEET_TRACKER_DLV_CD"      , model.get("SWEET_TRACKER_DLV_CD"+i));
                modelDt.put("SWEET_TRACKER_IF_YN"     	, model.get("SWEET_TRACKER_IF_YN"+i));
                modelDt.put("ORG_INVC_NO"    			, model.get("ORG_INVC_NO"+i));
                modelDt.put("ORG_SWEET_TRACKER_DLV_CD"  , model.get("ORG_SWEET_TRACKER_DLV_CD"+i));
                
                //UPDATE 조건
                modelDt.put("INVC_NO"     	  , model.get("INVC_NO"+i));
                                
//                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS100);
                
                // ## KCC 실적전송 여부확인 (실적있는 경우 삭제/수정 불가), 화주 하드코딩 (0000002940) ##
                if(model.get("LC_ID"+i) != null && "0000002940".equals(model.get("LC_ID"+i).toString()))
                {
                	String validMsg = validData(modelDt);
                	if(validMsg != null && !validMsg.isEmpty())
                	{
                		m.put("errCnt", '1');
                        m.put("MSG", validMsg);
                        return m;
                	}
                }
            }            
   
            // Save Data (Update / Delete)
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	
            	Map<String, Object> modelDt = new HashMap<String, Object>();
            	
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                //WHERE 조건
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("LC_ID"     , model.get("LC_ID"+i));
                modelDt.put("CUST_ID"    , model.get("CUST_ID"+i));
                
                modelDt.put("ORD_ID"    	  			, model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"     	  			, model.get("ORD_SEQ"+i));
                modelDt.put("DLV_COMP_CD"     			, model.get("DLV_COMP_CD"+i));
                modelDt.put("SWEET_TRACKER_DLV_CD"      , model.get("SWEET_TRACKER_DLV_CD"+i));
                modelDt.put("SWEET_TRACKER_IF_YN"     	, model.get("SWEET_TRACKER_IF_YN"+i));
                modelDt.put("ORG_INVC_NO"    			, model.get("ORG_INVC_NO"+i));
                modelDt.put("ORG_SWEET_TRACKER_DLV_CD"  , model.get("ORG_SWEET_TRACKER_DLV_CD"+i));
                
                //UPDATE 조건
                modelDt.put("INVC_NO"     	  , model.get("INVC_NO"+i));
                
            	if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.invcNoInfoUpdate(modelDt);                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.invcNoInfoDelete(modelDt);                  
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }            
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    private String validData(Map<String, Object> model)
    {    	
    	String validMsg = "";
    	
    	ObjectMapper objectMapper = new ObjectMapper();
    	List data= dao.getValidData(model);    	
    	
    	validLoop:
    	for(Object dt : data)
    	{
    		Map dr = objectMapper.convertValue(dt, Map.class);    		    		
    		
    		if(("Y").equals(dr.get("CUSTOMER_IF_YN").toString())
    				|| ("Y").equals(dr.get("CUSTOMER_IF_YN_SUB").toString()))
    		{
    			validMsg = dr.get("INVOICE_NO").toString();
    			break validLoop;
    		}    		
    		
    	}
    
    	
    	return validMsg.isEmpty() ? null : String.format("Invalid Data (INVOICE_NO) : %s", validMsg) ;
    }
    
    
    /*-
  	 * Method ID	: getCustOrdDegree
  	 * Method 설명	: 화주별 주문차수
  	 * @param 
  	 * @return
  	 */
      public Map<String, Object> getCustOrdDegree(Map<String, Object> model) throws Exception {
  		Map<String, Object> map = new HashMap<String, Object>();
  		map.put("DS_ORD_DEGREE", dao.getCustOrdDegree(model));
  		return map;
  	}
      
}