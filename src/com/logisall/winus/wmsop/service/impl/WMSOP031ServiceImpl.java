package com.logisall.winus.wmsop.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsop.service.WMSOP031Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSOP031Service")
public class WMSOP031ServiceImpl implements WMSOP031Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP031Dao")
    private WMSOP031Dao dao;
    
    /**
     * Method ID   : selectPoolGrp
     * Method 설명    : 물류용기출고관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectPoolGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("POOLGRP", dao.selectPoolGrp(model));
        return map;
    }
    
    /**
     * Method ID   : list
     * Method 설명    : 출고관리  조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID    	: retryOutboundOrder
     * 대체 Method 설명     : B2C 출고 주문 재접수 
     * 작성자            	: KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> outBoundReOrder(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	List<Map<String, Object>> reOrderList= new ArrayList();
    	reOrderList = dao.getReOrderList(model);

    	Date today = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
    	String formatted_today = sdf.format(today);
    	
    	int insCnt = reOrderList.size();

        try{
            if(insCnt > 0){
                String[] no             = new String[insCnt];     
                
                String[] outReqDt       = new String[insCnt];         
                String[] inReqDt        = new String[insCnt];     
                String[] custOrdNo      = new String[insCnt];     
                String[] custOrdSeq     = new String[insCnt];    
                String[] trustCustCd    = new String[insCnt];     
                
                String[] transCustCd    = new String[insCnt];                     
                String[] transCustTel   = new String[insCnt];         
                String[] transReqDt     = new String[insCnt];     
                String[] custCd         = new String[insCnt];
                String[] ordQty         = new String[insCnt];     
                
                String[] uomCd          = new String[insCnt];                
                String[] sdeptCd        = new String[insCnt];         
                String[] salePerCd      = new String[insCnt];     
                String[] carCd          = new String[insCnt];     
                String[] drvNm          = new String[insCnt];     
                
                String[] dlvSeq         = new String[insCnt];                
                String[] drvTel         = new String[insCnt];         
                String[] custLotNo      = new String[insCnt];     
                String[] blNo           = new String[insCnt];     
                String[] recDt          = new String[insCnt];     
                
                String[] outWhCd        = new String[insCnt];                
                String[] inWhCd         = new String[insCnt];         
                String[] makeDt         = new String[insCnt];     
                String[] timePeriodDay  = new String[insCnt];     
                String[] workYn         = new String[insCnt];     
                
                String[] rjType         = new String[insCnt];                
                String[] locYn          = new String[insCnt];         
                String[] confYn         = new String[insCnt];     
                String[] eaCapa         = new String[insCnt];     
                String[] inOrdWeight    = new String[insCnt];     
                
                String[] itemCd         = new String[insCnt];                
                String[] itemNm         = new String[insCnt];         
                String[] transCustNm    = new String[insCnt];     
                String[] transCustAddr  = new String[insCnt];     
                String[] transEmpNm     = new String[insCnt];     
                
                String[] remark         = new String[insCnt];                
                String[] transZipNo     = new String[insCnt];         
                String[] etc2           = new String[insCnt];     
                String[] unitAmt        = new String[insCnt];     
                String[] transBizNo     = new String[insCnt];     
                
                String[] inCustAddr     = new String[insCnt];                
                String[] inCustCd       = new String[insCnt];         
                String[] inCustNm       = new String[insCnt];     
                String[] inCustTel      = new String[insCnt];     
                String[] inCustEmpNm    = new String[insCnt];     
                
                String[] expiryDate     = new String[insCnt];
                String[] salesCustNm    = new String[insCnt];
                String[] zip     		= new String[insCnt];
                String[] addr     		= new String[insCnt];
                String[] addr2     		= new String[insCnt];
                
                String[] phone1    	 	= new String[insCnt];
                String[] etc1    		= new String[insCnt];
                String[] unitNo    		= new String[insCnt];               
                String[] timeDate       = new String[insCnt];   //상품유효기간     
                String[] timeDateEnd    = new String[insCnt];   //상품유효기간만료일
                
                String[] timeUseEnd     = new String[insCnt];   //소비가한만료일
                String[] phone2			= new String[insCnt];   //고객전화번호2
                String[] buyCustNm		= new String[insCnt];   //주문자명
                String[] buyPhone1		= new String[insCnt];   //주문자전화번호1
                String[] salesCompanyNm		= new String[insCnt];   //salesCompanyNm
             
                String[] ordDegree		= new String[insCnt];   //주문등록차수
                String[] bizCond		= new String[insCnt];   //업태
                String[] bizType		= new String[insCnt];   //업종
                String[] bizNo			= new String[insCnt];   //사업자등록번호
                String[] custType		= new String[insCnt];   //화주타입
                
                String[] dataSenderNm		= new String[insCnt];   //쇼핑몰
                String[] legacyOrgOrdNo	= new String[insCnt];   //사방넷주문번호
//                String[] invoiceNo			= new String[listBodyCnt];   //쇼핑몰
                
                String[] custSeq 			=  new String[insCnt];   //템플릿구분
                String[] ordDesc 			=  new String[insCnt];   //ord_desc
                String[] dlvMsg1 			=  new String[insCnt];   //배송메세지1
                String[] dlvMsg2 			=  new String[insCnt];   //배송메세지2
                
                // String breakYnOrdDegree = "N";
                
                /*
                 * 2022.02.04
                 * KSJ
                 * 사용안하는 속성은 소문자로 작성함
                 */
                for(int i = 0 ; i < insCnt ; i ++){
        			String NO = Integer.toString(i+1);
                	no[i]               = NO;
                    
                    outReqDt[i]         			= (String) reOrderList.get(i).get("outReqDt");    
                    inReqDt[i]          			= (String) reOrderList.get(i).get("inReqDt");     
                    custOrdNo[i]        			= (String) "copy-" +reOrderList.get(i).get("ORG_ORD_NO");   
                    custOrdSeq[i]       			= (String) reOrderList.get(i).get("ORD_DETAIL_NO");   
                    trustCustCd[i]      			= (String) reOrderList.get(i).get("trustCustCd");      
                    
                    transCustCd[i]      			= (String) reOrderList.get(i).get("TRANS_CUST_CD");    
                    transCustTel[i]     			= (String) reOrderList.get(i).get("transCustTel");    
                    transReqDt[i]       			= (String) reOrderList.get(i).get("transReqDt");    
                    custCd[i]           			= (String) reOrderList.get(i).get("CUST_CD");
                    ordQty[i]           			= String.valueOf(reOrderList.get(i).get("ORD_QTY"));    
                    
                    uomCd[i]            			= (String) reOrderList.get(i).get("UOM_CD");     // 필요해보임
                    sdeptCd[i]          			= (String) reOrderList.get(i).get("sdeptCd");    
                    salePerCd[i]        			= (String) reOrderList.get(i).get("salePerCd");    
                    carCd[i]            			= (String) reOrderList.get(i).get("carCd");
                    drvNm[i]            			= (String) reOrderList.get(i).get("drvNm");
                    
                    dlvSeq[i]           			= (String) reOrderList.get(i).get("dlvSeq");    
                    drvTel[i]          				= (String) reOrderList.get(i).get("drvTel");    
                    custLotNo[i]        			= (String) reOrderList.get(i).get("CUST_LOT_NO");    
                    blNo[i]             			= (String) reOrderList.get(i).get("blNo");    
                    recDt[i]            			= (String) reOrderList.get(i).get("recDt");    
                    
                    outWhCd[i]          			= (String) reOrderList.get(i).get("OUT_WH_CD");
                    inWhCd[i]           			= (String) reOrderList.get(i).get("IN_WH_CD");
                    makeDt[i]           			= (String) reOrderList.get(i).get("makeDt");
                    timePeriodDay[i]    			= (String) reOrderList.get(i).get("timePeriodDay");
                    workYn[i]           			= (String) reOrderList.get(i).get("workYn");
                    
                    rjType[i]           			= (String) reOrderList.get(i).get("rjType");    
                    locYn[i]            			= (String) reOrderList.get(i).get("locYn");    
                    confYn[i]           			= (String) reOrderList.get(i).get("confYn");    
                    eaCapa[i]           			= (String) reOrderList.get(i).get("eaCapa");    
                    inOrdWeight[i]      			= (String) reOrderList.get(i).get("inOrdWeight");   
                    
                    itemCd[i]           			= (String) reOrderList.get(i).get("ITEM_CD");    
                    itemNm[i]          	 			= (String) reOrderList.get(i).get("itemNm");    
                    transCustNm[i]      			= (String) reOrderList.get(i).get("transCustNm");    
                    transCustAddr[i]    			= (String) reOrderList.get(i).get("transCustAddr");    
                    transEmpNm[i]       			= (String) reOrderList.get(i).get("transEmpNm");    

                    remark[i]           			= (String) reOrderList.get(i).get("remark");    
                    transZipNo[i]       			= (String) reOrderList.get(i).get("transZipNo");    
                    etc2[i]             			= (String) reOrderList.get(i).get("ETC2");    
                    unitAmt[i]          			= (String) reOrderList.get(i).get("unitAmt");    
                    transBizNo[i]       			= (String) reOrderList.get(i).get("transBizNo");    
                    
                    inCustAddr[i]       			= (String) reOrderList.get(i).get("inCustAddr");   
                    inCustCd[i]         			= (String) reOrderList.get(i).get("inCustCd");    
                    inCustNm[i]         			= (String) reOrderList.get(i).get("inCustNm");    
                    inCustTel[i]        			= (String) reOrderList.get(i).get("inCustTel");    
                    inCustEmpNm[i]      			= (String) reOrderList.get(i).get("inCustEmpNm");    
                    
                    expiryDate[i]       			= (String) reOrderList.get(i).get("expiryDate");
                    salesCustNm[i]      			= (String) reOrderList.get(i).get("D_CUST_NM");
                    zip[i]       					= (String) reOrderList.get(i).get("D_POST");
                    addr[i]       					= (String) reOrderList.get(i).get("D_ADDRESS");
                    addr2[i]       					= (String) reOrderList.get(i).get("D_ADDRESS");
                    phone1[i]       				= (String) reOrderList.get(i).get("D_TEL");
                    
                    etc1[i]      					= (String) reOrderList.get(i).get("etc1");
                    unitNo[i]      					= (String) reOrderList.get(i).get("UNIT_NO");
                    timeDate[i]         			= (String) reOrderList.get(i).get("timeDate");      
                    timeDateEnd[i]      			= (String) reOrderList.get(i).get("timeDateEnd");      
                    timeUseEnd[i]       			= (String) reOrderList.get(i).get("timeUseEnd");  
                    
                    phone2[i]       				= (String) reOrderList.get(i).get("D_MOBILE");     
                    buyCustNm[i]       				= (String) reOrderList.get(i).get("buyCustNm");     
                    buyPhone1[i]       				= (String) reOrderList.get(i).get("D_MOBILE");
                    salesCompanyNm[i]   			= (String) reOrderList.get(i).get("salesCompanyNm");
                    ordDegree[i]       				= String.valueOf(reOrderList.get(i).get("ORD_DEGREE"));
                    bizCond[i]       				= (String) reOrderList.get(i).get("D_BIZ_COND");
                    bizType[i]       				= (String) reOrderList.get(i).get("D_BIZ_TYPE");
                    bizNo[i]       					= (String) reOrderList.get(i).get("D_BIZ_NO");
                    custType[i]       				= (String) reOrderList.get(i).get("D_CUST_TYPE");
                    
                    dataSenderNm[i]         		= (String) reOrderList.get(i).get("DATA_SENDER_NM");
                    legacyOrgOrdNo[i]      			= (String) reOrderList.get(i).get("LEGACY_ORG_ORD_NO");
                    
                    custSeq[i]						= (String) reOrderList.get(i).get("custSeq");
                    
                    ordDesc[i]     			  		= (String) reOrderList.get(i).get("ORD_DESC");
                    dlvMsg1[i]       				= (String) reOrderList.get(i).get("DLV_MSG1");
                    dlvMsg2[i]       				= (String) reOrderList.get(i).get("DLV_MSG2");
                   
                }
                
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
               
                modelIns.put("vrOrdType"		, "O"); // 출고주문이기에 고정값 추후 수정해야할수 있음
                modelIns.put("vrOrdSubtype"		, model.get("vrSrchOrdSubType"));
                
                modelIns.put("no"  				, no);
                
                modelIns.put("reqDt"     		, outReqDt);
                modelIns.put("whCd"      		, outWhCd);
                
                modelIns.put("custOrdNo"    	, custOrdNo);
                modelIns.put("custOrdSeq"   	, custOrdSeq);
                modelIns.put("trustCustCd"  	, trustCustCd); //5
                
                modelIns.put("transCustCd"  	, transCustCd);
                modelIns.put("transCustTel" 	, transCustTel);
                modelIns.put("transReqDt"   	, transReqDt);
                modelIns.put("custCd"       	, custCd);
                modelIns.put("ordQty"       	, ordQty);      //10
                
                modelIns.put("uomCd"        	, uomCd);
                modelIns.put("sdeptCd"      	, sdeptCd);
                modelIns.put("salePerCd"    	, salePerCd);
                modelIns.put("carCd"        	, carCd);
                modelIns.put("drvNm"        	, drvNm);       //15
                
                modelIns.put("dlvSeq"       	, dlvSeq);
                modelIns.put("drvTel"       	, drvTel);
                modelIns.put("custLotNo"    	, custLotNo);
                modelIns.put("blNo"         	, blNo);
                modelIns.put("recDt"        	, recDt);       //20
                
                modelIns.put("makeDt"       	, makeDt);
                modelIns.put("timePeriodDay"	, timePeriodDay);
                modelIns.put("workYn"       	, workYn);
                
                modelIns.put("rjType"       	, rjType);
                modelIns.put("locYn"        	, locYn);       //25
                modelIns.put("confYn"       	, confYn);     
                modelIns.put("eaCapa"       	, eaCapa);
                modelIns.put("inOrdWeight"  	, inOrdWeight); //28
                
                modelIns.put("itemCd"           , itemCd);
                modelIns.put("itemNm"           , itemNm);
                modelIns.put("transCustNm"      , transCustNm);
                modelIns.put("transCustAddr"    , transCustAddr);
                modelIns.put("transEmpNm"       , transEmpNm);
                
                modelIns.put("remark"           , remark);
                modelIns.put("transZipNo"       , transZipNo);
                modelIns.put("etc2"             , etc2);
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("transBizNo"       , transBizNo);
                
                modelIns.put("inCustAddr"       , inCustAddr);
                modelIns.put("inCustCd"         , inCustCd);
                modelIns.put("inCustNm"         , inCustNm);                 
                modelIns.put("inCustTel"        , inCustTel);
                modelIns.put("inCustEmpNm"      , inCustEmpNm);
                
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("salesCustNm"      , salesCustNm);
                modelIns.put("zip"       		, zip);
                modelIns.put("addr"       		, addr);
                modelIns.put("addr2"       		, addr2);
                modelIns.put("phone1"       	, phone1);
                
                modelIns.put("etc1"     	 	, etc1);
                modelIns.put("unitNo"     	 	, unitNo);
                modelIns.put("phone2"			, phone2);  
                modelIns.put("buyCustNm"		, buyCustNm);  
                modelIns.put("buyPhone1"		, buyPhone1);
                modelIns.put("salesCompanyNm"	, salesCompanyNm);

                modelIns.put("ordDegree"		, ordDegree);
                modelIns.put("bizCond"			, bizCond);
                modelIns.put("bizType"			, bizType);
                modelIns.put("bizNo"			, bizNo);
                modelIns.put("custType"			, custType);
                
                modelIns.put("dataSenderNm"		, dataSenderNm);
                modelIns.put("legacyOrgOrdNo"	, legacyOrgOrdNo);
//                modelIns.put("invoiceNo"		, invoiceNo);
                modelIns.put("custSeq"       	, custSeq);
                
                modelIns.put("ordDesc"       	, ordDesc);
                modelIns.put("dlvMsg1"       	, dlvMsg1);
                modelIns.put("dlvMsg2"       	, dlvMsg2);
                
                modelIns.put("LC_ID"    		, model.get("SS_SVC_NO"));
                modelIns.put("WORK_IP"  		, model.get("SS_CLIENT_IP"));
                modelIns.put("USER_NO"  		, model.get("SS_USER_NO"));

            	modelIns = (Map<String, Object>)dao.saveExcelOrderB2Cts(modelIns);	
            	ServiceUtil.isValidReturnCode("WMSOP031", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }  
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(Exception e) {
           throw e;
        }
        
        return m;
    } 
    
//    /**
//     * 
//     * Method ID   : sum
//     * Method 설명    : 출고관리 합계
//     * 작성자               : chsong
//     * @param   model
//     * @return
//     * @throws  Exception
//     */
//    @Override
//    public Map<String, Object> sum(Map<String, Object> model) throws Exception {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("SUM", dao.sum(model));
//        return map;
//    }
      
}
