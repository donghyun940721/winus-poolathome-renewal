package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP335Dao")
public class WMSOP335Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
   
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : 화면내 필요한 상품군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    public List rawListByCustSummary(Map<String, Object> model) {
		List custs = list("wmsop335.listByCustSummary", model);
    	return custs;
	}
    
	public Object listByCustSummary(Map<String, Object> model) {
		List custs = rawListByCustSummary(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}

	public Object listByCustDetail(Map<String, Object> model) {
		
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmsop335.listByCustDetail", model));
    	return genericResultSet;
	}

	public Object listCountByCust(Map<String, Object> model) {
		return executeView("wmsop335.listCountByCust", model);
	}

	/**
     * Method ID  : outInvalidView
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public Object outInvalidView(Map<String, Object> model){
        return executeQueryForList("wmsop335.outInvalidView", model);
    }
    
    public List rawListByCustSummaryWork(Map<String, Object> model) {
		List custs = list("wmsop335.listByCustSummaryWork", model);
    	return custs;
	}
    
	public Object listByCustSummaryWork(Map<String, Object> model) {
		List custs = rawListByCustSummaryWork(model);
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(custs);
    	return genericResultSet;
	}
	
	/**
     * Method ID    : autoBestLocSaveMultiV3
     * Method 설명      : 출고관리 출고주문 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object autoBestLocSaveMultiV3(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001.sp_auto_location_type_multiV3", model);
        return model;
    }
    
    /**
     * Method ID    : updatePickingTotal/Poi
     * Method 설명      : 토탈피킹리스트발행
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object updatePickingTotalV2Poi(Map<String, Object> model){
        executeUpdate("wmsop335.pk_wmsop030.sp_print_picking_poino", model);
        return model;
    }
    public Object updatePickingTotalV2(Map<String, Object> model){
        executeUpdate("wmsop335.pk_wmsop030.sp_print_picking", model);
        return model;
    }
    
    /**
     * Method ID    : confirmPickingTotalV2
     * Method 설명      : 토탈피킹리스트확정
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public Object confirmPickingTotalV2(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.SP_PICKING", model);
        return model;
    }
    
    /**
     * Method ID  : billingListDetailV2
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet billingListDetailV2(Map<String, Object> model) {
        return executeQueryPageWq("wmsop335.billingListDetailV2", model);
    }
    
    /**
     * Method ID    : autoDeleteLocSaveV2
     * Method 설명      : 로케이션 지정 삭제
     * 작성자                 : MonkeySeok
     * @param   model
     * @return
     */
    public Object autoDeleteLocSaveV2(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop000.sp_del_loc_multi", model);
        return model;
    }
    
    /**
     * Method ID    : saveOutCompleteV2
     * Method 설명      : 출고확정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOutCompleteV2(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_out_complete", model);
        return model;
    }
    
    /**
     * Method ID  : ritemIdSearchCd
     * Method 설명  : ritemIdSearchCd 상품조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object ritemIdSearchCd(Map<String, Object> model){
        return executeQueryForList("wmsop335.ritemIdSearchCd", model);
    }
}
