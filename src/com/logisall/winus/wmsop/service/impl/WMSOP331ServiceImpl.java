package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
 

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP331Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.common.RestApiUtil;


/*SF*/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
@Service("WMSOP331Service")
public class WMSOP331ServiceImpl implements WMSOP331Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP331Dao")
    private WMSOP331Dao dao;
    
    /**
     * Method ID   : selectItemGrp
     * Method 설명    : 출고관리 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * Method ID   : listByCustSummary
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustSummary(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByCustSummary(model));
        return map;
    }
    @Override
    public Map<String, Object> listByCustSummaryPoi(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByCustSummaryPoi(model));
        return map;
    }
    
    /**
     * Method ID   : listByCustDetail
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByCustDetail(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: rogenCrossDomainHttp
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> rogenCrossDomainHttp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	//ssl disable
        	disableSslVerification();
        	
        	String sUrl = "https://52.79.206.98:5101/SAEROPNLWMS/ROGEN/ORDER";
	        //System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "winus01" + ":" + "winus01";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			//Json Data
			String jsonInputString = "{\"orderResult\":  {\"ordId\":\""+(String) model.get("ordId")+"\""
									+					",\"lcId\":\""+(String) model.get("lcId")+"\""
									+					",\"freightType\":\""+(String) model.get("freightType")+"\""
									+					",\"qty\":\""+(String) model.get("qty")+"\""
									+					",\"priceAmt\":\""+(String) model.get("priceAmt")+"\""
									+					",\"extraAmt\":\""+(String) model.get("extraAmt")+"\""
									+					",\"ordPayKey\":\""+(String) model.get("ordPayKey")+"\"}}";
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()) {
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            //System.out.println(response.toString());
	            //JSONObject jsonData = new JSONObject(response.toString());
	            //m.put("header"	, jsonData.get("header"));
                //m.put("message"	, jsonData.get("message"));
                //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
                m.put("rst"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID	: saeroCompleteCrossDomainHttp
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> saeroCompleteCrossDomainHttp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	//ssl disable
        	disableSslVerification();
        	
        	String sUrl = "https://52.79.206.98:5101/SAEROPNLWMS/ORDER/COMPLETE";
	        System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "winus01" + ":" + "winus01";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			//Json Data
			String jsonInputString = "{\"orderResult\":  {\"ordId\":\""+(String) model.get("ordId")+"\"}}";
			
			System.out.println("ordId: "+(String) model.get("ordId"));
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()) {
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            //JSONObject jsonData = new JSONObject(response.toString());
	            //m.put("header"	, jsonData.get("header"));
                //m.put("message"	, jsonData.get("message"));
                //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
                m.put("rst"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
	        TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
	            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
	                return null;
	            }
	            public void checkClientTrusted(X509Certificate[] certs, String authType) {
	            }
	            public void checkServerTrusted(X509Certificate[] certs, String authType) {
	            }
	        }};

	        // Install the all-trusting trust manager
	        SSLContext sc = SSLContext.getInstance("SSL");
	        sc.init(null, trustAllCerts, new java.security.SecureRandom());
	        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

	        // Create all-trusting host name verifier
	        HostnameVerifier allHostsValid = new HostnameVerifier() {
	            public boolean verify(String hostname, SSLSession session) {
	                return true;
	            }
	        };
	        
	        // Install the all-trusting host verifier
	        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
	    } catch (NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    } catch (KeyManagementException e) {
	        e.printStackTrace();
	    }
	}
	
	/**
     * 
     * 대체 Method ID	: billingListDetail
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> billingListDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.billingListDetail(model));
        return map;
    }
    
    /**
     * Method ID	: popupList
     * Method 설명	: 그리드 컬럼 리스트 조회
     * 작성자			: 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> popupList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.popupList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: popupSave
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> popupSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ORD_ID"	, model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"	, model.get("ORD_SEQ"+i));
                modelDt.put("INVC_NO"	, model.get("INVC_NO"+i));
                modelDt.put("REG_NO"    , model.get(ConstantIF.SS_USER_NO));
                modelDt.put("UPD_NO"    , model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    	, model.get(ConstantIF.SS_SVC_NO));
                
                dao.popupSave(modelDt);
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
