package com.logisall.winus.wmsop.service.impl;

import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP000Dao")
public class WMSOP000Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID  : selectUom
     * Method 설명  : UOM 데이터셋
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectUom(Map<String, Object> model){
        return executeQueryForList("wmsms100.selectUom", model);
    }
    
    /**
     * Method ID    : centerRule
     * Method 설명      : 센터별정책항목
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object centerRule(Map<String, Object> model){
        return executeQueryForList("wmsms020.centerRule", model);
    }
    
    /**
     * Method ID    : assertRule
     * Method 설명      : 정책
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object assertRule(Map<String, Object> model){
        executeUpdate("wmsop000.pk_wmsop000.sp_assert_rule", model);
        return model;
    }
    
    /**
     * Method ID    : listCarPop
     * Method 설명      : 배차작업 차량목록
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listCarPop(Map<String, Object> model) {
        return executeQueryPageWq("wmsms050.listCarPop", model);
    }
    
    /**
     * Method ID    : listDsp
     * Method 설명      : 배차작업 배차내역
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDsp(Map<String, Object> model) {
        return executeQueryPageWq("wmsop050.listDsp", model);
    }
    
    /**
     * Method ID    : listNewDsp
     * Method 설명      : 미배차내역 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listNewDsp(Map<String, Object> model) {
        return executeQueryPageWq("wmsop011.listNewDsp", model);
    }
    
    /**
     * Method ID    : saveDsp
     * Method 설명      : 배차내역 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveDsp(Map<String, Object> model){
    	
    	String LC_ID = (String)model.get("LC_ID");
    	if (LC_ID != null && StringUtils.isNotEmpty(LC_ID) && LC_ID.equals("0000002940")){//kcc
    		executeUpdate("wmsop000.pk_wmsop000.sp_save_dsp_kcc", model);
    	}else{
    		executeUpdate("wmsop000.pk_wmsop000.sp_save_dsp", model);
    	}
       
        return model;
    }
    
    /**
     * Method ID    : deleteDsp
     * Method 설명      : 배차내역 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deleteDsp(Map<String, Object> model){
        
        
        String LC_ID = (String)model.get("LC_ID");
    	if (LC_ID != null && StringUtils.isNotEmpty(LC_ID) && LC_ID.equals("0000002940")){//kcc
    		executeUpdate("wmsop000.pk_wmsop000.sp_delete_dsp_kcc", model);
    	}else{
    		executeUpdate("wmsop000.pk_wmsop000.sp_delete_dsp", model);
    	}
    	
    	
        return model;
    }
    
    /**
     * Method ID    : listConfirmDsp
     * Method 설명      : 입차확인 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listConfirmDsp(Map<String, Object> model) {
        return executeQueryPageWq("wmsop050.listConfirmDsp", model);
    }
    
    /**
     * Method ID    : saveConfirmCarIn
     * Method 설명      : 입차확인 승인,취소
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveConfirmCarIn(Map<String, Object> model){
        executeUpdate("wmsop000.pk_wmsop000.sp_confirm_car_in", model);
        return model;
    }
   
    /**
     * Method ID    : saveSetDock
     * Method 설명      : Dock장 지정,해제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSetDock(Map<String, Object> model){
        executeUpdate("wmsop000.pk_wmsop000.sp_set_dock", model);
        return model;
    }
    
    /**
     * Method ID    : listLoc
     * Method 설명      : Location 지정 목록
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listLoc(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.listLoc", model);
    }
    
    /**
     * Method ID    : listLoc
     * Method 설명      : Location 지정 목록
     * 작성자                 : schan
     * @param   model
     * @return
     */
    public GenericResultSet listLocV2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.listLocV2", model);
    }

    
    /**
     * Method ID    : saveLocInitType
     * Method 설명      : Location 추천위해
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveLocInitType(Map<String, Object> model){
        executeUpdate("wmsop000.pk_wmsop000.sp_init_work_wrap", model);
        return model;
    }
    
    /**
     * Method ID    : listLocSub
     * Method 설명      : Location 지정 Sub 목록
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listLocSub(Map<String, Object> model) {
        return executeQueryPageWq("wmsst502.listLocSub", model);
    }
    
    /**
     * Method ID    : saveLoc
     * Method 설명      : Location 지정 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveLoc(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001_sable7.sp_save_loc", model);
        return model;
    }
    
    /**
     * Method ID    : deleteLoc
     * Method 설명      : Location 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deleteLoc(Map<String, Object> model){
        executeUpdate("wmsop000.pk_wmsop000.sp_del_loc", model);
        return model;
    }
    
    /**
     * Method ID    : locAutoInLoc
     * Method 설명      : 입고 로케이션 추천
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object locAutoInLoc(Map<String, Object> model){
        executeUpdate("wmsst001.pk_wmsst001.pc_putaway_loc", model);
        return model;
    }
    
    /**
     * Method ID    : locAutoInLoc_2048
     * Method 설명      : 입고 로케이션 추천
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object locAutoInLoc_2048(Map<String, Object> model){
        executeUpdate("wmsst001.pk_wmsst001.pc_putaway_loc_2048", model);
        return model;
    }
    
    /**
     * Method ID    : locAutoOutLoc
     * Method 설명      : 출고 로케이션 추천
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object locAutoOutLoc(Map<String, Object> model){
        executeUpdate("wmsst001.pk_wmsst001.pc_picking_loc", model);
        return model;
    }    
    
    /**
     * Method ID    : locAutoOutLoc_2048
     * Method 설명      : 출고 로케이션 추천
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object locAutoOutLoc_2048(Map<String, Object> model){
        executeUpdate("wmsst001.pk_wmsst001.pc_picking_loc_2048", model);
        return model;
    }   
    
    /**
     * Method ID    : locAutoInLoc
     * Method 설명      : 입고 수량변경
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveLocChangeWorkQty(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001.sp_set_workqty", model);
        return model;
    }
    
    /**
     * Method ID    : saveSetWorkDate
     * Method 설명      : 입고일변경
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSetWorkDate(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001.sp_set_workdate", model);
        return model;
    }
    
    /**
     * Method ID    : saveSetWorkDateMemo
     * Method 설명      : 입고일변경 시 메모작성
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSetWorkDateMemo(Map<String, Object> model){
        executeUpdate("wmsop001.pk_wmsop001.saveSetWorkDateMemo", model);
        return model;
    }
    
    /**
     * Method ID    : setLocAutoInLoc
     * Method 설명      : 입고 로케이션 추천2
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object setLocAutoInLoc(Map<String, Object> model){
        executeUpdate("wmsst001.pk_wmsst001_2.pc_putaway_loc", model);
        return model;
    }
    
    /**
     * Method ID    : setLocAutoOutLoc
     * Method 설명      : 출고 로케이션 추천2
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object setLocAutoOutLoc(Map<String, Object> model){
        executeUpdate("wmsst001.pk_wmsst001_2.pc_picking_loc", model);
        return model;
    } 
    
    
    /**
     * Method ID    : listTransporDsp
     * Method 설명      : 운송자 거래처 목록
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listTransporDsp(Map<String, Object> model) {
        return executeQueryPageWq("wmsms011.search", model);
    }
    
    
    /**
     * Method ID    : listTransCustIdDsp
     * Method 설명      : 운송자 거래처 지정내역
     * 작성자                 :  yhku
     * @param   model
     * @return
     */
    public GenericResultSet listTransCustIdDsp(Map<String, Object> model) {
        return executeQueryPageWq("wmsop050.listTransCustIdDsp", model);
    }
    
    
    /**
     * Method ID    : saveTransDsp
     * Method 설명      : 운송장 미등록 내역 등록,수정
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object saveTransDsp(Map<String, Object> model){
    	 executeUpdate("wmsop050.saveTransDsp", model);
        return model;
    }
    
    /**
     * Method ID    : deleteTransDsp
     * Method 설명      : 운송장 내역 삭제
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object updateDelTransDsp(Map<String, Object> model){
    	executeUpdate("wmsop050.updateDelTransDsp", model);
        return model;
    }
    
    
}

 
