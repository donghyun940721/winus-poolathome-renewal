package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.components.headertoolbar.actions.MoveElementCommand;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP335Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;





/*SF*/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONObject;
import org.json.XML;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.OrderWebService;

@Service("WMSOP335Service")
public class WMSOP335ServiceImpl implements WMSOP335Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP335Dao")
    private WMSOP335Dao dao;
    
    /**
     * Method ID   : selectItemGrp
     * Method 설명    : 출고관리 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * Method ID   : listByCustSummary
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : MonkeySeok
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustSummary(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByCustSummary(model));
        return map;
    }
    
    /**
     * Method ID   : listByCustDetail
     * Method 설명    : 화주별 출고관리  조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByCustDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByCustDetail(model));
        return map;
    }
    
    /**
     * Method ID   : listCountByCust
     * Method 설명    : 화주별 출고관리카운트 조회
     * 작성자               : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCountByCust(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listCountByCust(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : outInvalidView
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	public Map<String, Object> outInvalidView(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("DS_INVALID")) {
			map.put("DS_INVALID", dao.outInvalidView(model));
		}
		return map;
	}
	
	/**
     * 
     * 대체 Method ID   : autoBestLocSaveMultiV
     * 대체 Method 설명    : 로케이션추천 자동V3
     * 작성자                      : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoBestLocSaveMultiV3(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	List list = dao.rawListByCustSummaryWork(model);
        	
            int tmpCnt = list.size();
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                //String[] ordSeq   = new String[tmpCnt];
                String[] typeSt   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]	= (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	//ordSeq[i]	= (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
                	typeSt[i]	= (String)model.get("vrSrchTypeSt");
                	
                	System.out.println(" >> " + (String)model.get("vrSrchTypeSt"));
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId"		, ordId);
                //modelIns.put("ordSeq"		, ordSeq);
                modelIns.put("typeSt"		, typeSt);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.autoBestLocSaveMultiV3(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP335", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
     *  Method ID  : updatePickingTotalV2/Poi 
     *  Method 설명  : 토탈피킹리스트발행
     *  작성자             : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updatePickingTotalV2Poi(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	
        	int workStatCnt = 0;
        	int chkIdsCnt = 0;
        	List list = dao.rawListByCustSummaryWork(model);
        	int tmpCnt = list.size();
            if(tmpCnt > 0){           
            
	            String[] ordId = new String[tmpCnt];
	            String[] ordSeq = new String[tmpCnt];
	            String[] poiItemCnt = new String[tmpCnt];
	            
	            
	            for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
                	poiItemCnt[i]    = (String)((Map<String, String>)list.get(i)).get("POI_ITEM_CNT");

                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
                    int outOrdQty = Integer.parseInt(String.valueOf(((Map<String, String>)list.get(i)).get("OUT_ORD_QTY")));
                    
                  
                    if(Integer.parseInt(workStat) == 450){
            			workStatCnt++;
            		}
            		
            		if(Integer.parseInt(workStat) < 230){
            			if(0 < outOrdQty){
            				chkIdsCnt++;
            			}
            		}
            		
                	//System.out.println(ordId[i] + "::::" + ordSeq[i]);
                }
	            
	            if(workStatCnt > 0){
	            	if(tmpCnt != workStatCnt){
	            		String msg = "매핑 작업시 모든 상태가 매핑 이어야 합니다.";
	            		throw new BizException(msg);
	            	}
	            }
	            
	            if(chkIdsCnt > 0){
	        		String msg = "피킹리스트발행시로케이션지정은필수입니다";
	        		throw new BizException(msg);
	            }
	            
	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();

	            modelIns.put("ordId", ordId);
	            modelIns.put("ordSeq", ordSeq);
	            modelIns.put("poiItemCnt", poiItemCnt);

	            // session 및 등록정보
	            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

	            // dao
	            modelIns = (Map<String, Object>)dao.updatePickingTotalV2Poi(modelIns);
	            ServiceUtil.isValidReturnCode("WMOP335", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
  	    	
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    @Override
    public Map<String, Object> updatePickingTotalV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	
        	int workStatCnt = 0;
        	int chkIdsCnt = 0;
        	List list = dao.rawListByCustSummaryWork(model);
        	int tmpCnt = list.size();
            if(tmpCnt > 0){           
            
	            String[] ordId = new String[tmpCnt];
	            String[] ordSeq = new String[tmpCnt];
	            
	            
	            for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");

                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
                    int outOrdQty = Integer.parseInt(String.valueOf(((Map<String, String>)list.get(i)).get("OUT_ORD_QTY")));
                    
                  
                    if(Integer.parseInt(workStat) == 450){
            			workStatCnt++;
            		}
            		
            		if(Integer.parseInt(workStat) < 230){
            			if(0 < outOrdQty){
            				chkIdsCnt++;
            			}
            		}
                  
                	//System.out.println(ordId[i] + "::::" + ordSeq[i]);
                }
	            
	            if(workStatCnt > 0){
	            	if(tmpCnt != workStatCnt){
	            		String msg = "매핑 작업시 모든 상태가 매핑 이어야 합니다.";
	            		throw new BizException(msg);
	            	}
	            }
	            
	            if(chkIdsCnt > 0){
	        		String msg = "피킹리스트발행시로케이션지정은필수입니다";
	        		throw new BizException(msg);
	            }
	            
	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();

	            modelIns.put("ordId", ordId);
	            modelIns.put("ordSeq", ordSeq);

	            // session 및 등록정보
	            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

	            // dao
	            modelIns = (Map<String, Object>)dao.updatePickingTotalV2(modelIns);
	            ServiceUtil.isValidReturnCode("WMOP335", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
  	    	
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    /**
     *  Method ID  : confirmPickingTotalV2
     *  Method 설명  : 토탈피킹리스트 확정
     *  작성자             : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> confirmPickingTotalV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	
        	List list = dao.rawListByCustSummaryWork(model);
        	int tmpCnt = list.size();
            int workStatCnt = 0;
        	if(tmpCnt > 0){           
	            String[] ordId = new String[tmpCnt];
	            String[] ordSeq = new String[tmpCnt];
	            
	            for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
                	
                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
                    
                    if(Integer.parseInt(workStat) < 230 || Integer.parseInt(workStat) > 990){
            			workStatCnt++;
            		}
                	System.out.println(ordId[i] + "::::" + ordSeq[i]);
                }
	            
	            if(workStatCnt > 0){
            		String msg = "모든 상태가  피킹리스트 발행 이어야 합니다.";
            		throw new BizException(msg);
	            }

	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();

	            modelIns.put("ordId", ordId);
	            modelIns.put("ordSeq", ordSeq);
	            
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

	            // dao
	            modelIns = (Map<String, Object>)dao.confirmPickingTotalV2(modelIns);
	            ServiceUtil.isValidReturnCode("WMOP335", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
  	    	
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID	: billingListDetailV2
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> billingListDetailV2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.billingListDetailV2(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : autoDeleteLocSaveV2
     * 대체 Method 설명    : 로케이션지정 삭제
     * 작성자                      : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoDeleteLocSaveV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

        	List list = dao.rawListByCustSummaryWork(model);
        	int tmpCnt = list.size();
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                String[] ordSeq   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
                    //log.info("AAAAAAAAAAAAAA:"+(String)model.get("I_ORD_SEQ"+i));
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
                
                
                //dao                
                modelIns = (Map<String, Object>)dao.autoDeleteLocSaveV2(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP335", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	/**
     * 
     * Method ID   : saveOutCompleteV2
     * Method 설명    : 출고확정
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutCompleteV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
        	List list = dao.rawListByCustSummaryWork(model);
        	int tmpCnt = list.size();
            if(tmpCnt > 0){           
            
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                String[] workQty = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
                    workQty[i]    		= null;  
                    //System.out.println(ordId[i]+ " " + ordSeq[i] + " " + workQty[i]);
                    
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("workQty", workQty);
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOutCompleteV2(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP335", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> ritemIdSearchCd(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ITEM", dao.ritemIdSearchCd(model));
		
		return map;
	}
}