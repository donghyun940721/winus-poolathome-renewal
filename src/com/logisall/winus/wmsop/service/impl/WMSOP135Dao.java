package com.logisall.winus.wmsop.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsop.vo.WMSOP035VO;
import com.logisall.winus.wmsop.vo.WMSOP135VO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP135Dao")
public class WMSOP135Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    
       
    /**
     * Method ID    : getOrdSeqUpdate
     * Method 설명      : 주문 상세번호 정보 update
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object getOrdSeqUpdate(Map<String, Object> model){
        //executeUpdate("wmsop135.getOrdSeqUpdate", model);
        return model;
    }
   
    
    /**
     * Method ID    : getOrdList
     * Method 설명      : 주문 상세번호  데이터셋 List
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public  List<WMSOP135VO> getOrdList(Map<String, Object> model) {
        return executeQueryForList("wmsop135.getOrdList", model);
    }
    
    
    /**
     * Method ID    : getOrdSeqInfo
     * Method 설명      : 주문 상세번호  데이터셋 List
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public   WMSOP135VO getOrdSeqInfo(Map<String, Object> model) {
        return (WMSOP135VO) executeQueryForObject("wmsop135.getOrdSeqInfo", model);
    }
    
    
    /**
     * Method ID    : getOrdSeqList
     * Method 설명      : 주문 상세번호  데이터셋 List
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public   List<WMSOP135VO> getOrdSeqList(Map<String, Object> model) {
        return executeQueryForList("wmsop135.getOrdSeqList", model);
    }
    
    
    
   /* public GenericResultSet getOrdSeqList(Map<String, Object> model) {
        return executeQueryPageWq("wmsop135.getOrdSeqList", model);
    }*/
    
    /**
     * Method ID    : updateOrdDesc
     * Method 설명      : 비고1, 비고2 업데이트
     * 작성자                 : Seongjun kwon
     * @param   model
     * @return
     */
    public Object updateOrdDesc(Map<String, Object> model){
        return executeUpdate("wmsop135.updateOrdDesc", model);
    }
    
    
    
    /**
     * Method ID    : autoBestLocSaveMultiQtyList
     * Method 설명      : autoBestLocSaveMultiQtyList
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object autoBestLocSaveMultiQtyList(Map<String, Object> model) {
        return executeQueryForList("wmsop135.autoBestLocSaveMultiQtyList", model);
    }
    
    
    
    /**
     * Method ID    : list
     * Method 설명      : 출고관리 조회
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public GenericResultSet listExcel(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outListKcc", model);
    }
    
    

    /**
     * Method ID    : deliveryComplete
     * Method 설명      : 배송완료(이동출고)
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deliveryComplete(Map<String, Object> model){
        executeUpdate("wmsop135.pk_wmsif106.sp_kcc_shp_rdc_rtn_order", model);
        return model;
    }
    
    
    
}
