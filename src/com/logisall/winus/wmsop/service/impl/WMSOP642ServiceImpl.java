package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.components.headertoolbar.actions.MoveElementCommand;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;






















/*SF*/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONObject;
import org.json.XML;
import org.json.simple.JSONArray;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.OrderWebService;

@Service("WMSOP642Service")
public class WMSOP642ServiceImpl implements WMSOP642Service{

    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSOP642Dao")
    private WMSOP642Dao dao;
    
    /**
     * Method ID	: selectItemGrp
     * Method 설명	: 출고관리 화면에서 필요한 데이터
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * Method ID	: listByDlvSummary
     * Method 설명	: 택배접수조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByDlvSummary(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        //추가접수제외
        if("on".equals(model.get("chkInvcAddFlag"))){
    		model.put("chkInvcAddFlag", "N");
    	}
        
        //반품접수 제외
        if("on".equals(model.get("chkInvcRtnFlag"))){
    		model.put("chkInvcRtnFlag", "N");
    	}
        
        map.put("LIST", dao.listByDlvSummary(model));
        return map;
    }
    
    /**
     * Method ID	: listByDlvSummaryDiv
     * Method 설명	: 택배접수조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listByDlvSummaryDiv(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByDlvSummaryDiv(model));
        return map;
    }
    
    /**
     * Method ID	: list2ByDlvHistory
     * Method 설명	: 택배접수조회
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list2ByDlvHistory(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.list2ByDlvHistory(model));
        return map;
    }
    
    /**
     * Method ID	: list3ByDlvHistory
     * Method 설명	: 택배접수조회
     * 작성자			: summer h //tab3
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list3ByDlvHistory(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.list3ByDlvHistory(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: DlvShipment
     * 대체 Method 설명	: 택배접수
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvShipment(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        
//        String hostUrl		= (String)model.get("hostUrl");
//        String eaiUrl 	= "";
//    	if(hostUrl != null && hostUrl.equals("localhost")){
//    		eaiUrl 	= "https://172.31.10.253:5201/"; //실운영(local에서 접근)
//        }else{
//        	eaiUrl 	= "https://52.79.206.98:5201/"; //실운영
//        }
//    	
//        
//        try{
//        	//송장 추가발행 시 기 발행 송장번호 삭제 로직은 배제한다.
//        	if("N".equals(model.get("ADD_FLAG").toString())){
//    			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
//            		//체크박스 전체에서 송장 접수를 했을 때 이미 송장번호가 있으면 CJ접수 시 WMSDF110 테이블의 이력 삭제 선행 처리 (DEL_YN = Y)
//            		try{
//                		if (model.get("INVC_NO"+i).toString().length() != 0 && !StringUtils.isEmpty((String)model.get("INVC_NO"+i))){
//                			Map<String, Object> modelDel = new HashMap<String, Object>();
//                			modelDel.put("ORD_ID"    	, model.get("ORD_ID"+i));
//                			modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
//                			modelDel.put("INVC_NO"		, model.get("INVC_NO"+i));
//                			modelDel.put("DLV_COMP_CD"	, "04");
//                            
//                            dao.wmsdf110DelYnUpdate(modelDel);
//                		}
//                    } catch(Exception e){
//                        throw e;
//                    }
//    			}
//        	}
//        	
//            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
//            	// 분리접수 구분
//            	String sUrl = "";
//            	if("NORMAL".equals(model.get("DIV_FLAG").toString())){
//            		sUrl = eaiUrl + "PARCEL/CJLOGIS/COMM/ORDER";
//                }
//                
//            	try{
//            		//1)N:일반, 2)R:일반이지만 단포검수 추가송장만, 3)Y:추가송장버튼
//                	if("N".equals(model.get("ADD_FLAG").toString())){
//            			//일반송장발행 시
//                		//PROC_FLAG = 1 일 경우만  : (고객명/주소/전화번호가 동일 한 1row만 처리) 
//                    	if("1".equals(model.get("PROC_FLAG"+i))){                  		
//                        	//Json Data
//                			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
//        				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
//        				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
//                									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
//                									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
//                									+					",\"PARCEL_BOX_TY\":\""		+model.get("PARCEL_BOX_TY").toString()+"\""
//                									+					",\"PARCEL_ETC_TY\":\""		+model.get("PARCEL_ETC_TY").toString()+"\""
//                									+					",\"FR_DATE\":\""			+model.get("FR_DATE").toString()+"\""
//                									+					",\"TO_DATE\":\""			+model.get("TO_DATE").toString()+"\""
//                									+					",\"ORD_DEGREE\":\""		+model.get("ORD_DEGREE").toString()+"\""
//                									+					",\"ORD_PH\":\""			+model.get("ORD_PH").toString()+"\""
//                									+					",\"ORD_PD\":\""			+model.get("ORD_PD").toString()+"\""
//                									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
//                									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
//                									+					",\"ADD_NROW\":\""			+"-1"+"\""
//                									
//                									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
//                									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
//                									+					",\"RCVR_NM\":\""			+(String)model.get("RCVR_NM"+i)+"\""
//                									+					",\"RCVR_ADDR\":\""			+(String)model.get("RCVR_ADDR"+i)+"\""
//                									+					",\"RCVR_DETAIL_ADDR\":\""	+(String)model.get("RCVR_DETAIL_ADDR"+i)+"\""
//                									+					",\"ORD_ID\":\""			+(String)model.get("ORD_ID"+i)+"\""
//                									+					",\"ORD_SEQ\":\""			+(String)model.get("ORD_SEQ"+i)+"\""
//                									+					",\"DATA_SENDER_NM\":\""	+(String)model.get("DATA_SENDER_NM"+i)+"\""
//                									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\"}}";
//                			//System.out.println(jsonInputString);
//                			
//                        	//ssl disable
//                        	disableSslVerification();
//                	        
//                	        URL url = null; 
//                	        url = new URL(sUrl);
//                	        
//                	        HttpsURLConnection con = null;
//                        	con = (HttpsURLConnection) url.openConnection();
//                        	
//                        	//웹페이지 로그인 권한 적용
//                        	String userpass = "eaiuser01" + ":" + "eaiuser01";
//                        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
//
//                        	con.setRequestProperty ("Authorization", basicAuth);
//                        	con.setDoInput(true);
//                        	con.setDoOutput(true);
//                        	con.setRequestMethod("POST");
//                        	con.setConnectTimeout(0);
//                        	con.setReadTimeout(0);
//                        	con.setRequestProperty("Content-Type", "application/json");
//                			con.setRequestProperty("Accept", "application/json");
//                			
//                			//JSON 보내는 Output stream
//                	        try(OutputStream os = con.getOutputStream()) {
//                	            byte[] input = jsonInputString.getBytes("utf-8");
//                	            os.write(input, 0, input.length);
//                	        }
//                	        
//                	        //Response data 받는 부분
//                	        
//                	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
//                	            StringBuilder response = new StringBuilder();
//                	            String responseLine = null;
//                	            while ((responseLine = br.readLine()) != null) {
//                	                response.append(responseLine.trim());
//                	            }
//                	            
//                	            //System.out.println(response.toString());
//                	            
//                	            JSONObject jsonData = new JSONObject(response.toString());
//                	            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
//                	            m.put("header"	, docResponse.get("header")); // Y, E
//                	            m.put("message"	, docResponse.get("message"));// msg
//                	            
//                                //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
//                                //m.put("rst"	, response.toString());
//                	        }
//                        	con.disconnect();
//                    	}
//                		
//                	}else if("Y".equals(model.get("ADD_FLAG").toString())){
//                		//추가송장발행 시
//                    	//Json Data
//            			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
//    				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
//    				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
//            									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
//            									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
//            									+					",\"PARCEL_BOX_TY\":\""		+model.get("PARCEL_BOX_TY").toString()+"\""
//            									+					",\"PARCEL_ETC_TY\":\""		+model.get("PARCEL_ETC_TY").toString()+"\""
//            									+					",\"FR_DATE\":\""			+model.get("FR_DATE").toString()+"\""
//            									+					",\"TO_DATE\":\""			+model.get("TO_DATE").toString()+"\""
//            									+					",\"ORD_DEGREE\":\""		+model.get("ORD_DEGREE").toString()+"\""
//            									+					",\"ORD_PH\":\""			+model.get("ORD_PH").toString()+"\""
//            									+					",\"ORD_PD\":\""			+model.get("ORD_PD").toString()+"\""
//            									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
//            									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
//            									+					",\"ADD_NROW\":\""			+"-1"+"\""
//            									
//            									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
//            									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
//            									+					",\"RCVR_NM\":\""			+(String)model.get("RCVR_NM"+i)+"\""
//            									+					",\"RCVR_ADDR\":\""			+(String)model.get("RCVR_ADDR"+i)+"\""
//            									+					",\"RCVR_DETAIL_ADDR\":\""	+(String)model.get("RCVR_DETAIL_ADDR"+i)+"\""
//            									+					",\"ORD_ID\":\""			+(String)model.get("ORD_ID"+i)+"\""
//            									+					",\"ORD_SEQ\":\""			+(String)model.get("ORD_SEQ"+i)+"\""
//            									+					",\"DATA_SENDER_NM\":\""	+(String)model.get("DATA_SENDER_NM"+i)+"\""
//            									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\"}}";
//            			//System.out.println(jsonInputString);
//            			
//                    	//ssl disable
//                    	disableSslVerification();
//            	        //System.out.println("sUrl : " + sUrl);
//            	        
//            	        URL url = null; 
//            	        url = new URL(sUrl);
//            	        
//            	        HttpsURLConnection con = null;
//                    	con = (HttpsURLConnection) url.openConnection();
//                    	
//                    	//웹페이지 로그인 권한 적용
//                    	String userpass = "eaiuser01" + ":" + "eaiuser01";
//                    	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
//
//                    	con.setRequestProperty ("Authorization", basicAuth);
//                    	con.setDoInput(true);
//                    	con.setDoOutput(true);
//                    	con.setRequestMethod("POST");
//                    	con.setConnectTimeout(0);
//                    	con.setReadTimeout(0);
//                    	con.setRequestProperty("Content-Type", "application/json");
//            			con.setRequestProperty("Accept", "application/json");
//            			
//            			//JSON 보내는 Output stream
//            	        try(OutputStream os = con.getOutputStream()) {
//            	            byte[] input = jsonInputString.getBytes("utf-8");
//            	            os.write(input, 0, input.length);
//            	        }
//            	        
//            	        //Response data 받는 부분
//            	        
//            	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
//            	            StringBuilder response = new StringBuilder();
//            	            String responseLine = null;
//            	            while ((responseLine = br.readLine()) != null) {
//            	                response.append(responseLine.trim());
//            	            }
//            	            
//            	            //System.out.println(response.toString());
//            	            
//            	            JSONObject jsonData = new JSONObject(response.toString());
//            	            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
//            	            m.put("header"	, docResponse.get("header")); // Y, E
//            	            m.put("message"	, docResponse.get("message"));// msg
//            	            
//                            //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
//                            //m.put("rst"	, response.toString());
//            	        }
//                    	con.disconnect();
//                    	
//                	}else if("R".equals(model.get("ADD_FLAG").toString())){
//                		//일반 주문의 경우 상품마스터 기준 상품관리TYPE이 개체 인 상품은 주문수량만큼 송장 지동 추가발행
//                		//TYPE_ST : 상품정보관리의 상품관리TYPE 이 S 인것 만
//                		if("S".equals(model.get("TYPE_ST"+i))){
//                			//상품별 주문수량만큼 송장 지동 추가발행
//                			for(int j = 0 ; j < Integer.parseInt((String)model.get("ORD_QTY"+i)) ; j ++){
//                            	//Json Data
//                    			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
//            				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
//            				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
//                    									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
//                    									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
//                    									+					",\"PARCEL_BOX_TY\":\""		+model.get("PARCEL_BOX_TY").toString()+"\""
//                    									+					",\"PARCEL_ETC_TY\":\""		+model.get("PARCEL_ETC_TY").toString()+"\""
//                    									+					",\"FR_DATE\":\""			+model.get("FR_DATE").toString()+"\""
//                    									+					",\"TO_DATE\":\""			+model.get("TO_DATE").toString()+"\""
//                    									+					",\"ORD_DEGREE\":\""		+model.get("ORD_DEGREE").toString()+"\""
//	                									+					",\"ORD_PH\":\""			+model.get("ORD_PH").toString()+"\""
//	                									+					",\"ORD_PD\":\""			+model.get("ORD_PD").toString()+"\""
//                    									+					",\"ADD_FLAG\":\""			+"Y"+"\""
//                    									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
//                    									+					",\"ADD_NROW\":\""			+Integer.toString(j+1)+"/"+model.get("ORD_QTY"+i)+"\""
//                    									
//                    									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
//                    									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
//                    									+					",\"RCVR_NM\":\""			+(String)model.get("RCVR_NM"+i)+"\""
//                    									+					",\"RCVR_ADDR\":\""			+(String)model.get("RCVR_ADDR"+i)+"\""
//                    									+					",\"RCVR_DETAIL_ADDR\":\""	+(String)model.get("RCVR_DETAIL_ADDR"+i)+"\""
//                    									+					",\"ORD_ID\":\""			+(String)model.get("ORD_ID"+i)+"\""
//                    									+					",\"ORD_SEQ\":\""			+(String)model.get("ORD_SEQ"+i)+"\""
//                    									+					",\"DATA_SENDER_NM\":\""	+(String)model.get("DATA_SENDER_NM"+i)+"\""
//                    									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\"}}";
//                    			//System.out.println(jsonInputString);
//                    			
//                            	//ssl disable
//                            	disableSslVerification();
//                    	        //System.out.println("sUrl : " + sUrl);
//                    	        
//                    	        URL url = null; 
//                    	        url = new URL(sUrl);
//                    	        
//                    	        HttpsURLConnection con = null;
//                            	con = (HttpsURLConnection) url.openConnection();
//                            	
//                            	//웹페이지 로그인 권한 적용
//                            	String userpass = "eaiuser01" + ":" + "eaiuser01";
//                            	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());
//
//                            	con.setRequestProperty ("Authorization", basicAuth);
//                            	con.setDoInput(true);
//                            	con.setDoOutput(true);
//                            	con.setRequestMethod("POST");
//                            	con.setConnectTimeout(0);
//                            	con.setReadTimeout(0);
//                            	con.setRequestProperty("Content-Type", "application/json");
//                    			con.setRequestProperty("Accept", "application/json");
//                    			
//                    			//JSON 보내는 Output stream
//                    	        try(OutputStream os = con.getOutputStream()) {
//                    	            byte[] input = jsonInputString.getBytes("utf-8");
//                    	            os.write(input, 0, input.length);
//                    	        }
//                    	        
//                    	        //Response data 받는 부분
//                    	        
//                    	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
//                    	            StringBuilder response = new StringBuilder();
//                    	            String responseLine = null;
//                    	            while ((responseLine = br.readLine()) != null) {
//                    	                response.append(responseLine.trim());
//                    	            }
//                    	            
//                    	            //System.out.println(response.toString());
//                    	            
//                    	            JSONObject jsonData = new JSONObject(response.toString());
//                    	            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
//                    	            m.put("header"	, docResponse.get("header")); // Y, E
//                    	            m.put("message"	, docResponse.get("message"));// msg
//                    	            
//                                    //JSONObject jsonDataBody = new JSONObject(jsonData.get("body"));
//                                    //m.put("rst"	, response.toString());
//                    	        }
//                            	con.disconnect();
//                    		}
//                		}else{
//            	           //주문중에 상품관리TYPE 이 S 없을 경우 (상단 결과 전달 )
//                 			if(model.get("header") != null && !model.get("header").equals("")){
//                 				m.put("header"	, model.get("header")); // Y, E
//                 	            m.put("message"	, model.get("message"));// msg
//                 			}else{
//                 				m.put("header"	, "Y"); // Y, E
//                 	            m.put("message"	, "[성공] 접수완료.");// msg
//                 			}
//                 			
//                		}
//                	}
//                } catch(Exception e){
//                    throw e;
//                }
//            }
//        } catch(Exception e){
//            throw e;
//        }
        return m;
    }
    

    /**
     * 
     * 대체 Method ID	: DlvInvcNoOrderComm
     * 대체 Method 설명	: 택배접수 (검수 화면 & 택배접수관리 화면 공통 사용 ) 건건히 수행  
     * 작성자			: yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvInvcNoOrderComm(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         
    	 
    	 String hostUrl		= (String)model.get("hostUrl");
         String eaiUrl 	= "";
         String sUrl    = "";
     	 if(hostUrl != null && hostUrl.equals("localhost")){
     		eaiUrl 	= "https://172.31.10.253:5201/"; //실운영(local에서 접근)
         }else{
         	eaiUrl 	= "https://52.79.206.98:5201/"; //실운영
         }
    	 
     	 if(model.get("PARCEL_COM_TY").equals("04")){//대한통운
     		sUrl = eaiUrl + "PARCEL/CJLOGIS/COMM/ORDER/V2";
      	 }else if (model.get("PARCEL_COM_TY").equals("06")){//로젠택배
      		sUrl = eaiUrl + "PARCEL/LOGENLOGIS/COMM/ORDER/V2";
      	 }else if (model.get("PARCEL_COM_TY").equals("08")){//롯데택배
       		sUrl = eaiUrl + "PARCEL/LOTTELOGIS/COMM/ORDER/V2";
       	 }
     		 
         try{
        	 
    		//송장 추가발행 시 기 발행 송장번호 삭제 로직은 배제한다.
        	if("N".equals(model.get("ADD_FLAG").toString())){
        		
      			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
              		//체크박스 전체에서 송장 접수를 했을 때 이미 송장번호가 있으면 송장접수 시 WMSDF110 테이블의 이력 삭제 선행 처리 (DEL_YN = Y)
              		try{
              			if(model.get("INVC_NO"+i) != null && !model.get("INVC_NO"+i).equals("")){
          				//if(model.get("INVC_NO"+i).toString().length() != 0 && !StringUtils.isEmpty((String)model.get("INVC_NO"+i))){
                  			Map<String, Object> modelDel = new HashMap<String, Object>();
                  			modelDel.put("ORD_ID"    	, model.get("ORD_ID"+i));
                  			modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
                  			modelDel.put("INVC_NO"		, model.get("INVC_NO"+i));
                  			modelDel.put("LC_ID"	    , model.get("LC_ID"+i));
                  			modelDel.put("DLV_COMP_CD"	, model.get("PARCEL_COM_TY"));
                              
                             dao.wmsdf110DelYnUpdate(modelDel);
                  		//}
              			}
                      } catch(Exception e){
                          throw e;
                      }
      			}
          	}

          	//운임비금액
          	String parcelPayPrice = "";
      		if(model.get("PARCEL_PAY_PRICE") != null && !model.get("PARCEL_PAY_PRICE").equals("")){
      			parcelPayPrice  = model.get("PARCEL_PAY_PRICE").toString();
      		}
      		//박스구분
      		String parcelBoxTy = "";
      		if(model.get("PARCEL_BOX_TY") != null && !model.get("PARCEL_BOX_TY").equals("")){
      			parcelBoxTy  = model.get("PARCEL_BOX_TY").toString();
      		}
      		//기타구분
      		String parcelEtcTy = "";
      		if(model.get("PARCEL_ETC_TY") != null && !model.get("PARCEL_ETC_TY").equals("")){
      			parcelEtcTy  = model.get("PARCEL_ETC_TY").toString();
      		}
      		//옵션1
     		String optionType1 = "";
     		if(model.get("OPT_1") != null && !model.get("OPT_1").equals("")){
     			optionType1  = model.get("OPT_1").toString();
     		}
      		
      		
     		ArrayList<String> DenseValList = new ArrayList<String>();
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	try{
            		
            		/*1)N:일반*/
                 	if("N".equals(model.get("ADD_FLAG").toString())){
                 		
            		
			     		/* (DIV_FLAG : DIV) 박스 넘버링으로 나누기(개체, 총량 사용 안함 ), 수량쪼개기 */
			      		//1. 검수화면 
			     		//2. 택배 접수관리 화면  -  옵션 : 박스추천별 송장접수 시 사용 
			      		if( "DIV".equals(model.get("DIV_FLAG").toString()) ){
	      			
			      			/* 그룹핑처리 */
		                 	//그룹화
				     		Boolean checkRun = true;
	             			//일반송장발행 시
	                 		//총량상품
	                 		String denseVal = model.get("DENSE_FLAG"+i).toString();
	             			for (String denseCompare : DenseValList) { // 송장 중복건 제외 
	             		        if (denseCompare.equals(denseVal)) {
	             		        	checkRun = false; //중복 있으면 수행X
	             		        	break;
	             		        }
	             		    }
	                		
	             			//그룹화. 
	             			if(denseVal != null && !denseVal.equals("") && checkRun ){
	             				
	             				String boxNo = "";
	             				
	             				//JSON ORD_INFO
	             				DenseValList.add(model.get("DENSE_FLAG"+i).toString());
	                 			JSONObject jsonObject = new JSONObject();
	        					JSONArray ord_seq_array = new JSONArray();
	                 			for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
	                 				String denseCompare =  model.get("DENSE_FLAG"+j).toString();
	                 				if(denseCompare != null && !denseCompare.equals("") && denseVal.equals(denseCompare)){
	                 					JSONObject data = new JSONObject();
	        	    					data.put("ORD_ID", (String)model.get("ORD_ID"+j));
	        	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
	        	    					data.put("ORD_QTY", (String)model.get("ORD_QTY"+j));
	        	    					//data.put("TRACKING_NO", (String)model.get("TRACKING_NO"+j));
	        	    					//page_val : 수량쪼개기일경우 송장출력시 로케이션 할당 부분에 활용 - seq 상품기준으로 몇개 송장의 몇번째인지(1 / 3)   
	        	    					if(model.get("TOTALPAGE"+j) != null && !model.get("TOTALPAGE"+j).equals("")){
	        	    						data.put("ADD_NROW", (String)model.get("TOTALPAGE"+j));
	        	    					}
	        	    					//parcelBoxTy : 박스추천인경우 송장별로 박스코드가 다르기때문, 
	        	    					if(model.get("PARCEL_BOX_TY"+j) != null && !model.get("PARCEL_BOX_TY"+j).equals("")){
	        	    						parcelBoxTy = (String)model.get("PARCEL_BOX_TY"+j);
	        	    					}
	        	    					//boxNo : gubun 박스순번 
	        	    					boxNo = (String)model.get("TRACKING_NO"+j) ; 
	        	    					ord_seq_array.add(data);
	                 				}
	                 			}
	        				    jsonObject.put("ORD_INFO", ord_seq_array);
	    				        String AddDataJson = ord_seq_array.toString();
	    				        
	
	                         	//Json Data
	                 			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
	         				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
	         				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
	                 									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
	                 									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
	                 									+					",\"PARCEL_PAY_PRICE\":\""	+parcelPayPrice+"\""
	                 									+					",\"PARCEL_BOX_TY\":\""		+parcelBoxTy+"\""
	                 									+					",\"PARCEL_ETC_TY\":\""		+parcelEtcTy+"\""
	                 									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
	                 									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
	                 									+					",\"OPT_1\":\""				+optionType1+"\""
	                 									+					",\"BOX_NO\":\""			+boxNo+"\""
	                 									+					",\"WORK_TYPE\":\""			+"DIV\""
	                 									
	                 									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
	                 									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
	                 									+					",\"ORD_INFO\":"			+ AddDataJson
	                 									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\""
	                 									+ 					"}"
	                 									+ ",\"PUB_HOST_NAME\": 	\""+ConstantWSIF.PUB_HOST_NAME+"\"}";
	                 			//System.out.println("DIV jsonInputString(T)"+ jsonInputString);
	                 			
	                 			m =  connectionDlvEAI(sUrl, jsonInputString, m);
	                 			
	             			}
	        
	             			
	             			
	             		/* (DIV_FLAG : NORMAL) [기본 ] 개체, 총량 사용하여 로직 태움.  */
			      		}else{
			      		
			      			/* 그룹핑처리 */
	                 		//그룹화 
		    	     		Boolean checkRun = true;
	                 		
	             			//일반송장발행 시
	                 		//총량상품
	                     	if("T".equals(model.get("TYPE_ST"+i))){  
	                     		
	                     		String denseVal = model.get("DENSE_FLAG"+i).toString();
	                 			for (String denseCompare : DenseValList) { // 송장 중복건 제외 
	                 		        if (denseCompare.equals(denseVal)) {
	                 		        	checkRun = false; //중복 있으면 수행X
	                 		        	break;
	                 		        }
	                 		    }
	                    		
	                 			//총량인 경우 그룹화. 
	                 			if(denseVal != null && !denseVal.equals("") && checkRun ){
	                 				
	                 				String boxNo = "";
	                 				
	                 				//JSON ORD_INFO
	                 				DenseValList.add(model.get("DENSE_FLAG"+i).toString());
	                     			JSONObject jsonObject = new JSONObject();
	            					JSONArray ord_seq_array = new JSONArray();
	                     			for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
	                     				String denseCompare =  model.get("DENSE_FLAG"+j).toString();
	                     				if(denseCompare != null && !denseCompare.equals("") && denseVal.equals(denseCompare)){
	                     					JSONObject data = new JSONObject();
	            	    					data.put("ORD_ID", (String)model.get("ORD_ID"+j));
	            	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
	            	    					data.put("ORD_QTY", (String)model.get("ORD_QTY"+j));
	            	    					data.put("ADD_NROW", "1/1");
	            	    					//data.put("TRACKING_NO", (String)model.get("TRACKING_NO"+j)); //gubun : 박스순번
	            	    					ord_seq_array.add(data);
	            	    					
	            	    					//boxNo : gubun 박스순번 
	            	    					boxNo = (String)model.get("TRACKING_NO"+j) ; 
	                     				}
	                     			}
	            				    jsonObject.put("ORD_INFO", ord_seq_array);
	        				        String AddDataJson = ord_seq_array.toString();
	        				        
	
	                             	//Json Data
	                     			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
	             				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
	             				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
	                     									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
	                     									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
	                     									+					",\"PARCEL_PAY_PRICE\":\""	+parcelPayPrice+"\""
	                     									+					",\"PARCEL_BOX_TY\":\""		+parcelBoxTy+"\""
	                     									+					",\"PARCEL_ETC_TY\":\""		+parcelEtcTy+"\""
	                     									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
	                     									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
	                     									+					",\"OPT_1\":\""				+optionType1+"\""
	                     									+					",\"BOX_NO\":\""			+boxNo+"\""
	                     									+					",\"WORK_TYPE\":\""			+"T\""
	                     									
	                     									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
	                     									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
	                     									+					",\"ORD_INFO\":"			+ AddDataJson
	                     									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\""
	                     									+ 					"}"
	                     									+ ",\"PUB_HOST_NAME\": 	\""+ConstantWSIF.PUB_HOST_NAME+"\"}";
//	                     			System.out.println("jsonInputString(T)"+ jsonInputString);
	                     			
	                     			m =  connectionDlvEAI(sUrl, jsonInputString, m);
	        				        
	                 			}
	
	                 			
	                 	
	                     	}else if("S".equals(model.get("TYPE_ST"+i))){
	                     	/* 단행처리 */
	                 		//일반 주문의 경우 상품마스터 기준 상품관리TYPE이 개체 인 상품은 주문수량만큼 송장 지동 추가발행
	                     	//TYPE_ST : 개체(S) 수량만큼 송장 접수 
	                     	//개체인경우 최대수량만큼  -> 추가 송장처럼 진행함
	                     		
	                 			//개체상품의  주문수량/포장 단위 수량 (default : 1) 만큼 송장 지동 추가발행
	                 			int ItemTypeSInvcCnt = Integer.parseInt((String)model.get("TYPE_S_INVC_CNT"+i));
	                 			int CBMQty = Integer.parseInt((String)model.get("CBM_QTY"+i));
	                 			int outOrdQty = Integer.parseInt((String)model.get("ORD_QTY"+i));
	                 			
	                 			for(int j = 0 ; j < ItemTypeSInvcCnt ; j ++){
	                 				int ordQty = 0;
	                 				if( outOrdQty-(CBMQty*(j+1)) > 0 ){
	                 					ordQty = CBMQty;
	                 				}else{
	                 					ordQty = outOrdQty - (CBMQty*j) ;
	                 				}
	                 				
	                 				//JSON ORD_INFO
	                     			JSONObject jsonObject = new JSONObject();
	            					JSONArray ord_seq_array = new JSONArray();
	             					JSONObject data = new JSONObject();
	    	    					data.put("ORD_ID", (String)model.get("ORD_ID"+i));
	    	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+i));
	    	    					data.put("CBM_QTY", CBMQty+""); //string 변환    CBM_QTY : 개체로 인한, 수량쪼개기 발생. 송장출력시 로케이션 할당 부분에 활용
	    	    					data.put("ORD_QTY", ordQty+""); //string 변환
	    	    					data.put("ADD_NROW", Integer.toString(j+1)+"/"+ItemTypeSInvcCnt );  // page_val : 1/4
	    	    					//data.put("TRACKING_NO", (String)model.get("TRACKING_NO"+i));// gubun 박스순번
	    	    					ord_seq_array.add(data);
	            				    jsonObject.put("ORD_INFO", ord_seq_array);
	        				        String AddDataJson = ord_seq_array.toString();
	        				        //boxNo : gubun 박스순번 
	        				        String boxNo = (String)model.get("TRACKING_NO"+i); 
	
	        				        //System.out.println(j+1 +"TYPE_S_INVC_CNT:"+ItemTypeSInvcCnt+ "/ CBMQty : "+CBMQty+" / ordQty: " + ordQty);
	                 				
	                             	//Json Data
	                     			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
	             				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
	             				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
	                     									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
	                     									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
	                     									+					",\"PARCEL_PAY_PRICE\":\""	+parcelPayPrice+"\""
	                     									+					",\"PARCEL_BOX_TY\":\""		+parcelBoxTy+"\""
	                     									+					",\"PARCEL_ETC_TY\":\""		+parcelEtcTy+"\""
	                     									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""  										 // 개체 인지 확인위한 구분자 => INVC_ADD_FLAG 에 입력됨.(N, Y, S)
	                     									//+					",\"ADD_NROW\":\""			+Integer.toString(j+1)+"/"+ItemTypeSInvcCnt+"\"" // page_val : 1/4
	                     									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
	                     									+					",\"OPT_1\":\""				+optionType1+"\""
	                     									+					",\"BOX_NO\":\""			+boxNo+"\""
	                     									+					",\"WORK_TYPE\":\""			+"S\""
	                     									
	                     									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
	                     									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
	                     									+					",\"ORD_INFO\":"			+ AddDataJson
	                     									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\""
	                     									+ 					"}"
	                     									+ ",\"PUB_HOST_NAME\": 	\""+ConstantWSIF.PUB_HOST_NAME+"\"}";
	                     			
//	                     			System.out.println("jsonInputString(S) : " +jsonInputString);
	                     			
	                     			m =  connectionDlvEAI(sUrl, jsonInputString, m);
	                 			}
	                     	}//if(T or S)
	              		}//if(DIV or NOMAL)	
	                 
			      		
			      		
			      	/*2)Y:추가송장버튼*/
	                }else if("Y".equals(model.get("ADD_FLAG").toString())){
                 		//추가송장발행 시 - 건건히 모두 접수 ( 송장 존재시, 삭제 작업 은 SKIP ) 
                 		
                 		//JSON ORD_INFO
             			JSONObject jsonObject = new JSONObject();
    					JSONArray ord_seq_array = new JSONArray();
     					JSONObject data = new JSONObject();
    					data.put("ORD_ID", (String)model.get("ORD_ID"+i));
    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+i));
    					data.put("ORD_QTY", (String)model.get("ORD_QTY"+i));
				        if(model.get("TOTALPAGE"+i) != null && !model.get("TOTALPAGE"+i).equals("")){
				        	data.put("ADD_NROW", (String)model.get("TOTALPAGE"+i) );
    					}
    					//data.put("TRACKING_NO", (String)model.get("TRACKING_NO"+i));
    					if(model.get("PARCEL_BOX_TY"+i) != null && !model.get("PARCEL_BOX_TY"+i).equals("")){
    						parcelBoxTy = (String)model.get("PARCEL_BOX_TY"+i);
    					}
    					ord_seq_array.add(data);
    				    jsonObject.put("ORD_INFO", ord_seq_array);
				        String AddDataJson = ord_seq_array.toString();
				        
				        
				        //boxNo : gubun 박스순번 -> 추가송장에서는 의미 없음 
				        String boxNo = (String)model.get("TRACKING_NO"+i); 
				        
                     	//Json Data
             			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
     				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
     				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
             									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
             									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
             									+					",\"PARCEL_PAY_PRICE\":\""	+parcelPayPrice+"\""
             									+					",\"PARCEL_BOX_TY\":\""		+parcelBoxTy+"\""
             									+					",\"PARCEL_ETC_TY\":\""		+parcelEtcTy+"\""
             									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
             									//+					",\"ADD_NROW\":\""			+addNRow+"\""
             									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
                     							+					",\"OPT_1\":\""				+optionType1+"\""
                     							+					",\"BOX_NO\":\""			+boxNo+"\""
                     							+					",\"WORK_TYPE\":\""			+"ADD\""
             									
             									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
             									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
             									+					",\"RCVR_NM\":\""			+(String)model.get("RCVR_NM"+i)+"\""
             									+					",\"ORD_INFO\":"			+ AddDataJson
             									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\""
             									+ 					"}"
             									+ ",\"PUB_HOST_NAME\": 	\""+ConstantWSIF.PUB_HOST_NAME+"\"}";
             			
//	             			System.out.println("jsonInputString(ADD) : " +jsonInputString);
             			
             			m =  connectionDlvEAI(sUrl, jsonInputString, m);
             			
                 	
	                }//if(N or Y)
                 } catch(Exception e){
                     throw e;
                 }
             }//for
         } catch(Exception e){
             throw e;
         }
         return m;
    }
    
    

    /**
     * 
     * 대체 Method ID	: DlvInvcNoOrderCommTotal 
     * 대체 Method 설명	: 택배접수 (검수 화면 & 택배접수관리 화면 공통 사용 ) total 전송 
     * 작성자			: yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvInvcNoOrderCommTotal(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         
    	 String hostUrl		= (String)model.get("hostUrl");
         String eaiUrl 	= "";
         String sUrl    = "";
     	 if(hostUrl != null && hostUrl.equals("localhost")){
     		eaiUrl 	= "https://172.31.10.253:5201/"; //실운영(local에서 접근)
         }else{
         	eaiUrl 	= "https://52.79.206.98:5201/"; //실운영
         }
    	 
     	 if(model.get("PrevYN") == null){
     		 if(model.get("PARCEL_COM_TY").equals("04")){//대한통운
          		//sUrl = eaiUrl + "PARCEL/CJLOGIS/COMM/ORDER/V2"; //건건히 eai 전송
          		sUrl = eaiUrl + "PARCEL/CJLOGIS/COMM/TOTALORDER";
           	 }else if (model.get("PARCEL_COM_TY").equals("06")){//로젠택배
           		//sUrl = eaiUrl + "PARCEL/LOGENLOGIS/COMM/ORDER/V2";
           		sUrl = eaiUrl + "PARCEL/LOGENLOGIS/COMM/TOTALORDER";
           	 }else if (model.get("PARCEL_COM_TY").equals("08")){//롯데택배
           		//sUrl = eaiUrl + "PARCEL/LOTTELOGIS/COMM/ORDER/V2";
            	sUrl = eaiUrl + "PARCEL/LOTTELOGIS/COMM/TOTALORDER";
           	 }
     	 }else if(model.get("PrevYN") == "Y"){
     		 if(model.get("PARCEL_COM_TY").equals("04")){//대한통운
     			 sUrl = eaiUrl + "PARCEL/CJLOGIS/COMM/TOTALORDER/PREVPRINT";
        	 }
     	 }
     	
     		 
         try{
        	 
    		//송장 추가발행 시 기 발행 송장번호 삭제 로직은 배제한다.
        	if("N".equals(model.get("ADD_FLAG").toString())){
        		
      			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
              		//체크박스 전체에서 송장 접수를 했을 때 이미 송장번호가 있으면 송장접수 시 WMSDF110 테이블의 이력 삭제 선행 처리 (DEL_YN = Y)
              		try{
              			if(model.get("INVC_NO"+i) != null && !model.get("INVC_NO"+i).equals("")){
          				//if(model.get("INVC_NO"+i).toString().length() != 0 && !StringUtils.isEmpty((String)model.get("INVC_NO"+i))){
                  			Map<String, Object> modelDel = new HashMap<String, Object>();
                  			modelDel.put("ORD_ID"    	, model.get("ORD_ID"+i));
                  			modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
                  			modelDel.put("INVC_NO"		, model.get("INVC_NO"+i));
                  			modelDel.put("LC_ID"	    , model.get("LC_ID"+i));
                  			modelDel.put("DLV_COMP_CD"	, model.get("PARCEL_COM_TY"));
                              
                             dao.wmsdf110DelYnUpdate(modelDel);
                  		//}
              			}
                      } catch(Exception e){
                          throw e;
                      }
      			}
          	}

          	//운임비금액
          	String parcelPayPrice = "";
      		if(model.get("PARCEL_PAY_PRICE") != null && !model.get("PARCEL_PAY_PRICE").equals("")){
      			parcelPayPrice  = model.get("PARCEL_PAY_PRICE").toString();
      		}
      		//박스구분
      		String parcelBoxTy = "";
      		if(model.get("PARCEL_BOX_TY") != null && !model.get("PARCEL_BOX_TY").equals("")){
      			parcelBoxTy  = model.get("PARCEL_BOX_TY").toString();
      		}
      		//기타구분
      		String parcelEtcTy = "";
      		if(model.get("PARCEL_ETC_TY") != null && !model.get("PARCEL_ETC_TY").equals("")){
      			parcelEtcTy  = model.get("PARCEL_ETC_TY").toString();
      		}
      		//옵션1
     		String optionType1 = "";
     		if(model.get("OPT_1") != null && !model.get("OPT_1").equals("")){
     			optionType1  = model.get("OPT_1").toString();
     		}
     		//순서1
     		String sortType1 = "";
     		if(model.get("SORT_1") != null && !model.get("SORT_1").equals("")){
     			sortType1  = model.get("SORT_1").toString();
     		}
     		
     		
//     		//시간체크용
//     		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
//			Date date = new Date(System.currentTimeMillis());
//			System.out.println("START  : " + formatter.format(date));

			//TOTAL JSON
			JSONObject jsonDoc  = new JSONObject();
			JSONArray jsonTotalOrder_array = new JSONArray();
      		
     		ArrayList<String> DenseValList = new ArrayList<String>();
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	try{
            		
            		/*1)N:일반*/
                 	if("N".equals(model.get("ADD_FLAG").toString())){
            		
                 		
                 		/* (DIV_FLAG : DIV) 박스 넘버링으로 나누기(개체, 총량 사용 안함 ), 수량쪼개기 */
	              		//1. 검수화면 
	             		//2. 택배 접수관리 화면  -  옵션 : 박스추천별 송장접수 시 사용 
	              		if( "DIV".equals(model.get("DIV_FLAG").toString()) ){
              			
	              			/* 그룹핑처리 */
		                 	//그룹화
				     		Boolean checkRun = true;
	             			//일반송장발행 시
	                 		//총량상품
	                 		String denseVal = model.get("DENSE_FLAG"+i).toString();
	             			for (String denseCompare : DenseValList) { // 송장 중복건 제외 
	             		        if (denseCompare.equals(denseVal)) {
	             		        	checkRun = false; //중복 있으면 수행X
	             		        	break;
	             		        }
	             		    }
	                		
	             			//그룹화. 
	             			if(denseVal != null && !denseVal.equals("") && checkRun ){
	             				
	             				String boxNo = "";
	             				
	             				//JSON ORD_INFO
	             				DenseValList.add(model.get("DENSE_FLAG"+i).toString());
	                 			JSONObject jsonObject = new JSONObject();
	        					JSONArray ord_seq_array = new JSONArray();
	                 			for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
	                 				String denseCompare =  model.get("DENSE_FLAG"+j).toString();
	                 				if(denseCompare != null && !denseCompare.equals("") && denseVal.equals(denseCompare)){
	                 					JSONObject data = new JSONObject();
	        	    					data.put("ORD_ID", (String)model.get("ORD_ID"+j));
	        	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
	        	    					data.put("ORD_QTY", (String)model.get("ORD_QTY"+j));
	        	    					//data.put("TRACKING_NO", (String)model.get("TRACKING_NO"+j));
	        	    					//page_val : 수량쪼개기일경우 송장출력시 로케이션 할당 부분에 활용 - seq 상품기준으로 몇개 송장의 몇번째인지(1 / 3)   
	        	    					if(model.get("TOTALPAGE"+j) != null && !model.get("TOTALPAGE"+j).equals("")){
	        	    						data.put("ADD_NROW", (String)model.get("TOTALPAGE"+j));
	        	    					}
	        	    					//parcelBoxTy : 박스추천인경우 송장별로 박스코드가 다르기때문, 
	        	    					if(model.get("PARCEL_BOX_TY"+j) != null && !model.get("PARCEL_BOX_TY"+j).equals("")){
	        	    						parcelBoxTy = (String)model.get("PARCEL_BOX_TY"+j);
	        	    					}
	        	    					//boxNo : gubun 박스순번 
	        	    					boxNo = (String)model.get("TRACKING_NO"+j) ; 
	        	    					ord_seq_array.add(data);
	                 				}
	                 			}
	        				    jsonObject.put("ORD_INFO", ord_seq_array);
	    				        String AddDataJson = ord_seq_array.toString();
	
	                         	//Json Data
//	                 			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
//	         				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
//	         				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
//	                 									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
//	                 									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
//	                 									+					",\"PARCEL_PAY_PRICE\":\""	+parcelPayPrice+"\""
//	                 									+					",\"PARCEL_BOX_TY\":\""		+parcelBoxTy+"\""
//	                 									+					",\"PARCEL_ETC_TY\":\""		+parcelEtcTy+"\""
//	                 									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
//	                 									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
//	                 									+					",\"OPT_1\":\""				+optionType1+"\""
//	                 									+					",\"BOX_NO\":\""			+boxNo+"\""
//	                 									
//	                 									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
//	                 									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
//	                 									+					",\"ORD_INFO\":"			+ AddDataJson
//	                 									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\"}}";
//	                 			//System.out.println("DIV jsonInputString(T)"+ jsonInputString);
//	                 			
//	                 			m =  connectionDlvEAI(sUrl, jsonInputString, m);

        				        JSONObject jsonObj = new JSONObject();
        				        jsonObj.put("PARCEL_SEQ_YN",		model.get("PARCEL_SEQ_YN").toString());
        				        jsonObj.put("PARCEL_COM_TY", 		model.get("PARCEL_COM_TY").toString());
        				        jsonObj.put("PARCEL_COM_TY_SEQ", 	model.get("PARCEL_COM_TY_SEQ").toString());
        				        jsonObj.put("PARCEL_ORD_TY", 		model.get("PARCEL_ORD_TY").toString());
	        				    jsonObj.put("PARCEL_PAY_TY", 		model.get("PARCEL_PAY_TY").toString());
	        				    jsonObj.put("PARCEL_PAY_PRICE",		parcelPayPrice);
	        				    jsonObj.put("PARCEL_BOX_TY", 		parcelBoxTy);
	        				    jsonObj.put("PARCEL_ETC_TY", 		parcelEtcTy);
	        				    jsonObj.put("ADD_FLAG", 			model.get("ADD_FLAG").toString());
	        				    jsonObj.put("DIV_FLAG", 			model.get("DIV_FLAG").toString());
	        				    jsonObj.put("OPT_1", 				optionType1);
	        				    jsonObj.put("SORT_1", 				sortType1);
	        				    jsonObj.put("BOX_NO", 				boxNo);
	        				    jsonObj.put("WORK_TYPE", 			"DIV");
	        				    jsonObj.put("LC_ID", 				(String)model.get("LC_ID"+i));
	        				    jsonObj.put("CUST_ID", 				(String)model.get("CUST_ID"+i));
	        				    jsonObj.put("ORD_INFO", 			ord_seq_array);//AddDataJson
	        				    jsonObj.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
	        				    jsonTotalOrder_array.add(jsonObj);
	             			}
	        				    
	        				    
	        			/* (DIV_FLAG : NORMAL) [기본 ] 개체, 총량 사용하여 로직 태움.  */
	              		}else{
	              			
	                 		/* 그룹핑처리 */
	                 		//그룹화 
	              			Boolean checkRun = true;
	                 		
	             			//일반송장발행 시
	                 		//총량상품
	                     	if("T".equals(model.get("TYPE_ST"+i))){  
	                     		
	                     		String denseVal = model.get("DENSE_FLAG"+i).toString();
	                 			for (String denseCompare : DenseValList) { // 송장 중복건 제외 
	                 		        if (denseCompare.equals(denseVal)) {
	                 		        	checkRun = false; //중복 있으면 수행X
	                 		        	break;
	                 		        }
	                 		    }
	                    		
	                 			//총량인 경우 그룹화. 
	                 			if(denseVal != null && !denseVal.equals("") && checkRun ){
	                 				
	                 				String boxNo = "";
	                 				
	                 				//JSON ORD_INFO
	                 				DenseValList.add(model.get("DENSE_FLAG"+i).toString());
	                     			JSONObject jsonObject = new JSONObject();
	            					JSONArray ord_seq_array = new JSONArray();
	                     			for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
	                     				String denseCompare =  model.get("DENSE_FLAG"+j).toString();
	                     				if(denseCompare != null && !denseCompare.equals("") && denseVal.equals(denseCompare)){
	                     					JSONObject data = new JSONObject();
	            	    					data.put("ORD_ID", (String)model.get("ORD_ID"+j));
	            	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
	            	    					data.put("ORD_QTY", (String)model.get("ORD_QTY"+j));
	            	    					data.put("ADD_NROW", "1/1");
	            	    					//data.put("TRACKING_NO", (String)model.get("TRACKING_NO"+j)); //gubun : 박스순번
	            	    					ord_seq_array.add(data);
	            	    					
	            	    					//boxNo : gubun 박스순번 
	            	    					boxNo = (String)model.get("TRACKING_NO"+j) ; 
	                     				}
	                     			}
	            				    jsonObject.put("ORD_INFO", ord_seq_array);
	        				        String AddDataJson = ord_seq_array.toString();
	        				        
	
	                             	//Json Data
//	                     			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
//	             				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
//	             				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
//	                     									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
//	                     									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
//	                     									+					",\"PARCEL_PAY_PRICE\":\""	+parcelPayPrice+"\""
//	                     									+					",\"PARCEL_BOX_TY\":\""		+parcelBoxTy+"\""
//	                     									+					",\"PARCEL_ETC_TY\":\""		+parcelEtcTy+"\""
//	                     									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
//	                     									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
//	                     									+					",\"OPT_1\":\""				+optionType1+"\""
//	                     									+					",\"BOX_NO\":\""			+boxNo+"\""
//	                     									
//	                     									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
//	                     									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
//	                     									+					",\"ORD_INFO\":"			+ AddDataJson
//	                     									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\"}}";
////	                     			System.out.println("jsonInputString(T)"+ jsonInputString);
//	                     			
//	                     			m =  connectionDlvEAI(sUrl, jsonInputString, m);
	        				        

	        				        JSONObject jsonObj = new JSONObject();
	        				        jsonObj.put("PARCEL_SEQ_YN",		model.get("PARCEL_SEQ_YN").toString());
	        				        jsonObj.put("PARCEL_COM_TY", 		model.get("PARCEL_COM_TY").toString());
	        				        jsonObj.put("PARCEL_COM_TY_SEQ", 	model.get("PARCEL_COM_TY_SEQ").toString());
	        				        jsonObj.put("PARCEL_ORD_TY", 		model.get("PARCEL_ORD_TY").toString());
		        				    jsonObj.put("PARCEL_PAY_TY", 		model.get("PARCEL_PAY_TY").toString());
		        				    jsonObj.put("PARCEL_PAY_PRICE",		parcelPayPrice);
		        				    jsonObj.put("PARCEL_BOX_TY", 		parcelBoxTy);
		        				    jsonObj.put("PARCEL_ETC_TY", 		parcelEtcTy);
		        				    jsonObj.put("ADD_FLAG", 			model.get("ADD_FLAG").toString());
		        				    jsonObj.put("DIV_FLAG", 			model.get("DIV_FLAG").toString());
		        				    jsonObj.put("OPT_1", 				optionType1);
		        				    jsonObj.put("SORT_1", 				sortType1);
		        				    jsonObj.put("BOX_NO", 				boxNo);
		        				    jsonObj.put("WORK_TYPE", 			"T");
		        				    jsonObj.put("LC_ID", 				(String)model.get("LC_ID"+i));
		        				    jsonObj.put("CUST_ID", 				(String)model.get("CUST_ID"+i));
		        				    jsonObj.put("ORD_INFO", 			ord_seq_array);//AddDataJson
		        				    jsonObj.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
		        				    jsonTotalOrder_array.add(jsonObj);
	                     			
	                 			}
	
	                 			
	                 	
	                     	}else if("S".equals(model.get("TYPE_ST"+i))){
	                     	/* 단행처리 */
	                 		//일반 주문의 경우 상품마스터 기준 상품관리TYPE이 개체 인 상품은 주문수량만큼 송장 지동 추가발행
	                     	//TYPE_ST : 개체(S) 수량만큼 송장 접수 
	                     	//개체인경우 최대수량만큼  -> 추가 송장처럼 진행함
	                     		
	                 			//개체상품의  주문수량/포장 단위 수량 (default : 1) 만큼 송장 지동 추가발행
	                 			int ItemTypeSInvcCnt = Integer.parseInt((String)model.get("TYPE_S_INVC_CNT"+i));
	                 			int CBMQty = Integer.parseInt((String)model.get("CBM_QTY"+i));
	                 			int outOrdQty = Integer.parseInt((String)model.get("ORD_QTY"+i));
	                 			
	                 			for(int j = 0 ; j < ItemTypeSInvcCnt ; j ++){
	                 				int ordQty = 0;
	                 				if( outOrdQty-(CBMQty*(j+1)) > 0 ){
	                 					ordQty = CBMQty;
	                 				}else{
	                 					ordQty = outOrdQty - (CBMQty*j) ;
	                 				}
	                 				
	                 				//JSON ORD_INFO
	                     			JSONObject jsonObject = new JSONObject();
	            					JSONArray ord_seq_array = new JSONArray();
	             					JSONObject data = new JSONObject();
	    	    					data.put("ORD_ID", (String)model.get("ORD_ID"+i));
	    	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+i));
	    	    					data.put("CBM_QTY", CBMQty+""); //string 변환    CBM_QTY : 개체로 인한, 수량쪼개기 발생. 송장출력시 로케이션 할당 부분에 활용
	    	    					data.put("ORD_QTY", ordQty+""); //string 변환
	    	    					data.put("ADD_NROW", Integer.toString(j+1)+"/"+ItemTypeSInvcCnt );  // page_val : 1/4
	    	    					//data.put("TRACKING_NO", (String)model.get("TRACKING_NO"+i));// gubun 박스순번
	    	    					ord_seq_array.add(data);
	            				    jsonObject.put("ORD_INFO", ord_seq_array);
	        				        String AddDataJson = ord_seq_array.toString();
	        				        //boxNo : gubun 박스순번 
	        				        String boxNo = (String)model.get("TRACKING_NO"+i); 
	
	        				        //System.out.println(j+1 +"TYPE_S_INVC_CNT:"+ItemTypeSInvcCnt+ "/ CBMQty : "+CBMQty+" / ordQty: " + ordQty);
	                 				
	                             	//Json Data
//	                     			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
//	             				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
//	             				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
//	                     									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
//	                     									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
//	                     									+					",\"PARCEL_PAY_PRICE\":\""	+parcelPayPrice+"\""
//	                     									+					",\"PARCEL_BOX_TY\":\""		+parcelBoxTy+"\""
//	                     									+					",\"PARCEL_ETC_TY\":\""		+parcelEtcTy+"\""
//	                     									+					",\"ADD_FLAG\":\""			+"S"+"\""  										 // 개체 인지 확인위한 구분자 => INVC_ADD_FLAG 에 입력됨.(N, Y, S)
//	                     									+					",\"ADD_NROW\":\""			+Integer.toString(j+1)+"/"+ItemTypeSInvcCnt+"\"" // page_val : 1/4
//	                     									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
//	                     									+					",\"OPT_1\":\""				+optionType1+"\""
//	                     									+					",\"BOX_NO\":\""			+boxNo+"\""
//	                     									
//	                     									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
//	                     									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
//	                     									+					",\"ORD_INFO\":"			+ AddDataJson
//	                     									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\"}}";
//	                     			
////	                     			System.out.println("jsonInputString(S) : " +jsonInputString);
//	                     			
//	                     			m =  connectionDlvEAI(sUrl, jsonInputString, m);
	        				        
	        				        JSONObject jsonObj = new JSONObject();
	        				        jsonObj.put("PARCEL_SEQ_YN",		model.get("PARCEL_SEQ_YN").toString());
	        				        jsonObj.put("PARCEL_COM_TY", 		model.get("PARCEL_COM_TY").toString());
	        				        jsonObj.put("PARCEL_COM_TY_SEQ", 	model.get("PARCEL_COM_TY_SEQ").toString());
	        				        jsonObj.put("PARCEL_ORD_TY", 		model.get("PARCEL_ORD_TY").toString());
		        				    jsonObj.put("PARCEL_PAY_TY", 		model.get("PARCEL_PAY_TY").toString());
		        				    jsonObj.put("PARCEL_PAY_PRICE",		parcelPayPrice);
		        				    jsonObj.put("PARCEL_BOX_TY", 		parcelBoxTy);
		        				    jsonObj.put("PARCEL_ETC_TY", 		parcelEtcTy);
		        				    jsonObj.put("ADD_FLAG", 			model.get("ADD_FLAG").toString());// 개체 인지 확인위한 구분자 => INVC_ADD_FLAG 에 입력됨.(N, Y, S)
		        				    //jsonObj.put("ADD_NROW", 			Integer.toString(j+1)+"/"+ItemTypeSInvcCnt); // page_val : 1/4
		        				    jsonObj.put("DIV_FLAG", 			model.get("DIV_FLAG").toString());
		        				    jsonObj.put("OPT_1", 				optionType1);
		        				    jsonObj.put("SORT_1", 				sortType1);
		        				    jsonObj.put("BOX_NO", 				boxNo);
		        				    jsonObj.put("WORK_TYPE", 			"S");
		        				    jsonObj.put("LC_ID", 				(String)model.get("LC_ID"+i));
		        				    jsonObj.put("CUST_ID", 				(String)model.get("CUST_ID"+i));
		        				    jsonObj.put("ORD_INFO", 			ord_seq_array);//AddDataJson
		        				    jsonObj.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
		        				    jsonTotalOrder_array.add(jsonObj);
	                 			}
	                     	
	                     	}//if(T or S)
	              		}//if(DIV or NOMAL)	

	              		
	             			
	             	/*2)Y:추가송장버튼*/
                 	}else if("Y".equals(model.get("ADD_FLAG").toString())){
                 		//(X) 기존 :  추가송장발행 시 - 건건히 모두 접수 ( 송장 존재시, 삭제 작업 은 SKIP ) 
                 		//2023-01-25  택배 추가송장 접수 >  합포기준으로 추가송장 발행 로직 수정
                 		//2023-01-25 S(객체)는 현재 분리되지 않고 합포로 추가송장 발행 중..
                 		
                 		/* 그룹핑처리 */
                 		//그룹화 
              			Boolean checkRun = true;
                 		
                 		String denseVal = model.get("DENSE_FLAG"+i).toString();
             			for (String denseCompare : DenseValList) { // 송장 중복건 제외 
             		        if (denseCompare.equals(denseVal)) {
             		        	checkRun = false; //중복 있으면 수행X
             		        	break;
             		        }
             		    }
             			
             			//합포기준으로  그룹화. 
             			if(denseVal != null && !denseVal.equals("") && checkRun ){
             				
             				String boxNo = "";
             				
             				//JSON ORD_INFO
             				DenseValList.add(model.get("DENSE_FLAG"+i).toString());
                 			JSONObject jsonObject = new JSONObject();
        					JSONArray ord_seq_array = new JSONArray();
                 			for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
                 				String denseCompare =  model.get("DENSE_FLAG"+j).toString();
                 				if(denseCompare != null && !denseCompare.equals("") && denseVal.equals(denseCompare)){
                 					JSONObject data = new JSONObject();
        	    					data.put("ORD_ID", (String)model.get("ORD_ID"+j));
        	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
        	    					data.put("ORD_QTY", (String)model.get("ORD_QTY"+j));
        	    					if(model.get("PAGE_VAL"+j) != null && !model.get("PAGE_VAL"+j).equals("")){
        	    						data.put("ADD_NROW", (String)model.get("PAGE_VAL"+j));
        	    					}
        	    					//parcelBoxTy : 송장별로 박스코드가 다를경우?
        	    					if(model.get("PARCEL_BOX_TY"+j) != null && !model.get("PARCEL_BOX_TY"+j).equals("")){
        	    						parcelBoxTy = (String)model.get("PARCEL_BOX_TY"+j);
        	    					}
        	    					ord_seq_array.add(data);
        	    					
        	    					//boxNo : gubun 박스순번 
        	    					boxNo = (String)model.get("TRACKING_NO"+j) ; 
                 				}
                 			}
        				    jsonObject.put("ORD_INFO", ord_seq_array);
    				        String AddDataJson = ord_seq_array.toString();
    				        
             			
	             			JSONObject jsonObj = new JSONObject();
					        jsonObj.put("PARCEL_SEQ_YN",		model.get("PARCEL_SEQ_YN").toString());
					        jsonObj.put("PARCEL_COM_TY", 		model.get("PARCEL_COM_TY").toString());
					        jsonObj.put("PARCEL_COM_TY_SEQ", 	model.get("PARCEL_COM_TY_SEQ").toString());
					        jsonObj.put("PARCEL_ORD_TY", 		model.get("PARCEL_ORD_TY").toString());
	    				    jsonObj.put("PARCEL_PAY_TY", 		model.get("PARCEL_PAY_TY").toString());
	    				    jsonObj.put("PARCEL_PAY_PRICE",		parcelPayPrice);
	    				    jsonObj.put("PARCEL_BOX_TY", 		parcelBoxTy);
	    				    jsonObj.put("PARCEL_ETC_TY", 		parcelEtcTy);
	    				    jsonObj.put("ADD_FLAG", 			model.get("ADD_FLAG").toString());
	    				    //jsonObj.put("ADD_NROW", 			addNRow);
	    				    jsonObj.put("DIV_FLAG", 			model.get("DIV_FLAG").toString());
	    				    jsonObj.put("OPT_1", 				optionType1);
	    				    jsonObj.put("SORT_1", 				sortType1);
	    				    jsonObj.put("BOX_NO", 				boxNo);
	    				    jsonObj.put("WORK_TYPE", 			"ADD");
	    				    jsonObj.put("LC_ID", 				(String)model.get("LC_ID"+i));
	    				    jsonObj.put("CUST_ID", 				(String)model.get("CUST_ID"+i));
	    				    jsonObj.put("ORD_INFO", 			ord_seq_array);//AddDataJson
	    				    jsonObj.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
	    				    jsonTotalOrder_array.add(jsonObj);
             			}
                 	
                 	}//if(N or Y)
				} catch(Exception e){
					throw e;
				}
	        }//for
      		
            jsonDoc.put("PUB_HOST_NAME", ConstantWSIF.PUB_HOST_NAME);	
      		jsonDoc.put("docRequest", jsonTotalOrder_array);
			String jsonInputString = jsonDoc.toString();
			
			//System.out.println("jsonInputString(Total) : " + jsonInputString);
			m =  connectionDlvEAI(sUrl, jsonInputString, m);		
			
//			//시간체크용
//			date = new Date(System.currentTimeMillis());
//			System.out.println("END  : " +  formatter.format(date));

         } catch(Exception e){
             throw e;
         }
         return m;
    }
    
    
    /**
     * 
     * 대체 Method ID	 : custAddrRefineEAI 
     * 대체 Method 설명 : custAddrRefineEAI 
     * 작성자			: yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custAddrRefineEAI(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		 
		String hostUrl		= (String)model.get("hostUrl");
		String eaiUrl 	= "";
		if(hostUrl != null && hostUrl.equals("localhost")){
			eaiUrl 	= "https://172.31.10.253:5201/"; //실운영(local에서 접근)
		}else{
		 	eaiUrl 	= "https://52.79.206.98:5201/"; //실운영
		}
		String sUrl = eaiUrl + "PARCEL/COMM/ADDR_REFINE/TOTAL";
     	 
		try{
			//TOTAL JSON
			JSONObject jsonTotalDoc  = new JSONObject();
			JSONArray jsonTotalOrder_array = new JSONArray();
			
			JSONObject jsonDoc04  = new JSONObject();
			JSONArray jsonOrder04_array = new JSONArray(); //대한통운만 주소정재 재처리 되어있음 
			JSONObject jsonDocEtc  = new JSONObject();
			JSONArray jsonOrderEtc_array = new JSONArray();
			int cnt = Integer.parseInt(model.get("selectIds_2").toString());
			if(cnt> 0){
				
			    for(int i = 0 ; i < Integer.parseInt(model.get("selectIds_2").toString()) ; i ++){
					
					JSONObject jsonObj = new JSONObject();
				    jsonObj.put("CUSTOMER_KEY", 		model.get("CUSTOMER_KEY"+i).toString());
				    jsonObj.put("INVC_NO", 				model.get("INVC_NO"+i).toString());
					String parcelComTy = model.get("PARCEL_COM_TY"+i).toString();
					if(parcelComTy.equals("04")){
						jsonOrder04_array.add(jsonObj);
					}else{
						jsonOrderEtc_array.add(jsonObj);
					}
				}
				
				if(jsonOrder04_array.size() > 0){
					jsonDoc04.put("PARCEL_COM_TY", "04");
					jsonDoc04.put("LC_ID",				(String)model.get(ConstantIF.SS_SVC_NO));
					jsonDoc04.put("CUST_ID", 			model.get("vrSrchCustId").toString());
					jsonDoc04.put("ADDRESS_H", 			model.get("custInfoAddr1").toString());
					jsonDoc04.put("ADDRESS_D", 			model.get("custInfoAddr2").toString());
					jsonDoc04.put("USER_NO", 			(String)model.get(ConstantIF.SS_USER_NO));
					jsonDoc04.put("PARCEL_LIST", jsonOrder04_array);
					jsonTotalOrder_array.add(jsonDoc04);
				}
				if(jsonOrderEtc_array.size() > 0){
					jsonDocEtc.put("PARCEL_COM_TY", "ETC");
					jsonDocEtc.put("LC_ID",				(String)model.get(ConstantIF.SS_SVC_NO));
					jsonDocEtc.put("CUST_ID", 			model.get("vrSrchCustId").toString());
					jsonDocEtc.put("ADDRESS_H", 		model.get("custInfoAddr1").toString());
					jsonDocEtc.put("ADDRESS_D", 		model.get("custInfoAddr2").toString());
					jsonDocEtc.put("USER_NO", 			(String)model.get(ConstantIF.SS_USER_NO));
					jsonDocEtc.put("PARCEL_LIST", jsonOrderEtc_array);
					jsonTotalOrder_array.add(jsonDocEtc);
				}
				
	        	jsonTotalDoc.put("PUB_HOST_NAME", ConstantWSIF.PUB_HOST_NAME);
	        	jsonTotalDoc.put("docRequest", jsonTotalOrder_array);
	      		String jsonInputString = jsonTotalDoc.toString();
				
	      		//System.out.println("jsonInputString(AddrRefineEAI) : " + jsonInputString);
				m =  connectionDlvEAI(sUrl, jsonInputString, m);	
           
			}else{
				m.put("header", "Y");
            }

		} catch(Exception e){
			throw e;
		}
     	return m;
    }

    /**
     * 
     * 대체 Method ID	: DlvShipmentPostTotal 
     * 대체 Method 설명	: 택배송장접수 (후행) total 전송 
     * 작성자			: yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvShipmentPostTotal(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 
    	 String hostUrl		= (String)model.get("hostUrl");
         String eaiUrl 	= "";
         String sUrl    = "";
     	 if(hostUrl != null && hostUrl.equals("localhost")){
     		eaiUrl 	= "https://172.31.10.253:5201/"; //실운영(local에서 접근)
         }else{
         	eaiUrl 	= "https://52.79.206.98:5201/"; //실운영
         }
    	 
 		 if(model.get("PARCEL_COM_TY").equals("04")){//대한통운
      		sUrl = eaiUrl + "PARCEL/CJLOGIS/COMM/TOTALORDER/POST";
       	 }else if (model.get("PARCEL_COM_TY").equals("06")){//로젠택배
       	 }else if (model.get("PARCEL_COM_TY").equals("08")){//롯데택배
       	 }
     	
     		 
         try{
     		
//     		//시간체크용
//     		SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
//			Date date = new Date(System.currentTimeMillis());
//			System.out.println("START  : " + formatter.format(date));

			//TOTAL JSON
			JSONObject jsonDoc  = new JSONObject();
			JSONArray jsonTotalOrder_array = new JSONArray();
      		
     		ArrayList<String> DenseValList = new ArrayList<String>();
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	try{
            		
            		//※박스구분, 박스순번, 수량, 운송구분, 운임구분, 옵션 최신화 하여 접수 진행.
            		/*1)N:일반*/
                 	if("N".equals(model.get("ADD_FLAG").toString())){
            		
              			/* 그룹핑처리 */
                 		//송장기준으로 그룹화.
			     		Boolean checkRun = true;
                 		
                 		String denseVal = model.get("INVC_NO"+i).toString();
             			for (String denseCompare : DenseValList) { // 송장 중복건 제외 
             		        if (denseCompare.equals(denseVal)) {
             		        	checkRun = false; //중복 있으면 수행X
             		        	break;
             		        }
             		    }
             			if(denseVal != null && !denseVal.equals("") && checkRun ){
             				
             				String boxNo = "";
             				
             				//JSON ORD_INFO
             				DenseValList.add(model.get("INVC_NO"+i).toString());
                 			JSONObject jsonObject = new JSONObject();
        					JSONArray ord_seq_array = new JSONArray();
                 			for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
                 				String denseCompare =  model.get("INVC_NO"+j).toString();
                 				if(denseCompare != null && !denseCompare.equals("") && denseVal.equals(denseCompare)){
                 					JSONObject data = new JSONObject();
        	    					data.put("ORD_ID", (String)model.get("ORD_ID"+j));
        	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
        	    					ord_seq_array.add(data);
                 				}
                 			}
        				    jsonObject.put("ORD_INFO", ord_seq_array);

    				        JSONObject jsonObj = new JSONObject();
    				        jsonObj.put("INVC_NO", 				denseVal);
    				        jsonObj.put("PARCEL_COM_TY", 		model.get("PARCEL_COM_TY").toString());
    				        jsonObj.put("PARCEL_BOX_TY", 		model.get("PARCEL_BOX_TY").toString());
        				    jsonObj.put("LC_ID", 				(String)model.get("LC_ID"+i));
        				    jsonObj.put("CUST_ID", 				(String)model.get("CUST_ID"+i));
        				    jsonObj.put("ORD_INFO", 			ord_seq_array);//AddDataJson
        				    jsonObj.put("USER_NO", 				(String)model.get(ConstantIF.SS_USER_NO));
        				    jsonTotalOrder_array.add(jsonObj);
             			}
	        		
                 	}//if(N or Y)
				} catch(Exception e){
					throw e;
				}
	        }//for
      		
      		jsonDoc.put("docRequest", jsonTotalOrder_array);			
      		jsonDoc.put("I_MODIFY_CODE", "");// ""  : 수정없이 기존정보 그대로 송장 접수  / BOX : PARCEL_BOX_TY 만 변경하여 접수
			String jsonInputString = jsonDoc.toString();
			
//			System.out.println("jsonInputString(POST) : " + jsonInputString);
			m =  connectionDlvEAI(sUrl, jsonInputString, m);		
			
//			//시간체크용
//			date = new Date(System.currentTimeMillis());
//			System.out.println("END  : " +  formatter.format(date));

         } catch(Exception e){
             throw e;
         }
         return m;
    }
    

    /**
     * 
     * 대체 Method ID	: DlvShipmentRtnComm
     * 대체 Method 설명	: 택배접수  반품( 택배접수관리 화면 )
     * 작성자			: yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvShipmentRtnComm(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 String hostUrl		= (String)model.get("hostUrl");
         String eaiUrl 	= "";
         String sUrl    = "";
     	 if(hostUrl != null && hostUrl.equals("localhost")){
     		eaiUrl 	= "https://172.31.10.253:5201/"; //실운영(local에서 접근)
         }else{
         	eaiUrl 	= "https://52.79.206.98:5201/"; //실운영
         }
     	 
     	 if(model.get("PARCEL_COM_TY").equals("04")){//대한통운
          	sUrl = eaiUrl +  "PARCEL/CJLOGIS/COMM/RTN/ORDER";
          	
     	 }else if(model.get("PARCEL_COM_TY").equals("08")){//롯데택배
          	sUrl = eaiUrl +  "PARCEL/LOTTELOGIS/COMM/RTN/ORDER";
          	
     	 }else if(model.get("PARCEL_COM_TY").equals("06")){//로젠택배
          	sUrl = eaiUrl +  "PARCEL/LOGENLOGIS/COMM/RTN/ORDER";
     	 }
     	
         try{
         	//기송장 삭제 로직 필요시 넣기. 
        	 
        	 
        	//운임비금액
          	String parcelPayPrice = "";
      		if(model.get("PARCEL_PAY_PRICE") != null && !model.get("PARCEL_PAY_PRICE").equals("")){
      			parcelPayPrice  = model.get("PARCEL_PAY_PRICE").toString();
      		}
      		//박스구분
      		String parcelBoxTy = "";
      		if(model.get("PARCEL_BOX_TY") != null && !model.get("PARCEL_BOX_TY").equals("")){
      			parcelBoxTy  = model.get("PARCEL_BOX_TY").toString();
      		}
      		//기타구분
      		String parcelEtcTy = "";
      		if(model.get("PARCEL_ETC_TY") != null && !model.get("PARCEL_ETC_TY").equals("")){
      			parcelEtcTy  = model.get("PARCEL_ETC_TY").toString();
      		}
      		        	 
         	//반품로직
        	//String invcNoPrev = "";
        	ArrayList<String> invcNoList = new ArrayList<String>();
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
             	try{
             		Boolean checkRun = true;
             		
             		//HTML엔티티 문자 변환
             		//String rcvrAddr = (String) model.get("RCVR_ADDR"+i);
             		//String rcvrAddrConvert = org.springframework.web.util.HtmlUtils.htmlUnescape(rcvrAddr);
             		//String rcvrDetailAddr = (String) model.get("RCVR_DETAIL_ADDR"+i);
             		//String rcvrDetailAddrConvert = org.springframework.web.util.HtmlUtils.htmlUnescape(rcvrDetailAddr);
            		
             		String invcNo = model.get("INVC_NO"+i).toString();
         			for (String invcNoCompare : invcNoList) { // 송장 중복건 제외 
         		        if (invcNoCompare.equals(invcNo)) {
         		        	checkRun = false;
         		        	break;
         		        }
         		    }
         			
             		if(invcNo != null && !invcNo.equals("") && checkRun ){
             			
             			invcNoList.add(model.get("INVC_NO"+i).toString());
             			JSONObject jsonObject = new JSONObject();
    					JSONArray ord_seq_array = new JSONArray();
             			
             			for(int j = 0 ; j < Integer.parseInt(model.get("selectIds").toString()) ; j ++){
             				String invcNoCompare =  model.get("INVC_NO"+j).toString();
             				if(invcNoCompare != null && !invcNoCompare.equals("") && invcNo.equals(invcNoCompare)){
             					//JSON NEW
             					JSONObject data = new JSONObject();
    	    					data.put("ORD_ID", (String)model.get("ORD_ID"+j));
    	    					data.put("ORD_SEQ", (String)model.get("ORD_SEQ"+j));
    	    					data.put("ORD_QTY", (String)model.get("ORD_QTY"+j));
    	    					ord_seq_array.add(data);
             				}
             			}
             		
    				    jsonObject.put("ORD_INFO", ord_seq_array);
				        String AddDataJson = ord_seq_array.toString();
             		
                     	//Json Data
             			String jsonInputString = "{\"docRequest\":	 {\"PARCEL_SEQ_YN\":\""		+model.get("PARCEL_SEQ_YN").toString()+"\""
     				        					+					",\"PARCEL_COM_TY\":\""		+model.get("PARCEL_COM_TY").toString()+"\""				
     				        					+					",\"PARCEL_COM_TY_SEQ\":\""	+model.get("PARCEL_COM_TY_SEQ").toString()+"\""
             									+					",\"PARCEL_ORD_TY\":\""		+model.get("PARCEL_ORD_TY").toString()+"\""
             									+					",\"PARCEL_PAY_TY\":\""		+model.get("PARCEL_PAY_TY").toString()+"\""
             									+					",\"PARCEL_PAY_PRICE\":\""	+parcelPayPrice+"\""
             									+					",\"PARCEL_BOX_TY\":\""		+parcelBoxTy+"\""
             									+					",\"PARCEL_ETC_TY\":\""		+parcelEtcTy+"\""
             									+					",\"ADD_FLAG\":\""			+model.get("ADD_FLAG").toString()+"\""
             									+					",\"DIV_FLAG\":\""			+model.get("DIV_FLAG").toString()+"\""
             									
             									+					",\"LC_ID\":\""				+(String)model.get("LC_ID"+i)+"\""
             									+					",\"CUST_ID\":\""			+(String)model.get("CUST_ID"+i)+"\""
             									+					",\"INVC_NO\":\""			+ invcNo +"\""
             									+					",\"USER_NO\":\""		    +(String)model.get(ConstantIF.SS_USER_NO)+"\""
             									+                   ",\"ETC2\":\""              +(model.containsKey("ETC2") ? (String)model.get("ETC2").toString().replaceAll("\"", "") : "")+"\""
             									+					",\"ORD_INFO\":"			+ AddDataJson
             									+ 					"}"
             									+ ",\"PUB_HOST_NAME\": 	\""+ConstantWSIF.PUB_HOST_NAME+"\"}";
             			System.out.println("jsonInputString(RTN) : "+ jsonInputString);
             			
                     	m =  connectionDlvEAI(sUrl, jsonInputString, m);

             		}
             		
                 } catch(Exception e){
                     throw e;
                 }
             }
            
            
         } catch(Exception e){
             throw e;
         }
         return m;
    }
    
    
    public Map<String, Object> connectionDlvEAI (String sUrl, String jsonInputString, Map<String, Object> m) throws Exception {
    	//ssl disable
     	disableSslVerification();
        //System.out.println("sUrl : " + sUrl);
        
        URL url = null; 
        url = new URL(sUrl);
        
        HttpsURLConnection con = null;
     	con = (HttpsURLConnection) url.openConnection();
     	
     	//웹페이지 로그인 권한 적용
     	String userpass = "eaiuser01" + ":" + "eaiuser01";
     	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

     	con.setRequestProperty ("Authorization", basicAuth);
     	con.setDoInput(true);
     	con.setDoOutput(true);
     	con.setRequestMethod("POST");
     	con.setConnectTimeout(0);
     	con.setReadTimeout(0);
     	con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
		
		//JSON 보내는 Output stream
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        
        //Response data 받는 부분
        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            //System.out.println(response.toString());
            
            JSONObject jsonData = new JSONObject(response.toString());
            JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
            Iterator iterator = docResponse.keys();
            while(iterator.hasNext()){
                String key = (String)iterator.next();
                //System.out.println("iterator .. :  "+key);
                if(key.equals("header"))  m.put("header" , docResponse.get("header").toString());// Y, E
                if(key.equals("message")) m.put("message", docResponse.get("message").toString());// msg
                //------ 추가 ------ 
                if(key.equals("poino")) m.put("POI_NO", docResponse.get("poino").toString());//poino
            }
            
        }
	        
     	con.disconnect();
     	
     	return m;
    }
   
    

    /**
     * 
     * 대체 Method ID	: DlvInvcNoPrintPre
     * 대체 Method 설명	: 택배송장출력(선행) 
     * 작성자			: yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvInvcNoPrintPre(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
         
    	
         return m;
    }
    

    /**
     * 
     * 대체 Method ID		: DlvShipmenHanjin
     * 대체 Method 설명	: 택배접수(한진택배) 
     * 작성자				: dhkim
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvShipmentHanjin(Map<String, Object> model) throws Exception {
    	 Map<String, Object> m = new HashMap<String, Object>();
    	 Map<String, Object> sendDataInfos = new HashMap<String, Object>(); 	// 정보성 데이터 집합
    	 List<String> sendDataKeyList = new ArrayList<String>();				// 수하인 기준정보
    	 String hostUrl		= (String)model.get("hostUrl");
         String eaiUrl 	= "";
         
     	 if(hostUrl != null && hostUrl.equals("localhost")){
     		eaiUrl 	= "https://172.31.10.253:5201/"; //실운영(local에서 접근)
         }else{
         	eaiUrl 	= "https://52.79.206.98:5201/"; //실운영
         }     	      
    	 
         try{
         	//송장 추가발행 시 기 발행 송장번호 삭제 로직은 배제한다.
         	if("N".equals(model.get("ADD_FLAG").toString())){
     			for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
             		//체크박스 전체에서 송장 접수를 했을 때 이미 송장번호가 있으면 접수 시 WMSDF110 테이블의 이력 삭제 선행 처리 (DEL_YN = Y)
             		try{
                 		if (model.get("INVC_NO"+i).toString().length() != 0 && !StringUtils.isEmpty((String)model.get("INVC_NO"+i))){
                 			Map<String, Object> modelDel = new HashMap<String, Object>();
                 			modelDel.put("ORD_ID"    	, model.get("ORD_ID"+i));
                 			modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
                 			modelDel.put("INVC_NO"		, model.get("INVC_NO"+i));
                 			modelDel.put("LC_ID"	    , model.get("LC_ID"+i));
                 			modelDel.put("DLV_COMP_CD"	, model.get("PARCEL_COM_TY")); // HanJin CD : 05
                             
                            dao.wmsdf110DelYnUpdate(modelDel);
                 		}
                     } catch(Exception e){
                         throw e;
                     }
     			}
         	}
         	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
             	// 분리접수 구분
            	String sUrl = eaiUrl +  "PARCEL/HJLOGIS/COMM/ORDER";			// 한진 Service Flow 호출
//             	String sUrl = eaiUrl +  "PARCEL/HJLOGIS/COMM/ORDER_COMM";		// 한진 운영환경 Service Flow 호출
//             	String sUrl = eaiUrl +  "PARCEL/HJLOGIS/COMM/ORDER_COMM_DEV";	// 한진 개발환경 Service Flow 호출
                String addFlag = model.get("ADD_FLAG").toString();				// (Y: 송장추가접수 , N: 택배배송접수)
                String procFlag = model.get("PROC_FLAG" + i).toString();		// 수화인정보(이름,주소)와 전송회사정보가 같은 복수행의 존재여부 (조회쿼리 참조)
                String procType = model.get("TYPE_ST" + i).toString();			// 상품타입 : 상품 기준정보 (T:총량, S:개체)
//             	int ordQty = Integer.parseInt((String)model.get("ORD_QTY"+i)); 	// 개체수량 : 상품타입에 따라, 개체수량당 운송장번호 발행

             	String ordId		= model.get("ORD_ID"+ i).toString();					// 주문번호
             	String comTy 		= model.get("PARCEL_COM_TY").toString();				// 택배구분
             	String comTySeq 	= model.get("PARCEL_COM_TY_SEQ").toString();			// 보내는이              	
             	String addr 		= model.get("RCVR_ADDR" + i).toString();		   		// 수하인 주소1(동이상 주소)
             	String addrDetail 	= model.get("RCVR_DETAIL_ADDR" + i).toString();			// 수하인 주소2(동미만 주소)
             	String tel 			= (model.get("RCVR_TEL" + i).toString()).replace("-", "");		   			// 수하인 번호
             	String name 		= model.get("RCVR_NM" + i).toString();					// 수하인명
             	String trackingNo 	= model.get("TRACKING_NO" + i).toString();				// 박스번호
             	             	
//             	String dataKey =  comTy + "_" +  comTySeq + "_" + ordId + "_" + addr + "_" + addrDetail + "_" + name;
             	String dataKey =  comTy + "_" +  comTySeq + "_" + trackingNo + "_" + tel + "_" + addr + "_" + addrDetail + "_" + name;

             	System.out.println(String.format("[DEBUG] addFlag : %s, procFlag : %s, procType : %s", addFlag, procFlag, procType));
             	System.out.println(String.format("[DEBUG] dataKey : %s", dataKey));
             	             	               
                // 일반 접수(N)의 경우
                if(("N").equals(addFlag))
                {
                	// 수하인정보가 같은 행의 경우 한 건만 발행	
                	if("T".equals(procType) && sendDataKeyList.indexOf(dataKey) < 0){                		
                		 m = sendDlvOrder(model, sUrl, i, -1, "N");
                		sendDataKeyList.add(dataKey);             
                	}
                	
                    // 상품마스터 기준 상품관리TYPE이 개체(S) 인 상품은 주문수량만큼 송장발행
                	if("S".equals(procType)){
                		
                		int ItemTypeSInvcCnt = Integer.parseInt((String)model.get("TYPE_S_INVC_CNT"+i));  	// 개체상품이 갖는 총 운송장수량
             			int CBMQty = Integer.parseInt((String)model.get("CBM_QTY"+i));						// 한 박스에 적재되는 최대수량
             			int outOrdQty = Integer.parseInt((String)model.get("OUT_ORD_QTY"+i));				// 총 주문수량
             			
                     	// 개체상품의  주문수량/포장 단위 수량 (default : 1) 만큼 송장 지동 추가발행
             			System.out.println(String.format("[DEBUG] ItemTypeSInvcCnt : %s, CBMQty : %s, outOrdQty : %s", ItemTypeSInvcCnt, CBMQty, outOrdQty));
             			
//                		for(int j = 0 ; j < ordQty ; j ++){
//                    		 m = sendDlvOrder(model, sUrl, i, j, "N");
//                    	}
                		for(int j = 0 ; j < ItemTypeSInvcCnt ; j ++){
                			int ordQty = 0;
             				if( outOrdQty-(CBMQty*(j+1)) > 0 ){
             					ordQty = CBMQty;
             				}else{
             					ordQty = outOrdQty - (CBMQty*j) ;
             				}
             				System.out.println("[DEBUG] ordQty1 : " + ordQty);
             				model.put("ORD_QTY" + i, ordQty);
             				m = sendDlvOrder(model, sUrl, i, j, "N");
                    	}
                	}                	
                }    
                // 추가송장발행(Y)
                else{
                	 m = sendDlvOrder(model, sUrl, i, -1, "Y");
                }               
             }
            
            sendDataInfos.put("SEND_KEY", sendDataKeyList);
            
         } catch(Exception e){
             throw e;
         }
         return m;
    }
    
    
    /** 
     * 택배 주문접수 인터페이스 호출 및 응답처리
     * 
     * @param model			: 원본 데이터
     * @param sUrl			: EAI URL 경로
     * @param rowIdx		: UI기준 처리 행 번호
     * @param ordQtyIdx		: 대상 상품의 타입이 개체(S)인 경우, 개체수량
     * @param addFlag		: 접수구분 (Y:추가접수, N:일반접수)
     * @return
     * @throws Exception
     */
    private static Map<String, Object> sendDlvOrder(Map<String, Object> model, String sUrl, int rowIdx, int ordQtyIdx, String addFlag) throws Exception
    {
    	Map<String, Object> m = new HashMap<String, Object>();
    	
    	// Set Parameter 
    	String jsonInputString = "{\"docInsertOrd\": {"
				+					"\"EDI_COD\":\""			+model.get("EDI_CODE").toString() +  "\""				// EDI코드 (한진제공)
				+					",\"API_KEY\":\""			+model.get("API_KEY").toString() + "\""					// API 키
				+					",\"ORD_NUM\":\""			+model.get("ORD_ID"+ rowIdx).toString() + "\""			// 주문번호
				+					",\"WBL_NUM\":\"" 			+ "\""													// 운송장번호 (공란 / 반품처리시 사용)
				+					",\"DLV_DIV\":\""			+model.get("COM_DLV_DIV").toString()+"\""				// 배송구분
				+					",\"CUS_NUM\":\""			+model.get("CSR_NUM").toString() +  "\""				// 고객 신용번호 (한진제공)
				+					",\"PIC_YMD\":\""			+model.get("OUT_REQ_DT" + rowIdx).toString().replace("-", "") +"\""		// 상품출고일(집하요청일자)
				+					",\"SND_ZIP\":\""			+model.get("SENDR_ZIP_NO").toString()+"\""				// 송하인 우편번호            					
				+					",\"SND_AD1\":\""			+model.get("SENDR_ADDR1").toString()+"\""				// 송하인 주소1(동이상 주소)
				+					",\"SND_AD2\":\""			+model.get("SENDR_ADDR2").toString()+"\""				// 송하인 주소2(동미만 주소)
				+					",\"SND_NAM\":\""			+model.get("SENDR_NM").toString()+"\""					// 송하인명
				+					",\"SND_TEL\":\""			+model.get("SENDR_TEL_NO").toString()+"\""				// 송하인 전화번호
				+					",\"SND_HPH\":\""			+ "\""													// 송하인 핸드폰번호
				+					",\"SND_CRG\":\""			+model.get("SENDR_NM").toString()+"\""					// 송하인 담당자명(?)
				+					",\"RCV_ZIP\":\""			+model.get("ZIP_NO" + rowIdx).toString()+"\""			// 수하인 우편번호
				+					",\"RCV_AD1\":\""			+model.get("RCVR_ADDR" + rowIdx).toString()+"\""		// 수하인 주소1(동이상 주소)
				+					",\"RCV_AD2\":\""			+model.get("RCVR_DETAIL_ADDR" + rowIdx).toString()+"\""	// 수하인 주소2(동미만 주소)
				+					",\"RCV_NAM\":\""			+model.get("RCVR_NM" + rowIdx).toString()+"\""			// 수하인명
				+					",\"RCV_TEL\":\""			+(model.get("RCVR_TEL" + rowIdx).toString()).replace("-", "")+"\""			// 수하인 전화번호
				+					",\"RCV_HPH\":\""			+(model.get("RCVR_TEL" + rowIdx).toString()).replace("-", "")+"\""			// 수하인 핸드폰번호
				+					",\"RCV_CRG\":\""			+ "\""													// 수하인 담당자명(?)
				+					",\"DLV_MSG\":\""			+model.get("DLV_MSG1" + rowIdx).toString()+"\""			// 배송메시지
				+					",\"GOD_NAM\":\""			+model.get("ITEM_NM" + rowIdx).toString()+"\""			// 상품명
				+					",\"BOX_QTY\":\""			+"1"+"\""												// 박스수량 (연속송장 개발 이후 측정가능)
//				+					",\"BOX_QTY\":\""			+model.get("ORD_QTY" + rowIdx).toString()+"\""			// 박스수량
				+					",\"PAY_CON\":\""			+model.get("COM_PAY_CON").toString()+"\""				// 지불조건		
				+					",\"BOX_TYP\":\""			+model.get("COM_BOX_TYP").toString()+"\""				// 박스구분
				+					",\"MEM_NO1\":\"" 			+ "\""		
				+					",\"MEM_NO2\":\"" 			+ "\""            					
				+					",\"MEM_NO3\":\"" 			+ "\""		
				+					",\"MEM_NO4\":\"" 			+ "\""		
				+					",\"O_YN\":\"" 				+ "\""	
				+					",\"O_SQLCODE\":\"" 		+ "\""		
				+					",\"O_SQLERRM\":\"" 		+ "\""
				
				+					",\"PARCEL_COM_TY\":\""		+ model.get("PARCEL_COM_TY").toString()+"\""
				+					",\"PARCEL_COM_TY_SEQ\":\""	+ model.get("PARCEL_COM_TY_SEQ").toString()+"\""
				+					",\"PARCEL_SEQ_YN\":\""		+ model.get("PARCEL_SEQ_YN").toString()+"\""
				+					",\"ADD_NROW\":\""			+ (ordQtyIdx < 0 ? "-1" : ordQtyIdx + "/" + model.get("ORD_QTY"+rowIdx)) + "\""				
				+					",\"LC_ID\":\""				+ model.get("LC_ID"+ rowIdx).toString() + "\""
				+					",\"CUST_ID\":\""			+ model.get("CUST_ID"+ rowIdx).toString() + "\""
				+					",\"FR_DATE\":\""			+ model.get("FR_DATE").toString().replace("-", "")+"\""
 				+					",\"TO_DATE\":\""			+ model.get("TO_DATE").toString().replace("-", "")+"\""
 				+					",\"ORD_ID\":\""			+ model.get("ORD_ID"+ rowIdx).toString() + "\""
 				+					",\"ORD_SEQ\":\""			+ model.get("ORD_SEQ"+ rowIdx).toString() + "\""
 				+					",\"ORD_DEGREE\":\""		+ model.get("ORD_DEGREE").toString() + "\""
 				+					",\"ORD_PH\":\""            + model.get("ORD_PH").toString() + "\""
				+					",\"ORD_PD\":\""			+ model.get("ORD_PD").toString() + "\""
				+					",\"DATA_SENDER_NM\":\""	+ model.get("DATA_SENDER_NM"+ rowIdx).toString() + "\""
 				+					",\"ADD_FLAG\":\""			+ addFlag + "\""
 				+					",\"PROC_TYP\":\""			+ model.get("TYPE_ST" + rowIdx).toString() + "\""
 				+					",\"USER_NO\":\""		    + model.get(ConstantIF.SS_USER_NO).toString() + "\""
 				+					",\"ORD_KEY\":\""		    + model.get("ORD_ID"+ rowIdx).toString() + "_"
 																+ model.get("ORD_SEQ"+ rowIdx).toString() + "_"
 																+ model.get("TYPE_ST" + rowIdx).toString() + "\""
 			    +					",\"ORD_QTY\":\""			+ model.get("ORD_QTY" + rowIdx).toString() + "\""
 			    +					",\"TRACKING_NO\":\""		+ model.get("TRACKING_NO" + rowIdx).toString() + "\""
				+ 					"}}"; 		    	
    	
		System.out.println("[DEBUG] SEND DATA :: >>>> " + jsonInputString);
			
     	disableSslVerification();
	        
        URL url = null; 
        url = new URL(sUrl);
	        
        HttpsURLConnection con = null;
     	con = (HttpsURLConnection) url.openConnection();
     	
     	//웹페이지 로그인 권한 적용
     	String userpass = "eaiuser01" + ":" + "eaiuser01";
     	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

     	con.setRequestProperty ("Authorization", basicAuth);
     	con.setDoInput(true);
     	con.setDoOutput(true);
     	con.setRequestMethod("POST");
     	con.setConnectTimeout(0);
     	con.setReadTimeout(0);
     	con.setRequestProperty("Content-Type", "application/json");
		con.setRequestProperty("Accept", "application/json");
 			
		// [Request]
        try(OutputStream os = con.getOutputStream()) {
            byte[] input = jsonInputString.getBytes("utf-8");
            os.write(input, 0, input.length);
        }
        
        // [Response]  	        
        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
            StringBuilder response = new StringBuilder();
            String responseLine = null;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }        	
            
            JSONObject jsonData = new JSONObject(response.toString());
            
            System.out.println("[DEBUG] RECEIVE DATA :: >>>> " +  jsonData);
            
            if(jsonData.has("jsonString"))
            {
            	JSONObject jsonString = new JSONObject(jsonData.get("docResponse").toString());
            	m.put("header"	, jsonString.get("header")); // Y, E
                m.put("message"	, jsonString.get("message"));// msg	            	
            }else if(jsonData.has("docResponse")){
            	JSONObject docResponse = new JSONObject(jsonData.get("docResponse").toString());
                m.put("header"	, docResponse.get("header")); // Y, E
                m.put("message"	, docResponse.get("message"));// msg	
            }
            else{
            	m.put("header"	, jsonData.get("header")); // Y, E
                m.put("message"	, jsonData.get("message"));// msg
            }                     	            

        }
     	con.disconnect();
     	
     	return m;
    }
    
    
    
    /**
     * 
     * 대체 Method ID	: disableSslVerification
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	/**
     * Method ID : saveCsv
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCsv(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.insertCsv(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
    
    /*
	 * Method ID   : dlvInfoInsert / insertDF110
	 * Method 설명 : 택배이력등록 (출고관리 - 용인)
	 * 작성자      : SUMMER HYUN
	 * @param 
	 * @return
	 */
    public Map<String, Object> dlvInfoInsert(Map<String, Object> model) throws Exception {
    	
    	Map<String, Object> m = new HashMap<String, Object>();
         int errCnt = 0;
         
         try{
        	 
             for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                 Map<String, Object> modelDt = new HashMap<String, Object>();
                 modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                 
                 modelDt.put("selectIds" , model.get("selectIds"));
                 modelDt.put("LC_ID"		, (String)model.get(ConstantIF.SS_SVC_NO));
                 modelDt.put("CUST_ID"    , model.get("CUST_ID"+i));
                 
                 modelDt.put("ORD_ID"    	  , model.get("ORD_ID"+i));
                 modelDt.put("ORD_SEQ"     	  , model.get("ORD_SEQ"+i));
                 modelDt.put("DLV_COMP_CD"     , model.get("DLV_COMP_CD"+i));
                 modelDt.put("INVC_NO"     , model.get("ORG_ORD_ID"+i));
                 modelDt.put("SWEET_TRACKER_DLV_CD"     , model.get("DLV_COMP_CD"+i));
                 modelDt.put("ORD_DATE"     , model.get("ORD_DATE"+i));
                
                dao.dlvInfoInsert(modelDt);
             }
             
             m.put("errCnt", errCnt);
             m.put("MSG", MessageResolver.getMessage("save.success"));            
             
         } catch(Exception e){
             throw e;
         }
         return m;
	}
    
    
    /*-
	 * Method ID   : dlvSenderInfo
	 * Method 설명 : 택배배송 송화인정보
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> dlvDlvCompInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DLV_COMP_INFO", dao.dlvDlvCompInfo(model));
		return map;
	}
    
    
    /*-
	 * Method ID   : dlvSenderInfo
	 * Method 설명 : 택배배송 송화인정보
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> dlvSenderInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DLV_SENDER_INFO", dao.dlvSenderInfo(model));
		return map;
	}
    
    /*-
  	 * Method ID   : dlvSenderInfoDetail
  	 * Method 설명 : 택배배송 세분화 송화인정보
  	 * 작성자      : dhkim
  	 * @param 
  	 * @return
  	 */
    public Map<String, Object> dlvSenderInfoDetail(Map<String, Object> model) throws Exception {
  		Map<String, Object> map = new HashMap<String, Object>();
  		map.put("DLV_SENDER_INFO", dao.dlvSenderInfoDetail(model));
  		return map;
  	}
    
    /**
     * 
     * 대체 Method ID		: boxNoUpdate
     * 대체 Method 설명		: boxNoUpdate코드 저장
     * 작성자				: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> boxNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("ORD_ID"    	, model.get("ORD_ID"));
            modelDt.put("ORD_SEQ"    	, model.get("ORD_SEQ"));
            modelDt.put("TRACKING_NO"	, model.get("TRACKING_NO"));
            
            dao.boxNoUpdate(modelDt);
            m.put("errCnt", "0");
            
        } catch(Exception e){
        	m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    public Map<String, Object> dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelSP = new HashMap<String, Object>();
        
        String getTimeStamp = dao.getOracleSysTimeStamp(modelSP);
		if (getTimeStamp != null && !StringUtils.isEmpty(getTimeStamp)) {
			try{
				int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
                String[] ordId  		= new String[tmpCnt];                
                String[] ordSeq 		= new String[tmpCnt];
                String[] invcNo 		= new String[tmpCnt];
                
	            for(int i = 0 ; i < tmpCnt ; i ++){
	            	ordId[i]    	= (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   	= (String)model.get("ORD_SEQ"+i);
                    invcNo[i]   	= (String)model.get("INVC_NO"+i);
	            }
	            
	            //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId"		, ordId);
                modelIns.put("ordSeq"		, ordSeq);
                modelIns.put("invcNo"		, invcNo);
                modelIns.put("printThisNo"	, getTimeStamp);
                modelIns.put("LC_ID"		, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.dlvPrintPoiNoUpdate(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP642", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                
                
                //택배 설정별 운송장 상품명 명칭 설정 정보
                if(model.get("PARCEL_COM_TY_SEQ") != null && !model.get("PARCEL_COM_TY_SEQ").equals("")){
                	m.put("INVC_ITEMNM_INFO", dao.dlvInvcItemNmSetInfo(model));
                }	
                
	            m.put("MSG"			, MessageResolver.getMessage("save.success"));
	            m.put("POI_NO_YN"	, "Y");
	            m.put("POI_NO"		, getTimeStamp);
	            
	        } catch(Exception e){
	        	m.put("MSG"			, MessageResolver.getMessage("save.error"));
	            m.put("POI_NO_YN"	, "N");
	            m.put("POI_NO"		, "");
	            throw e;
	        }
        }
        return m;
    }
    
    

    @Override
    public Map<String, Object> invcNoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                //WHERE 조건
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("LC_ID"     , model.get("LC_ID"+i));
                modelDt.put("CUST_ID"    , model.get("CUST_ID"+i));
                
                modelDt.put("ORD_ID"    	  , model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"     	  , model.get("ORD_SEQ"+i));
                modelDt.put("DLV_COMP_CD"     , model.get("DLV_COMP_CD"+i));
                modelDt.put("ORG_INVC_NO"     , model.get("ORG_INVC_NO"+i));
                if(model.get("SWEET_TRACKER_IF_YN"+i) != null){
                	modelDt.put("SWEET_TRACKER_IF_YN"     , model.get("SWEET_TRACKER_IF_YN"+i));
                }
                
                //UPDATE 조건
                modelDt.put("INVC_NO"     	  , model.get("INVC_NO"+i));
                
                
//                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS100);
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.invcNoUpdate(modelDt);                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    @Override
    public Map<String, Object> invcNoInfoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        String strGubun = "Y";        
        
        try{
        	// Element Validation 
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	
            	Map<String, Object> modelDt = new HashMap<String, Object>();
            	
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                //WHERE 조건
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("LC_ID"     , model.get("LC_ID"+i));
                modelDt.put("CUST_ID"    , model.get("CUST_ID"+i));
                
                modelDt.put("ORD_ID"    	  			, model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"     	  			, model.get("ORD_SEQ"+i));
                modelDt.put("DLV_COMP_CD"     			, model.get("DLV_COMP_CD"+i));
                modelDt.put("SWEET_TRACKER_DLV_CD"      , model.get("SWEET_TRACKER_DLV_CD"+i));
                modelDt.put("SWEET_TRACKER_IF_YN"     	, model.get("SWEET_TRACKER_IF_YN"+i));
                modelDt.put("ORG_INVC_NO"    			, model.get("ORG_INVC_NO"+i));
                modelDt.put("ORG_SWEET_TRACKER_DLV_CD"  , model.get("ORG_SWEET_TRACKER_DLV_CD"+i));
                
                //UPDATE 조건
                modelDt.put("INVC_NO"     	  , model.get("INVC_NO"+i));
                                
//                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSMS100);
                
                // ## KCC 실적전송 여부확인 (실적있는 경우 삭제/수정 불가), 화주 하드코딩 (0000002940) ##
//                if(model.get("LC_ID"+i) != null && "0000002940".equals(model.get("LC_ID"+i).toString()))
//                {
//                	String validMsg = validData(modelDt);
//                	if(validMsg != null && !validMsg.isEmpty())
//                	{
//                		m.put("errCnt", '1');
//                        m.put("MSG", validMsg);
//                        return m;
//                	}
//                }
            }            
   
            // Save Data (Update / Delete)
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	
            	Map<String, Object> modelDt = new HashMap<String, Object>();
            	
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                //WHERE 조건
                modelDt.put("selectIds" , model.get("selectIds"));
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"+i));
                modelDt.put("LC_ID"     , model.get("LC_ID"+i));
                modelDt.put("CUST_ID"    , model.get("CUST_ID"+i));
                
                modelDt.put("ORD_ID"    	  			, model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"     	  			, model.get("ORD_SEQ"+i));
                modelDt.put("DLV_COMP_CD"     			, model.get("DLV_COMP_CD"+i));
                modelDt.put("SWEET_TRACKER_DLV_CD"      , model.get("SWEET_TRACKER_DLV_CD"+i));
                modelDt.put("SWEET_TRACKER_IF_YN"     	, model.get("SWEET_TRACKER_IF_YN"+i));
                modelDt.put("ORG_INVC_NO"    			, model.get("ORG_INVC_NO"+i));
                modelDt.put("ORG_SWEET_TRACKER_DLV_CD"  , model.get("ORG_SWEET_TRACKER_DLV_CD"+i));
                
                //UPDATE 조건
                modelDt.put("INVC_NO"     	  , model.get("INVC_NO"+i));
                
            	if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.invcNoInfoUpdate(modelDt);                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.invcNoInfoDelete(modelDt);                  
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }            
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }    
    
    
    /*-
	 * Method ID	: getCustOrdDegree
	 * Method 설명	: 화주별 주문차수
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustOrdDegree(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("DS_ORD_DEGREE", dao.getCustOrdDegree(model));
		return map;
	}
    
    @Override
    public Map<String, Object> listByDlvSummaryExcel(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
		model.put("pageIndex", "1");
		model.put("pageSize", "60000");
		map.put("LIST", dao.listByDlvSummaryExcel(model));
		return map;
    }
    
    /**
     * 
     * 대체 Method ID	: DlvShipDelete
     * 대체 Method 설명	: 택배접수
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> DlvShipDelete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	//송장 추가발행 시 기 발행 송장번호 삭제 로직은 배제한다.
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
        		//체크박스 전체에서 송장 접수를 했을 때 이미 송장번호가 있으면 CJ접수 시 WMSDF110 테이블의 이력 삭제 선행 처리 (DEL_YN = Y)
        		try{
            		if (model.get("INVC_NO"+i).toString().length() != 0 && !StringUtils.isEmpty((String)model.get("INVC_NO"+i))){
            			Map<String, Object> modelDel = new HashMap<String, Object>();
            			modelDel.put("ORD_ID"    	, model.get("ORD_ID"+i));
            			modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
            			modelDel.put("INVC_NO"		, model.get("INVC_NO"+i));
            			modelDel.put("DLV_COMP_CD"	, model.get("DLV_COMP_CD"));
            			modelDel.put("LC_ID"	    , (String)model.get(ConstantIF.SS_SVC_NO));
                        
                        dao.wmsdf110DelYnUpdate(modelDel);
            		}
                } catch(Exception e){
                    throw e;
                }
            }
    		
    		m.put("header"	, "Y"); // Y, E
            m.put("message"	, "[성공] 삭제완료.");// msg
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /*-
	 * Method ID	: getCustInfo
	 * Method 설명	: 고객정보별 원주문번호 조회
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		
		model.put("rcvrAddr", (String) model.get("rcvrAddr"));
		model.put("rcvrDetailAddr", (String) model.get("rcvrDetailAddr"));
		
		/*
		String rcvrAddr = (String) model.get("rcvrAddr");
		String rcvrAddrConvert = org.springframework.web.util.HtmlUtils.htmlUnescape(rcvrAddr);
		String rcvrAddrDecode = URLDecoder.decode(rcvrAddr, StandardCharsets.UTF_8.name());
		String rcvrDetailAddr = (String) model.get("rcvrDetailAddr");
		String rcvrDetailAddrConvert = org.springframework.web.util.HtmlUtils.htmlUnescape(rcvrDetailAddr);
		String rcvrDetailAddrDecode = URLDecoder.decode(rcvrDetailAddr, "utf-8");
		
		System.out.println("rcvrAddrDecode : " + rcvrAddrDecode);
		System.out.println("rcvrAddr : " + rcvrAddr);
		System.out.println("rcvrAddr extractHtmlTag1 : " +  extractHtmlTag(rcvrAddrConvert));
		System.out.println("rcvrAddrConvert : " +  rcvrAddrConvert);
		System.out.println("rcvrAddrConvert extractHtmlTag2 : " +  extractHtmlTag(rcvrAddrConvert));
		
		System.out.println("rcvrDetailAddrDecode : " + rcvrDetailAddrDecode);
		System.out.println("rcvrDetailAddr : " + rcvrDetailAddr);
		System.out.println("rcvrDetailAddr extractHtmlTag : " +  extractHtmlTag(rcvrDetailAddr));
		System.out.println("rcvrDetailAddrConvert : " + rcvrDetailAddrConvert);
		System.out.println("rcvrDetailAddrConvert extractHtmlTag2 : " +  extractHtmlTag(rcvrDetailAddrConvert));*/
		
		map.put("DS_ORG_ORD_NO", dao.getCustInfo(model));
		map.put("DS_INVC_INFO" , dao.getCustInfoInvcInFo(model));
		return map;
	}
    
    /*-
	 * Method ID	: getCustInfoV2
	 * Method 설명	: 고객정보별 원주문번호 조회 om010, op010 조인
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustInfoV2(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		String rcvrAddr = (String) model.get("rcvrAddr");
		String rcvrAddrConvert = org.springframework.web.util.HtmlUtils.htmlUnescape(rcvrAddr);
		String rcvrDetailAddr = (String) model.get("rcvrDetailAddr");
		String rcvrDetailAddrConvert = org.springframework.web.util.HtmlUtils.htmlUnescape(rcvrDetailAddr);
		model.put("rcvrAddr", extractHtmlTag(rcvrAddrConvert));
		model.put("rcvrDetailAddr", extractHtmlTag(rcvrDetailAddrConvert));
		
		map.put("DS_ORG_ORD_NO", dao.getCustInfoV2(model));
		return map;
	}
    
    
    public static String extractHtmlTag(String htmlText) throws Exception {
        if (isEmpty(htmlText))
            return htmlText;
        // <br/> to \n
        String insentiveCase = "(?u)";
        htmlText = htmlText.replaceAll(insentiveCase + "(<br\\s*(/)?>|</p>)", "\n");

        // remove html tag
        htmlText = htmlText.replaceAll("<.*?>", "");

        htmlText = htmlText.replaceAll(insentiveCase + "&apos;", "'");
        htmlText = htmlText.replaceAll(insentiveCase + "&amp;", "&");
        return htmlText;
    }

    public static boolean isEmpty(String str) {
        if (str == null || "".equals(str))
            return true;
        return false;
    }
    /**
     * 
     * 대체 Method ID		: custInfoUpdate
     * 대체 Method 설명		: custInfoUpdate
     * 작성자				: chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custInfoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("custInfoName"    	, model.get("custInfoName"));
            modelDt.put("custInfoNameOrg"   , model.get("custInfoNameOrg"));
            modelDt.put("custInfoTelNo"    	, model.get("custInfoTelNo"));
            
            String rcvrAddr = (String) model.get("custInfoAddr1");
            String rcvrAddrConvert = org.springframework.web.util.HtmlUtils.htmlUnescape(rcvrAddr);
    		String rcvrDetailAddr = (String) model.get("custInfoAddr2");
    		String rcvrDetailAddrConvert = org.springframework.web.util.HtmlUtils.htmlUnescape(rcvrDetailAddr);
    		//System.out.println("rcvrAddr extractHtmlTag : " +  rcvrAddrConvert);
    		//System.out.println("rcvrDetailAddr extractHtmlTag : " +  rcvrDetailAddrConvert);
    		
            if(model.get("selectIds") != null && !model.get("selectIds").equals("")){
            	int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            	String[] orgOrdId  		= new String[tmpCnt];                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	orgOrdId[i]    	= (String)model.get("orgOrdId"+i);               
                }
                modelDt.put("orgOrdNoArr", orgOrdId);
    		}else{
                List<String> orgOrdNoArr = new ArrayList();
                String[] custInfoOrgOrdIds = model.get("custInfoOrgOrdIds").toString().split(",");
                for (String keyword : custInfoOrgOrdIds ){
                	orgOrdNoArr.add(keyword);
                }
                modelDt.put("orgOrdNoArr", orgOrdNoArr);
    		}
    		modelDt.put("custInfoAddr1"    	, rcvrAddrConvert);
            modelDt.put("custInfoAddr2"    	, rcvrDetailAddrConvert);
            modelDt.put("custInfoZipNo"    	, model.get("custInfoZipNo"));
            modelDt.put("vrSrchCustId" 		, model.get("vrSrchCustId"));
            modelDt.put("LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
            modelDt.put("USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
            
            
            dao.custInfoUpdate(modelDt);
            m.put("errCnt", "0");
            
        } catch(Exception e){
        	m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID		: wmsdf110DelYnUpdate
     * 대체 Method 설명		: wmsdf110DelYnUpdate
     * 작성자				: schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> wmsdf110DelYnUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	Map<String, Object> modelDel = new HashMap<String, Object>();
        	//modelDel.put("ORD_ID"    	, model.get("ORD_ID"));
 			//modelDel.put("ORD_SEQ"    	, model.get("ORD_SEQ"));
 			//modelDel.put("INVC_NO"		, model.get("INVC_NO"));
 			//modelDel.put("LC_ID"	    , model.get("LC_ID"));
 			//modelDel.put("DLV_COMP_CD"	, model.get("PARCEL_COM_TY"));
 			
            dao.wmsdf110DelYnUpdate(model);
            m.put("errCnt", "0");
            
        } catch(Exception e){
        	m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    
    /*
     * Method ID  : dlvInvcItemNmSetInfo
     * Method 설명  : 택배 송장별 상품명 명칭 정보
     * 작성자             : yhku
     * @param model
     * @return
     */
       public Map<String, Object> dlvInvcItemNmSetInfo(Map<String, Object> model) throws Exception {
   		Map<String, Object> map = new HashMap<String, Object>();
   		map.put("INVC_ITEMNM_INFO", dao.dlvInvcItemNmSetInfo(model));
   		return map;
   	}
       
       /*
        * Method ID  : uploadRtnOrder
        * Method 설명  : 반품접수 템플릿업로드
        * 작성자             : schan
        * @param model
        * @return
        */
       public Map<String, Object> uploadRtnOrder(Map<String, Object> model,List<Map> list) throws Exception{
           Map<String, Object> m = new HashMap<String, Object>();
           try{
               
               String prevInvcNo = "";
               List<String>ritemCdList = new ArrayList<String>();
               List<String>ordQtyList = new ArrayList<String>();
               String prevOrdDesc = "";
               for(int i =0 ; i < list.size(); i++){
                   Map object = list.get(i);
                   
                   String invcNo = object.containsKey("INVC_NO") ? (String)object.get("INVC_NO").toString().replaceAll("\"", "") : "";
                   String ritemCd = object.containsKey("RITEM_CD") ? (String)object.get("RITEM_CD").toString().replaceAll("\"", "") : "";
                   String ordQty = object.containsKey("ORD_QTY") ? (String)object.get("ORD_QTY").toString().replaceAll("\"", "") : "";
                   String ordDesc = object.containsKey("ORD_DESC") ? (String)object.get("ORD_DESC").toString().replaceAll("\"", "") : "";
                   
                   if(invcNo.equals("")){
                       throw new BizException("송장번호가 누락되었습니다. "+i+"행");
                   }
                   if(i == 0){
                       ritemCdList.clear();
                       ordQtyList.clear();
                       ritemCdList.add(ritemCd);
                       ordQtyList.add(ordQty);
                       prevOrdDesc = ordDesc;
                       prevInvcNo = invcNo;
                   }
                   else if (!invcNo.equals(prevInvcNo)){
                       Map<String,Object> invcInfoMap = new HashMap<String,Object>();
                       invcInfoMap.put("INVC_NO",prevInvcNo);
                       invcInfoMap.put("RITEM_CD",ritemCdList);
                       invcInfoMap.put("ORD_QTY",ordQtyList);
                       invcInfoMap.put("ORD_DESC",prevOrdDesc);
                       
                       invcInfoMap.put("vrCustId"          ,model.get("vrCustId"));
                       invcInfoMap.put("SS_SVC_NO"         ,model.get("SS_SVC_NO"));
                       invcInfoMap.put("SS_CLIENT_IP"      ,model.get("SS_CLIENT_IP"));
                       invcInfoMap.put("SS_USER_NO"        ,model.get("SS_USER_NO"));
                       invcInfoMap.put("hostUrl"           ,model.get("hostUrl"));
                       
                       try{
                           //EAI Prameter Set
                           Map<String,Object> resultInfoMap = uploadRtnOrderParameterSet(invcInfoMap);
                           
                           //System.out.println(resultInfoMap);
                           //EAI CALL
                           m = DlvShipmentRtnComm(resultInfoMap);
                           //System.out.println(m);
                       }
                       catch(Exception e){
                           throw new BizException(e.getMessage()+" "+ i + "행");
                       }
                       //새송장번호 생성
                       ritemCdList.clear();
                       ordQtyList.clear();
                       ritemCdList.add(ritemCd);
                       ordQtyList.add(ordQty);
                       prevOrdDesc = ordDesc;
                       prevInvcNo = invcNo;
                       
                   }
                   else{
                       ritemCdList.add(ritemCd);
                       ordQtyList.add(ordQty);
                   }
                   if(i==(list.size()-1)){
                       Map<String,Object> invcInfoMap = new HashMap<String,Object>();
                       invcInfoMap.put("INVC_NO",prevInvcNo);
                       invcInfoMap.put("RITEM_CD",ritemCdList);
                       invcInfoMap.put("ORD_QTY",ordQtyList);
                       invcInfoMap.put("ORD_DESC",prevOrdDesc);
                       
                       invcInfoMap.put("vrCustId"          ,model.get("vrCustId"));
                       invcInfoMap.put("SS_SVC_NO"         ,model.get("SS_SVC_NO"));
                       invcInfoMap.put("SS_CLIENT_IP"      ,model.get("SS_CLIENT_IP"));
                       invcInfoMap.put("SS_USER_NO"        ,model.get("SS_USER_NO"));
                       invcInfoMap.put("hostUrl"           ,model.get("hostUrl"));
                       
                       try{
                           //EAI Prameter Set
                           Map<String,Object> resultInfoMap = uploadRtnOrderParameterSet(invcInfoMap);
                           
                           //System.out.println(resultInfoMap);
                           //EAI CALL
                           m = DlvShipmentRtnComm(resultInfoMap);
                           //System.out.println(m);
                           if(m.get("header").toString().equals("E")){
                               throw new BizException(m.get("message").toString()+" "+ i + "행");
                           }
                       }
                       catch(Exception e){
                           throw new BizException(e.getMessage()+" "+ i + "행");
                       }
                   }
               }
               
               m.put("MSG", MessageResolver.getMessage("save.success"));
               m.put("MSG_ORA", "");
               m.put("errCnt", 0);
               
           }catch(BizException be){
               if (log.isErrorEnabled()) {
                   log.error("Fail to upload Excel info :", be);
               }
               m = new HashMap<String, Object>();
               m.put("MSG", MessageResolver.getMessage("save.error"));
               m.put("MSG_ORA", be.getMessage());
               m.put("errCnt", "1");
           }
           catch(Exception e){
               throw e;
           }
           return m;
       }
       
       @SuppressWarnings("unchecked")
       public Map<String,Object> uploadRtnOrderParameterSet(Map<String,Object> map)throws Exception{
           Map<String, Object> m = new HashMap<String, Object>();
           try{
               Map<String,Object> modelIns = new HashMap<String,Object>();
               
               //1. InvcNO로 info 정보 가져오기
               List list = dao.getRtnOrderInfo(map);
               
               if(list.size() == 0){
                   throw new BizException("해당 송장의 정보가 없습니다.");
               }
               
               List<String> ritemCdList = (List<String>)map.get("RITEM_CD");
               List<String> ordQtyList = (List<String>)map.get("ORD_QTY");
               
               
               
               //2. 해당 정보 토대로 InsertMap 만들어주기
               for(int i = 0 ; i < list.size(); i++){
                   Map<String, String> obj = (Map<String, String>)list.get(i);
                   m.put("ORD_ID"+i, (String)obj.get("ORD_ID").toString());
                   m.put("ORD_SEQ"+i, String.valueOf(obj.get("ORD_SEQ")));
                   m.put("LC_ID"+i, (String)obj.get("LC_ID").toString());
                   m.put("CUST_ID"+i, (String)obj.get("CUST_ID").toString());
                   m.put("INVC_NO"+i, (String)obj.get("INVC_NO").toString());
                   
                   String ritemCd = (String)obj.get("RITEM_CD").toString();
                   
                   if(ritemCdList.contains(ritemCd)){
                       String inputOrdQty = ordQtyList.get(ritemCdList.indexOf(ritemCd));
                       m.put("ORD_QTY"+i, inputOrdQty == null || inputOrdQty.equals("") ? String.valueOf(obj.get("PARCEL_QTY")) : inputOrdQty);
                   }else{
                       m.put("ORD_QTY"+i, String.valueOf(obj.get("PARCEL_QTY")));
                   }
                   
                   if(i==0){
                       m.put("selectIds", String.valueOf(list.size()));
                       m.put("DIV_FLAG", "NORMAL");
                       m.put("PARCEL_COM_TY", (String)obj.get("PARCEL_COM_TY").toString());
                       m.put("PARCEL_ORD_TY", (String)obj.get("PARCEL_ORD_TY").toString());
                       m.put("PARCEL_SEQ_YN", "Y");
                       m.put("PARCEL_BOX_TY", (String)obj.get("PARCEL_BOX_TY").toString());
                       m.put("PARCEL_COM_TY_SEQ", (String)obj.get("PARCEL_COM_TY_SEQ").toString());
                       m.put("PARCEL_ETC_TY", (String)obj.get("PARCEL_ETC_TY").toString());
                       m.put("PARCEL_PAY_TY", (String)obj.get("PARCEL_PAY_TY").toString());
                       m.put("ADD_FLAG", "N");
                       m.put("hostUrl",(String)map.get("hostUrl").toString());
                       m.put("ETC2",(String)map.get("ORD_DESC").toString());
                       m.put(ConstantIF.SS_USER_NO,(String)map.get("SS_USER_NO").toString());
                   }
               }
           }
           catch(Exception e){
               throw e;
           }
           return m;
       }
     
}