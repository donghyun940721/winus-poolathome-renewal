package com.logisall.winus.wmsop.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSOP210Dao")
public class WMSOP210Dao extends SqlMapAbstractDAO {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 품목라벨조회
     * 작성자                 : KHKIM
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryWq("wmsop210.list", model);
    }
    
    public GenericResultSet list2(Map<String, Object> model) {
        return executeQueryWq("wmsop210.list2", model);
    }
    
    public Object updateAutoLabel(Map<String, Object> model) {
		executeUpdate("wmsop210.updateAutoLabel", model);
		return model;
	}
    
    public List rawListByList2(Map<String, Object> model) {
		List custs = list("wmsop210.rawListByList2", model);
    	return custs;
	}
    
    
}
