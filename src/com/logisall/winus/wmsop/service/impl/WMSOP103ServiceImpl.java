package com.logisall.winus.wmsop.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP103Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSOP103Service")
public class WMSOP103ServiceImpl extends AbstractServiceImpl implements WMSOP103Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP103Dao")
    private WMSOP103Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSOP103 = {"UPLOAD_ID"};

   /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID  : saveE2
     * 대체 Method 설명    : 엑셀목록 저장
     * 작성자                        : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
            model.put("UPLOAD_TITLE", model.get("D_FILE_NAME"));
			model.put("FILE_SIZE"   , model.get("D_FILE_SIZE"));				
			model.put("LC_ID"       , model.get(ConstantIF.SS_SVC_NO));
			model.put("REG_NO"      , model.get(ConstantIF.SS_USER_NO));
			model.put("CUST_ID"     , model.get("D_CUST_ID"));
			
            String fileUploadId = (String) dao.insert(model);
			m.put("MSG", MessageResolver.getMessage("insert.success"));
			m.put("RST", fileUploadId);
        } catch (Exception e) {
        	throw e;
        }
	return m;
    }
    
	/**
     * Method ID : saveExcelInfo103
     * Method 설명 : 엑셀읽기저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo103(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{
            	model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
				model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
				model.put("FILE_ROW" , insertCnt);
				
				// 1.고객관리 입력 및 수정 WMSCT010
				if (model.get("vrSaveGb").equals("INSERT")){
					System.out.println("여기?");
					dao.insertExcelInfo103(model, list);
				}else if(model.get("vrSaveGb").equals("UPDATE")){
					dao.insertExcelInfoUpdate(model, list);
				}
				
                dao.updateFileRow(model);
                
                m.put("MSG", MessageResolver.getMessage("save.success"));
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                m.put("MSG", MessageResolver.getMessage("save.error"));
    			m.put("MSG_ORA", e.getMessage());
    			m.put("errCnt", "1");
                throw e;
            }
        return m;
    }
//    
//    /**
//     * 
//     * 대체 Method ID   : save
//     * 대체 Method 설명    :
//     * 작성자                      : chsong
//     * @param model
//     * @return
//     * @throws Exception
//     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));
                modelDt.put("UPLOAD_ID"     	, model.get("UPLOAD_ID"+i));
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                
                System.out.println("ssss :: " + model.get("ST_GUBUN"+i) + " // " + model.get("UPLOAD_ID"+i));
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSOP103);
                
                if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	dao.deleteUpdateWmssp010(modelDt);
                	dao.deleteUpdateWmsct010(modelDt);
                	
					dao.delete(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
//    
//    /**
//     * Method ID  : excelDownCustom
//     * Method 설명    : Type별 주문정보 엑셀다운로드
//     * 작성자                 : chsong
//     * @param   model
//     * @return 
//     * @throws Exception 
//     */
//     @Override
//     public Map<String, Object> excelDownCustom(Map<String, Object> model) throws Exception {
//         Map<String, Object> map = new HashMap<String, Object>();
//         model.put("pageIndex", "1");
//         model.put("pageSize", "60000");
//         map.put("LIST", dao.excelDownCustom(model));
//         return map;
//     }
//     
//     /**
//      * 
//      * 대체 Method ID   : keyInSaveInfo
//      * 대체 Method 설명    : 배송주문 일반저장
//      * 작성자                      : chsong
//      * @param model
//      * @return
//      * @throws Exception
//      */
//     @Override
//     public Map<String, Object> keyInSaveInfo(Map<String, Object> model, String[] cellName) throws Exception {
//         Map<String, Object> m = new HashMap<String, Object>();
//         int lastCell = cellName.length;
//         int errCnt = 0;
//         
//         List list = new ArrayList();
//         Map rowMap = null;
//         
//         try{
//        	 for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
//            	 rowMap = new LinkedHashMap();
//            	 
//        		 for(int k = 0 ; k < lastCell; k ++){
//        			 rowMap.put(cellName[k], model.get(cellName[k]+i));
//        		 }
//        		 list.add(rowMap);
//        	 }
//        	 
//        	 int insertCnt = (list != null)?list.size():0;
// 
//             model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
//             model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
//             model.put("FILE_ROW" , insertCnt);
// 				
//             // 1.고객관리 입력 및 수정 WMSCT010
//             if (model.get("vrSaveGb").equals("INSERT")){
//            	 dao.insertExcelInfo(model, list);
//             }else if(model.get("vrSaveGb").equals("UPDATE")){
//            	 dao.insertExcelInfoUpdate(model, list);
//             }
//             dao.updateFileRow(model);
//             
//             m.put("MSG", MessageResolver.getMessage("save.success"));
//             m.put("MSG_ORA", "");
//             m.put("errCnt", errCnt);
//                 
//         } catch(Exception e){
//        	 m.put("MSG", MessageResolver.getMessage("save.error"));
//        	 m.put("MSG_ORA", e.getMessage());
//        	 m.put("errCnt", "1");
//        	 throw e;
//         }
//         return m;
//     }
//     
//     /**
//      * Method ID : saveExcelInfoMulti
//      * Method 설명 : 엑셀읽기저장
//      * 작성자 : 기드온
//      * @param model
//      * @return
//      * @throws Exception
//      */
//     public Map<String, Object> saveExcelInfoMulti(Map<String, Object> model, List list) throws Exception {
//         Map<String, Object> m = new HashMap<String, Object>();
//         int errCnt = 0;
//         int insertCnt = (list != null)?list.size():0;
//             try{
//             	model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
// 				model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
// 				model.put("FILE_ROW" , insertCnt);
// 				
// 				System.out.println("vrTrustCustId : "+ model.get("vrTrustCustId"));
// 				
// 				// 1.고객관리 입력 및 수정 WMSCT010
// 				if (model.get("vrSaveGb").equals("INSERT")){
// 					dao.insertExcelInfoMulti(model, list);
// 				}else if(model.get("vrSaveGb").equals("UPDATE")){
// 					dao.insertExcelInfoUpdate(model, list);
// 				}
//                 dao.updateFileRow(model);
//                 
//                 m.put("MSG", MessageResolver.getMessage("save.success"));
//                 m.put("MSG_ORA", "");
//                 m.put("errCnt", errCnt);
//                 
//             } catch(Exception e){
//                 m.put("MSG", MessageResolver.getMessage("save.error"));
//     			m.put("MSG_ORA", e.getMessage());
//     			m.put("errCnt", "1");
//                 throw e;
//             }
//         return m;
//     }
}
