package com.logisall.winus.wmsop.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.components.headertoolbar.actions.MoveElementCommand;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.OliveAes256;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP338Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;






/*SF*/
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.logging.Logger;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONObject;
import org.json.XML;
import org.apache.pdfbox.io.MemoryUsageSetting;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;

import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.OrderWebService;

@Service("WMSOP338Service")
public class WMSOP338ServiceImpl implements WMSOP338Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP338Dao")
    private WMSOP338Dao dao;
    
    /**
     * Method ID   : selectItemGrp
     * Method 설명    : 출고관리 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    @Override
    public Map<String, Object> listByHeaderKakao(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByHeaderKakao(model));
        return map;
    }
    
    @Override
    public Map<String, Object> listByCustDetailKakao(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("LIST", dao.listByCustDetailKakao(model));
        return map;
    }
    
	@Override
    public Map<String, Object> updatePickingTotalKakao(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
        try{
        	
        	int workStatCnt = 0;
        	int chkIdsCnt = 0;
//        	model.put("vrSrchAddressValid", "1");
        	List list = dao.rawListByCustSummaryWorkKaKao(model);
        	int tmpCnt = list.size();
            if(tmpCnt > 0){           
            
	            String[] ordId = new String[tmpCnt];
	            String[] ordSeq = new String[tmpCnt];
	            
	            
	            for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");

                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
                    int outOrdQty = Integer.parseInt(String.valueOf(((Map<String, String>)list.get(i)).get("OUT_ORD_QTY")));
                    
                  
                    if(Integer.parseInt(workStat) == 450){
            			workStatCnt++;
            		}
            		
            		if(Integer.parseInt(workStat) < 230){
            			if(0 < outOrdQty){
            				chkIdsCnt++;
            			}
            		}
                  
                    
                	//System.out.println(ordId[i] + "::::" + ordSeq[i]);
                }
	            
	            if(workStatCnt > 0){
	            	if(tmpCnt != workStatCnt){
	            		String msg = "매핑 작업시 모든 상태가 매핑 이어야 합니다.";
	            		throw new BizException(msg);
	            	}
	            }
	            
	            if(chkIdsCnt > 0){
	        		String msg = "피킹리스트발행시로케이션지정은필수입니다";
	        		throw new BizException(msg);
	            }
	            
	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();

	            modelIns.put("ordId", ordId);
	            modelIns.put("ordSeq", ordSeq);

	            // session 및 등록정보
	            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

	            // dao
	            modelIns = (Map<String, Object>)dao.updatePickingTotal(modelIns);
	            ServiceUtil.isValidReturnCode("WMOP338", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
  	    	
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
	}
	
	@Override
    public Map<String, Object> confirmPickingTotal(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
//        	System.out.println( " ----- :  "+ model.get("ORD_IDs"));
        	List list = dao.rawListByCustSummaryWorkKaKao(model);
        	int tmpCnt = list.size();
            int workStatCnt = 0;
        	if(tmpCnt > 0){           
            
	            String[] ordId = new String[tmpCnt];
	            String[] ordSeq = new String[tmpCnt];
	            
	            
	            for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
                	

                    String workStat = (String)((Map<String, String>)list.get(i)).get("WORK_STAT");
                    
                  
                    if(Integer.parseInt(workStat) < 230 || Integer.parseInt(workStat) > 990){
            			workStatCnt++;
            		}
            		
                    
                	//System.out.println(ordId[i] + "::::" + ordSeq[i]);
                }
	            
	            if(workStatCnt > 0){
	            	
            		String msg = "모든 상태가  피킹리스트 발행 이어야 합니다.";
            		throw new BizException(msg);
	          
	            }


//	            // 프로시져에 보낼것들 다담는다
	            Map<String, Object> modelIns = new HashMap<String, Object>();

	            modelIns.put("ordId", ordId);
	            modelIns.put("ordSeq", ordSeq);
	            
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
	            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

	            // dao
	            modelIns = (Map<String, Object>)dao.confirmPickingTotal(modelIns);
	            ServiceUtil.isValidReturnCode("WMOP030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
  	    	
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("list.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			}       	
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        }catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error") );
        }
        return m;
    }
	
	@Override
    public Map<String, Object> saveOutCompleteV2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{
        	List list = dao.rawListByCustSummaryWorkKaKao(model);
        	int tmpCnt = list.size();
            if(tmpCnt > 0){           
            
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];
                String[] workQty = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_ID");
                	ordSeq[i]    = (String)((Map<String, String>)list.get(i)).get("ORD_SEQ");
                    workQty[i]    		= null;  
                    //System.out.println(ordId[i]+ " " + ordSeq[i] + " " + workQty[i]);
                    
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("workQty", workQty);
                
                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOutComplete(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	
	 @Override
	    public Map<String, Object> listCountByCust(Map<String, Object> model) throws Exception {
	        Map<String, Object> map = new HashMap<String, Object>();
	        
	        map.put("LIST", dao.listCountByCust(model));
	        return map;
	    }
	 
	 /**
	     * 
	     * Method ID : pickingListPoiNo 
		 * Method 설명 : 피킹리스트 poi update
		 * 작성자 : sing09
		 * 
		 * @param model
		 * @return
	     */
	    @Override
	    public Map<String, Object> pickingListPoiNo(Map<String, Object> model) throws Exception {
	        Map<String, Object> m = new HashMap<String, Object>();
	        try{
	        	DateFormat dateFormatYMD = new SimpleDateFormat("yyyyMMddHHmmss");
				Date now = new Date();
				String vSYSDATE = dateFormatYMD.format(now) + Integer.parseInt(model.get("selectIds").toString());
				for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	                Map<String, Object> modelDt = new HashMap<String, Object>();
	                modelDt.put("SS_SVC_NO"     , model.get(ConstantIF.SS_SVC_NO));
	                
	                modelDt.put("ORD_ID"   		, model.get("ORD_ID"+i));        //INSERT -> SEQ   
	                modelDt.put("ORD_SEQ"   	, model.get("ORD_SEQ"+i));
	                modelDt.put("POI_NO"   		, vSYSDATE);
//	                System.out.println(modelDt);
	                dao.pickingListPoiNo(modelDt);
	            }
	            m.put("errCnt", 0);
	            m.put("POI", vSYSDATE);
	            m.put("MSG", MessageResolver.getMessage("list.success"));
	            
	        } catch(Exception e){
	        	if (log.isErrorEnabled()) {
					log.error("Fail to get result :", e);
				} 
	            m.put("errCnt", 1);
	            m.put("MSG", MessageResolver.getMessage("save.error") );
	        }
	        return m;
	    }
}