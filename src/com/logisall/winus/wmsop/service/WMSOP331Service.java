package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP331Service {
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustSummary(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustSummaryPoi(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> rogenCrossDomainHttp(Map<String, Object> model) throws Exception;
	public Map<String, Object> saeroCompleteCrossDomainHttp(Map<String, Object> model) throws Exception;
	public Map<String, Object> billingListDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> popupList(Map<String, Object> model) throws Exception;
	public Map<String, Object> popupSave(Map<String, Object> model) throws Exception;
}