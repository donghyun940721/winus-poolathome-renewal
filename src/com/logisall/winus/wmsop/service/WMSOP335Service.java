package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

import com.m2m.jdfw5x.egov.database.GenericResultSet;


public interface WMSOP335Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustSummary(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByCustDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> listCountByCust(Map<String, Object> model) throws Exception;
	public Map<String, Object> outInvalidView(Map<String, Object> model) throws Exception;
	public Map<String, Object> autoBestLocSaveMultiV3(Map<String, Object> model) throws Exception;
	public Map<String, Object> updatePickingTotalV2Poi(Map<String, Object> model) throws Exception;
	public Map<String, Object> updatePickingTotalV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> confirmPickingTotalV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> billingListDetailV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> autoDeleteLocSaveV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveOutCompleteV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> ritemIdSearchCd(Map<String, Object> model) throws Exception;
}
