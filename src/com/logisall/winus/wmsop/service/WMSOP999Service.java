package com.logisall.winus.wmsop.service;

import java.util.Map;


public interface WMSOP999Service {
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listOrderDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listOrderDetailOm(Map<String, Object> model) throws Exception;
    public Map<String, Object> listInOrderItem(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteInOrder(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listSimpleInItem(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveSimpleInOrder(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listOutOrderItem(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listLocSearch(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveOutOrder(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> etcSaveOutOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> etcSaveInOrder(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveSimpleOutOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> etcSaveV2(Map<String, Object> model) throws Exception;

	public Map<String, Object> saveInOrderV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveOutOrderOm(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> saveInModifyOrder(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveInModifyOrderOut(Map<String, Object> model) throws Exception;
}
