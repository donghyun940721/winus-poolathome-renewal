package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

import com.m2m.jdfw5x.egov.database.GenericResultSet;

public interface WMSOP640Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvSummary(Map<String, Object> model) throws Exception;
	public Map<String, Object> list2ByDlvHistory(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvShipment(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveCsv(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> dlvSenderInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> boxNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> invcNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> invcNoInfoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustOrdDegree(Map<String, Object> model) throws Exception;
}