package com.logisall.winus.wmsop.service;

import java.util.Map;

public interface WMSOP910Service {
    public Map<String, Object> listE1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4Header(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4Detail(Map<String, Object> model) throws Exception;
    
    
    public Map<String, Object> listE1SummaryCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2SummaryCount(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3SummaryCount(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
}
