package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

import com.m2m.jdfw5x.egov.database.GenericResultSet;

public interface WMSOP642Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvSummary(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvSummaryDiv(Map<String, Object> model) throws Exception;
	public Map<String, Object> list2ByDlvHistory(Map<String, Object> model) throws Exception;
	public Map<String, Object> list3ByDlvHistory(Map<String, Object> model) throws Exception; //TAB3
	public Map<String, Object> DlvShipment(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvInvcNoOrderComm(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvInvcNoOrderCommTotal(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvShipmentRtnComm(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvInvcNoPrintPre(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvShipmentHanjin(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveCsv(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> dlvInfoInsert(Map<String, Object> model) throws Exception;
	public Map<String, Object> dlvSenderInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> dlvSenderInfoDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> dlvInvcItemNmSetInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> dlvDlvCompInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> boxNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> invcNoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> invcNoInfoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustOrdDegree(Map<String, Object> model) throws Exception;
	public Map<String, Object> listByDlvSummaryExcel(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvShipDelete(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustInfoV2(Map<String, Object> model) throws Exception;
	public Map<String, Object> custInfoUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> custAddrRefineEAI(Map<String, Object> model) throws Exception;
	public Map<String, Object> wmsdf110DelYnUpdate(Map<String, Object> model) throws Exception;
	public Map<String, Object> DlvShipmentPostTotal(Map<String, Object> model) throws Exception;
	public Map<String, Object> uploadRtnOrder(Map<String, Object> model,List<Map> list) throws Exception;
	
}