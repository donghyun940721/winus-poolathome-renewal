package com.logisall.winus.wmsop.service;

import java.util.List;
import java.util.Map;

public interface WMSOP103Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveExcelInfo103(Map<String, Object> model, List list) throws Exception;
//    public Map<String, Object> saveExcelInfoMulti(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
//    public Map<String, Object> excelDownCustom(Map<String, Object> model) throws Exception;
//    public Map<String, Object> keyInSaveInfo(Map<String, Object> model, String[] cellName) throws Exception;
}