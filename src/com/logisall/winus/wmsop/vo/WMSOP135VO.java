package com.logisall.winus.wmsop.vo;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import com.logisall.winus.frm.common.util.OliveAes256;
import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;

public class WMSOP135VO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2518464732146069891L;
	
	
	private String ordId;
	private String ordSeq;
	private String orgOrdId;
	private String orgOrdSeq;
	private String custOrdSeq;
	private String ordSubtype;
	private String workStat;
	private String workStat1;
	private String ordType;
	private String wmedct;
	
	
	
	public String getWmedct() {
		return wmedct;
	}
	public void setWmedct(String wmedct) {
		this.wmedct = wmedct;
	}
	public String getOrdType() {
		return ordType;
	}
	public void setOrdType(String ordType) {
		this.ordType = ordType;
	}
	public String getOrgOrdId() {
		return orgOrdId;
	}
	public void setOrgOrdId(String orgOrdId) {
		this.orgOrdId = orgOrdId;
	}
	
	public String getOrdId() {
		return ordId;
	}
	public void setOrdId(String ordId) {
		this.ordId = ordId;
	}
	public String getOrdSeq() {
		return ordSeq;
	}
	public void setOrdSeq(String ordSeq) {
		this.ordSeq = ordSeq;
	}
	public String getOrgOrdSeq() {
		return orgOrdSeq;
	}
	public void setOrgOrdSeq(String orgOrdSeq) {
		this.orgOrdSeq = orgOrdSeq;
	}
	public String getOrdSubtype() {
		return ordSubtype;
	}
	public String getCustOrdSeq() {
		return custOrdSeq;
	}
	public void setCustOrdSeq(String custOrdSeq) {
		this.custOrdSeq = custOrdSeq;
	}
	public void setOrdSubtype(String ordSubtype) {
		this.ordSubtype = ordSubtype;
	}
	public String getWorkStat() {
		return workStat;
	}
	public void setWorkStat(String workStat) {
		this.workStat = workStat;
	}
	public String getWorkStat1() {
		return workStat1;
	}
	public void setWorkStat1(String workStat1) {
		this.workStat1 = workStat1;
	}
	
	
	
}
