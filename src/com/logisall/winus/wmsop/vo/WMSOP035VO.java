package com.logisall.winus.wmsop.vo;

import java.io.Serializable;

public class WMSOP035VO implements Serializable{
	
	private static final long serialVersionUID = 8597702313617503434L;
	
	private byte[] graphicImage;

	public byte[] getGraphicImage() {
		return graphicImage;
	}

	public void setGraphicImage(byte[] graphicImage) {
		this.graphicImage = graphicImage;
	}
	
}
