package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP910Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP910Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP910Service")
	private WMSOP910Service service;
	
	/*-
	 * Method ID    : wmsop910
	 * Method 설명      : 입/출고(통합)화면
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP910.action")
	public ModelAndView wmsop910(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP910",  service.selectData(model));
	}
	
	/*-
	 * Method ID    : listE1
     * Method 설명      : 입고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP910/listE1.action")
	public ModelAndView listE1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE2
     * Method 설명      : 출고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP910/listE2.action")
	public ModelAndView listNew(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE3
	 * Method 설명      : 화주별 출고관리 조회(OM)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE3.action")
	public ModelAndView listE3(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE4Header
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE4Header.action")
	public ModelAndView listE4Header(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE4Header(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE4Detail
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE4Detail.action")
	public ModelAndView listE4Detail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE4Detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE1SummaryCount
	 * Method 설명      : 입출고통합관리 -> 입고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE1SummaryCount.action")
	public ModelAndView listE1SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE1SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE2SummaryCount
	 * Method 설명      : 입출고통합관리 -> B2B 출고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE2SummaryCount.action")
	public ModelAndView listE2SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE2SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    	: listE3SummaryCount
	 * Method 설명      : 입출고통합관리 -> B2C 출고관리 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP910/listE3SummaryCount.action")
	public ModelAndView listE3SummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listE3SummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}