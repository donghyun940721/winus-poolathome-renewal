package com.logisall.winus.wmsop.web;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP000Service;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsop.service.WMSOP135Service;
import com.logisall.winus.wmsop.vo.WMSOP135VO;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;


@Controller
public class WMSOP135Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP030Service")
	private WMSOP030Service wmsop030service;

	@Resource(name = "WMSOP000Service")
	private WMSOP000Service wmsop000service;
	

	@Resource(name = "WMSOP135Service")
	private WMSOP135Service wmsop135service;
	
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service wmsif000service;

	
	static final String[] COLUMN_NAME_WMSOP135E12 = {
		"I_ORD_ID", "I_ORD_SEQ", "I_LOC_CD"
	};
	
	
	
	

	/*-
	 * Method ID    : saveLoc
	 * Method 설명      : 로케이션 지정(건별) / 로케이션 지정 등록,수정
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/saveLoc.action")
	public ModelAndView saveLoc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			m = wmsop000service.saveLoc(model);
			
//			int tmpCnt = (int) m.get("errCnt");
//			if(tmpCnt >= 0 ){//성공시
				
				WMSOP135VO vo = new WMSOP135VO();
				vo = (WMSOP135VO) wmsop135service.getOrdSeqInfo(model);
				
				m = wmsop135service.OrdSeqInfoTraceEAICall(model, vo);
//			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	
	
	
	
	/*-
	 * Method ID    : autoBestLocSave
	 * Method 설명      : 로케이션추천지정(일괄) /  로케이션추천 자동
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/autoBestLocSave.action")
	public ModelAndView autoBestLocSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		
		try {
			 m = wmsop030service.autoBestLocSave(model);
			 //m.put("errCnt", 0);
			 
			//int tmpCnt = (int) m.get("errCnt");
			//if(tmpCnt >= 0 ){//성공시
				
				//ord list
				List<WMSOP135VO> list = wmsop135service.getOrdList(model);
				
				m = wmsop135service.OrdSeqListTraceEAICall(model, list);
			//}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("errCnt", 1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : autoBestLocSaveMulti
	 * Method 설명      : 로케이션추천지정(다중주문일괄/주문번호, seq) / 아산물류센터 로케이션추천 자동
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/autoBestLocSaveMulti.action")
	public ModelAndView autoBestLocSaveMulti(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			 m = wmsop030service.autoBestLocSaveMulti(model);
			
			//int tmpCnt = (int) m.get("errCnt");
			//if(tmpCnt >= 0 ){//성공시
			
				//ord seq list
				List<WMSOP135VO> list = wmsop135service.getOrdSeqList(model);
				
				m = wmsop135service.OrdSeqListTraceEAICall(model, list);
			//}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	 * Method ID    : autoBestLocSaveMultiV2
	 * Method 설명      : 로케이션추천지정(다중주문일괄/주문번호) /아산물류센터 로케이션추천 자동V2
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/autoBestLocSaveMultiV2.action")
	public ModelAndView autoBestLocSaveMultiV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			m = wmsop030service.autoBestLocSaveMultiV2(model);
			
			//int tmpCnt = (int) m.get("errCnt");
			//if(tmpCnt >= 0 ){//성공시
				
				//ord list
				List<WMSOP135VO> list = wmsop135service.getOrdList(model);
				
				m = wmsop135service.OrdSeqListTraceEAICall(model, list);
			//}
	        
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	

	/*-
	 * Method ID    : autoBestLocSaveMultiQtyList
	 * Method 설명      : 로케이션추천지정(다중주문일괄/주문번호) 재고 수량 파악
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/autoBestLocSaveMultiQtyList.action")
	public ModelAndView autoBestLocSaveMultiQtyList(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = wmsop135service.autoBestLocSaveMultiQtyList(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	
	
	/*-
	 * Method ID    : autoDeleteLocSave
	 * Method 설명      : 로케이션지정 삭제 일괄
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/autoDeleteLocSave.action")
	public ModelAndView autoDeleteLocSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			m = wmsop030service.autoDeleteLocSave(model);
			
			int tmpCnt = (int) m.get("errCnt");
			if(tmpCnt >= 0 ){//성공시
			
				//ord seq list
				List<WMSOP135VO> list = wmsop135service.getOrdSeqList(model);
				
				m = wmsop135service.OrdSeqListTraceEAICall(model, list);
			}
			
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	 * Method ID    : outPickingCancel
	 * Method 설명      : 피킹취소
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/outPickingCancel.action")
	public ModelAndView outPickingCancel(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = wmsop030service.outPickingCancel(model);
			
			int tmpCnt = (int) m.get("errCnt");
			if(tmpCnt >= 0 ){//성공시
				
				//ord seq list
				List<WMSOP135VO> list = wmsop135service.getOrdSeqList(model);
				
				m = wmsop135service.OrdSeqListTraceEAICall(model, list);
			}
			
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID        : /WMSOP135pop.action
	 * Method 설명      		: KCC 거래명세서 발행
	 * 작성자            		: Seongjun Kwon
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP135pop.action")
	public ModelAndView WMSOP135pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP135pop");
	}
	
	/*-
	 * Method ID    : updateOrdDesc
	 * Method 설명   : 출고관리KCC 비고1, 비고2 수정
	 * 작성자         : Seongjun Kwon
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/updateOrdDesc.action")
	public ModelAndView updateOrdDesc(Map<String, Object> model) {
		ModelAndView mav =  new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m =  wmsop135service.updateOrdDesc(model);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	* Method ID    : listExcel
	* Method 설명      : 엑셀다운로드
	* 작성자                 : yhku
	* @param model
	* @return
	*/
	@RequestMapping("/WMSOP135/listExcelKcc.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = wmsop135service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
	       try{
	           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
	           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
	           String[][] headerEx = {
	                                   {MessageResolver.getText("작업상태")   , "0", "0", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문번호")   , "1", "1", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문SEQ")   , "2", "2", "0", "0", "100"}
	                                  ,{MessageResolver.getText("LOT NO")   , "3", "3", "0", "0", "100"}
	                                  ,{MessageResolver.getText("배송지정일")   , "4", "4", "0", "0", "100"}
	                                  ,{MessageResolver.getText("센터출하일")   , "5", "5", "0", "0", "100"}
	                                  ,{MessageResolver.getText("상품코드")   , "6", "6", "0", "0", "100"}
	                                  ,{MessageResolver.getText("상품명")   , "7", "7", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문량")   , "8", "8", "0", "0", "100"}
	                                  ,{MessageResolver.getText("UOM")   , "9", "9", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문중량")   , "10", "10", "0", "0", "100"}
	                                  ,{MessageResolver.getText("출고량")   , "11", "11", "0", "0", "100"}
	                                  ,{MessageResolver.getText("UOM")   , "12", "12", "0", "0", "100"}
	                                  ,{MessageResolver.getText("출고중량")   , "13", "13", "0", "0", "100"}
	                                  ,{MessageResolver.getText("화주")   , "14", "14", "0", "0", "100"}
	                                  ,{MessageResolver.getText("배송처")   , "15", "15", "0", "0", "100"}
	                                  ,{MessageResolver.getText("송장번호")   , "16", "16", "0", "0", "100"}
	                                  ,{MessageResolver.getText("현재고")   , "17", "17", "0", "0", "100"}
	                                  ,{MessageResolver.getText("불량재고")   , "18", "18", "0", "0", "100"}
	                                  ,{MessageResolver.getText("PLT수량")   , "19", "19", "0", "0", "100"}
	                                  ,{MessageResolver.getText("BOX수량")   , "20", "20", "0", "0", "100"}
	                                  ,{MessageResolver.getText("창고")   , "21", "21", "0", "0", "100"}
	                                  ,{MessageResolver.getText("비고1")   , "22", "22", "0", "0", "100"}
	                                  ,{MessageResolver.getText("비고2")   , "23", "23", "0", "0", "100"}
	                                  ,{MessageResolver.getText("원주문번호")   , "24", "24", "0", "0", "100"}
	                                  ,{MessageResolver.getText("원주문SEQ")   , "25", "25", "0", "0", "100"}
	                                  ,{MessageResolver.getText("컨테이너번호")   , "26", "26", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문구분")   , "27", "27", "0", "0", "100"}
	                                  
	                                  ,{MessageResolver.getText("주문자")   , "28", "28", "0", "0", "100"}
	                                  ,{MessageResolver.getText("전화번호")   , "29", "29", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주소")   , "30", "30", "0", "0", "100"}
	                                  ,{MessageResolver.getText("차량번호")   , "31", "31", "0", "0", "100"}
	                                  ,{MessageResolver.getText("톤수")   , "32", "32", "0", "0", "100"}
	                                  ,{MessageResolver.getText("변경톤수")   , "33", "33", "0", "0", "100"}
	                                  ,{MessageResolver.getText("영업사원")   ,  "34", "34", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문등록일")   , "35", "35", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문수정일")   , "36", "36", "0", "0", "100"}
	                                  ,{MessageResolver.getText("(작업)수정일")   , "37", "37", "0", "0", "100"}
	                                 };
	           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
	           String[][] valueName = {
	                                   {"WORK_STAT"  , "S"},
	                                   {"VIEW_ORD_ID"  , "S"},
	                                   {"ORD_SEQ"  , "S"},
	                                   {"CUST_LOT_NO"  , "S"},
	                                   {"OUT_REQ_DT"  , "S"},
	                                   {"OUT_DT"  , "S"},
	                                   {"RITEM_CD"  , "S"},
	                                   {"RITEM_NM"  , "S"},
	                                   {"OUT_ORD_QTY"  , "N"},
	                                   {"OUT_ORD_UOM_NM"  , "S"},
	                                   {"OUT_ORD_WEIGHT"  , "S"},
	                                   {"REAL_OUT_QTY"  , "N"},
	                                   {"OUT_UOM_NM"  , "S"},
	                                   {"REAL_OUT_WEIGHT"  , "S"},
	                                   {"CUST_NM"  , "S"},
	                                   {"TRANS_DLV_CUST_NM"  , "S"},
	                                   {"INVC_NO"  , "S"},
	                                   {"STOCK_QTY"  , "N"},
	                                   {"BAD_QTY"  , "N"},
	                                   {"REAL_PLT_QTY"  , "N"},
	                                   {"REAL_BOX_QTY"  , "N"},
	                                   {"OUT_WH_NM"  , "S"},
	                                   {"ORD_DESC"  , "S"},
	                                   {"ETC2"  , "S"},
	                                   {"ORG_ORD_ID"  , "S"},
	                                   {"CUST_ORD_SEQ"  , "S"},
	                                   {"CNTR_NO"  , "S"},
	                                   {"ORD_SUBTYPE_NM"  , "S"},
	                                   
	                                   {"SALES_CUST_NM"  , "S"}, 
	                                   {"PHONE_1"  , "S"},       
	                                   {"ADDR"  , "S"},          
	                                   {"CAR_CD"  , "S"},        
	                                   {"CAR_TON"  , "S"},       
	                                   {"ADJUCT_CAR_TON"  , "S"},
	                                   {"SALESMAN_PHONE"  , "S"},
	                                   {"OM_REG_DT"  , "S"},     
	                                   {"OM_UPD_DT"  , "S"},     
	                                   {"UPD_DT"  , "S"}         
	                                  }; 
	           
				// 파일명
				String fileName = MessageResolver.getText("출고관리");
				// 시트명
				String sheetName = "Sheet1";
				// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
				String marCk = "N";
				// ComUtil코드
				String etc = "";

				ExcelWriter wr = new ExcelWriter();
				wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("fail download Excel file...", e);
				}
			}
		}
	
	
	
	

	/*-
	 * Method ID    : deliveryComplete
	 * Method 설명      : 배송완료(이동출고건) 
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP135/deliveryComplete.action")
	public ModelAndView deliveryComplete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			m = wmsop135service.deliveryComplete(model);
	        
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
}
