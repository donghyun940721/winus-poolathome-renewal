package com.logisall.winus.wmsop.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP210Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP210Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP210Service")
	private WMSOP210Service service;

	/*-
	 * Method ID    : wmsop010
	 * Method 설명      : 입출고현황 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP210.action")
	public ModelAndView wmsop210(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP210");
	}
	
	@RequestMapping("/WMSOP210pop.action")
	public ModelAndView wmsop210pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP210pop");
	}
	@RequestMapping("/WMSOP210pop2.action")
	public ModelAndView wmsop210pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP210pop2");
	}
	@RequestMapping("/WMSOP210pop3.action")
	public ModelAndView WMSOP210pop3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP210pop3");
	}
	@RequestMapping("/WMSOP210pop4.action")
	public ModelAndView WMSOP210pop4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP210pop4");
	}
	/*-
	 * Method ID    : list
	 * Method 설명      : 입출고현황  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP210/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP210/list2.action")
	public ModelAndView list2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP210/updateAutoLabel.action")
	public ModelAndView outPickingCancel(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateAutoLabel(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}				 
		mav.addAllObjects(m);
		return mav;
	}
}