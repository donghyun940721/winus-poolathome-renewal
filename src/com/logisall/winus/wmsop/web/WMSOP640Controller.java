package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP640Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP640Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	static final String[] COLUMN_NAME_WMSOP640 = {"ORD_ID", "ORD_SEQ", "DLV_COMP_CD", "INVC_NO", "SWEET_TRACKER_DLV_CD", "DLV_PRICE", "ORD_DATE"};

	
	@Resource(name = "WMSOP640Service")
	private WMSOP640Service service;

	/*-
	 * Method ID    : wmsop640
	 * Method 설명      : 택배접수
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP640.action")
	public ModelAndView wmsop640(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP640", service.selectItemGrp(model));
	}
	@RequestMapping("/WMSOP640pop2.action")
	public ModelAndView wmsom010pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP640pop2");
	}
	
	/*-
	 * Method ID    : listByDlvSummary
	 * Method 설명	: 출고관리(송장발행화면)
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/listByDlvSummary.action")
	public ModelAndView listByDlvSummary(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByDlvSummary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list2ByDlvHistory
	 * Method 설명	: 출고관리(택배접수이력엑셀등록기록조회)
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/list2ByDlvHistory.action")
	public ModelAndView list2ByDlvHistory(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list2ByDlvHistory(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: DlvShipment640
	 * Method 설명	: 택배접수
	 * 작성자                 	: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/DlvShipment.action")
	public ModelAndView DlvShipment640(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.DlvShipment(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : wmsop640e3
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSOP640E3.action")
	public ModelAndView wmsop640e3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP640E3");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP640/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP640, 0, startRow, 10000, 0);

			for(int i =0 ; i<list.size() ; i++){
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap = list.get(i);
				int rowNum = i+2;
				
				String fieldOrdId = (String) hashMap.get(COLUMN_NAME_WMSOP640[0]);
				if(fieldOrdId == null || fieldOrdId.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP640[0]+"(주문번호)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldOrdSeq = (String) hashMap.get(COLUMN_NAME_WMSOP640[1]);
				if(fieldOrdSeq == null || fieldOrdSeq.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP640[1]+"(주문seq)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldDlvCompCd = (String) hashMap.get(COLUMN_NAME_WMSOP640[2]);
				if(fieldDlvCompCd == null || fieldDlvCompCd.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP640[2]+"(택배사코드)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldInvcNo = (String) hashMap.get(COLUMN_NAME_WMSOP640[3]);
				if(fieldInvcNo == null || fieldInvcNo.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP640[3]+"(송장번호)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldSweetTrackerDlvCd = (String) hashMap.get(COLUMN_NAME_WMSOP640[4]);
				if(fieldSweetTrackerDlvCd == null || fieldSweetTrackerDlvCd.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP640[4]+"(택배사번호)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldOrdDate = (String) hashMap.get(COLUMN_NAME_WMSOP640[6]);
				if(fieldOrdDate == null || fieldOrdDate.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP640[6]+"(관리일자)는 필수 값입니다. 라인 : "+rowNum);
				}
			}
			
			m = service.saveCsv(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID   : dlvSenderInfo
	 * Method 설명 : 택배배송 송화인정보
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP640/dlvSenderInfo.action")
	public ModelAndView dlvSenderInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.dlvSenderInfo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : boxNoUpdate
	 * Method 설명	: boxNoUpdate
	 * 작성자			: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/boxNoUpdate.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.boxNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: dlvPrintPoiNoUpdate
	 * Method 설명	: 택배접수
	 * 작성자                 	: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP640/dlvPrintPoiNoUpdate.action")
	public ModelAndView dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.dlvPrintPoiNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP640/invcNoUpdate.action")
	public ModelAndView invcNoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.invcNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	@RequestMapping("/WMSOP640/invcNoInfoUpdate.action")
	public ModelAndView invcNoInfoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.invcNoInfoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: getCustOrdDegree
	 * Method 설명	: 화주별 주문차수
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP640/getCustOrdDegree.action")
	public ModelAndView getCustOrdDegree(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustOrdDegree(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
}
