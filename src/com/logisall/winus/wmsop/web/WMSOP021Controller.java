package com.logisall.winus.wmsop.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsop.service.WMSOP021Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP021Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP021Service")
	private WMSOP021Service service;

	/*-
	 * Method ID    : wmsop021
	 * Method 설명      : 물류용기입고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP021.action")
	public ModelAndView wmsop021(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP021", service.selectPoolGrp(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 물류용기입고관리  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP021/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}
