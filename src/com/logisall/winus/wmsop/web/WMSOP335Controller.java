package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP335Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP335Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP335Service")
	private WMSOP335Service service;

	@RequestMapping("/WINUS/WMSOP335.action")
	public ModelAndView wmsop335(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP335", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP3351.action")
	public ModelAndView wmsop3351(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP3351", service.selectItemGrp(model));
	}
	
	/*-
	 * Method ID    : listByCustSummary
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP335/listByCustSummary.action")
	public ModelAndView listByCustSummary(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustSummary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustDetail
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP335/listByCustDetail.action")
	public ModelAndView listByCustDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustCount
	 * Method 설명      : 화주별 출고관리  간략 조회 카운트
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP335/listCountByCust.action")
	public ModelAndView listCountByCust(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listCountByCust(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : outInvalidView
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP335/outInvalidView.action")
	public ModelAndView deviceInvalidView(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.outInvalidView(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : autoBestLocSaveMultiV3
	 * Method 설명      :  로케이션추천 자동V3
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP335/autoBestLocSaveMultiV3.action")
	public ModelAndView autoBestLocSaveMultiV3(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoBestLocSaveMultiV3(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : /WMSOP335popSearoV2.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP335popSearoV2.action")
	public ModelAndView WMSOP335popSearoV2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP335popSearoV2");
	}
	@RequestMapping("/WMSOP3351popSearoV2.action")
	public ModelAndView WMSOP3351popSearoV2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP3351popSearoV2");
	}
	
	/*-
	 * Method ID    : pickingListSaeroV2Poi
	 * Method 설명      : 토탈피킹리스트 확정 새로피엔엘
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP335/pickingListSaeroV2Poi.action")
	public ModelAndView pickingListSaeroV2Poi(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.updatePickingTotalV2Poi(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	@RequestMapping("/WMSOP335/pickingListSaeroV2.action")
	public ModelAndView pickingListSaeroV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.updatePickingTotalV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : confirmPickingTotalV2
	 * Method 설명      : 토탈피킹리스트 확정
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP335/confirmPickingTotalV2.action")
	public ModelAndView confirmPickingTotalV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.confirmPickingTotalV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID	: billingListDetailV2
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOP335/billingListDetailV2.action")
    public ModelAndView billingListDetailV2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.billingListDetailV2(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
	 * Method ID    : autoDeleteLocSaveV2
	 * Method 설명      : 로케이션지정 삭제 일괄
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP335/autoDeleteLocSaveV2.action")
	public ModelAndView autoDeleteLocSaveV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoDeleteLocSaveV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : saveOutCompleteV2
	* Method 설명      : 출고확정
	* 작성자                 : KHKIM
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP335/saveOutCompleteV2.action")
	public ModelAndView saveOutCompleteV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutCompleteV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP335/ritemIdSearchCd.action")
	public ModelAndView ritemIdSearchCd(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.ritemIdSearchCd(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
