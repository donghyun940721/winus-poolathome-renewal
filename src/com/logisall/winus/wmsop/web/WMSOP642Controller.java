package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP642Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP642Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	static final String[] COLUMN_NAME_WMSOP642 = {"ORD_ID", "ORD_SEQ", "DLV_COMP_CD", "INVC_NO", "SWEET_TRACKER_DLV_CD", "DLV_PRICE", "ORD_DATE", "BOX_NO", "PARCEL_QTY"};

	
	@Resource(name = "WMSOP642Service")
	private WMSOP642Service service;

	/*-
	 * Method ID    : wmsop642
	 * Method 설명      : 택배접수
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP642.action")
	public ModelAndView wmsop642(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP642", service.selectItemGrp(model));
	}
//	@RequestMapping("/WINUS/WMSOP641.action")
//	public ModelAndView wmsop641(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsop/WMSOP641", service.selectItemGrp(model));
//	}
	@RequestMapping("/WMSOP642pop2.action")
	public ModelAndView wmsom010pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP642pop2");
	}
	
	/*-
     * Method ID    : WMSOP642pop3
     * Method 설명      : 반송접수템플릿업로드 팝업
     * 작성자                 : schan
     * @param   model
     * @return  
     * @throws Exception 
     */
   @RequestMapping("/WMSOP642pop3.action")
    public ModelAndView WMSOP642pop3(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsop/WMSOP642pop3");
    }
	
	/*-
	 * Method ID    : listByDlvSummary
	 * Method 설명	: 출고관리(송장발행화면)
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/listByDlvSummary.action")
	public ModelAndView listByDlvSummary(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByDlvSummary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list2ByDlvHistory
	 * Method 설명	: 출고관리(택배접수이력엑셀등록기록조회)
	 * 작성자
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/list2ByDlvHistory.action")
	public ModelAndView list2ByDlvHistory(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list2ByDlvHistory(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list3ByDlvHistory TAB3
	 * Method 설명	: 출고관리(택배접수이력엑셀등록기록조회)
	 * 작성자 SUMMER HYUN
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/list3ByDlvHistory.action")
	public ModelAndView list3ByDlvHistory(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list3ByDlvHistory(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: DlvShipment642
	 * Method 설명	: 택배접수
	 * 작성자                 	: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 
	@RequestMapping("/WMSOP642/DlvShipment.action")
	public ModelAndView DlvShipment(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		
		try {
			// 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS //
			if(model.get("PARCEL_COM_TY").equals("04") || model.get("PARCEL_COM_TY").equals("08") || model.get("PARCEL_COM_TY").equals("06")){
				m = service.DlvShipmentComm(model);
				
			}else if(model.get("PARCEL_COM_TY").equals("05")){
				m = service.DlvShipmentHanjin(model);				
			}else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}*/
	
	

	/*-
	 * Method ID	: DlvShipment642
	 * Method 설명	: 택배접수
	 * 작성자                 	: yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/DlvInvcNoOrder.action")
	public ModelAndView DlvInvcNoOrder(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		try {
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			 if(model.get("PARCEL_COM_TY").equals("04") 
					 	|| model.get("PARCEL_COM_TY").equals("06")
					 	|| model.get("PARCEL_COM_TY").equals("08") ){
				m = service.DlvInvcNoOrderComm(model);		
				
			 }else if(model.get("PARCEL_COM_TY").equals("05")){
				m = service.DlvShipmentHanjin(model);		
				
			 }else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID	: DlvInvcNoOrderTotal
	 * Method 설명	: 택배접수
	 * 작성자                 	: yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/DlvInvcNoOrderTotal.action")
	public ModelAndView DlvInvcNoOrderTotal(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		try {
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			 if(model.get("PARCEL_COM_TY").equals("04") 
					 	|| model.get("PARCEL_COM_TY").equals("06")
					 	|| model.get("PARCEL_COM_TY").equals("08") ){
				//eai 정보 말아서 1번 송신
				m = service.DlvInvcNoOrderCommTotal(model);		
				
			 }else if(model.get("PARCEL_COM_TY").equals("05")){
				m = service.DlvShipmentHanjin(model);		
				
			 }else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/*-
	 * Method ID	: DlvShipment642
	 * Method 설명	: 택배접수반품
	 * 작성자                 	: yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/DlvShipmentRtn.action")
	public ModelAndView DlvShipmentRtn(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		try {
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			if(model.get("PARCEL_COM_TY").equals("04") 
					|| model.get("PARCEL_COM_TY").equals("08") 
					|| model.get("PARCEL_COM_TY").equals("06")){
				m = service.DlvShipmentRtnComm(model);
				
			}else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	
	/*-
	 * Method ID	: DlvInvcNoPrintPre
	 * Method 설명	: 택배접수
	 * 작성자                 	: yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/DlvInvcNoPrintPrev.action")
	public ModelAndView DlvInvcNoPrintPrev(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		try {
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			if(model.get("PARCEL_COM_TY").equals("04")){
				
				//eai 정보 말아서 1번 송신
				model.put("PrevYN"		, "Y");
				m = service.DlvInvcNoOrderCommTotal(model);		
				
			}else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	
	/*-
	 * Method ID	: DlvShipmentPost
	 * Method 설명	: 택배접수
	 * 작성자                 	: yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/DlvShipmentPost.action")
	public ModelAndView DlvShipmentPost(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		try {
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			if(model.get("PARCEL_COM_TY").equals("04")){
				//eai 정보 말아서 1번 송신
				m = service.DlvShipmentPostTotal(model);		
				
			}else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID : wmsop642e3
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSOP642E3.action")
	public ModelAndView wmsop642e3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP642E3");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP642/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP642, 0, startRow, 10000, 0);
			
			for(int i =0 ; i<list.size() ; i++){
				Map<String, Object> hashMap = new HashMap<String, Object>();
				hashMap = list.get(i);
				int rowNum = i+2;
				
				String fieldOrdId = (String) hashMap.get(COLUMN_NAME_WMSOP642[0]);
				if(fieldOrdId == null || fieldOrdId.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP642[0]+"(주문번호)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldOrdSeq = (String) hashMap.get(COLUMN_NAME_WMSOP642[1]);
				if(fieldOrdSeq == null || fieldOrdSeq.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP642[1]+"(주문seq)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldDlvCompCd = (String) hashMap.get(COLUMN_NAME_WMSOP642[2]);
				if(fieldDlvCompCd == null || fieldDlvCompCd.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP642[2]+"(택배사코드)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldInvcNo = (String) hashMap.get(COLUMN_NAME_WMSOP642[3]);
				if(fieldInvcNo == null || fieldInvcNo.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP642[3]+"(송장번호)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldSweetTrackerDlvCd = (String) hashMap.get(COLUMN_NAME_WMSOP642[4]);
				if(fieldSweetTrackerDlvCd == null || fieldSweetTrackerDlvCd.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP642[4]+"(택배사번호)는 필수 값입니다. 라인 : "+rowNum);
				}
				
				String fieldOrdDate = (String) hashMap.get(COLUMN_NAME_WMSOP642[6]);
				if(fieldOrdDate == null || fieldOrdDate.equals("")){
					throw new Exception("Exception:"+COLUMN_NAME_WMSOP642[6]+"(관리일자)는 필수 값입니다. 라인 : "+rowNum);
				}
			}
			
			m = service.saveCsv(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID   : dlvInfoInsert / insertDF
	 * Method 설명 : 택배이력등록 (출고관리 - 용인)
	 * 작성자      : SUMMER HYUN
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP642/dlvInfoInsert.action")
	public ModelAndView dlvInfoInsert(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.dlvInfoInsert(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID   : dlvSenderInfo
	 * Method 설명 : 택배배송 송화인정보
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP642/dlvSenderInfo.action")
	public ModelAndView dlvSenderInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			
			/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
			if(("05").equals(model.get("PARCEL_COM_TY").toString())){
				mav = new ModelAndView("jsonView", service.dlvSenderInfoDetail(model));					
			}
			else{
				mav = new ModelAndView("jsonView", service.dlvSenderInfo(model));
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID   : dlvSenderInfo
	 * Method 설명 : 택배배송 송화인정보
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP642/dlvDlvCompInfo.action")
	public ModelAndView dlvDlvCompInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
				mav = new ModelAndView("jsonView", service.dlvDlvCompInfo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : boxNoUpdate
	 * Method 설명	: boxNoUpdate
	 * 작성자			: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/boxNoUpdate.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.boxNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: dlvPrintPoiNoUpdate
	 * Method 설명	: 택배접수
	 * 작성자                 	: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/dlvPrintPoiNoUpdate.action")
	public ModelAndView dlvPrintPoiNoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.dlvPrintPoiNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP642/invcNoUpdate.action")
	public ModelAndView invcNoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.invcNoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
		
	@RequestMapping("/WMSOP642/invcNoInfoUpdate.action")
	public ModelAndView invcNoInfoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.invcNoInfoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: getCustOrdDegree
	 * Method 설명	: 화주별 주문차수
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP642/getCustOrdDegree.action")
	public ModelAndView getCustOrdDegree(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustOrdDegree(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	@RequestMapping("/WMSOP642/listByDlvSummaryExcel.action")
	public void listByDlvSummaryExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listByDlvSummaryExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.listeExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void listeExcelDown(HttpServletResponse response, GenericResultSet grs) {
       try{
           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
           String[][] headerEx = {
                                   {MessageResolver.getText("주문번호")   		, "0", "0", "0", "0", "100"}
                                  ,{MessageResolver.getText("상품명")   		, "1", "1", "0", "0", "100"}
                                  ,{MessageResolver.getText("받는분이름")  		, "2", "2", "0", "0", "100"}
                                  ,{MessageResolver.getText("받는분전화번호")   	, "3", "3", "0", "0", "100"}
                                  ,{MessageResolver.getText("받는분핸드폰번호")  , "4", "4", "0", "0", "100"}
                                  ,{MessageResolver.getText("받는분주소")   	, "5", "5", "0", "0", "100"}
                                  ,{MessageResolver.getText("배송메시지")   	, "6", "6", "0", "0", "100"}
                                  ,{MessageResolver.getText("주문일자")  		, "7", "7", "0", "0", "100"}
                                  ,{MessageResolver.getText("우편번호")   		, "8", "8", "0", "0", "100"}
                                  ,{MessageResolver.getText("우편번호")   		, "9", "9", "0", "0", "100"}
                                  ,{MessageResolver.getText("상품코드")  		, "10", "10", "0", "0", "100"}
                                  ,{MessageResolver.getText("수량")   		, "11", "11", "0", "0", "100"}
                                  ,{MessageResolver.getText("쇼핑몰")   		, "12", "12", "0", "0", "100"}
                                  ,{MessageResolver.getText("사방넷주문번호")   	, "13", "13", "0", "0", "100"}
                                  ,{MessageResolver.getText("운송장번호")   	, "14", "14", "0", "0", "100"}
                                 };
           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
           String[][] valueName = {
                                   {"ORG_ORD_ID"  			, "S"},
                                   {"ITEM_NM"  				, "S"},
                                   {"RCVR_NM"  				, "S"},
                                   {"RCVR_TEL"  			, "S"},
                                   {"RCVR_TEL2" 			, "S"},
                                   {"ADDR"  				, "S"},
                                   {"REMARK_1"  			, "S"},
                                   {"ORD_DT"  				, "S"},
                                   {"ZIP"  					, "S"},
                                   {"ZIP1"  				, "S"},
                                   {"ITEM_CODE"  			, "S"},
                                   {"ORD_QTY"  				, "N"},
                                   {"DATA_SENDER_NM"  		, "S"},
                                   {"LEGACY_ORG_ORD_NO"  	, "S"},
                                   {"INVC_NO"				, "S"}
                                  }; 
           
			// 파일명
			String fileName = MessageResolver.getText("택배송장이력");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID	: DlvShipDelete
	 * Method 설명	: 택배송장삭제
	 * 작성자                 	: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/DlvShipDelete.action")
	public ModelAndView DlvShipDelete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		/* 01 우체국 | 04 CJ대한통운 | 05 한진택배 | 06 로젠택배 | 08 롯데택배 | 11 일양로지스 | 12 EMS | 13 DHL | 14 UPS */
		String[] enableCompanyList = {"04", "08", "05", "06"};		
		
		try {
			String dlvCom = model.get("PARCEL_COM_TY").toString(); 
			
			if(Arrays.asList(enableCompanyList).contains(dlvCom)){
				model.put("DLV_COMP_CD"	, model.get("PARCEL_COM_TY"));
				m = service.DlvShipDelete(model);
			}		
			else{
				m.put("header", "E");
				m.put("message", "택배사 연동이 되지 않았거나 해당 서비스를 이용 할 수 없습니다.");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: getCustInfo
	 * Method 설명	: 고객정보별 원주문번호 조회
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP642/getCustInfo.action")
	public ModelAndView getCustInfo(Map<String, Object> model, HttpServletRequest request) {
		ModelAndView mav = null;
		try {
			String rcvrNmDecode = URLDecoder.decode(request.getParameter("rcvrNm"), "utf-8");
			String rcvrAddrDecode = URLDecoder.decode(request.getParameter("rcvrAddr"), "utf-8");
			String rcvrDetailAddrDecode = URLDecoder.decode(request.getParameter("rcvrDetailAddr"), "utf-8");
			model.put("rcvrNm", rcvrNmDecode);
			model.put("rcvrAddr", rcvrAddrDecode);
			model.put("rcvrDetailAddr", rcvrDetailAddrDecode);
			
			mav = new ModelAndView("jsonView", service.getCustInfo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: getCustInfoV2
	 * Method 설명	: 고객정보별 원주문번호 조회 om010, op010 조인
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP642/getCustInfoV2.action")
	public ModelAndView getCustInfoV2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustInfoV2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : custInfoUpdate
	 * Method 설명	: custInfoUpdate
	 * 작성자			: chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/custInfoUpdate.action")
	public ModelAndView custInfoUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.custInfoUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	

	
	/*-
	 * Method ID    : custInfoUpdateAddrRefineEAI
	 * Method 설명	: custInfoUpdateAddrRefineEAI
	 * 작성자			: yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP642/custInfoUpdateAddrRefineEAI.action")
	public ModelAndView custInfoUpdateAddrRefineEAI(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		try {
			m = service.custAddrRefineEAI(model);
			String rstCD = (String) m.get("header");
			if(rstCD.equals("Y")){//성공시
				m = service.custInfoUpdate(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : custAddrRefineEAI
	 * Method 설명	: custAddrRefineEAI
	 * 작성자			: yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	
	@RequestMapping("/WMSOP642/custAddrRefineEAI.action")
	public ModelAndView custAddrRefineEAI(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String hostUrl = request.getServerName();
		model.put("hostUrl"		, hostUrl);
		
		try {
			m = service.custAddrRefineEAI(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("header", "E");
			m.put("message", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	 
	/*
     * Method ID  : dlvInvcItemNmSetInfo
     * Method 설명  : 택배 송장별 상품명 명칭 정보
     * 작성자             : yhku
     * @param model
     * @return
     */
	@RequestMapping("/WMSOP642/dlvInvcItemNmSetInfo.action")
	public ModelAndView dlvInvcItemNmSetInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			
			mav = new ModelAndView("jsonView", service.dlvInvcItemNmSetInfo(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*
     * Method ID  : uploadRtnOrder
     * Method 설명  : 반품접수 엑셀입력
     * 작성자             : schan
     * @param model
     * @return
     */
    @RequestMapping("/WMSOP642/uploadRtnOrder.action")
    public ModelAndView uploadRtnOrder(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m = null;
        try {
               
            request.setCharacterEncoding("utf-8");

            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
            MultipartFile file = multipartRequest.getFile("txtFile");
            String fileName = file.getOriginalFilename();
            String filePaths = ConstantIF.TEMP_PATH;

            if (!FileHelper.existDirectory(filePaths)) {
                FileHelper.createDirectorys(filePaths);
            }
            File destinationDir = new File(filePaths);
            File destination = File.createTempFile("excelTemp", fileName, destinationDir);
            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

            int startRow = Integer.parseInt((String) model.get("startRow"));
            
            List<Map> list = new ArrayList<Map>();
            
            String[] COLUMN_NAME_WMSMS090_PK = {"INVC_NO","RITEM_CD","ORD_QTY","ORD_DESC"};
            
            list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSMS090_PK, 0, startRow, 100000, 0);
            
            Map<String, Object> mapBody = new HashMap<String, Object>();
            mapBody.put("vrCustId"          ,model.get("vrCustId"));
            mapBody.put("SS_SVC_NO"         ,model.get("SS_SVC_NO"));
            mapBody.put("SS_CLIENT_IP"      ,model.get("SS_CLIENT_IP"));
            mapBody.put("SS_USER_NO"        ,model.get("SS_USER_NO"));
            mapBody.put("hostUrl"           ,request.getServerName());
            
            m = service.uploadRtnOrder(mapBody, list);

            if (destination.exists()) {
                destination.delete();
            }

            destination.deleteOnExit();
            mav.addAllObjects(m);

        } catch (Exception e) {
            if (log.isErrorEnabled()) {
                log.error("Fail to upload Excel info :", e);
            }
            m = new HashMap<String, Object>();
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("MSG_ORA", e.getMessage());
            m.put("errCnt", "1");
            mav.addAllObjects(m);
        }
        return mav;
    }
	
	
}
