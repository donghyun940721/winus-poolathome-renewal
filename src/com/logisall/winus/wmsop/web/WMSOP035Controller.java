package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP035Service;
import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

import org.apache.commons.codec.binary.Base64;

@Controller
public class WMSOP035Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP035Service")
	private WMSOP035Service service;
	
	@Resource(name = "WMSIF000Service")
	private WMSIF000Service ifService;
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP035.action")
	public ModelAndView wmsop035(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP035");
	}
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035pop.action")
	public ModelAndView wmsop035pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP035pop");
	}
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035pop2.action")
	public ModelAndView wmsop035pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP035pop2");
	}
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035pop3.action")
	public ModelAndView wmsop035pop3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP035pop3");
	}
	
	/*-
	 * Method ID    : wmsop035
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035pop4.action")
	public ModelAndView wmsop035pop4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP035pop4");
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 출고관리  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list2
	 * Method 설명      : 출고관리  조회
	 * 작성자                 : KSJ 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/list2.action")
	public ModelAndView list2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			Map<String, Object> searchResult = service.list(model);
			mav = new ModelAndView("jqGridJsonView", searchResult);
			// mav.getModel().get("LIST").
//			if(searchResult.get("LIST").)
			// mav.addObject("PrintResut", )
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : searchUnshippedDHL
	 * Method 설명      : 올리브영 미발행 내역 조회
	 * 작성자                 : chSong -> KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/searchUnshippedDHL.action")
	public ModelAndView searchUnshippedDHL(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jqGridJsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.searchUnshippedDHL(model);
			//m.put("totCnt", 10);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID   : getCustIdByLcId
	 * Method 설명 : LC_ID로 해당 코드의 화주ID 가져오기
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP035/list2.action")
	public ModelAndView getCustIdByLcId(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}

	
	/*-
	 * Method ID : poplist
	 * Method 설명 : 세트상품 조회 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP035/pdfDown.action")
	public @ResponseBody byte[] pdfDown(Map<String, Object> model) throws Exception {
		model.put("ordIds", ((String)model.get("ordIds")).replace("&apos;", "'"));
		return service.pdfDown(model);
	}
	
	/*-
	 * Method ID    : 
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP035/outSaveComplete.action")
	public ModelAndView outSaveComplete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.outSaveComplete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    		: invoicePrintingEMS
	 * Method 설명       :  EMS 송장 출력  (출고확정 -> 프린트시간 업데이트 -> PDF 출력)
	 * 작성자                : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP035/invoicePrintingEMS.action")
	public ModelAndView invoicePrintingEMS(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			if(!model.get("workStat").equals("990")){ // 출고확정상태가 아닐 경우
				m.put("outSaveComplete", service.outSaveCompleteV2(model)); // 단건 출고확정 처리	
			}
			
			m.put("updatetPrintDt", service.updatetPrintDt(model));
			m.put("emsAddress", service.emsAddress(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("ERR_MSG", e.getMessage());
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
    /**
	 * encodeBase64toString
	 * @param data
	 * @return
	 */
	private static String encodeBase64toString(byte[] data) {
		String result = "";
		Base64 base64encoder = new Base64();
		try {
			result = new String(base64encoder.encode(data),"UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	/*-
	 * Method ID    		: invoicePrintingDHL
	 * Method 설명       :  DHL 송장 출력  (출고확정 -> 프린트시간 업데이트 -> PDF 출력)
	 * 작성자                : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP035/invoicePrintingDHL.action")
	public ModelAndView  invoicePrintingDHL(Map<String, Object> model) throws Exception { // @ResponseBody byte[]
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			if(!model.get("workStat").equals("990")){ // 출고확정상태가 아닐 경우
				m.put("outSaveComplete", service.outSaveCompleteV2(model)); // 단건 출고확정 처리	
			}
			m.put("updatetPrintDt", service.updatetPrintDt(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to invoicePrintingDHL :"+ e.getMessage());
			}
		}
		
		m.put("pdfData", encodeBase64toString(service.pdfDown(model)));
		mav.addAllObjects(m);
		
		return mav;
	}
	
	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030/dhlExcel.action")
	public void excelDown2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.dhlExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	

	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
       try{
    	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
        //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
    	   String[][] headerEx = {
		                		 {MessageResolver.getText("예약구분")				, "0", "0", "0", "0", "100"}
	                            ,{MessageResolver.getText("집하예정일")				, "1", "1", "0", "0", "100"}
	                            ,{MessageResolver.getText("보내는사람")				, "2", "2", "0", "0", "100"}
	                            ,{MessageResolver.getText("보내는분연락처")			, "3", "3", "0", "0", "100"}
	                            ,{MessageResolver.getText("판매자 연락처")			, "4", "4", "0", "0", "100"}
	                            
	                            ,{MessageResolver.getText("보내는분우편번호")			, "5", "5", "0", "0", "100"}
	                            ,{MessageResolver.getText("택배 처리점소명")			, "6", "6", "0", "0", "100"}
	                            ,{MessageResolver.getText("받는분성명")				, "7", "7", "0", "0", "100"}
	                            ,{MessageResolver.getText("받는분전화번호")			, "8", "8", "0", "0", "100"}
	                            ,{MessageResolver.getText("받는분기타연락처")			, "9", "9", "0", "0", "100"}
	                            
	                            ,{MessageResolver.getText("받는분우편번호")			, "10", "10", "0", "0", "100"}
	                            ,{MessageResolver.getText("받는분주소(전체및분할)")		, "11", "11", "0", "0", "100"}
	                            ,{MessageResolver.getText("운송장번호")				, "12", "12", "0", "0", "100"}
	                            ,{MessageResolver.getText("고객주문번호")			, "13", "13", "0", "0", "100"}
	                            ,{MessageResolver.getText("품목명")				, "14", "14", "0", "0", "100"}
	                            
	                            ,{MessageResolver.getText("수량")					, "15", "15", "0", "0", "100"}
	                            ,{MessageResolver.getText("아이스팩 추가 개수")			, "16", "16", "0", "0", "100"}
	                            ,{MessageResolver.getText("박스수량")				, "17", "17", "0", "0", "100"}
	                            ,{MessageResolver.getText("박스타입")				, "18", "18", "0", "0", "100"}
	                            ,{MessageResolver.getText("기본운임")				, "19", "19", "0", "0", "100"}
	                            
	                            ,{MessageResolver.getText("배송메세지1")			, "20", "20", "0", "0", "100"}
	                            ,{MessageResolver.getText("배송메세지2")			, "21", "21", "0", "0", "100"}
	                            ,{MessageResolver.getText("운임구분")				, "22", "22", "0", "0", "100"}
	                            ,{MessageResolver.getText("총중량")				, "23", "23", "0", "0", "100"}
                              };
        //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
    	   String[][] valueName = {
		                		 {"ISNULL"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"SALES_CUST_NM_FROM"	, "S"}
	                            ,{"PHONE_2_FROM"		, "S"}
	                            ,{"TEL"					, "S"}
	                            
	                            ,{"ISNULL"				, "S"}
	                            ,{"DEALT_BRAN_NM"		, "S"}
	                            ,{"SALES_CUST_NM_TO"	, "S"}
	                            ,{"PHONE_1"				, "S"}
	                            ,{"PHONE_2_TO"			, "S"}
	                            
	                            ,{"ISNULL"				, "S"}
	                            ,{"ADDR"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"SALES_CUST_NO"		, "S"}
	                            ,{"ITEM_NAME"			, "S"}
	                            
	                            ,{"ORD_QTY"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            
	                            ,{"ORD_DESC"			, "S"}
	                            ,{"ETC1"				, "S"}
	                            ,{"ISNULL"				, "S"}
	                            ,{"SUM_REAL_OUT_WEIGHT"	, "S"}
                               }; 
                
    			// 파일명
                String fileName = MessageResolver.getText("CJ대한통운");
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";

    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : list2DHL
	 * Method 설명      : 수출신고정보
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/list2DHL.action")
	public ModelAndView list22(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2DHL(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : list2DHL
	 * Method 설명      : 수출신고정보
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/list2EMS.action")
	public ModelAndView list2EMS(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2EMS(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : list2DHL
	 * Method 설명      : 수출신고정보
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/request2EMS.action")
	public ModelAndView request2EMS(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.request2EMS(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : list2DHL
	 * Method 설명      : 수출신고정보
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/export2EMS.action")
	public ModelAndView export2EMS(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.export2EMS(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : saveList2DHL
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/saveList2DHL.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveList2DHL(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : saveList2DHL
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/saveExport2EMS.action")
	public ModelAndView saveExport2EMS(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			String[] cellName = new String[11];
			cellName[0] = "SEQ";
			cellName[1] = "EXP_DCLR_NO";
			cellName[2] = "ORG_ORD_ID";
			cellName[3] = "D_COUNTRY";
			cellName[4] = "J_CUST_NM";
			cellName[5] = "CUSTOM_CLEARANCE_NUMBER";
			cellName[6] = "D_CUST_NM";
			cellName[7] = "ACPT_DT";
			cellName[8] = "LOAD_DTY_TM_IM";
			cellName[9] = "PRICE";
			cellName[10] = "AGENCY";
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 10000, 0);
			int listSize = list.size();
			for(int i = listSize - 1; i >= 0; i--){
				Object seq = ((Map)list.get(i)).get("SEQ");
				if(seq == null || "".equals((String)seq)){
					list.remove(i);
				}else{
					String ordId = service.getOrdId((String)((Map)list.get(i)).get("ORG_ORD_ID"));
					((Map)list.get(i)).put("ORD_ID", ordId);
				}
			}
			//run
			m = service.saveExport2EMS(list);
			int listReSize = list.size();
			for(int i = 0; i < listReSize; i++){
				Map<String, Object> map = new HashMap<>();
				String cMethod		= "POST";
				String cUrl			= "WINUS.OLIVE:OLIVE_RS/SHIPPING_REQUEST_TOTAL";
				String pOrdId		= (String)((Map)list.get(i)).get("ORD_ID");
				map.put("cMethod"		, cMethod);
				map.put("cUrl"		, cUrl);
				map.put("pOrdId"		, pOrdId);
				
				ifService.crossDomainHttpWsMobile(map);
				
			}
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	/*-
	 * Method ID    : address
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/emsAddress.action")
	public ModelAndView emsAddress(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.emsAddress(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : addressHistory
	 * Method 설명      : WMSOM010History 주소 변경 이력 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/addressHistory.action")
	public ModelAndView addressHistory(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.addressHistory(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : addressOM010
	 * Method 설명      : WMSOM010 원주문 최근 주소 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/addressOM010.action")
	public ModelAndView addressOM010(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.addressOM010(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : address
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/checkBlNo.action")
	public ModelAndView checkBlNo(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.checkBlNo(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : address
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/updateBlNo.action")
	public ModelAndView updateBlNo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			String[] cellName = new String[2];
			cellName[0] = "ORG_ORD_ID";
			cellName[1] = "NEW_BLNO";
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 10000, 0);
			int listSize = list.size();
			for(int i = listSize - 1; i >= 0; i--){
				Object seq = ((Map)list.get(i)).get("ORG_ORD_ID");
				if(seq == null || "".equals((String)seq)){
					list.remove(i);
				}else{
					String ordId = service.getOrdId((String)((Map)list.get(i)).get("ORG_ORD_ID"));
					((Map)list.get(i)).put("ORD_ID", ordId);
				}
			}
			//run
			boolean result = service.updateBlNo(list);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
			if(result){
				int listReSize = list.size();
				for(int i = 0; i < listReSize; i++){
					Map<String, Object> map = new HashMap<>();
					String cMethod		= "POST";
					String cUrl			= "WINUS.OLIVE:OLIVE_RS/TRACKING_REQUEST_TOTAL";
					String pOrdId		= (String)((Map)list.get(i)).get("ORD_ID");
					map.put("cMethod"		, cMethod);
					map.put("cUrl"		, cUrl);
					map.put("pOrdId"		, pOrdId);
					
					ifService.crossDomainHttpWsMobile(map);
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : insertShipment
	 * Method 설명      : 송장조회(올리브영) EMS, DHL 송장접수 (현장용)
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/insertShipment.action")
	public ModelAndView insertShipment(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			
			Map<String, Object> map = new HashMap<>();
			
			String cMethod		= "POST";
			String cUrl			= "WINUS.OLIVE:OLIVE_RS/SHIPPING_REQUEST_TOTAL";
			String pOrdId 		= service.getOrdId((String) model.get("ORG_ORD_ID"));
			
			map.put("cMethod"		, cMethod);
			map.put("cUrl"		    , cUrl);
			map.put("pOrdId"		, pOrdId);
				
			m = ifService.crossDomainHttpWsMobile(map);
			
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE5
	 * Method 설명      : 올리브영 수출신고번호 or 운송장번호 누락 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/listE5.action")
	public ModelAndView listE5(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listE5(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE6
	 * Method 설명      : 올리브영 운송장 출력여부 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/listE6.action")
	public ModelAndView listE6(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listE6(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : selectPrintDt
	 * Method 설명      : 올리브영 운송장 출력 시간 조회
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/selectPrintDt.action")
	public ModelAndView selectPrintDt(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			 mav = new ModelAndView("jsonView", service.selectPrintDt(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : selectPrintDt
	 * Method 설명      : 올리브영 운송장 출력 시간 업데이트
	 * 작성자                 : kSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP035/updatetPrintDt.action")
	public ModelAndView updatetPrintDt(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updatetPrintDt(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		
		mav.addAllObjects(m);
		return mav;
	}
}
