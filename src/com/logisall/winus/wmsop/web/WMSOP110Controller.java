package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP110Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP110Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSOP110Service")
    private WMSOP110Service service;
    
    
	static final String[] COLUMN_NAME_WMSOP110 = {
			"ITEM_GRP_ID", "WGT_ARROW_RATE", "BEST_DATE_NUM", "BEST_DATE_UNIT"
	};

	/*-
	 * Method ID    : wmsop110
	 * Method 설명  : 상품별물류기기관리 화면
	 * 작성자       : kwt
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP110.action")
	public ModelAndView wmsop110(Map<String, Object> model) throws Exception {
		Map<String, Object> m = null;
		ModelAndView mav = new ModelAndView("winus/wmsop/WMSOP110");
		try {
			m = service.getItemList(model);
			if (m != null) {
				mav.addAllObjects(m);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : list
	 * Method 설명  : 상품별물류기기 조회
	 * 작성자       : kwt
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP110/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : save
	 * Method 설명  : 상품별물류기기정보 저장
	 * 작성자       : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP110/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명  : 상품별물류기기정보 저장
	 * 작성자       : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP110/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : listItem
	 * Method 설명 : 상품별물류기기관리 창고 팝업 자동조회
	 * 작성자      : kwt
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP110/listItem.action")
	public ModelAndView listItem(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		m = service.listItem(model);
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명  : 엑셀다운로드
	 * 작성자       : kwt
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP110/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel file...", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("상품코드")    , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")      , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("상품유형")    , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("물류기기명")   , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("상품수량")     , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("물류기기수량")  , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("커버수량")     , "6", "6", "0", "0", "100"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"GOODS_ITEM_CD"   , "S"},
                                    {"ITEM_KOR_NM"     , "S"},
                                    {"ITEM_TYPE_EXCEL"     , "S"},
                                    {"EXC_POOL_KOR_NM_EXCEL"     , "S"},
                                    {"GOODS_QTY"     , "N"},
                                    {"POOL_QTY"     , "N"},
                                    {"COVER_QTY"     , "N"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("상품별물류기기관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : listPool
	 * Method 설명  : 삼품의 물류기기조회
	 * 작성자       : chsong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP110/listPool.action")
	public ModelAndView listPool(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listPool(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : wmsop110E2
	 * Method 설명      : 엑셀업로드 화면
	 * 작성자                 : kwt
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP110E2.action")
	public ModelAndView wmsop110E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP110E2");
	}

	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP096/uploadItemGroupInfo.action")
	public ModelAndView uploadItemGroupInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP110, 0, startRow, 10000, 0);
			m = service.saveCsvItemGroup(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to upload excel file...", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP110/saveS2.action")
	public ModelAndView saveS2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveS2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
    
}
