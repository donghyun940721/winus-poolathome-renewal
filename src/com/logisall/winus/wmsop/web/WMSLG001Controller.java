package com.logisall.winus.wmsop.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSLG001Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSLG001Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSLG001Service")
	private WMSLG001Service service;

	/*-
	 * Method ID    : wmsop010
	 * Method 설명      : 입출고현황 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSLG001.action")
	public ModelAndView wmslg001(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSLG001");
	}

//	/*-
//	 * Method ID    : list
//	 * Method 설명      : 입출고현황  조회
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
	@RequestMapping("/WMSLG001/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSLG001/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("주문번호")      , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("입고/출고")     , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("상품코드")      , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")       , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("입/출고확정일")  , "4", "4", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("입/출고변경일")  , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("변경사유1")     , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("변경사유2")     , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("등록일")       , "8", "8", "0", "0", "100"},
                                   {MessageResolver.getText("등록자")       , "9", "9", "0", "0", "100"}
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"ORD_ID"         , "S"},
                                    {"ORD_TYPE_NM"    , "S"},
                                    {"RITEM_CD"       , "S"},
                                    {"RITEM_NM"       , "S"},
                                    {"FROM_DT"   	  , "S"},
                                    
                                    {"TO_DT"   		  , "S"},
                                    {"REASON1"        , "S"},
                                    {"REASON2"        , "S"},
                                    {"REG_DT"         , "S"},
                                    {"REG_NM"         , "S"}
                                    
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("입출고날짜변경이력조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}


}
