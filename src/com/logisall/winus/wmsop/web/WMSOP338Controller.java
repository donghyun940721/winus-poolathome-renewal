package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP338Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP338Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP338Service")
	private WMSOP338Service service;

	@RequestMapping("/WINUS/WMSOP338.action")
	public ModelAndView WMSOP338(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP338", service.selectItemGrp(model));
	}

	@RequestMapping("/WMSOP338/listByHeaderKakao.action")
	public ModelAndView listByHeaderKakao(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByHeaderKakao(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP338/listByCustDetailKakao.action")
	public ModelAndView listByCustDetailKakao(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustDetailKakao(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	@RequestMapping("/WMSOP338/pickingListKakao.action")
	public ModelAndView pickingListKakao(Map<String, Object> model) {
//			m = service.updatePickingTotalKakaoScreen(model);
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		model.put("ORD_IDs", ((String)model.get("ORD_IDs")).replace("&apos;", "'"));
		try {
			m = service.updatePickingTotalKakao(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP338/confirmPickingTotal.action")
	public ModelAndView confirmPickingTotal(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		model.put("ORD_IDs", ((String)model.get("ORD_IDs")).replace("&apos;", "'"));
		try {
//			System.out.println( " +++++++ :  "+ model.get("ORD_IDs"));
			m = service.confirmPickingTotal(model);
		
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP338/saveOutCompleteV2.action")
	public ModelAndView saveOutCompleteV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		model.put("ORD_IDs", ((String)model.get("ORD_IDs")).replace("&apos;", "'"));
		try {
			m = service.saveOutCompleteV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : listByCustCount
	 * Method 설명      : 화주별 출고관리  간략 조회 카운트
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	
	@RequestMapping("/WMSOP338/listCountByCust.action")
	public ModelAndView listCountByCust(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listCountByCust(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : pickingListPoiNo 
	 * Method 설명 : 피킹리스트 poi update
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP338/pickingListPoiNo.action")
	public ModelAndView pickingListPoiNo(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.pickingListPoiNo(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}