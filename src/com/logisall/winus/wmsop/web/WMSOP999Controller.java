package com.logisall.winus.wmsop.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsop.service.WMSOP999Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP999Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP999Service")
	private WMSOP999Service service;

	/*-
	 * Method ID    : wmsop999T1
	 * Method 설명      : 입고주문 신규 팝업 필요 데이타셋 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999T1.action")
	public ModelAndView wmsop999T1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP999T1", service.selectItemGrp(model));
	}
	
	
	/*-
	 * Method ID    : wmsop999T5
	 * Method 설명      : 입고주문 신규 팝업 필요 데이타셋 (대화물류 분리)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999T5.action")
	public ModelAndView wmsop999T5(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP999T5", service.selectItemGrp(model));
	}

	/*-
	 * Method ID    		: WMSOP999T6
	 * Method 설명      	: 출고관리(화주별_OM) 출고주문 신규 팝업 
	 * 작성자                : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999T6.action")
	public ModelAndView wmsop999T6(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP999T6", service.selectItemGrp(model));
	}


	/*-
	* Method ID    : listOrderDetail
	* Method 설명      : 입출고관리 상세 주문정보 조회
	* 작성자                 : chSong
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSOP999/listOrderDetail.action")
	public ModelAndView listOrderDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.listOrderDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	* Method ID    		: listOrderDetailOm
	* Method 설명      	: 입출고관리 상세 주문정보 조회(OM)
	* 작성자            : KSJ
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSOP999/listOrderDetailOm.action")
	public ModelAndView listOrderDetailOm(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.listOrderDetailOm(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listInOrderItem
	 * Method 설명      : 입고관리 상세 상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999/listInOrderItem.action")
	public ModelAndView listInOrderItem(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listInOrderItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveInOrder
	 * Method 설명      : 입고관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/saveInOrder.action")
	public ModelAndView saveInOrder(Map<String, Object> model) {

		if (log.isInfoEnabled()) {
			//log.info(model);
		}

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : deleteInOrder
	 * Method 설명      : 입고관리 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/deleteInOrder.action")
	public ModelAndView deleteInOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteInOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listSimpleInItem
	 * Method 설명      : 간편입고주문 상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999/listSimpleInItem.action")
	public ModelAndView listSimpleInItem(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listSimpleInItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveSimpleInOrder
	 * Method 설명      : 간편입고주문 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/saveSimpleInOrder.action")
	public ModelAndView saveSimpleInOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSimpleInOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop999T2
	 * Method 설명      : 출고주문 신규 팝업 필요 데이타셋 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999T2.action")
	public ModelAndView wmsop999T2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP999T2", service.selectItemGrp(model));
	}

	/*-
	 * Method ID    : listOutOrderItem
	 * Method 설명      : 출고관리 주문 상품내역
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999/listOutOrderItem.action")
	public ModelAndView listOutOrderItem(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listOutOrderItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listLocSearch
	 * Method 설명      : 출고관리 해당상품로케이션
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999/listLocSearch.action")
	public ModelAndView listLocSearch(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listLocSearch(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveOutOrder
	 * Method 설명      : 출고관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/saveOutOrder.action")
	public ModelAndView saveOutOrder(Map<String, Object> model) {
		log.info("model    : " + model);
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	 * Method ID    	     : saveOutOrderOm
	 * Method 설명      	 : 출고관리(OM) 저장
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/saveOutOrderOm.action")
	public ModelAndView saveOutOrderOm(Map<String, Object> model) {
		//log.info("model    : " + model);
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutOrderOm(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	 * Method ID    : etcSaveOutOrder
	 * Method 설명      : 아산물류센터 원지문, BL번호 출고관리 저장
	 * 작성자                 : wdy
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/etcSaveOutOrder.action")
	public ModelAndView etcSaveOutOrder(Map<String, Object> model) {
		//log.info("model    : " + model);
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.etcSaveOutOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : etcSaveInOrder
	 * Method 설명      : 아산물류센터 입고일자 저장
	 * 작성자                 : wdy
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/etcSaveInOrder.action")
	public ModelAndView etcSaveInOrder(Map<String, Object> model) {
		//log.info("model    : " + model);
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.etcSaveInOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : deleteOutOrder
	 * Method 설명      : 출고관리 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/deleteOutOrder.action")
	public ModelAndView deleteOutOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : saveSimpleOutOrder
	 * Method 설명      : 간편출고주문 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/saveSimpleOutOrder.action")
	public ModelAndView saveSimpleOutOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSimpleOutOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop999T3
	 * Method 설명      : 물류용기 입고주문
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999T3.action")
	public ModelAndView wmsop999T3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP999T3");
	}

	/*-
	 * Method ID    : wmsop999T4
	 * Method 설명      : 물류용기 출고주문
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP999T4.action")
	public ModelAndView wmsop999T4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP999T4");
	}
	
	/*-
	 * Method ID    : etcSaveOutOrder
	 * Method 설명      : 아산물류센터 원지문, BL번호 출고관리 저장
	 * 작성자                 : wdy
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/etcSaveV2.action")
	public ModelAndView etcSaveV2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.etcSaveV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveInOrderV2
	 * Method 설명      : 입고관리 저장V2
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/saveInOrderV2.action")
	public ModelAndView saveInOrderV2(Map<String, Object> model) {

		if (log.isInfoEnabled()) {
			//log.info(model);
		}

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInOrderV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveInModifyOrder
	 * Method 설명      : 수정주문 입고
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/saveInModifyOrder.action")
	public ModelAndView saveInModifyOrder(Map<String, Object> model) {
		
		if (log.isInfoEnabled()) {
			//log.info(model);
		}
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInModifyOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : saveInModifyOrderOut
	 * Method 설명      : 수정주문 출고
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP999/saveInModifyOrderOut.action")
	public ModelAndView saveInModifyOrderOut(Map<String, Object> model) {
		
		if (log.isInfoEnabled()) {
			//log.info(model);
		}
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInModifyOrderOut(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
