package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ExcelWriter;
import com.m2m.jdfw5x.egov.database.GenericResultSet;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsop.service.WMSOP103Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP103Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP103Service")
	private WMSOP103Service service;

	static final String[] COLUMN_NAME_WMSOP103 = {
		  "O_SEQ"
		, "O_DLV_COM_ID"
		, "O_SALES_COMPANY_ID"
		, "O_ORG_ORD_ID"
		, "O_BUY_CUST_NM"
		, "O_BUY_PHONE_1"
		, "O_BUY_PHONE_2"
		, "O_CUSTOMER_NM"
		, "O_PHONE_1"
		, "O_PHONE_2"
		, "O_ZIP"
		, "O_ADDR"
		, "O_PRODUCT"
		, "O_DLV_ORD_TYPE"
		, "O_QTY"
		, "O_DLV_SET_DT"
		
		, "O_PAY_REQ_YN"			//(처리유형_접수)PAY_REQ_YN
		, "O_ETC1"					//비고 기타1(인수증메모)
		, "O_MEMO"					//처리요청내역(처리내역_배송메모)
		
		, "O_BUYED_DT"				//발주일
		, "O_DLV_DT"
		, "O_DLV_ORD_STAT"
		, "O_PRODUCT_NM"
		, "O_SERIAL_NO"
		, "O_CASE1"
		, "O_CASE2"
		, "O_GUARANTEE_PERIOD_MM"
		, "O_REG_CONTENTS"				//접수내역
	};
	
	/*-
	 * Method ID    : WMSOP100
	 * Method 설명      : 엑셀주문입력
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSOP103.action")
	public ModelAndView WMSOP103(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP103");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 공지사항 목록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP103/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : WMSOP100E2
	 * Method 설명      : 배송주문 엑셀등록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP103E2.action")
	public ModelAndView WMSOP103E2(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP103E2");
	}
	
	
	/*-
	 * Method ID    : saveE2
	 * Method 설명      : 엑셀업로드
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("WMSOP103E2/save.action")
	public ModelAndView saveE2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP103/uploadExcelInfo.action")
	public ModelAndView uploadExcelInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP103 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP103, 0, startRow, 10000, 0);
			m = service.saveExcelInfo103(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP103/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
