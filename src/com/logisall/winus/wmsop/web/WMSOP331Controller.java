package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP331Service;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.logisall.ws.interfaces.common.exception.InterfaceException;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP331Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP331Service")
	private WMSOP331Service service;

	/*-
	 * Method ID    : wmsop331
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP331.action")
	public ModelAndView wmsop331(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP331", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP336.action")
	public ModelAndView wmsop336(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP336", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP3361.action")
	public ModelAndView wmsop3361(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP3361", service.selectItemGrp(model));
	}
	
	/*-
	 * Method ID    : listByCustSummary
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP331/listByCustSummary.action")
	public ModelAndView listByCustSummary(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustSummary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	@RequestMapping("/WMSOP331/listByCustSummaryPoi.action")
	public ModelAndView listByCustSummaryPoi(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustSummaryPoi(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustDetail
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP331/listByCustDetail.action")
	public ModelAndView listByCustDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: rogenCrossDomainHttp
	 * Method 설명	: 
	 * 작성자			: 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP331/rogenCrossDomainHttp.action")
	public ModelAndView crossDomainHttp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.rogenCrossDomainHttp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: saeroCompleteCrossDomainHttp
	 * Method 설명	: 
	 * 작성자			: 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP331/saeroCompleteCrossDomainHttp.action")
	public ModelAndView saeroCompleteCrossDomainHttp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.saeroCompleteCrossDomainHttp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: invcNoPrintDlvOrder
	 * Method 설명	: 
	 * 작성자			: 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP331/invcNoPrintDlvOrder.action")
	public ModelAndView invcNoPrintDlvOrder(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP331E2");
	}
	
	/**
     * Method ID	: billingListDetail
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOP331/billingListDetail.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.billingListDetail(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
	 * Method ID	: popup
	 * Method 설명 	: 
	 * 작성자 			: 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP331/popup.action")
	public ModelAndView popup(Map<String, Object> model) throws Exception  {
		return new ModelAndView("winus/wmsop/WMSOP331Q1");
	}
	
	/*-
	 * Method ID	: popupList
	 * Method 설명	: 그리드 조회
	 * 작성자 			: 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP331/popupList.action")
	public ModelAndView popupList(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.popupList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: popupSave
	 * Method 설명	: 
	 * 작성자			: 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP331/popupSave.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.popupSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
