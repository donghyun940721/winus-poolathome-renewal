package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ExcelWriter;
import com.m2m.jdfw5x.egov.database.GenericResultSet;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsop.service.WMSOP100Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP100Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP100Service")
	private WMSOP100Service service;

	static final String[] COLUMN_NAME_WMSOP100 = {
		  "O_SEQ"
		, "O_DLV_COM_ID"
		, "O_SALES_COMPANY_ID"
		, "O_ORG_ORD_ID"
		, "O_BUY_CUST_NM"
		, "O_BUY_PHONE_1"
		, "O_BUY_PHONE_2"
		, "O_CUSTOMER_NM"
		, "O_PHONE_1"
		, "O_PHONE_2"
		, "O_ZIP"
		, "O_ADDR"
		, "O_PRODUCT"
		, "O_DLV_ORD_TYPE"
		, "O_QTY"
		, "O_DLV_SET_DT"
		, "O_ETC1"
		, "O_MEMO"
		, "O_BUYED_DT"
		, "O_DLV_DT"
		, "O_DLV_ORD_STAT"
		, "O_PRODUCT_NM"
		, "O_SERIAL_NO"
		, "O_CASE1"
		, "O_CASE2"
		, "O_GUARANTEE_PERIOD_MM"
	};
	
	static final String[] COLUMN_NAME_WMSOP105 = {
		  "O_SEQ"
		, "O_DLV_COM_ID"
		, "O_SALES_COMPANY_ID"
		, "O_ORG_ORD_ID"
		, "O_CUSTOMER_NM"
		, "O_PHONE_1"
		, "O_PHONE_2"
		, "O_ZIP"
		, "O_ADDR"
		, "O_PRODUCT"
		, "O_QTY"
		, "O_PRODUCT_D0"
		, "O_QTY_D0"
		, "O_PRODUCT_D1"
		, "O_QTY_D1"
		, "O_PRODUCT_D2"
		, "O_QTY_D2"
		, "O_PRODUCT_D3"
		, "O_QTY_D3"
		, "O_PRODUCT_D4"
		, "O_QTY_D4"
		, "O_PRODUCT_D5"
		, "O_QTY_D5"
		, "O_PRODUCT_D6"
		, "O_QTY_D6"
		, "O_PRODUCT_D7"
		, "O_QTY_D7"
		, "O_DLV_ORD_TYPE"
		, "O_DLV_SET_DT"
		, "O_ETC1"
		, "O_MEMO"
		, "O_BUYED_DT"
		, "O_CASE1"
		, "O_BUY_CUST_NM"
		, "O_BUY_PHONE_1"
		, "O_BUY_PHONE_2"
	};
	
	static final String[] COLUMN_NAME_WMSOP105ZOIT = {
		  "O_SEQ"
		, "O_DLV_COM_ID"
		, "O_SALES_COMPANY_ID"
		, "O_CUSTOMER_NM"
		, "O_PHONE_1"
		, "O_PHONE_2"
		, "O_ZIP"
		, "O_ADDR"
		, "O_PRODUCT"
		, "O_QTY"
		, "O_PRODUCT_D0"
		, "O_QTY_D0"
		, "O_PRODUCT_D1"
		, "O_QTY_D1"
		, "O_PRODUCT_D2"
		, "O_QTY_D2"
		, "O_PRODUCT_D3"
		, "O_QTY_D3"
		, "O_PRODUCT_D4"
		, "O_QTY_D4"
		, "O_PRODUCT_D5"
		, "O_QTY_D5"
		, "O_PRODUCT_D6"
		, "O_QTY_D6"
		, "O_PRODUCT_D7"
		, "O_QTY_D7"
		, "O_DLV_ORD_TYPE"
		, "O_ETC1"
		, "O_MEMO"
		, "O_BUYED_DT"
		
		, "O_BUY_CUST_NM"
		, "O_CUST_ID_NO"
		, "O_BIZ_NO"
		, "O_REAL_BUYED_DT"
		, "O_REAL_PAYED_DT"
		, "O_PAY_TYPE"
		, "O_DLV_SET_DT"
		, "O_BATCH_NO"
		, "O_ORG_ORD_ID"
		, "O_DEDUCT_NO"
	};
	
	static final String[] COLUMN_NAME_WMSOP105JOA = {
		  "O_SEQ"
		, "O_SALES_COMPANY_ID"
		, "O_ORG_ORD_ID"
		, "O_CUSTOMER_NM"
		, "O_PHONE_1"
		, "O_PHONE_2"
		, "O_ZIP"
		, "O_ADDR"
		, "O_PRODUCT"
		, "O_QTY"
		, "O_PRODUCT_D0"
		, "O_QTY_D0"
		, "O_PRODUCT_D1"
		, "O_QTY_D1"
		, "O_PRODUCT_D2"
		, "O_QTY_D2"
		, "O_PRODUCT_D3"
		, "O_QTY_D3"
		, "O_PRODUCT_D4"
		, "O_QTY_D4"
		, "O_PRODUCT_D5"
		, "O_QTY_D5"
		, "O_PRODUCT_D6"
		, "O_QTY_D6"
		, "O_PRODUCT_D7"
		, "O_QTY_D7"
		, "O_DLV_ORD_TYPE"
		, "O_DLV_SET_DT"
		, "O_ETC1"
		, "O_MEMO"
		, "O_BUYED_DT"
		, "O_BUY_CUST_NM"
		, "O_BUY_PHONE_1"
		, "O_BUY_PHONE_2"
	};
	static final String[] COLUMN_NAME_WMSOP105CT = {
        "O_SEQ"
      , "O_DLV_COM_ID"
      , "O_SALES_COMPANY_ID"
      , "O_ORG_ORD_ID"
	  , "O_FIRST_INS_DT"
      , "O_CUSTOMER_NM"
      , "O_PHONE_1"
      , "O_PHONE_2"
      , "O_ZIP"
      , "O_ADDR"
	  , "O_PRODUCT"
	  , "O_QTY"
      , "O_BUYED_DT"
      , "O_BUY_CUST_NM"
      , "O_BUY_PHONE_1"
      , "O_BUY_PHONE_2"
  };
	static final String[] COLUMN_NAME_WMSOP107 = {
		"O_SEQ"
		, "O_DLV_COM_ID"
		, "O_SALES_COMPANY_ID"
		, "O_ORG_ORD_ID"
		, "O_CUSTOMER_NM"
		, "O_PHONE_1"
		, "O_PHONE_2"
		, "O_ZIP"
		, "O_ADDR"
		, "O_PRODUCT"
		, "O_QTY"
		, "O_DLV_SET_DT"
		, "O_FIRST_INS_DT"
		, "O_PAY_REQ_YN"
		, "O_REQ_COST"
		, "O_ETC1"
		, "O_MEMO"
		, "O_BUYED_DT"
		, "O_AS_COST_MEMO"
		, "O_AS_ETC1"
		, "O_AS_ETC2"
	};
	static final String[] COLUMN_NAME_WMSOP105CS = {
        "O_SEQ"
      , "O_DLV_COM_ID"
      , "O_SALES_COMPANY_ID"
      , "O_ORG_ORD_ID"
	  , "O_FIRST_INS_DT"
      , "O_CUSTOMER_NM"
      , "O_PHONE_1"
      , "O_PHONE_2"
      , "O_ZIP"
      , "O_ADDR"
	  , "O_PRODUCT"
	  , "O_QTY"
      , "O_BUYED_DT"
      , "O_BUY_CUST_NM"
      , "O_BUY_PHONE_1"
      , "O_BUY_PHONE_2"
      , "O_CS_REG_CONTENTS"
      , "O_CS_MEMO"
  };
	
	/*-
	 * Method ID    : WMSOP100
	 * Method 설명      : 엑셀주문입력
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSOP100.action")
	public ModelAndView WMSOP100(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100");
	}
	
	
	/*-
	 * Method ID    : WMSOP108
	 * Method 설명      : 통합주문등록(조아)
	 * 작성자                 : kijun11
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSOP108.action")
	public ModelAndView WMSOP108(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP108");
	}


	/*-
	 * Method ID    : WMSOP101
	 * Method 설명      : 엑셀주문입력 주문수정
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSOP101.action")
	public ModelAndView WMSOP101(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP101");
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 공지사항 목록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : WMSOP100E2
	 * Method 설명      : 배송주문 엑셀등록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E2.action")
	public ModelAndView WMSOP100E2(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E2");
	}
	
	
	/*-
	 * Method ID    : WMSOP100E3
	 * Method 설명      : 배송주문 일반등록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E3.action")
	public ModelAndView WMSOP100E3(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E3");
	}
	
	/*-
	 * Method ID    : WMSOP100E4
	 * Method 설명      : 배송주문(H/D)일반등록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E4.action")
	public ModelAndView WMSOP100E4(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E4");
	}
	
	/*-
	 * Method ID    : WMSOP100E5
	 * Method 설명      : 배송주문 엑셀등록 H/D (22.04.28)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E5.action")
	public ModelAndView WMSOP100E5(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E5");
	}
	
	/*-
	 * Method ID    : WMSOP100E6
	 * Method 설명      : 배송주문 일반등록H/D (22.04.28)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E6.action")
	public ModelAndView WMSOP100E6(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E6");
	}
	
	/*-
	 * Method ID    : WMSOP100E5ZOIT
	 * Method 설명      : 배송주문 엑셀등록 H/D ZOIT
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E5ZOIT.action")
	public ModelAndView WMSOP100E5ZOIT(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E5ZOIT");
	}
	
	/*-
	 * Method ID    : WMSOP100E6ZOIT
	 * Method 설명      : 배송주문 일반등록H/D ZOIT
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E6ZOIT.action")
	public ModelAndView WMSOP100E6ZOIT(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E6ZOIT");
	}
	
	/*-
	 * Method ID    : WMSOP100E5JOA
	 * Method 설명      : 배송주문 엑셀등록 H/D JOA
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E5JOA.action")
	public ModelAndView WMSOP100E5JOA(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E5JOA");
	}
	
	/*-
	 * Method ID    : WMSOP100E6JOA
	 * Method 설명      : 배송주문 일반등록H/D JOA
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E6JOA.action")
	public ModelAndView WMSOP100E6JOA(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E6JOA");
	}
	
	/*-
	 * Method ID    : WMSOP100E5CT
	 * Method 설명      : 배송주문 고객정보등록
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E5CT.action")
	public ModelAndView WMSOP100E5CT(Map<String, Object> model) {
	    return new ModelAndView("winus/WMSOP/WMSOP100E5CT");
	}
	
	/*-
	 * Method ID    : WMSOP100E6CT
	 * Method 설명      : 배송주문 고객정보등록
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E6CT.action")
	public ModelAndView WMSOP100E6CT(Map<String, Object> model) {
	    return new ModelAndView("winus/WMSOP/WMSOP100E6CT");
	}
	
	/*-
	 * Method ID    : WMSOP100E5CS
	 * Method 설명      : 배송주문 CS등록
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E5CS.action")
	public ModelAndView WMSOP100E5CS(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E5CS");
	}
	
	/*-
	 * Method ID    : WMSOP100E6CS
	 * Method 설명      : 배송주문 CS등록
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E6CS.action")
	public ModelAndView WMSOP100E6CS(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E6CS");
	}
	
	/*-
	 * Method ID    : WMSOP100E7
	 * Method 설명      : AS주문 엑셀등록
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E7.action")
	public ModelAndView WMSOP100E7(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E7");
	}

	/*-
	 * Method ID    : WMSOP100E8
	 * Method 설명      : AS주문 일반등록
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP100E8.action")
	public ModelAndView WMSOP100E8(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP100E8");
	}
	
	
	/*-
	 * Method ID    : saveE2
	 * Method 설명      : 엑셀업로드
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("WMSOP100E2/save.action")
	public ModelAndView saveE2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveE5
	 * Method 설명      : 엑셀업로드 HD
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("WMSOP100E5/save.action")
	public ModelAndView saveE5(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE5(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : saveE7
	 * Method 설명      : 엑셀업로드 AS
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("WMSOP100E7/save.action")
	public ModelAndView saveE7(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE7(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUpload_HD
	 * Method 설명  : Excel 파일 읽기 HD 22.04.28
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP100/uploadExcelInfo_HD.action")
	public ModelAndView uploadExcelInfo_HD(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP105 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			if("0000003780".equals(model.get(ConstantIF.SS_SVC_NO)) || "0000003820".equals(model.get(ConstantIF.SS_SVC_NO))){
				List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP105ZOIT, 0, startRow, 10000, 0);
				m = service.saveExcelInfo_HD(model, list);
			}else{
				List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP105, 0, startRow, 10000, 0);
				m = service.saveExcelInfo_HD(model, list);
			}
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : uploadExcelInfo_CT
	 * Method 설명  : Excel 파일 읽기 CT 22.11.01
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP100/uploadExcelInfo_CT.action")
	public ModelAndView uploadExcelInfo_CT(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m = null;
	    try {
	        request.setCharacterEncoding("utf-8");
	        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	        MultipartFile file = multipartRequest.getFile("txtFile");
	        String fileName = file.getOriginalFilename();
	        String filePaths = ConstantIF.TEMP_PATH;
	        
	        if (!FileHelper.existDirectory(filePaths)) {
	            FileHelper.createDirectorys(filePaths);
	        }
	        File destinationDir = new File(filePaths);
	        File destination = File.createTempFile("excelTemp", fileName, destinationDir);
	        FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
	        
	        int startRow = Integer.parseInt((String) model.get("startRow"));
	        
	        //COLUMN_NAME_WMSOP105 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
            List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP105CT, 0, startRow, 10000, 0);
            m = service.saveExcelInfo_CT(model, list);
	        
	        destination.deleteOnExit();
	        mav.addAllObjects(m);
	        
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to upload Excel info :", e);
	        }
	        m = new HashMap<String, Object>();
	        m.put("MSG", MessageResolver.getMessage("save.error"));
	        m.put("MSG_ORA", e.getMessage());
	        m.put("errCnt", "1");
	        mav.addAllObjects(m);
	    }
	    return mav;
	}
	
	/*-
	 * Method ID  : uploadExcelInfo_CS
	 * Method 설명  : Excel 파일 읽기 CT 23.01.08
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP100/uploadExcelInfo_CS.action")
	public ModelAndView uploadExcelInfo_CS(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP105 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP105CS, 0, startRow, 10000, 0);
			m = service.saveExcelInfo_CS(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUpload_AS
	 * Method 설명  : Excel 파일 읽기 HD 22.04.28
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP100/uploadExcelInfo_AS.action")
	public ModelAndView uploadExcelInfo_AS(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP107 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP107, 0, startRow, 10000, 0);
			m = service.saveExcelInfo_AS(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP100/uploadExcelInfo.action")
	public ModelAndView uploadExcelInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP100 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP100, 0, startRow, 10000, 0);
			m = service.saveExcelInfo(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : delete
	 * Method 설명      : 
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : excelDownCustom
	 * Method 설명   : Type별 주문정보 엑셀다운로드
	 */
	@RequestMapping("/WMSOP100/excelDownCustom.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelDownCustom(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownCustom(response, grs, (String)model.get("uploadTitle"), (String)model.get("custCd"));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDownCustom(HttpServletResponse response, GenericResultSet grs, String uploadTitle, String custCd) {
        try{
            if(custCd.equals("GISAN")){
            	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
                String[][] headerEx = {
                                       {MessageResolver.getMessage("발주일자")		, "0", "0", "0", "0", "200"},
                                       {MessageResolver.getMessage("구분")		, "1", "1", "0", "0", "200"},
                                       {MessageResolver.getMessage("인수자")		, "2", "2", "0", "0", "200"},
                                       {MessageResolver.getMessage("제품명")		, "3", "3", "0", "0", "200"},
                                       {MessageResolver.getMessage("색상")		, "4", "4", "0", "0", "200"},
                                       
                                       {MessageResolver.getMessage("수량")		, "5", "5", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송비-선불")	, "6", "6", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송비-착불")	, "7", "7", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송일")		, "8", "8", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송기사")		, "9", "9", "0", "0", "200"},
                                       
                                       {MessageResolver.getMessage("주소")		, "10", "10", "0", "0", "200"},
                                       {MessageResolver.getMessage("전화번호")		, "11", "11", "0", "0", "200"},
                                       {MessageResolver.getMessage("휴대번호")		, "12", "12", "0", "0", "200"},
                                       {MessageResolver.getMessage("기타사항")		, "13", "13", "0", "0", "200"},
                                       {MessageResolver.getMessage("배송예정일")	, "14", "14", "0", "0", "200"},
                                       
                                       {MessageResolver.getMessage("쇼핑몰명")		, "15", "15", "0", "0", "200"},
                                       {MessageResolver.getMessage("구성품")		, "16", "16", "0", "0", "200"},
                                       {MessageResolver.getMessage("주문번호")		, "17", "17", "0", "0", "200"},
                                       {MessageResolver.getMessage("쇼핑몰발주번호")	, "18", "18", "0", "0", "200"},
                                       {MessageResolver.getMessage("통행료")		, "19", "19", "0", "0", "200"},
                                       
                                       {MessageResolver.getMessage("외곽추가요금")	, "20", "20", "0", "0", "200"}
                                      };
                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
                String[][] valueName = {
                                        {"BUYED_DT"           	, "S"},
                                        {"WORK_STAT"           	, "S"},
                                        {"SALES_CUST_NM"       	, "S"},
                                        {"PRODUCT"          	, "S"},
                                        {""          			, "S"},
                                        
                                        {"QTY"          		, "S"},
                                        {""          			, "S"},
                                        {""          			, "S"},
                                        {"DLV_DT"          		, "S"},
                                        {""          			, "S"},
                                        
                                        {"ADDR"          		, "S"},
                                        {"PHONE_1"          	, "S"},
                                        {"PHONE_2"          	, "S"},
                                        {"ETC1"          		, "S"},
                                        {"DLV_REQ_DT"          	, "S"},
                                        
                                        {"SALES_COMPANY_NM"     , "S"},
                                        {""          			, "S"},
                                        {"ORG_ORD_ID"          	, "S"},
                                        {""          			, "S"},
                                        {""          			, "S"},
                                        
                                        {""          			, "S"}
                                       }; 
                
    			// 파일명
    			String fileName = MessageResolver.getText(uploadTitle);
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";

    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
            }else if(custCd.equals("HOWSER")){
            	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
                String[][] headerEx = {
                						{MessageResolver.getMessage("자사주문번호")		, "0", "0", "0", "0", "200"},               
                						{MessageResolver.getMessage("수취인명")		, "1", "1", "0", "0", "200"},
                                        {MessageResolver.getMessage("수취인연락처1")		, "2", "2", "0", "0", "200"},
                                        {MessageResolver.getMessage("수취인연락처2")		, "3", "3", "0", "0", "200"},
                                        {MessageResolver.getMessage("수취인인주소")		, "4", "4", "0", "0", "200"},
                		
                                        {MessageResolver.getMessage("판매처")			, "5", "5", "0", "0", "200"}, 
                                        {MessageResolver.getMessage("의뢰타입")		, "6", "6", "0", "0", "200"},
                                        {MessageResolver.getMessage("메시지")			, "7", "7", "0", "0", "200"},
                                        {MessageResolver.getMessage("현장수금액")		, "8", "8", "0", "0", "200"},
                                        {MessageResolver.getMessage("입고예정일")		, "9", "9", "0", "0", "200"},
                                        
                                        {MessageResolver.getMessage("고객계약확인서")	, "10", "10", "0", "0", "200"},
                                        {MessageResolver.getMessage("상코드")			, "11", "11", "0", "0", "200"},
                                        {MessageResolver.getMessage("수량")			, "12", "12", "0", "0", "200"}
                                      };
                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
                String[][] valueName = {
                						{"SALES_CUST_ID"       	, "S"},
                						{"SALES_CUST_NM"        , "S"},
                                        {"PHONE_1"          	, "S"},
                                        {"PHONE_2"          	, "S"},
                                        {"ADDR"          		, "S"},
                                        
                                        {"SALES_COMPANY_NM"     , "S"},
                                        {"WORK_STAT"          	, "S"},
                                        {"ETC1"     			, "S"},
                                        {""          			, "S"},
                                        {""          			, "S"},
                                        
                                        {""          			, "S"},
                                        {"PRODUCT"          	, "S"},
                                        {"QTY"          		, "S"}
                                       }; 
                
    			// 파일명
    			String fileName = MessageResolver.getText(uploadTitle);
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";
    			
    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            }
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : keyInSaveInfo
	 * Method 설명      : 배송주문 일반저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/keyInSaveInfo.action")
	public ModelAndView keyInSaveInfo(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.keyInSaveInfo(model, COLUMN_NAME_WMSOP100);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : keyInSaveInfo_HD
	 * Method 설명      : 배송주문 일반저장 HD
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/keyInSaveInfo_HD.action")
	public ModelAndView keyInSaveInfo_HD(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			if("0000003780".equals(model.get(ConstantIF.SS_SVC_NO)) || "0000003820".equals(model.get(ConstantIF.SS_SVC_NO))){
				m = service.keyInSaveInfo_HD(model, COLUMN_NAME_WMSOP105ZOIT);
			}else{
				m = service.keyInSaveInfo_HD(model, COLUMN_NAME_WMSOP105);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : keyInSaveInfo_CT
	 * Method 설명      : 고객주문업로드
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/keyInSaveInfo_CT.action")
	public ModelAndView keyInSaveInfo_CT(Map<String, Object> model) throws Exception {
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m = new HashMap<String, Object>();
	    try {
            m = service.keyInSaveInfo_CT(model, COLUMN_NAME_WMSOP105CT);
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to save :", e);
	        }
	        m.put("MSG", MessageResolver.getMessage("save.error"));
	        m.put("MSG_ORA", e.getMessage());
	        m.put("errCnt", "1");
	    }
	    mav.addAllObjects(m);
	    return mav;
	}
	
	/*-
	 * Method ID    : keyInSaveInfo_CS
	 * Method 설명      : CS주문업로드
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/keyInSaveInfo_CS.action")
	public ModelAndView keyInSaveInfo_CS(Map<String, Object> model) throws Exception {
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m = new HashMap<String, Object>();
	    try {
            m = service.keyInSaveInfo_CS(model, COLUMN_NAME_WMSOP105CS);
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to save :", e);
	        }
	        m.put("MSG", MessageResolver.getMessage("save.error"));
	        m.put("MSG_ORA", e.getMessage());
	        m.put("errCnt", "1");
	    }
	    mav.addAllObjects(m);
	    return mav;
	}
	
	/*-
	 * Method ID    : keyInSaveInfo_AS
	 * Method 설명      : AS주문 일반저장 
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/keyInSaveInfo_AS.action")
	public ModelAndView keyInSaveInfo_AS(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.keyInSaveInfo_AS(model, COLUMN_NAME_WMSOP107);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSOP102
	 * Method 설명      : 배송주문등록(H/D)
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSOP102.action")
	public ModelAndView WMSOP102(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP102");
	}
	
	@RequestMapping("/WMSOP102E2.action")
	public ModelAndView WMSOP102E2(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP102E2");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP102/uploadExcelInfoMulti.action")
	public ModelAndView uploadExcelInfoMulti(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP100 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP100, 0, startRow, 10000, 0);
			m = service.saveExcelInfoMulti(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}


	
	/*-
	 * Method ID    : keyInSaveInfo
	 * Method 설명      : 배송주문 일반저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/keyInSaveInfoMulti.action")
	public ModelAndView keyInSaveInfoMulti(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.keyInSaveInfoMulti(model, COLUMN_NAME_WMSOP100);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID	: checkInsertPossible
	 * Method 설명	: 주문등록 시간체크
	 * 작성자			: 
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP100/checkInsertPossible.action")
	public ModelAndView checkInsertPossible(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.checkInsertPossible(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	

	/*-
	 * Method ID    : spUploadKeyIn_DLV
	 * Method 설명      : 배송주문일반업로드  Package
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/spUploadKeyIn_DLV.action")
	public ModelAndView spUploadKeyIn_DLV(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			if("0000003700".equals(model.get(ConstantIF.SS_SVC_NO))){
				m = service.spInsertKeyIn_DLV(model, COLUMN_NAME_WMSOP105JOA);
			}else{
				m = service.spInsertKeyIn_DLV(model, COLUMN_NAME_WMSOP105);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : spUploadExcel_DLV
	 * Method 설명  : 배송주문 엑셀 업로드 Package 
	 * 작성자             : sing09
	 * @param model
	 */
	@RequestMapping("/WMSOP100/spUploadExcel_DLV.action")
	public ModelAndView spUploadExcel_DLV(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP105 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			if("0000003700".equals(model.get(ConstantIF.SS_SVC_NO))){
				List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP105JOA, 0, startRow, 10000, 0);
				m = service.spInsertExcel_DLV(model, list, COLUMN_NAME_WMSOP105JOA);
			}else{
				List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP105, 0, startRow, 10000, 0);
				m = service.spInsertExcel_DLV(model, list, COLUMN_NAME_WMSOP105);
			}
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : spUploadKeyIn_CT
	 * Method 설명      : 고객주문일반업로드  Package
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/spUploadKeyIn_CT.action")
	public ModelAndView spUploadKeyIn_CT(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.spInsertKeyIn_CT(model, COLUMN_NAME_WMSOP105CT);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : spUploadExcel_CT
	 * Method 설명  : 고객주문 엑셀 업로드 Package 
	 * 작성자             : sing09
	 * @param model
	 */
	@RequestMapping("/WMSOP100/spUploadExcel_CT.action")
	public ModelAndView spUploadExcel_CT(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
	    ModelAndView mav = new ModelAndView("jsonView");
	    Map<String, Object> m = null;
	    try {
	        request.setCharacterEncoding("utf-8");
	        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
	        MultipartFile file = multipartRequest.getFile("txtFile");
	        String fileName = file.getOriginalFilename();
	        String filePaths = ConstantIF.TEMP_PATH;
	        
	        if (!FileHelper.existDirectory(filePaths)) {
	            FileHelper.createDirectorys(filePaths);
	        }
	        File destinationDir = new File(filePaths);
	        File destination = File.createTempFile("excelTemp", fileName, destinationDir);
	        FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
	        
	        int startRow = Integer.parseInt((String) model.get("startRow"));
	        
	        //COLUMN_NAME_WMSOP105 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
            List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP105CT, 0, startRow, 10000, 0);
            m = service.spInsertExcel_CT(model, list, COLUMN_NAME_WMSOP105CT);
	        
	        destination.deleteOnExit();
	        mav.addAllObjects(m);
	        
	    } catch (Exception e) {
	        m = new HashMap<String, Object>();
	        m.put("MSG", MessageResolver.getMessage("save.error"));
	        m.put("MSG_ORA", e.getMessage());
	        m.put("errCnt", "1");
	        mav.addAllObjects(m);
	    }
	    return mav;
	}
	
	/*-
	 * Method ID    : spUploadKeyIn_CS
	 * Method 설명      : CS주문일반업로드  Package
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/spUploadKeyIn_CS.action")
	public ModelAndView spUploadKeyIn_CS(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.spInsertKeyIn_CS(model, COLUMN_NAME_WMSOP105CS);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : spUploadExcel_CS
	 * Method 설명  : 고객주문 엑셀 업로드 Package 
	 * 작성자             : sing09
	 * @param model
	 */
	@RequestMapping("/WMSOP100/spUploadExcel_CS.action")
	public ModelAndView spUploadExcel_CS(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP105 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP105CS, 0, startRow, 10000, 0);
			m = service.spInsertExcel_CS(model, list, COLUMN_NAME_WMSOP105CS);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}

	/*-
	 * Method ID    : spUploadKeyIn_AS
	 * Method 설명      : AS주문일반업로드  Package
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP100/spUploadKeyIn_AS.action")
	public ModelAndView spUploadKeyIn_AS(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.spInsertKeyIn_AS(model, COLUMN_NAME_WMSOP107);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : spUploadExcel_AS
	 * Method 설명  : AS주문 엑셀 업로드 Package 
	 * 작성자             : sing09
	 * @param model
	 */
	@RequestMapping("/WMSOP100/spUploadExcel_AS.action")
	public ModelAndView spUploadExcel_AS(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSOP107 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP107, 0, startRow, 10000, 0);
			m = service.spInsertExcel_AS(model, list, COLUMN_NAME_WMSOP107);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
}
