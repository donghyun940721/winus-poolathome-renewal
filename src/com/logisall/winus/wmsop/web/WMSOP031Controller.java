package com.logisall.winus.wmsop.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsop.service.WMSOP031Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP031Controller {
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSOP031Service")
    private WMSOP031Service service;
    
    /**
     * Method ID    : wmsop030
     * Method 설명      : 출고관리 화면
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSOP031.action")
    public ModelAndView wmsop030(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsop/WMSOP031" , service.selectPoolGrp(model)); 
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 물류용기출고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOP031/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
        }
        return mav;
    }
//    
//    /**
//    * Method ID    : sum
//    * Method 설명      : 출고관리 합계
//    * 작성자                 : chSong
//    * @param   model
//    * @return  
//    * @throws Exception 
//    */
//   @RequestMapping("/WMSOP030/sum.action")
//   public ModelAndView sum(Map<String, Object> model) throws Exception {
//       ModelAndView mav = null;
//       
//       try {
//           mav = new ModelAndView("jsonView", service.sum(model));
//       } catch (Exception e) {
//           e.printStackTrace();
//       }
//       return mav;
//   }
    
//   /**
//    * Method ID    : deleteOrder
//    * Method 설명      : 주문삭제
//    * 작성자                 : chSong
//    * @param   model
//    * @return  
//    */
//   @RequestMapping("/WMSOP030/deleteOrder.action")
//   public ModelAndView deleteOrder(Map<String, Object> model){
//       ModelAndView mav = new ModelAndView("jsonView");
//       Map<String, Object> m  = new HashMap<String, Object>();
//       try{
//           m = service.deleteOrder(model);
//       }catch(Exception e){
//           e.printStackTrace();
//           m.put("MSG", MessageResolver.getMessage("save.error"));       
//       }
//       mav.addAllObjects(m);
//       return mav;
//   } 
//   
//   /**
//    * Method ID    : listExcel
//    * Method 설명      : 엑셀다운로드
//    * 작성자                 : chsong
//    * @param model
//    * @return
//    */
//   @RequestMapping("/WMSOP030/excel.action")
//   public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
//       Map<String, Object> map = new HashMap<String, Object>();
//       try {
//           map = service.listExcel(model);
//           GenericResultSet grs = (GenericResultSet)map.get("LIST");
//           if(grs.getTotCnt() > 0){
//               this.doExcelDown(response, grs);
//           }
//       }catch (Exception e) {
//           e.printStackTrace();
//       }
//   }
//   protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
//       try{
//           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
//           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
//           String[][] headerEx = {
//                                   {MessageResolver.getText("작업상태")   , "0", "0", "0", "0", "100"}
//                                  ,{MessageResolver.getText("주문번호")   , "1", "1", "0", "0", "100"}
//                                  ,{MessageResolver.getText("주문SEQ")   , "2", "2", "0", "0", "100"}
//                                  ,{MessageResolver.getText("화주")      , "3", "3", "0", "0", "100"}
//                                  ,{MessageResolver.getText("배송처")     , "4", "4", "0", "0", "100"}
//                                  
//                                  ,{MessageResolver.getText("출고예정일")  , "5", "5", "0", "0", "100"}
//                                  ,{MessageResolver.getText("출고일자")   , "6", "6", "0", "0", "100"}
//                                  ,{MessageResolver.getText("상품코드")   , "7", "7", "0", "0", "100"}
//                                  ,{MessageResolver.getText("상품명")     , "8", "8", "0", "0", "100"}
//                                  ,{MessageResolver.getText("주문량")     , "9", "9", "0", "0", "100"}
//                                  
//                                  ,{"UOM"                               , "10", "10", "0", "0", "100"}
//                                  ,{MessageResolver.getText("주문주량")     , "11", "11", "0", "0", "100"}
//                                  ,{MessageResolver.getText("출고량")   , "12", "12", "0", "0", "100"}
//                                  ,{"UOM"                               , "13", "13", "0", "0", "100"}
//                                  ,{MessageResolver.getText("출고중량")   , "14", "14", "0", "0", "100"}
//                                  
//                                  ,{MessageResolver.getText("송장번호")         , "15", "15", "0", "0", "100"}
//                                  ,{MessageResolver.getText("현재고")          , "16", "16", "0", "0", "100"}
//                                  ,{MessageResolver.getText("불량재고")         , "17", "17", "0", "0", "100"}
//                                  ,{MessageResolver.getText("상품유효기간")       , "18", "18", "0", "0", "100"}
//                                  ,{MessageResolver.getText("상품유효기간만료일")   , "19", "19", "0", "0", "100"}
//                                  
//                                  ,{MessageResolver.getText("소비기한만료일"), "20", "20", "0", "0", "100"}
//                                  ,{MessageResolver.getText("비고1")      , "21", "21", "0", "0", "100"}
//                                  ,{MessageResolver.getText("비고2")      , "22", "22", "0", "0", "100"}
//                                  ,{MessageResolver.getText("PLT수량")    , "23", "23", "0", "0", "100"}
//                                  ,{MessageResolver.getText("BOX수량")    , "24", "24", "0", "0", "100"}
//                                  
//                                  ,{MessageResolver.getText("창고")    , "25", "25", "0", "0", "100"}
//                                  ,{MessageResolver.getText("주문상세")    , "26", "26", "0", "0", "100"}
//                                  ,{MessageResolver.getText("피킹WORK")    , "27", "27", "0", "0", "100"}
//                                  ,{MessageResolver.getText("출고WORK")    , "28", "28", "0", "0", "100"}
//                                  ,{MessageResolver.getText("출고SEQ")    , "29", "29", "0", "0", "100"}
//                                  
//                                  ,{"ORD_ID"         , "30", "30", "0", "0", "100"}
//                                 };
//           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
//           String[][] valueName = {
//                                   {"WORK_STAT_EXCEL"  , "S"},
//                                   {"VIEW_ORD_ID"      , "S"},
//                                   {"ORD_SEQ"          , "S"},
//                                   {"CUST_NM"           , "S"},
//                                   {"TRANS_CUST_NM"       , "S"},
//                                   
//                                   {"OUT_REQ_DT"        , "S"},
//                                   {"OUT_DT"            , "S"},
//                                   {"RITEM_CD"         , "S"},
//                                   {"RITEM_NM"         , "S"},
//                                   {"OUT_ORD_QTY"       , "S"},
//                                   
//                                   {"OUT_ORD_UOM_NM"    , "S"},
//                                   {"IN_ORD_WEIGHT"      , "S"},
//                                   {"REAL_OUT_QTY"   , "S"},
//                                   {"OUT_UOM_NM"    , "S"},
//                                   {"REAL_OUT_WEIGHT"     , "S"},
//                                   
//                                   {"INV_NO"     , "S"},
//                                   {"STOCK_QTY", "S"},
//                                   {"BAD_QTY"         , "S"},
//                                   {"TIME_DATE"         , "S"},
//                                   {"TIME_DATE_END"           , "S"},
//                                   
//                                   {"TIME_USE_END"      , "S"},
//                                   {"ORD_DESC"    , "S"},
//                                   {"ETC2"         , "S"},
//                                   {"REAL_PLT_QTY"            , "S"},
//                                   {"REAL_BOX_QTY"      , "S"},
//                                   
//                                   {"OUT_WH_NM"          , "S"},
//                                   {"ORD_SUBTYPE"         , "S"},
//                                   {"PICKING_WORK_ID"         , "S"},
//                                   
//                                   {"OUT_WORK_ID"         , "S"},
//                                   {"OUT_WORK_SEQ"         , "S"},
//                                   {"ORD_ID"         , "S"}
//                                  }; 
//           
//           //파일명
//           String FileName = MessageResolver.getText("출고현황");
//           //시트명
//           String sheetName = "Sheet1";
//           //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
//           String marCk = "N";
//           //ComUtil코드
//           String Etc = "";
//           
//           ExcelWriter wr = new ExcelWriter();
//           wr.ExDown(ConstantIF.attachPath, grs, headerEx, valueName, FileName, sheetName, marCk, Etc, response);
//       } catch(Exception e) {
//           e.getMessage();
//       }
//   }
//   
//   
//   /**
//    * Method ID    : saveAlloc
//    * Method 설명      : 할당 처리, 취소 (할당처리, 할당취소 분할해서 처리햇던거 합침, 프로시져만 다른거타지 보내는것같아서)
//    * 작성자                 : chSong
//    * @param   model
//    * @return  
//    */
//   @RequestMapping("/WMSOP030/saveAlloc.action")
//   public ModelAndView saveAlloc(Map<String, Object> model){
//       ModelAndView mav = new ModelAndView("jsonView");
//       Map<String, Object> m  = new HashMap<String, Object>();
//       try{
//           m = service.saveAlloc(model);
//       }catch(Exception e){
//           e.printStackTrace();
//           m.put("MSG", MessageResolver.getMessage("save.error"));       
//       }
//       mav.addAllObjects(m);
//       return mav;
//   }      
//    
//   /**
//    * Method ID    : saveOrdPick
//    * Method 설명      : 피킹확정
//    * 작성자                 : chSong
//    * @param   model
//    * @return  
//    */
//   @RequestMapping("/WMSOP030/saveOrdPick.action")
//   public ModelAndView saveOrdPick(Map<String, Object> model){
//       ModelAndView mav = new ModelAndView("jsonView");
//       Map<String, Object> m  = new HashMap<String, Object>();
//       try{
//           m = service.saveOrdPick(model);
//       }catch(Exception e){
//           e.printStackTrace();
//           m.put("MSG", MessageResolver.getMessage("save.error"));       
//       }
//       mav.addAllObjects(m);
//       return mav;
//   }      
//       
//   /**
//    * Method ID    : saveCancelPick
//    * Method 설명      : 피킹취소
//    * 작성자                 : chSong
//    * @param   model
//    * @return  
//    */
//   @RequestMapping("/WMSOP030/saveCancelPick.action")
//   public ModelAndView saveCancelPick(Map<String, Object> model){
//       ModelAndView mav = new ModelAndView("jsonView");
//       Map<String, Object> m  = new HashMap<String, Object>();
//       try{
//           m = service.saveCancelPick(model);
//       }catch(Exception e){
//           e.printStackTrace();
//           m.put("MSG", MessageResolver.getMessage("save.error"));       
//       }
//       mav.addAllObjects(m);
//       return mav;
//   }      
//   
//   /**
//    * Method ID    : saveOutComplete
//    * Method 설명      : 출고확정
//    * 작성자                 : chSong
//    * @param   model
//    * @return  
//    */
//   @RequestMapping("/WMSOP030/saveOutComplete.action")
//   public ModelAndView saveOutComplete(Map<String, Object> model){
//       ModelAndView mav = new ModelAndView("jsonView");
//       Map<String, Object> m  = new HashMap<String, Object>();
//       try{
//           m = service.saveOutComplete(model);
//       }catch(Exception e){
//           e.printStackTrace();
//           m.put("MSG", MessageResolver.getMessage("save.error"));       
//       }
//       mav.addAllObjects(m);
//       return mav;
//   }  
//   
//   /**
//    * Method ID    : saveSimpleOut
//    * Method 설명      : 간편출고저장
//    * 작성자                 : chSong
//    * @param   model
//    * @return  
//    */
//   @RequestMapping("/WMSOP030/saveSimpleOut.action")
//   public ModelAndView saveSimpleOut(Map<String, Object> model){
//       ModelAndView mav = new ModelAndView("jsonView");
//       Map<String, Object> m  = new HashMap<String, Object>();
//       try{
//           m = service.saveSimpleOut(model);
//       }catch(Exception e){
//           e.printStackTrace();
//           m.put("MSG", MessageResolver.getMessage("save.error"));       
//       }
//       mav.addAllObjects(m);
//       return mav;
//   }  
//   
//   
//   /**
//    * Method ID    : wmsop030E5
//    * Method 설명      : 긴급RACK보충 화면
//    * 작성자                 : chSong
//    * @param   model
//    * @return  
//    * @throws Exception 
//    */
//   @RequestMapping("/WMSOP030E5.action")
//   public ModelAndView wmsop030E5(Map<String, Object> model) throws Exception {
//       return new ModelAndView("winus/wmsop/WMSOP030E5");     
//   }   
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    
//    /**
//     * Method ID    : wmsop030E2
//     * Method 설명      : 템플립업로드 화면
//     * 작성자                 : chSong
//     * @param   model
//     * @return  
//     * @throws Exception 
//     */
//    @RequestMapping("/WMSOP030E2.action")
//    public ModelAndView wmsop030E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
//            return new ModelAndView("winus/wmsop/WMSOP030E2");     
//    }
//    
//    
//    /**
//     * Method ID    : sampleExcelDown
//     * Method 설명      : 엑셀 샘플 다운
//     * 작성자                 : chsong
//     * @param model
//     * @return
//     */
//    @RequestMapping("/WMSOP030E2/sampleExcelDown.action")
//    public void sampleExcelDown(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
//        Map<String, Object> map = new HashMap<String, Object>();
//        try {
//            map = service.sampleExcelDown(model);
//            GenericResultSet grs = (GenericResultSet)map.get("LIST");
//            if(grs.getTotCnt() > 0){
//                this.doSampleExcelDown(response, grs, model);
//            }
//        }catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    
//    protected void doSampleExcelDown(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
//        try{
//            int Col_Size  = Integer.parseInt(model.get("Col_Size").toString());
//            int HRow_Size = 6;  //고정값
//            int VRow_Size = 2;  //고정값
//            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
//            String[][] headerEx = new String[Col_Size][HRow_Size];
//            String[][] valueName = new String[Col_Size][VRow_Size];
//            for(int i = 0 ; i < Col_Size ; i++){
//                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
//                headerEx[i] = new String[] {MessageResolver.getText((String)model.get("Name_"+i))   , i+"", i+"", "0", "0", "100"};
//                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
//                valueName[i] = new String[] {(String)model.get("Col_"+i)  , "S"};
//            }
//            //파일명
//            String FileName = MessageResolver.getText("템플릿");
//            //시트명
//            String sheetName = "Sheet1";
//            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
//            String marCk = "N";
//            //ComUtil코드
//            String Etc = "";
//            
//            ExcelWriter wr = new ExcelWriter();
//            wr.ExDown(ConstantIF.attachPath, grs, headerEx, valueName, FileName, sheetName, marCk, Etc, response);
//        } catch(Exception e) {
//            e.getMessage();
//        }
//    }
//    
//
//    /**
//     * Method ID  : inExcelFileUpload
//     * Method 설명  : Excel 파일 읽기
//     * 작성자             : chSong
//     * @param request
//     * @param response
//     * @param model
//     * @return
//     */
//    @RequestMapping("/WMSOP030E2/inExcelFileUpload.action")
//    public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model,  @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
//        ModelAndView mav = null;
//        try {
//            request.setCharacterEncoding("utf-8");
//            System.out.println("==========");
//            System.out.println(model);
//            System.out.println(request);
//            System.out.println(txtFile);
//            MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
//            MultipartFile file = multipartRequest.getFile("txtFile");
//    
//            String fileName = file.getOriginalFilename();
//            String filePaths = "C:/Tmp/";//CONTS에 정의된 변수
//            
//            //디렉토리 존재유무 확인
//            if(!FileHelper.existDirectory(filePaths)) {
//                FileHelper.createDirectorys(filePaths);//디렉토리생성
//            }
//            
//            File destinationDir = new File(filePaths);
//            
//            File destination = File.createTempFile("excelTemp", fileName, destinationDir); //두개의 문자값으로 조합...하여 파일이름을 변경저장
//            FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
//            
//            int colSize = Integer.parseInt((String)model.get("colSize"));         
//            String[] cellName = new String[colSize];
//            for(int i = 0 ; i < colSize ; i++){
//                cellName[i] = "S_"+i;
//            }
//            
//            int startRow = Integer.parseInt((String)model.get("startRow"));
//            
//            List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 1000, 0);//로우제한sheet idx,시작행 idx,끝행(시작행에서 몇번째),시작열 idx)
//            Map<String, Object> map = new HashMap<String, Object>();
//            map.put("LIST", list);
//            
//            if(destination.exists()){
//                destination.delete();
//            }
//            mav = new ModelAndView("jsonView", map);
//        } catch(Exception e){
//            e.printStackTrace();
//        }
//        return mav;
//    }
//    
//    /**
//     * Method ID    : saveExcelOrder
//     * Method 설명      : 템플릿 주문 저장
//     * 작성자                 : chSong
//     * @param   model
//     * @return  
//     */
//    @RequestMapping("/WMSOP030E2/saveExcelOrder.action")
//    public ModelAndView saveExcelOrder(Map<String, Object> model){
//        ModelAndView mav = new ModelAndView("jsonView");
//        Map<String, Object> m  = new HashMap<String, Object>();
//        try{
//            m = service.saveExcelOrder(model);
//        }catch(Exception e){
//            e.printStackTrace();
//            m.put("MSG", MessageResolver.getMessage("save.error"));       
//        }
//        mav.addAllObjects(m);
//        return mav;
//    }    
    
}
