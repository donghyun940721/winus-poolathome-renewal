package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP030Service")
	private WMSOP030Service service;

	static final String[] COLUMN_NAME_WMSOP030E12 = {
		"I_ORD_ID", "I_ORD_SEQ", "I_LOC_CD"
	};
	/*-
	 * Method ID    : wmsop030
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP030.action")
	public ModelAndView wmsop030(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP130.action")
	public ModelAndView wmsop130(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP130", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP131.action")
	public ModelAndView wmsop131(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP131", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP132.action")
	public ModelAndView wmsop132(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP132", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP133.action")
	public ModelAndView wmsop133(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP133", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP134.action")
	public ModelAndView wmsop134(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP134", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP135.action")
	public ModelAndView wmsop135(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP135", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP230.action")
	public ModelAndView wmsop230(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP230", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP330.action")
	public ModelAndView wmsop330(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP330", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP337.action")
	public ModelAndView WMSOP337(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP337", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP339.action")
	public ModelAndView WMSOP339(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP339", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP430.action")
	public ModelAndView wmsop430(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP430", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP630.action")
	public ModelAndView wmsop630(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP630", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP123.action")
	public ModelAndView wmsop123(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP123", service.selectItemGrp(model));
	}
	
	/*-
	 * Method ID    : WMSOP134Q1
	 * Method 설명      : 거래명세서 OZ 신규
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP134Q1.action")
	public ModelAndView WMSOP134Q1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP134Q1");
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 출고관리  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID    : listByCust
	 * Method 설명      : 화주별 출고관리  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByCust.action")
	public ModelAndView listByCust(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCust(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : listByOlive
	 * Method 설명      : 화주별 출고관리  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByOlive.action")
	public ModelAndView listByOlive(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.listByOlive(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listByCustCJ
	 * Method 설명      : 화주별 출고관리  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByCustCJ.action")
	public ModelAndView listByCustCJ(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustCJ(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustSF
	 * Method 설명      : 화주별 출고관리  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByCustSF.action")
	public ModelAndView listByCustSF(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustSF(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : inspectionList
	 * Method 설명 : 검수내역 조회 화면(올리브영)
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP630pop3.action")
	public ModelAndView inspectionListView(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP630pop3");
	}
	
	/*-
	 * Method ID : inspectionList
	 * Method 설명 : 검수내역 조회 (올리브영)
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/inspectionList.action")
	public ModelAndView inspectionList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.inspectionList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : /WMSOP630pop4
	 * Method 설명      : 올리브영 주소 변경 이력 조회 팝업
	 * 작성자                 : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP630pop4.action")
	public ModelAndView WMSOP630pop4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP630pop4");
	}
	
	/*-
	* Method ID    : deleteOrder
	* Method 설명      : 주문삭제
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/deleteOrder.action")
	public ModelAndView deleteOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteOrder(model);	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/*-
	* Method ID    : deleteOrderV2
	* Method 설명      : 주문삭제
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/deleteOrderV2.action")
	public ModelAndView deleteOrderV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteOrderV2(model);	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	* Method ID           : deleteOrderOm
	* Method 설명        :  출고관리(화주별_OM) 주문 완전 삭제 (delYn이 아닌 delete)
	* 작성자                 : KSJ
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/deleteOrderOm.action")
	public ModelAndView deleteOrderOm(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteOrderNcode(model);	
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	* Method ID    : listExcel
	* Method 설명      : 엑셀다운로드
	* 작성자                 : chsong
	* @param model
	* @return
	*/
	@RequestMapping("/WMSOP030/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID    : listExcel(PLT)
     * Method 설명      : 엑셀다운로드(PLT)
     * 작성자                 : smics
     * @param model
     * @return
	 */
	@RequestMapping("/WMSOP030/excelPlt.action")
	public void listExcelPlt(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelPlt(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
       try{
           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
           String[][] headerEx = {
				        		   {MessageResolver.getText("주문구분")   , "0", "0", "0", "0", "100"}
                                  ,{MessageResolver.getText("작업상태")   , "1", "1", "0", "0", "100"}
                                  ,{MessageResolver.getText("원주문번호")  , "2", "2", "0", "0", "100"}
                                  ,{MessageResolver.getText("주문번호")   , "3", "3", "0", "0", "100"}
                                  ,{MessageResolver.getText("주문SEQ")  , "4", "4", "0", "0", "100"}
                                  ,{MessageResolver.getText("LOT NO") ,  "5", "5", "0", "0", "100"}
                                  ,{MessageResolver.getText("출고예정일")  , "6", "6", "0", "0", "100"}
                                  ,{MessageResolver.getText("출고일자")   , "7", "7", "0", "0", "100"}
                                  ,{MessageResolver.getText("화주")   , "8", "8", "0", "0", "100"}
                                  ,{MessageResolver.getText("배송처")   , "9", "9", "0", "0", "100"}
                                  ,{MessageResolver.getText("상품코드")   , "10", "10", "0", "0", "100"}
                                  ,{MessageResolver.getText("상품명")   , "11", "11", "0", "0", "100"}
                                  ,{MessageResolver.getText("주문량")   , "12", "12", "0", "0", "100"}
                                  ,{MessageResolver.getText("UOM")   , "13", "13", "0", "0", "100"}
                                  ,{MessageResolver.getText("출고량")   , "14", "14", "0", "0", "100"}
                                  ,{MessageResolver.getText("UOM")   , "15", "15", "0", "0", "100"}
                                  ,{MessageResolver.getText("비고1")   , "16", "16", "0", "0", "100"}
                                  ,{MessageResolver.getText("비고2")   , "17", "17", "0", "0", "100"}
                                  ,{MessageResolver.getText("주문중량")   , "18", "18", "0", "0", "100"}
                                  ,{MessageResolver.getText("출고중량")   , "19", "19", "0", "0", "100"}
                                  ,{MessageResolver.getText("송장번호")   , "20", "20", "0", "0", "100"}
                                  ,{MessageResolver.getText("현재고")   , "21", "21", "0", "0", "100"}
                                  ,{MessageResolver.getText("불량재고")   , "22", "22", "0", "0", "100"}
                                  ,{MessageResolver.getText("PLT수량")   , "23", "23", "0", "0", "100"}
                                  ,{MessageResolver.getText("BOX수량")   , "24", "24", "0", "0", "100"}
                                  ,{MessageResolver.getText("창고")   , "25", "25", "0", "0", "100"}
                                  ,{MessageResolver.getText("컨테이너번호")   , "26", "26", "0", "0", "100"}
                                 };
           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
           String[][] valueName = {
				        		   {"ORD_SUBTYPE_NM"  , "S"},
                                   {"WORK_STAT_EXCEL"  , "S"},
                                   {"ORG_ORD_ID"  , "S"},
                                   {"VIEW_ORD_ID"  , "S"},
                                   {"ORD_SEQ"  , "S"},
                                   {"CUST_LOT_NO"  , "S"},
                                   {"OUT_REQ_DT"  , "S"},
                                   {"OUT_DT"  , "S"},
                                   {"CUST_NM"  , "S"},
                                   {"TRANS_CUST_NM"  , "S"},
                                   {"RITEM_CD"  , "S"},
                                   {"RITEM_NM"  , "S"},
                                   {"OUT_ORD_QTY"  , "N"},
                                   {"OUT_ORD_UOM_NM"  , "S"},
                                   {"REAL_OUT_QTY"  , "N"},
                                   {"OUT_UOM_NM"  , "S"},
                                   {"ORD_DESC"  , "S"},
                                   {"ETC2"  , "S"},
                                   {"OUT_ORD_WEIGHT"  , "S"},
                                   {"REAL_OUT_WEIGHT"  , "S"},
                                   {"INV_NO"  , "S"},
                                   {"STOCK_QTY"  , "N"},
                                   {"BAD_QTY"  , "N"},
                                   {"REAL_PLT_QTY"  , "N"},
                                   {"REAL_BOX_QTY"  , "N"},
                                   {"OUT_WH_NM"  , "S"},
                                   {"CNTR_NO"  , "S"}
                                  }; 
           
			// 파일명
			String fileName = MessageResolver.getText("출고관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	* Method ID    : saveAlloc
	* Method 설명      : 할당 처리, 취소 (할당처리, 할당취소 분할해서 처리햇던거 합침, 프로시져만 다른거타지 보내는것같아서)
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveAlloc.action")
	public ModelAndView saveAlloc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveAlloc(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	* Method ID    : saveOrdPick
	* Method 설명      : 피킹확정
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveOrdPick.action")
	public ModelAndView saveOrdPick(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOrdPick(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	* Method ID    : saveCancelPick
	* Method 설명      : 피킹취소
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveCancelPick.action")
	public ModelAndView saveCancelPick(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCancelPick(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	* Method ID    : saveOutComplete
	* Method 설명      : 출고확정
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveOutComplete.action")
	public ModelAndView saveOutComplete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutComplete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	* Method ID    : outUpdateZero
	* Method 설명      : 출하0처리확정
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/outUpdateZero.action")
	public ModelAndView outUpdateZero(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.outUpdateZero(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	* Method ID    : cancelOutReReceiving
	* Method 설명      : 출고확정취소 처리 후 자동 재입고
	* 작성자                 : MonkeySeok
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/cancelOutReReceiving.action")
	public ModelAndView cancelOutReReceiving(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.cancelOutReReceiving(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : saveSimpleOut
	* Method 설명      : 간편출고저장
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveSimpleOut.action")
	public ModelAndView saveSimpleOut(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSimpleOut(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop030E2
	 * Method 설명      : 템플립업로드 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030E2.action")
	public ModelAndView wmsop030E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030E2");
	}
	
	/*-
	 * Method ID    : wmsop030E4
	 * Method 설명      : 템플립업로드 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030E4.action")
	public ModelAndView wmsop030E4(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030E4");
	}
	
	/*-
	 * Method ID    : wmsop030E4ALL
	 * Method 설명      : 템플립업로드 화면 (화주구분없이 일괄 업로드)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030E4ALL.action")
	public ModelAndView wmsop030E4ALL(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030E4ALL");
	}
	
	@RequestMapping("/WMSOP030E13.action")
	public ModelAndView WMSOP030E13(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030E13");
	}
	
	/*-
	 * Method ID    : wmsop122E2
	 * Method 설명      : 템플립업로드 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP122E2.action")
	public ModelAndView WMSOP122E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP122E2");
	}
	
	/*-
	 * Method ID    : wmsop030SE2
	 * Method 설명      : 템플립업로드 화면 SAP
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030SE2.action")
	public ModelAndView wmsop030SE2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030SE2");
	}
	
	/*-
	 * Method ID    : wmsop030SE3
	 * Method 설명      : 템플립업로드 화면 B2C
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030SE3.action")
	public ModelAndView wmsop030SE3(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030SE3");
	}

	/*-
	 * Method ID    	: WMSOP030SE5
	 * Method 설명      : B2C 템플립업로드 화면(공란적용)
	 * 작성자           : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030SE5.action")
	public ModelAndView WMSOP030SE5(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030SE5");
	}

	/*-
	 * Method ID    	: wmsop030SE6
	 * Method 설명      : 입출고 B2B 템플릿 입력(주문상세유형 선택 추가)
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030SE6.action")
	public ModelAndView WMSOP030SE6(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030SE6");
	}
	
	/*-
	 * Method ID    : wmsop030SE13
	 * Method 설명      : 템플립업로드 화면 B2D
	 * 작성자                 : seongjun
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030SE4.action")
	public ModelAndView wmsop030SE4(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030SE4");
	}

	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E2/sampleExcelDown.action")
	public void sampleExcelDown(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.getSampleExcelDown(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doSampleExcelDown(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	protected void doSampleExcelDown(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
		try {
			int iColSize = Integer.parseInt(model.get("Col_Size").toString());
			int iHRowSize = 6; // 고정값
			int iVRowSize = 2; // 고정값
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			String[][] headerEx = new String[iColSize][iHRowSize];
			String[][] valueName = new String[iColSize][iVRowSize];
			for (int i = 0; i < iColSize; i++) {
				// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
				headerEx[i] = new String[]{MessageResolver.getText((String) model.get("Name_" + i)), i + "", i + "", "0", "0", "100"};
				// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
				// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
				valueName[i] = new String[]{(String) model.get("Col_" + i), "S"};
			}
			// 파일명
			String fileName = MessageResolver.getText("템플릿");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : chSong
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E2/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int colSize = Integer.parseInt((String) model.get("colSize"));
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}

			int startRow = Integer.parseInt((String) model.get("startRow"));

			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 1000, 0);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("LIST", list);

			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", map);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUploadAllowBlank
	 * Method 설명  : Excel 파일 읽기 - 공란 허용 로직적용
	 * 작성자             : kimzero
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E4/inExcelFileUploadAllowBlank.action")
	public ModelAndView inExcelFileUploadAllowBlank(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			//STEP 1. EXCEL 인코딩 셋팅.
			request.setCharacterEncoding(ConstantIF.PROPERTY_FILE_ENCODING);		//'utf-8' String Const.

			//STEP 1-1. 파일을 이름으로 받아서 임시저장 하기 위한 Directory 셋팅.
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			//STEP 1-1. Directory 체크 -> 없으면 생성.
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			//STEP 1-2. 셋팅한 경로로 Path 셋팅후 파일 저장(FileCopyUtils 사용)
			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			
			//STEP 2. 임시저장으로 떨군 파일을 다시 읽어들이기 위한 기준 정보들 셋팅(colSize, startRow, destination, cellName 등)
			List<Map<String, Object>> excelTemplate = new ArrayList();
			excelTemplate = service.getTemplate(model);
			
			//STEP 3. 미리 정의해둔 템플릿 형태를 읽어들여 파일과 함께 전달 -> Row / Col 기준대로 파싱.
			List<Map<String, Object>> list = new ArrayList();
			list = ExcelReader.excelReadRowByTemplate(excelTemplate, destination);
			
			//STEP 4. 파싱된 List<Map> 형태의 데이터로 DB Insert Query 수행.
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		    , model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO"		    , model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	    , model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	    , model.get("SS_USER_NO"));
			mapBody.put("vrCustCd"		    , model.get("vrCustCd"));
			mapBody.put("vrCustId"		    , model.get("vrCustId"));
			mapBody.put("vrTemplateType"	, model.get("vrTemplateType"));
			model.put("templateId", "0000000001");
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
			
			//STEP 4-1. Run Query.
			m = service.saveExcelOrderJavaAllowBlank(mapBody, mapHeader);
			
			//FINISH. 모든 스텝 완료후 임시저장했던 파일 삭제.
			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUploadAllowBlankALL
	 * Method 설명  : Excel 파일 읽기 - 공란 허용 로직적용 - 화주통합템플릿
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E4/inExcelFileUploadAllowBlankALL.action")
	public ModelAndView inExcelFileUploadAllowBlankALL(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			//STEP 1. EXCEL 인코딩 셋팅.
			request.setCharacterEncoding(ConstantIF.PROPERTY_FILE_ENCODING);		//'utf-8' String Const.
			
			//STEP 1-1. 파일을 이름으로 받아서 임시저장 하기 위한 Directory 셋팅.
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수
			
			//STEP 1-1. Directory 체크 -> 없으면 생성.
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}
			
			//STEP 1-2. 셋팅한 경로로 Path 셋팅후 파일 저장(FileCopyUtils 사용)
			File destinationDir = new File(filePaths);
			
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			//STEP 2. 임시저장으로 떨군 파일을 다시 읽어들이기 위한 기준 정보들 셋팅(colSize, startRow, destination, cellName 등)
			List<Map<String, Object>> excelTemplate = new ArrayList();
			excelTemplate = service.getTemplateALL(model);
			
			//STEP 3. 미리 정의해둔 템플릿 형태를 읽어들여 파일과 함께 전달 -> Row / Col 기준대로 파싱.
			List<Map<String, Object>> list = new ArrayList();
			list = ExcelReader.excelReadRowByTemplate(excelTemplate, destination);
			
			//STEP 4. 파싱된 List<Map> 형태의 데이터로 DB Insert Query 수행.
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		    , model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO"		    , model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	    , model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	    , model.get("SS_USER_NO"));
			mapBody.put("vrTemplateType"	, model.get("vrTemplateType"));
			mapBody.put("vrOrdSubType"		, model.get("vrOrdSubType"));
			model.put("templateId"			, model.get("vrTemplateId"));
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
			
			//STEP 5. Run Query.
			m = service.saveExcelOrderJavaAllowBlankALL(mapBody, mapHeader);
			
			//FINISH. 모든 스텝 완료후 임시저장했던 파일 삭제.
			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기 - 대화물류 전용(*자체 템플릿 파싱 로직 적용)
	 * 작성자             : ykim
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E4/inExcelUploadCustTemplate.action")
	public ModelAndView inExcelFileUploadCustTemplate(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			//STEP 1. EXCEL 인코딩 셋팅.
			request.setCharacterEncoding(ConstantIF.PROPERTY_FILE_ENCODING);		//'utf-8' String Const.

			//STEP 1-1. 파일을 이름으로 받아서 임시저장 하기 위한 Directory 셋팅.
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			//STEP 1-1. Directory 체크 -> 없으면 생성.
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			//STEP 1-2. 셋팅한 경로로 Path 셋팅후 파일 저장(FileCopyUtils 사용)
			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			
			//STEP 2. 임시저장으로 떨군 파일을 다시 읽어들이기 위한 기준 정보들 셋팅(colSize, startRow, destination, cellName 등)
			List<Map<String, Object>> excelTemplate = new ArrayList();
			excelTemplate = service.getTemplate(model);
			
			//STEP 3. 미리 정의해둔 템플릿 형태를 읽어들여 파일과 함께 전달 -> Row / Col 기준대로 파싱.
			List<Map<String, Object>> list = new ArrayList();
			list = ExcelReader.excelReadRowByTemplate(excelTemplate, destination);
			
			//STEP 4. 파싱된 List<Map> 형태의 데이터로 DB Insert Query 수행.
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		    , model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO"		    , model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	    , model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	    , model.get("SS_USER_NO"));
			mapBody.put("vrCustCd"		    , model.get("vrCustCd"));
			mapBody.put("vrTemplateType"	, model.get("vrTemplateType"));
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
			
			//STEP 4-1. Run Query.
			m = service.saveExcelOrderJava(mapBody, mapHeader);
			
			
			//FINISH. 모든 스텝 완료후 임시저장했던 파일 삭제.
			if (destination.exists()) {
				destination.delete();
			}
			
			//완료후 결과 리턴.
			mav.addAllObjects(m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}

	/*-
	 * Method ID    : saveExcelOrder
	 * Method 설명      : 템플릿 주문 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030E2/saveExcelOrder.action")
	public ModelAndView saveExcelOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveExcelOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save excel :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveExcelOrderSAP
	 * Method 설명      : 템플릿 주문 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030SE2/saveExcelOrderSAP.action")
	public ModelAndView saveExcelOrderSAP(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveExcelOrderSAP(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save excel :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : wmsop030E5
	 * Method 설명      : 긴급RACK보충 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030E5.action")
	public ModelAndView wmsop030E5(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030E5");
	}

	/*-
	 * Method ID    : listRackSearch
	 * Method 설명      : 긴급RACK보충  조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030E5/listRackSearch.action")
	public ModelAndView listRackSearch(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listRackSearch(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : wmsop030E7
	 * Method 설명      : 출고 주문 자동생성화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030E7.action")
	public ModelAndView wmsop030E7(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030E7");
	}
	
	/*-
	 * Method ID    : autoBestLocSave
	 * Method 설명      : 아산물류센터 로케이션추천 자동
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/autoBestLocSave.action")
	public ModelAndView autoBestLocSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoBestLocSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : ordDelSetReordInsert
	 * Method 설명      : 주문재등록
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/ordDelSetReordInsert.action")
	public ModelAndView ordDelSetReordInsert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.ordDelSetReordInsert(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : asnSave
	 * Method 설명      : ASN생성버튼
	 * 작성자                 : smics
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/asnSave.action")
	public ModelAndView asnSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.asnSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID    : WMSOP030T1
     * Method 설명        : 출고관리 작업지시 버튼
     * 작성자                     : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOP030T.action")
    public ModelAndView wmsop030T1(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsop/WMSOP030T", service.selectBox(model));
    }
    
    /**
     * Method ID    : WorkUpdateOrder
     * Method 설명        : 작업지시 팝업
     * 작성자                     : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSOP030/WorkUpdateOrder.action")
    public ModelAndView WorkUpdateOrder(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.WorkUpdateOrder(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
	 * Method ID    : saveWorkOrder
	 * Method 설명      : 작업지시 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/saveWorkOrder.action")
	public ModelAndView saveWorkOrder(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveWorkOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : changeMapping
	* Method 설명      : 입출고매핑전환
	* 작성자                 : MonkeySeok
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/changeMapping.action")
	public ModelAndView changeMapping(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.changeMapping(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
	 * Method ID : adminOrderDelete
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030/adminOrderDelete.action")
	public ModelAndView adminOrderDelete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.adminOrderDelete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 현재고 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSOP030T/PoplistExcel.action")
	public void PoplistExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.WorkUpdateOrder(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownPoplistExcel(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDownPoplistExcel(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("LOT번호")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("주문수량")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("매핑수량")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("차이수량")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("물류용기명")	, "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("SEQ16")		, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("SAP바코드")	, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일자")		, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("오더번호")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("오더순번")		, "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("상품코드")		, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("EPC코드")		, "12", "12", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_LOT_NO"          , "S"},
                                    {"TOTAL_QTY"       		, "N"},
                                    {"QTY"          		, "N"},
                                    {"GAP_QTY"          	, "N"},
                                    {"RTI_CODE"          	, "S"},
                                    
                                    {"SEQ16"  				, "S"},
                                    {"SAP_BARCODE"			, "S"},
                                    {"REG_DT"           	, "S"},
                                    {"VIEW_ORD_ID"         	, "S"},
                                    {"VIEW_ORD_SEQ"         , "S"},
                                    
                                    {"ITEM_CODE"           	, "S"},
                                    {"ITEM_NAME"            , "S"},
                                    {"RTI_EPC_CD"           , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("작업지시상세내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030/listExcel2.action")
	public void excelDown2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.getExcelDown2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
		try {
			int iColSize = Integer.parseInt(model.get("Col_Size").toString());
			int iHRowSize = 6; // 고정값
			int iVRowSize = 2; // 고정값
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			String[][] headerEx = new String[iColSize][iHRowSize];
			String[][] valueName = new String[iColSize][iVRowSize];
			for (int i = 0; i < iColSize; i++) {
				String nName = MessageResolver.getText((String) model.get("Name_" + i)).replace("&lt;br&gt;", "").replace("&lt;/font&gt;&lt;br&gt;", "").replace("&lt;/font&gt;", "").replace("&lt;font color=&quot;#B1B1B1&quot;&gt;", "");
				// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
				headerEx[i] = new String[]{nName, i + "", i + "", "0", "0", "100"};
				// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
				// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
				valueName[i] = new String[]{(String) model.get("Col_" + i), "S"};
			}
			// 파일명
			String fileName = MessageResolver.getText("출고관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	* Method ID    : outOrderCntInit
	* Method 내용      : 출고주문등록수량조회
	* 작성자                : MonkeySeok
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSOP030/outOrderCntInit.action")
	public ModelAndView inOrderCntInit(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.outOrderCntInit(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get lcinfolist :", e);
			}
		}
		return mav;
	}
	
	/*-
	* Method ID    : outWorkingCntInit
	* Method 내용      : 출고작업중수량조회
	* 작성자                 : MonkeySeok
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSOP030/outWorkingCntInit.action")
	public ModelAndView outWorkingCntInit(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.outWorkingCntInit(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get lcinfolist :", e);
			}
		}
		return mav;
	}
	
	/**
     * Method ID    : WMSOP030E8
     * Method 설명        : 사용자정보 상세보기
     * 작성자                     : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSOP030E8.action")
    public ModelAndView wmsop030E8(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsop/WMSOP030E8");
    }
    
    /*-
	 * Method ID   : customerInfo
	 * Method 설명 : 고객정보 상세조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP030/customerInfo.action")
	public ModelAndView customerInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.customerInfo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : ifOutOrd
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/ifOutOrd.action")
	public ModelAndView ifOutOrd(Map<String, Object> model) {

		if (log.isInfoEnabled()) {
			log.info(model);
		}

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.ifOutOrd(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSOP030T1
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030T1.action")
	public ModelAndView wmsop030t1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030T1");
	}
	
	/*-
	 * Method ID    : wmsop030E9
	 * Method 설명      : 송장번호입력
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030E9.action")
	public ModelAndView wmsop030E9(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030E9");
	}
	
	/*-
	 * Method ID  : outDlvNoUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : chSong
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E9/outDlvNoUpload.action")
	public ModelAndView outDlvNoUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int colSize = Integer.parseInt((String) model.get("colSize"));
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}

			int startRow = Integer.parseInt((String) model.get("startRow"));

			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 1000, 0);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("LIST", list);

			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", map);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : saveDlvNo
	 * Method 설명      : 송장번호 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030E9/saveDlvNo.action")
	public ModelAndView saveDlvNo(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveDlvNo(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save excel :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : listExcelDlvNoTemp
	* Method 설명      : 엑셀다운로드DlvNoTemp
	* 작성자                 : chsong
	* @param model
	* @return
	*/
	@RequestMapping("/WMSOP030/excelDlvNoTemp.action")
	public void listExcelDlvNoTemp(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelDlvNoTemp(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownDlvNoTemp(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDownDlvNoTemp(HttpServletResponse response, GenericResultSet grs) {
       try{
           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
           String[][] headerEx = {
                                   {MessageResolver.getText("주문번호")  	 , "0", "0", "0", "0", "100"}
                                  ,{MessageResolver.getText("주문순번")		, "1", "1", "0", "0", "100"}
                                  ,{MessageResolver.getText("고객명")		, "2", "2", "0", "0", "100"}
                                  ,{MessageResolver.getText("고객주소")		, "3", "3", "0", "0", "100"}
                                  ,{MessageResolver.getText("고객전화번호")	, "4", "4", "0", "0", "100"}
                                  ,{MessageResolver.getText("원주문번호")	, "5", "5", "0", "0", "100"}
                                  ,{MessageResolver.getText("배송사명입력")	, "6", "6", "0", "0", "100"}
                                  ,{MessageResolver.getText("송장번호입력")	, "7", "7", "0", "0", "100"}
                                  ,{MessageResolver.getText("기타")		, "8", "8", "0", "0", "100"}
                                 };
           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
           String[][] valueName = {
                                   {"ORD_ID"  				, "S"},
                                   {"ORD_SEQ"  				, "S"},
                                   {"P_SALES_CUST_NM"   	, "S"},
                                   {"P_ADDR"  				, "S"},
                                   {"P_PHONE_1"  			, "S"},
                                   {"ORG_ORD_ID"  			, "S"},
                                   {"P_DELIVERY_COMPANY"  	, "S"},
                                   {"P_DELIVERY_NO"  		, "S"},
                                   {"ORD_DESC"  			, "S"}
                                  }; 
           
			// 파일명
			String fileName = MessageResolver.getText("송장번호입력템플릿");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID  : inExcelFileUploadJava
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : chSong
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E4/inExcelFileUploadJava.action")
	public ModelAndView inExcelFileUploadjava(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			/* 엑셀 데이터 추출 */
			int colSize = Integer.parseInt((String) model.get("colSize"));
			
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 10000, 0);
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		,model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO"		,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	,model.get("SS_USER_NO"));
			mapBody.put("vrCustCd"		,model.get("vrCustCd"));
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
			
			//run
			m = service.saveExcelOrderJava(mapBody, mapHeader);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUploadJavaALL
	 * Method 설명  : Excel 화주, 등록구분 없이 모두 등록
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E4ALL/inExcelFileUploadJava.action")
	public ModelAndView inExcelFileUploadjavaALL(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수
			
			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}
			
			File destinationDir = new File(filePaths);
			
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			//STEP 2. 임시저장으로 떨군 파일을 다시 읽어들이기 위한 기준 정보들 셋팅(colSize, startRow, destination, cellName 등)
			List<Map<String, Object>> excelTemplate = new ArrayList();
			excelTemplate = service.getTemplate(model);
			//STEP 3. 미리 정의해둔 템플릿 형태를 읽어들여 파일과 함께 전달 -> Row / Col 기준대로 파싱.
			List<Map<String, Object>> list = new ArrayList();
			list = ExcelReader.excelReadRowByTemplate(excelTemplate, destination);
			
			//STEP 4. 파싱된 List<Map> 형태의 데이터로 DB Insert Query 수행.
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		,model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO"		,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	,model.get("SS_USER_NO"));
			mapBody.put("vrCustCd"		,model.get("vrCustCd"));
			mapBody.put("vrCustId"		    , model.get("vrCustId"));
			mapBody.put("vrTemplateType"	, model.get("vrTemplateType"));
			
			model.put("templateId"	, model.get("vrTemplateId"));
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
			
			//run
			m = service.saveExcelOrderJavaALL(mapBody, mapHeader);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : inExcelFileUploadJavaALL
	 * Method 설명  : Excel 화주, 등록구분 없이 모두 등록
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E4ALL/inExcelFileUploadJavaCheck.action")
	public ModelAndView inExcelFileUploadjavaALLCheck(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수
			
			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}
			
			File destinationDir = new File(filePaths);
			
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			//STEP 2. 임시저장으로 떨군 파일을 다시 읽어들이기 위한 기준 정보들 셋팅(colSize, startRow, destination, cellName 등)
			List<Map<String, Object>> excelTemplate = new ArrayList();
			excelTemplate = service.getTemplateALL(model);
			//STEP 3. 미리 정의해둔 템플릿 형태를 읽어들여 파일과 함께 전달 -> Row / Col 기준대로 파싱.
			List<Map<String, Object>> list = new ArrayList();
			list = ExcelReader.excelReadRowByTemplate(excelTemplate, destination);
			
			//STEP 4. 파싱된 List<Map> 형태의 데이터로 DB Insert Query 수행.
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		    , model.get("vrOrdType"));
			mapBody.put("SS_SVC_NO"		    , model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	    , model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	    , model.get("SS_USER_NO"));
			mapBody.put("vrTemplateType"	, model.get("vrTemplateType"));
			mapBody.put("vrOrdSubType"		, model.get("vrOrdSubType"));
			model.put("templateId"			, model.get("vrTemplateId"));
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
			
			//run
			m = service.saveExcelOrderJavaALLCheck(mapBody, mapHeader);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	
	
	/*-
	 * Method ID    : checkOrgOrdNum
	 * Method 설명      : 출고관리(v2.0)신규 원주문번호 체크
	 * 작성자                 : kijun11
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/checkOrgOrdNum.action")
	public ModelAndView checkOrgOrdNum(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {		
			m = service.beforeSavecheckOrgOrdNum(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	 * Method ID    : autoDeleteLocSave
	 * Method 설명      : 로케이션지정 삭제 일괄
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/autoDeleteLocSave.action")
	public ModelAndView autoDeleteLocSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoDeleteLocSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : autoBestLocSaveMulti
	 * Method 설명      : 아산물류센터 로케이션추천 자동
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/autoBestLocSaveMulti.action")
	public ModelAndView autoBestLocSaveMulti(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoBestLocSaveMulti(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : autoBestLocSaveMultiV2
	 * Method 설명      : 아산물류센터 로케이션추천 자동V2
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/autoBestLocSaveMultiV2.action")
	public ModelAndView autoBestLocSaveMultiV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoBestLocSaveMultiV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	/*-
	 * Method ID    : autoBestLocSaveMultiPart
	 * Method 설명      : 로케이션추천 부분재고지정
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/autoBestLocSaveMultiPart.action")
	public ModelAndView autoBestLocSaveMultiPart(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoBestLocSaveMultiPart(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	
	/*-
	 * Method ID  : inExcelFileUploadJavaB2C
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : chSong
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030E4/inExcelFileUploadJavaB2C.action")
	public ModelAndView inExcelFileUploadJavaB2C(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			/* 엑셀 데이터 추출 */
			int colSize = Integer.parseInt((String) model.get("colSize"));
			
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 100000, 0);
			Map<String, Object> mapBody = new HashMap<String, Object>();
//            log.info("LEGACY_ORG_ORD_NO:"+model.get("LEGACY_ORG_ORD_NO"));
//            log.info("PHONE_2:"+model.get("PHONE_2"));
            
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"			,model.get("vrOrdType"));
			mapBody.put("vrSrchCustCd"		,model.get("vrSrchCustCd"));
			mapBody.put("vrSrchCustId"		,model.get("vrSrchCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			mapBody.put("vrCustSeq"			,model.get("vrCustSeq"));
			mapBody.put("vrCustSeqNm"		,model.get("vrCustSeqNm"));
			mapBody.put("vrSrchOrdDegree"	,model.get("vrSrchOrdDegree"));
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);

			m = service.saveExcelOrderJavaB2T(mapBody, mapHeader);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	@RequestMapping("/WMSOP030E4/inExcelFileUploadJavaB2D.action")
	public ModelAndView inExcelFileUploadJavaB2D(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			/* 엑셀 데이터 추출 */
			int colSize = Integer.parseInt((String) model.get("colSize"));
			
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 10000, 0);
			Map<String, Object> mapBody = new HashMap<String, Object>();
            //log.info("PHONE_1:"+model.get("PHONE_1"));
            //log.info("PHONE_2:"+model.get("PHONE_2"));
            
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		,model.get("vrOrdType"));
			mapBody.put("vrSrchCustCd"	,model.get("vrSrchCustCd"));
			mapBody.put("vrSrchCustId"	,model.get("vrSrchCustId"));
			mapBody.put("SS_SVC_NO"		,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	,model.get("SS_USER_NO"));
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
			
			//run
			m = service.saveExcelOrderJavaB2D(mapBody, mapHeader);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	* Method ID    : listExcelB2C
	* Method 설명      : 엑셀다운로드B2C
	* 작성자                 : chsong
	* @param model
	* @return
	*/
	@RequestMapping("/WMSOP030/excelB2C.action")
	public void listExcelB2C(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			/*
			int insCnt = Integer.parseInt(model.get("selectIds").toString());
        	int arrCnt = 0;
        	String[] ordId = new String[insCnt];
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
        		ordId[arrCnt] = (String)model.get("ORD_ID"+i);
        		arrCnt++;
        	}
        	//프로시져 파라미터 입력
    		model.put("arrOrdId", ordId);
    		*/
			map = service.listExcelB2C(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownB2C(response, grs, (String)model.get("logisCustGb"));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDownB2C(HttpServletResponse response, GenericResultSet grs, String logisCustGb) {
       try{
    	   if(logisCustGb.equals("CJLOGIS")){
            	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
                String[][] headerEx = {
				                		 {MessageResolver.getText("예약구분")				, "0", "0", "0", "0", "100"}
			                            ,{MessageResolver.getText("집하예정일")				, "1", "1", "0", "0", "100"}
			                            ,{MessageResolver.getText("보내는사람")				, "2", "2", "0", "0", "100"}
			                            ,{MessageResolver.getText("보내는분연락처")			, "3", "3", "0", "0", "100"}
			                            ,{MessageResolver.getText("판매자 연락처")			, "4", "4", "0", "0", "100"}
			                            
			                            ,{MessageResolver.getText("보내는분우편번호")			, "5", "5", "0", "0", "100"}
			                            ,{MessageResolver.getText("택배 처리점소명")			, "6", "6", "0", "0", "100"}
			                            ,{MessageResolver.getText("받는분성명")				, "7", "7", "0", "0", "100"}
			                            ,{MessageResolver.getText("받는분전화번호")			, "8", "8", "0", "0", "100"}
			                            ,{MessageResolver.getText("받는분기타연락처")			, "9", "9", "0", "0", "100"}
			                            
			                            ,{MessageResolver.getText("받는분우편번호")			, "10", "10", "0", "0", "100"}
			                            ,{MessageResolver.getText("받는분주소(전체및분할)")		, "11", "11", "0", "0", "100"}
			                            ,{MessageResolver.getText("운송장번호")				, "12", "12", "0", "0", "100"}
			                            ,{MessageResolver.getText("고객주문번호")			, "13", "13", "0", "0", "100"}
			                            ,{MessageResolver.getText("품목명")				, "14", "14", "0", "0", "100"}
			                            
			                            ,{MessageResolver.getText("수량")					, "15", "15", "0", "0", "100"}
			                            ,{MessageResolver.getText("아이스팩 추가 개수")			, "16", "16", "0", "0", "100"}
			                            ,{MessageResolver.getText("박스수량")				, "17", "17", "0", "0", "100"}
			                            ,{MessageResolver.getText("박스타입")				, "18", "18", "0", "0", "100"}
			                            ,{MessageResolver.getText("기본운임")				, "19", "19", "0", "0", "100"}
			                            
			                            ,{MessageResolver.getText("배송메세지1")			, "20", "20", "0", "0", "100"}
			                            ,{MessageResolver.getText("배송메세지2")			, "21", "21", "0", "0", "100"}
			                            ,{MessageResolver.getText("운임구분")				, "22", "22", "0", "0", "100"}
			                            ,{MessageResolver.getText("총중량")				, "23", "23", "0", "0", "100"}
                                      };
                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
                String[][] valueName = {
				                		 {"ISNULL"				, "S"}
			                            ,{"ISNULL"				, "S"}
			                            ,{"SALES_CUST_NM_FROM"	, "S"}
			                            ,{"PHONE_2_FROM"		, "S"}
			                            ,{"TEL"					, "S"}
			                            
			                            ,{"ISNULL"				, "S"}
			                            ,{"DEALT_BRAN_NM"		, "S"}
			                            ,{"SALES_CUST_NM_TO"	, "S"}
			                            ,{"PHONE_1"				, "S"}
			                            ,{"PHONE_2_TO"			, "S"}
			                            
			                            ,{"ISNULL"				, "S"}
			                            ,{"ADDR"				, "S"}
			                            ,{"ISNULL"				, "S"}
			                            ,{"SALES_CUST_NO"		, "S"}
			                            ,{"ITEM_NAME"			, "S"}
			                            
			                            ,{"ORD_QTY"				, "S"}
			                            ,{"ISNULL"				, "S"}
			                            ,{"ISNULL"				, "S"}
			                            ,{"ISNULL"				, "S"}
			                            ,{"ISNULL"				, "S"}
			                            
			                            ,{"ORD_DESC"			, "S"}
			                            ,{"ETC1"				, "S"}
			                            ,{"ISNULL"				, "S"}
			                            ,{"SUM_REAL_OUT_WEIGHT"	, "S"}
                                       }; 
                
    			// 파일명
                String fileName = MessageResolver.getText("CJ대한통운");
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";

    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
            }else if(logisCustGb.equals("LOTTE")){
            	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
                String[][] headerEx = {
			                			 {MessageResolver.getText("주문일")			,"0" ,"0", "0", "0", "100"}
					        		   , {MessageResolver.getText("납기일자")			,"1" ,"1", "0", "0", "100"}
					        		   , {MessageResolver.getText("수취인")			,"2" ,"2", "0", "0", "100"}
					        		   , {MessageResolver.getText("수취인핸드폰")		,"3" ,"3", "0", "0", "100"}
					        		   , {MessageResolver.getText("수취인전화번호")		,"4" ,"4", "0", "0", "100"}
					        		   
					        		   , {MessageResolver.getText("우편번호")			,"5" ,"5", "0", "0", "100"}
					        		   , {MessageResolver.getText("주소")				,"6" ,"6", "0", "0", "100"}
					        		   , {MessageResolver.getText("배송메시지")			,"7" ,"7", "0", "0", "100"}
					        		   , {MessageResolver.getText("품명")				,"8" ,"8", "0", "0", "100"}
					        		   , {MessageResolver.getText("규격")				,"9" ,"9", "0", "0", "100"}
					        		   
					        		   , {MessageResolver.getText("수량")				,"10" ,"10", "0", "0", "100"}
					        		   , {MessageResolver.getText("비고")				,"11" ,"11", "0", "0", "100"}
					        		   , {MessageResolver.getText("주문번호")			,"12" ,"12", "0", "0", "100"}
					        		   , {MessageResolver.getText("품번")				,"13" ,"13", "0", "0", "100"}
					        		   , {MessageResolver.getText("운송장번호")			,"14" ,"14", "0", "0", "100"}
					        		   
					        		   , {MessageResolver.getText("Lot No#")		,"15" ,"15", "0", "0", "100"}
					        		   , {MessageResolver.getText("배송일")			,"16" ,"16", "0", "0", "100"}
					        		   , {MessageResolver.getText("택배사코드")			,"17" ,"17", "0", "0", "100"}
					        		   , {MessageResolver.getText("SCM거래처")			,"18" ,"18", "0", "0", "100"}
					        		   , {MessageResolver.getText("출고창고")			,"19" ,"19", "0", "0", "100"}
					        		   
					        		   , {MessageResolver.getText("중단")				,"20" ,"20", "0", "0", "100"}
					        		   , {MessageResolver.getText("주문확정")			,"21" ,"21", "0", "0", "100"}
					        		   , {MessageResolver.getText("사은품명")			,"22" ,"22", "0", "0", "100"}
					        		   , {MessageResolver.getText("사은품번호")			,"23" ,"23", "0", "0", "100"}
					        		   , {MessageResolver.getText("중단일자")			,"24" ,"24", "0", "0", "100"}
					        		   
					        		   , {MessageResolver.getText("중단사유")			,"25" ,"25", "0", "0", "100"}
					        		   , {MessageResolver.getText("중단자코드")			,"26" ,"26", "0", "0", "100"}
					        		   , {MessageResolver.getText("중단자")			,"27" ,"27", "0", "0", "100"}
					        		   , {MessageResolver.getText("주문구분")			,"28" ,"28", "0", "0", "100"}
					        		   , {MessageResolver.getText("고객인도일")			,"29" ,"29", "0", "0", "100"}
					        		   
					        		   , {MessageResolver.getText("배송방법")			,"30" ,"30", "0", "0", "100"}
					        		   , {MessageResolver.getText("배송권역")			,"31" ,"31", "0", "0", "100"}
					        		   , {MessageResolver.getText("용적합계")			,"32" ,"32", "0", "0", "100"}
					        		   , {MessageResolver.getText("임가공상품타입")		,"33" ,"33", "0", "0", "100"}
					        		   , {MessageResolver.getText("임가공구성요소수량")	,"34" ,"34", "0", "0", "100"}
					        		   
					        		   , {MessageResolver.getText("추천박스유형")		,"35" ,"35", "0", "0", "100"}
					        		   , {MessageResolver.getText("총중량")			,"36" ,"36", "0", "0", "100"}
					        		   //, {MessageResolver.getText("합포장")			,"36" ,"36", "0", "0", "100"}
                                      };
                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
                String[][] valueName = {
			                			 {"OUT_REQ_DT" 			,"S"}
					        		   , {"OUT_DT" 				,"S"}
					        		   , {"SALES_CUST_NM_TO" 	,"S"}
					        		   , {"PHONE_1" 			,"S"}
					        		   , {"PHONE_2_TO" 			,"S"}
					        		   
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ADDR" 				,"S"}
					        		   , {"ORD_DESC" 			,"S"}
					        		   , {"ITEM_NAME" 			,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   
					        		   , {"ORD_QTY" 			,"S"}
					        		   , {"ETC1"				,"S"}
					        		   , {"ORG_ORD_ID" 			,"S"}
					        		   , {"ITEM_CODE" 			,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   
					        		   , {"ISNULL" 				,"S"}
					        		   , {"ISNULL" 				,"S"}
					        		   , {"REAL_OUT_WEIGHT" 	,"S"}
					        		   , {"SET_ITEM_TYPE" 		,"S"}
					        		   , {"SET_ITEM_CNT" 		,"S"}
					        		   
					        		   , {"REQUEST_BOX_SIZE" 	,"S"}
					        		   , {"SUM_REAL_OUT_WEIGHT"	,"S"}
					        		   //, {"SUM_CNT" 			,"S"}
                                       }; 
                
    			// 파일명
                String fileName = MessageResolver.getText("롯데택배");
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";
    			
    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            }
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : WMSOP030E10
	 * Method 설명          : PDF 뷰어 화면(팝업) 
	 * 작성자                          : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030E10.action")
	public ModelAndView WMSOP030E10(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP030E10");
	}
	
	/*-
	* Method ID    : saveCjDataInsert
	* Method 설명      : 할당 처리, 취소 (할당처리, 할당취소 분할해서 처리햇던거 합침, 프로시져만 다른거타지 보내는것같아서)
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveCjDataInsert.action")
	public ModelAndView saveCjDataInsert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCjDataInsert(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveCJConfirm
	 * Method 설명      : cj 택배 송장 인터페이스 확정
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/saveCJConfirm.action")
	public ModelAndView saveCJConfirm(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> confList = new HashMap<String, Object>();
		Map<String, Object> confListInvc = new HashMap<String, Object>();
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String inDate   = ((String)model.get("ORD_DATE")).replaceAll("\"", "");
			//new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
			String inTime   = new java.text.SimpleDateFormat("HHmmss").format(new java.util.Date());
			String ifId     = "IF_D"+inDate+"T"+inTime+"_"+(String)model.get(ConstantIF.SS_USER_NO);
			
			//CJ택배 주문 예정자료 조회
			model.put("vrSrchCjTempDlvCompCd", "CJ");
			model.put("vrSrchCjTempInsertFlg", "Y");
			confList = service.listByCustCJ_setParam(model);
			
			//CJ택배 주문자료 WMSDF020 테이블 Insert,WMSDF000 송장번호 채번업데이트, WMSDF010 & WMSDF020 송장번호 Insert
			confList.put("IF_ID"		, ifId);
			confList.put("SS_SVC_NO"	, (String)model.get(ConstantIF.SS_SVC_NO));
			confList.put("SS_CLIENT_IP"	, (String)model.get(ConstantIF.SS_CLIENT_IP));
			confList.put("SS_USER_NO"	, (String)model.get(ConstantIF.SS_USER_NO));
			m = service.cjConfListInsertToWMSDF020(confList); /* 배송주소정제 Insert (for반복문 함께처리) */
			System.out.println("CJ 주소정제 service 진행 확인");
			
			if(m.get("errCnt").equals(-1)){
				//패키지 처리 오류
				//m.put("MSG", "주소정제 후 주문인터페이스 입력 오류. (관리자문의)");
			}else if(m.get("errCnt").equals(-2)){
				m.put("MSG", "주문 할 자료가 없습니다. (e.-2)");
			}else{
				//CJ택배 주문 정보 조회 (WMSDF020 Select : IF_ID)
				model.put("vrSrchIfId", ifId);
				confListInvc = service.listByCustCJ_ordInvcNoConf(model);
				
				//CJ택배 주문자료 CJ주문 V_RCPT_WINUS010 테이블 Insert (CJ DB)
				m = service.cjConfListInsertToV_RCPT_WINUS010(confListInvc);
				System.out.println("CJ shipment service 진행 확인");
				
				//@@
				//모든 IF 주문확정 처리 완료 후 WMSDF020, WMSDF010 IF_CONF_FLG = 'Y' 처리
				m = service.cjConfListUpdateToWMSDF020(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : GodomallOrderUpdate
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/IF_GODOMALL_ORDER_UPDATE.action")
	public ModelAndView godomallOrderUpdate(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String key 			= model.get("key").toString();
			String partner_key 	= model.get("partner_key").toString();
			String url 			= model.get("url").toString();
        
			model.put("key"				,key);
			model.put("partner_key"		,partner_key);
			model.put("url"				,url);
			m = service.godomallOrderUpdate(model);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : /WMSOP130RCJpop.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP130RCJpop.action")
	public ModelAndView wmsop130RCJpop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP130RCJpop");
	}
	
	/*-
	 * Method ID    : saveCJReturn
	 * Method 설명      : cj 택배 반품입력 인터페이스
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/saveCJReturn.action")
	public ModelAndView saveCJReturn(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> confList = new HashMap<String, Object>();
		Map<String, Object> confListInvc = new HashMap<String, Object>();
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			String inDate   = ((String)model.get("ORD_DATE")).replaceAll("\"", "");
			//new java.text.SimpleDateFormat("yyyyMMdd").format(new java.util.Date());
			String inTime   = new java.text.SimpleDateFormat("HHmmss").format(new java.util.Date());
			String ifId     = "IF_D"+inDate+"T"+inTime+"_"+(String)model.get(ConstantIF.SS_USER_NO);
			
			//CJ택배 주문 예정자료 조회
			model.put("vrSrchCjTempDlvCompCd", "CJ");
			model.put("vrSrchCjIfConfFlg", "Y");
			confList = service.listByCustCJReturn_setParam(model);
			
			confList.put("IF_ID"		, ifId);
			confList.put("SS_SVC_NO"	, (String)model.get(ConstantIF.SS_SVC_NO));
			confList.put("SS_CLIENT_IP"	, (String)model.get(ConstantIF.SS_CLIENT_IP));
			confList.put("SS_USER_NO"	, (String)model.get(ConstantIF.SS_USER_NO));
			m = service.cjReturnListInsertToWMSDF020(confList);
			
			if(m.get("errCnt").equals(-1)){
				//패키지 처리 오류
				m.put("MSG", "주소정제 후 주문인터페이스 입력 오류. (관리자문의)");
			}else if(m.get("errCnt").equals(-2)){
				m.put("MSG", "주문 할 자료가 없습니다. (e.-2)");
			}else{
				//CJ택배 주문 정보 조회 (WMSDF020 Select : IF_ID)
				model.put("vrSrchIfId", ifId);
				confListInvc = service.listByCustCJ_ordInvcNoConf(model);
				//CJ택배 주문자료 CJ주문 V_RCPT_WINUS010 테이블 Insert (CJ DB)
				m = service.cjConfListInsertToV_RCPT_WINUS010(confListInvc);
				
				//@@
				//모든 IF 주문확정 처리 완료 후 WMSDF020 IF_CONF_FLG = 'Y' 처리
				m = service.cjReturnListUpdateToWMSDF020(model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : saveSfDataInsert
	* Method 설명      : 할당 처리, 취소 (할당처리, 할당취소 분할해서 처리햇던거 합침, 프로시져만 다른거타지 보내는것같아서)
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveSfDataInsert.action")
	public ModelAndView saveSfDataInsert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSfDataInsert(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : invoicPdfDownloadSF
	* Method 설명      : 
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/invoicPdfDownloadSF.action")
	public ModelAndView invoicPdfDownloadSF(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.invoicPdfDownloadSF(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : /WMSOP130RSFpop.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP130RSFpop.action")
	public ModelAndView wmsop130RSFpop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP130RSFpop");
	}
	
	/*-
	* Method ID    : saveSfDataCancel
	* Method 설명      : 취소
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveSfDataCancel.action")
	public ModelAndView saveSfDataCancel(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSfDataCancel(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustSummary
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByCustSummary.action")
	public ModelAndView listByCustSummary(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustSummary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustDetail
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByCustDetail.action")
	public ModelAndView listByCustDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustCount
	 * Method 설명      : 화주별 출고관리  간략 조회 카운트
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listCountByCust.action")
	public ModelAndView listCountByCust(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listCountByCust(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : autoBestLocSaveMultiV3
	 * Method 설명      :  로케이션추천 자동V3
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/autoBestLocSaveMultiV3.action")
	public ModelAndView autoBestLocSaveMultiV3(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoBestLocSaveMultiV3(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	/*-
	 * Method ID    : autoDeleteLocSaveV2
	 * Method 설명      : 로케이션지정 삭제 일괄
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/autoDeleteLocSaveV2.action")
	public ModelAndView autoDeleteLocSaveV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoDeleteLocSaveV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : saveOutCompleteV2
	* Method 설명      : 출고확정
	* 작성자                 : KHKIM
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveOutCompleteV2.action")
	public ModelAndView saveOutCompleteV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutCompleteV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	* Method ID    : saveOutCompleteV2
	* Method 설명      : 출고확정
	* 작성자                 : KHKIM
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP030/saveOutCompleteV3.action")
	public ModelAndView saveOutCompleteV3(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutCompleteV3(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : pickingTotalSearch
	 * Method 설명      : 토탈 피킹리스트발행 프로시져및 pdf생성
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/pickingTotalSearchV2.action")
	public ModelAndView pickingTotalSearchV2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {

			m = service.updatePickingTotalV2(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);


		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : batch
	 * Method 설명      : 배치 프로시져
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/batch.action")
	public ModelAndView batch(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.batch(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
		
	
	}
	
	/*-
	 * Method ID    : /WMSOP330pop
	 * Method 설명      : 
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP630pop.action")
	public ModelAndView WMSOP630pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP630pop");
	}
	/*-
	 * Method ID    : /WMSOP330pop2
	 * Method 설명      : 
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP630pop2.action")
	public ModelAndView WMSOP630pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP630pop2");
	}
	
	/*-
	 * Method ID    : /WMSOP330pop
	 * Method 설명      : 
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP330pop.action")
	public ModelAndView WMSOP330pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP330pop");
	}
	
	/*-
	 * Method ID    : listByCustNoCount
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByCustNoCount.action")
	public ModelAndView listByCustNoCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByCustNoCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : KHKIM
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030/pickingListExcel.action")
	public void pickingListExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.pickingListExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doPickingExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doPickingExcelDown(HttpServletResponse response, GenericResultSet grs) {
	       try{
	           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
	           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
	           String[][] headerEx = {
	                                   {MessageResolver.getText("품온정보")   , "0", "0", "0", "0", "100"}
	                                  ,{MessageResolver.getText("바코드")   , "1", "1", "0", "0", "100"}
	                                  ,{MessageResolver.getText("상품코드")   , "2", "2", "0", "0", "100"}
	                                  ,{MessageResolver.getText("상품명")   , "3", "3", "0", "0", "100"}
	                                  ,{MessageResolver.getText("LOC")   , "4", "4", "0", "0", "100"}
	                                  ,{MessageResolver.getText("규격")   , "5", "5", "0", "0", "100"}
	                                  ,{MessageResolver.getText("대표단위")   , "6", "6", "0", "0", "100"}
	                                  ,{MessageResolver.getText("요약")   , "7", "7", "0", "0", "100"}
	                                 };
	           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
	           String[][] valueName = {
	                                   {"TEMP_NM"  , "S"},
	                                   {"ITEM_BAR_CD"  , "S"},
	                                   {"RITEM_CD"  , "S"},
	                                   {"RITEM_NM"  , "S"},
	                                   {"LOC_NM"  , "S"},
	                                   {"ITEM_STANDARD"  , "S"},
	                                   {"OUT_ORD_UOM_ID"  , "S"},
	                                   {"QTY"  , "S"}
	                                  
	                                  }; 
	           
				// 파일명
				String fileName = MessageResolver.getText("피킹리스트");
				// 시트명
				String sheetName = "Sheet1";
				// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
				String marCk = "N";
				// ComUtil코드
				String etc = "";

				ExcelWriter wr = new ExcelWriter();
				wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("fail download Excel file...", e);
				}
			}
		}
	
	/*-
	 * Method ID    : outInvalidView
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP330/outInvalidView.action")
	public ModelAndView deviceInvalidView(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.outInvalidView(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : confirmPickingTotal
	 * Method 설명      : 토탈피킹리스트 확정
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/confirmPickingTotal.action")
	public ModelAndView confirmPickingTotal(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			
			m = service.confirmPickingTotal(model);
		
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : confirmPickingSearo
	 * Method 설명      : 토탈피킹리스트 확정 새로피엔엘
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/pickingListSaero.action")
	public ModelAndView confirmPickingSaero(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		model.put("ORD_IDs", ((String)model.get("ORD_IDs")).replace("&apos;", "'"));
		try {
			m = service.updatePickingTotalV2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSOP030E11
	 * Method 설명   : 운영관리 > 출고관리 > 파일업로드 화면 및 기능
	 * 작성자         : KCR
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030E11.action")
	public ModelAndView WMSOP030E11(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP030E11");
	}
	
	@RequestMapping("/WMSOP030/saveFile.action")
	public ModelAndView saveFile(Map<String, Object> model) {
		
		log.info(model);
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveFile(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSOP030
	 * Method 설명   : 운영관리 > 출고관리 > 작업중 해제
	 * 작성자         : MonkeySeok
	 * 날짜 : 2021-02-18
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/workingUnlock.action")
	public ModelAndView workingUnlock(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.workingUnlock(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("ERROR :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : totalWorkingUnlock
	 * Method 설명   : 출고관리(올리브영) 헤더테이블 작업중 해제
	 * 작성자         : Seongjun Kwon
	 * 날짜 : 2021-08-12
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/totalWorkingUnlock.action")
	public ModelAndView totalWorkingUnlock(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.totalWorkingUnlock(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("ERROR :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID	: getPickingLog
	 * Method 설명	: 피킹리스트 발행 LOG 리스트 조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP030/getPickingLog.action")
	public ModelAndView getPickingLog(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getPickingLog(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: productOliveSoldOut
	 * Method 설명	: 품절처리
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP030/productOliveSoldOut.action")
	public ModelAndView productOliveSoldOut(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.productOliveSoldOut(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID	: cancelProductOliveSoldOut
	 * Method 설명	: 품절처리취소
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP030/cancelProductOliveSoldOut.action")
	public ModelAndView cancelProductOliveSoldOut(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.cancelProductOliveSoldOut(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustSummary
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByOliveSummary.action")
	public ModelAndView listByOliveSummary(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByOliveSummary(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustDetail
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByOliveDetail.action")
	public ModelAndView listByOliveDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.listByOliveDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustCount
	 * Method 설명      : 화주별 출고관리  간략 조회 카운트
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listCountByOlive.action")
	public ModelAndView listCountByOlive(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listCountByOlive(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	

	/*-
	 * Method ID    : autoBestLocSaveMultiV3
	 * Method 설명      :  로케이션추천 자동V3
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/autoBestLocSaveMultiOlive.action")
	public ModelAndView autoBestLocSaveMultiOlive(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoBestLocSaveMultiOlive(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : pickingTotalSearch
	 * Method 설명      : 토탈 피킹리스트발행 프로시져및 pdf생성
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/pickingTotalSearchOlive.action")
	public ModelAndView pickingTotalSearchOlive(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {

			m = service.updatePickingTotalOlive(model);

			// log.info("[ pdfFileName ] :" + pdfFileName);


		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : /WMSOP030popOlive.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030popOlive.action")
	public ModelAndView WMSOP030popOlive(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030popOlive");
	}
	
	
	
	/*-
	 * Method ID    : confirmPickingSearo
	 * Method 설명      : 토탈피킹리스트 확정 새로피엔엘
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/pickingListOlive.action")
	public ModelAndView pickingListOlive(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		model.put("ORD_IDs", ((String)model.get("ORD_IDs")).replace("&apos;", "'"));
		try {
			m = service.updatePickingTotalOlive(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : confirmPickingSearo
	 * Method 설명      : 토탈피킹리스트 확정 새로피엔엘
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/searchAddressOlive.action")
	public ModelAndView searchAddressOlive(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jqGridJsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.searchAddressOlive(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : confirmPickingSearo
	 * Method 설명      : 토탈피킹리스트 확정 새로피엔엘
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/updateAddressOlive.action")
	public ModelAndView updateAddressOlive(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateAddressOlive(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : confirmPickingSearo
	 * Method 설명      : 토탈피킹리스트 확정 새로피엔엘
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/updateInvalidAddressOlive.action")
	public ModelAndView updateInvalidAddressOlive(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateInvalidAddressOlive(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : WMSOP030E12
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSOP030E12.action")
	public ModelAndView WMSOP030E12(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030E12");
	}
	
	/*-
	 * Method ID : uploadLcoInfo
	 * Method 설명 : 엑셀파일업로드
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param txtFile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSOP030E12/uploadLcoInfo.action")
	public ModelAndView uploadLcoInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP030E12, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : noneBillingFlag
	 * Method 설명      : 작업지시 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/noneBillingFlag.action")
	public ModelAndView noneBillingFlag(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.noneBillingFlag(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: /WMSOP630popSplmtList
	 * Method 설명	: 보충지시서 
	 * 작성자			: 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP630popSplmtList.action")
	public ModelAndView WMSOP630popSplmtList(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP630popSplmtList");
	}
	
	/*-
	 * Method ID    : listByOm
	 * Method 설명      : 화주별 출고관리 조회(OM)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/outByListOm.action")
	public ModelAndView listByOm(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.outByListOm(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	@RequestMapping("/WMSOP030/outListByDelivery.action")
	public ModelAndView listByDelivery(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.outListByDelivery(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : /WMSOP030pop.action
	 * Method 설명      : 토탈피킹리스트 QR 발행
	 * 작성자                 : Seongjun Kwon
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030pop.action")
	public ModelAndView WMSOP030pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030pop");
	}
	/*-
	 * Method ID    : /WMSOP030popvx1.action WMSOP030popvx2.action
	 * Method 설명      : 카카오vx 피킹리스트
	 * 작성자                 : Seongjun Kwon
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030popvx1.action")
	public ModelAndView WMSOP030popvx1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030popvx1");
	}
	
	@RequestMapping("/WMSOP030popvx2.action")
	public ModelAndView WMSOP030popvx2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030popvx2");
	}
	
	@RequestMapping("/WMSOP030popvx3.action")
	public ModelAndView WMSOP030popvx3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030popvx3");
	}
	
	
	@RequestMapping("/WMSOP030popvx4.action")
	public ModelAndView WMSOP030popvx4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030popvx4");
	}
	
	@RequestMapping("/WMSOP030popvx5.action")
	public ModelAndView WMSOP030popvx5(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030popvx5");
	}
	
	/*-
	 * Method ID    : /WMSOP030pop.action
	 * Method 설명      : TNC로지스 거래명세서발행 
	 * 작성자                 : KCR
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP638Q1.action")
	public ModelAndView WMSOP638Q1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP638Q1");
	}
	
	@RequestMapping("/WMSOP638Q2.action")
	public ModelAndView WMSOP638Q2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP638Q2");
	}
	
	@RequestMapping("/WMSOP638Q3.action")
	public ModelAndView WMSOP638Q3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP638Q3");
	}
	
	@RequestMapping("/WMSOP513Q1.action")
	public ModelAndView WMSOP513Q1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP513Q1");
	}
	
	@RequestMapping("/WMSOP030/listNew.action")
	public ModelAndView listNew(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listNew(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	@RequestMapping("/WMSOP030/asnList.action")
	public ModelAndView asnList(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.asnList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	@RequestMapping("/WMSOP030/outPickingCancel.action")
	public ModelAndView outPickingCancel(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.outPickingCancel(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP030/outListByKcc.action")
	public ModelAndView outListByKcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.outListByKcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*상차피킹리스트 취소*/
	@RequestMapping("/WMSOP030/outTallyCancel.action")
	public ModelAndView outTallyCancel(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			System.out.println("여기1");
			m = service.outTallyCancel(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP030E13/inExcelFileUploadJavaInB2C.action")
	public ModelAndView inExcelFileUploadJavaInB2C(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			/* 엑셀 데이터 추출 */
			int colSize = Integer.parseInt((String) model.get("colSize"));
			
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 10000, 0);
			Map<String, Object> mapBody = new HashMap<String, Object>();
//            log.info("LEGACY_ORG_ORD_NO:"+model.get("LEGACY_ORG_ORD_NO"));
//            log.info("PHONE_2:"+model.get("PHONE_2"));
            
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		,model.get("vrOrdType"));
			mapBody.put("vrSrchCustCd"	,model.get("vrSrchCustCd"));
			mapBody.put("vrSrchCustId"	,model.get("vrSrchCustId"));
			mapBody.put("SS_SVC_NO"		,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	,model.get("SS_USER_NO"));
			mapBody.put("vrCustSeq"		,model.get("vrCustSeq"));
			
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
//			System.out.println(mapBody.get("vrSrchCustId"));
//			
//			m = service.saveExcelOrderJavaB2T(mapBody, mapHeader);
			
			m = service.saveExcelOrderJavaB2O(mapBody, mapHeader);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listByCustSummary
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByHeader.action")
	public ModelAndView listByHeader(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByHeader(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listKccByHeader
	 * Method 설명      : 화주별 출고관리  kcc
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByKccHeader.action")
	public ModelAndView listByKccHeader(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listByKccHeader(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : listByKccDetail
	 * Method 설명      : 화주별 출고관리  간략 조회
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByKccDetail.action")
	public ModelAndView listByKccDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.listByKccDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : listByKccCount
	 * Method 설명      : 화주별 출고관리  간략 조회 카운트 (OM 추가 )
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP030/listByKccCount.action")
	public ModelAndView listByKccCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listByKccCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	* Method ID    : listExcelOM
	* Method 설명      : 엑셀다운로드 (출고관리 화주별 OM)
	* 작성자                 : KSJ
	* @param model
	* @return
	*/
	@RequestMapping("/WMSOP030/listExcelOM.action")
	public void listExcelOM(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = service.listExcelOM(model);	
			
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doListExcelOMDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doListExcelOMDown(HttpServletResponse response, GenericResultSet grs) {
	       try{
	           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
	           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
	           String[][] headerEx = {
	                                   {MessageResolver.getText("작업상태")   , "0", "0", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문번호")   , "1", "1", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문SEQ")   , "2", "2", "0", "0", "100"}
	                                  ,{MessageResolver.getText("LOT 번호")   , "3", "3", "0", "0", "100"}
	                                  ,{MessageResolver.getText("출고예정일")   , "4", "4", "0", "0", "100"}
	                                  
	                                  ,{MessageResolver.getText("출고일자")   , "5", "5", "0", "0", "100"}
	                                  ,{MessageResolver.getText("상품코드")   , "6", "6", "0", "0", "100"}
	                                  ,{MessageResolver.getText("상품명")   , "7", "7", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문량")   , "8", "8", "0", "0", "100"}
	                                  ,{MessageResolver.getText("UOM")   , "9", "9", "0", "0", "100"}
	                                  
	                                  ,{MessageResolver.getText("출고량")   , "10", "10", "0", "0", "100"}
	                                  ,{MessageResolver.getText("UOM")   , "11", "11", "0", "0", "100"}
	                                  ,{MessageResolver.getText("할당로케이션")   , "12", "12", "0", "0", "100"}
	                                  ,{MessageResolver.getText("가용재고")   , "13", "13", "0", "0", "100"}
	                                  ,{MessageResolver.getText("현재고")   , "14", "14", "0", "0", "100"}
	                                  
	                                  ,{MessageResolver.getText("비고#1")   , "15", "15", "0", "0", "100"}
	                                  ,{MessageResolver.getText("주문구분")   , "16", "16", "0", "0", "100"}
	                                  ,{MessageResolver.getText("화주코드")   , "17", "17", "0", "0", "100"}
	                                  ,{MessageResolver.getText("화주명")      , "18", "18", "0", "0", "100"}
	                                  ,{MessageResolver.getText("원주문번호")   , "19", "19", "0", "0", "100"}
	                                  ,{MessageResolver.getText("원주문SEQ")   , "20", "20", "0", "0", "100"}
	                                  ,{MessageResolver.getText("체널구분")  		 , "21", "21", "0", "0", "100"}
	                                  ,{MessageResolver.getText("세부주문번호1")   , "22", "22", "0", "0", "100"}
	                                  ,{MessageResolver.getText("세부주문번호2")   , "23", "23", "0", "0", "100"}
	                                  
	                                  ,{MessageResolver.getText("고객명")    , "24", "24", "0", "0", "100"}
	                                  ,{MessageResolver.getText("전화번호")   , "25", "25", "0", "0", "100"}
	                                  ,{MessageResolver.getText("우편번호")   , "26", "26", "0", "0", "100"}
	                                  ,{MessageResolver.getText("배송처주소")  , "27", "27", "0", "0", "100"}
	                                  ,{MessageResolver.getText("배송메시지")  , "28", "28", "0", "0", "100"}
	                                  
	                                  ,{MessageResolver.getText("배송사명")   , "29", "29", "0", "0", "100"}
	                                  ,{MessageResolver.getText("제조일자")   , "30", "30", "0", "0", "100"}
	                                  ,{MessageResolver.getText("규격")      , "31", "31", "0", "0", "100"}
	                                  ,{MessageResolver.getText("송장번호")   , "32", "32", "0", "0", "100"}
	                                  
	                                 };
	           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
	           String[][] valueName = {
	                                   {"WORK_STAT_EXCEL"  , "S"},
	                                   {"ORD_ID"  , "S"},
	                                   {"ORD_SEQ"  , "S"},
	                                   {"CUST_LOT_NO"  , "S"},
	                                   {"OUT_REQ_DT"  , "S"},
	                                   
	                                   {"OUT_DT"  , "S"},
	                                   {"RITEM_CD"  , "S"},
	                                   {"P_ITEM_NM"  , "S"},
	                                   {"OUT_ORD_QTY"  , "N"},
	                                   {"OUT_ORD_UOM_NM"  , "S"},
	                                   
	                                   {"REAL_OUT_QTY"  , "N"},
	                                   {"OUT_UOM_NM"  , "S"},
	                                   {"LOC_CD"  , "S"},
	                                   {"SHIPPING_ABLE_QTY"  , "N"},
	                                   {"STOCK_QTY"  , "N"},
	                                   
	                                   {"ETC2"  , "S"},
	                                   {"ORD_SUBTYPE_NM"  , "S"},
	                                   {"CUST_CD"  , "S"},
	                                   {"CUST_NM"  , "S"},
	                                   {"ORG_ORD_ID"  , "S"},
	                                   {"CUST_ORD_SEQ"  , "S"},
	                                   
	                                   {"ORD_INCOM_ETC_TYPE_NM"  , "S"},
	                                   {"LEGACY_ORG_ORD_NO"  , "S"},
	                                   {"LEGACY_ORD_DETAIL_NO"  , "S"},
	                                   {"P_SALES_CUST_NM"  , "S"},
	                                   {"P_PHONE_1"  , "S"},
	                                   {"P_ZIP"  , "S"},
	                                   {"P_ADDR"  , "S"},
	                                   
	                                   {"ETC2"  , "S"},
	                                   {"P_DELIVERY_COMPANY"  , "S"},
	                                   {"MAKE_DT"  , "S"},
	                                   {"UNIT_NM"  , "S"},
	                                   {"P_DELIVERY_NO"  , "S"},
	                                  }; 
	           
				// 파일명
				String fileName = MessageResolver.getText("출고관리");
				// 시트명
				String sheetName = "Sheet1";
				// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
				String marCk = "N";
				// ComUtil코드
				String etc = "";

				ExcelWriter wr = new ExcelWriter();
				wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("fail download Excel file...", e);
				}
			}
		}
	
	
	/*-
	* Method ID    : listExcelOM
	* Method 설명      : 엑셀다운로드 (출고관리 화주별 OM)
	* 작성자                 : KSJ
	* @param model
	* @return
	*/
	@RequestMapping("/WMSOP030/ncodeExcelDown.action")
	public void ncodeExcelDown(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			
			map = service.nCodeExcelList(model);	
			
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			this.nCodeExcelWrite(response, grs);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void nCodeExcelWrite(HttpServletResponse response, GenericResultSet grs) {
	       try{
	           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
	           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
	           String[][] headerEx = {
	                                   {MessageResolver.getText("업무구분")  , "0", "0", "0", "0", "100"}
	                                  ,{MessageResolver.getText("일자")     , "1", "1", "0", "0", "100"}
	                                  ,{MessageResolver.getText("거래처 코드") , "2", "2", "0", "0", "100"}
	                                  ,{MessageResolver.getText("존")       , "3", "3", "0", "0", "100"}
	                                  ,{MessageResolver.getText("바코드")    , "4", "4", "0", "0", "100"}
	                                  
	                                  ,{MessageResolver.getText("품번")     , "5", "5", "0", "0", "100"}
	                                  ,{MessageResolver.getText("색상")     , "6", "6", "0", "0", "100"}
	                                  ,{MessageResolver.getText("사이즈")    , "7", "7", "0", "0", "100"}
	                                  ,{MessageResolver.getText("수량")      , "8", "8", "0", "0", "100"}
	                                  ,{MessageResolver.getText("판매금액")   , "9", "9", "0", "0", "100"}
	                                  
	                                 };
	           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
	           String[][] valueName = {
	                                   {"ORD_TYPE"     , "S"},
	                                   {"ORD_DT"       , "S"},
	                                   {"TRANS_CUST"   , "S"},
	                                   {"ZONE"         , "S"},
	                                   {"ITEM_BAR_CD"  , "S"},
	                                   
	                                   {"ITEM_CODE" , "S"},
	                                   {"COLOR"     , "S"},
	                                   {"ITEM_SIZE" , "S"},
	                                   {"REAL_QTY"  , "N"},
	                                   {"PRICE"     , "S"}
	                                  }; 
	           
				// 파일명
				String fileName = MessageResolver.getText("엔코드 실적내역");
				// 시트명
				String sheetName = "Sheet1";
				// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
				String marCk = "N";
				// ComUtil코드
				String etc = "";

				ExcelWriter wr = new ExcelWriter();
				wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("fail download Excel file...", e);
				}
			}
		}
	

	/*-
	* Method ID    : delreCreAsn
	* Method 설명      : ASN삭제 및 재생성
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
//	@RequestMapping("/WMSOP030/delreCreAsn.action")
//	public ModelAndView delreCreAsn(Map<String, Object> model) {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.delreCreAsn(model);	
//			
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to delete :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
	
	@RequestMapping("/WMSOP030/delreCreAsn.action")
	public ModelAndView delreCreAsn(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delreCreAsn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		mav.addAllObjects(m);
		return mav;
	}

    
    /*-
	 * Method ID    		: workpickingOrder
	 * Method 설명      	: 피킹 -> 작업지시 할당 
	 * 작성자               : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/pickingWorkOrder.action")
	public ModelAndView pickingWorkOrder(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.pickingWorkOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    	: combineOrdDegree_B2B
	 * Method 설명      : 차수 합치기 B2B
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/combineOrdDegree_B2B.action")
	public ModelAndView combineOrdDegree_B2B(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.combineOrdDegree_B2B(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	/*-
	 * Method ID    	: decomposeOrdDegree_B2B
	 * Method 설명      : 차수 분해 B2B
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/decomposeOrdDegree_B2B.action")
	public ModelAndView decomposeOrdDegree_B2B(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.decomposeOrdDegree_B2B(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID	: wmsop132Q1
	 * Method 설명	: 출고관리 화주별 OM -> CS 접수 팝업
	 * 작성자       : KSJ
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP132Q1.action")
	public ModelAndView wmsop132Q1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		mav = new ModelAndView("winus/wmsop/WMSOP132Q1", service.getCutomerOrderInfo(model));
		return mav;
	}
	
	
	/*-
	 * Method ID	: wmsop132Q1
	 * Method 설명	: 출고관리 화주별 OM -> CS 접수 팝업 -> 고객정보 수정
	 * 작성자       : KSJ
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSOP132/custInfoSave.action")
	public ModelAndView custInfoSave(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.custInfoSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	/*-
	 * Method ID       : inExcelFileUploadCommonB2TS
	 * Method 설명     : B2C 엑셀업로드 (공란 허용) - WMSOP030TS
	 * 작성자          : KSJ
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP030/inExcelFileUploadCommonB2C_TS.action")
	public ModelAndView inExcelFileUploadCommonB2C_TS(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		
		try {
			
			//STEP 1. EXCEL 인코딩 셋팅.
			request.setCharacterEncoding(ConstantIF.PROPERTY_FILE_ENCODING);		//'utf-8' String Const.

			//STEP 1-1. 파일을 이름으로 받아서 임시저장 하기 위한 Directory 셋팅.
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			//STEP 1-1. Directory 체크 -> 없으면 생성.
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			//STEP 1-2. 셋팅한 경로로 Path 셋팅후 파일 저장(FileCopyUtils 사용)
			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			
			//STEP 2. 임시저장으로 떨군 파일을 다시 읽어들이기 위한 기준 정보들 셋팅(colSize, startRow, destination, cellName 등)
			List<Map<String, Object>> excelTemplate = new ArrayList();
			excelTemplate = service.getTemplateInfoV2(model);
			
			//STEP 3. 미리 정의해둔 템플릿 형태를 읽어들여 파일과 함께 전달 -> Row / Col 기준대로 파싱.
			List<Map<String, Object>> list = new ArrayList();
			list = ExcelReader.excelReadRowByTemplate(excelTemplate, destination);
			
			//STEP 4. 파싱된 List<Map> 형태의 데이터로 DB Insert Query 수행.
			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("vrOrdType"		    , model.get("vrOrdType"));
			mapBody.put("vrOrdSubType"		, model.get("vrOrdSubType"));
			mapBody.put("SS_SVC_NO"		    , model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"	    , model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"	    , model.get("SS_USER_NO"));
			mapBody.put("vrCustCd"		    , model.get("vrCustCd"));
			mapBody.put("vrCustId"		    , model.get("vrCustId"));
			mapBody.put("vrCustSeq"			, model.get("vrCustSeq"));
			mapBody.put("vrCustSeqNm"		, model.get("vrCustSeqNm"));
			mapBody.put("vrTemplateType"	, model.get("vrTemplateType"));
			mapBody.put("vrSrchOrdDegree"	, model.get("vrSrchOrdDegree"));
			model.put("templateId", "0000000001");
			
			/* DB 컬럼 추출 */
			Map<String, Object> mapHeader = new HashMap<String, Object>();
			mapHeader = service.getResult(model);
			
			m = service.saveExcelOrderJavaCommonB2C_TS(mapBody, mapHeader);
			
			if (destination.exists()) {
				destination.delete();
			}
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	
	/*-
	 * Method ID     : saveListRtnOrderJavaB2TS
	 * Method 설명      	 : 반품접수
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/saveListRtnOrderJavaB2TS.action")
	public ModelAndView saveListRtnOrderJavaB2TS(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			//run
			m = service.saveListRtnOrderJavaB2TS(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID     : saveListRtnOrderJavaB2AS
	 * Method 설명      	 : 반품접수
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/saveListRtnOrderJavaB2AS.action")
	public ModelAndView saveListRtnOrderJavaB2AS(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			//run
			m = service.saveListRtnOrderJavaB2AS(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID     : saveBoxRecom
	 * Method 설명   : 박스추천
	 * 작성자         : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP030/saveBoxRecom.action")
	public ModelAndView saveBoxRecom(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveBoxRecom(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
}
