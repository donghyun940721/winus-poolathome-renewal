package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP040Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP030Service")
	private WMSOP030Service service;

	/*-
	 * Method ID    : wmsop030
	 * Method 설명      : 출고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	/*
	@RequestMapping("/WINUS/WMSOP030.action")
	public ModelAndView wmsop030(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP030", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP130.action")
	public ModelAndView wmsop130(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP130", service.selectItemGrp(model));
	}
	*/
/*	@RequestMapping("/WINUS/WMSOP330.action")
	public ModelAndView wmsop330(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP330", service.selectItemGrp(model));
	}	*/
}
