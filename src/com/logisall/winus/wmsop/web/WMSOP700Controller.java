package com.logisall.winus.wmsop.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP700Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP700Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP700Service")
	private WMSOP700Service service;
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 통합조회(입/출고/재고) 화면
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSOP700.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsop/WMSOP700", service.selectBox(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list1
	 * Method 설명 : 통합조회(입/출고) 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP700/list.action")
	public ModelAndView list1(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	/*-
	 * Method ID : list2
	 * Method 설명 : 통합조회(재고) 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP700/listE02.action")
	public ModelAndView list2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : list3
	 * Method 설명 : 통합조회(센터별재고) 조회
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP700/listE03.action")
	public ModelAndView list3(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : listExcel1
	 * Method 설명 : 통합조회(입/출고/재고) 엑셀다운
	 * 작성자 : 이성중
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSOP700/excel.action")
	public void listExcel1(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : 이성중
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주LOT번호"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("이동중수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품상태"), "7", "7", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("로케이션"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션 유형"), "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("UOM"), "10", "10", "0", "0", "200"},                                   
                                   {MessageResolver.getMessage("재고중량"), "11", "11", "0", "0", "200"}                           
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"SUBUL_DT"         , "S"},
                                    {"CUST_NM"          , "S"},
                                    {"CUST_LOT_NO"      , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    
                                    {"STOCK_QTY"        , "N"},
                                    {"NOW_MOVING_QTY"   , "N"},
                                    {"ITEM_STAT"        , "S"},
                                    
                                    {"LOC_CD"        	, "S"},
                                    {"LOC_TYPE"         , "S"},                                    

                                    {"UOM_NM"           , "S"},                                    
                                    {"STOCK_WEIGHT"     , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("택배발급이력_상세");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}  
}