package com.logisall.winus.wmsop.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP000Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSOP000Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP000Service")
	private WMSOP000Service service;

	/*-
	 * Method ID    : centerRule
	 * Method 설명      : 센터정책항목
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000/centerRule.action")
	public ModelAndView centerRule(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.getCenterRule(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get center rule :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : assertRule
	 * Method 설명      : 정책검사
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000/assertRule.action")
	public ModelAndView assertRule(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.getAssertRule(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to asser rule :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop000Q1
	 * Method 설명      : 배차작업 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q1.action")
	public ModelAndView wmsop000Q1(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP000Q1");
	}

	/*-
	 * Method ID    : listCarPop
	 * Method 설명      : 배차작업 차량조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP000Q1/listCarPop.action")
	public ModelAndView listCarPop(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listCarPop(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listDsp
	 * Method 설명      : 배차작업 배차내역조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP000Q1/listDsp.action")
	public ModelAndView listDsp(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listDsp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveDsp
	 * Method 설명      : 배차내역 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q1/saveDsp.action")
	public ModelAndView saveDsp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveDsp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : deleteDsp
	 * Method 설명      : 배차내역 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q1/deleteDsp.action")
	public ModelAndView deleteDsp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteDsp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcelDsp
	 * Method 설명      : 배차내역 엑셀
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP000Q1/excel.action")
	public void listExcelDsp(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelDsp(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
			String[][] headerEx = {{MessageResolver.getText("차량번호"), "0", "0", "0", "0", "100"}, {MessageResolver.getText("주문번호"), "1", "1", "0", "0", "100"}, {MessageResolver.getText("순번"), "2", "2", "0", "0", "100"},
					{MessageResolver.getText("작업상태"), "3", "3", "0", "0", "100"}, {MessageResolver.getText("상품"), "4", "4", "0", "0", "100"}

					, {MessageResolver.getText("주문수량"), "5", "5", "0", "0", "100"}, {MessageResolver.getText("배차수량"), "6", "6", "0", "0", "100"}, {"UOM", "7", "7", "0", "0", "100"},
					{MessageResolver.getText("기사휴대폰번호"), "8", "8", "0", "0", "100"}, {MessageResolver.getText("담당기사"), "9", "9", "0", "0", "100"}

					, {MessageResolver.getText("운송사"), "10", "10", "0", "0", "100"}, {MessageResolver.getText("배송구분"), "11", "11", "0", "0", "100"}, {MessageResolver.getText("배송요청일"), "12", "12", "0", "0", "100"},
					{MessageResolver.getText("긴급출고여부"), "13", "13", "0", "0", "100"}, {MessageResolver.getText("상차료"), "14", "14", "0", "0", "100"}

					, {MessageResolver.getText("하차료"), "15", "15", "0", "0", "100"}, {MessageResolver.getText("운송료"), "16", "16", "0", "0", "100"}, {MessageResolver.getText("기타비용"), "17", "17", "0", "0", "100"},
					{MessageResolver.getText("배송존"), "18", "18", "0", "0", "100"}, {MessageResolver.getText("배송블록"), "19", "19", "0", "0", "100"}

					, {MessageResolver.getText("배송순위"), "20", "20", "0", "0", "100"}, {MessageResolver.getText("배송처코드"), "21", "21", "0", "0", "100"}, {MessageResolver.getText("배송처주소"), "22", "22", "0", "0", "100"},
					{MessageResolver.getText("배송요청시간"), "23", "23", "0", "0", "100"}, {MessageResolver.getText("환산BOX수량"), "24", "24", "0", "0", "100"}

					, {MessageResolver.getText("환산PLT수량"), "25", "25", "0", "0", "100"}, {MessageResolver.getText("배송완료수량"), "26", "26", "0", "0", "100"}, {MessageResolver.getText("용적합계"), "27", "27", "0", "0", "100"},
					{MessageResolver.getText("주문종류"), "28", "28", "0", "0", "100"}};
			// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
			// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {{"CAR_CD", "S"}, {"ORD_ID", "S"}, {"ORD_SEQ", "S"}, {"WORK_STAT", "S"}, {"RITEM_NM", "S"},

					{"ORD_ORD_QTY", "N"}, {"ORD_QTY", "N"}, {"ORD_UOM_NM", "S"}, {"DRV_TEL", "S"}, {"DRV_NM", "S"},

					{"TRANS_CD", "S"}, {"DLV_TYPE", "S"}, {"TRANS_REQ_DT", "S"}, {"KIN_OUT_YN", "S"}, {"UP_AMT", "N"},

					{"DOWN_AMT", "N"}, {"TRANS_AMT", "N"}, {"ETC_AMT", "N"}, {"DLV_ZONE", "S"}, {"DLV_BLOCK", "S"},

					{"DLV_SEQ", "S"}, {"TRANS_CUST_CD", "S"}, {"TRANS_CUST_ADDR", "S"}, {"DLV_REQ_DT", "S"}, {"EXP_BOX_QTY", "N"},

					{"EXP_PLT_QTY", "N"}, {"DLV_CONF_QTY", "N"}, {"CAPA_TOT", "N"}, {"ORD_TYPE", "S"}};

			// 파일명
			String fileName = MessageResolver.getText("배차현황");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : wmsop000Q2
	 * Method 설명      : 미배차주문내역 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q2.action")
	public ModelAndView wmsop000Q2(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP000Q2");
	}

	/*-
	 * Method ID    : listNewDsp
	 * Method 설명      : 미배차내역 조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP000Q2/listNewDsp.action")
	public ModelAndView listNewDsp(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listNewDsp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : wmsop000Q3
	 * Method 설명      : 입차확인 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q3.action")
	public ModelAndView wmsop000Q3(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP000Q3");
	}

	/*-
	 * Method ID    : listConfirmDsp
	 * Method 설명      : 입차확인내역 조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP000Q3/listConfirmDsp.action")
	public ModelAndView listConfirmDsp(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listConfirmDsp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveConfirmCarIn
	 * Method 설명      : 입차확인 승인/취소
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q3/saveConfirmCarIn.action")
	public ModelAndView saveConfirmCarIn(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveConfirmCarIn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop000Q4
	 * Method 설명      : Dock지정 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q4.action")
	public ModelAndView wmsop000Q4(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP000Q4");
	}

	//
	/*-
	 * Method ID    : saveSetDock
	 * Method 설명      : Dock장 지정, 해제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q4/saveSetDock.action")
	public ModelAndView saveSetDock(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSetDock(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop000Q5
	 * Method 설명      : 로케이션지정 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5.action")
	public ModelAndView wmsop000Q5(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP000Q5", service.selectData(model));
	}
	
	/*-
	 * Method ID    : wmsop000Q5V2
	 * Method 설명      : 로케이션지정 화면(건별 통합)
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5V2.action")
	public ModelAndView wmsop000Q5V2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP000Q5V2", service.selectData(model));
	}

	/*-
	 * Method ID    : saveLocInitType
	 * Method 설명      : 로케이션 추천?? 위해??
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/saveLocInitType.action")
	public ModelAndView saveLocInitType(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.saveLocInitType(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : saveLocInitTypeV2
	 * Method 설명      : 로케이션 추천 WorkId 초기화
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/saveLocInitTypeV2.action")
	public ModelAndView saveLocInitTypeV2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.saveLocInitTypeV2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listLoc
	 * Method 설명      : 로케이션지정 목록
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/listLoc.action")
	public ModelAndView listLoc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listLoc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listLocV2
	 * Method 설명      : 로케이션지정 목록(건별 통합)
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/listLocV2.action")
	public ModelAndView listLocV2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listLocV2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : listLocSub
	 * Method 설명      : 로케이션 Sub 목록
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/listLocSub.action")
	public ModelAndView listLocSub(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listLocSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : locAutoInLoc
	 * Method 설명      : 입고 로케이션 추천
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/locAutoInLoc.action")
	public ModelAndView locAutoInLoc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateLocAutoIn(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : locAutoOutLoc
	 * Method 설명      : 출고 로케이션 추천
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/locAutoOutLoc.action")
	public ModelAndView locAutoOutLoc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateLocAutoOut(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : saveLoc
	 * Method 설명      : 로케이션 지정 등록,수정
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/saveLoc.action")
	public ModelAndView saveLoc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveLoc(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : deleteLoc
	 * Method 설명      : 로케이션  지정 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/deleteLoc.action")
	public ModelAndView deleteLoc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteLoc(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : saveLocChangeWorkQty
	 * Method 설명      : 입고수량 변경
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q5/saveLocChangeWorkQty.action")
	public ModelAndView saveLocChangeWorkQty(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveLocChangeWorkQty(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop000Q5
	 * Method 설명      : 작업일 변경 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q6.action")
	public ModelAndView wmsop000Q6(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP000Q6");
	}

	/*-
	 * Method ID    : saveSetWorkDate
	 * Method 설명      : 작업일 변경
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q6/saveSetWorkDate.action")
	public ModelAndView saveSetWorkDate(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSetWorkDate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save work date :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop000Q7
	 * Method 설명      : 로케이션지정 화면2
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q7.action")
	public ModelAndView wmsop000Q7(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP000Q7", service.selectData(model));
	}

	/*-
	 * Method ID    : setLocAutoInLoc
	 * Method 설명      : 입고 로케이션 추천2
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q7/setLocAutoInLoc.action")
	public ModelAndView setLocAutoInLoc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateSetLocAutoIn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to set loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : setLocAutoOutLoc
	 * Method 설명      : 출고 로케이션 추천2
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q7/setLocAutoOutLoc.action")
	public ModelAndView setLocAutoOutLoc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateSetLocAutoOut(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to set loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : wmsop000Q8
	 * Method 설명      : 간편배차
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q8.action")
	public ModelAndView wmsop000Q8(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP000Q8");
	}

	/*-
	 * Method ID    : setLocAutoInLoc
	 * Method 설명      : 입고 로케이션 추천2
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q8/list.action")
	public ModelAndView batchList(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateSetLocAutoIn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to set loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSOP000Q9.action")
	public ModelAndView WMSOP000Q9(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP000Q9");
	}
	
	
	
	
	/*-
	 * Method ID    : wmsop000Q10
	 * Method 설명      : 운송사 지정 화면
	 * 작성자                 : yhku 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q10.action")
	public ModelAndView wmsop000Q10(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/WMSOP000Q10");
	}
	
	
	/*-
	 * Method ID    : listTransporDsp
	 * Method 설명      : 운송사 조회
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP000Q10/listTransporDsp.action")
	public ModelAndView listTransporDsp(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			
			mav = new ModelAndView("jqGridJsonView", service.listTransporDsp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : listTransCustIdDsp
	 * Method 설명      : 운송사 조회
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP000Q10/listTransCustIdDsp.action")
	public ModelAndView listTransCustIdDsp(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			
			mav = new ModelAndView("jqGridJsonView", service.listTransCustIdDsp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : wmsop000Q11
	 * Method 설명      : 운송장 미지정내역 화면
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q11.action")
	public ModelAndView wmsop000Q11(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/wmsop000Q11");
	}
	
	/*-
	 * Method ID    : wmsop000Q12
	 * Method 설명      : 상품별 현재고 조회
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q12.action")
	public ModelAndView wmsop000Q12(Map<String, Object> model) {
		return new ModelAndView("winus/wmsop/wmsop000Q12");
	}

	
	/*-
	 * Method ID    : saveTransDsp
	 * Method 설명      : 운송사 내역 저장
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP000Q10/saveTransDsp.action")
	public ModelAndView saveTransDsp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveTransDsp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	
	
}
