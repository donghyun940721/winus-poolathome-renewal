package com.logisall.winus.wmsop.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsop.service.WMSOP020Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSOP020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSOP020Service")
	private WMSOP020Service service;

	static final String[] COLUMN_NAME_WMSOP020E8 = {
		"I_ORD_ID", "I_ORD_SEQ", "I_LOC_CD"
	};

	static final String[] COLUMN_NAME_WMSOP020E8V2 = {
		"I_ORD_ID", "I_ORD_SEQ", "I_RITEM_CD","I_RITEM_NM","I_IN_ORD_QTY","I_LOC_CD"
	};
	
	/*-
	 * Method ID    : wmsop020
	 * Method 설명      : 입고관리 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSOP020.action")
	public ModelAndView wmsop020(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP020", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP120.action")
	public ModelAndView wmsop120(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP120", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP121.action")
	public ModelAndView wmsop121(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP121", service.selectItemGrp(model));
	}
	@RequestMapping("/WINUS/WMSOP122.action")
	public ModelAndView wmsop122(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP122", service.selectItemGrp(model));
	}

	/*-
	 * Method ID    : list
     * Method 설명      : 입고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception  
	 */
	@RequestMapping("/WMSOP020/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	* Method ID    : lcinfolist
	* Method 占썬�삼옙      : 占쏙옙�⑨옙�울옙�깍옙 占썩�뤄옙
	* 占쏙옙占쎄�占쏙옙                 : chSong
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSOP020/lcinfolist.action")
	public ModelAndView lcinfolist(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.lcinfolist(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get lcinfolist :", e);
			}
		}
		return mav;
	}
	
	/*-
	* Method ID    : inOrderCntInit
	* Method 占썬�삼옙      : 
	* 占쏙옙占쎄�占쏙옙                 : chSong
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSOP020/inOrderCntInit.action")
	public ModelAndView inOrderCntInit(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.inOrderCntInit(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get lcinfolist :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : deleteOrder
     * Method 설명      : 주문삭제
     * 작성자                 : chSong
     * @param   model
     * @return   
	 */
	@RequestMapping("/WMSOP020/deleteOrder.action")
	public ModelAndView deleteOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : saveInvcNoOrder
     * Method 설명      : 반품송장 저장. 
     * 작성자                 : yhku (11.25이관) 
     * @param   model
     * @return   
	 */
	@RequestMapping("/WMSOP020/saveInvcNoOrder.action")
	public ModelAndView saveInvcNoOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInvcNoOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	
	
	/*-
	 * Method ID    : listExcel
     * Method 설명      : 엑셀다운로드
     * 작성자                 : chsong
     * @param model
     * @return
	 */
	@RequestMapping("/WMSOP020/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID    : listExcel(PLT)
     * Method 설명      : 엑셀다운로드(PLT)
     * 작성자                 : smics
     * @param model
     * @return
	 */
	@RequestMapping("/WMSOP020/excelPlt.action")
	public void listExcelPlt(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelPlt(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 占썬�삼옙 : 占쏙옙占쏙옙占쏙옙占쏙옙 占썬�쇽옙��占쏙옙占�
	 * 占쏙옙占쎄�占쏙옙 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
         try{
        	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
             String[][] headerEx = {
				                     {MessageResolver.getText("작업상태")   , "0", "0", "0", "0", "100"}
				                    ,{MessageResolver.getText("주문번호")   , "1", "1", "0", "0", "100"}
				                    ,{MessageResolver.getText("주문SEQ")   , "2", "2", "0", "0", "100"}
				                    ,{MessageResolver.getText("LOT NO")   , "3", "3", "0", "0", "100"}
				                    ,{MessageResolver.getText("로케이션")   , "4", "4", "0", "0", "100"}
				                    ,{MessageResolver.getText("입고예정일")   , "5", "5", "0", "0", "100"}
				                    ,{MessageResolver.getText("입고일자")   , "6", "6", "0", "0", "100"}
				                    ,{MessageResolver.getText("상품코드")   , "7", "7", "0", "0", "100"}
				                    ,{MessageResolver.getText("상품명")   , "8", "8", "0", "0", "100"}
				                    ,{MessageResolver.getText("주문량")   , "9", "9", "0", "0", "100"}
				                    ,{MessageResolver.getText("주문중량")   , "10", "10", "0", "0", "100"}
				                    ,{MessageResolver.getText("입고량")   , "11", "11", "0", "0", "100"}
				                    ,{MessageResolver.getText("입고중량")   , "12", "12", "0", "0", "100"}
				                    ,{MessageResolver.getText("UOM")   , "13", "13", "0", "0", "100"}
				                    ,{MessageResolver.getText("화주")   , "14", "14", "0", "0", "100"}
				                    ,{MessageResolver.getText("입고처")   , "15", "15", "0", "0", "100"}
				                    ,{MessageResolver.getText("PLT수량")   , "16", "16", "0", "0", "100"}
				                    ,{MessageResolver.getText("BOX수량")   , "17", "17", "0", "0", "100"}
				                    ,{MessageResolver.getText("정상")   , "18", "18", "0", "0", "100"}
				                    ,{MessageResolver.getText("비고")   , "19", "19", "0", "0", "100"}
				                    ,{MessageResolver.getText("비고")   , "20", "20", "0", "0", "100"}
				                    ,{MessageResolver.getText("유효기간")   , "21", "21", "0", "0", "100"}
				                    ,{MessageResolver.getText("컨테이너번호")   , "22", "22", "0", "0", "100"}
				                    ,{MessageResolver.getText("상품유효기간")   , "23", "23", "0", "0", "100"}
				                    ,{MessageResolver.getText("상품유효기간만료일")   , "24", "24", "0", "0", "100"}
				                    ,{MessageResolver.getText("배차내역")   , "25", "25", "0", "0", "100"}
				                    ,{MessageResolver.getText("배차확정여부")   , "26", "26", "0", "0", "100"}
				                    ,{MessageResolver.getText("PDA작업여부")   , "27", "27", "0", "0", "100"}
				                    ,{MessageResolver.getText("PDA작업상태")   , "28", "28", "0", "0", "100"}
				                    ,{MessageResolver.getText("매핑여부")   , "29", "29", "0", "0", "100"}
				                    ,{MessageResolver.getText("BL번호")   , "30", "30", "0", "0", "100"}
				                    ,{MessageResolver.getText("원주문번호")   , "31", "31", "0", "0", "100"}
				                    ,{MessageResolver.getText("주문구분")   , "32", "32", "0", "0", "100"}
				                    ,{MessageResolver.getText("금액")   , "33", "33", "0", "0", "100"}
				                   };
           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
             String[][] valueName = {
                                     {"WORK_STAT", "S"},
                                     {"VIEW_ORD_ID", "S"},
                                     {"ORD_SEQ", "S"},
                                     {"CUST_LOT_NO", "S"},
                                     {"LOC_CD", "S"},
                                     {"IN_REQ_DT", "S"},
                                     {"IN_DT", "S"},
                                     {"RITEM_CD", "S"},
                                     {"RITEM_NM", "S"},
                                     {"IN_ORD_QTY", "N"},
                                     {"IN_ORD_WEIGHT", "S"},
                                     {"REAL_IN_QTY", "N"},
                                     {"REAL_IN_WEIGHT", "S"},
                                     {"IN_ORD_UOM_CD", "S"},
                                     {"CUST_NM", "S"},
                                     {"IN_CUST_NM", "S"},
                                     {"REAL_PLT_QTY", "N"},
                                     {"REAL_BOX_QTY", "N"},
                                     {"ORD_SUBTYPE", "S"},
                                     {"ORD_DESC", "S"},
                                     {"ETC2", "S"},
                                     {"VALID_DT", "S"},
                                     {"CNTR_NO", "S"},
                                     {"ITEM_BEST_DATE", "S"},
                                     {"ITEM_BEST_DATE_END", "S"},
                                     {"CAR_CD", "S"},
                                     {"CAR_CONF_YN", "S"},
                                     {"PDA_FINISH_YN", "S"},
                                     {"PDA_STAT", "S"},
                                     {"MAPPING_YN", "S"},
                                     {"BL_NO", "S"},
                                     {"ORG_ORD_ID", "S"},
                                     {"ORD_SUBTYPE_NM", "S"},
                                     {"PRICE", "NR"}
                                    }; 
             
             //파일명
             String fileName = MessageResolver.getText("입고관리");
             //시트명
             String sheetName = "Sheet1";
             //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
             String marCk = "N";
             //ComUtil코드
             String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : saveConfirmGrn
     * Method 설명      : 입하확정
     * 작성자                 : chSong
     * @param   model
     * @return   
	 */
	@RequestMapping("/WMSOP020/saveConfirmGrn.action")
	public ModelAndView saveConfirmGrn(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveConfirmGrn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save confirm :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : saveCancelGrn
     * Method 설명      : 입하취소
     * 작성자                 : chSong
     * @param   model
     * @return   
	 */
	@RequestMapping("/WMSOP020/saveCancelGrn.action")
	public ModelAndView saveCancelGrn(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCancelGrn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save confirm :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveInComplete
     * Method 설명      : 입고확정
     * 작성자                 : chSong
     * @param   model
     * @return  
	 */
	@RequestMapping("/WMSOP020/saveInComplete.action")
	public ModelAndView saveInComplete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInComplete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save confirm :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveInCompleteOutOrder
     * Method 설명      : 입고확정 후 출고오더 생성
     * 작성자                 : chSong
     * @param   model
     * @return  
	 */
	@RequestMapping("/WMSOP020/saveInCompleteOutOrder.action")
	public ModelAndView saveInCompleteOutOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveInCompleteOutOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save confirm :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : saveCancelInComplete
     * Method 설명      : 입고확정취소
     * 작성자                 : MonkeySeok
     * @param   model
     * @return  
	 */
	@RequestMapping("/WMSOP020/saveCancelInComplete.action")
	public ModelAndView saveCancelInComplete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCancelInComplete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save confirm :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveSimpleIn
	 * Method 설명      : 간편입고저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return 
	 */
	@RequestMapping("/WMSOP020/saveSimpleIn.action")
	public ModelAndView saveSimpleIn(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSimpleIn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : saveSimpleIn
     * Method 설명      : 템플릿 유효성검사
     * 작성자                 : chSong
     * @param   model
     * @return    
	 */
	@RequestMapping("/WMSOP020/chkTest.action")
	public ModelAndView chkTest(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.checkTest(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to check :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsop020E7
     * Method 설명      : 입고 주문 자동생성화면
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
	 */
	@RequestMapping("/WMSOP020E7.action")
	public ModelAndView wmsop020E7(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP020E7");
	}

	/*-
	 * Method ID    : listInOrderItemASN
     * Method 설명      : 입고관리ASN  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
	 */
	@RequestMapping("/WMSOP020E7/listInOrderItemASN.action")
	public ModelAndView listInOrderItemASN(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.listInOrderItemASN(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveAsnOrder
     * Method 설명      : 입고 ASN 저장
     * 작성자                 : chSong
     * @param   model
     * @return  
	 */
	@RequestMapping("/WMSOP020E7/saveAsnOrder.action")
	public ModelAndView saveInOrder(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveAsnOrder(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save asn order :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}  
	
	/*-
	 * Method ID    : ifInOrdLocMapp
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP020/ifInOrdLocMapp.action")
	public ModelAndView ifInOrdLocMapp(Map<String, Object> model) {

		if (log.isInfoEnabled()) {
			log.info(model);
		}

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.ifInOrdLocMapp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP020/listExcel2.action")
	public void excelDown2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.getExcelDown2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
		try {
			int iColSize = Integer.parseInt(model.get("Col_Size").toString());
			int iHRowSize = 6; // 고정값
			int iVRowSize = 2; // 고정값
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			String[][] headerEx = new String[iColSize][iHRowSize];
			String[][] valueName = new String[iColSize][iVRowSize];
			for (int i = 0; i < iColSize; i++) {
				String nName = MessageResolver.getText((String) model.get("Name_" + i)).replace("&lt;br&gt;", "").replace("&lt;/font&gt;&lt;br&gt;", "").replace("&lt;/font&gt;", "").replace("&lt;font color=&quot;#B1B1B1&quot;&gt;", "");
				// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
				headerEx[i] = new String[]{nName, i + "", i + "", "0", "0", "100"};
				// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
				// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
				valueName[i] = new String[]{(String) model.get("Col_" + i), "S"};
			}
			// 파일명
			String fileName = MessageResolver.getText("템플릿로케이션지정");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	
	/*-
	 * Method ID    : autoBestLocSave
	 * Method 설명      : 아산물류센터 로케이션추천 자동
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP020/autoBestLocSave.action")
	public ModelAndView autoBestLocSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoBestLocSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : WMSOP020E8
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSOP020E8.action")
	public ModelAndView WMSOP020E8(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP020E8");
	}
	
	/*-
	 * Method ID : uploadLcoInfo
	 * Method 설명 : 엑셀파일업로드
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param txtFile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSOP020E8/uploadLcoInfo.action")
	public ModelAndView uploadLcoInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = null;
			if(model.containsKey("vrFromTemplateId") && model.get("vrFromTemplateId").equals("WMSOP910E1")){
				list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP020E8V2, 0, startRow, 10000, 0);
			}
			else{
				list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSOP020E8, 0, startRow, 10000, 0);
			}
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	* Method ID    : asnOutOrd
	* Method 설명      : 
	* 작성자                 : chSong
	* @param   model
	* @return  
	*/
	@RequestMapping("/WMSOP020/asnOutOrd.action")
	public ModelAndView asnOutOrd(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.asnOutOrd(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : WMSOP020E9
	 * Method 설명          : PDF 뷰어 화면(팝업) 
	 * 작성자                          : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP020E9.action")
	public ModelAndView WMSOP030E10(Map<String, Object> model) {
		return new ModelAndView("winus/WMSOP/WMSOP020E9");
	}
	
	/*-
	 * Method ID    : /WMSOP020pop.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSOP020pop.action")
	public ModelAndView wmsop020pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsop/WMSOP020pop");
	}
	
	/*-
	 * Method ID	: autoFixLocSave
	 * Method 설명	: 입고 Fix 로케이션 추천지정
	 * 작성자			: chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSOP020/autoFixLocSave.action")
	public ModelAndView autoFixLocSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.autoFixLocSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : simpleInWorkingReset
	 * Method 설명   : 운영관리 > 입고관리 > 작업초기화
	 * 작성자     : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSOP020/simpleInWorkingReset.action")
	public ModelAndView simpleInWorkingReset(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.simpleInWorkingReset(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("ERROR :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
