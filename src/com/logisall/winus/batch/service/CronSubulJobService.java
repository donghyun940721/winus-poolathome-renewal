package com.logisall.winus.batch.service;

import java.util.Map;

public interface CronSubulJobService {

	public Map<String, Object> runSpJob(Map<String, Object> model) throws Exception;	
}
