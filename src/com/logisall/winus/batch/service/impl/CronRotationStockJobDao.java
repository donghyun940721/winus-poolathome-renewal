package com.logisall.winus.batch.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("cronRotationStockJobDao")
public class CronRotationStockJobDao extends SqlMapAbstractDAO{
	
	Log log = LogFactory.getLog(this.getClass());

    public Object runSpRotationalStockTakingJob(Map<String, Object> model){
        executeUpdate("pk_wmsjb000.sp_rotational_stock_taking", model);
        return model;
    }    
}
