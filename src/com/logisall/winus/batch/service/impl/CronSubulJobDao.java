package com.logisall.winus.batch.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("cronSubulJobDao")
public class CronSubulJobDao extends SqlMapAbstractDAO{
    
    public Object runSpFillStockAllJob(Map<String, Object> model){
        executeUpdate("pk_wmsjb000.sp_fillstockall_job", model);
        return model;
    }    
}
