package com.logisall.winus.batch.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.batch.service.CronSubulJobService;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;



@Service("cronSubulJobService")
@SuppressWarnings("PMD")
public class CronSubulJobServiceImpl extends AbstractServiceImpl implements CronSubulJobService {
	
	Log logger = LogFactory.getLog(this.getClass());
	
	@Resource(name="cronSubulJobDao")
	private CronSubulJobDao dao;
	    
	public Map<String, Object> runSpJob(Map<String, Object> model) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();	
		
		try {
			String workDate = DateUtil.getCurrentDate("yyyyMMdd");
			String subulDate = DateUtil.getTargetDate(workDate, -1);
			
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("SUBUL_DT", subulDate);
            //dao                
            modelIns = (Map<String, Object>)dao.runSpFillStockAllJob(modelIns);
            
            if ( logger.isInfoEnabled() ) {
            	logger.info("[CronSubulJob] Make Subul Job Data FOR SUBUL DATE : " + subulDate);
            	logger.info("[CronSubulJob] START... ");
            }
            ServiceUtil.isValidReturnCode("CronQuartzJob", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
    		map.put("errCnt", 0);
    		// map.put("MSG", MessageResolver.getMessage("save.success"));
            if ( logger.isInfoEnabled() ) {
            	logger.info("[CronSubulJob] END... (Success)");
            }
    		
        } catch(BizException be) {
            map.put("errCnt", 1);
            map.put("MSG", be.getMessage() );
            
            if ( logger.isInfoEnabled() ) {
            	logger.info("[CronSubulJob] END... (Error Occured) :" + be.getMessage() );
            }            
            
		}catch (Exception e) {
		    if ( logger.isInfoEnabled() ) {
            	logger.info("[CronSubulJob] END... (Error Occured) :" + e.getMessage() );
            }  
		    throw e;
        }
		return map;
	}
}
