package com.logisall.winus.batch.service.impl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.logisall.winus.batch.service.QuartzJobService;



public class QuartzJobServiceImpl implements QuartzJobService {

	Log log = LogFactory.getLog(this.getClass());
	
	@Override
	public void printLog() {
		if ( log.isDebugEnabled() ) {
			log.debug("test log");
		}
	}
	
}
