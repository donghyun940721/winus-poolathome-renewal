package com.logisall.winus.batch.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.batch.service.CronRotationStockJobService;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;



@Service("cronRotationStockJobService")
@SuppressWarnings("PMD")
public class CronRotationStockJobServiceImpl extends AbstractServiceImpl implements CronRotationStockJobService {
	
	Log logger = LogFactory.getLog(this.getClass());
	
	@Resource(name="cronRotationStockJobDao")
	private CronRotationStockJobDao dao;
	    
	public Map<String, Object> runSpJob(Map<String, Object> model) throws Exception{
		Map<String, Object> map = new HashMap<String, Object>();	
		
		try {
			String workDate = DateUtil.getCurrentDate("yyyyMMdd");
            Map<String, Object> modelIns = new HashMap<String, Object>();
            //session 및 등록정보
            modelIns.put("WORK_DT", workDate);
            modelIns.put("WORK_NO", "BATCH");
            //dao                
            modelIns = (Map<String, Object>)dao.runSpRotationalStockTakingJob(modelIns);
            
            if ( logger.isInfoEnabled() ) {
            	logger.info("[CronQuartzJob] Make Rotational Stock Taking Data FOR WORK DATE : " + workDate);
            	logger.info("[CronQuartzJob] START... ");
            }
            ServiceUtil.isValidReturnCode("CronQuartzJob", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
    		map.put("errCnt", 0);
    		// map.put("MSG", MessageResolver.getMessage("save.success"));
            if ( logger.isInfoEnabled() ) {
            	logger.info("[CronQuartzJob] END... (Success)");
            }
    		
        } catch(BizException be) {
            map.put("errCnt", 1);
            map.put("MSG", be.getMessage() );
            
            if ( logger.isInfoEnabled() ) {
            	logger.info("[CronQuartzJob] END... (Error Occured) :" + be.getMessage() );
            }            
            
		}catch (Exception e) {
		    if ( logger.isInfoEnabled() ) {
            	logger.info("[CronQuartzJob] END... (Error Occured) :" + e.getMessage() );
            }  
		    throw e;
        }
		return map;
	}
}
