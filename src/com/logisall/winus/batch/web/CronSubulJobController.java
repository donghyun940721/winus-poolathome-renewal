package com.logisall.winus.batch.web;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Controller;

import com.logisall.winus.batch.service.CronSubulJobService;



@Controller
public class CronSubulJobController extends QuartzJobBean{

	CronSubulJobService cronSubulJobService;
	
	protected Log log = LogFactory.getLog(this.getClass());
	
    @Autowired
    public void setCronSubulJobService(CronSubulJobService cronSubulJobService) {
        this.cronSubulJobService = cronSubulJobService;
    }

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
//		
//		Map<String, Object> model = new HashMap<String, Object>();
//		try {
//			cronSubulJobService.runSpJob(model);
//		}catch(Exception e){
//        	if (log.isErrorEnabled()) {
//				log.error("Fail to Subul Job Service :", e);
//			}
//		}
	}
	
}
