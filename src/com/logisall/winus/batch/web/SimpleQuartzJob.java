package com.logisall.winus.batch.web;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.logisall.winus.batch.service.QuartzJobService;




public class SimpleQuartzJob extends QuartzJobBean {
	
	private QuartzJobService quartzJobService;

	public void setQuartzJobService(QuartzJobService quartzJobService) {
		this.quartzJobService = quartzJobService;
		
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0)
			throws JobExecutionException {
		quartzJobService.printLog();
	}

}
