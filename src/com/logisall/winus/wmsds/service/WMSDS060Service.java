package com.logisall.winus.wmsds.service;

import java.util.Map;

public interface WMSDS060Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> detail(Map<String, Object> model) throws Exception;
	public Map<String, Object> detailItemlist(Map<String, Object> model) throws Exception;
	public Map<String, Object> familyList(Map<String, Object> model) throws Exception;
	public Map<String, Object> familyDetailList(Map<String, Object> model) throws Exception;
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
