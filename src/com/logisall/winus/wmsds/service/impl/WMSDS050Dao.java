package com.logisall.winus.wmsds.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDS050Dao")
public class WMSDS050Dao extends SqlMapAbstractDAO{

		/**
     * Method ID : list
     * Method 설명 : 배송 통계 리스트
     * 작성자 : 공통
     * @param model
     * @return
     */
	public List list(Map<String, Object> model) {
		return executeQueryForList("wmsds050.list", model);
	}    
	/**
	 * Method ID : listSum
	 * Method 설명 : 배송 통계 리스트
	 * 작성자 : 공통
	 * @param model
	 * @return
	 */
	public List listSum(Map<String, Object> model) {
		return executeQueryForList("wmsds050.listSum", model);
	}    
    
	/**
	 * Method ID : driverList
	 * Method 설명 : 기사 통계 리스트
	 * 작성자 : 공통
	 * @param model
	 * @return
	 */
	public List driverList(Map<String, Object> model) {
		return executeQueryForList("wmsds050.driverList", model);
	}    

	/**
	 * Method ID : driverList
	 * Method 설명 : 기사 통계 리스트
	 * 작성자 : 공통
	 * @param model
	 * @return
	 */
	public List driverNmList(Map<String, Object> model) {
		return executeQueryForList("wmsds050.driverNmList", model);
	}
	/**
	 * Method ID : driverList
	 * Method 설명 : 기사 통계 리스트
	 * 작성자 : 공통
	 * @param model
	 * @return
	 */
	public List dateList(Map<String, Object> model) {
		return executeQueryForList("wmsds050.dateList", model);
	}
	
	public List selectDriverList(Map<String, Object> model) {
		return executeQueryForList("wmsds050.selectDriverList", model);
	}
	public List<Map<String, Object>> custList(Map<String, Object> model) {
		return executeQueryForList("wmsds050.custNmList", model);
	}
	public List averageList(Map<String, Object> model) {
		return executeQueryForList("wmsds050.averageList", model);
	}
	public List<Map<String, Object>> driverListSum(Map<String, Object> model) {
		return executeQueryForList("wmsds050.driverListSum", model);
	}
	public List averageListSum(Map<String, Object> model) {
		return executeQueryForList("wmsds050.averageListSum", model);
	}
}
