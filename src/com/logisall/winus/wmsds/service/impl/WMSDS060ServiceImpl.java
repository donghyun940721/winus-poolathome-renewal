package com.logisall.winus.wmsds.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsct.service.impl.WMSSP010Dao;
import com.logisall.winus.wmsst.service.impl.WMSST230Dao;
import com.logisall.winus.wmsds.service.WMSDS060Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
	
@Service("WMSDS060Service")
public class WMSDS060ServiceImpl implements WMSDS060Service {
    protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSDS060Dao")
    private WMSDS060Dao dao;

    /**
     * Method ID : list
     * Method 설명 : RDC 배송통계 리스트 
     * 작성자 : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
        	if(model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if(model.get("rows") == null) {
                model.put("pageSize", "200");
            } else {
                model.put("pageSize", model.get("rows"));
            }
        	map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	/**
	 * Method ID : detailItemlist
	 * Method 설명 : RDC 배송통계 리스트 
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> detailItemlist(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if(model.get("page") == null) {
				model.put("pageIndex", "1");
			} else {
				model.put("pageIndex", model.get("page"));
			}
			if(model.get("rows") == null) {
				model.put("pageSize", "100");
			} else {
				model.put("pageSize", model.get("rows"));
			}
			map.put("LIST", dao.detailItemlist(model));
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}
	/**
     * Method ID : detail
     * Method 설명 : RDC 배송통계 리스트 
     * 작성자 : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> detail(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
        	map.put("DETAIL", dao.detail(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	 /**
     * Method ID : familyList
     * Method 설명 : RDC 배송통계 리스트 
     * 작성자 : KHKIM
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> familyList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
        	if(model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if(model.get("rows") == null) {
                model.put("pageSize", "200");
            } else {
                model.put("pageSize", model.get("rows"));
            }
        	map.put("LIST", dao.familyList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	/**
	 * Method ID : familyDetailList
	 * Method 설명 : RDC 배송통계 리스트 
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> familyDetailList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			if(model.get("page") == null) {
				model.put("pageIndex", "1");
			} else {
				model.put("pageIndex", model.get("page"));
			}
			if(model.get("rows") == null) {
				model.put("pageSize", "200");
			} else {
				model.put("pageSize", model.get("rows"));
			}
			map.put("LIST", dao.familyDetailList(model));
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}
	
	

    /**
     * Method ID : listExcel
     * Method 설명 : 임가공 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listExcel(model));
        
        return map;
    }
	
}