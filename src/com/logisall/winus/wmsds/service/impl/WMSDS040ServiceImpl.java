package com.logisall.winus.wmsds.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsds.service.WMSDS040Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;


@Service("WMSDS040Service")
public class WMSDS040ServiceImpl implements WMSDS040Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSDS040Dao")
    private WMSDS040Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSDS040 = {"DLV_ORD_ID", "SALES_CUST_ID"};
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        //map.put("DRIVERS", dao.selectDrivers(model));
        //map.put("ITEMGRP", dao.selectItem(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 고객관리 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        List<String> vrSrchRitemIdArr = new ArrayList();
        String[] spVrSrchRitemIdLike = model.get("vrSrchRitemId").toString().split(",");
        for (String keyword : spVrSrchRitemIdLike ){
        	vrSrchRitemIdArr.add(keyword);
        }
        model.put("vrSrchRitemIdArr", vrSrchRitemIdArr);
        
        map.put("LIST", dao.list(model));
        return map;
    }
}
