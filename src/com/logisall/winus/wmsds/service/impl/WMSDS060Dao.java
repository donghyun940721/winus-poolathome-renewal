package com.logisall.winus.wmsds.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDS060Dao")
public class WMSDS060Dao extends SqlMapAbstractDAO{

		/**
     * Method ID : list
     * Method 설명 : 배송 통계 리스트
     * 작성자 : 공통
     * @param model
     * @return
     */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsds060.list", model);
	}
	
	/**
     * Method ID : list
     * Method 설명 : 배송 통계 리스트
     * 작성자 : 공통
     * @param model
     * @return
     */
	public Object detail(Map<String, Object> model) {
		return executeQueryForObject("wmsds060.detail", model);
	}

	public GenericResultSet detailItemlist(Map<String, Object> model) {
		return executeQueryPageWq("wmsds060.detailItemList", model);
	}

	public GenericResultSet familyList(Map<String, Object> model) {
		return executeQueryPageWq("wmsds060.familyList", model);
	}
	public GenericResultSet familyDetailList(Map<String, Object> model) {
		return executeQueryPageWq("wmsds060.familyDetailList", model);
	}

	public GenericResultSet listExcel(Map<String, Object> model) {
		GenericResultSet genericResultSet = new GenericResultSet();
		List list = executeQueryForList("wmsds060.listExcel", model);
		genericResultSet.setList(list);
		return genericResultSet;
		
	}

	
}
