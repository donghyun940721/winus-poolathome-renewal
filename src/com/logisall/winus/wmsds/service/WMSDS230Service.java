package com.logisall.winus.wmsds.service;

import java.util.Map;


public interface WMSDS230Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> getSampleExcelDown(Map<String, Object> model) throws Exception;
}
