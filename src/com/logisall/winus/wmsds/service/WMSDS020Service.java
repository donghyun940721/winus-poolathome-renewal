package com.logisall.winus.wmsds.service;

import java.util.Map;

public interface WMSDS020Service {
	public Map<String, Object> selectLc(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
