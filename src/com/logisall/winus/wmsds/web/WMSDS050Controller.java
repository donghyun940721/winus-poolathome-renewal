package com.logisall.winus.wmsds.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSSP010Service;
import com.logisall.winus.wmsst.service.WMSST230Service;
import com.logisall.winus.wmsds.service.WMSDS050Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSDS050Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSDS050Service")
	private WMSDS050Service service;
	
	/*-
	 * Method ID : 
	 * Method 설명 : 배차통계
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSDS050.action")
	public ModelAndView wmsds050(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsds/WMSDS050", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : STG710E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS050/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : TG700E02
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS050/driverList.action")
	public ModelAndView driverList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.driverList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : 
	 * Method 설명 : 평균 배송일 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS050/averageList.action")
	public ModelAndView averageList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.averageList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : TG710
	 * Method 설명 : 콤보박스 기사리스트
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDS050/selectDriverList.action")
	public ModelAndView selectDriverList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.selectDriverList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	
}