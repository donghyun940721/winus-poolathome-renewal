package com.logisall.winus.wmsmo.service;

import java.util.List;
import java.util.Map;

public interface WMSMO502Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
}
