package com.logisall.winus.wmsmo.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO202Dao")
public class WMSMO202Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID  : selectPoolGrp
     * Method 설명  : 화면내 필요한 용기군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectPoolGrp(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 개체재고및이력추적 개체재고리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsmo202.list", model);
    }   

    /**
     * Method ID    : insert
     * Method 설명      : 물류용기관리 등록
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model){ //wmsmo202.insert
        executeUpdate("wmsmo202.PK_WMSOP030.SP_RTI_OUT_COMPLETE", model);
        return model;
    }
    
    /**
     * Method ID  : selectCust
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectCust(Map<String, Object> model){
        return executeQueryForList("wmsmo201.selectCust", model);
    }
    
    /**
    * Method ID  : selectPool
    * Method 설명  : Zone 데이터셋
    * 작성자             : 기드온
    * @param model
    * @return
    */
    public Object selectPool(Map<String, Object> model){
    return executeQueryForList("wmsmo907.selectPool", model);
    }
}
