package com.logisall.winus.wmsmo.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsmo.service.WMSMO030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMO030Service")
public class WMSMO030ServiceImpl implements WMSMO030Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMO030Dao")
    private WMSMO030Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 출고RFID관리  조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * Method ID   : detailList
     * Method 설명    : 출고RFID관리  상세조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.detailList(model));
        return map;
    }
    
    /**
     * Method ID   : save
     * Method 설명    : 출고RFID관리 저장
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] ordId      = new String[totCnt];
            String[] ordSeq     = new String[totCnt];
            String[] workQty    = new String[totCnt];
            String[] rtiEpcCd   = new String[totCnt];
            String[] workDt     = new String[totCnt];
            String[] workWgt    = new String[totCnt];
            
            for(int i = 0 ; i < totCnt ; i++){
                ordId[i]    = (String)model.get("ORD_ID"+i);               
                ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                workQty[i]  = (String)model.get("WORK_QTY"+i);               
                rtiEpcCd[i] = (String)model.get("RTI_EPC_CD"+i);   
                workDt[i]   = (String)model.get("WORK_DT"+i);    
                workWgt[i]  = (String)model.get("WORK_WGT"+i);
            }
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("ordId"    , ordId);
            modelIns.put("ordSeq"   , ordSeq);
            modelIns.put("workQty"  , workQty);
            modelIns.put("rtiEpcCd" , rtiEpcCd);
            modelIns.put("workDt"   , workDt);
            modelIns.put("workWgt"  , workWgt);
            
            modelIns.put("lcId"     , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("workIp"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("userNo"   , (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao.save(modelIns);
            ServiceUtil.isValidReturnCode("WMSMO030", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
}
