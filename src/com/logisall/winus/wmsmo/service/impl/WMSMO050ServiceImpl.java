package com.logisall.winus.wmsmo.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsmo.service.WMSMO050Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMO050Service")
public class WMSMO050ServiceImpl implements WMSMO050Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMO050Dao")
    private WMSMO050Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 매핑  조회
     * 작성자               : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * Method ID   : detailList
     * Method 설명    : 매핑  상세조회
     * 작성자               : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.detailList(model));
        return map;
    }
    
    /**
     * Method ID   : save
     * Method 설명    : 출고RFID관리 저장
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] rtiEpcCd   = new String[totCnt];
            String[] ritemId   = new String[totCnt];
            Map<String, Object> modelIns = new HashMap<String, Object>();
            for(int i = 0 ; i < totCnt ; i++){
                ritemId[i]  = (String)model.get("RITEM_ID"+i);
                rtiEpcCd[i] = (String)model.get("RTI_EPC_CD"+i);   
                modelIns.put("I_PDA_NO"   , (String)model.get("PDA_NO"+i));
                modelIns.put("I_EVENT_DT"   , (String)model.get("WORK_DT"+i));
                modelIns.put("I_TRNS_STAT_CD"   , (String)model.get("TRNS_STAT_CD"+i));
                modelIns.put("I_EVENT_CD"   , (String)model.get("EVENT_CD"+i));
                modelIns.put("I_PDA_STAT"   , (String)model.get("PDA_STAT"+i));
            }
            
            modelIns.put("I_RITEM_ID" , ritemId);
            modelIns.put("I_RTI_EPC_CD" , rtiEpcCd);
            modelIns.put("I_LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
              
            modelIns = (Map<String, Object>)dao.save(modelIns);
            ServiceUtil.isValidReturnCode("WMSMO040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
}
