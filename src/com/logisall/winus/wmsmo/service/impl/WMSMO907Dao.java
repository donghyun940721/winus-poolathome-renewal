package com.logisall.winus.wmsmo.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO907Dao")
public class WMSMO907Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectPool
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmsmo907.selectPool", model);
    }
    
    /**
     * Method ID  : selectCust
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectCust(Map<String, Object> model){
        return executeQueryForList("wmsmo907.selectCust", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 물류용기관리 등록
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model){
        executeUpdate("wmsmo907.PK_WMSST040.PC_HHT_SAV_MOVESTOCK", model);
        return model;
    }
        
    /**
     * Method ID  : list
     * Method 설명  : 물류용기관리 조회
     * 작성자             : 기드온
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsmo907.list", model);
    }
    
    /**
     * Method ID  : selectLotLike
     * Method 설명  : Lot 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectLotLike(Map<String, Object> model){
        return executeQueryForList("wmsst040.selectLotLike", model);
    }

}
