package com.logisall.winus.wmsmo.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsmo.service.WMSMO908Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMO908Service")
public class WMSMO908ServiceImpl implements WMSMO908Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMO908Dao")
    private WMSMO908Dao dao;

    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    @Override
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.detailList(model));
        return map;
    }
    
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());           
            String[] custLotNo     = new String[totCnt];
            String[] realOutQty    = new String[totCnt];
           
            for(int i = 0 ; i < totCnt ; i++){
            	 
            	custLotNo[0]   = (String)model.get("CUST_LOT_NO"+i);
            	realOutQty[0]  = (String)model.get("REAL_OUT_QTY"+i);

                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("LC_ID"        , (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("RTI_EPC_CD"   , (String)model.get("RTI_EPC_CD"+i));
                modelIns.put("CUST_LOT_NO"  , custLotNo);
                modelIns.put("REAL_OUT_QTY" , realOutQty);
                modelIns.put("USER_NO"      , (String)model.get(ConstantIF.SS_USER_NO));

                modelIns = (Map<String, Object>)dao.save(modelIns);
                ServiceUtil.isValidReturnCode("WMSMO908", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    @Override
    public Map<String, Object> checkWeight(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // int errCnt = 1;
        // String errMsg ="";
        
        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String lcID      	= null;
            String custId     	= null;           
            String inCustId   	= null;
            String rItemId    	= null;
            String itemQty    	= null;
            String realWgt   	= null;            
            String ordType 		= null;
            String rtiEpcCd		= null;
            
            Map<String, Object> modelIns = null;
            for(int i = 0 ; i < totCnt ; i++){
            	lcID    	= (String)model.get("LC_ID"+i);               
            	custId   	= (String)model.get("CUST_ID"+i);                
            	inCustId   	= (String)model.get("IN_CUST_ID"+i);
            	rItemId  	= (String)model.get("RITEM_ID"+i);
            	ordType  	= (String)model.get("ORD_TYPE"+i);
            	rtiEpcCd	= (String)model.get("RTI_EPC_CD"+i);
            	
            	if (StringUtils.isNotEmpty(rtiEpcCd)) {
            		rtiEpcCd = rtiEpcCd.toLowerCase().replaceFirst("urn:epc:id:grai:", "");
            		rtiEpcCd = rtiEpcCd.substring(0, rtiEpcCd.lastIndexOf("."));
            	}
            	
            	
            	if ( "01".equals(ordType) ) {
            		itemQty		= (String)model.get("REAL_IN_QTY"+i);
            		realWgt		= (String)model.get("REAL_IN_WGT"+i);
            		
            	} else if ( "02".equals(ordType) ) {
            		itemQty		= (String)model.get("REAL_OUT_QTY"+i);
            		realWgt		= (String)model.get("REAL_OUT_WGT"+i);
            		
            	} 
                modelIns = new HashMap<String, Object>();
                modelIns.put("LC_ID"    	, lcID);
                modelIns.put("CUST_ID"   	, custId);
                modelIns.put("IN_CUST_ID"   , inCustId);
                modelIns.put("RITEM_ID"   	, rItemId);
                modelIns.put("RTI_EPC_CD"   , rtiEpcCd);                
                modelIns.put("ITEM_QTY"   	, itemQty);
                modelIns.put("REAL_WGT"   	, realWgt);
                modelIns = (Map<String, Object>)dao.checkWeight(modelIns);                
                ServiceUtil.isValidReturnCode("WMSMO908", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));            	
            }       
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }     
    
}
