package com.logisall.winus.wmsmo.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO501Dao")
public class WMSMO501Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsmo501.inMobileItemList", model);
    }
    
    public GenericResultSet detailList(Map<String, Object> model) {
        return executeQueryPageWq("wmsmo501.inMobileItemListDetail", model);
    }
    
    public Object save(Map<String, Object> model){
        executeUpdate("wmsmo501.PK_WMSOP030.SP_HHT_OUT_USA_COMPLETE", model);
        return model;
    }
      
}
