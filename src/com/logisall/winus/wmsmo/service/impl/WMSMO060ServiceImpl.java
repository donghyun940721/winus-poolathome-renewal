package com.logisall.winus.wmsmo.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsmo.service.WMSMO060Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMO060Service")
public class WMSMO060ServiceImpl implements WMSMO060Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMO060Dao")
    private WMSMO060Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 매핑  조회
     * 작성자               : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * Method ID   : detailList
     * Method 설명    : 매핑  상세조회
     * 작성자               : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.detailList(model));
        return map;
    }
    
    /**
     * Method ID   : save
     * Method 설명    : 출고RFID관리 저장
     * 작성자               : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] ritemId   = new String[totCnt];
            String[] itemBarCd   = new String[totCnt];
            String[] boxBarCd   = new String[totCnt];
            String[] itemEpcCd   = new String[totCnt];
            String[] qty   = new String[totCnt];
            
            for(int i = 0 ; i < totCnt ; i++){
                ritemId[i]  = (String)model.get("RITEM_ID"+i);
                itemBarCd[i] = (String)model.get("ITEM_BAR_CD"+i);   
                boxBarCd[i] = (String)model.get("BOX_BAR_CD"+i);   
                itemEpcCd[i] = (String)model.get("ITEM_EPC_CD"+i);   
                qty[i] = (String)model.get("QTY"+i);   
            }
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("I_RITEM_ID" , ritemId);
            modelIns.put("I_ITEM_BAR_CD" , itemBarCd);
            modelIns.put("I_BOX_BAR_CD" , boxBarCd);
            modelIns.put("I_ITEM_EPC_CD" , itemEpcCd);
            modelIns.put("I_QTY" , qty);
            
            modelIns.put("I_PDA_NO"   , (String)model.get("PDA_NO"+totCnt));
            modelIns.put("I_EVENT_DT"   , (String)model.get("WORK_DT"+totCnt));
            modelIns.put("I_TRNS_STAT_CD"   , (String)model.get("TRNS_STAT_CD"+totCnt));
            modelIns.put("I_EVENT_CD"   , (String)model.get("EVENT_CD"+totCnt));
            modelIns.put("I_PDA_STAT"   , (String)model.get("PDA_STAT"+totCnt));
            
            modelIns.put("I_LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
              
            modelIns = (Map<String, Object>)dao.save(modelIns);
            ServiceUtil.isValidReturnCode("WMSMO040", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
}
