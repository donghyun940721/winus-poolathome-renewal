package com.logisall.winus.wmsmo.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsmo.service.WMSMO201Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMO201Service")
public class WMSMO201ServiceImpl implements WMSMO201Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSMO201Dao")
    private WMSMO201Dao dao;
    
    /**
     * 대체 Method ID    : selectBox
     * 대체 Method 설명      : 물류용기관리 화면 데이타셋
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("POOL", dao.selectPool(model));
        map.put("CUST", dao.selectCust(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID    : list
     * 대체 Method 설명      : 물류용기관리 조회
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
      
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 물류용기관리 저장
     * 작성자                      : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            String[] poolRtiEpcCd    	 = new String[totCnt];
           
            Map<String, Integer> pdaSeqMap = new HashMap<String, Integer>();
            for(int i = 0 ; i < totCnt ; i++){    
            	poolRtiEpcCd[i]   = ((String)model.get("POOL_EPC_CD"+i)).replace("_", ":") + (String)model.get("POOL_EPC_DOT"+i) + (String)model.get("POOL_EPC_SERIAL"+i);
            }
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("RTI_EPC_CD"      , poolRtiEpcCd);
            modelIns.put("CUST_CD"         , (String)model.get("vrSrchCustId"));          
            modelIns.put("LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("WORD_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao.insert(modelIns);
            ServiceUtil.isValidReturnCode("WMSMO201", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 엑셀다운
     * 작성자                      : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }

}
