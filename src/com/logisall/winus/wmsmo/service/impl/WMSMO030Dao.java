package com.logisall.winus.wmsmo.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO030Dao")
public class WMSMO030Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 출고RFID관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsop010.outRfidList", model);
    }
    
    /**
     * Method ID    : lisdetailList
     * Method 설명      : 출고RFID관리 상세조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet detailList(Map<String, Object> model) {
        return executeQueryPageWq("wmsop011.listRfidOutOrderItem", model);
    }  
    
    /**
     * Method ID    : save
     * Method 설명      : 출고RFID관리 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsop030.pk_wmsop030.sp_hht_out_complete", model);
        return model;
    }
}
