package com.logisall.winus.wmsmo.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsmo.service.WMSMO809Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSMO809Service")
public class WMSMO809ServiceImpl implements WMSMO809Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSMO809Dao")
    private WMSMO809Dao dao;

    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    @Override
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.detailList(model));
        return map;
    }
    
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] ordId      = new String[totCnt];
            String[] ordSeq     = new String[totCnt];
            
            String[] eventDt     = new String[totCnt];
            String[] ordType    = new String[totCnt];
            String[] ordSubType    = new String[totCnt];
            String[] custId    = new String[totCnt];
            String[] rtiEpcCode    = new String[totCnt];            
            String[] mappingType    = new String[totCnt];
            
            String[] pdaWorkSeq    = new String[totCnt];
            
            String[] ritemId    = new String[totCnt];
            String[] qty    = new String[totCnt];
            String[] inOrdWeight    = new String[totCnt];
            String[] realInWgt    = new String[totCnt];
            String[] realOutWgt    = new String[totCnt];
            String[] realInQty    = new String[totCnt];
            
            String[] realOutQty    = new String[totCnt];
            String[] uomId    = new String[totCnt];
            String[] itemBarCd    = new String[totCnt];
            String[] boxBarCd    = new String[totCnt];
            String[] makeDt    = new String[totCnt];
            
            String[] rtiId    = new String[totCnt];
            String[] itemEpcCd   = new String[totCnt];
            String[] barcodeSn    = new String[totCnt];
            String[] childMappingType    = new String[totCnt];
            
            String[] custLotNo    = new String[totCnt];
            String[] inCustId    = new String[totCnt];
            String[] transCustID    = new String[totCnt];
            
            String[] packageNo    = new String[totCnt];
            String[] packageLevel    = new String[totCnt];
            
            Map<String, Integer> pdaSeqMap = new HashMap<String, Integer>();
            
            for(int i = 0 ; i < totCnt ; i++){
            	ordId[i]    = (String)model.get("ORD_ID"+i);               
            	ordSeq[i]   = (String)model.get("ORD_SEQ"+i);
                
            	eventDt[i]   = (String)model.get("EVENT_DT"+i);
            	
            	ordType[i]  = (String)model.get("ORD_TYPE"+i);
            	ordSubType[i]  = (String)model.get("ORD_SUBTYPE"+i);
            	custId[i]  = (String)model.get("CUST_ID"+i);
            	
            	mappingType[i]  = (String)model.get("MAPPING_TYPE"+i);
            	
            	String	rtiEpcCd = (String)model.get("RTI_EPC_CD"+i); 
            	
            	Integer nextSeq = 1;
            	if (pdaSeqMap.containsKey(rtiEpcCd)) {
            		nextSeq = (Integer)(pdaSeqMap.get(rtiEpcCd)) + 1; 
            	}
            	pdaSeqMap.put(rtiEpcCd, nextSeq);            
            	rtiEpcCode[i]  = rtiEpcCd;
            	pdaWorkSeq[i]  = String.valueOf(nextSeq);  
            	// PDA_WORK_SEQ[i]  = String.valueOf(i+1);            	            	
                
            	ritemId[i]  = (String)model.get("RITEM_ID"+i);
            	qty[i]  = (String)model.get("QTY"+i);
            	inOrdWeight[i]  = (String)model.get("IN_ORD_WEIGHT"+i);
            	realInWgt[i]  = (String)model.get("REAL_IN_WGT"+i);
            	realOutWgt[i]  = (String)model.get("REAL_OUT_WGT"+i);
            	realInQty[i]  = (String)model.get("REAL_IN_QTY"+i);
                
            	realOutQty[i]  = (String)model.get("REAL_OUT_QTY"+i);
            	uomId[i]  = (String)model.get("UOM_ID"+i);
            	itemBarCd[i]  = (String)model.get("ITEM_BAR_CD"+i);
            	boxBarCd[i]  = (String)model.get("BOX_BAR_CD"+i);
            	makeDt[i]  = (String)model.get("MAKE_DT"+i);
                
            	rtiId[i]  = (String)model.get("RTI_ID"+i);
            	itemEpcCd[i]  = (String)model.get("ITEM_EPC_CD"+i);
            	barcodeSn[i]  = (String)model.get("BARCODE_SN"+i);
            	childMappingType[i]  = (String)model.get("CHILD_MAPPING_TYPE"+i);
            	
            	custLotNo[i]  = (String)model.get("CUST_LOT_NO"+i);
            	inCustId[i]  = (String)model.get("IN_CUST_ID"+i);
            	transCustID[i]  = (String)model.get("TRANS_CUST_ID"+i);

            	packageNo[i]  = (String)model.get("PACKAGE_NO"+i);
            	packageLevel[i]  = (String)model.get("PACKAGE_LEVEL"+i);
            }
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("ORD_ID"    , ordId);
            modelIns.put("ORD_SEQ"   , ordSeq);
            
            
            modelIns.put("EVENT_CD"  , "999999");
            modelIns.put("LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("PDA_NO"  , "ULNLC001");
            modelIns.put("EVENT_DT"   , eventDt[0]);            
            modelIns.put("TRNS_STAT_CD"  , "C");
            modelIns.put("PDA_STAT"  , "OK");
            
            modelIns.put("ORD_TYPE"  , ordType);
            modelIns.put("ORD_SUBTYPE"  , ordSubType);
            modelIns.put("CUST_ID"  , custId);
            modelIns.put("RTI_EPC_CD"  , rtiEpcCode);
            modelIns.put("MAPPING_TYPE"  , mappingType);
            
            modelIns.put("PDA_WORK_SEQ"  , pdaWorkSeq);
            
            modelIns.put("RITEM_ID"  , ritemId);
            modelIns.put("QTY"  , qty);
            modelIns.put("IN_ORD_WEIGHT"  , inOrdWeight);
            modelIns.put("REAL_IN_WGT"  , realInWgt);
            modelIns.put("REAL_OUT_WGT"  , realOutWgt);
            modelIns.put("REAL_IN_QTY"  , realInQty);
            
            modelIns.put("REAL_OUT_QTY"  , realOutQty);
            modelIns.put("UOM_ID"  , uomId);
            modelIns.put("ITEM_BAR_CD"  ,itemBarCd);
            modelIns.put("BOX_BAR_CD"  , boxBarCd);
            modelIns.put("MAKE_DT"  , makeDt);
            
            modelIns.put("RTI_ID"  , rtiId);
            modelIns.put("ITEM_EPC_CD"  , itemEpcCd);
            modelIns.put("BARCODE_SN"  , barcodeSn);
            modelIns.put("CHILD_MAPPING_TYPE"  , childMappingType);
            
            modelIns.put("CUST_LOT_NO"  , custLotNo);
            modelIns.put("IN_CUST_ID"  , inCustId);
            modelIns.put("TRANS_CUST_ID"  , transCustID);
            
            modelIns.put("PACKAGE_NO"  , packageNo);
            modelIns.put("PACKAGE_LEVEL"  , packageLevel);
            
            modelIns.put("lcId"     , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("workIp"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("userNo"   , (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao.save(modelIns);
            ServiceUtil.isValidReturnCode("WMSMO809", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
    
    @Override
    public Map<String, Object> checkWeight(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // int errCnt = 1;
        // String errMsg ="";
        
        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String lcID      	= null;
            String custId     	= null;           
            String inCustId   	= null;
            String rItemId    	= null;
            String itemQty    	= null;
            String realWgt   	= null;            
            String ordType 		= null;
            String rtiEpcCd		= null;
            
            Map<String, Object> modelIns = null;
            for(int i = 0 ; i < totCnt ; i++){
            	lcID    	= (String)model.get("LC_ID"+i);               
            	custId   	= (String)model.get("CUST_ID"+i);                
            	inCustId   	= (String)model.get("IN_CUST_ID"+i);
            	rItemId  	= (String)model.get("RITEM_ID"+i);
            	ordType  	= (String)model.get("ORD_TYPE"+i);
            	rtiEpcCd	= (String)model.get("RTI_EPC_CD"+i);
            	
            	if (StringUtils.isNotEmpty(rtiEpcCd)) {
            		rtiEpcCd = rtiEpcCd.toLowerCase().replaceFirst("urn:epc:id:grai:", "");
            		rtiEpcCd = rtiEpcCd.substring(0, rtiEpcCd.lastIndexOf("."));
            	}
            	
            	
            	if ( "01".equals(ordType) ) {
            		itemQty		= (String)model.get("REAL_IN_QTY"+i);
            		realWgt		= (String)model.get("REAL_IN_WGT"+i);
            		
            	} else if ( "02".equals(ordType) ) {
            		itemQty		= (String)model.get("REAL_OUT_QTY"+i);
            		realWgt		= (String)model.get("REAL_OUT_WGT"+i);
            		
            	} 
                modelIns = new HashMap<String, Object>();
                modelIns.put("LC_ID"    	, lcID);
                modelIns.put("CUST_ID"   	, custId);
                modelIns.put("IN_CUST_ID"   , inCustId);
                modelIns.put("RITEM_ID"   	, rItemId);
                modelIns.put("RTI_EPC_CD"   , rtiEpcCd);                
                modelIns.put("ITEM_QTY"   	, itemQty);
                modelIns.put("REAL_WGT"   	, realWgt);
                modelIns = (Map<String, Object>)dao.checkWeight(modelIns);                
                ServiceUtil.isValidReturnCode("WMSMO809", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));            	
            }       
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }     
    
}
