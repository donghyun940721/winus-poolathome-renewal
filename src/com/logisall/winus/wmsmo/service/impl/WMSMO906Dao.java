package com.logisall.winus.wmsmo.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSMO906Dao")
public class WMSMO906Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectZone
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmsmo906.selectPool", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 물류용기관리 등록
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model){ //wmsmo906.insert
        executeUpdate("wmsmo906.PK_WMSOP040.SP_DEMAPPING_COMPLETE", model);
        return model;
    }
        
    /**
     * Method ID  : list
     * Method 설명  : 물류용기관리 조회
     * 작성자             : 기드온
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsmo906.list", model);
    }

}
