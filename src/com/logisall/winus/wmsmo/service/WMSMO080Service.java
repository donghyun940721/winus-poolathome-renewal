package com.logisall.winus.wmsmo.service;

import java.util.Map;

public interface WMSMO080Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception;
}
