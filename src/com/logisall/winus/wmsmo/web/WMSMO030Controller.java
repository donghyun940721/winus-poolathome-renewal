package com.logisall.winus.wmsmo.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsmo.service.WMSMO030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSMO030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMO030Service")
	private WMSMO030Service service;

 
    /**
     * Method ID   : wmsmo030
     * Method 설명    : RFID출고 화면
     * 작성자               : chSong
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSMO030.action")
    public ModelAndView wmsmo030(Map<String, Object> model){
        return new ModelAndView("winus/wmsmo/WMSMO030");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }

    /**
     * Method ID    : list
     * Method 설명      : RFID출고관리  조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSMO030/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : detailList
     * Method 설명      : RFID출고관리  상세조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSMO030/detailList.action")
    public ModelAndView detailList(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.detailList(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : save
     * Method 설명      : RFID출고관리  저장
     * 작성자                 : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSMO030/save.action")
    public ModelAndView save(Map<String, Object> model){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();
        try{
            m = service.save(model);
        }catch(Exception e){
            e.printStackTrace();
            m.put("MSG", MessageResolver.getMessage("save.error"));       
        }
        mav.addAllObjects(m);
        return mav;
    }    
    
}

    
  
