package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG220Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG220Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG220Service")
	private WMSTG220Service service;

    /**
	 * Method ID : wmsms090 Method 설명 : 상품정보관리 화면 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSTG220.action")
	public ModelAndView wmstg220(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG220", service.selectData(model));
	}

	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG220/list.action")
	public ModelAndView listItem(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}
	
    @RequestMapping("/WMSTG220/detailList.action")
    public ModelAndView detailList(Map<String, Object> model) throws Exception {
        ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.detailList(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
     * Method ID : excelDownDetail
     * Method 설명 : 재고선입선출 디테일 엑셀 다운로드
     * 작성자 : 기드온
     * @param request
     * @param response
     * @param model
     */
    @RequestMapping("/WMSST220/excelDownDetail.action")
    public void excelDownDetail(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	map = service.excelDownDetail(model);
            
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelTotalDown(response, grs);
            }
        }catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
        }
    }
    protected void doExcelTotalDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("LOT번호") , "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션") , "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일자") , "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("수량")    , "3", "3", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_LOT_NO"     , "S"},
                                    {"LOC_CD"          , "S"},
                                    {"WORK_DT"         , "S"},
                                    {"STOCK_QTY"       , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("재고선입선출조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}

    
  
