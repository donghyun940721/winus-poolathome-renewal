package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG899Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG899Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG899Service")
    private WMSTG899Service service;
    
    /**
     * Method ID	: WMSTG899
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSTG899.action")
	public ModelAndView WMSTG899(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG899");
	}
    
    /*-
	 * Method ID	: getListCard
	 * Method 설명	: 
	 * 작성자			: 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSTG899/getListCard.action")
	public ModelAndView getListCard(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getListCard(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: getDetailList
	 * Method 설명	: 
	 * 작성자			: 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSTG899/getDetailList.action")
	public ModelAndView getDetailList(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.getDetailList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: getAreaLine
	 * Method 설명	: 
	 * 작성자			: 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSTG899/getAreaLine.action")
	public ModelAndView getAreaLine(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.getAreaLine(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: getBar
	 * Method 설명	: 
	 * 작성자			: 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSTG899/getBar.action")
	public ModelAndView getBar(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.getBar(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
}
