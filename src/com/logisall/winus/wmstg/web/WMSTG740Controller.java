package com.logisall.winus.wmstg.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmstg.service.WMSTG740Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG740Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG740Service")
	private WMSTG740Service service;

	/*-
	 * Method ID    : list_T1_kcc
	 * Method 설명      : 장기재고율
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG740/list_T1_kcc.action")
	public ModelAndView list_T1_kcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T1_kcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list_T1_kcc
	 * Method 설명      : 장기재고율
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG740/list_T2_kcc.action")
	public ModelAndView list_T2_kcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T2_kcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID    : list_T1_kcc
	 * Method 설명      : 장기재고율
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG740/list_T3_kcc.action")
	public ModelAndView list_T3_kcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T3_kcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list_T4_kcc
	 * Method 설명      : 장기재고율
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG740/list_T4_kcc.action")
	public ModelAndView list_T4_kcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T4_kcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list_T5_kcc
	 * Method 설명      : 장기재고율
	 * 작성자                 : dhkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG740/list_T5_kcc.action")
	public ModelAndView list_T5_kcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T5_kcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list_T7_kcc
	 * Method 설명      : 재고정확도
	 * 작성자                 : dhkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG740/list_T7_kcc.action")
	public ModelAndView list_T7_kcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T7_kcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list_T8_kcc
	 * Method 설명      : 결품율
	 * 작성자                 : dhkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG740/list_T9_kcc.action")
	public ModelAndView list_T8_kcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T9_kcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : mnV2
	 * Method 설명 : 현재고 조회 화면 V2.0 (Spread JS 적용)
	 * 작성자 : Seongjun Kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG740.action")
	public ModelAndView mnV2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG740", service.selectBox(model));
	}
	
	/*-
	 * Method ID    : list_T11_kcc
	 * Method 설명      : 일별 전체 작업 진행 현황(B2B)
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG740/list_T11_kcc.action")
	public ModelAndView list_T01_kcc(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jqGridJsonView", service.list_T11_kcc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}
	
}
