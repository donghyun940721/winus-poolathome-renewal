package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG998Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG998Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG998Service")
    private WMSTG998Service service;
    
    /**
     * Method ID    : wmstg998
     * Method 설명      : 로케이션별재고조회 화면
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSTG998.action")
    public ModelAndView wmstg998(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmstg/WMSTG998", service.selectBox(model));     
    }

    /**
     * Method ID    : list
     * Method 설명      : 로케이션별별재고 목록 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTG998/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    
    /**
     * Method ID    : listGubun
     * Method 설명      : 로케이션별별재고 목록 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTG998/listGubun.action")
    public ModelAndView listGubun(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listGubun(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : listDetail
     * Method 설명      : 로케이션별별재고 목록 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTG998/listDetail.action")
    public ModelAndView listDetail(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : listDetailLocSet
     * Method 설명      : 로케이션별별재고 목록 조회 (LOC 지정 상태 일 경우)
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTG998/listDetailLocSet.action")
    public ModelAndView listDetailLocSet(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listDetailLocSet(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
	 * Method ID   : wmstg998E1
	 * Method 설명    : 재고이동 팝업
	 * 작성자               : wdy
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG998E1.action")
	public ModelAndView wmstg998E1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG998E1");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pop :", e);
			}
		}
		return mav;
	}
    
    /**
     * Method ID    : listExcel
     * Method 설명      : 엑셀다운로드
     * 작성자                 : chsong
     * @param model
     * @return
     */
    @RequestMapping("/WMSTG998/excel.action")
    public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcel(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelDown(response, grs);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("로케이션")      , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("창고")         , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("화주")         , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("상품코드")      , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")       , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("수량")         , "5", "5", "0", "0", "100"},
                                   {"UOM"                                  , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("적치율")+"(%)" , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("가용")+"PLT"   , "8", "8", "0", "0", "100"}

  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"LOC_CD"         , "S"},
                                    {"WH_NM"          , "S"},
                                    {"CUST_NM"        , "S"},
                                    {"RITEM_CD"       , "S"},
                                    {"RITEM_NM"       , "S"},
                                    {"STOCK_QTY"      , "N"},
                                    {"UOM_NM"         , "S"},
                                    {"LOC_PER"        , "N"},
                                    {"REMAIN_PLT_QTY" , "N"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("로케이션별별재고현황");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        } catch(Exception e) {
            if (log.isWarnEnabled()) {
               	log.warn("fail download Excel file...", e);
            }
        }
    }
    
    /*-
	 * Method ID   : listDetail
	 * Method 설명    : 재고이동 팝업 리스트
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG998E3/listDetailPop.action")
	public ModelAndView listDetailPop(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listDetailPop(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pop :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : reMapping
	 * Method 설명      : 재매핑
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG998/reMapping.action")
	public ModelAndView reMapping(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.reMapping(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSTG998/locSetSearch.action")
	public ModelAndView locSetSearch(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.locSetSearch(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSTG998/locSetSearchRun.action")
	public ModelAndView locSetSearchRun(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.locSetSearchRun(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
    @RequestMapping("/WMSTG998/errCntView.action")
	public ModelAndView errCntView(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.errCntView(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
    
    @RequestMapping("/WMSTG998/zoneNmView.action")
   	public ModelAndView zoneNmView(Map<String, Object> model) {
   		ModelAndView mav = null;
   		try {
   			mav = new ModelAndView("jsonView", service.zoneNmView(model));
   		} catch (Exception e) {
   			if (log.isErrorEnabled()) {
   				log.error("Fail to get Manage Code :", e);
   			}
   		}
   		return mav;
   	}
    
    /*-
	 * Method ID    : updateLocStat
	 * Method 설명      : LOC상태변경
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG998/updateLocStat.action")
	public ModelAndView updateLocStat(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.updateLocStat(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSTG998E1/selectLocStat.action")
	public ModelAndView selectLocStat(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.selectLocStat(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
