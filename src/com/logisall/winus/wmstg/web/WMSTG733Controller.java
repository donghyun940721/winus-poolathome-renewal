package com.logisall.winus.wmstg.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST051Service;
import com.logisall.winus.wmstg.service.WMSTG733Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSTG733Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG733Service")
	private WMSTG733Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 일별재고 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG733.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG733");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	@RequestMapping("/WMSTG733pop1.action")
	public ModelAndView WMSTG733pop1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG733pop1");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 일별재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG733/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID : listE2
	 * Method 설명 : 주문단가조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG733/listE2.action")
	public ModelAndView listE2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listE3
	 * Method 설명 : 박스추천조회
	 * 작성자 : SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG733/listE3.action")
	public ModelAndView listE3(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID   : WMSYS240inExcelUploadTemplate
	 * Method 설명 : 시리얼번호 입력 엑셀업로드
	 * 작성자      : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG733/inExcelUploadTemplate.action")
	public ModelAndView inExcelUploadTemplate(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		
		ModelAndView mav = null;
		Map<String, Object> m = null;
		
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			//String filePaths = ConstantIF.TEMP_PATH;
			String filePaths = "C:\\";	
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);

			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			model.put("WORK_IP", request.getRemoteAddr());

			//int startRow = Integer.parseInt((String) model.get("startRow"));
			int startRow = 1;
			
			List<Map> list = new ArrayList<Map>();
			// 
			String[] colNameArray = {
				"ORD_ID"
				, "ORD_SEQ"
				, "RITEM_CD"
				, "CONF_BOX_NO"
				, "SERIAL_NO"
			};

			list = ExcelReader.excelLimitRowRead(destination, colNameArray, 0, startRow, 100000, 0);
			
			m = service.inExcelUploadTemplate(model, list);

			destination.deleteOnExit();
			mav = new ModelAndView("jsonView", m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	/*-
	 * Method ID   : WMSYS240inExcelUploadTemplate
	 * Method 설명 : 시리얼번호 입력 엑셀업로드
	 * 작성자      : schan
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG733/deleteSeiral.action")
	public ModelAndView deleteSeiral(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = new HashMap<String,Object>();
		try {
			m = service.deleteSeiral(model);
			mav = new ModelAndView("jsonView", m);

			} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	
	/*-
	 * Method ID   : WMSTG733Tab3UpdateRecomBox
	 * Method 설명 : 박스추천내역 수정사항 저장 
	 * 작성자      : SUMMER HYUN
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG733/updateRecomBox.action")
	public ModelAndView updateRecomBox(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		Map<String, Object> m = new HashMap<String,Object>();
		try {
			m = service.updateRecomBox(model);
			mav = new ModelAndView("jsonView", m);

			} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
}