package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG610Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG610Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG610Service")
	private WMSTG610Service service;
	
	
	/*-
	 * Method ID : 
	 * Method 설명 :
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG610.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG610", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : TG601E01
	 * Method 설명 : 통계관리 > 재고조정이력조회
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG610/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
    
	}
	
	/*-
	 * Method ID : TG601E01
	 * Method 설명 : 통계관리 > 재고조정이력조회 > 엑셀다운로드
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	
	@RequestMapping("/WMSTG610/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	

    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("재고수량"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업수량"), "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("사유")	, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("주문구분"), "7", "7", "0", "0", "200"}
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"          , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    {"STOCK_QTY"        , "NR"},
                                    {"WORK_QTY"         , "NR"},
                                    {"LOC"          	, "S"},
                                    {"REASON"          	, "S"},
                                    {"WORK_TYPE"        , "S"}
                                    
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("재고조정이력조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}