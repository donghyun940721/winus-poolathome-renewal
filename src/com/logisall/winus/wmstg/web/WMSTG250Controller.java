package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG250Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG250Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG250Service")
	private WMSTG250Service service;

	/**
     * Method ID    : wmstg250
     * Method 설명      : 물류용기재고화면
     * 작성자                 : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WINUS/WMSTG250.action")
    public ModelAndView wmstg250(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmstg/WMSTG250");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }

	/*-
	 * Method ID : list
	 * Method 설명 : 재고 입출고내역 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG250/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 재고 입출고내역 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSTG250/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("물류센터"),		"0", "0", "0", "0", "200"},
            					   {MessageResolver.getMessage("화주"),			"1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"),		"2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"),			"3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("등록구분"), 		"4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("배송상태"), 		"5", "5", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("주문"),			"6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("배송완료"), 		"7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("차이수량"),		"8", "8", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"REQ_DLV_JOIN_ID"	, "S"},
				            		{"CUST"				, "S"},
				            		{"RITEM_CD"			, "S"},
				            		{"RITEM"			, "S"},
				            		{"DLV_TYPE"			, "S"},
				            		{"DLV_STAT"			, "S"},
				            		
				            		{"DLV_QTY"			, "S"},
				            		{"INOUT_QTY"		, "S"},
				            		{"QTY_DIFF"			, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("배송_일별배송현황");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
    }
}