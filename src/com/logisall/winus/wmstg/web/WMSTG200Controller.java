package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG200Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG200Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG200Service")
	private WMSTG200Service service;

    /**
     * Method ID    : wmstg200
     * Method 설명      : 물류용기재고화면
     * 작성자                 : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WINUS/WMSTG200.action")
    public ModelAndView wmstg200(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmstg/WMSTG200", service.selectPoolGrp(model));        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
    
    /**
     * Method ID    : wmscm091Q5
     * Method 설명      : 상품조회 POP 화면(멀티셀렉트, LOT번호 조회조건 추가)
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSTG200T1.action")
    public ModelAndView wmstg200T1(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmstg/WMSTG200T1", service.DetailPop(model));     
    }

    /**
     * Method ID    : list
     * Method 설명      : 물류용기 현재고 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSTG200/list.action")
    public ModelAndView list(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
   
    /**
     * Method ID    : listSub
     * Method 설명      : 물류용기 현재고 상세조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSTG200/listSub.action")
    public ModelAndView listSub(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.listSub(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
 
    
    /**
     * Method ID    : DtailPop
     * Method 설명      : 물류용기 현재고 조회
     * 작성자                 : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSTG200/DetailPop.action")
    public ModelAndView DetailPop(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.DetailPop(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : listExcel
     * Method 설명      : 물류용기 현재고 엑셀
     * 작성자                 : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSTG200/excel.action")
    public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcel(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelDown(response, grs);
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                    {MessageResolver.getMessage("화주명")        	,"0"  ,"0" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("EPC코드")    	,"1"  ,"1" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("상품코드")   		,"2"  ,"2" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("상태")     		,"3"  ,"3" ,"0" ,"0" ,"200"}
                                   ,{MessageResolver.getMessage("REG_DT")       ,"4"  ,"4" ,"0" ,"0" ,"200"}
                                   
                                   ,{MessageResolver.getMessage("CUST_ID")     	,"5"  ,"5" ,"0" ,"0" ,"200"}

                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                     {"CUST_NM"     , "S"}
                                    ,{"EPC_CD"      , "S"}
                                    ,{"STOCK_ID"    , "S"}
                                    ,{"WORK_STAT"   , "S"}
                                    ,{"REG_DT"      , "S"}
                                    
                                    ,{"CUST_ID"  	, "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getMessage("LOT별이력추적");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isWarnEnabled()) {
               	log.warn("fail download Excel file...", e);
            }
        }
    }
}
