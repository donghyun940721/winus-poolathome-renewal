package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG700Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG700Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG700Service")
	private WMSTG700Service service;
	
	
	/*-
	 * Method ID : 
	 * Method 설명 : 배차통계
	 * 작성자 : KHKIM
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG700.action")
	public ModelAndView wmstg700(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG700");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : TG700E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG700/list.action")
	public ModelAndView list0(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
}