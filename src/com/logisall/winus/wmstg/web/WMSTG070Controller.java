package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG070Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG070Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG070Service")
	private WMSTG070Service service;

	/*-
	 * Method ID    : wmstg070
	 * Method 설명      : LOT별재고조회 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG070.action")
	public ModelAndView wmstg070(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG070", service.selectItemGrp(model));
	}
	
	
	/*-
	 * Method ID    : wmstg090
	 * Method 설명      : LOT별재고조회 화면(new)
	 * 작성자                 : ykim
	 * @param   model
	 * @desc    jqgrid => spreadjs 로 변경개발.
	 * @date    21.09.08
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG090.action")
	public ModelAndView wmstg090(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG090", service.selectItemGrp(model));
	}
	

	/*-
	 * Method ID    : list
	 * Method 설명      : LOT별재고 목록 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG070/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			//KCC홈씨씨 조회로직 분리
			if(String.valueOf(model.get("SS_SVC_NO")).equals("0000002940")){
				mav = new ModelAndView("jqGridJsonView", service.list_kcc(model));
			}else{
				mav = new ModelAndView("jqGridJsonView", service.list(model));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG070/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}

    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {"LOT_NO"                           	 , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("LOCK")      , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("화주")     	 , "2", "2", "0", "0", "100"},
                                   {MessageResolver.getText("창고")     	 , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("로케이션")  	 , "4", "4", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("상품코드")     , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")     	 , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("수량")  	     , "7", "7", "0", "0", "100"},
                                   {MessageResolver.getText("무게")  	     , "8", "8", "0", "0", "100"},
                                   {"UOM"                                , "9", "9", "0", "0", "100"},
                                   
                                   {"UNIT_NO"                            , "10", "10", "0", "0", "100"},
                                   {MessageResolver.getText("컨테이너번호")  , "11", "11", "0", "0", "100"},
                                   {MessageResolver.getText("생성일자")	 , "12", "12", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_LOT_NO"	, "S"},
                                    {"LOCK_YN"    	, "S"},
                                    {"CUST_NM"    	, "S"},
                                    {"WH_NM"      	, "S"},
                                    {"LOC_CD"     	, "S"},
                                    
                                    {"RITEM_CD"   	, "S"},
                                    {"RITEM_NM"   	, "S"},
                                    {"STOCK_QTY"  	, "N"},
                                    {"STOCK_WEIGHT" , "N"},
                                    {"UOM_NM"     	, "S"},
                                    
                                    {"UNIT_NO"    	, "S"},
                                    {"CNTR_NO"    	, "S"},
                                    {"REG_DT"     	, "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("LOT별재고");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
    
    /*-
	 * Method ID    : save
	 * Method 설명      : LOT별재고현황 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG070/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
