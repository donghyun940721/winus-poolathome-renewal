package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG080Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG080Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG080Service")
	private WMSTG080Service service;

	/*-
	 * Method ID    : wmstg080
	 * Method 설명      : 유통기한별 재고현황 화면 조회(올푸드)
	 * 작성자                 : seongjun kwon
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG080.action")
	public ModelAndView wmstg080(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG080", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 유통기한별 재고현황 정보 조회 (올푸드)
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG080/prevList.action")
	public ModelAndView prevList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.prevList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 유통기한별 재고현황 정보 조회 (올푸드)
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG080/curList.action")
	public ModelAndView curList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.curList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 유통기한별 재고현황 excel (올푸드)
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG080/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			String date = model.get("vrSrchReqDtTo").toString().replaceAll("-","");
			model.put("vrSrchReqDtTo", date);
			date = model.get("vrSrchReqDtFrom").toString().replaceAll("-","");
			model.put("vrSrchReqDtFrom", date);
			
			if(model.get("excelType").equals("C")){
				map = service.curListExcel(model);
				GenericResultSet grs = (GenericResultSet) map.get("LIST");
				if (grs.getTotCnt() > 0) {
					this.doExcelDownCurrent(response, grs);
				}
			}else if(model.get("excelType").equals("P")){
				map = service.prevListExcel(model);
				GenericResultSet grs = (GenericResultSet) map.get("LIST");
				if (grs.getTotCnt() > 0) {
					this.doExcelDownPrev(response, grs);
				}
			}else{
				throw new Exception("에러");
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download Excel file :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDownCurrent
	 * Method 설명 : 유통기한별 재고현황 (현재고+기발주) excel 다운 (올푸드)
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
    protected void doExcelDownCurrent(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
					            		{MessageResolver.getText("상품코드")      		, "0", "0", "0", "0", "200"}
					            		, {MessageResolver.getText("상품명")      		, "1", "1", "0", "0", "200"}
					            		, {MessageResolver.getText("유통기한")      		, "2", "2", "0", "0", "200"}
					            		, {MessageResolver.getText("입수")      			, "3", "3", "0", "0", "200"}
					            		, {MessageResolver.getText("UOM")      			, "4", "4", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("(전)재고")      		, "5", "5", "0", "0", "200"}
					            		, {MessageResolver.getText("입고일수")      		, "6", "6", "0", "0", "200"}
					            		, {MessageResolver.getText("입고량")      		, "7", "7", "0", "0", "200"}
					            		, {MessageResolver.getText("출고일수")      		, "8", "8", "0", "0", "200"}
					            		, {MessageResolver.getText("출고량")      		, "9", "9", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("재고")      			, "10", "10", "0", "0", "200"}
					            		, {MessageResolver.getText("발주신호")     		, "11", "11", "0", "0", "200"}
					            		, {MessageResolver.getText("회전율")     			, "12", "12", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 5")     			, "13", "13", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 6")      		, "14", "14", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("회전 7")				, "15", "15", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 8")      		, "16", "16", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 9")      		, "17", "17", "0", "0", "200"}
					            		, {MessageResolver.getText("입고일수")      		, "18", "18", "0", "0", "200"}
					            		, {MessageResolver.getText("입고량")				, "19", "19", "0", "0", "200"}
					            			
					            		, {MessageResolver.getText("출고일수")				, "20", "20", "0", "0", "200"}
					            		, {MessageResolver.getText("출고량")				, "21", "21", "0", "0", "200"}
					            		, {MessageResolver.getText("재고")      			, "22", "22", "0", "0", "200"}
					            		, {MessageResolver.getText("발주신호")      		, "23", "23", "0", "0", "200"}
					            		, {MessageResolver.getText("회전율")				, "24", "24", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("회전 5")				, "25", "25", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 6")      		, "26", "26", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 7")				, "27", "27", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 8")				, "28", "28", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 9")				, "29", "29", "0", "0", "200"}
					            		, {MessageResolver.getText("고유코드")				, "30", "30", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
					            		  
					            		  {"ITEM_CODE"				, "S"}
					            		, {"ITEM_NM"				, "S"}
					            		, {"ITEM_DATE_END"			, "S"}
					            		, {"UNIT_QTY"				, "NR"}
					            		, {"UNIT_NM"				, "S"}
					            		
					            		
					            		, {"START_STOCK_QTY"		, "NR"}
					            		, {"PREV_IN_DT_CNT"			, "NR"}
					            		, {"PREV_IN_QTY"			, "NR"}
					            		, {"PREV_OUT_DT_CNT"		, "NR"}
					            		, {"PREV_OUT_QTY"			, "NR"}
					            		
					            		, {"END_STOCK_QTY"			, "NR"}
					            		, {"PREV_ORDER_SIGNAL"		, "S"}
					            		, {"PREV_TURN_OVER"			, "N"}
					            		, {"PREV_TURN_OVER_5"		, "S"}
					            		, {"PREV_TURN_OVER_6"		, "S"}
					            		
					            		, {"PREV_TURN_OVER_7"		, "S"}
					            		, {"PREV_TURN_OVER_8"		, "S"}
					            		, {"PREV_TURN_OVER_9"		, "S"}
					            		, {"CUR_IN_DT_CNT"			, "NR"}
					            		, {"CUR_IN_QTY"				, "NR"}
					            		
					            		, {"CUR_OUT_DT_CNT"			, "NR"}
					            		, {"CUR_OUT_QTY"			, "NR"}
					            		, {"CUR_STOCK_QTY"			, "NR"}
					            		, {"CUR_ORDER_SIGNAL"		, "S"}
					            		, {"CUR_TURN_OVER"			, "N"}
					            		
					            		, {"CUR_TURN_OVER_5"		, "S"}
					            		, {"CUR_TURN_OVER_6"		, "S"}
					            		, {"CUR_TURN_OVER_7"		, "S"}
					            		, {"CUR_TURN_OVER_8"		, "S"}
					            		, {"CUR_TURN_OVER_9"		, "S"}
					            		
					            		, {"RITEM_ID"				, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("유통기한별 재고현황(현재고+기발주)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
    
    /*-
	 * Method ID : doExcelDownPrev
	 * Method 설명 : 유통기한별 재고현황 (지정기간) excel 다운 (올푸드)
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
    protected void doExcelDownPrev(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
					            		{MessageResolver.getText("상품코드")      		, "0", "0", "0", "0", "200"}
					            		, {MessageResolver.getText("상품명")      		, "1", "1", "0", "0", "200"}
					            		, {MessageResolver.getText("유통기한")      		, "2", "2", "0", "0", "200"}
					            		, {MessageResolver.getText("입수")      			, "3", "3", "0", "0", "200"}
					            		, {MessageResolver.getText("UOM")      			, "4", "4", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("(전)재고")      		, "5", "5", "0", "0", "200"}
					            		, {MessageResolver.getText("입고일수")      		, "6", "6", "0", "0", "200"}
					            		, {MessageResolver.getText("입고량")      		, "7", "7", "0", "0", "200"}
					            		, {MessageResolver.getText("출고일수")      		, "8", "8", "0", "0", "200"}
					            		, {MessageResolver.getText("출고량")      		, "9", "9", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("재고")      			, "10", "10", "0", "0", "200"}
					            		, {MessageResolver.getText("발주신호")     		, "11", "11", "0", "0", "200"}
					            		, {MessageResolver.getText("회전율")     			, "12", "12", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 5")     			, "13", "13", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 6")      		, "14", "14", "0", "0", "200"}
					            		
					            		, {MessageResolver.getText("회전 7")				, "15", "15", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 8")      		, "16", "16", "0", "0", "200"}
					            		, {MessageResolver.getText("회전 9")      		, "17", "17", "0", "0", "200"}
					            		, {MessageResolver.getText("고유코드")				, "30", "30", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
					            		  
					            		  {"ITEM_CODE"				, "S"}
					            		, {"ITEM_NM"				, "S"}
					            		, {"ITEM_DATE_END"			, "S"}
					            		, {"UNIT_QTY"				, "NR"}
					            		, {"UNIT_NM"				, "S"}
					            		
					            		, {"START_STOCK_QTY"		, "NR"}
					            		, {"IN_DT_CNT"				, "NR"}
					            		, {"IN_QTY"					, "NR"}
					            		, {"OUT_DT_CNT"				, "NR"}
					            		, {"OUT_QTY"				, "S"}
					            		
					            		, {"END_STOCK_QTY"			, "S"}
					            		, {"ORDER_SIGNAL"			, "S"}
					            		, {"TURN_OVER"				, "N"}
					            		, {"TURN_OVER_5"			, "S"}
					            		, {"TURN_OVER_6"			, "S"}
					            		
					            		, {"TURN_OVER_7"			, "S"}
					            		, {"TURN_OVER_8"			, "S"}
					            		, {"TURN_OVER_9"			, "S"}
					            		, {"RITEM_ID"				, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("유통기한별 재고현황(지정기간)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
