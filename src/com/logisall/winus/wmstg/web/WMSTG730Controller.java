package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG730Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG730Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG730Service")
	private WMSTG730Service service;
	
	
	/*-
	 * Method ID : TG600E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 화면
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG730.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG730", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSTG730/listE01.action")
	public ModelAndView list0(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE01(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSTG730/listE02.action")
	public ModelAndView listE02(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE02(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSTG730/listE03.action")
	public ModelAndView listE03(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE03(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSTG730/listE04.action")
	public ModelAndView listE04(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE04(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSTG730/excelDown01.action")
	public void listExcel0(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.list01Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown01(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown01(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("품목코드")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("품명")   	, "1", "1", "0", "0", "250"},
				            		{MessageResolver.getText("ERP출고 주문 수량")  , "2", "2", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"RITEM_CD" 		,"S"},
				            		{"RITEM_NM" 		,"S"},
				            		{"OUT_ORD_QTY" 	,"NR"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("출고주문대비 재고미보유 항목");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	

	@RequestMapping("/WMSTG730/excelDownE02.action")
	public void excelDownE02(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.list02Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown02(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown02(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("품목ID")  		, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("품목코드")   	, "1", "1", "0", "0", "250"},
				            		{MessageResolver.getText("품목명")  		, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("로케이션")  		, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("로케이션바코드")  , "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("재고수량")  		, "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("주문수량")  		, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("재고보관 로케이션")  		, "7", "7", "0", "0", "100"},
				            		{MessageResolver.getText("픽스로케이션 재고 수량") 	, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("보충필수 수량") 			, "9", "9", "0", "0", "100"}

                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"RITEM_ID" 		,"S"},
				            		{"RITEM_CD" 		,"S"},
				            		{"RITEM_NM" 		,"S"},
				            		{"LOC_CD" 			,"S"},
				            		{"LOC_BARCODE_CD" 	,"S"},
				            		{"STOCK_QTY" 		,"NR"},
				            		{"ORD_QTY" 			,"NR"},
				            		{"NOW_LOC_CD" 		,"S"},
				            		{"FIX_QTY" 			,"NR"},
				            		{"REQ_TRANS_QTY" 	,"NR"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("출고주문대비 재고미보유 항목");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	@RequestMapping("/WMSTG730/excelDownE03.action")
	public void excelDownE03(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.list03Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown03(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	
	protected void doExcelDown03(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            						{MessageResolver.getText("LOC")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("TOTAL_COUNT_HEADER")  	, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("TT")  , "2", "2", "0", "0", "100"},
//				            		{MessageResolver.getText("TT")  	, "3", "3", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
//            						{"LOC" 		,"NR"},
				            		{"LOC" 		,"S"},
				            		{"TOTAL_COUNT_HEADER" 	,"NR"},
				            		{"TT" 		,"S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("작업진행현황");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	
	
	
}