package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG170Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG170Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG170Service")
	private WMSTG170Service service;

	/*-
	 * Method ID    : wmstg070
	 * Method 설명      : LOT별재고조회 화면
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSTG170.action")
	public ModelAndView wmstg070(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG170", service.selectItemGrp(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : LOT별재고 목록 조회
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSTG170/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG170/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}

	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getText("출고일자")    , "0", "0", "0", "0", "100"},               
            					   {MessageResolver.getText("화주")      , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("거래처")     , "2", "2", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("LOT번호")   , "3", "3", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("상품코드")    , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")     , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("출고량")     , "6", "6", "0", "0", "100"},
                                   {"UOM"                               , "7", "7", "0", "0", "100"}, 
                                   {MessageResolver.getText("출고중량")    , "8", "8", "0", "0", "100"},
                                   {MessageResolver.getText("PLT수량")    , "9", "9", "0", "0", "100"},
                                   {MessageResolver.getText("BOX수량")    , "10", "10", "0", "0", "100"},
                                   {MessageResolver.getText("창고")       , "11", "11", "0", "0", "100"},
                                   {MessageResolver.getText("비고")       , "12", "12", "0", "0", "100"},
                                   
                                   {MessageResolver.getText("컨테이너번호")  , "13", "13", "0", "0", "100"}
                                   
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"VIEW_OUT_DT"     , "S"},                
            						{"CUST_NM"         , "S"},
                                    {"TRANS_CUST_NM"   , "S"},
                                    
                                    {"CUST_LOT_NO"     , "S"},
                                    
                                    {"RITEM_CD"        , "S"},
                                    {"RITEM_NM"        , "S"},
                                    {"OUT_ORD_QTY"     , "N"},
                                    
                                    {"OUT_UOM_NM"      , "S"},
                                    {"OUT_ORD_WEIGHT"  , "N"},
                                    {"REAL_PLT_QTY"    , "N"},
                                    {"REAL_BOX_QTY"    , "N"},
                                    {"OUT_WH_NM"       , "S"},
                                    {"ETC2"            , "S"},
                                    
                                    {"CNTR_NO"         , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("거래처별출고현황");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
}
