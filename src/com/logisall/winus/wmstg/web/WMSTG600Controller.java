package com.logisall.winus.wmstg.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmstg.service.WMSTG600Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSTG600Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSTG600Service")
	private WMSTG600Service service;
	
	
	/*-
	 * Method ID : TG600E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 화면
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSTG600.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmstg/WMSTG600", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : TG600E01
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/list0.action")
	public ModelAndView list0(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list0(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	 /*-
	 * Method ID    : excelDown0
	 * Method 설명   : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 화면 엑셀 다운로드
	 * 작성자         : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/excelDown0.action")
	public void listExcel0(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.list0Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown0(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown0(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("품목코드")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("품명")   	, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("주문수량")  , "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("출고수량")	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("수수료율")	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("단가")		, "5", "5", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("합계")   	, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("예상운송료") , "7", "8", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"ITEM_CD" 		,"S"},
				            		{"ITEM_NM" 		,"S"},
				            		{"ERP_ORD_QTY" 		,"S"},
				            		{"WINUS_OUT_QTY" 	,"S"},
				            		{"SERVICE_RATE" 	,"N"},
				            		{"UM" 				,"NR"},
				            		{"TOTAL_UM" 		,"NR"},
				            		{"TOTAL_TRANS_UM" 	,"NR"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("기간별배송비현황");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	
	@RequestMapping("/WMSTG600P01.action")
	public ModelAndView WMSTG600P01(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmstg/WMSTG600P01");
	}
	
	/*-
	 * Method ID : TG600P01
	 * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > 기간별 배송비현황 상세 팝업 조회
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/pList01.action")
	public ModelAndView pList01(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.pList01(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	 /*-
	 * Method ID    : excelDown0
	 * Method 설명   : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 셀 더블클릭시 상세 팝업 엑셀다운로드
	 * 작성자         : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/excelDownP0.action")
	public void popListExcel0(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			map = service.list0Excel(model);
			map = service.listPop0Excel(model);
			
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownPop0(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDownPop0(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("품목코드")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("품명")   	, "1", "1", "0", "0", "160"},
				            		{MessageResolver.getText("주문수량")  , "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("출고수량")	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("거래처코드")	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("거래처명")	, "5", "5", "0", "0", "130"}
//				            		
//				            		{MessageResolver.getText("합계")   	, "5", "5", "0", "0", "100"},
//				            		{MessageResolver.getText("예상운송료") , "6", "6", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"ITEM_CD" 		,"S"},
				            		{"ITEM_NM" 		,"S"},
				            		{"ERP_ORD_QTY" 		,"S"},
				            		{"WINUS_OUT_QTY" 	,"S"},
				            		{"CUST_CD" 			,"S"},
				            		{"CUST_NM" 			,"S"}
//				            		{"TOTAL_UM" 		,"NR"},
//				            		{"TOTAL_TRANS_UM" 	,"NR"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("기간별 배송비 상세현황");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : TG600E02
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > ERP 수신 주문정보 조회 > 가맹점
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/listE02.action")
	public ModelAndView listE02(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE02(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : excelDown0
	 * Method 설명   : 통계관리 > 이디야 조회 테스트 > ERP 수신 주문정보 조회
	 * 작성자         : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/excelDownE02.action")
	public void excelDownE02(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listE02Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			
			if (grs.getTotCnt() > 0) {
				this.doExcelDownE02(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDownE02(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("주문번호")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("거래처코드")  , "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("거래처명")   ,  "2", "2", "0", "0", "120"},
				            		{MessageResolver.getText("배송일")  	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("품목코드")	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("품목명")	, "5", "5", "0", "0", "200"},
				            		{MessageResolver.getText("수량")		, "6", "6", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"ORD_NO" 		,"S"},
				            		{"PARTNER_CD" 	,"S"},
				            		{"PARTNER_NM" 	,"S"},
				            		{"DLV_DT" 		,"S"},
				            		{"ITEM_CD"	 	,"S"},
				            		{"ITEM_NM"	 	,"S"},
				            		{"QT"	 		,"N"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("ERP 수신 주문 정보 조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : TG600E03
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 일별 ERP 전송 이력조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/listE03.action")
	public ModelAndView listE03(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE03(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : excelDown0
	 * Method 설명   : 통계관리 > 이디야 조회 테스트 > ERP 수신 주문정보 조회
	 * 작성자         : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/excelDownE03.action")
	public void excelDownE03(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listE03Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			
			if (grs.getTotCnt() > 0) {
				this.doExcelDownE03(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDownE03(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("주문번호")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("주문SEQ")  , "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("상품코드")  ,  "2", "2", "0", "0", "120"},
				            		{MessageResolver.getText("상품명")  	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("예정일")	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("수량")		, "5", "5", "0", "0", "200"},
				            		{MessageResolver.getText("유통기한")	, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("수불일자")	, "7", "7", "0", "0", "100"},
				            		{MessageResolver.getText("등록일자")	, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("전송일자")	, "9", "9", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"NO_RCV" 		,"S"},
				            		{"SEQ_RCV" 		,"S"},
				            		{"ITEM_CD" 		,"S"},
				            		{"ITEM_NM" 		,"S"},
				            		{"DT_IO"	 	,"S"},
				            		{"QT_IO_MM"	 	,"NR"},
				            		{"DT_EXPIRY" 	,"S"},
				            		{"SUBUL_DT"	 	,"S"},
				            		{"REG_DT"	 	,"S"},
				            		{"UPD_DT" 		,"S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("ERP 수신 주문 정보 조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	
	
	/*-
	 * Method ID : TG600E03
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > ERP 주문정보 대비 실적 전송 내역
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/listE04.action")
	public ModelAndView listE04(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE04(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : excelDownE04
	 * Method 설명   : 통계관리 > 이디야 조회 테스트 > ERP 주문정보 대비 실적 전송 내역
	 * 작성자         : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/excelDownE04.action")
	public void excelDownE04(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listE04Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			
			if (grs.getTotCnt() > 0) {
				this.doExcelDownE04(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDownE04(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("주문번호")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("주문SEQ")  , "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("거래처코드") ,  "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("거래처명")  	, "3", "3", "0", "0", "120"},
				            		
				            		{MessageResolver.getText("상품코드")  ,  "4", "4", "0", "0", "120"},
				            		{MessageResolver.getText("상품명")  	, "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("예정일")	, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("수량")		, "7", "7", "0", "0", "200"},
				            		{MessageResolver.getText("수불일자")	, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("등록일자")	, "9", "9", "0", "0", "100"},
				            		{MessageResolver.getText("전송일자")	, "10", "10", "0", "0", "100"}
				            		
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"NO_RCV" 		,"S"},
				            		{"SEQ_RCV" 		,"S"},
				            		{"PARTNER_CD" 	,"S"},
				            		{"PARTNER_NM" 	,"S"},
				            		{"ITEM_CD" 		,"S"},
				            		{"ITEM_NM" 		,"S"},
				            		{"DT_IO"	 	,"S"},
				            		{"QT_IO_MM"	 	,"NR"},
				            		{"SUBUL_DT"	 	,"S"},
				            		{"REG_DT"	 	,"S"},
				            		{"UPD_DT" 		,"S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("ERP 수신 주문 정보 조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
     * Method ID    : updateE02
     * Method 설명   : 통계관리 > 이디야 조회 테스트 > 2.ERP 수신 주문정보 조회 > 삭제 
     * 작성자         : 김채린
     * @param   model
     * @return  
     */
	
    @RequestMapping("/WMSTG600/updateE02.action")
    public ModelAndView updateE02(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.updateE02(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }    
    

	/*-
     * Method ID    : updateE02
     * Method 설명   : 통계관리 > 이디야 조회 테스트 > 2.ERP 수신 주문정보 조회 > 주문유형 별 품목 변경 
     * 작성자         : 김채린
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSTG600/updateE03.action")
    public ModelAndView updateE03(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.updateE03(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }    
    
    

	/*-
	 * Method ID : 
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 상품,LOT별 거래처 출고실적 조회
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/listE05.action")
	public ModelAndView listE05(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE05(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	

	/*-
	 * Method ID    : excelDown5
	 * Method 설명   : 통계관리 > 이디야 조회 테스트 > 상품,LOT별 거래처 출고실적 엑셀
	 * 작성자         : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/excelDownE05.action")
	public void excelDownE05(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listE05Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			
			if (grs.getTotCnt() > 0) {
				this.doExcelDownE05(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	

	protected void doExcelDownE05(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
					            		{MessageResolver.getText("작업일")  	, "0", "0", "0", "0", "100"},
					            		{MessageResolver.getText("LOT번호")  , "1", "1", "0", "0", "100"},
					            		{MessageResolver.getText("유효일자") 	, "2", "2", "0", "0", "100"},
					            		{MessageResolver.getText("상품코드")  	, "3", "3", "0", "0", "120"},
					            		{MessageResolver.getText("상품명")  	, "4", "4", "0", "0", "120"},
					            		{MessageResolver.getText("거래처코드") , "5", "5", "0", "0", "100"},
					            		{MessageResolver.getText("거래처명")	, "6", "6", "0", "0", "100"},
					            		{MessageResolver.getText("수량")		, "7", "7", "0", "0", "200"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
					            		{"WORK_DT" 				,"S"},
					            		{"CUST_LOT_NO" 			,"S"},
					            		{"ITEM_BEST_DATE_END" 	,"S"},
					            		{"RITEM_CD" 			,"S"},
					            		{"RITEM_NM" 			,"S"},
					            		{"TRANS_CUST_CD" 		,"S"},
					            		{"TRANS_CUST_NM"		,"S"},
					            		{"REAL_OUT_QTY"	 		,"NR"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("거래처 출고실적 조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	
	/*-
	 * Method ID : TG600E06
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 출고 현황
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/listE06.action")
	public ModelAndView list6(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE06(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	

	 /*-
	 * Method ID    : excelDown0
	 * Method 설명   : 통계관리 > 이디야 조회 테스트 > 기간별 배송비현황 화면 엑셀 다운로드
	 * 작성자         : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/excelDown06.action")
	public void listExcel06(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listE06Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown06(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown06(HttpServletResponse response, GenericResultSet grs) {
       try{
           //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
           //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
           String[][] headerEx = {
				            		{MessageResolver.getText("품목코드")  		, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("품명")   		, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("ERP주문수량") 	, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("OMS주문수량")  	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("출고수량")		, "4", "4", "0", "0", "100"}
                                 };
           
           //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
           String[][] valueName = {
				            		{"ITEM_CD" 			,"S"},
				            		{"ITEM_NM" 			,"S"},
				            		{"ERP_ORD_QTY" 		,"S"},
				            		{"OMS_ORD_QTY" 		,"S"},
				            		{"WINUS_OUT_QTY" 	,"S"}
				            		
                                  }; 

			// 파일명
			String fileName = MessageResolver.getText("기간별출고현황");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	

	/*-
	 * Method ID : TG600E06
	 * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 출고 현황
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSTG600/listE07.action")
	public ModelAndView list7(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE07(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
}