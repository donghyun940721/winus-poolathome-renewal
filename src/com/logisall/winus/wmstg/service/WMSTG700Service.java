package com.logisall.winus.wmstg.service;

import java.util.Map;

public interface WMSTG700Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
}
