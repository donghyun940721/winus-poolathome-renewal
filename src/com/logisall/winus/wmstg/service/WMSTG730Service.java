package com.logisall.winus.wmstg.service;

import java.util.Map;
import java.util.List;

public interface WMSTG730Service {
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE01(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE02(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE03(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE04(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> list01Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> list02Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> list03Excel(Map<String, Object> model) throws Exception;
    
    
}
