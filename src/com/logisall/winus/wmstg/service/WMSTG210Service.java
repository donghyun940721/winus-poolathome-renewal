package com.logisall.winus.wmstg.service;

import java.util.Map;

public interface WMSTG210Service {
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItem(Map<String, Object> model) throws Exception;

}
