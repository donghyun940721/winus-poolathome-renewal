package com.logisall.winus.wmstg.service;

import java.util.Map;
import java.util.List;

public interface WMSTG600Service {
    public Map<String, Object> list0(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE02(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE03(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE04(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE05(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE06(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE07(Map<String, Object> model) throws Exception;
    
    
    public Map<String, Object> pList01(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list0Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE02Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE03Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE04Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE05Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE06Excel(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listPop0Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateE02(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateE03(Map<String, Object> model) throws Exception;
}
