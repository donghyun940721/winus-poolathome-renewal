package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG998Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listGubun(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetailLocSet(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetailPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> reMapping(Map<String, Object> model) throws Exception;
    public Map<String, Object> locSetSearch(Map<String, Object> model) throws Exception;
    public Map<String, Object> locSetSearchRun(Map<String, Object> model) throws Exception;
    public Map<String, Object> errCntView(Map<String, Object> model) throws Exception;
    public Map<String, Object> zoneNmView(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateLocStat(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectLocStat(Map<String, Object> model) throws Exception;
}
