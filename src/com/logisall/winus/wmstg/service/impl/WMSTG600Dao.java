package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG600Dao")
public class WMSTG600Dao extends SqlMapAbstractDAO{

		/**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
        /**
     * Method ID : list0
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > 기간별배송비현황 조회
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet list0(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.list0", model);
	}
	
	 /**
     * Method ID : pList01
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별배송비현황 셀 더블클릭시 팝업조회
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet pList01(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.pList01", model);
	}
	
	/**
     * Method ID : listE02
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > ERP 수신 주문 정보 조회 > 가맹점
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE02(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE02", model);
	}
	
	/**
     * Method ID : listE02Sale
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > ERP 수신 주문 정보 조회 > 유통판매
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE02Sale(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE02Sale", model);
	}
	
	/**
     * Method ID : listE02In
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > ERP 수신 주문 정보 조회 > 정상입고
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE02In(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE02In", model);
	}
	
	/**
     * Method ID : listE02Out
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > ERP 수신 주문 정보 조회 > 반품입고
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE02Out(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE02Out", model);
	}
	
	/**
     * Method ID : listE02
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > 일별 ERP 전송 이력 조회 > 입고
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE03(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE03", model);
	}
	/**
     * Method ID : listE02
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > 일별 ERP 전송 이력 조회 > 입고
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE03OutOrd(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE03OutOrd", model);
	}
	
	/**
     * Method ID : listE04
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > ERP 주문정보 대비 실적 전송 내역> 유통
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE04(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE04", model);
	}
	
	/**
     * Method ID : listE04
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > ERP 주문정보 대비 실적 전송 내역> 정상입고
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE04InOrd(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE04InOrd", model);
	}
	
	/**
     * Method ID : listE04
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > ERP 주문정보 대비 실적 전송 내역> 반품입고
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE04RtnOrd(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE04RtnOrd", model);
	}
	
	/**
     * Method ID : updateOp011,updateOm010
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 2.ERP 수신 주문정보 조회 > 삭제 
     * 작성자 : 김채린
     * @param modeL
     * @return
     */
	public void updateOm010(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmstg600.updateOm010", modelDt);
	}
	public void updateOp011(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmstg600.updateOp011", modelDt);
	}
	
	/**
     * Method ID : updateOp011,updateOm010
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 2.ERP 수신 주문정보 조회 > 품목변경
     * 작성자 : 김채린
     * @param modeL
     * @return
     */
	public void updateOm010Item(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmstg600.updateOm010Item", modelDt);
	}
	
	public void updateOp011Item(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmstg600.updateOp011Item", modelDt);
	}
	
	
	/**
     * Method ID : 
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 2.ERP 수신 주문정보 조회 > 주문유형 별 품목 변경
     * updateTwmsItem	 : 가맹점 품목변경
     * updateTwmsPosItem : 유통판매 품목변경
     * updateErpRcvlItem : 정상입고 품목변경
     * updateCzRetlItem  : 반품입고 품목변경
     * 작성자 : 김채린
     * @param modeL
     * @return
     */
	
	public void updateTwmsOrderItem(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmstg600.updateTwmsOrderItem", modelDt);
	}
	
	public void updateTwmsPosItem(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmstg600.updateTwmsPosItem", modelDt);
	}
	
	public void updateErpRcvlItem(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmstg600.updateErpRcvlItem", modelDt);
	}
	
	public void updateCzRetlItem(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		executeDelete("wmstg600.updateCzRetlItem", modelDt);
	}
	
	/**
     * Method ID : listE05
     * Method 설명 : 통계관리 > 이디야 조회 테스트 >  상품,LOT별 거래처 출고실적 조회
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	public GenericResultSet listE05(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE05", model);
	}
	
    /**
     * Method ID : list06
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > 기간별 출고현황
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE06(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE06", model);
	}
	
	/**
     * Method ID : list06
     * Method 설명 : 통계꽌리 > 이디야 조회 테스트 > 기간별 출고현황
     * 작성자 : 김채린
     * @param model
     * @return
     */
	public GenericResultSet listE07(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg600.listE07", model);
	}
}
