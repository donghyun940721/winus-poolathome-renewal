package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG070Dao")
public class WMSTG070Dao extends SqlMapAbstractDAO{

	/**
     * Method ID  : selectItemGrp
     * Method �ㅻ�  : ��硫대�� ������ ����援� 媛��몄�ㅺ린
     * ���깆��             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID    : list
     * Method 설명      : LOT별재고조회 목록 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmstg070.lotItemList", model);
    }

    /**
     * Method ID    : list
     * Method 설명      : LOT별재고조회 목록 조회(kcc홈씨씨)
     * 작성자                 : ykim
     * @param   model
     * @return
     */
    public GenericResultSet list_kcc(Map<String, Object> model) {
        return executeQueryPageWq("wmstg070.lotItemList_kcc", model);
    }
    
    /**
	 * Method ID : update 
	 * Method 설명 : LOT별재고현황 저장
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmstg070.update", model);
	}
}
