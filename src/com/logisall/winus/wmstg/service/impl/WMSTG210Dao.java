package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG210Dao")
public class WMSTG210Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    public GenericResultSet detailList(Map<String, Object> model) {
        return executeQueryPageWq("wmstg210.detailList", model);
    }
    
    /**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model) {
	return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectLocMaster(Map<String, Object> model) {
	return executeQueryForList("wmstg210.referLoc", model);
    }
    
    /**
     * Method ID : listItem Method 설명 : 상품정보 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listItem(Map<String, Object> model) {
	return executeQueryPageWq("wmstg210.listItem", model);
    }
}

