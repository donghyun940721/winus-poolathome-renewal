package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;







import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmstg.service.WMSTG733Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Service("WMSTG733Service")
public class WMSTG733ServiceImpl extends AbstractServiceImpl implements WMSTG733Service {
    
    @Resource(name = "WMSTG733Dao")
    private WMSTG733Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 입별재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : listE2
     * Method 설명 : 일별재고-> 주문 단가 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		map.put("LIST", dao.listE2(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }
    
    /**
     * Method ID : listE3
     * Method 설명 : 박스추천조회
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		map.put("LIST", dao.listE3(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   	: inExcelUploadTemplate
     * 대체 Method 설명     : 시리얼번호 템플릿 업로드 
     * 작성자               : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> inExcelUploadTemplate(Map<String, Object> model,  List list) throws Exception {
    	int listBodyCnt = (list != null)?list.size():0;
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		if(listBodyCnt > 0){
    			String[] ordId = new String[listBodyCnt];
    			String[] ordSeq = new String[listBodyCnt];
    			String[] ritemCd = new String[listBodyCnt];
    			String[] confBoxNo = new String[listBodyCnt];
    			String[] serialNo = new String[listBodyCnt];
    			for(int i = 0 ; i < listBodyCnt ; i ++){
    				Map<String,String> temp = (Map<String,String>)list.get(i);
    				ordId[i]		= temp.get("ORD_ID");
    				ordSeq[i]		= temp.get("ORD_SEQ");
    				ritemCd[i]		= temp.get("RITEM_CD");
    				confBoxNo[i]		= temp.get("CONF_BOX_NO");    				
    				serialNo[i]		= temp.get("SERIAL_NO");
    			}
    			m.put("LC_ID", (String)model.get("vrLcId"));
    			m.put("CUST_ID", (String)model.get("vrCustId"));
    			m.put("ORD_ID", ordId);
    			m.put("ORD_SEQ", ordSeq);
    			m.put("RITEM_CD", ritemCd);
    			m.put("CONF_BOX_NO", confBoxNo);
    			m.put("SERIAL_NO", serialNo);
    			m.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                m.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			dao.inExcelUploadTemplate(m);
    			ServiceUtil.isValidReturnCode("WMSTG733", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   m.put("errCnt", 1);
    	   m.put("MSG_ORA", e.getMessage());
    	}
       
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   	: deleteSeiral
     * 대체 Method 설명     : 시리얼번호 선택 삭제 
     * 작성자               : schan
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteSeiral(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		int tempCnt = Integer.parseInt(model.get("tempCnt").toString());
    		if(tempCnt > 0){
    			String[] ordId = new String[tempCnt];
    			String[] ordSeq = new String[tempCnt];
    			String[] confBoxNo = new String[tempCnt];
    			String[] serialNo = new String[tempCnt];
    			for(int i = 0 ; i < tempCnt ; i ++){
    				ordId[i]		= (String)model.get("ORD_ID"+i);
    				ordSeq[i]		= (String)model.get("ORD_SEQ"+i);
    				confBoxNo[i]	= (String)model.get("CONF_BOX_NO"+i);    				
    				serialNo[i]		= (String)model.get("SERIAL_NO"+i);
    			}
    			
    			m.put("ORD_ID", ordId);
    			m.put("ORD_SEQ", ordSeq);
    			m.put("CONF_BOX_NO", confBoxNo);
    			m.put("SERIAL_NO", serialNo);
    			m.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                m.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
    			dao.deleteSeiral(m);
    			ServiceUtil.isValidReturnCode("WMSTG733", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   m.put("errCnt", 1);
    	   m.put("MSG_ORA", e.getMessage());
    	}
       
        return m;
    }
    
    
    /**
     * 
     * 대체 Method ID   : updateRecomBox
     * 대체 Method 설명    : 박스추천내역수정
     * 작성자               	  : SUMMER HYUN
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateRecomBox(Map<String, Object> model) throws Exception {
    	 
    	 Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		int tempCnt = Integer.parseInt(model.get("tempCnt").toString());
    		if(tempCnt > 0){
    			
    			//필요한 값 DB로 보내서 조회 
    			 
    			for(int i = 0 ; i < tempCnt ; i ++){
    				
    				//Map<String, Object> m2 = new HashMap<String, Object>();
    				
    				m.put("LC_ID",(String)model.get("LC_ID"+i));
    				m.put("CUST_ID",(String)model.get("CUST_ID"+i));
    				m.put("ORD_ID",(String)model.get("ORD_ID"+i));
    				m.put("ORD_SEQ",(String)model.get("ORD_SEQ"+i));
    				m.put("ORD_TYPE",(String)model.get("ORD_TYPE"+i));
    				m.put("RECOM_SEQ",(String)model.get("RECOM_SEQ"+i));
    				m.put("RECOM_BOX_ID",(String)model.get("RECOM_BOX_ID"+i));
    				m.put("RECOM_BOX_NO",(String)model.get("RECOM_BOX_NO"+i));
    				m.put("FA_ISSUE_LABEL",(String)model.get("FA_ISSUE_LABEL"+i));
    				//수정자, WORK_IP
    				m.put("WORK_IP",(String)model.get(ConstantIF.SS_CLIENT_IP));
    				m.put("UPD_NO",(String)model.get(ConstantIF.SS_USER_NO));
    				dao.updateRecomBox(m);
    			}
    		}
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   m.put("errCnt", 1);
    	   m.put("MSG_ORA", e.getMessage());
    	}
       
        return m;
    }
    
}

