package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmstg.service.WMSTG250Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG250Service")
public class WMSTG250ServiceImpl extends AbstractServiceImpl implements WMSTG250Service {
    
    @Resource(name = "WMSTG250Dao")
    private WMSTG250Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
               
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : listExcel
     * Method 설명 : 재고 입출고내역 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();           
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        
        return map;
    }
}
