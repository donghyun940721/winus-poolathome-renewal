package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmstg.service.WMSTG996Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG996Service")
public class WMSTG996ServiceImpl implements WMSTG996Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG996Dao")
    private WMSTG996Dao dao;
    
    /**
     * 
     * 대체 Method ID   : selectLocList
     * 대체 Method 설명    : 제품군별재고조회 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LOCLIST", dao.selectLocList(model));
        map.put("POOL"   , dao.selectPool(model));
        return map;
    }
}
