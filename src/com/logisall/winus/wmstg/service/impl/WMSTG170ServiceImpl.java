package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmstg.service.WMSTG170Service;

@Service("WMSTG170Service")
public class WMSTG170ServiceImpl implements WMSTG170Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG170Dao")
    private WMSTG170Dao dao;
    
    /**
     * 
     * Method ID   : selectItemGrp
     * Method �ㅻ�    : ��怨�愿�由� ��硫댁���� ������ �곗�댄��
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : LOT별재고조회 목록 조회
     * 작성자                      : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : LOT재고 목록 엑셀용조회
     * 작성자                      : 기드온
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }  
}
