package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG730Dao")
public class WMSTG730Dao extends SqlMapAbstractDAO{

		/**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    public GenericResultSet listE01(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg730.listE01", model);
	}
    
    public GenericResultSet listE02(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg730.listE02", model);
	}
    
    public GenericResultSet listE03(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg730.listE03", model);
	}
    
    public GenericResultSet listE04(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmstg730.listE04", model);
	}
    
    
    
    
    
    
}
