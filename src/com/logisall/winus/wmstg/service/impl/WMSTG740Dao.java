package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG740Dao")
public class WMSTG740Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
	 * Method ID    : list_T1_kcc
	 * Method 설명      : 출고 리드타임(kcc홈씨씨)
	 * 작성자                 : sing09
	 * @param   model
	 * @return
	 */
	public GenericResultSet list_T1_kcc(Map<String, Object> model) {
		return executeQueryPageWq("wmstg740.list_T1_kcc", model);
	}
	
	/**
	 * Method ID    : list_T2_kcc
	 * Method 설명      : 출고 리드타임(kcc홈씨씨)
	 * 작성자                 : dhkim
	 * @param   model
	 * @return
	 */
	public GenericResultSet list_T2_kcc(Map<String, Object> model) {
		return executeQueryPageWq("wmstg740.list_T2_kcc", model);
	}
	
	/**
	 * Method ID    : list_T3_kcc
	 * Method 설명      : 출고 리드타임(kcc홈씨씨)
	 * 작성자                 : sing09
	 * @param   model
	 * @return
	 */
	public GenericResultSet list_T3_kcc(Map<String, Object> model) {
		return executeQueryPageWq("wmstg740.list_T3_kcc", model);
	}
	
	/**
     * Method ID    : list_T4_kcc
     * Method 설명      : 장기재고율(kcc홈씨씨)
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public GenericResultSet list_T4_kcc(Map<String, Object> model) {
        return executeQueryPageWq("wmstg740.list_T4_kcc", model);
    }
    
	/**
     * Method ID    : list_T5_kcc
     * Method 설명      : 장기재고율(kcc홈씨씨)
     * 작성자                 : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet list_T5_kcc(Map<String, Object> model) {
        return executeQueryPageWq("wmstg740.list_T5_kcc", model);
    }
    
	/**
     * Method ID    : list_T7_kcc
     * Method 설명      : 장기재고율(kcc홈씨씨)
     * 작성자                 : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet list_T7_kcc(Map<String, Object> model) {
        return executeQueryPageWq("wmstg740.list_T7_kcc", model);
    }
    
	/**
     * Method ID    : list_T9_kcc
     * Method 설명      : 결품율(kcc홈씨씨)
     * 작성자                 : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet list_T9_kcc(Map<String, Object> model) {
        return executeQueryPageWq("wmstg740.list_T9_kcc", model);
    }
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
	 * Method ID    : list_T11_kcc
	 * Method 설명      : 일별 전체 작업 진행 현황(B2B)
	 * 작성자                 : yhku
	 * @param   model
	 * @return
	 */
	public GenericResultSet list_T11_kcc(Map<String, Object> model) {
		return executeQueryPageWq("wmstg740.list_T11_kcc", model);
	}
    
}
