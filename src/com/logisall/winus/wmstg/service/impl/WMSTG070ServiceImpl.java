package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmstg.service.WMSTG070Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG070Service")
public class WMSTG070ServiceImpl implements WMSTG070Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG070Dao")
    private WMSTG070Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSTG070 = {"STOCK_ID", "LOCK_YN"};
    
    /**
     * 
     * Method ID   : selectItemGrp
     * Method �ㅻ�    : ��怨�愿�由� ��硫댁���� ������ �곗�댄��
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : LOT별재고조회 목록 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : LOT별재고조회 목록 조회(kcc홈씨씨)
     * 작성자                      : ykim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_kcc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        map.put("LIST", dao.list_kcc(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : LOT재고 목록 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }  
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : LOT별재고현황 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 	, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  	, model.get("ST_GUBUN"+i));

                modelDt.put("STOCK_ID"      , model.get("STOCK_ID"+i));
                modelDt.put("LOCK_YN"     	, model.get("LOCK_YN"+i));
                
                modelDt.put("LC_ID"    		, model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("WORK_IP"		, model.get(ConstantIF.SS_CLIENT_IP));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSTG070);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    //dao.insert(modelDt);
                	dao.update(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
