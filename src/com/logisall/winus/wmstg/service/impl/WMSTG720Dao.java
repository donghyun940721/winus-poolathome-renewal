package com.logisall.winus.wmstg.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG720Dao")
public class WMSTG720Dao extends SqlMapAbstractDAO{

	/**
     * Method ID : list
     * Method 설명 : 창고정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmstg720.list", model);
    }   
}
