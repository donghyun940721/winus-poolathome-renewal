package com.logisall.winus.wmstg.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmstg.service.WMSTG899Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG899Service")
public class WMSTG899ServiceImpl implements WMSTG899Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG899Dao")
    private WMSTG899Dao dao;
    
    /*-
	 * Method ID	: getListCard
	 * Method 설명	: 
	 * 작성자			: 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> getListCard(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if(		 srchKey.equals("CARD_A")){
			map.put("CARD_A", dao.getListCard_A(model));
		}else if(srchKey.equals("CARD_B")){
			map.put("CARD_B", dao.getListCard_B(model));
		}else if(srchKey.equals("CARD_C")){
			map.put("CARD_C", dao.getListCard_C(model));
		}else if(srchKey.equals("CARD_D")){
			map.put("CARD_D", dao.getListCard_D(model));
		}
		
		return map;
	}
    
    /*-
	 * Method ID	: getDetailList
	 * Method 설명	: 
	 * 작성자			: 기드온
	 * @param 
	 * @return
	 */
    @Override
    public Map<String, Object> getDetailList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.getDetailList(model));
        return map;
    }
    
    /*-
	 * Method ID	: getAreaLine
	 * Method 설명	: 
	 * 작성자			: 기드온
	 * @param 
	 * @return
	 */
    @Override
    public Map<String, Object> getAreaLine(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.getAreaLine(model));
        return map;
    }
    
    /*-
	 * Method ID	: getBar
	 * Method 설명	: 
	 * 작성자			: 기드온
	 * @param 
	 * @return
	 */
    @Override
    public Map<String, Object> getBar(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.getBar(model));
        return map;
    }
}
