package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG899Dao")
public class WMSTG899Dao extends SqlMapAbstractDAO{

	/**
     * Method ID	: getListCard
     * Method 설명	: 
     * 작성자			: chsong
     * @param model
     * @return
     */
    public Object getListCard_A(Map<String, Object> model){
        return executeQueryForList("wmstg899.getListCard_A", model);
    }
    public Object getListCard_B(Map<String, Object> model){
        return executeQueryForList("wmstg899.getListCard_B", model);
    }
    public Object getListCard_C(Map<String, Object> model){
        return executeQueryForList("wmstg899.getListCard_C", model);
    }
    public Object getListCard_D(Map<String, Object> model){
        return executeQueryForList("wmstg899.getListCard_D", model);
    }
    
    /**
     * Method ID	: getDetailList
     * Method 설명	: 
     * 작성자			: chsong
     * @param model
     * @return
     */
	public GenericResultSet getDetailList(Map<String, Object> model) {
		return executeQueryPageWq("wmstg899.getDetailList", model);
	}
	
	/**
     * Method ID	: getAreaLine
     * Method 설명	: 
     * 작성자			: chsong
     * @param model
     * @return
     */
	public GenericResultSet getAreaLine(Map<String, Object> model) {
		return executeQueryPageWq("wmstg899.getAreaLine", model);
	}
	
	/**
     * Method ID	: getBar
     * Method 설명	: 
     * 작성자			: chsong
     * @param model
     * @return
     */
	public GenericResultSet getBar(Map<String, Object> model) {
		return executeQueryPageWq("wmstg899.getBar", model);
	}
}


