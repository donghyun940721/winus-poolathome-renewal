package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG733Dao")
public class WMSTG733Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 출고박스조회
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryWq("wmstg733.list", model);
    }   
    /**
     * Method ID : listE2
     * Method 설명 : 출고 시리얼 조회
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	return executeQueryWq("wmstg733.listE2", model);
    }
    
    /**
     * Method ID : listE3
     * Method 설명 : 박스추천조회
     * 작성자 : SUMMER HYUN
     * @param model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
    	return executeQueryWq("wmstg733.listE3", model);
    }
    
    /**
     * Method ID : inExcelUploadTemplate
     * Method 설명 : 출고 시리얼 업로드
     * 작성자 : schan
     * @param model
     * @return
     */
    public Object inExcelUploadTemplate(Map<String, Object> model) {
    	executeUpdate("wmstg733.inExcelUploadTemplate", model);
    	return model;
    }
    
    /**
     * Method ID : deleteSeiral
     * Method 설명 : 선택 시리얼 삭제
     * 작성자 : schan
     * @param model
     * @return
     */
    public Object deleteSeiral(Map<String, Object> model) {
    	executeUpdate("wmstg733.pk_wmsdf010.sp_parcel_serial_chk_delete", model);
    	return model;
    }
    
    /**
     * Method ID : updateRecomBox
     * Method 설명 : 추천박스업데이트(수정)
     * 작성자 : SUMMER HYUN 
     * @param model
     * @return
     */
    public Object updateRecomBox(Map<String, Object> model) {
    	//return executeQueryWq("wmstg733.updateRecomBoxT", model);
    	executeUpdate("wmstg733.updateRecomBox", model);
    	return model;
    }

}

