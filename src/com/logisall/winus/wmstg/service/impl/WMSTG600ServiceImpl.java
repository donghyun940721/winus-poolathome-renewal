package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmstg.service.WMSTG600Service;
//import com.logisall.winus.wmsms.service.impl.WMSMS090Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG600Service")
public class WMSTG600ServiceImpl implements WMSTG600Service {
    protected Log log = LogFactory.getLog(this.getClass());
	
    @Resource(name = "WMSTG600Dao")
    private WMSTG600Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSTG070 = {"STOCK_ID", "LOCK_YN"};

	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
            map.put("ORD01", dao.selectOrd01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	

    /**
     * Method ID : list0
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비 현황 조회
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list0(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
                       
            map.put("LIST", dao.list0(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : list0
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비 현황 엑셀 다운로드
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list0Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list0(model));
        return map;
	}
    
	
	/**
     * Method ID : pList01
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별배송비현황 셀 더블클릭시 팝업조회
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> pList01(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.pList01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : list0
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비 현황 상세 엑셀 다운로드
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listPop0Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.pList01(model));
        return map;
	}
	
	/**
     * Method ID : list
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > ERP 수신 주문정보 조회
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE02(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("orderGrpId", (String)model.get("vrOrderGrpId"));
			
            //orderGrpId : [1:가맹점 2:유통판매 3:정상입고 4:반품입고]
            
			if(map.get("orderGrpId").equals("1")) {
				map.put("LIST", dao.listE02(model));
			}else if(map.get("orderGrpId").equals("2")){
				map.put("LIST", dao.listE02Sale(model));
			}else if(map.get("orderGrpId").equals("3")) {
				map.put("LIST", dao.listE02In(model));
			}else{
				map.put("LIST", dao.listE02Out(model));
			}
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	
	/**
     * Method ID : listE02Excel
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > ERP 수신 주문정보조회 엑셀
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE02Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
        map.put("orderGrpId", (String)model.get("vrOrderGrpId"));
		
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
       //orderGrpId : [1:가맹점 2:유통판매 3:정상입고 4:반품입고]
        
		if(map.get("orderGrpId").equals("1")) {
			map.put("LIST", dao.listE02(model));
		}else if(map.get("orderGrpId").equals("2")){
			map.put("LIST", dao.listE02Sale(model));
		}else if(map.get("orderGrpId").equals("3")) {
			map.put("LIST", dao.listE02In(model));
		}else{
			 map.put("LIST", dao.listE02Out(model));
		}
		
        return map;
	}
	
	/**
     * Method ID : list
     * Method 설명 : 통계관리 > 이디야 조회 테스트 >  일별 ERP 전송 이력 조회 > 입고
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE03(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
			
            map.put("orderId", (String)model.get("vrOrderId"));
            
            //vrOrderId : [1:입고 2:출고 ]
			if(map.get("orderId").equals("1")) {
				 map.put("LIST", dao.listE03(model));
			}else if(map.get("orderId").equals("2")){
				 map.put("LIST", dao.listE03OutOrd(model));
			}
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : listE02Excel
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > ERP 수신 주문정보조회 엑셀
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE03Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("orderId", (String)model.get("vrOrderId"));
        
        //vrOrderId : [1:입고 2:출고 ]
        
		if(map.get("orderId").equals("1")) {
			 map.put("LIST", dao.listE03(model));
		}else if(map.get("orderId").equals("2")){
			 map.put("LIST", dao.listE03OutOrd(model));
		}
		
        return map;
	}
	

	
	/**
     * Method ID : list
     * Method 설명 : 통계관리 > 이디야 조회 테스트 >  ERP 주문 정보 대비 실적 전송 내역> 입고
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE04(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
			
            map.put("orderType", (String)model.get("vrOrderType"));
            
            //vrOrderId : [1:유통 2:정상입고 3:반품입고]
			if(map.get("orderType").equals("1")) {
				 map.put("LIST", dao.listE04(model));
			}else if(map.get("orderType").equals("2")){
				 map.put("LIST", dao.listE04InOrd(model));
			}else if(map.get("orderType").equals("3")){
				 map.put("LIST", dao.listE04RtnOrd(model));
			}
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : listE02Excel
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > ERP 주문정보 대비 실적 전송 내역
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE04Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("orderType", (String)model.get("vrOrderType"));
        
        //vrOrderId : [1:입고 2:출고 ]
        
      //vrOrderId : [1:유통 2:정상입고 3:반품입고]
		if(map.get("orderType").equals("1")) {
			 map.put("LIST", dao.listE04(model));
		}else if(map.get("orderType").equals("2")){
			 map.put("LIST", dao.listE04InOrd(model));
		}else if(map.get("orderType").equals("3")){
			 map.put("LIST", dao.listE04RtnOrd(model));
		}
		
        return map;
	}
	
	/**
     * Method ID : updateE02
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 2.ERP 수신 주문정보 조회 > 삭제 
     * 작성자 : 김채린
     * @param updateE02
     * @return
     * @throws Exception
     */	
	
	@Override
	public Map<String, Object> updateE02(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
//			System.out.println(Integer.parseInt(model.get("selectIds").toString()));
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();			
				
				//session 정보
				modelDt.put("ORD_NO",  model.get("ORD_NO" + i));
				modelDt.put("ORD_DESC",  model.get("ORD_DESC" + i));
				modelDt.put("ITEM_CD",  model.get("ITEM_CD" + i));
				modelDt.put("SS_USER_NO", model.get("SS_USER_NO"));
//				System.out.println(modelDt.get("ORD_DESC"));
				
				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.updateOm010(modelDt);
					dao.updateOp011(modelDt);
				}else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
				
				m.put("MSG", MessageResolver.getMessage("삭제되었습니다"));
				
			}
			
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	@Override
	public Map<String, Object> updateE03(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
//			System.out.println(Integer.parseInt(model.get("selectIds").toString()));
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();			
				
				//session 정보
				modelDt.put("ORD_NO",  model.get("ORD_NO" + i));
				modelDt.put("ORD_DESC",  model.get("ORD_DESC" + i));
				modelDt.put("ITEM_CD",  model.get("ITEM_CD" + i));
				modelDt.put("TO_ITEM_CD",  model.get("TO_ITEM_CD" + i));
				modelDt.put("SS_USER_NO", model.get("SS_USER_NO"));
				modelDt.put("ORD_GRPID", model.get("ORD_GRPID"+ i));
				modelDt.put("LC_ID", model.get("LC_ID"+ i));
				
//				System.out.println("ITEM_CD : "+ model.get("ITEM_CD" + i));
//				System.out.println("TO_ITEM_CD : "+ model.get("TO_ITEM_CD" + i));
//				System.out.println("LC_ID : "+ model.get("LC_ID" + i));
				
				if ("1".equals(model.get("ORD_GRPID" + i))) {
					
//					System.out.println("1. 가맹점");
					dao.updateOm010Item(modelDt);					//WMSOM010 UPDATE
					dao.updateOp011Item(modelDt);					//WMSOP011 UPDATE
					dao.updateTwmsOrderItem(modelDt);					//TWMS_IF_ERP_ORDER_D@WINUSUSR_IF
					
				}else if("2".equals(model.get("ORD_GRPID" + i))){
					
//					System.out.println("2. 유통");
					dao.updateOm010Item(modelDt);					//WMSOM010 UPDATE
					dao.updateOp011Item(modelDt);					//WMSOP011 UPDATE
					dao.updateTwmsPosItem(modelDt);					//TWMS_IF_ERP_POS_DISTRIBUTED@WINUSUSR_IF
					
					
				}else if("3".equals(model.get("ORD_GRPID" + i))){
					
//					System.out.println("3. 정상");
					dao.updateOm010Item(modelDt);					//WMSOM010 UPDATE
					dao.updateOp011Item(modelDt);					//WMSOP011 UPDATE
					dao.updateErpRcvlItem(modelDt);					//TWMS_IF_ERP_RCVL@WINUSUSR_IF@WINUSUSR_IF
					
				}else if("4".equals(model.get("ORD_GRPID" + i))){
					
//					System.out.println("4.반품");
					dao.updateOm010Item(modelDt);					//WMSOM010 UPDATE
					dao.updateOp011Item(modelDt);					//WMSOP011 UPDATE
					dao.updateCzRetlItem(modelDt);					//TWMS_IF_ERP_CZRETL@WINUSUSR_IF
				}else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
				
				m.put("MSG", MessageResolver.getMessage("변경 되었습니다"));
				
			}
			
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	
	

	/**
     * Method ID : listE05
     * Method 설명 : 통계관리 > 이디야 조회 테스트 >  상품,LOT별 거래처 출고실적 조회
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE05(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
			map.put("LIST", dao.listE05(model));
			
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : listE05
     * Method 설명 : 통계관리 > 이디야 조회 테스트 >  상품,LOT별 거래처 출고실적 엑셀
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE05Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "1000000");
        map.put("LIST", dao.listE05(model));
		
        return map;
	}
	
	 /**
     * Method ID : listE06
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 출고현황
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE06(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
                       
            map.put("LIST", dao.listE06(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	@Override
	public Map<String, Object> listE06Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "1000000");
        map.put("LIST", dao.listE06(model));
		
        return map;
	}

	
	/**
     * Method ID : list0
     * Method 설명 : 통계관리 > 이디야 조회 테스트 > 기간별 배송비 현황 조회
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> listE07(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
                       
            map.put("LIST", dao.listE07(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
}