package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmstg.service.WMSTG999Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSTG999Service")
public class WMSTG999ServiceImpl implements WMSTG999Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSTG999Dao")
    private WMSTG999Dao dao;
    
    /**
     * 
     * 대체 Method ID   : selectLocList
     * 대체 Method 설명    : 제품군별재고조회 화면에서 필요한 데이터
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LOCLIST", dao.selectLocList(model));
        map.put("POOL"   , dao.selectPool(model));
        return map;
    }
    
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 로케이션별재고 목록 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listGubun
     * 대체 Method 설명    : 로케이션별재고 목록 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listGubun(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listGubun(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listDetail
     * 대체 Method 설명    : 로케이션별재고 목록 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listDetail(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listDetailLocSet
     * 대체 Method 설명    : 로케이션별재고 목록 조회 (LOC 지정 상태 일 경우)
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetailLocSet(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listDetailLocSet(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 창고별재고 목록 엑셀용조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }  
    
    /**
     * Method ID   : listDetailPop
     * Method 설명    : 재고이동 팝업 리스트
     * 작성자               : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listDetailPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if ("Y".equals(model.get("vrNewGb"))) {
            	map.put("LIST", dao.searchDetailType1(model));
            } else if ("G".equals(model.get("vrNewGb"))) {
            	map.put("LIST", dao.searchDetailType2(model));
            } else {
            	map.put("LIST", dao.searchDetailType3(model));
            }
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : reMapping
     * 대체 Method 설명    : 재매핑
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> reMapping(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 	, model.get("selectIds"));
                modelDt.put("ORD_ID"  		, model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"    	, model.get("ORD_SEQ"+i));
                modelDt.put("RTI_EPC_CD"    , model.get("RTI_EPC_CD"+i));
                modelDt.put("LC_ID"         , (String)model.get(ConstantIF.SS_SVC_NO));                
                modelDt.put("USER_NO"       , (String)model.get(ConstantIF.SS_USER_NO));
                modelDt.put("WORD_IP"       , (String)model.get(ConstantIF.SS_CLIENT_IP));

                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.reMapping_WMSRF010(modelDt);
                    dao.reMapping_WMSRF011(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    public Map<String, Object> locSetSearch(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LOC_EPC", dao.locSetSearch(model));
		return map;
	}
    
    public Map<String, Object> locSetSearchRun(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("LOC_ROWCNT", dao.locSetSearchRun(model));
		return map;
	}
    
    public Map<String, Object> errCntView(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("ERRCNTVIEW")) {
			map.put("ERRCNTVIEW", dao.errCntView(model));
		}
		return map;
	}

}
