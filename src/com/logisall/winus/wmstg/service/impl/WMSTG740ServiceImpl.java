package com.logisall.winus.wmstg.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmstg.service.WMSTG740Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSTG740Service")
public class WMSTG740ServiceImpl extends AbstractServiceImpl implements WMSTG740Service {
    
    @Resource(name = "WMSTG740Dao")
    private WMSTG740Dao dao;

    
    /**
     * 대체 Method ID   : list_T1_kcc
     * 대체 Method 설명    : 출고 리드타임(kcc홈씨씨)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_T1_kcc(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("LIST", dao.list_T1_kcc(model));
    	return map;
    }
    
    /**
     * 대체 Method ID   : list_T2_kcc
     * 대체 Method 설명    : 출고 리드타임(kcc홈씨씨)
     * 작성자                      : dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_T2_kcc(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("LIST", dao.list_T2_kcc(model));
    	return map;
    }
    
    /**
     * 대체 Method ID   : list_T3_kcc
     * 대체 Method 설명    : 입고/출고/반품 실적(kcc홈씨씨)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_T3_kcc(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("LIST", dao.list_T3_kcc(model));
    	return map;
    }
    
    /**
     * 대체 Method ID   : list_T4_kcc
     * 대체 Method 설명    : 장기재고율(kcc홈씨씨)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_T4_kcc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        map.put("LIST", dao.list_T4_kcc(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : list_T5_kcc
     * 대체 Method 설명    : 장기재고율(kcc홈씨씨)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_T5_kcc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        map.put("LIST", dao.list_T5_kcc(model));
        return map;
    }    
    
    /**
     * 대체 Method ID   : list_T5_kcc
     * 대체 Method 설명    : 장기재고율(kcc홈씨씨)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_T7_kcc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        map.put("LIST", dao.list_T7_kcc(model));
        return map;
    }        
    
    /**
     * 대체 Method ID   : list_T9_kcc
     * 대체 Method 설명    : 결품율(kcc홈씨씨)
     * 작성자                      : dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_T9_kcc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        map.put("LIST", dao.list_T9_kcc(model));
        return map;
    }    
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    

    /**
     * 대체 Method ID   : list_T11_kcc
     * 대체 Method 설명    : 일별 전체작업진행현황(B2B)
     * 작성자                      : yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list_T11_kcc(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("LIST", dao.list_T11_kcc(model));
    	return map;
    }
    
    
}
