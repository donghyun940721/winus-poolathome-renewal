package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG080Dao")
public class WMSTG080Dao extends SqlMapAbstractDAO{
	/**
     * Method ID : prevList
     * Method 설명 : 유통기한별 재고 조회 지정기간
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
    public GenericResultSet prevList(Map<String, Object> model) {
        return executeQueryPageWq("wmstg080.prevList", model);
    }   
    
    /**
     * Method ID : curList
     * Method 설명 : 유통기한별 재고 조회 지정기간 + 현재고 + 기발주
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
    public GenericResultSet curList(Map<String, Object> model) {
        return executeQueryPageWq("wmstg080.curList", model);
    }   
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
}
