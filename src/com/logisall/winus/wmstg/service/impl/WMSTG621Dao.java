package com.logisall.winus.wmstg.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSTG621Dao")
public class WMSTG621Dao extends SqlMapAbstractDAO{

		/**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 공통
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : list
     * Method 설명 : 단가 조회
     * 작성자 : sing09
     * @param model
     * @return
     */
	public GenericResultSet list(Map<String, Object> model) throws Exception {
		return executeQueryPageWq("wmstg621.list", model);
	}
    
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmstg621.update", model);
	}
	
    /**
     * Method ID   		: listSummaryCount
     * Method 설명      : 입출고내역 COUNT SUMMARY
     * 작성자           : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
	public Object listSummaryCount(Map<String, Object> model) {
		return executeView("wmstg621.listSummaryCount", model);
	}
}
