package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG200Service {
    public Map<String, Object> selectPoolGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> DetailPop(Map<String, Object> model) throws Exception;
}
