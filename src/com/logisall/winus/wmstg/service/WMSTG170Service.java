package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG170Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
