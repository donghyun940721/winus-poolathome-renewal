package com.logisall.winus.wmstg.service;

import java.util.List;
import java.util.Map;


public interface WMSTG733Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> inExcelUploadTemplate(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> deleteSeiral(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateRecomBox(Map<String, Object> model) throws Exception;
}
