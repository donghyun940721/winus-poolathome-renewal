package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG070Service {
	public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
}
