package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG899Service {
	public Map<String, Object> getListCard(Map<String, Object> model) throws Exception;
	public Map<String, Object> getDetailList(Map<String, Object> model) throws Exception;
	public Map<String, Object> getAreaLine(Map<String, Object> model) throws Exception;
	public Map<String, Object> getBar(Map<String, Object> model) throws Exception;
}
