package com.logisall.winus.wmstg.service;

import java.util.Map;


public interface WMSTG740Service {
    public Map<String, Object> list_T1_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_T2_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_T3_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_T4_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_T5_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_T7_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_T9_kcc(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list_T11_kcc(Map<String, Object> model) throws Exception;
}

