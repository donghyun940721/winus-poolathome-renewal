package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST055Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}
