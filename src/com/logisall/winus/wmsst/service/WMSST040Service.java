package com.logisall.winus.wmsst.service;

import java.util.List;
import java.util.Map;


public interface WMSST040Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listHeader(Map<String, Object> model) throws Exception;
    public Map<String, Object> del(Map<String, Object> model) throws Exception;
    public Map<String, Object> work(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveGrn(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listRackDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> DetailPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> workStatPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelTotal(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model) throws Exception;
}
