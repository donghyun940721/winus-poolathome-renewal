package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST160Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST160Service")
public class WMSST160ServiceImpl extends AbstractServiceImpl implements WMSST160Service {
    
    @Resource(name = "WMSST160Dao")
    private WMSST160Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 순환재고조사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
   
    
    /**
     * 대체 Method ID    : selectBox
     * 대체 Method 설명      : 물류용기관리 화면 데이타셋
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("inKey", "CYCL01");
        map.put("CYCL01", dao.selectCYCL01(model));
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 임가공 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> main = new HashMap<String, Object>();
        int errCnt = 0;

            try{
                int iuCnt = Integer.parseInt(model.get("selectIds").toString());
                
                if(iuCnt > 0){
                    for(int i=0; i<iuCnt; i++){
                        main.put("ST_GUBUN", model.get("ST_GUBUN"+i));
                        main.put("CYCL_STOCK_ID", model.get("CYCL_STOCK_ID"+i));
                        main.put("WORK_SEQ", model.get("WORK_SEQ"+i));
                        main.put("RITEM_ID", model.get("RITEM_ID"+i));
                        main.put("SS_USER_NO", model.get("SS_USER_NO"));
                        
                        main.put("REAL_QTY", model.get("REAL_QTY"+i));
                        main.put("DIFF_REASON", model.get("DIFF_REASON"+i));
                        main.put("ERR_CD", model.get("ERR_CD"+i));
                        
                        if(main.get("ST_GUBUN").equals("UPDATE")){
                            dao.update(main);
                        }else{
                            errCnt++;
                        }
                    }
                }else{
                    errCnt++;
                }
                
                if(errCnt == 0){
                    m.put("MSG", MessageResolver.getMessage("save.success"));
                    m.put("errCnt", errCnt);
                }else{
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                    m.put("errCnt", errCnt);
                }
            }catch (Exception e) {
                throw e;
            }
        return m;
    }
    
    
    /**
     * Method ID : save_T2
     * Method 설명 : 임가공 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save_T2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> main = new HashMap<String, Object>();
        int errCnt = 0;

            try{
                int iuCnt = Integer.parseInt(model.get("selectIds").toString());
                
                if(iuCnt > 0){
                    for(int i=0; i<iuCnt; i++){
                        main.put("ST_GUBUN", model.get("ST_GUBUN"+i));
                        main.put("CYCL_STOCK_ID", model.get("CYCL_STOCK_ID"+i));
                        main.put("WORK_SEQ", model.get("WORK_SEQ"+i));
                        main.put("RITEM_ID", model.get("RITEM_ID"+i));
                        main.put("SS_USER_NO", model.get("SS_USER_NO"));
                        
                        main.put("REAL_QTY", model.get("REAL_QTY"+i));
                        main.put("DIFF_REASON", model.get("DIFF_REASON"+i));
                        main.put("ERR_CD", model.get("ERR_CD"+i));
                        main.put("UPD_NO", model.get("UPD_NO"+i));
                
                        
                        dao.update_T2(main);
                    }
                }else{
                    errCnt++;
                }
                
                if(errCnt == 0){
                    m.put("MSG", MessageResolver.getMessage("save.success"));
                    m.put("errCnt", errCnt);
                }else{
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                    m.put("errCnt", errCnt);
                }
            }catch (Exception e) {
                throw e;
            }
        return m;
    }
    
    
    @Override
    public Map<String, Object> orderInsert(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            int totCnt = Integer.parseInt(model.get("selectIds").toString());
            String[] lcId        = new String[totCnt];
            String[] custId      = new String[totCnt];
            String[] LocId       = new String[totCnt];
            String[] ExpQty      = new String[totCnt];
            String[] RealQty     = new String[totCnt];
            String[] DiffReason  = new String[totCnt];
            String[] WhId        = new String[totCnt];
            String[] WorkStat    = new String[totCnt];
            String[] WorkSeq     = new String[totCnt];
            String[] CyclStockId = new String[totCnt];
            String[] ErrCd       = new String[totCnt];
            String[] WorkDt      = new String[totCnt];
            String[] RitemId     = new String[totCnt];
            String[] uomId       = new String[totCnt];
            
            Map<String, Integer> pdaSeqMap = new HashMap<String, Integer>();
            
            for(int i = 0 ; i < totCnt ; i++){
            	lcId [i]        = (String)model.get("LC_ID"+i);
            	custId [i]      = (String)model.get("CUST_ID"+i);
            	LocId [i]       = (String)model.get("LOC_ID"+i);
            	ExpQty [i]      = (String)model.get("EXP_QTY"+i);
            	RealQty [i]     = (String)model.get("REAL_QTY"+i);
            	DiffReason [i]  = (String)model.get("DIFF_REASON"+i);
            	WhId [i]        = (String)model.get("WH_ID"+i);
            	WorkStat [i]    = (String)model.get("WORK_STAT"+i);
            	WorkSeq [i]     = (String)model.get("WORK_SEQ"+i);
            	CyclStockId [i] = (String)model.get("CYCL_STOCK_ID"+i);
            	ErrCd [i]       = (String)model.get("ERR_CD"+i);
            	WorkDt [i]      = (String)model.get("WORK_DT"+i);
            	RitemId [i]     = (String)model.get("RITEM_ID"+i);
            	uomId [i]       = (String)model.get("UOM_ID"+i);

            	/*
            	System.out.println( "1 : " + lcId [i]    );   
            	System.out.println( "2 : " + custId [i]     );
            	System.out.println( "3 : " + LocId [i]      );
            	System.out.println( "4 : " + ExpQty [i]     );
            	System.out.println( "5 : " + RealQty [i]    );
            	System.out.println( "6 : " + DiffReason [i] );
            	System.out.println( "7 : " + WhId [i]       );
            	System.out.println( "8 : " + WorkStat [i]   );
            	System.out.println( "9 : " + WorkSeq [i]    );
            	System.out.println( "10 : " + CyclStockId [i]);
            	System.out.println( "11 : " + ErrCd [i]      );
            	System.out.println( "12 : " + WorkDt [i]     );
            	System.out.println( "13 : " + RitemId [i]    );
            	System.out.println( "14 : " + RitemId [i]    );
            	System.out.println( "15 : " + (String)model.get(ConstantIF.SS_USER_NO)    );
            	System.out.println( "16 : " + (String)model.get(ConstantIF.SS_CLIENT_IP)    );
            	*/

            }
            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("LC_ID"         , lcId);
            modelIns.put("CUST_ID"       , custId);
            modelIns.put("LOC_ID"        , LocId);
            modelIns.put("EXP_QTY"       , ExpQty);
            modelIns.put("REAL_QTY"      , RealQty);
            modelIns.put("DIFF_REASON"   , DiffReason);
            modelIns.put("WH_ID"         , WhId);
            modelIns.put("WORK_STAT"     , WorkStat);
            modelIns.put("WORK_SEQ"      , WorkSeq);
            modelIns.put("CYCL_STOCK_ID" , CyclStockId);
            modelIns.put("ERR_CD"        , ErrCd);
            modelIns.put("WORK_DT"       , WorkDt);
            modelIns.put("RITEM_ID"      , RitemId);
            modelIns.put("UOM_ID"        , uomId);
            modelIns.put("USER_NO"       , (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("WORK_IP"       , (String)model.get(ConstantIF.SS_CLIENT_IP));
            //modelIns.put("lcId"          , (String)model.get(ConstantIF.SS_SVC_NO));
            
            modelIns = (Map<String, Object>)dao.orderInsert(modelIns);
            ServiceUtil.isValidReturnCode("WMSST160", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    
    /**
     * Method ID : listExcel
     * Method 설명 : 순환재고조사 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   	: inExcelUploadTemplate
     * 대체 Method 설명     : 템플릿 업로드 
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> inExcelUploadTemplate(Map<String, Object> model,  List list) throws Exception {
    	int listBodyCnt = (list != null)?list.size():0;
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		if(listBodyCnt > 0){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			String[] CUST_NM = new String[listBodyCnt];
    			String[] LOC_CD = new String[listBodyCnt];
    			String[] RITEM_CD = new String[listBodyCnt];
    			String[] RITEM_NM = new String[listBodyCnt];
    			String[] EXP_QTY = new String[listBodyCnt];
    			String[] REAL_QTY = new String[listBodyCnt];
    			String[] DIFF_REASON = new String[listBodyCnt];
    			String[] ERR_CD = new String[listBodyCnt];
    			String[] BEST_DATE = new String[listBodyCnt];
    			String[] WORK_STAT = new String[listBodyCnt];
    			String[] WORK_DT = new String[listBodyCnt];
    			String[] CYCL_STOCK_ID = new String[listBodyCnt];
    			String[] WORK_SEQ = new String[listBodyCnt];
    			for(int i = 0 ; i < listBodyCnt ; i ++){
    				Map<String,String> temp = (Map<String,String>)list.get(i);
    				CUST_NM[i] = temp.get("CUST_NM");
    				LOC_CD[i] = temp.get("LOC_CD");
    				RITEM_CD[i] = temp.get("RITEM_CD");
    				RITEM_NM[i] = temp.get("RITEM_NM");
    				EXP_QTY[i] = temp.get("EXP_QTY");
    				REAL_QTY[i] = temp.get("REAL_QTY");
    				DIFF_REASON[i] = temp.get("DIFF_REASON");
    				ERR_CD[i] = temp.get("ERR_CD");
    				BEST_DATE[i] = temp.get("BEST_DATE");
    				WORK_STAT[i] = temp.get("WORK_STAT");
    				WORK_DT[i] = temp.get("WORK_DT");
    				CYCL_STOCK_ID[i] = temp.get("CYCL_STOCK_ID");
    				WORK_SEQ[i] = temp.get("WORK_SEQ");
    			}
    			modelDt.put("CUST_NM",CUST_NM);
    			modelDt.put("LOC_CD",LOC_CD);
    			modelDt.put("RITEM_CD",RITEM_CD);
    			modelDt.put("RITEM_NM",RITEM_NM);
    			modelDt.put("EXP_QTY",EXP_QTY);
    			modelDt.put("REAL_QTY",REAL_QTY);
    			modelDt.put("DIFF_REASON",DIFF_REASON);
    			modelDt.put("ERR_CD",ERR_CD);
    			modelDt.put("BEST_DATE",BEST_DATE);
    			modelDt.put("WORK_STAT",WORK_STAT);
    			modelDt.put("WORK_DT",WORK_DT);
    			modelDt.put("CYCL_STOCK_ID",CYCL_STOCK_ID);
    			modelDt.put("WORK_SEQ",WORK_SEQ);

	            modelDt.put("LC_ID", model.get(ConstantIF.SS_SVC_NO));
	            modelDt.put("WORK_IP", model.get(ConstantIF.SS_CLIENT_IP));
	            modelDt.put("USER_NO", model.get(ConstantIF.SS_USER_NO));
	            
    			dao.inExcelUploadTemplate(modelDt);
    		}
    		//TEMPLATE_ID, TEMPLATE_TYPE => parameter로 받아오기.
    		
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
    		
    	}catch(Exception e) {
    	   throw e;
    	}
       
        return m;
          
    }

       
}
