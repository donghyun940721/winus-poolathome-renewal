package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST190Dao")
public class WMSST190Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID  : selectPoolGrp
     * Method 설명  : 화면내 필요한 용기군 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectPoolGrp(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 개체재고및이력추적 개체재고리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst190.list", model);
    }   

    /**
     * Method ID  : listSub 
     * Method 설명  : 개체재고및이력추적 이력추적이동
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listSub(Map<String, Object> model){
        return executeQueryPageWq("wmsst190.listSub", model);
    }
    
    /**
     * Method ID  : DetailPop 
     * Method 설명  : 개체재고및이력추적 이력추적이동
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet DetailPop(Map<String, Object> model){
        return executeQueryPageWq("wmsst190.DetailPop", model);
    }
    
    /**
    * Method ID  : selectPool
    * Method 설명  : Zone 데이터셋
    * 작성자             : 기드온
    * @param model
    * @return
    */
    public Object selectPool(Map<String, Object> model){
    return executeQueryForList("wmsmo907.selectPool", model);
    }
}
