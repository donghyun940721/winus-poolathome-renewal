package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST230Dao")
public class WMSST230Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : list
     * Method 설명 : 현재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst230.list", model);
    }   
    
    /**
     * Method ID : sublist
     * Method 설명 : 현재고 서브 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublist(Map<String, Object> model){
        return executeQueryPageWq("wmsst230.sublist", model);
    }
    
    /**
	 * Method ID  : selectLc
	 * Method 설명  : selectLc
	 * 작성자             : chsong
	 * @param model
	 * @return
	 */
	public Object selectLc(Map<String, Object> model){
		return executeQueryForList("wmsst230.selectLc", model);
	}
	
	/**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : hListExcel
     * Method 설명 : 현재고 조회(센터별처리현황) 헤더 엑셀 다운로드
     * 작성자 : KSJ
     * @param model
     * @return
     */
    public GenericResultSet hListExcel(Map<String, Object> model) {
        return executeQueryPageWq("wmsst230.hListExcel", model);
    }   
    
    /**
     * Method ID : dListExcel
     * Method 설명 : 현재고 조회(센터별처리현황) 디테일 엑셀 다운로드
     * 작성자 : KSJ
     * @param model
     * @return
     */
    public GenericResultSet dListExcel(Map<String, Object> model) {
        return executeQueryPageWq("wmsst230.dListExcel", model);
    }   
}
