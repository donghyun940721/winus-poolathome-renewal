package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST290Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST290Service")
public class WMSST290ServiceImpl extends AbstractServiceImpl implements WMSST290Service {
    
    @Resource(name = "WMSST290Dao")
    private WMSST290Dao dao;

    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
//            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
//            map.put("ORD01", dao.selectOrd01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
    
    /**
     * Method ID : list
     * Method 설명 : 
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    public Map<String, Object> sublist(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.sublist(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    

    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
  
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            String[] custId     = new String[iuCnt];
            String[] ritemId	= new String[iuCnt];
            String[] workQty	= new String[iuCnt];
            String[] memo		= new String[iuCnt];
            
//            String[] workIp		= new String[iuCnt];
//            String[] userNo		= new String[iuCnt];
            
            for(int i = 0 ; i < iuCnt; i ++){
            	ritemId[i]      = (String) model.get("RITEM_ID"+i);
                workQty[i]      = (String) model.get("WORK_QTY"+i);
                custId[i]		= (String) model.get("CUST_ID"+i);
                memo[i]			= (String) model.get("MEMO"+i);
            }
            if(iuCnt > 0){
            	Map<String, Object> modelSP = new HashMap<String, Object>();
            	modelSP.put("I_SET_RITEM_ID"	, ritemId);
            	modelSP.put("I_WORK_QTY"		, workQty);
            	modelSP.put("I_CUST_ID"			, custId);
            	modelSP.put("I_MEMO"			, memo);
            	modelSP.put("I_LC_ID"		, model.get("SS_SVC_NO"));
            	modelSP.put("I_WORK_IP"		, model.get("SS_CLIENT_IP"));
            	modelSP.put("I_USER_NO"		, model.get("SS_USER_NO"));
            	
//            	System.out.println("여기1.?? "+ model.get("SS_SVC_NO"));
//            	System.out.println("여기2. custId : ?? "+ custId);
            	
            	dao.save(modelSP);
                
//                m.put("errCnt", errCnt);
            } 
            m.put("MSG", MessageResolver.getMessage("update.success"));
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    public Map<String, Object> dlt(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
  
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            String[] custId     = new String[iuCnt];
            String[] ritemId	= new String[iuCnt];
            String[] workQty	= new String[iuCnt];
            String[] memo		= new String[iuCnt];
            
//            String[] workIp		= new String[iuCnt];
//            String[] userNo		= new String[iuCnt];
            
            for(int i = 0 ; i < iuCnt; i ++){
            	ritemId[i]      = (String) model.get("RITEM_ID"+i);
                workQty[i]      = (String) model.get("WORK_QTY"+i);
                custId[i]		= (String) model.get("CUST_ID"+i);
                memo[i]			= (String) model.get("MEMO"+i);
            }
            if(iuCnt > 0){
            	Map<String, Object> modelSP = new HashMap<String, Object>();
            	modelSP.put("I_SET_RITEM_ID"	, ritemId);
            	modelSP.put("I_WORK_QTY"		, workQty);
            	modelSP.put("I_CUST_ID"			, custId);
            	modelSP.put("I_MEMO"			, memo);
            	modelSP.put("I_LC_ID"		, model.get("SS_SVC_NO"));
            	modelSP.put("I_WORK_IP"		, model.get("SS_CLIENT_IP"));
            	modelSP.put("I_USER_NO"		, model.get("SS_USER_NO"));
            	
            	dao.dlt(modelSP);
                
//                m.put("errCnt", errCnt);
            } 
            m.put("MSG", MessageResolver.getMessage("update.success"));
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    
    
    
    
}
