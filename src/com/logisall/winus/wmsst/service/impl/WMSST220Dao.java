package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST220Dao")
public class WMSST220Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID : list
     * Method 설명 : 현재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object list(Map<String, Object> model){
        return executeQueryForList("wmsst220.list", model);
    }
    
    /**
     * Method ID : list
     * Method 설명 : 현재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object listExcel(Map<String, Object> model){
        return executeQueryPageWq("wmsst220.list", model);
    }
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : sublist
     * Method 설명 : 현재고 서브 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet sublist(Map<String, Object> model){
        return executeQueryPageWq("wmsst220.sublist", model);
    }
    
    
    /**
     * Method ID : delete
     * Method 설명 : 현재고 비고 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model){
        return executeUpdate("wmsst220.update", model);
    }
    
    /**
     * Method ID  : save
     * Method 설명  : 현재고 PLT 수량 수정
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object save(Map<String, Object> model){
        return  executeUpdate("wmsst220.pk_wmsst220.sp_save_plt_qty", model);
    }
    
    /**
     * Method ID : poplist
     * Method 설명 : 세트상품 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet poplist(Map<String, Object> model) {
        return executeQueryPageWq("wmsst220.searchSet", model);
    }   
    
    /**
     * Method ID : popdetail
     * Method 설명 : 세트상품 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet popdetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst220.subSearch", model);
    }   
}
