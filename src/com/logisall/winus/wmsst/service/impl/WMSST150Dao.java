package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST150Dao")
public class WMSST150Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : listBlNo
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listEpccd(Map<String, Object> model) {
        return executeQueryPageWq("wmsst150.listEpccd", model);
    }
    
 

    /**
     * Method ID  : save
     * Method 설명  : 재고 입출고내역 작업일자, 작업id 수정 프로시저
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object save(Map<String, Object> model){
        return  executeUpdate("wmsst020.pk_wmsop001.sp_set_workdate", model);
    }
    
    /**
	 * Method ID : update 
	 * Method 설명 : UOM코드 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateSapCd(Map<String, Object> model) {
		return executeUpdate("wmsst150.updateSapCd", model);
	}
	
	/**
	* Method ID  : selectPool
	* Method 설명  : Zone 데이터셋
	* 작성자             : 기드온
	* @param model
	* @return
	*/
	public Object selectPool(Map<String, Object> model){
	return executeQueryForList("wmsmo907.selectPool", model);
	}

}
