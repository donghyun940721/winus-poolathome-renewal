package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST040Dao")
public class WMSST040Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID : list
     * Method 설명 : 재고이동 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst040.list", model);
    }
    
    /**
      * Method ID : detail
      * Method 설명 : 재고이동 상세조회
      * 작성자 : 기드온
      * @param model
      * @return
      */
     public GenericResultSet detail(Map<String, Object> model) {
         return executeQueryPageWq("wmsst040.detail", model);
     }

     /**
      * Method ID : listExcel
      * Method 설명 : 재고이동 조회
      * 작성자 : 기드온
      * @param model
      * @return
      */
     public GenericResultSet listExcel(Map<String, Object> model) {
         return executeQueryPageWq("wmsst040.listExcel", model);
     }
     
    /**
     * Method ID : selectOrd01
     * Method 설명 : 주문종류 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectOrd03
     * Method 설명 : 주문단계 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectOrd03(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectOrd04
     * Method 설명 : 주문상태 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectOrd04(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectSts00
     * Method 설명 : 작업상태 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectSts00(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectSts10
     * Method 설명 : 작업진행상태 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectSts10(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : searchHead
     * Method 설명 : 재고이동 pop 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet searchHead(Map<String, Object> model) {
        return executeQueryPageWq("wmsst040.searchHead", model);
    }
    
    /**
     * Method ID : searchDetail
     * Method 설명 : 재고이동 pop 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet searchDetailType1(Map<String, Object> model) {
        return executeQueryPageWq("wmsst040.searchDetailType1", model);
    }
    
    /**
     * Method ID : searchDetail
     * Method 설명 : 재고이동 pop 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet searchDetailType2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst040.searchDetailType2", model);
    }

    /**
     * Method ID : searchDetail
     * Method 설명 : 재고이동 pop 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet searchDetailType3(Map<String, Object> model) {
        return executeQueryPageWq("wmsst040.searchDetailType3", model);
    }
    
    /**
     * Method ID  : del
     * Method 설명  : 재고이동 지시 취소
     * 작성자             : 김진석
     * @param model
     * @return
     */
    public Object del(Map<String, Object> model){
        executeUpdate("wmsst040.pk_wmsst040.pc_del_movestock", model);
        return model;
    }
    
    /**
     * Method ID  : work
     * Method 설명  : 재고이동 지시
     * 작성자             : 김진석
     * @param model
     * @return
     */
    public Object work(Map<String, Object> model){
        executeUpdate("wmsst040.pk_wmsst040.pc_work_movestock", model);
        return model;
    }
    

    /**
     * Method ID  : save
     * Method 설명  : 재고이동 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */
    
    public Object save(Map<String, Object> model){
        executeUpdate("wmsst040.pk_wmsst040.pc_sav_movestock", model);
        return model;
    }
    
    /**
     * Method ID  : save_grn
     * Method 설명  : 재고이동 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object saveGrn(Map<String, Object> model){
        executeUpdate("wmsst040.pk_wmsst040.pc_sav_movestock_grn", model);
        return model;
    }
    /**
     * Method ID   : listRackDetail
     * Method 설명    : 재고이동 팝업 리스트2 (lack보충을 통한 재고이동)
     * 작성자               : chSong
     * @param model
     * @return
     */
    public GenericResultSet listRackDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.searchLackDetail", model);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
  * Method ID : listBarcode
  * Method 설명 : 재고 입출고내역 조회 (바코드)
  * 작성자 : 기드온
  * @param model
  * @return
  */
 public GenericResultSet listBarcode(Map<String, Object> model) {
     return executeQueryPageWq("wmsst020.listBarcode", model);
 }   

    
    /**
     * Method ID    : listSum
     * Method 설명      : 재고 입출고내역 합계
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object listSum(Map<String, Object> model) {
        return executeView("wmsst020.listSum", model);
    }
    /**
     * Method ID    : listBarcodeSum
     * Method 설명      : 재고 입출고내역 합계(바코드)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Object listBarcodeSum(Map<String, Object> model) {
        return executeView("wmsst020.listBarcodeSum", model);
    }
    
    /**
     * Method ID  : selectLotLike
     * Method 설명  : Lot 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectLotLike(Map<String, Object> model){
        return executeQueryForList("wmsst040.selectLotLike", model);
    }
    
    /**
     * Method ID  : DetailPop 
     * Method 설명   : 재고이동 통합조회 팝업
     * 작성자                 : chsong
     * @param model
     * @return
     */
    public GenericResultSet DetailPop(Map<String, Object> model){
        return executeQueryPageWq("wmsst040.DetailPop", model);
    }
    
    /**
     * Method ID  : workStatPop 
     * Method 설명   : 재고이동 통합조회 팝업
     * 작성자                 : chsong
     * @param model
     * @return
     */
    public GenericResultSet workStatPop(Map<String, Object> model){
        return executeQueryPageWq("wmsst040.workStatPop", model);
    }
    
    /**
     * Method ID  : selectPool
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectPool(Map<String, Object> model){
     return executeQueryForList("wmsmo907.selectPool", model);
     }
     
     /**
      * Method ID  : saveUploadData
      * Method 설명  : 재고이동 엑셀
      * 작성자             : 
      * @param model
      * @return
      */
     public Object saveUploadData(Map<String, Object> model){
         executeUpdate("wmsst040.pk_wmsst040.spSavMoveStockExcel", model);
         return model;
     }
}
