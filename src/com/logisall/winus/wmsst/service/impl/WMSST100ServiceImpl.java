package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST100Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST100Service")
public class WMSST100ServiceImpl extends AbstractServiceImpl implements WMSST100Service {
    
    @Resource(name = "WMSST100Dao")
    private WMSST100Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 명의변경 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 명의변경 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
//    /**
//     * Method ID : save
//     * Method 설명 : 명의변경 저장
//     * 작성자 : 기드온
//     * @param model
//     * @return
//     * @throws Exception
//     */
//    public Map<String, Object> save(Map<String, Object> model) throws Exception {
//        Map<String, Object> m = new HashMap<String, Object>();
//        Map<String, Object> modelMain = new HashMap<String, Object>();
//        int errCnt = 0;
//            System.out.println(model);
//        try{
//              // 상세 내용 저장
//              modelMain.put("I_CHANGE_WORK_ID", model.get("vrChangeWorkId"));
//              modelMain.put("I_WORK_SEQ", model.get("vrWorkSeq"));
//              modelMain.put("I_WORK_DT", model.get("calApplyFrDt_O"));
//              modelMain.put("I_SELL_CUST_ID", model.get("vrContDeptId"));
//              modelMain.put("I_SELL_RITEM_ID", model.get("vrRealDeptId"));
//              modelMain.put("I_SELL_QTY", model.get("sellQty"));
//              modelMain.put("I_SELL_UOM_ID", model.get("SELL_UOM_ID"));
//              modelMain.put("I_BUY_CUST_ID", model.get("vrBuyDeptId"));
//              modelMain.put("I_BUY_RITEM_ID", model.get("vrRealBuyDeptId"));
//              modelMain.put("I_BUY_QTY", model.get("buyQty"));
//              modelMain.put("I_BUY_UOM_ID", model.get("BUY_UOM_ID"));
//              modelMain.put("I_LC_ID", model.get("SS_SVC_NO"));
//              modelMain.put("I_WORK_STAT", model.get("workStat"));
//              modelMain.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
//              modelMain.put("I_USER_NO", model.get("SS_USER_NO"));
//            
//              String[] val = dao.subSave(modelMain);
//             
//              System.out.println("프로시저 결과");
//              System.out.println(val[0]); // 메시지
//              System.out.println(val[1]); // 코드
//              
//              if(!val[1].equals("0")){
//                errCnt++;
//              }
//              
//              if(model.get("vrChangeWorkId").equals("")){
//                  String work_id = dao.search_work_id(model);
//                  m.put("vrChangeWorkId", work_id);
//              }else{
//                  m.put("vrChangeWorkId", model.get("vrChangeWorkId"));
//              }
//              
//              // 그리드 저장 시작
//            int iuCnt = Integer.parseInt(model.get("I_selectIds").toString());
//            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
//            
//            if(iuCnt > 0){
//                
//                String[] changeWorkId = new String[iuCnt]; 
//                String[] workSeq = new String[iuCnt]; 
//                String[] workSubseq = new String[iuCnt]; 
//                String[] sellLocId = new String[iuCnt]; 
//                String[] sellQty = new String[iuCnt]; 
//                
//                String[] sellUomId = new String[iuCnt]; 
//                String[] buyLocId = new String[iuCnt]; 
//                String[] buyQty = new String[iuCnt]; 
//                String[] buyUomId = new String[iuCnt]; 
//                String[] workStat = new String[iuCnt]; 
//                String[] subLotId = new String[iuCnt]; 
//                String[] stockId = new String[iuCnt]; 
//                
//                for(int i=0; i<iuCnt; i++){
//                    if(model.get("I_CHANGE_WORK_ID"+i).equals("")){
//                        String work_id = dao.search_work_id(model);
//                        changeWorkId[i]     = work_id;
//                        m.put("vrChangeWorkId", work_id);
//                    }else{
//                        changeWorkId[i]     = (String)model.get("I_CHANGE_WORK_ID"+i);
//                        m.put("vrChangeWorkId", (String)model.get("I_CHANGE_WORK_ID"+i));
//                    }
//                    workSeq[i]          = (String)model.get("I_WORK_SEQ"+i);
//                    workSubseq[i]       = (String)model.get("I_WORK_SUBSEQ"+i);
//                    sellLocId[i]        = (String)model.get("I_SELL_LOC_ID"+i);
//                    sellQty[i]          = (String)model.get("I_SELL_QTY"+i);
//                    
//                    sellUomId[i]        = (String)model.get("I_SELL_UOM_ID"+i);
//                    buyLocId[i]         = (String)model.get("I_BUY_LOC_ID"+i);
//                    buyQty[i]           = (String)model.get("I_BUY_QTY"+i);
//                    buyUomId[i]         = (String)model.get("I_BUY_UOM_ID"+i);
//                    workStat[i]         = (String)model.get("I_SUB_LOT_ID"+i);
//                    
//                    subLotId[i]         = (String)model.get("I_STOCK_ID"+i);
//                    stockId[i]          = (String)model.get("I_WORK_STAT"+i);
//                }
//
//                Map<String, Object> modelSP = new HashMap<String, Object>();
//                
//                modelSP.put("I_CHANGE_WORK_ID", changeWorkId);
//                modelSP.put("I_WORK_SEQ", workSeq);
//                modelSP.put("I_WORK_SUBSEQ", workSubseq);
//                modelSP.put("I_SELL_LOC_ID", sellLocId);
//                modelSP.put("I_SELL_QTY", sellQty);
//                
//                modelSP.put("I_SELL_UOM_ID", sellUomId);
//                modelSP.put("I_BUY_LOC_ID", buyLocId);
//                modelSP.put("I_BUY_QTY", buyQty);
//                modelSP.put("I_BUY_UOM_ID", buyUomId);
//                modelSP.put("I_SUB_LOT_ID", workStat);
//                
//                modelSP.put("I_STOCK_ID", subLotId);
//                modelSP.put("I_WORK_STAT", stockId);
//
//                modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
//                modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
//                   
//               String[] val2 = dao.save(modelSP);
//               
//               System.out.println("프로시저 결과");
//               System.out.println(val2[0]); // 메시지
//               System.out.println(val2[1]); // 코드
//               
//               if(model.get("I_CHANGE_WORK_ID0").equals("")){
//                   String work_id = dao.search_work_id(model);
//                   m.put("vrChangeWorkId", work_id);                   
//               }else{
//                   m.put("vrChangeWorkId", (String)model.get("I_CHANGE_WORK_ID0"));                   
//               }
//               if(!val2[1].equals("0")){
//                       errCnt++;
//               }               
//                   
//            }
//            
//            // 그리드 삭제 시작
//            if(delCnt > 0){
//                String[] changeWorkId = new String[delCnt]; 
//                String[] workSeq = new String[delCnt]; 
//                String[] workSubseq = new String[delCnt]; 
//                
//                for(int i=0; i<delCnt; i++){
//                    changeWorkId[i]     = (String)model.get("D_CHANGE_WORK_ID"+i);
//                    workSeq[i]          = (String)model.get("D_WORK_SEQ"+i);
//                    workSubseq[i]       = (String)model.get("D_WORK_SUBSEQ"+i);
//                }
//                
//                Map<String, Object> modelSP = new HashMap<String, Object>();
//                
//                modelSP.put("I_CHANGE_WORK_ID", changeWorkId);
//                modelSP.put("I_WORK_SEQ", workSeq);
//                modelSP.put("I_WORK_SUBSEQ", workSubseq);
//
//                modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
//                modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
//                
//                String[] val2 = dao.delete(modelSP);
//                
//                System.out.println("프로시저 결과");
//                System.out.println(val2[0]); // 메시지
//                System.out.println(val2[1]); // 코드
//                m.put("vrChangeWorkId", (String)model.get("D_CHANGE_WORK_ID0"));
//                
//                if(!val2[1].equals("0")){
//                        errCnt++;
//                }               
//            }         
//            
//         
//               if(errCnt == 0){                   
//                   m.put("vrWorkSeq", model.get("vrWorkSeq"));
//                   m.put("MSG", MessageResolver.getMessage("save.success"));
//                   m.put("errCnt", errCnt);
//               }else{
//                   m.put("MSG", MessageResolver.getMessage("save.error")); 
//                   m.put("errCnt", errCnt);
//               }
//            
//        } catch(Exception e){
//            throw e;
//        }
//        return m;
//    }
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
            model.put("inKey", "ACC14"); // 반올림
            map.put("ACC14", dao.selectACC14(model)); 
            model.put("inKey", "STS00"); // 작업상태
            map.put("STS00", dao.selectSTS00(model)); 
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : detail
     * Method 설명 : 명의변경 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DETAIL", dao.detail(model)); // 유저정보 가져오기
        return map; 
    }
    
    /**
     * Method ID : getSubSearch
     * Method 설명 : 명의변경 그리드 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getSubSearch(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.subSearch(model));
            
  
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : saveConfirm
     * Method 설명 : 명의변경
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveConfirm(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{   
                Map<String, Object> modelSP = new HashMap<String, Object>();
                
                modelSP.put("I_CHANGE_WORK_ID", model.get("vrChangeWorkId"));
                modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
                modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
                    
                modelSP = (Map<String, Object>)dao.subConfirm(modelSP);  
                ServiceUtil.isValidReturnCode("WMSST100", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
                
                m.put("vrChangeWorkId", model.get("vrChangeWorkId"));
                m.put("vrWorkSeq", model.get("vrWorkSeq"));
                m.put("errCnt", errCnt);
                m.put("MSG",  MessageResolver.getMessage("name.change"));

        } catch(BizException be) {
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );
        	
        }catch (Exception e) {
            log.error(e.toString());
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }
        return m;
    }
    
    
    /**
     * Method ID : delete
     * Method 설명 : 명의변경 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();        
        // int errCnt = 0;
        try{              
            int delCnt = Integer.parseInt(model.get("selectIds").toString());            
            if(delCnt > 0){
                
                String[] changeWorkId = new String[delCnt]; 
                
                for(int i=0; i<delCnt; i++){
                    changeWorkId[i]     = (String)model.get("CHANGE_WORK_ID"+i);
                }
                    
                Map<String, Object> modelSP = new HashMap<String, Object>();
                
                modelSP.put("I_CHANGE_WORK_ID", changeWorkId);
                modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
                modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
                modelSP = (Map<String, Object>) dao.deleteChangeWork(modelSP);
                ServiceUtil.isValidReturnCode("WMSST100", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
            }
            m.put("vrWorkSeq", model.get("vrWorkSeq"));
            m.put("MSG", MessageResolver.getMessage("delete.success"));
            m.put("errCnt", 0);
                
        } catch(BizException be) {
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );
        	
        }catch (Exception e) {
            log.error(e.toString());
            m.put("MSG", MessageResolver.getMessage("delete.error"));
        }
        return m;
    }
    
    /**
     * Method ID    : save
     * Method 설명      : 명의변경 저장
     * 작성자                 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        //메인저장
        //workId 가져오기
        //그리드 저장
        String workId = "";
        try{
            Map<String, Object> modelResult = new HashMap<String, Object>();
            model.put("calApplyFrDt", model.get("calApplyFrDt").toString().replace("-", ""));
            model.put("lcId", model.get(ConstantIF.SS_SVC_NO));
            model.put("gvUserIP", model.get(ConstantIF.SS_CLIENT_IP));
            model.put("gvUserNo", model.get(ConstantIF.SS_USER_NO));
                        
            modelResult = (Map<String, Object>)dao.subSave(model);
            ServiceUtil.isValidReturnCode("WMSST100", String.valueOf(modelResult.get("O_MSG_CODE")), (String)modelResult.get("O_MSG_NAME"));

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
                
            if(insCnt > 0){
                String[] changeWorkId   = new String[insCnt]; 
                String[] workSeq        = new String[insCnt]; 
                String[] workSubseq     = new String[insCnt]; 
                String[] sellLocId      = new String[insCnt]; 
                String[] sellQty        = new String[insCnt]; 
                String[] sellUomId      = new String[insCnt]; 
                String[] buyLocId       = new String[insCnt]; 
                String[] buyQty         = new String[insCnt]; 
                String[] buyUomId       = new String[insCnt]; 
                String[] workStat       = new String[insCnt]; 
                String[] subLotId       = new String[insCnt]; 
                String[] stockId        = new String[insCnt]; 
                    
                for(int i = 0 ; i < insCnt ; i++){
                    if(model.get("I_CHANGE_WORK_ID"+i).equals("")){
                    	workId   = dao.getWorkId(model);
                    	changeWorkId[i] = workId;
                    }else{
                        changeWorkId[i] = (String)model.get("I_CHANGE_WORK_ID"+i);
                    }
                    workSeq[i]          = (String)model.get("I_WORK_SEQ"+i);
                    workSubseq[i]       = (String)model.get("I_WORK_SUBSEQ"+i);
                    sellLocId[i]        = (String)model.get("I_SELL_LOC_ID"+i);
                    sellQty[i]          = (String)model.get("I_SELL_QTY"+i);
                  
                    sellUomId[i]        = (String)model.get("I_SELL_UOM_ID"+i);
                    buyLocId[i]         = (String)model.get("I_BUY_LOC_ID"+i);
                    buyQty[i]           = (String)model.get("I_BUY_QTY"+i);
                    buyUomId[i]         = (String)model.get("I_BUY_UOM_ID"+i);
                    workStat[i]         = (String)model.get("I_WORK_STAT"+i);
                    
                    subLotId[i]         = (String)model.get("I_SUB_LOT_ID"+i);
                    stockId[i]          = (String)model.get("I_STOCK_ID"+i);
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("changeWorkId" , changeWorkId);
                modelIns.put("workSeq"      , workSeq);
                modelIns.put("workSubseq"   , workSubseq);
                modelIns.put("sellLocId"    , sellLocId);
                modelIns.put("sellQty"      , sellQty);
              
                modelIns.put("sellUomId"    , sellUomId);
                modelIns.put("buyLocId"     , buyLocId);
                modelIns.put("buyQty"       , buyQty);
                modelIns.put("buyUomId"     , buyUomId);
                modelIns.put("workStat"     , workStat);
              
                modelIns.put("subLotId"     , subLotId);
                modelIns.put("stockId"      , stockId);
  
                modelIns.put("gvUserIP"     , model.get("SS_CLIENT_IP"));
                modelIns.put("gvUserNo"     , model.get("SS_USER_NO"));
                
                modelIns = (Map<String, Object>)dao.save(modelIns);
                ServiceUtil.isValidReturnCode("WMSST100", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            if(delCnt > 0){
                String[] changeWorkId   = new String[delCnt]; 
                String[] workSeq        = new String[delCnt]; 
                String[] workSubseq     = new String[delCnt];  
                
                for(int i = 0 ; i < delCnt ; i++){
                    changeWorkId[i]     = (String)model.get("D_CHANGE_WORK_ID"+i);
                    workSeq[i]          = (String)model.get("D_WORK_SEQ"+i);
                    workSubseq[i]       = (String)model.get("D_WORK_SUBSEQ"+i);
                }
                
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modeldel = new HashMap<String, Object>();
              
                modeldel.put("changeWorkId" , changeWorkId);
                modeldel.put("workSeq"      , workSeq);
                modeldel.put("workSubseq"   , workSubseq);
  
                modeldel.put("gvUserIP"     , model.get("SS_CLIENT_IP"));
                modeldel.put("gvUserNo"     , model.get("SS_USER_NO"));
              
                modelResult = (Map<String, Object>)dao.delete(modeldel);
                ServiceUtil.isValidReturnCode("WMSST100", String.valueOf(modeldel.get("O_MSG_CODE")), (String)modeldel.get("O_MSG_NAME"));
            }
            
            m.put("errCnt"  , 0);
            m.put("MSG"     , MessageResolver.getMessage("save.success"));
            m.put("RESULT"  , workId);
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );            
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    public Map<String, Object> saveAllLoc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        //메인저장
        //workId 가져오기
        //그리드 저장
        String workId = "";
        try{
            Map<String, Object> modelResult = new HashMap<String, Object>();
            workId   = dao.getWorkId(model);
            
            model.put("workId"		 , workId);
            
            model.put("calApplyFrDt", model.get("calApplyFrDt").toString().replace("-", ""));
            model.put("contDeptId"	, model.get("vrContDeptId"));		//양도화주
            model.put("realDeptId"	, model.get("vrRealDeptId"));		// 양도RITEM_ID
            model.put("sellQty"		, model.get("vrSellQty"));			// 양도수량
            model.put("sellUomId"	, model.get("vrSellUomId"));		// UOMID
            
            model.put("buyDeptId"		, model.get("vrBuyDeptId"));		// 양수화주
            model.put("realBuyDeptId"	, model.get("vrRealBuyDeptId"));	// 양수RITEM_ID
            model.put("buyQty"			, model.get("vrBuyQty"));			// 양수수량
            model.put("buyUomId"		, model.get("vrBuyUomId"));			// UOMID
            
            model.put("lcId"		 , model.get(ConstantIF.SS_SVC_NO));
            model.put("I_WORK_IP"	 , model.get("SS_CLIENT_IP"));
            model.put("I_USER_NO"	 , model.get("SS_USER_NO"));
            
        	model = (Map<String, Object>)dao.saveAllLoc(model);
            ServiceUtil.isValidReturnCode("WMSST100", String.valueOf(model.get("O_MSG_CODE")), (String)model.get("O_MSG_NAME"));
//            }
        
            m.put("errCnt"  , 0);
            m.put("MSG"     , MessageResolver.getMessage("save.success"));
            m.put("RESULT"  , workId);
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );            
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
}
