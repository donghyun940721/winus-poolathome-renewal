package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST040Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST040Service")
public class WMSST040ServiceImpl extends AbstractServiceImpl implements WMSST040Service {
    
    @Resource(name = "WMSST040Dao")
    private WMSST040Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 재고이동 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }    

    /**
     * Method ID : detail
     * Method 설명 : 재고이동 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.detail(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }    
    
    /**
     * Method ID : listExcel
     * Method 설명 : 재고이동 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        //map.put("LIST", dao.list(model));
        map.put("LIST", dao.listExcel(model));
        
        return map;
    }
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            model.put("inKey", "ORD01"); // 주문종류
            map.put("ORD01", dao.selectOrd01(model));
            model.put("inKey", "ORD03"); // 주문단계
            map.put("ORD03", dao.selectOrd03(model));
            model.put("inKey", "ORD04"); // 주문상태
            map.put("ORD04", dao.selectOrd04(model));
            model.put("inKey", "STS00"); // 작업상태
            map.put("STS00", dao.selectSts00(model));
            model.put("inKey", "STS10"); // 작업진행상태
            map.put("STS10", dao.selectSts10(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectReqPerson2
     * Method 설명 : 셀렉트 박스 조회 2
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {            
            //로케이션필터쿼리
            map.put("LOCLIKE", dao.selectLotLike(model));
            map.put("POOL", dao.selectPool(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     * Method ID   : listDetail
     * Method 설명    : 재고이동 팝업 리스트
     * 작성자               : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if ("Y".equals(model.get("vrNewGb"))) {
            	map.put("LIST", dao.searchDetailType1(model));
            } else if ("G".equals(model.get("vrNewGb"))) {
            	map.put("LIST", dao.searchDetailType2(model));
            } else {
            	map.put("LIST", dao.searchDetailType3(model));
            }
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID   : listHeader
     * Method 설명    : 재고이동 팝업 리스트 (Header)
     * 작성자               : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listHeader(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.searchHead(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }  
    
    /**
     * Method ID : del
     * Method 설명 : 재고입출고 지시 취소
     * 작성자 : 김진석
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> del(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            Map<String, Object> modelIn = new HashMap<String, Object>();
         
            modelIn.put("vrWorkId", model.get("vrWorkId"));           
            modelIn.put("vrLcId", model.get("SS_SVC_NO"));
            modelIn.put("gvUserIP", model.get("SS_CLIENT_IP"));
            modelIn.put("gvUserNo", model.get("SS_USER_NO"));
               
            modelIn = (Map<String, Object>)dao.del(modelIn);
            ServiceUtil.isValidReturnCode("WMSST040", String.valueOf(modelIn.get("O_MSG_CODE")), (String)modelIn.get("O_MSG_NAME"));
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("delete.success"));
            m.put("result", (String)modelIn.get("O_MSG_NAME"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : work
     * Method 설명 : 재고입출고 지시
     * 작성자 : 김진석
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> work(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] workSeq    = new String[tmpCnt]; 
            String[] fromLocCd  = new String[tmpCnt]; 
            String[] toLocCd    = new String[tmpCnt]; 
            String[] ritemId    = new String[tmpCnt]; 
            String[] subLotId   = new String[tmpCnt]; 
            
            String[] uomId      = new String[tmpCnt]; 
            String[] moveQty    = new String[tmpCnt]; 
            String[] pltQty     = new String[tmpCnt]; 
            String[] workStat   = new String[tmpCnt]; 
            String[] itemWorkDt = new String[tmpCnt]; 
            
            String[] itemWorkTm = new String[tmpCnt];
            String[] makeDt     = new String[tmpCnt]; 
            String[] custId     = new String[tmpCnt]; 
            String[] custLotNo  = new String[tmpCnt]; 
            String[] blNo       = new String[tmpCnt]; 
            
            String[] stockId    = new String[tmpCnt]; 
            String[] reasonCd   = new String[tmpCnt]; 
            
            for(int i = 0 ; i < tmpCnt; i ++){
                
                workSeq[i]      = (String)model.get("workSeq"+i);
                fromLocCd[i]    = (String)model.get("fromLocCd"+i);
                toLocCd[i]      = (String)model.get("toLocCd"+i);
                ritemId[i]      = (String)model.get("ritemId"+i);
                subLotId[i]     = (String)model.get("subLotId"+i);
                
                uomId[i]        = (String)model.get("uomId"+i);
                moveQty[i]      = (String)model.get("moveQty"+i);
                pltQty[i]       = (String)model.get("pltQty"+i);
                workStat[i]     = (String)model.get("workStat"+i);
                itemWorkDt[i]   = (String)model.get("itemWorkDt"+i);
             
                itemWorkTm[i]   = (String)model.get("itemWorkTm"+i);
                makeDt[i]       = (String)model.get("makeDt"+i);
                custId[i]       = (String)model.get("custId"+i);
                custLotNo[i]    = (String)model.get("custLotNo"+i);
                blNo[i]         = (String)model.get("blNo"+i);
                
                stockId[i]      = (String)model.get("stockId"+i);
                reasonCd[i]     = (String)model.get("reasonCd"+i);
                
            }

            Map<String, Object> modelIn = new HashMap<String, Object>();
         
            modelIn.put("vrWorkId", model.get("vrWorkId"));
            modelIn.put("vrWorkCd", model.get("vrWorkCd"));
            modelIn.put("vrWorkDt", model.get("vrWorkDt"));
            
            modelIn.put("workSeq", workSeq);
            modelIn.put("fromLocCd", fromLocCd);
            modelIn.put("toLocCd", toLocCd);
            modelIn.put("ritemId", ritemId);
            modelIn.put("subLotId", subLotId);
            
            modelIn.put("uomId", uomId);
            modelIn.put("moveQty", moveQty);
            modelIn.put("pltQty", pltQty);
            modelIn.put("workStat", workStat);
            modelIn.put("itemWorkDt", itemWorkDt);
            
            modelIn.put("itemWorkTm", itemWorkTm);
            modelIn.put("makeDt", makeDt);
            modelIn.put("custId", custId);
            modelIn.put("custLotNo", custLotNo);
            modelIn.put("blNo", blNo);
            
            modelIn.put("stockId", stockId);
            modelIn.put("reasonCd", reasonCd);
            
            modelIn.put("vrLcId", model.get("SS_SVC_NO"));
            modelIn.put("gvUserIP", model.get("SS_CLIENT_IP"));
            modelIn.put("gvUserNo", model.get("SS_USER_NO"));
               
            modelIn = (Map<String, Object>)dao.work(modelIn);
            ServiceUtil.isValidReturnCode("WMSST040", String.valueOf(modelIn.get("O_MSG_CODE")), (String)modelIn.get("O_MSG_NAME"));            
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("result", (String)modelIn.get("O_MSG_NAME"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 재고입출고내역 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] workSeq    = new String[tmpCnt]; 
            String[] fromLocCd  = new String[tmpCnt]; 
            String[] toLocCd    = new String[tmpCnt]; 
            String[] ritemId    = new String[tmpCnt]; 
            String[] subLotId   = new String[tmpCnt]; 
            
            String[] uomId      = new String[tmpCnt]; 
            String[] moveQty    = new String[tmpCnt]; 
            String[] pltQty     = new String[tmpCnt]; 
            String[] workStat   = new String[tmpCnt]; 
            String[] itemWorkDt = new String[tmpCnt]; 
            
            String[] itemWorkTm = new String[tmpCnt];
            String[] makeDt     = new String[tmpCnt]; 
            String[] custId     = new String[tmpCnt]; 
            String[] custLotNo  = new String[tmpCnt]; 
            String[] blNo       = new String[tmpCnt]; 
            
            String[] stockId    = new String[tmpCnt]; 
            String[] reasonCd   = new String[tmpCnt];
            String[] workDesc   = new String[tmpCnt];
            
            for(int i = 0 ; i < tmpCnt; i ++){
                
                workSeq[i]      = (String)model.get("workSeq"+i);
                fromLocCd[i]    = (String)model.get("fromLocCd"+i);
                toLocCd[i]      = (String)model.get("toLocCd"+i);
                ritemId[i]      = (String)model.get("ritemId"+i);
                subLotId[i]     = (String)model.get("subLotId"+i);
                
                uomId[i]        = (String)model.get("uomId"+i);
                moveQty[i]      = (String)model.get("moveQty"+i);
                pltQty[i]       = (String)model.get("pltQty"+i);
                workStat[i]     = (String)model.get("workStat"+i);
                itemWorkDt[i]   = (String)model.get("itemWorkDt"+i);
             
                itemWorkTm[i]   = (String)model.get("itemWorkTm"+i);
                makeDt[i]       = (String)model.get("makeDt"+i);
                custId[i]       = (String)model.get("custId"+i);
                custLotNo[i]    = (String)model.get("custLotNo"+i);
                blNo[i]         = (String)model.get("blNo"+i);
                
                stockId[i]      = (String)model.get("stockId"+i);
                reasonCd[i]     = (String)model.get("reasonCd"+i);
                workDesc[i]     = (String)model.get("workDesc"+i);
                
            }

            Map<String, Object> modelIn = new HashMap<String, Object>();
         
            modelIn.put("vrWorkId", model.get("vrWorkId"));
            modelIn.put("vrWorkCd", model.get("vrWorkCd"));
            modelIn.put("vrWorkDt", model.get("vrWorkDt"));
            
            modelIn.put("workSeq", workSeq);
            modelIn.put("fromLocCd", fromLocCd);
            modelIn.put("toLocCd", toLocCd);
            modelIn.put("ritemId", ritemId);
            modelIn.put("subLotId", subLotId);
            
            modelIn.put("uomId", uomId);
            modelIn.put("moveQty", moveQty);
            modelIn.put("pltQty", pltQty);
            modelIn.put("workStat", workStat);
            modelIn.put("itemWorkDt", itemWorkDt);
            
            modelIn.put("itemWorkTm", itemWorkTm);
            modelIn.put("makeDt", makeDt);
            modelIn.put("custId", custId);
            modelIn.put("custLotNo", custLotNo);
            modelIn.put("blNo", blNo);
            
            modelIn.put("stockId", stockId);
            modelIn.put("reasonCd", reasonCd);
            modelIn.put("workDesc", workDesc);
            
            modelIn.put("vrLcId", model.get("SS_SVC_NO"));
            modelIn.put("gvUserIP", model.get("SS_CLIENT_IP"));
            modelIn.put("gvUserNo", model.get("SS_USER_NO"));
               
            modelIn = (Map<String, Object>)dao.save(modelIn);
            ServiceUtil.isValidReturnCode("WMSST040", String.valueOf(modelIn.get("O_MSG_CODE")), (String)modelIn.get("O_MSG_NAME"));
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("move.success"));
            m.put("result", (String)modelIn.get("O_MSG_NAME"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : save_grn
     * Method 설명 : 재고입출고내역 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveGrn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] workSeq    = new String[tmpCnt]; 
            String[] fromLocCd  = new String[tmpCnt]; 
            String[] toLocCd    = new String[tmpCnt]; 
            String[] ritemId    = new String[tmpCnt]; 
            String[] subLotId   = new String[tmpCnt]; 
            
            String[] uomId      = new String[tmpCnt]; 
            String[] moveQty    = new String[tmpCnt]; 
            String[] pltQty     = new String[tmpCnt]; 
            String[] workStat   = new String[tmpCnt]; 
            String[] itemWorkDt = new String[tmpCnt]; 
            
            String[] itemWorkTm = new String[tmpCnt];
            String[] makeDt     = new String[tmpCnt]; 
            String[] custId     = new String[tmpCnt]; 
            String[] custLotNo  = new String[tmpCnt]; 
            String[] blNo       = new String[tmpCnt]; 
            
            String[] stockId    = new String[tmpCnt]; 
            
            for(int i = 0 ; i < tmpCnt; i ++){
                
                workSeq[i]      = (String)model.get("workSeq"+i);
                fromLocCd[i]    = (String)model.get("fromLocCd"+i);
                toLocCd[i]      = (String)model.get("toLocCd"+i);
                ritemId[i]      = (String)model.get("ritemId"+i);
                subLotId[i]     = (String)model.get("subLotId"+i);
                
                uomId[i]        = (String)model.get("uomId"+i);
                moveQty[i]      = (String)model.get("moveQty"+i);
                pltQty[i]       = (String)model.get("pltQty"+i);
                workStat[i]     = (String)model.get("workStat"+i);
                itemWorkDt[i]   = (String)model.get("itemWorkDt"+i);
             
                itemWorkTm[i]   = (String)model.get("itemWorkTm"+i);
                makeDt[i]       = (String)model.get("makeDt"+i);
                custId[i]       = (String)model.get("custId"+i);
                custLotNo[i]    = (String)model.get("custLotNo"+i);
                blNo[i]         = (String)model.get("blNo"+i);
                
                stockId[i]      = (String)model.get("stockId"+i);
            }

            Map<String, Object> modelIn = new HashMap<String, Object>();
         
            modelIn.put("vrWorkId", model.get("vrWorkId"));
            modelIn.put("vrWorkCd", model.get("vrWorkCd"));
            modelIn.put("vrWorkDt", model.get("vrWorkDt"));
            
            modelIn.put("workSeq", workSeq);
            modelIn.put("fromLocCd", fromLocCd);
            modelIn.put("toLocCd", toLocCd);
            modelIn.put("ritemId", ritemId);
            modelIn.put("subLotId", subLotId);
            
            modelIn.put("uomId", uomId);
            modelIn.put("moveQty", moveQty);
            modelIn.put("pltQty", pltQty);
            modelIn.put("workStat", workStat);
            modelIn.put("itemWorkDt", itemWorkDt);
            
            modelIn.put("itemWorkTm", itemWorkTm);
            modelIn.put("makeDt", makeDt);
            modelIn.put("custId", custId);
            modelIn.put("custLotNo", custLotNo);
            modelIn.put("blNo", blNo);
            
            modelIn.put("stockId", stockId);
            
            modelIn.put("vrLcId", model.get("SS_SVC_NO"));
            modelIn.put("gvUserIP", model.get("SS_CLIENT_IP"));
            modelIn.put("gvUserNo", model.get("SS_USER_NO"));
               
            modelIn = (Map<String, Object>)dao.saveGrn(modelIn);
            ServiceUtil.isValidReturnCode("WMSST040", String.valueOf(modelIn.get("O_MSG_CODE")), (String)modelIn.get("O_MSG_NAME"));            
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("move.success"));
            m.put("result", (String)modelIn.get("O_MSG_NAME"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    /**
     * Method ID   : listRackDetail
     * Method 설명    : 재고이동 팝업 리스트2 (lack보충을 통한 재고이동)
     * 작성자               : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listRackDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "200");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listRackDetail(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 대체 Method ID   : DetailPop
     * 대체 Method 설명      : 재고이동 통합조회 팝업
     * 작성자                          : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> DetailPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.DetailPop(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 대체 Method ID   : workStatPop
     * 대체 Method 설명      : 재고이동 통합조회 팝업
     * 작성자                          : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> workStatPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.workStatPop(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listExcelTotal
     * Method 설명 : 재고이동 통합조회 팝업 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcelTotal(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.DetailPop(model));
        
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveUploadData
     * 대체 Method 설명    : 템플릿 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveUploadData(Map<String, Object> model) throws Exception {
    	Gson gson         = new Gson();
		String jsonString = gson.toJson(model);
		String sendData   = new StringBuffer().append(jsonString).toString();
		
		JsonParser Parser   = new JsonParser();
		JsonObject jsonObj  = (JsonObject) Parser.parse(sendData);
		JsonArray listBody  = (JsonArray) jsonObj.get("LIST");

		int listBodyCnt   = listBody.size();
    	Map<String, Object> m = new HashMap<String, Object>();
    	String curMsg	= "";
    	
        try{
            if(listBodyCnt > 0){
                for(int i = 0 ; i < listBodyCnt ; i ++){
                	JsonObject object = (JsonObject) listBody.get(i);
                	
                	//프로시져에 보낼것들 다담는다
                    Map<String, Object> modelIns = new HashMap<String, Object>();
                    
                    //main
                    modelIns.put("I_LC_ID"    		, model.get("SS_SVC_NO"));
                    modelIns.put("I_ITEM_CODE"		, (String)object.get("ITEM_CODE").toString().replaceAll("\"", ""));
                    modelIns.put("I_FROM_LOC_CD"	, (String)object.get("FROM_LOC_CD").toString().replaceAll("\"", ""));
                    modelIns.put("I_TO_LOC_CD"		, (String)object.get("TO_LOC_CD").toString().replaceAll("\"", ""));
                    modelIns.put("I_WORK_QTY"		, (String)object.get("WORK_QTY").toString().replaceAll("\"", ""));
                    modelIns.put("I_WORK_IP"  		, model.get("SS_CLIENT_IP"));  
                    modelIns.put("I_USER_NO"  		, model.get("SS_USER_NO"));

                    System.out.println("I_LC_ID : " + model.get("SS_SVC_NO"));
                    System.out.println("I_ITEM_CODE : " + (String)object.get("ITEM_CODE").toString().replaceAll("\"", ""));
                    System.out.println("I_FROM_LOC_CD : " + (String)object.get("FROM_LOC_CD").toString().replaceAll("\"", ""));
                    System.out.println("I_TO_LOC_CD : " + (String)object.get("TO_LOC_CD").toString().replaceAll("\"", ""));
                    System.out.println("I_WORK_QTY : " + (String)object.get("WORK_QTY").toString().replaceAll("\"", ""));
                    System.out.println("I_WORK_IP : " + model.get("SS_CLIENT_IP"));
                    System.out.println("I_LC_ID : " + model.get("SS_USER_NO"));
                    
                    curMsg = "Line in error(Cursor line): " + Integer.toString(i+1)+"line " + (String)object.get("ITEM_CODE").toString().replaceAll("\"", "");
                    
                    //dao                
                    modelIns = (Map<String, Object>)dao.saveUploadData(modelIns);
                    ServiceUtil.isValidReturnCode("WMSST040E5", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                }
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        } catch(Exception e){
            //throw e;
            throw new Exception( e.getMessage() + "Exception:" + curMsg);
        }
        return m;
    }
}
