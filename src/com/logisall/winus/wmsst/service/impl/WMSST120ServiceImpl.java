package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST120Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST120Service")
public class WMSST120ServiceImpl extends AbstractServiceImpl implements WMSST120Service {
    
    @Resource(name = "WMSST120Dao")
    private WMSST120Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 순환재고조사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.detail(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : detailNew
     * Method 설명 : 재고조사 상세 신규
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detailNew(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
            if("true".equals(model.get("chkEmptyStock"))){
                model.put("vrEmptyStock", "1");
            }
    		map.put("LIST", dao.detailNew(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }
    
    /**
     * 대체 Method ID    : selectBox
     * 대체 Method 설명      : 물류용기관리 화면 데이타셋
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("inKey", "CYCL01");
        map.put("CYCL01", dao.selectCYCL01(model));
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 임가공 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> main = new HashMap<String, Object>();
        int errCnt = 0;

            try{
                int iuCnt = Integer.parseInt(model.get("selectIds").toString());
                
                if(iuCnt > 0){
                    for(int i=0; i<iuCnt; i++){
                        main.put("ST_GUBUN", model.get("ST_GUBUN"+i));
                        main.put("CYCL_STOCK_ID", model.get("CYCL_STOCK_ID"+i));
                        main.put("WORK_SEQ", model.get("WORK_SEQ"+i));
                        main.put("REAL_QTY", model.get("REAL_QTY"+i));
                        main.put("DIFF_REASON", model.get("DIFF_REASON"+i));
                        main.put("DRIVER_QTY", model.get("DRIVER_QTY"+i));
                        main.put("UPD_NO_T2", model.get("UPD_NO_T2"+i));
                        main.put("SS_USER_NO", model.get("SS_USER_NO"));

                        dao.update(main);
                    }
                }else{
                    errCnt++;
                }
                
                if(errCnt == 0){
                    m.put("MSG", MessageResolver.getMessage("save.success"));
                    m.put("errCnt", errCnt);
                }else{
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                    m.put("errCnt", errCnt);
                }
            }catch (Exception e) {
                throw e;
            }
        return m;
    }
    
    /**
     * Method ID : saveNew
     * Method 설명 : 재고조사 저장 NEW
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveNew(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> main = new HashMap<String, Object>();
    	int errCnt = 0;
    	
    	try{
    		int iuCnt = Integer.parseInt(model.get("selectIds").toString());
    		
    		if(iuCnt > 0){
    			for(int i=0; i<iuCnt; i++){
    				main.put("ST_GUBUN", model.get("ST_GUBUN"+i));
    				main.put("CYCL_STOCK_ID", model.get("CYCL_STOCK_ID"+i));
    				main.put("WORK_SEQ", model.get("WORK_SEQ"+i));
    				main.put("REAL_QTY", model.get("REAL_QTY"+i));
    				main.put("UPD_NO_T2", model.get("UPD_NO_T2"+i));
    				main.put("SS_USER_NO", model.get("SS_USER_NO"));
    				
    				main.put("LOC_TYPE", model.get("LOC_TYPE"+i));
    				main.put("DIFF_REASON", model.get("DIFF_REASON"+i));
    				main.put("DRIVER_QTY", model.get("DRIVER_QTY"+i));
    				main.put("RITEM_ID", model.get("RITEM_ID"+i));
    				main.put("EXP_QTY", model.get("EXP_QTY"+i));
    				
    				main.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
    				dao.updateNew(main);
    			}
    		}else{
    			errCnt++;
    		}
    		
    		if(errCnt == 0){
    			m.put("MSG", MessageResolver.getMessage("save.success"));
    			m.put("errCnt", errCnt);
    		}else{
    			m.put("MSG", MessageResolver.getMessage("save.error"));
    			m.put("errCnt", errCnt);
    		}
    	}catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    
    /**
     * Method ID : updateSp
     * Method 설명 : 재실행
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateSp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        // int errCnt = 0;
        
        try{
        	String executeDate = (String)model.get("DATE_EXE");
        	if (StringUtils.isNotEmpty(executeDate)) {
        		executeDate = executeDate.replaceAll("-", "");        		
        	}
        	modelIns.put("WORK_DT", executeDate);
        	modelIns.put("WORK_NO", model.get(ConstantIF.SS_USER_NO));
        	modelIns.put("LC_ID", model.get("SS_SVC_NO"));
            modelIns = (Map<String, Object>)dao.runSpRotationalStockTakingJob(modelIns);
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
	        map.put("MSG", MessageResolver.getMessage("update.success"));
	        map.put("errCnt", 0);

        } catch(BizException be) {
            map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("update.error") );
            
            if ( log.isWarnEnabled() ) {
            	log.warn("[CronQuartzJob] END... (Error Occured) :" + be.getMessage() );
            }   	        
	        
        }catch (Exception e) {
            map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("update.error") );
            if (log.isErrorEnabled()) {
            	log.error("[CronQuartzJob] Error Occured :", e);
            }
        }
        return map;
    }
    
    /**
     * Method ID : cyclCustStart
     * Method 설명 : PAH 재고조사 신규
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> cyclCustStart(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	Map<String, Object> modelIns = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
			int iuCnt = Integer.parseInt(model.get("selectIds").toString());
    		if(iuCnt > 0){
    			String[] cyclCustId = model.get("MULIT_CUST_ID_ARR").toString().split(",");
        		String executeDate = (String)model.get("DATE_EXE");
        		if (StringUtils.isNotEmpty(executeDate)) {
        			executeDate = executeDate.replaceAll("-", "");        		
        		}
        		
        		modelIns.put("CUST_ID", cyclCustId);
        		modelIns.put("WORK_DT", executeDate);
        		modelIns.put("WORK_NO", model.get(ConstantIF.SS_USER_NO));
        		modelIns.put("LC_ID", model.get("SS_SVC_NO"));

        		modelIns = (Map<String, Object>)dao.spCyclCustStart(modelIns);
        		ServiceUtil.isValidReturnCode("WMSST120", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
        		map.put("errCnt", 0);
        		map.put("MSG", MessageResolver.getMessage("save.success"));
    		}else{
    			errCnt++;
    		}
    	}catch (Exception e) {
    		map.put("errCnt", errCnt);
    		map.put("MSG", MessageResolver.getMessage("update.error") );
    	}
    	return map;
    }
    
    /**
     * Method ID : saveCyclProcess
     * Method 설명 : 재고조사 기능 구분
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveCyclProcess(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	Map<String, Object> main = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		int iuCnt = Integer.parseInt(model.get("selectIds").toString());
    		if(iuCnt > 0){
    			for(int i=0; i<iuCnt; i++){
    				
    				main.put("CYCL_STOCK_ID", model.get("CYCL_STOCK_ID"+i));
    				main.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
    				main.put("SS_SVC_NO"	, model.get(ConstantIF.SS_SVC_NO));
    				
    				// 확정
    				if(model.get("CYCL_PRC_TYPE").equals("LOCK")){
        				main.put("CYCL_LOCK_YN", model.get("CYCL_LOCK_YN"+i));
        				dao.saveCyclLock(main);
        				dao.saveCyclLockDetail(main);
    				}
    				// 관리자수정
    				else if(model.get("CYCL_PRC_TYPE").equals("MOD")){
        				dao.saveCyclModify(main);
    				}
    				// 입력기한 지정
    				else if(model.get("CYCL_PRC_TYPE").equals("SUBMIT")){
        				main.put("CYCL_SUBMIT_DT", model.get("CYCL_SUBMIT_DT"+i));
        				dao.saveCyclSubmit(main);
    				}
    			}
    		}else{
    			errCnt++;
    		}
    		if(errCnt == 0){
    			m.put("MSG", MessageResolver.getMessage("save.success"));
    			m.put("errCnt", errCnt);
    		}else{
    			m.put("MSG", MessageResolver.getMessage("save.error"));
    			m.put("errCnt", errCnt);
    		}
    	}catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 순환재고조사 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : listExcelSub
     * Method 설명 : 순환재고조사 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcelSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.detail(model));
        
        return map;
    }    
}
