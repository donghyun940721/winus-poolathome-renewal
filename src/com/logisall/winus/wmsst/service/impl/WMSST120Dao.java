package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST120Dao")
public class WMSST120Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 순환재고조사 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst120.list", model);
    }
    
    /**
     * Method ID : list
     * Method 설명 : 순환재고조사 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet detail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst120.detail", model);
    }
    
    /**
     * Method ID : detailNew
     * Method 설명 : 순환재고조사 신규
     * 작성자 : sing09
     * @param model
     * @return
     */
    public GenericResultSet detailNew(Map<String, Object> model) {
    	return executeQueryPageWq("wmsst120.detailNew", model);
    }
    
    /**
     * Method ID : selectCYCL01
     * Method 설명 : 순환재고조사방식 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectCYCL01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID    : update
     * Method 설명      : 순환재고조사 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsst121.update", model);
    }  
    
    /**
     * Method ID    : updateNew
     * Method 설명      : 순환재고조사 수정New
     * 작성자                 : sing09
     * @param   model
     * @return  Object
     */
    public Object updateNew(Map<String, Object> model) {
    	return executeUpdate("wmsst121.updateNew", model);
    }  
    
    /**
     * Method ID    : updateSp
     * Method 설명      : 순환재고조사 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object updateSp(Map<String, Object> model) {
        return executeUpdate("wmsst121.updateSp", model);
    }   
    
    /**
     * Method ID    : saveCyclLock
     * Method 설명      : 재고조사 확정처리
     * 작성자                 : sing09
     * @param   model
     * @return  Object
     */
    public Object saveCyclLock(Map<String, Object> model) {
    	return executeUpdate("wmsst120.saveCyclLock", model);
    }   
    
    /**
     * Method ID    : saveCyclLockDetail
     * Method 설명      : 재고조사 확정처리 (상세 WMSST121)
     * 작성자                 : sing09
     * @param   model
     * @return  Object
     */
    public Object saveCyclLockDetail(Map<String, Object> model) {
    	return executeUpdate("wmsst121.saveCyclLockDetail", model);
    }   
    
    /**
     * Method ID    : saveCyclSubmit
     * Method 설명      : 재고조사 입력기한 지정
     * 작성자                 : sing09
     * @param   model
     * @return  Object
     */
    public Object saveCyclSubmit(Map<String, Object> model) {
    	return executeUpdate("wmsst120.saveCyclSubmit", model);
    }   
    
    /**
     * Method ID    : saveCyclModify
     * Method 설명      : 재고조사 관리자 수정
     * 작성자                 : sing09
     * @param   model
     * @return  Object
     */
    public Object saveCyclModify(Map<String, Object> model) {
    	return executeUpdate("wmsst120.saveCyclModify", model);
    }   
    
    /**
     * Method ID    : cyclCustStart
     * Method 설명      : PAH 재고조사 실행 신규
     * 작성자                 : sing09
     * @param   model
     * @return  Object
     */
    public Object spCyclCustStart(Map<String, Object> model){
    	executeUpdate("pk_wmsst120.sp_cycl_cust_start", model);
    	return model;
    }     
    
    public Object runSpRotationalStockTakingJob(Map<String, Object> model){
        executeUpdate("pk_wmsjb000.sp_rotational_stock_taking_man", model);
        return model;
    }      
}
