package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsst.service.WMSST071Service;
//import com.m2m.jdfw5x.egov.exception.BizException;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSST071Service")
public class WMSST071ServiceImpl implements WMSST071Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST071Dao")
	private WMSST071Dao dao;

	/**
	 * 대체 Method ID : selectData 대체 Method 설명 : 상품 목록 필요 데이타셋 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ITEMGRP", dao.selectItemGrp(model));
		map.put("UOM", dao.selectUom(model));
		return map;
	}

	/**
	 * 
	 * 대체 Method ID : listItem 대체 Method 설명 : 상품 목록 조회 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> listItem(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		if (model.get("page") == null) {
			model.put("pageIndex", "1");
		} else {
			model.put("pageIndex", model.get("page"));
		}
		if (model.get("rows") == null) {
			model.put("pageSize", "20");
		} else {
			model.put("pageSize", model.get("rows"));
		}
		map.put("LIST", dao.listItem(model));
		return map;
	}
    
    /**
     * Method ID : save
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
            	String[] lcId    	= new String[tmpCnt];
            	String[] ritemId    = new String[tmpCnt];
            	String[] custLotNo  = new String[tmpCnt];
            	String[] workQty    = new String[tmpCnt];
            	String[] sapBarcode = new String[tmpCnt];
            	
            	for(int i = 0 ; i < tmpCnt ; i ++){
            		lcId[i]       = (String)model.get(ConstantIF.SS_SVC_NO);
            		ritemId[i]    = (String)model.get("RITEM_ID"+i);
            		custLotNo[i]  = (String)model.get("CUST_LOT_NO"+i);
            		workQty[i]    = (String)model.get("WORK_QTY"+i);
            		sapBarcode[i] = (String)model.get("SAP_BARCODE"+i);
                }
            	
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("I_LC_ID"		, lcId);
                modelIns.put("I_RITEM_ID"	, ritemId);
                modelIns.put("I_CUST_LOT_NO", custLotNo);
                modelIns.put("I_WORK_QTY"	, workQty);
                modelIns.put("I_SAP_BARCODE", sapBarcode);
                
                //session 정보
                modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao
                //modelIns = (Map<String, Object>)dao.save(modelIns);
                ServiceUtil.isValidReturnCode("WMSST071", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
