package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsst.service.WMSST150Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST150Service")
public class WMSST150ServiceImpl extends AbstractServiceImpl implements WMSST150Service {
    
    @Resource(name = "WMSST150Dao")
    private WMSST150Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
               
            map.put("LIST", dao.listEpccd(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    
    /**
     * Method ID : listExcel
     * Method 설명 : 재고 입출고내역 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        
        
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listEpccd(model));
        
        return map;
        
    }
    
    
    /**
     * Method ID : save
     * Method 설명 : 재고입출고내역 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
  
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] iWorkDt = new String[iuCnt]; 
            String[] iWorkId = new String[iuCnt]; 
            
            for(int i = 0 ; i < iuCnt; i ++){
                iWorkDt[i]      = (String)model.get("WORK_DT"+i);
                iWorkId[i]      = (String)model.get("WORK_ID"+i);
            }
          
            if(iuCnt > 0){
               Map<String, Object> modelSP = new HashMap<String, Object>();
         
               modelSP.put("I_WORK_ID", iWorkId);
               modelSP.put("I_WORK_DT", iWorkDt);
               modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
               modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
          
               dao.save(modelSP);
               m.put("MSG", MessageResolver.getMessage("update.success"));
               m.put("errCnt", errCnt);
             }
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveSapCd
     * Method 설명 :  저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSapCd(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;

        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();

                modelDt.put("ORD_ID"         , model.get("ORD_ID"+i));
                modelDt.put("ORD_SEQ"        , model.get("ORD_SEQ"+i));
                modelDt.put("RTI_EPC_CD"     , model.get("RTI_EPC_CD"+i));
                modelDt.put("SAP_BARCODE"    , model.get("SAP_BARCODE"+i));

                if("INSERT".equals(model.get("ST_GUBUN"+i))){

                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.updateSapCd(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    
                }else{
                    errCnt++;
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID    : selectBox
     * 대체 Method 설명      : 물류용기관리 화면 데이타셋
     * 작성자                        : 기드온 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("POOL", dao.selectPool(model));
    	return map;
    }
}
