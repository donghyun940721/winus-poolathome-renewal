package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST076Dao")
public class WMSST076Dao extends SqlMapAbstractDAO{

    /**
     * Method ID    : list
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE01(Map<String, Object> model) {    	
    	if(model.get("vrEmptyLocation") == null ){ // 빈 로케이션 조회
    		return executeQueryPageWq("wmsst076.loItemList", model);
    	} else {
    		return executeQueryPageWq("wmsst076.emptyLocationList", model);
    	}    	
    }

    /**
     * Method ID    : list
     * Method 설명      : 로케이션별재고 목록 조회
     * 작성자                 : dhkim
     * @param   model
     * @return
     */
    public GenericResultSet listE02(Map<String, Object> model) {    	
    	if(model.get("vrEmptyLocation") == null ){ // 빈 로케이션 조회
    		return executeQueryPageWq("wmsst076.loItemListE02", model);
    	} else {
    		return executeQueryPageWq("wmsst076.emptyLocationList", model);
    	}    	
    }
    
    /**
	 * Method ID	: insert 
	 * Method 설명	: Lot 속성 변경이력 삽입
	 * 작성자			: dhkim
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmsst076.insert", model);
	}

	/**
	 * Method ID	: update 
	 * Method 설명	: Lot 속성 변경
	 * 작성자			: donghyun
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
//		return executeUpdate("wmsst076.update", model);
		
		return executeUpdate("wmsst076.pk_wmsst076.sp_change_lot_propert", model);
	}
}
