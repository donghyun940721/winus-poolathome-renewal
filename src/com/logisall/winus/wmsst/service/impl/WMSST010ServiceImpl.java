package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsst.service.WMSST010Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST010Service")
public class WMSST010ServiceImpl extends AbstractServiceImpl implements WMSST010Service {
    
    @Resource(name = "WMSST010Dao")
    private WMSST010Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 현재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            
            if("on".equals(model.get("chkEmptyStock"))){
                model.put("vrEmptyStock", "1");
            }
           
//            if(!"".equals(model.get("txtSrchCustCd"))){
//               model.put("vrSrchCustCd", model.get("txtSrchCustCd"));
//            }
           
            if(!"".equals(model.get("hdnCustId"))){
               model.put("vrSrchCustId", model.get("hdnCustId"));
            }
            if(!"".equals(model.get("txtSrchCustCd"))){
                model.put("vrSrchCustCd", model.get("txtSrchCustCd"));
            }
//            System.out.println(model.get("vrSrchCustCd")); //vrSrchCustCd
            
            if(!"".equals(model.get("hdnWhId"))){
                model.put("vrSrchWhId", model.get("hdnWhId"));
            }
            if(!"".equals(model.get("txtSrchItemCd"))){
                model.put("vrSrchItemCd", model.get("txtSrchItemCd"));
            }
            if(!"".equals(model.get("txtSrchItemNm"))){
                model.put("vrSrchItemNm", model.get("txtSrchItemNm"));
            }
            if(!"".equals(model.get("txtSrchBlNo"))){
                model.put("vrSrchBlNo", model.get("txtSrchBlNo"));
            }
            
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listV2
     * Method 설명 : 현재고 조회V2
     * 작성자 : seongjun Kwon
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listV2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if("on".equals(model.get("chkEmptyStock"))){
                model.put("vrEmptyStock", "1");
            }
           
            if(!"".equals(model.get("hdnCustId"))){
               model.put("vrSrchCustId", model.get("hdnCustId"));
            }
            if(!"".equals(model.get("txtSrchCustCd"))){
                model.put("vrSrchCustCd", model.get("txtSrchCustCd"));
            }
            
            if(!"".equals(model.get("hdnWhId"))){
                model.put("vrSrchWhId", model.get("hdnWhId"));
            }
            if(!"".equals(model.get("txtSrchItemCd"))){
                model.put("vrSrchItemCd", model.get("txtSrchItemCd"));
            }
            if(!"".equals(model.get("txtSrchItemNm"))){
                model.put("vrSrchItemNm", model.get("txtSrchItemNm"));
            }
            if(!"".equals(model.get("txtSrchBlNo"))){
                model.put("vrSrchBlNo", model.get("txtSrchBlNo"));
            }
            
            map.put("LIST", dao.listV2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("ZONEGUBUN", dao.selecZone(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listSub
     * Method 설명 : 현재고 서브 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.sublist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listV2_detail
     * Method 설명 : 현재고V2.0 detail 조회
     * 작성자 : seongjun kwon
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listV2_detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listV2_detail(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     * Method ID : listExcel
     * Method 설명 : 현재고 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        if("on".equals(model.get("chkEmptyStock"))){
            model.put("vrEmptyStock", "1");
        }
       
        if(!"".equals(model.get("hdnCustId"))){
           model.put("vrSrchCustId", model.get("hdnCustId"));
        }
        if(!"".equals(model.get("txtSrchCustCd"))){
            model.put("vrSrchCustCd", model.get("txtSrchCustCd"));
         }
        
        if(!"".equals(model.get("hdnWhId"))){
            model.put("vrSrchWhId", model.get("hdnWhId"));
        }
        if(!"".equals(model.get("txtSrchItemCd"))){
            model.put("vrSrchItemCd", model.get("txtSrchItemCd"));
        }
        if(!"".equals(model.get("txtSrchItemNm"))){
            model.put("vrSrchItemNm", model.get("txtSrchItemNm"));
        }
        if(!"".equals(model.get("txtSrchBlNo"))){
            model.put("vrSrchBlNo", model.get("txtSrchBlNo"));
        }
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : listExcel2
     * Method 설명 : 현재고 상세 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.sublist(model));
        
        return map;
    }
    /**
     * Method ID : save
     * Method 설명 : 현재고 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
  
        try{
            int iuCnt = Integer.parseInt(model.get("D_selectIds").toString());
            String[] iStockId 	= new String[iuCnt]; 
            String[] iPltQty  	= new String[iuCnt];
            String[] iOutExpQty = new String[iuCnt]; 
            String[] iLockYn	= new String[iuCnt];
            String[] iMakeDt	= new String[iuCnt];
            String[] iItemBestDateEnd	= new String[iuCnt];
            
            // 현재고 비고 저장
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("ritemId", model.get("RITEM_ID" + i));
                modelDt.put("LC_ID", model.get("SS_SVC_NO"));
                
                modelDt.put("remark", model.get("REMARK" + i));
                
                // modelDt.put("brandNm", model.get("MAKER_NM" + i));
                // modelDt.put("itemSize", model.get("ITEM_SIZE" + i));
                // modelDt.put("custItemCd", model.get("CUST_ITEM_CD" + i));
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            }
            
            // 현재고 재고수량 저장 
            for(int i = 0 ; i < iuCnt; i ++){
                iStockId[i]      = (String)model.get("D_STOCK_ID"+i);
                iPltQty[i]       = (String)model.get("D_REAL_PLT_QTY"+i);
                iOutExpQty[i]    = (String)model.get("D_OUT_EXP_QTY"+i);
                iLockYn[i]       = (String)model.get("D_LOCK_YN"+i);
                iMakeDt[i]       = (String)model.get("D_MAKE_DT"+i);
                iItemBestDateEnd[i]       = ((String)model.get("D_ITEM_BEST_DATE_END"+i)).replaceAll("-", "");
            }
          
            if(iuCnt > 0){
               Map<String, Object> modelSP = new HashMap<String, Object>();
         
               modelSP.put("I_STOCK_ID"		, iStockId);
               modelSP.put("I_PLT_QTY"		, iPltQty);
               modelSP.put("I_OUT_EXP_QTY"	, iOutExpQty);
               modelSP.put("I_LOCK_YN"		, iLockYn);
               modelSP.put("I_MAKE_DT"		, iMakeDt);
               modelSP.put("I_ITEM_BEST_DATE_END"		, iItemBestDateEnd);
               modelSP.put("I_USER_NO"		, model.get("SS_USER_NO"));
               modelSP.put("I_WORK_IP"		, model.get("SS_CLIENT_IP"));
          
               dao.save(modelSP);
               m.put("MSG", MessageResolver.getMessage("update.success"));
               m.put("errCnt", errCnt);
             }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listPop
     * Method 설명 : 세트상품 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.poplist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : detailPop
     * Method 설명 : 세트상품 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.popdetail(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectPopExcel
     * Method 설명 : 세트상품 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectPopExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();  
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.poplist(model));
        
        return map;
    }
    
    /**
     * Method ID	: subulManualComp
     * Method 설명	: 현재고 수불재생성
     * 작성자			: 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> subulManualComp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	int iuCnt = Integer.parseInt(model.get("selectIds").toString());
        	String[] ritemId = new String[iuCnt]; 

        	for(int i = 0 ; i < iuCnt; i ++){
	        	if("UPDATE".equals(model.get("ST_GUBUN"+i))){
		        	ritemId[i] = (String)model.get("RITEM_ID"+i);
	        	}else{
	        		errCnt++;
		        	m.put("errCnt", errCnt);
		        	throw new Exception(MessageResolver.getMessage("save.error"));
	        	}
        	}
        	
        	if(iuCnt > 0){
	        	Map<String, Object> modelSP = new HashMap<String, Object>();
	        	modelSP.put("I_LC_ID"		, model.get("SS_SVC_NO"));
	        	modelSP.put("I_RITEM_ID"	, ritemId);
	        	modelSP.put("I_WORK_IP"		, model.get("SS_CLIENT_IP"));
	        	modelSP.put("I_USER_NO"		, model.get("SS_USER_NO"));
	
	        	dao.subulManualComp(modelSP);
	        	m.put("MSG", MessageResolver.getMessage("update.success"));
	        	m.put("errCnt", errCnt);
        	}            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    /**
     * Method ID : itemList
     * Method 설명 : 상품별 현재고조회 팝업
     * 작성자 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> itemList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            model.put("vrSrchRitemId",((String)model.get("vrSrchRitemId")).split(","));
            map.put("LIST", dao.itemList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        
        return map;
    }
    
}
