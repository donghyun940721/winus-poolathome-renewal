package com.logisall.winus.wmsst.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST071Dao")
public class WMSST071Dao extends SqlMapAbstractDAO {

    /**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model) {
	return executeQueryForList("wmsms094.selectItemGrp", model);
    }

    /**
     * Method ID : selectUom Method 설명 : 신규생성시 LCID마다 다른 UOM 필요데이터 조회 작성자 :
     * chsong
     * 
     * @param model
     * @return
     */
    public Object selectUom(Map<String, Object> model) {
	return executeQueryForList("wmsms100.selectUom", model);
    }

    /**
     * Method ID : listItem Method 설명 : 상품정보 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listItem(Map<String, Object> model) {
	return executeQueryPageWq("wmsst071.listItem", model);
    }
    
    /**
     * Method ID    : save
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsst071.pk_wmsst070.sp_kit_complete_mobile", model);
        return model;
    }
}
