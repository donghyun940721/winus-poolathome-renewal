package com.logisall.winus.wmsst.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.PDF.model.Wmsst070HeardlVO;
import com.logisall.winus.frm.common.PDF.model.Wmsst070InDetailVO;
import com.logisall.winus.frm.common.PDF.model.Wmsst070OutDetailVO;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST070Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service("WMSST070Service")
public class WMSST070ServiceImpl extends AbstractServiceImpl implements WMSST070Service {
    
    @Resource(name = "WMSST070Dao")
    private WMSST070Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 임가공 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : updateComplete
     * Method 설명 : 임가공 조립/해체 확정
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelSP = new HashMap<String, Object>();
        
        try{
            // 그리드 저장 시작
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            if(iuCnt > 0){              
                modelSP.put("I_WORK_ID",     model.get("vrWorkId"));
                modelSP.put("I_WORK_IP",     model.get("SS_CLIENT_IP"));
                modelSP.put("I_USER_NO",     model.get("SS_USER_NO"));
           
                if(model.get("gubun").equals("kit")){
                    // 임가공 조립
                	modelSP = (Map<String, Object>) dao.updateKitComplete(modelSP);
                	ServiceUtil.isValidReturnCode("WMSST070", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));

                }else if(model.get("gubun").equals("unkit")){
                    // 임가공 해체
                	modelSP = (Map<String, Object>)dao.updateUnkitComplete(modelSP);
                	ServiceUtil.isValidReturnCode("WMSST070", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
                }
            }              
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );
        
        }catch (Exception e) {
            throw e;
        }
        return m;
        
    }
    /**
     * Method ID : save
     * Method 설명 : 임가공 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> main = new HashMap<String, Object>();
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            if(iuCnt > 0){
                for(int i=0; i<iuCnt; i++){
                    main.put("ST_GUBUN", model.get("ST_GUBUN"+i));
                    main.put("vrInWhId", model.get("vrInWhId"+i));
                    main.put("vrOutWhId", model.get("vrOutWhId"+i));
                    main.put("custId", model.get("custId"+i));
                    main.put("ritemId", model.get("ritemId"+i));
                    main.put("uomId", model.get("uomId"+i));
                    main.put("qty", model.get("qty"+i));
                    main.put("vrWorkId", model.get("vrWorkId"+i));                        
                    main.put("type", model.get("type"+i));               
                    
                    if(main.get("ST_GUBUN").equals("DELETE")){
                        Map<String, Object> modelSPD = new HashMap<String, Object>();
                        modelSPD.put("I_WORK_ID",     model.get("vrWorkId"+i));
                        modelSPD.put("I_WORK_IP",     model.get("SS_CLIENT_IP"));
                        modelSPD.put("I_USER_NO",     model.get("SS_USER_NO"));                        
                        modelSPD = (Map<String, Object>)dao.delete(modelSPD);
                        ServiceUtil.isValidReturnCode("WMSST070", String.valueOf(modelSPD.get("O_MSG_CODE")), (String)modelSPD.get("O_MSG_NAME"));
                    }
                }
                if(!main.get("ST_GUBUN").equals("DELETE")){
                    Map<String, Object> modelSP = new HashMap<String, Object>();
                    modelSP.put("I_IN_WH_ID",     main.get("vrInWhId"));
                    modelSP.put("I_OUT_WH_ID",    main.get("vrOutWhId"));
                    modelSP.put("I_CUST_ID",      main.get("custId"));
                    modelSP.put("I_RITEM_ID",     main.get("ritemId"));
                    modelSP.put("I_UOM_ID",       main.get("uomId"));
                    modelSP.put("I_WORK_QTY",     main.get("qty"));
                    modelSP.put("I_MAKE_CUST_CD", "");
                    
                    modelSP.put("I_LC_ID",        model.get("SS_SVC_NO"));
                    modelSP.put("I_WORK_IP",      model.get("SS_CLIENT_IP"));
                    modelSP.put("I_USER_NO",      model.get("SS_USER_NO"));
                    
                    if(main.get("type").equals("1")){
                        // 임가공 조립
                    	modelSP = (Map<String, Object>) dao.save(modelSP);
                    	ServiceUtil.isValidReturnCode("WMSST070", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
                       
                    }else if(main.get("type").equals("2")){
                        // 임가공 해체
                    	modelSP = (Map<String, Object>)dao.save2(modelSP);
                    	ServiceUtil.isValidReturnCode("WMSST070", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));

                    }
                }
            }              
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", 0);
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );                

        }catch (Exception e) {
            throw e;
        }
        return m;
    }


    /**
     * Method ID : listExcel
     * Method 설명 : 임가공 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : listExcel2
     * Method 설명 : 임가공 조립 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("vrWorkType", "90");
        model.put("vrOrdType", "01");
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.poplist(model));
        
        return map;
    }
    
    /**
     * Method ID : listExcel3
     * Method 설명 : 임가공 해체 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("vrWorkType", "100");
        model.put("vrOrdType", "02");
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.poplist(model));
        
        return map;
    }
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
            model.put("inKey", "WORK01"); // 작업구분
            map.put("WORK01", dao.selectWORK01(model)); 
            model.put("inKey", "ORD05"); // 임가공구분
            map.put("ORD05", dao.selectORD05(model)); 
            model.put("inKey", "STS00"); // 작업상태
            map.put("STS00", dao.selectSTS00(model)); 
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listSub
     * Method 설명 : 임가공 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.listSub(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listSub2
     * Method 설명 : 임가공 상세 조회2
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.listSub2(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listPop
     * Method 설명 : 임가공조립 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            model.put("vrWorkType", "90");
            model.put("vrOrdType", "01");
         
            map.put("LIST", dao.poplist(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : listPopSub
     * Method 설명 : 임가공해체 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listPopSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            model.put("vrWorkType", "100");
            model.put("vrOrdType", "02");
         
            map.put("LIST", dao.poplist(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : detailPop
     * Method 설명 : 임가공조립 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.popdetail(model));
            
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    
    /**
     * 
     * Method ID   : saveOrderKit
     * Method 설명    : 번들보충
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOrderKit(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];          
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                
                //session 및 등록정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOrderKit(modelIns);
                ServiceUtil.isValidReturnCode("WMSST070", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
        	m.put("errCnt", 1);
        	m.put("MSG", be.getMessage() );                

        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    

    /**
     * Method ID : getPreViewConf
     * Method 설명 : PDF 생성 (조립)
     * 작성자 : 기드온
     * @param model
     * @throws Exception
     */
    public Map<String, Object> getPreViewConf(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        model.put("USER_NAME", (String)model.get(ConstantIF.SS_USER_NAME));
        
        try {
			Map<String, Object> parameters = dao.selectHeaderMap(model);
			List<Map<String, Object>> parameterList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> parameterListIn = dao.selectInDeatilList(model);
			List<Map<String, Object>> parameterListOut = dao.selectOutDeatilList(model);
			
			if ( parameterListIn != null && parameterListIn.size() > 0  ) {
				parameterList.addAll(parameterListIn);
				
				if ( parameterListOut != null && parameterListOut.size() > 0  ) {
					parameterList.addAll(parameterListOut);
				}
				/*
			    	for (Map<String, Object> parameter : parameterList) {
			    		parameter.put("RITEM_NM", "A테스트상품001");
			    	}
		    	*/
			}	    	
			
	    	setReportParameter(parameters);
	    	
			parameters.put("SUB_TITLE_1", 	MessageResolver.getMessage("임가공(조립-입고)"));
			parameters.put("SUB_TITLE_2", 	MessageResolver.getMessage("임가공(조립-출고)"));
			parameters.put("SUBREPORT_DIR", ConstantIF.REPORT_FILE_PATH);
			parameters.put("subReportDataSource", parameterList);
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}    	    	
			JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_6);    	    	
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
			
			String createFile = pdfFilepath + File.separatorChar + pdfFileName;    		
			JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
		    		
	        m.put("ERRCNT", 0);
	        m.put("MSG", MessageResolver.getMessage("list.success"));
	        
	    } catch(BizException be) {
	    	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
	        m.put("ERRCNT", 1);
	        m.put("MSG", be.getMessage() );
	        
	    }catch(Exception e){
	    	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
	        m.put("ERRCNT", 1);
	        m.put("MSG", e.getMessage() );
	    }
	    return m;        
        
    }   
    
    private void setReportParameter(Map<String, Object> param) {
    	if (param == null) {
    		param = new HashMap<String, Object>(); 
    	}    	
    	param.put("MAIN_HEADER_LABEL_1", MessageResolver.getMessage("발행일자"));
    	param.put("MAIN_HEADER_LABEL_2", MessageResolver.getMessage("발행자"));
    	param.put("MAIN_HEADER_LABEL_3", MessageResolver.getMessage("화주"));
    	param.put("MAIN_HEADER_LABEL_4", MessageResolver.getMessage("확인자"));
    	param.put("MAIN_FOOTER_LABEL_1", MessageResolver.getMessage("비고") + " : ");
    	
    	param.put("MAIN_LIST_COLUMN_NAME_1", MessageResolver.getMessage("No."));
    	param.put("MAIN_LIST_COLUMN_NAME_2", MessageResolver.getMessage("제품"));
    	param.put("MAIN_LIST_COLUMN_NAME_3", MessageResolver.getMessage("수량"));
    	param.put("MAIN_LIST_COLUMN_NAME_4", MessageResolver.getMessage("UOM"));
    	param.put("MAIN_LIST_COLUMN_NAME_5", MessageResolver.getMessage("창고"));
    	param.put("MAIN_LIST_COLUMN_NAME_6", MessageResolver.getMessage("로케이션"));
    }    
    
    /**
     * Method ID : getPreViewConf
     * Method 설명 : PDF 생성 (조립)
     * 작성자 : 기드온
     * @param model
     * @throws Exception
     */
    public Map<String, Object> getPreViewConf_OLD(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        model.put("USER_NAME", (String)model.get(ConstantIF.SS_USER_NAME));
        Wmsst070HeardlVO h = dao.selectHeader(model);
        Wmsst070InDetailVO [] in = dao.selectInDeatil(model);
        Wmsst070OutDetailVO [] out = dao.selectOutDeatil(model);
        
        h.setFontPath(ConstantIF.FONT_NORMAL);
        h.setTitleFontPath(ConstantIF.FONT_TITLE);
        
        // h.setServerPath(ConstantIF.serverPdfPath+"sample/");
        // h.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
        h.setServerPath(ConstantIF.SERVER_PDF_PATH);
        h.setFileName(model.get("langName") + "_" + model.get("dateName"));

        // PDFCreate pdf = new PDFCreate();
        //  = pdf.createWmsst070(h, in, out);
        
        return m;
    }       
    
    /**
     * Method ID : getPreViewConfSub
     * Method 설명 : PDF 생성 (해체)
     * 작성자 : 기드온
     * @param model
     * @throws Exception
     */
    public Map<String, Object> getPreViewConfSub(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        model.put("USER_NAME", (String)model.get(ConstantIF.SS_USER_NAME));
        
        try {
			Map<String, Object> parameters = dao.selectHeader2Map(model);
			List<Map<String, Object>> parameterList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> parameterListIn = dao.selectInDeatil2List(model);
			List<Map<String, Object>> parameterListOut = dao.selectOutDeatil2List(model);
			
			if ( parameterListIn != null && parameterListIn.size() > 0  ) {
				parameterList.addAll(parameterListIn);
				
				if ( parameterListOut != null && parameterListOut.size() > 0  ) {
					parameterList.addAll(parameterListOut);
				}
				/*
			    	for (Map<String, Object> parameter : parameterList) {
			    		parameter.put("RITEM_NM", "A테스트상품001");
			    	}
		    	*/
			}	    	
	    	setReportParameter(parameters);
	    	parameters.put("SUB_TITLE_1", 	MessageResolver.getMessage("임가공(해체-출고)"));
			parameters.put("SUB_TITLE_2", 	MessageResolver.getMessage("임가공(해체-입고)"));
			parameters.put("SUBREPORT_DIR", ConstantIF.REPORT_FILE_PATH);
			parameters.put("subReportDataSource", parameterList);			
	    	
	    	JRBeanCollectionDataSource src = new JRBeanCollectionDataSource(parameterList);	    	
	    	
			String pdfFilepath	= (String)model.get("pdfFilePath");
			String pdfFileName 	= (String)model.get("pdfFileName");
			if (!CommonUtil.isNull(pdfFilepath)) {
				if (!FileUtil.existDirectory(pdfFilepath)) {
					FileHelper.createDirectorys(pdfFilepath);
				}
			}    	    	
			JasperReport report = JasperCompileManager.compileReport(ConstantIF.REPORT_FILE_6);    	    	
			JasperPrint jasperPrint = JasperFillManager.fillReport(report, parameters, src);
			
			String createFile = pdfFilepath + File.separatorChar + pdfFileName;    		
			JasperExportManager.exportReportToPdfFile(jasperPrint, createFile);
		    		
	        m.put("ERRCNT", 0);
	        m.put("MSG", MessageResolver.getMessage("list.success"));
	        
	    } catch(BizException be) {
	    	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", be);
			} 
	        m.put("ERRCNT", 1);
	        m.put("MSG", be.getMessage() );
	        
	    }catch(Exception e){
	    	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
	        m.put("ERRCNT", 1);
	        m.put("MSG", e.getMessage() );
	    }
	    return m; 
    }
    
    public Map<String, Object> getPreViewConfSub_OLD(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
//        DrbCertCipTestMasterVO c = new DrbCertCipTestMasterVO(); 
        model.put("USER_NAME", (String)model.get(ConstantIF.SS_USER_NAME));
//        System.out.println(model);
        Wmsst070HeardlVO h = dao.selectHeader2(model);
        Wmsst070InDetailVO [] in = dao.selectInDeatil2(model);
        Wmsst070OutDetailVO [] out = dao.selectOutDeatil2(model);
        
        h.setFontPath(ConstantIF.FONT_NORMAL);
        h.setTitleFontPath(ConstantIF.FONT_TITLE);
        
        // h.setServerPath(ConstantIF.serverPdfPath+"sample/");
        // h.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
        h.setServerPath(ConstantIF.SERVER_PDF_PATH);
        h.setFileName(model.get("langName") + "_" + model.get("dateName"));

        // PDFCreate pdf = new PDFCreate();
        // m = pdf.createWmsst070(h, in, out);
        
        return m;
    }      
    
    /*-
	 * Method ID   : workList
	 * Method 설명 : 
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> workList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		
		List<String> workIdArr = new ArrayList();
        String[] spVrWorkIdStr = model.get("vrWorkIdStr").toString().split(",");
        for (String keyword : spVrWorkIdStr ){
        	workIdArr.add(keyword);
        }
        model.put("workIdArr", workIdArr);
        
		map.put("WORKLIST", dao.workList(model));
		return map;
	}
}
