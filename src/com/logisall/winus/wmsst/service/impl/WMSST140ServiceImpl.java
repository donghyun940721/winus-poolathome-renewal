package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsst.service.WMSST140Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST140Service")
public class WMSST140ServiceImpl extends AbstractServiceImpl implements WMSST140Service {
    
    @Resource(name = "WMSST140Dao")
    private WMSST140Dao dao;

    /**
     * Method ID   : selectPoolGrp
     * Method 설명    : 물류용기출고관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectPoolGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("POOLGRP", dao.selectPoolGrp(model));
        return map;
    }

    /**
     * Method ID    : list
     * Method 설명      : 현재고 조회
     * 작성자                 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }           
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listSub
     * 대체 Method 설명    : 현재고 상세조회
     * 작성자                      : chSong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listSub(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 현재고 비고저장
     * 작성자                      : chSong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            // 현재고 비고 저장
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("vrRitemId" , model.get("RITEM_ID" + i));
                modelDt.put("gvLcId"    , model.get(ConstantIF.SS_SVC_NO));
                
                modelDt.put("vrRemark"  , model.get("REMARK" + i));
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }

    /**
     * 
     * 대체 Method ID   : listExcel
     * 대체 Method 설명    : 물류용기 현재고 엑셀리스트
     * 작성자                      : chSong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }


}


