package com.logisall.winus.wmsst.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmsst.service.WMSST220Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST220Service")
public class WMSST220ServiceImpl extends AbstractServiceImpl implements WMSST220Service {
    
    @Resource(name = "WMSST220Dao")
    private WMSST220Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 현재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("MAIN")) {
			if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
			map.put("MAIN", dao.list(model));
		}
		return map;
	}
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listSub
     * Method 설명 : 현재고 서브 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.sublist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    
    /**
     * Method ID : listExcel
     * Method 설명 : 현재고 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listExcel(model));
        
        return map;
    }
    
    /**
     * Method ID : listExcel2
     * Method 설명 : 현재고 상세 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.sublist(model));
        
        return map;
    }
    /**
     * Method ID : save
     * Method 설명 : 현재고 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
  
        try{
            int iuCnt = Integer.parseInt(model.get("D_selectIds").toString());
            String[] iStockId = new String[iuCnt]; 
            String[] iPltQty = new String[iuCnt]; 
            // 현재고 비고 저장
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("ritemId", model.get("RITEM_ID" + i));
                modelDt.put("LC_ID", model.get("SS_SVC_NO"));
                
                modelDt.put("remark", model.get("REMARK" + i));
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    m.put("MSG", MessageResolver.getMessage("update.success"));
                    m.put("errCnt", errCnt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            }
            
            // 현재고 재고수량 저장 
            for(int i = 0 ; i < iuCnt; i ++){
                iStockId[i]      = (String)model.get("D_STOCK_ID"+i);
                iPltQty[i]       = (String)model.get("D_REAL_PLT_QTY"+i);
            }
          
            if(iuCnt > 0){
               Map<String, Object> modelSP = new HashMap<String, Object>();
         
               modelSP.put("I_STOCK_ID", iStockId);
               modelSP.put("I_PLT_QTY", iPltQty);
               modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
               modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
          
               dao.save(modelSP);
               m.put("MSG", MessageResolver.getMessage("update.success"));
               m.put("errCnt", errCnt);
             }
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listPop
     * Method 설명 : 세트상품 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.poplist(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : detailPop
     * Method 설명 : 세트상품 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.popdetail(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectPopExcel
     * Method 설명 : 세트상품 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectPopExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();  
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.poplist(model));
        
        return map;
    }
    
    /**
     * Method ID    : getSampleExcelDown
     * Method 설명      : 엑셀 샘플 다운
     * 작성자                 : chsong
     * @param model
     * @return
     */
    @Override
    public Map<String, Object> getSampleExcelDown(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Map<String, Object> > sample = new ArrayList<Map<String, Object>>();
        int colSize = Integer.parseInt(model.get("Col_Size").toString());
        int rowSize = Integer.parseInt(model.get("Row_Size").toString());
        
        for(int k = 0 ; k < rowSize ; k++){
        	if(k == 0){
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
                    sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
                    sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}else{
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
        			sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
        			sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}
        }
        Map<String, Object> sampleRow = new HashMap<String, Object>();
        sampleRow.put("rowSize", rowSize);
        
        map.put("LIST", getExcelSampleList(sample, sampleRow));
        return map;
    }
    
    private GenericResultSet getExcelSampleList(List<Map<String, Object>> list, Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		int pageIndex = 1;
		int pageSize = 1;
		int pageTotal = 1;
		int pageBlank = (int) Math.ceil(pageTotal / (double) pageSize);
		int rowSize = Integer.parseInt(model.get("rowSize").toString());
		
		List sampleList = new ArrayList<Map<String, Object>>();
		
		for(int k = 0 ; k < rowSize ; k++){
			Map<String, Object> sample = new HashMap<String, Object>();
			for (Map<String, Object> sampleInfo : list) {	
				Object key = sampleInfo.get(k+"_sampleCol");
				if (key != null && StringUtils.isNotEmpty(key.toString())) {
					sample.put(key.toString(), sampleInfo.get(k+"_sampleRow"));
				}
			}
			sampleList.add(sample);
		}
		
		wqrs.setCpage(pageIndex);
		wqrs.setTpage(pageBlank);
		wqrs.setTotCnt(pageTotal);
		wqrs.setList(sampleList);
		return wqrs;
	}
}
