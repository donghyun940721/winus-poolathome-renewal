package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST110Dao")
public class WMSST110Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.wmsst110_search", model);
    }
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet detail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.wmsst110_search2", model);
    }
}
