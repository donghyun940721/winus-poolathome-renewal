package com.logisall.winus.wmsst.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsst.service.WMSST020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST020Service")
public class WMSST020ServiceImpl extends AbstractServiceImpl implements WMSST020Service {
    
    @Resource(name = "WMSST020Dao")
    private WMSST020Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 재고 입출고내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            String vrViewSet = "N";
            List<String> checkbox = new ArrayList<>();
            
            if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
            if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
            if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
            if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){checkbox.add("30");}
            if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
            if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
            if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
            if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
            if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
            model.put("vrViewSet", vrViewSet);
            model.put("chekbox", checkbox);
            
            if(!model.get("vrSrchBarcode").equals("")){
                map.put("LIST", dao.listBarcode(model));
            }else if(!model.get("vrSrchBlNo").equals("")){
                map.put("LIST", dao.listBlNo(model));
            }else{
                map.put("LIST", dao.listBlNo(model));
            }
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 재고 입출고내역 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();    
        
        String vrViewSet = "N";
        List<String> checkbox = new ArrayList<>();
        
        if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
        if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
        if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
        if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){checkbox.add("30");}
        if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
        if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
        if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
        if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
        if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
        
        model.put("vrViewSet", vrViewSet);
        model.put("chekbox", checkbox);
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listBlNo(model));
        
        return map;
    }

    /**
     * Method ID   		: listSummaryCount
     * Method 설명      : 재고입출고내역 COUNT SUMMARY
     * 작성자           : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listSummaryCount(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if (model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if (model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }

        String vrViewSet = "N";
        List<String> checkbox = new ArrayList<>();
        
        if(model.containsKey("chViewGrn") && model.get("chViewGrn").equals("on")){checkbox.add("21");}
        if(model.containsKey("chViewIn") && model.get("chViewIn").equals("on")){checkbox.add("20");}
        if(model.containsKey("chViewPicking") && model.get("chViewPicking").equals("on")){checkbox.add("31");}
        if(model.containsKey("chViewOut") && model.get("chViewOut").equals("on")){checkbox.add("30");}
        if(model.containsKey("chViewEtc") && model.get("chViewEtc").equals("on")){checkbox.add("50"); checkbox.add("60");}
        if(model.containsKey("chViewDlv") && model.get("chViewDlv").equals("on")){checkbox.add("39");}
        if(model.containsKey("chViewrtnIn") && model.get("chViewrtnIn").equals("on")){checkbox.add("22"); checkbox.add("23");}
        if(model.containsKey("chViewrtnOut") && model.get("chViewrtnOut").equals("on")){checkbox.add("33");}
        if(model.containsKey("chViewSet") && model.get("chViewSet").equals("on")){vrViewSet = "Y"; checkbox.add("90");}
        
        model.put("vrViewSet", vrViewSet);
        model.put("chekbox", checkbox);
        map.put("LIST", dao.listSummaryCount(model));
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 재고입출고내역 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
  
        try{
            int iuCnt = Integer.parseInt(model.get("selectIds").toString());
            
            String[] iWorkDt = new String[iuCnt]; 
            String[] iWorkId = new String[iuCnt]; 
            
            for(int i = 0 ; i < iuCnt; i ++){
                iWorkDt[i]      = (String)model.get("WORK_DT"+i);
                iWorkId[i]      = (String)model.get("WORK_ID"+i);
            }
          
            if(iuCnt > 0){
               Map<String, Object> modelSP = new HashMap<String, Object>();
         
               modelSP.put("I_WORK_ID", iWorkId);
               modelSP.put("I_WORK_DT", iWorkDt);
               modelSP.put("I_USER_NO", model.get("SS_USER_NO"));
               modelSP.put("I_WORK_IP", model.get("SS_CLIENT_IP"));
          
               dao.save(modelSP);
               m.put("MSG", MessageResolver.getMessage("update.success"));
               m.put("errCnt", errCnt);
             }
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
