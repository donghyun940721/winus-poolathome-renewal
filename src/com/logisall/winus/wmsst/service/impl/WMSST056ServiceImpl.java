package com.logisall.winus.wmsst.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsst.service.WMSST056Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST056Service")
public class WMSST056ServiceImpl extends AbstractServiceImpl implements WMSST056Service {
    
    @Resource(name = "WMSST056Dao")
    private WMSST056Dao dao;

    /**
     * Method ID : list
     * Method 설명 : 입별재고 조회
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            List<String> lcIdArr = new ArrayList();
            String[] spSrchArrLike = model.get("arrLcId").toString().split(",");
            for (String keyword : spSrchArrLike ){
                lcIdArr.add(keyword);
            }
            model.put("lcIdArr", lcIdArr);
            
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }  
    
    /**
     * Method ID : listT2
     * Method 설명 : 입별재고 조회
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listT2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            List<String> lcIdArr = new ArrayList();
            String[] spSrchArrLike = model.get("arrLcIdT2").toString().split(",");
            for (String keyword : spSrchArrLike ){
                lcIdArr.add(keyword);
            }
            model.put("lcIdArr", lcIdArr);
            
            map.put("LIST", dao.listT2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 입별재고 엑셀다운
     * 작성자 : 민환기
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    
    
    /**
     * Method ID : listExcel
     * Method 설명 : 배송센터일별재고조회 엑셀
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
    @Override
	public Map<String, Object> listPop0Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        List<String> lcIdArr = new ArrayList();
        String[] spSrchArrLike = model.get("arrLcId").toString().split(",");
        for (String keyword : spSrchArrLike ){
            lcIdArr.add(keyword);
        }
        model.put("lcIdArr", lcIdArr);
        
        
        map.put("LIST", dao.list(model));
        return map;
	}
    
    /**
     * Method ID : listPop2Excel
     * Method 설명 : 배배송센터일별재고 > 입출고이력조회 엑셀
     * 작성자 : 김채린
     * @param model
     * @return
     * @throws Exception
     */
    
    @Override
   	public Map<String, Object> listPop2Excel(Map<String, Object> model) throws Exception {
   		Map<String, Object> map = new HashMap<String, Object>();
           
   		   model.put("pageIndex", "1");
           model.put("pageSize", "60000");
           
           List<String> lcIdArr = new ArrayList();
           String[] spSrchArrLike = model.get("arrLcIdT2").toString().split(",");
           for (String keyword : spSrchArrLike ){
               lcIdArr.add(keyword);
           }
           model.put("lcIdArr", lcIdArr);
           
           map.put("LIST", dao.listT2(model));
           return map;
   	}
       
}
