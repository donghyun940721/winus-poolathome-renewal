package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST290Dao")
public class WMSST290Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	  
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
	   /**
     * Method ID : list
     * Method 설명 : 임가공통합작업
     * 작성자 : 김채린
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst290.list", model);
    }
    /**
     * Method ID : list
     * Method 설명 : 임가공통합작업
     * 작성자 : 김채린
     * @param model
     * @return
     */
    public GenericResultSet sublist(Map<String, Object> model) {
        return executeQueryPageWq("wmsst290.sublist", model);
    }
    
    public Object save(Map<String, Object> model){
        return  executeUpdate("wmsst290.pk_wmsst070.sp_kit_multi_complete", model);
    }
    
    
    public Object dlt(Map<String, Object> model){
        return  executeUpdate("wmsst290.pk_wmsst070.sp_unkit_multi_complete", model);
    }
    
  
}
