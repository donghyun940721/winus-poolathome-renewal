package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST050Dao")
public class WMSST050Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 재고조정 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst010.wmsst050_search", model);
    }   
    
    /**
     * Method ID : poplist
     * Method 설명 : 재고조정팝업 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet poplist(Map<String, Object> model) {
        return executeQueryPageWq("wmsms060.list", model);
    }   
    /**
     * Method ID : selectWork02
     * Method 설명 : 작업상세 셀렉트박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectWork02(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID  : save
     * Method 설명  : 재고조정 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object save(Map<String, Object> model){
        executeUpdate("wmsst010.pk_wmsst050.sp_modify_stock", model);
        return model;
    }
}

