package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST051Dao")
public class WMSST051Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 일별재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst050.list", model);
    }   
    
    /**
     * Method ID : detail
     * Method 설명 : 일별재고 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet detail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst050.detail", model);
    }   
    
    /**
     * Method ID : listPool
     * Method 설명 : 일별재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listPool(Map<String, Object> model) {
        return executeQueryPageWq("wmsst050.listPool", model);
    } 
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : listMn
     * Method 설명 : 월별재고 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listMn(Map<String, Object> model) {
        return executeQueryPageWq("wmsst050.listMn", model);
    }   
    
    public GenericResultSet listMnDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst050.listMnDetail", model);
    }   
    
}

