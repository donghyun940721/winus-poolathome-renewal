package com.logisall.winus.wmsst.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST100Dao")
public class WMSST100Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 명의변경 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst101.list", model);
    }
    
  
    /**
     * Method ID : selectACC14
     * Method 설명 : 반올림 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectACC14(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    /**
     * Method ID : selectSTS00
     * Method 설명 : 작업상태 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectSTS00(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : detail
     * Method 설명 : 명의변경 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object detail(Map<String, Object> model) {
        return executeView("wmsst101.detail", model);
    }
    
    /**
     * Method ID : subSearch
     * Method 설명 : 명의변경 상세그리드 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet subSearch(Map<String, Object> model) {
        return executeQueryPageWq("wmsst101.subSearch", model);
    }
    
    /**
     * Method ID  : subConfirm
     * Method 설명  : 명의변경 
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object subConfirm(Map<String, Object> model){
        executeUpdate("wmsst100.pk_wmsst100.sp_change_work", model);
        return model;
    }
    
    /**
     * Method ID  : subSave
     * Method 설명  : 명의변경 상세저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object subSave(Map<String, Object> model){
        executeUpdate("wmsst100.pk_wmsst100.sp_save_change_work", model);
        return model;
    }
    
    /**
     * Method ID  : getWorkId
     * Method 설명  : 명의변경 work_id 가져오기
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public String getWorkId(Map<String, Object> model){
        String workId = (String)executeView("wmsst100.search_work_id", model);
        return workId;
    }
    
    /**
     * Method ID  : save
     * Method 설명  : 명의변경 그리드 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object save(Map<String, Object> model){
        executeUpdate("wmsst100.pk_wmsst100.sp_save_change_work_loc", model);
        return model;
    }
    /**
     * Method ID  : delete
     * Method 설명  : 명의변경 그리드 삭제
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object delete(Map<String, Object> model){
        executeUpdate("wmsst100.pk_wmsst100.sp_del_change_wok_loc", model);
        return model;
    }
    
    /**
     * Method ID  : deleteChangeWork
     * Method 설명  : 명의변경 삭제
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object deleteChangeWork(Map<String, Object> model){
        executeUpdate("wmsst100.pk_wmsst100.sp_del_change_work", model);
        return model;
    }
    
    public Object saveAllLoc(Map<String, Object> model){
        executeUpdate("wmsst100.pk_wmsst100.sp_multi_change_work_loc", model);
        return model;
    }
}
