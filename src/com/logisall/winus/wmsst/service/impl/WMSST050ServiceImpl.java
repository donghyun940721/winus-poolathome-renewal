package com.logisall.winus.wmsst.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsst.service.WMSST050Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSST050Service")
public class WMSST050ServiceImpl extends AbstractServiceImpl implements WMSST050Service {
    
    @Resource(name = "WMSST050Dao")
    private WMSST050Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 재고조정 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listPop
     * Method 설명 : 재고조정팝업 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.poplist(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    /**
     * Method ID : listExcel
     * Method 설명 : 재고조정 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : selectPopExcel
     * Method 설명 : 재고조정 엑셀다운(팝업)
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectPopExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.poplist(model));
        
        return map;
    }
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
    
            
            model.put("inKey", "WORK02"); // 작업상세 (증가감소)
            map.put("WORK02", dao.selectWork02(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 재고조정 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelSP = new HashMap<String, Object>();
        try{
            modelSP.put("I_LC_ID", model.get("SS_SVC_NO"));
            modelSP.put("I_WORK_TYPE", model.get("cmbWorkType"));
            modelSP.put("I_SUB_LOT_ID", model.get("SUB_LOT_ID"));
            modelSP.put("I_RITEM_ID", model.get("RITEM_ID"));
            modelSP.put("I_LOC_ID", model.get("LOC_ID"));
            modelSP.put("I_UOM_ID", model.get("UOM_ID"));
            modelSP.put("I_WORK_QTY", model.get("WORK_QTY"));
            modelSP.put("I_CUST_LOT_NO", model.get("CUST_LOT_NO"));
            modelSP.put("I_REASON", model.get("taReason"));
            modelSP.put("I_WORK_IP",     model.get("SS_CLIENT_IP"));
            modelSP.put("I_USER_NO",     model.get("SS_USER_NO"));
            
            //유효기간만료일 추가
            modelSP.put("I_ITEM_BEST_DATE_END", model.get("ITEM_BEST_DATE_END").toString().replace("-", ""));            
            modelSP = (Map<String, Object>) dao.save(modelSP);
            ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelSP.get("O_MSG_CODE")), (String)modelSP.get("O_MSG_NAME"));
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", 0);
                
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", MessageResolver.getMessage("save.error"));
            
        }catch (Exception e) {
            throw e;
        }
        return m;
    }
}
