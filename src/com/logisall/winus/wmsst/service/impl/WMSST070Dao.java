package com.logisall.winus.wmsst.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.logisall.winus.frm.common.PDF.model.Wmsst070HeardlVO;
import com.logisall.winus.frm.common.PDF.model.Wmsst070InDetailVO;
import com.logisall.winus.frm.common.PDF.model.Wmsst070OutDetailVO;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSST070Dao")
public class WMSST070Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : list
     * Method 설명 : 임가공 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKit", model);
    }
  
    /**
     * Method ID : listSub
     * Method 설명 : 임가공 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitDetail", model);
    }
    
    /**
     * Method ID : listSub2
     * Method 설명 : 임가공 상세 조회2
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listSub2(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitDetail2", model);
    }
    
    /**
     * Method ID : selectWORK01
     * Method 설명 : 작업구분 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectWORK01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectORD05
     * Method 설명 : 임가공구분 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectORD05(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectSTS00
     * Method 설명 : 작업상태 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectSTS00(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : poplist
     * Method 설명 : 임가공조립 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet poplist(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitWork", model);
    }
    
    /**
     * Method ID : popdetail
     * Method 설명 : 임가공조립 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet popdetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsst500.searchKitWorkDetail", model);
    }
    
    /**
     * Method ID  : save
     * Method 설명  : 임가공조립 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object save(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_gen_kit_work", model);
        return model;
    }
    
    /**
     * Method ID  : save2
     * Method 설명  : 임가공해체 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object save2(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_gen_unkit_work", model);
        return model;
    }
    
    /**
     * Method ID  : delete
     * Method 설명  : 임가공조립 삭제
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object delete(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_del_work", model);
        return model;
    }

    /**
     * Method ID  : updateKitComplete
     * Method 설명  : 임가공조립확정
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object updateKitComplete(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_kit_complete", model);
        return model;
    }
    /**
     * Method ID  : updateUnkitComplete
     * Method 설명  : 임가공해체확정
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object updateUnkitComplete(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_unkit_complete", model);
        return model;
    }

     
    
    /**
     * Method ID    : saveSimpleOut
     * Method 설명      : 번들보충
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveOrderKit(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_order_kit", model);
        return model;
    }
    
    /**
     * Method ID    : selectHeader
     * Method 설명      : 임가공 작업지시서발행 헤더(조립)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Map<String, Object> selectHeaderMap(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsst500.selectHeaderMap", model);
    }
    
    public Wmsst070HeardlVO selectHeader(Map<String, Object> model) throws Exception{
        return (Wmsst070HeardlVO)getSqlMapClientTemplate().queryForObject("wmsst500.selectHeader", model);
    }
    
    /**
     * Method ID    : selectInDeatil
     * Method 설명      : 임가공 작업지시서발행 입고 디테일(조립)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */   
    public List<Map<String, Object> > selectInDeatilList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("wmsst500.selectDetailList", model);
    }
    
    public Wmsst070InDetailVO[] selectInDeatil(Map<String, Object> model) throws Exception{
        return (Wmsst070InDetailVO[])getSqlMapClientTemplate().queryForList("wmsst500.selectDetail", model).toArray(new Wmsst070InDetailVO[0]);
    }    
    /**
     * Method ID    : selectOutDeatil
     * Method 설명      : 임가공 작업지시서발행 출고 디테일(조립)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */   
    public List<Map<String, Object> > selectOutDeatilList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("wmsst500.selectDetail2List", model);
    }
    
    public Wmsst070OutDetailVO[] selectOutDeatil(Map<String, Object> model) throws Exception{
        return (Wmsst070OutDetailVO[])getSqlMapClientTemplate().queryForList("wmsst500.selectDetail2", model).toArray(new Wmsst070OutDetailVO[0]);
    }    
    
    /**
     * Method ID    : selectHeader2
     * Method 설명      : 임가공 작업지시서발행 헤더(해체)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */
    public Map<String, Object> selectHeader2Map(Map<String, Object> model) throws Exception{
        return (Map<String, Object>)getSqlMapClientTemplate().queryForObject("wmsst500.selectHeader2Map", model);
    }
    
    public Wmsst070HeardlVO selectHeader2(Map<String, Object> model) throws Exception{
        return (Wmsst070HeardlVO)getSqlMapClientTemplate().queryForObject("wmsst500.selectHeader2", model);
    }    
    /**
     * Method ID    : selectInDeatil2
     * Method 설명      : 임가공 작업지시서발행 입고 디테일(해체)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */   
    public List<Map<String, Object> > selectInDeatil2List(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsst500.select2DetailList", model);
    }
    
    public Wmsst070InDetailVO[] selectInDeatil2(Map<String, Object> model) throws Exception{
        return (Wmsst070InDetailVO[])getSqlMapClientTemplate().queryForList("wmsst500.select2Detail", model).toArray(new Wmsst070InDetailVO[0]);
    }    
    
    /**
     * Method ID    : selectOutDeatil2
     * Method 설명      : 임가공 작업지시서발행 출고 디테일(해체)
     * 작성자                 : 기드온
     * @param   model
     * @return
     */   
    public List<Map<String, Object> > selectOutDeatil2List(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >)getSqlMapClientTemplate().queryForList("wmsst500.selectDetail2List", model);
    }
    
    public Wmsst070OutDetailVO[] selectOutDeatil2(Map<String, Object> model) throws Exception{
        return (Wmsst070OutDetailVO[])getSqlMapClientTemplate().queryForList("wmsst500.selectDetail2", model).toArray(new Wmsst070OutDetailVO[0]);
    }    
    
    /**
     * Method ID  : workList
     * Method 설명  : workList
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object workList(Map<String, Object> model){
        return executeQueryForList("wmsst070.workList", model);
    }
}
