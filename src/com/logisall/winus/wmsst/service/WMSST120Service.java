package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST120Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> detailNew(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveNew(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveCyclProcess(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateSp(Map<String, Object> model) throws Exception;
    public Map<String, Object> cyclCustStart(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelSub(Map<String, Object> model) throws Exception;
}
