package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST100Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> getSubSearch(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveConfirm(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveAllLoc(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;
}
