package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listV2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listV2_detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectPopExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> subulManualComp(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> itemList(Map<String, Object> model) throws Exception;
}
