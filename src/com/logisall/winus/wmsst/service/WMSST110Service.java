package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST110Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;

}
