package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST020Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSummaryCount(Map<String, Object> model) throws Exception;

}
