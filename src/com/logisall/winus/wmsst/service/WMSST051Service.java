package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST051Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listMn(Map<String, Object> model) throws Exception;
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listMnDetail(Map<String, Object> model) throws Exception;
    
    
    
    public Map<String, Object> listPool(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelDownE04(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelDownE05(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> detailExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}
