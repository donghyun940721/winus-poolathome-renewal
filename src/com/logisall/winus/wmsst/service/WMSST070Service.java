package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST070Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listPopSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub2(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel3(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> getPreViewConf(Map<String, Object> model) throws Exception;//PDF 생성
    public Map<String, Object> getPreViewConfSub(Map<String, Object> model) throws Exception;//PDF 생성
    public Map<String, Object> saveOrderKit(Map<String, Object> model) throws Exception;
    public Map<String, Object> workList(Map<String, Object> model) throws Exception;
}
