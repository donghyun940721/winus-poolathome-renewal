package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST310Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelDown(Map<String, Object> model) throws Exception;
}
