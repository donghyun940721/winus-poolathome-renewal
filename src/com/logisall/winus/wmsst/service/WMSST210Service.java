package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST210Service {
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> DetailPop(Map<String, Object> model) throws Exception;
}
