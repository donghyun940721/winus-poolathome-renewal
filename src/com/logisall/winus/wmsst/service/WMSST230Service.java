package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST230Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> hListExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> dListExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> getSampleExcelDown(Map<String, Object> model) throws Exception;
}
