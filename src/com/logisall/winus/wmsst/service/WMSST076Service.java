package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST076Service {
    public Map<String, Object> listE01(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE02(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
}
