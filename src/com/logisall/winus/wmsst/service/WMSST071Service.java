package com.logisall.winus.wmsst.service;

import java.util.List;
import java.util.Map;

public interface WMSST071Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
}
