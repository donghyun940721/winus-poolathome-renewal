package com.logisall.winus.wmsst.service;

import java.util.Map;


public interface WMSST056Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listPop0Excel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listPop2Excel(Map<String, Object> model) throws Exception;
    
}
