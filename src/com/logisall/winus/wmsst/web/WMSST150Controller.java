package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST150Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST150Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST150Service")
	private WMSST150Service service;

	/**
     * Method ID    : wmsst150
     * Method 설명      : 물류용기재고화면
     * 작성자                 : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WINUS/WMSST150.action")
    public ModelAndView wmsst150(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsst/WMSST150", service.selectBox(model));        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }

	/*-
	 * Method ID : list
	 * Method 설명 : 재고 입출고내역 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST150/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 재고 입출고내역  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST150/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : saveSapCd
	 * Method 설명      :
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSST150/saveSapCd.action")
	public ModelAndView saveSapCd(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSapCd(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 재고 입출고내역 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST150/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("RNUM"),				"0", "0", "0", "0", "200"},
            					   {MessageResolver.getMessage("화주"),				"1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"),				"2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("PDA_NO"),			"3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("ORD_TYPE"), 		"4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("ORD_ID"), 			"5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("ORD_SEQ"),			"6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("RTI_EPC_CD"), 		"7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("PDA_WORK_SEQ"),		"8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("RTI_ID"),			"9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("ITEM_EPC_CD"),		"10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("RITEM_ID"),			"11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("QTY"),				"12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM_NM"),			"13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("REG_DT"),			"14", "14", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("REG_NO"),			"15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("ITEM_QTY"),			"16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("BEST_DATE_NUM"),	"17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("MAKE_DT"),			"18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("CUST_LOT_NO"),		"19", "19", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("EVENT_DT"),			"20", "20", "0", "0", "200"},
                                   {MessageResolver.getMessage("ITEM_CODE"),		"21", "21", "0", "0", "200"},
                                   {MessageResolver.getMessage("RTI_CODE"),			"22", "22", "0", "0", "200"},

                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"RNUM"				, "S"},
				            		{"LC_ID"			, "S"},
				            		{"CUST_NM"			, "S"},
				            		{"PDA_NO"			, "S"},
				            		{"ORD_TYPE"			, "S"},
				            		
				            		{"ORD_ID"			, "S"},
				            		{"ORD_SEQ"			, "S"},
				            		{"RTI_EPC_CD"		, "S"},
				            		{"PDA_WORK_SEQ"		, "S"},
				            		{"RTI_ID"			, "S"},
				            		
				            		{"ITEM_EPC_CD"		, "S"},
				            		{"RITEM_ID"			, "S"},
				            		{"QTY"              , "N"},
				            		{"UOM_NM"			, "S"},
				            		{"REG_DT"			, "S"},
				            		
				            		{"REG_NO"			, "S"},
				            		{"ITEM_QTY" 		, "N"},
				            		{"BEST_DATE_NUM"	, "S"},
				            		{"MAKE_DT"			, "S"},
				            		{"CUST_LOT_NO"		, "S"},
				            		
				            		{"EVENT_DT"			, "S"},
				            		{"ITEM_CODE"		, "S"},
				            		{"RTI_CODE"			, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("매핑EPC조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
    }
}