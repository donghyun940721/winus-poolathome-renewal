package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST051Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST051Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST051Service")
	private WMSST051Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 일별재고 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST051.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST051", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 일별재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST051/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : detail
	 * Method 설명 : 일별재고 상세조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST051/detail.action")
	public ModelAndView detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : listPool
	 * Method 설명 : 일별재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST051/listPool.action")
	public ModelAndView listPool(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listPool(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listExcel
	 * Method 설명 : 일별재고 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST051/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고량"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("일반출고"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("이벤트출고"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("재고량"), "7", "7", "0", "0", "200"},
                                   {"UOM", "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("현재고"), "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("현불량"), "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("AS재고"), "11", "11", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"SUBUL_DT"          , "S"},
                                    {"CUST_NM"           , "S"},
                                    {"RITEM_CD"          , "S"},
                                    {"RITEM_NM"          , "S"},
                                    {"IN_QTY"            , "N"},
                                    
                                    {"OUT_QTY"           , "N"},
                                    {"EVENT_QTY"         , "N"},
                                    {"STOCK_QTY"         , "N"},
                                    {"UOM_NM"            , "S"},
                                    {"GOOD_QTY"          , "N"},
        
                                    {"BAD_QTY"           , "N"},
                                    {"AS_QTY"            , "N"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("일별재고내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID : detailExcel
	 * Method 설명 : 일별재고 상세엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST051/detail_excel.action")
	public void detailExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.detailExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown2
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("구분"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("요청수량"), "5", "5", "0", "0", "200"},
                                   {"UOM", "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고량"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고량"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고"), "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("출고"), "10", "10", "0", "0", "200"},
                                   {"UOM", "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("요청일"), "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("주문번호"), "13", "13", "0", "0", "200"},
                                   {"ORD_ID", "14", "14", "0", "0", "200"},
                                   
                                   {"SEQ", "15", "15", "0", "0", "200"},
                                   {"WORK_ID", "16", "16", "0", "0", "200"},
                                   {"SEQ", "17", "17", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"SUBUL_DT"          , "S"},
                                    {"ORD_TYPE"           , "S"},
                                    {"CUST_NM"          , "S"},
                                    {"RITEM_CD"          , "S"},
                                    {"RITEM_NM"            , "S"},
                                    
                                    {"REQ_QTY"           , "N"},
                                    {"REQ_UOM_NM"         , "S"},
                                    {"IN_QTY"         , "N"},
                                    {"OUT_QTY"            , "N"},
                                    {"REAL_PLT_QTY_I"          , "N"},
                                    
                                    {"REAL_PLT_QTY_O"          , "N"},
                                    {"UOM_NM"          , "S"},
                                    {"REQ_DT"           , "S"},
                                    {"ORG_ORD_ID"            , "S"},
                                    {"ORD_ID"            , "S"},
                                    
                                    {"ORD_SEQ"            , "S"},
                                    {"WORK_ID"            , "S"},
                                    {"WORK_SEQ"            , "S"}
                                   }; 
            
            //파일명
            String fileName ="WMSST";
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
    
    /*-
	 * Method ID : listMn
	 * Method 설명 : 월별재고조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST051/listMn.action")
	public ModelAndView listMn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listMn(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : detailExcel
	 * Method 설명 : 일별재고 상세엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST051/excelDownE04.action")
	public void excelDownE04(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelDownE04(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.excelDownE04(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown2
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void excelDownE04(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("품목코드"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("품목명"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("전월 말일 재고"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고수량"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고수량"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("말일재고"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("UOM"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주코드"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주명"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("입출고수량"), "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("UNIT명"), "10", "10", "0", "0", "200"}
                                   
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"RITEM_CD"          , "S"},
                                    {"RITEM_NM"          , "S"},
                                    {"LAST_MONTH_STOCK"  , "N"},
                                    {"IN_QTY"         	 , "N"},
                                    {"OUT_QTY"         	 , "N"},
                                    {"LAST_DAY_STOCK"    , "N"},
                                    {"UOM_CD"            , "S"},
                                    {"CUST_CD"           , "S"},
                                    {"CUST_NM"         	 , "S"},
                                    {"INOUT_QTY"         , "N"},
                                    {"UNIT_NM"           , "S"}
                                   }; 
            
            //파일명
            String fileName ="월별재고내역";
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
    
    @RequestMapping("/WMSST051/listMnDetail.action")
	public ModelAndView listMnDetail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listMnDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
    
    @RequestMapping("/WMSST051/excelDownE05.action")
	public void excelDownE05(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelDownE05(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.excelDownE05(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown5
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void excelDownE05(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("거래처명"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("품목코드"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("품목명"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("전일재고"), "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("입고수량"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("출고수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("현재고"), "7", "7", "0", "0", "200"}
                                   
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"SUBLE_DT"          	, "S"},
                                    {"IN_CUST_ZONE"         , "S"},
                                    {"RITEM_CD"  			, "S"},
                                    {"RITEM_NM"         	, "S"},
                                    {"YES_STOCK"         	, "NR"},
                                    {"IN_QTY"    			, "NR"},
                                    {"OUT_QTY"            	, "NR"},
                                    {"STOCK_QTY"           	, "NR"}
                                   }; 
            
            //파일명
            String fileName ="월별재고내역";
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
}