package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST020Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST020Service")
	private WMSST020Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 재고 입출고내역 조회 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST020.action")
	public ModelAndView mn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST020");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 재고 입출고내역 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST020/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 재고 입출고내역  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST020/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 재고 입출고내역 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST020/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

    /*-
	 * Method ID    	: listSummaryCount
	 * Method 설명      : 재고입출고내역 count summary
	 * 작성자           : KSJ
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSST020/listSummaryCount.action")
	public ModelAndView listSummaryCount(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		
		try {
			mav = new ModelAndView("jsonView", service.listSummaryCount(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주")   , "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일자"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업시간"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("입/출고"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("상품명") , "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("수량")  , "6", "6", "0", "0", "200"},
                                   {"UOM", "7", "7", "0", "0", "200"},
                                   {"PLT", "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("중량"), "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("로케이션"), "10", "10", "0", "0", "200"},
                                   {"LOT_NO" , "11", "11", "0", "0", "200"},
                                   {"UNIT_NO", "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("거래처") , "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("비고")  , "14", "14", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("주문ID"), "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고")  , "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("담당자") , "17", "17", "0", "0", "200"}
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"           , "S"},
                                    {"WORK_DT"           , "S"},
                                    {"REG_TM"            , "S"},
                                    {"WORK_SUBTYPE_NM"   , "S"},
                                    {"RITEM_CD"          , "S"},
                                    
                                    {"RITEM_NM"          , "S"},
                                    {"WORK_QTY"          , "N"},
                                    {"WORK_UOM_NM"       , "S"},
                                    {"REAL_PLT_QTY"      , "N"},
                                    {"WEIGHT"            , "N"},
                                    
                                    {"LOC_NM"            , "S"},
                                    {"CUST_LOT_NO"       , "S"},
                                    {"UNIT_NO"       	 , "S"},
                                    {"TRANS_CUST_NM"     , "S"},
                                    {"ORD_DESC"          , "S"},
                                    
                                    {"ORD_ID2"           , "S"},
                                    {"WH_NM"             , "S"},
                                    {"REG_NM"            , "S"}
                                   }; 
            
            //파일명
            String fileName = MessageResolver.getText("재고입출고내역");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
}