package com.logisall.winus.wmsst.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsst.service.WMSST110Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST110Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST110Service")
	private WMSST110Service service;


	/**
     * Method ID : mn
     * Method 설명 : 유효기간경고관리 화면
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSST110.action")
    public ModelAndView mn(Map<String, Object> model) {
        return new ModelAndView("winus/wmsst/WMSST110");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
    
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSST110/list.action")
    public ModelAndView list(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
        } 
        return mav;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 유효기간경고관리 상세조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSST110/detail.action")
    public ModelAndView detail(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.detail(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get detail result :", e);
			}
        } 
        return mav;
    }
}
