package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST056Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST056Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST056Service")
	private WMSST056Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 일별재고 화면
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST056.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST056");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 일별재고 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST056/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 일별재고 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST056/listT2.action")
	public ModelAndView listT2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listT2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSST056/excelDownP0.action")
	public void popListExcel0(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			map = service.list0Excel(model);
			map = service.listPop0Excel(model);
			
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownPop0(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	
	/*-
	 * Method ID : doExcelDownPop0
	 * Method 설명 : 배송센터일별재고조회 엑셀다운
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	protected void doExcelDownPop0(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("작업일자")  		, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("센터")   		, "1", "1", "0", "0", "160"},
				            		{MessageResolver.getText("화주")  		, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("LOT번호")		, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("상품코드")		, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("상품명")		, "5", "5", "0", "0", "130"},
				            		{MessageResolver.getText("재고수량")		, "6", "6", "0", "0", "130"},
				            		{MessageResolver.getText("이동중수량")		, "7", "7", "0", "0", "130"},
				            		{MessageResolver.getText("제품상태")		, "8", "8", "0", "0", "130"},
				            		{MessageResolver.getText("로케이션")		, "9", "9", "0", "0", "130"},
				            		{MessageResolver.getText("로케이션 유형")	, "10", "10", "0", "0", "130"},
				            		{MessageResolver.getText("입수")			, "11", "11", "0", "0", "130"},
				            		{MessageResolver.getText("UOM")			, "12", "12", "0", "0", "130"},
				            		{MessageResolver.getText("재고중량")		, "13", "13", "0", "0", "130"},
				            		{MessageResolver.getText("창고ID")		, "14", "14", "0", "0", "130"},
				            		{MessageResolver.getText("환산PLT수량")	, "15", "15", "0", "0", "130"},
				            		{MessageResolver.getText("입고예정수량")	, "16", "16", "0", "0", "130"},
				            		{MessageResolver.getText("출고예정수량")	, "17", "17", "0", "0", "130"},
				            		{MessageResolver.getText("실제PLT수량")	, "18", "18", "0", "0", "130"},
				            		{MessageResolver.getText("물류센터ID")		, "19", "19", "0", "0", "130"},
				            		{MessageResolver.getText("로케이션ID")		, "20", "20", "0", "0", "130"},
				            		{MessageResolver.getText("물류용기여부")	, "21", "21", "0", "0", "130"},
				            		{MessageResolver.getText("재고ID")		, "22", "22", "0", "0", "130"},
				            		{MessageResolver.getText("내부LOT번호")	, "23", "23", "0", "0", "230"},
				            		{MessageResolver.getText("유효기간")		, "24", "24", "0", "0", "130"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"SUBUL_DT" 			,"S"},
				            		{"LC_CD" 				,"S"},
				            		{"CUST_NM" 				,"S"},
				            		{"CUST_LOT_NO" 			,"S"},
				            		{"RITEM_CD" 			,"S"},
				            		{"RITEM_NM" 			,"S"},
				            		{"STOCK_QTY" 			,"NR"},
				            		{"NOW_MOVING_QTY"		,"NR"},
				            		{"ITEM_STAT" 			,"S"},
				            		{"LOC_CD" 				,"S"},
				            		{"LOC_TYPE" 			,"S"},
				            		{"UNIT_NM" 				,"S"},
				            		{"UOM_NM" 				,"S"},
				            		{"STOCK_WEIGHT" 		,"S"},
				            		{"WH_ID" 				,"S"},
			            			{"EXP_PLT_QTY" 			,"NR"},
			            			{"IN_EXP_QTY" 			,"NR"},
			            			{"OUT_EXP_QTY" 			,"NR"},
			            			{"REAL_PLT_QTY" 		,"NR"},
				            		{"LC_ID" 				,"S"},
				            		{"LOC_ID" 				,"S"},
				            		{"POOL_YN" 				,"S"},
				            		{"STOCK_ID" 			,"S"},
				            		{"SUB_LOT_ID" 			,"S"},
				            		{"ITEM_BEST_DATE_END"	,"S"}
				            		
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("배송센터 일별재고조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	

	/*-
	 * Method ID : excelDownP2
	 * Method 설명 : 배송센터일별재고조회 - 입출고이력조회 엑셀
	 * 작성자 : 김채린
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST056/excelDownP2.action")
	public void popListExcel2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
//			map = service.list0Excel(model);
			map = service.listPop2Excel(model);
			
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownPop2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDownPop2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("물류센터")  	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("작업일자")  	, "1", "1", "0", "0", "160"},
				            		{MessageResolver.getText("작업시간")  	, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("작업타입")	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("상품코드")	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("상품명")	, "5", "5", "0", "0", "130"},
				            		{MessageResolver.getText("UOM")		, "6", "6", "0", "0", "130"},
				            		{MessageResolver.getText("작업수량")	, "7", "7", "0", "0", "130"},
				            		{MessageResolver.getText("작업자")	, "8", "8", "0", "0", "130"},
				            		{MessageResolver.getText("작업자")	, "9", "9", "0", "0", "130"},
				            		{MessageResolver.getText("비고1")		, "10", "10", "0", "0", "130"},
				            		{MessageResolver.getText("비고2")		, "11", "11", "0", "0", "130"},
				            		{MessageResolver.getText("로케이션")	, "12", "12", "0", "0", "130"},
				            		{MessageResolver.getText("LOT_NO")	, "13", "13", "0", "0", "130"},
				            		{MessageResolver.getText("주문구분")	, "14", "14", "0", "0", "130"},
				            		{MessageResolver.getText("상세구분")	, "15", "15", "0", "0", "130"},
				            		{MessageResolver.getText("착지정보")	, "16", "16", "0", "0", "130"}
				            		
//				            		
//				            		{MessageResolver.getText("합계")   	, "5", "5", "0", "0", "100"},
//				            		{MessageResolver.getText("예상운송료") , "6", "6", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"LC_NM" 			,"S"},
				            		{"WORK_DT" 			,"S"},
				            		{"REG_TM" 			,"S"},
				            		{"WORK_SUBTYPE_NM" 	,"S"},
				            		{"RITEM_CD" 		,"S"},
				            		{"RITEM_NM" 		,"S"},
				            		{"WORK_UOM_NM" 		,"S"},
				            		{"WORK_QTY" 		,"NR"},
				            		{"REG_NO" 			,"S"},
				            		{"REG_NM" 			,"S"},
				            		{"ORD_DESC" 		,"S"},
				            		{"ETC2" 		,"S"},
				            		{"LOC_NM" 			,"S"},
				            		{"CUST_LOT_NO" 		,"S"},
				            		{"P_WORK_TYPE_NM" 	,"S"},
			            			{"P_WORK_SUBTYPE_NM" ,"S"},
				            		{"P_CUST_NM" 		,"S"}
				            		
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("입출고 이력조회");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
}