package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST230Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST230Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST230Service")
	private WMSST230Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 현재고 조회 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST230.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST230", service.selectBox(model));
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 현재고 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST230/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}


	/*-
	 * Method ID : sublist
	 * Method 설명 : 현재고 서브 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST230/sublist.action")
	public ModelAndView sublist(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get sub list :", e);
			}
		}
		return mav;
	}
	
	
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 현재고 조회 화면 (신규) 
	 * 작성자 : kijun11
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST231.action")
	public ModelAndView wmsst231(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST231", service.selectBox(model));
	}

	
	
	
	
	

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 현재고 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST230/hExcel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.hListExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
			                       {MessageResolver.getMessage("화주명"), "0", "0", "0", "1", "200"},
			                       {MessageResolver.getMessage("상품군"), "1", "1", "0", "1", "200"},
			                       {MessageResolver.getMessage("상품코드"), "2", "2", "0", "1", "200"},
			                       {MessageResolver.getMessage("상품명"), "3", "3", "0", "1", "200"},
			                       {MessageResolver.getMessage("UOM"), "4", "4", "0", "1", "200"},
                       
			            		   {model.get("LC_NM01").toString(), "5", "9", "0", "0", "200"},
			                       {model.get("LC_NM02").toString(), "10", "14", "0", "0", "200"},
			                       {model.get("LC_NM03").toString(), "15", "19", "0", "0", "200"},
			                       {model.get("LC_NM04").toString(), "20", "24", "0", "0", "200"},
			                       {model.get("LC_NM05").toString(), "25", "29", "0", "0", "200"},
			                       {model.get("LC_NM06").toString(), "30", "34", "0", "0", "200"},
			                       {model.get("LC_NM07").toString(), "35", "39", "0", "0", "200"},
			                       {model.get("LC_NM08").toString(), "40", "44", "0", "0", "200"},
			                       {model.get("LC_NM09").toString(), "45", "49", "0", "0", "200"},
			                       {model.get("LC_NM10").toString(), "50", "54", "0", "0", "200"},
			                       
			                       {model.get("LC_NM11").toString(), "55", "59", "0", "0", "200"},
			                       {model.get("LC_NM12").toString(), "60", "64", "0", "0", "200"},
			                       {model.get("LC_NM13").toString(), "65", "69", "0", "0", "200"},
			                     
			                       {model.get("LC_NM14").toString(), "70", "74", "0", "0", "200"},
			                       {model.get("LC_NM15").toString(), "75", "79", "0", "0", "200"},
			                       
                                   
			                       {MessageResolver.getMessage("총 재고")	   , "80", "80", "0", "1", "120"},
                                   {MessageResolver.getMessage("총 정상재고")  , "81", "81", "0", "1", "120"},
                                   {MessageResolver.getMessage("총 불량재고")  , "82", "82", "0", "1", "120"},
                                   {MessageResolver.getMessage("총 입고예정")  , "83", "83", "0", "1", "120"},
                                   {MessageResolver.getMessage("총 미배송")   , "84", "84", "0", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "5", "5", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "6", "6", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "7", "7", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "8", "8", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "9", "9", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "10", "10", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "11", "11", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "12", "12", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "13", "13", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "14", "14", "1", "1", "120"},
                                  
                                   {MessageResolver.getMessage("재고")	 , "15", "15", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "16", "16", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "17", "17", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "18", "18", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "19", "19", "1", "1", "120"},
                              
                                   {MessageResolver.getMessage("재고")	 , "20", "20", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "21", "21", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "22", "22", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "23", "23", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "24", "24", "1", "1", "120"},
                                  
                                   {MessageResolver.getMessage("재고")	 , "25", "25", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "26", "26", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "27", "27", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "28", "28", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "29", "29", "1", "1", "120"},
                                 
                                   {MessageResolver.getMessage("재고")	 , "30", "30", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "31", "31", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "32", "32", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "33", "33", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "34", "34", "1", "1", "120"},
                                
                                   {MessageResolver.getMessage("재고")	 , "35", "35", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "36", "36", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "37", "37", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "38", "38", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "39", "39", "1", "1", "120"},
                                 
                                   {MessageResolver.getMessage("재고")	 , "40", "40", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "41", "41", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "42", "42", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "43", "43", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "44", "44", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "45", "45", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "46", "46", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "47", "47", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "48", "48", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "49", "49", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "50", "50", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "51", "51", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "52", "52", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "53", "53", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "54", "54", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "55", "55", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "56", "56", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "57", "57", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "58", "58", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "59", "59", "1", "1", "120"},

                                   {MessageResolver.getMessage("재고")	 , "60", "60", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "61", "61", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "62", "62", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "63", "63", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "64", "64", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "65", "65", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "66", "66", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "67", "67", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "68", "68", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "69", "69", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "70", "70", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "71", "71", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "72", "72", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "73", "73", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "74", "74", "1", "1", "120"},
                                   
                                   {MessageResolver.getMessage("재고")	 , "75", "75", "1", "1", "120"},
                                   {MessageResolver.getMessage("정상재고")  , "76", "76", "1", "1", "120"},
                                   {MessageResolver.getMessage("불량재고")  , "77", "77", "1", "1", "120"},
                                   {MessageResolver.getMessage("입고예정")  , "78", "78", "1", "1", "120"},
                                   {MessageResolver.getMessage("미배송")   , "79", "79", "1", "1", "120"}
                                   
                                   
                                   
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"   , "S"},
                                    {"ITEM_GRP_NM"  , "S"},
                                    {"RITEM_CD"  , "S"},
                                    {"RITEM_NM"  , "S"},
                                    {"UOM_CD"  	 , "S"},
                                    
                                    {"STOCK_QTY_01"			, "N"},
                                    {"NORMAL_QTY_01"		, "N"},
                                    {"BAD_QTY_01"			, "N"},
                                    {"READY_IN_QTY_01"		, "N"},
                                    {"DLV_OUT_REQ_QTY_01"	, "N"},
                                    
                                    {"STOCK_QTY_02"			, "N"},
                                    {"NORMAL_QTY_02"		, "N"},
                                    {"BAD_QTY_02"			, "N"},
                                    {"READY_IN_QTY_02"		, "N"},
                                    {"DLV_OUT_REQ_QTY_02"	, "N"},
                                    
                                    {"STOCK_QTY_03"			, "N"},
                                    {"NORMAL_QTY_03"		, "N"},
                                    {"BAD_QTY_03"			, "N"},
                                    {"READY_IN_QTY_03"		, "N"},
                                    {"DLV_OUT_REQ_QTY_03"	, "N"},
                                    
                                    {"STOCK_QTY_04"			, "N"},
                                    {"NORMAL_QTY_04"		, "N"},
                                    {"BAD_QTY_04"			, "N"},
                                    {"READY_IN_QTY_04"		, "N"},
                                    {"DLV_OUT_REQ_QTY_04"	, "N"},
                                    
                                    {"STOCK_QTY_05"			, "N"},
                                    {"NORMAL_QTY_05"		, "N"},
                                    {"BAD_QTY_05"			, "N"},
                                    {"READY_IN_QTY_05"		, "N"},
                                    {"DLV_OUT_REQ_QTY_05"	, "N"},
                                    
                                    {"STOCK_QTY_06"			, "N"},
                                    {"NORMAL_QTY_06"		, "N"},
                                    {"BAD_QTY_06"			, "N"},
                                    {"READY_IN_QTY_06"		, "N"},
                                    {"DLV_OUT_REQ_QTY_06"	, "N"},
                                    
                                    {"STOCK_QTY_07"			, "N"},
                                    {"NORMAL_QTY_07"		, "N"},
                                    {"BAD_QTY_07"			, "N"},
                                    {"READY_IN_QTY_07"		, "N"},
                                    {"DLV_OUT_REQ_QTY_07"	, "N"},
                                    
                                    {"STOCK_QTY_08"			, "N"},
                                    {"NORMAL_QTY_08"		, "N"},
                                    {"BAD_QTY_08"			, "N"},
                                    {"READY_IN_QTY_08"		, "N"},
                                    {"DLV_OUT_REQ_QTY_08"	, "N"},
                                    
                                    {"STOCK_QTY_09"			, "N"},
                                    {"NORMAL_QTY_09"		, "N"},
                                    {"BAD_QTY_09"			, "N"},
                                    {"READY_IN_QTY_09"		, "N"},
                                    {"DLV_OUT_REQ_QTY_09"	, "N"},
                                    
                                    {"STOCK_QTY_10"			, "N"},
                                    {"NORMAL_QTY_10"		, "N"},
                                    {"BAD_QTY_10"			, "N"},
                                    {"READY_IN_QTY_10"		, "N"},
                                    {"DLV_OUT_REQ_QTY_10"	, "N"},
                                    
                                    {"STOCK_QTY_11"			, "N"},
                                    {"NORMAL_QTY_11"		, "N"},
                                    {"BAD_QTY_11"			, "N"},
                                    {"READY_IN_QTY_11"		, "N"},
                                    {"DLV_OUT_REQ_QTY_11"	, "N"},
                                    
                                    {"STOCK_QTY_12"			, "N"},
                                    {"NORMAL_QTY_12"		, "N"},
                                    {"BAD_QTY_12"			, "N"},
                                    {"READY_IN_QTY_12"		, "N"},
                                    {"DLV_OUT_REQ_QTY_12"	, "N"},
                                    
                                    {"STOCK_QTY_13"			, "N"},
                                    {"NORMAL_QTY_13"		, "N"},
                                    {"BAD_QTY_13"			, "N"},
                                    {"READY_IN_QTY_13"		, "N"},
                                    {"DLV_OUT_REQ_QTY_13"	, "N"},
                                    
                                    {"STOCK_QTY_14"			, "N"},
                                    {"NORMAL_QTY_14"		, "N"},
                                    {"BAD_QTY_14"			, "N"},
                                    {"READY_IN_QTY_14"		, "N"},
                                    {"DLV_OUT_REQ_QTY_14"	, "N"},
                                    
                                    {"STOCK_QTY_15"			, "N"},
                                    {"NORMAL_QTY_15"		, "N"},
                                    {"BAD_QTY_15"			, "N"},
                                    {"READY_IN_QTY_15"		, "N"},
                                    {"DLV_OUT_REQ_QTY_15"	, "N"},
                                    
                                    {"ROW_SUM_1"			, "N"},
                                    {"ROW_SUM_2"			, "N"},
                                    {"ROW_SUM_3"			, "N"},
                                    {"ROW_SUM_5"			, "N"},
                                    {"ROW_SUM_4"			, "N"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText(MessageResolver.getMessage("현재고조회(센터별)"));
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : listExcel2
	 * Method 설명 : 현재고 상세 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST230/excel2.action")
	public void listExcel2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.dListExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	/*-
	 * Method ID : doExcelDown3
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품"), "2", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("재고수량"), "4", "4", "0", "0", "200"},
                                   {"UOM", "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("PLT수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("B/L번호"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("창고"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호"), "9", "9", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품등급"), "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("제조일자"), "11", "11", "0", "0", "200"},
                                   {"STOCK_ID", "12", "12", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_DT"          , "S"},
                                    {"LOC_NM"           , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    
                                    {"STOCK_QTY"  		, "N"},
                                    {"UOM_NM"           , "S"},
                                    {"REAL_PLT_QTY"		, "N"},
                                    {"BL_NO"         	, "S"},
                                    {"WH_NM"         	, "S"},
                                    
                                    {"CUST_LOT_NO"      , "S"},
                                    {"ITEM_CLASS"       , "S"},
                                    {"MAKE_DT"          , "S"},
                                    {"STOCK_ID"         , "S"},
                                   
                                   }; 
            
            //파일명
            String fileName ="WMSST";
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
            if (log.isErrorEnabled()) {
               	log.error("fail download Excel file...", e);
            }
        }
    }
	
	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST230/sampleExcelDown.action")
	public void sampleExcelDown(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.getSampleExcelDown(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doSampleExcelDown2(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

	protected void doSampleExcelDown2(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
		try {
			int iColSize = Integer.parseInt(model.get("Col_Size").toString());
			int iHRowSize = 6; // 고정값
			int iVRowSize = 2; // 고정값
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			String[][] headerEx = new String[iColSize][iHRowSize];
			String[][] valueName = new String[iColSize][iVRowSize];
			for (int i = 0; i < iColSize; i++) {
				String nName = MessageResolver.getText((String) model.get("Name_" + i)).replace("&lt;br&gt;", "").replace("&lt;/font&gt;&lt;br&gt;", "").replace("&lt;/font&gt;", "").replace("&lt;font color=&quot;#B1B1B1&quot;&gt;", "");
				// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
				headerEx[i] = new String[]{nName, i + "", i + "", "0", "0", "100"};
				// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
				// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..	
				if(i <= 5){
					
					valueName[i] = new String[]{(String) model.get("Col_" + i), "S"};
				}else{
					valueName[i] = new String[]{(String) model.get("Col_" + i), "N"};
				}
			}
			// 파일명
			String fileName = MessageResolver.getText("현재고조회(센터별처리현황)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
