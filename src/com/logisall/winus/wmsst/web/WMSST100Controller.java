package com.logisall.winus.wmsst.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST100Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSST100Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSST100Service")
	private WMSST100Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 명의변경 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSST100.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST100", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 명의변경 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST100/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : popmn
	 * Method 설명 : 명의변경 pop화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST100pop.action")
	public ModelAndView popmn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("winus/wmsst/WMSST100E2", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get pop list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : subSearch
	 * Method 설명 : 명의변경 상세 그리드 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST100/subSearch.action")
	public ModelAndView subSearch(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.getSubSearch(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search result :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : Confirm
	 * Method 설명 : 명의변경 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST100/Confirm.action")
	public ModelAndView updateConfirm(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveConfirm(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : delete
	 * Method 설명 : 명의변경 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST100/delete.action")
	public ModelAndView delete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.delete(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("delete.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 명의변경 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSST100/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}

    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("작업번호"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("완료"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("양도자"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품"), "4", "4", "0", "0", "200"},
                                   {MessageResolver.getMessage("수량"), "5", "5", "0", "0", "200"},
                                   {"UOM", "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("양수자"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("수량"), "9", "9", "0", "0", "200"},
                                   {"UOM", "10", "10", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CHANGE_WORK_ID_CUT"          , "S"},
                                    {"WORK_STAT_EXCEL"           , "S"},
                                    {"WORK_DT"           , "S"},
                                    {"SELL_CUST_NM"          , "S"},
                                    {"SELL_RITEM_NM"          , "S"},
                                    {"SELL_QTY"            , "N"},
                                    {"SELL_UOM_NM"            , "S"},
                                    {"BUY_CUST_NM"            , "S"},
                                    {"BUY_RITEM_NM"            , "S"},
                                    {"BUY_QTY"            , "N"},
                                    {"BUY_UOM_NM"            , "S"},
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("명의변경");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : wmsst100E2
	 * Method 설명      : 명의변경 팝업
	 * 작성자                 : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST100E2.action")
	public ModelAndView wmsst100E2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("winus/wmsst/WMSST100E2");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : detail
	 * Method 설명      : 명의변경 상세조회
	 * 작성자                 : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST100E2/detail.action")
	public ModelAndView detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : save
	 * Method 설명      : 명의변경 저장
	 * 작성자                 : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST100E2/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSST100E2/saveAllLoc.action")
	public ModelAndView saveAllLoc(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveAllLoc(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
}