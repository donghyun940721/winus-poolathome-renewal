package com.logisall.winus.wmsst.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsst.service.WMSST040Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSST040Controller {
	protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSST040Service")
    private WMSST040Service service;

    static final String[] COLUMN_NAME_WMSST040E5 = {"ITEM_CODE", "FROM_LOC_CD", "TO_LOC_CD", "WORK_QTY"};
    
    /*-
     * Method ID : mn
     * Method 설명 : 재고이동 화면
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSST040.action")
    public ModelAndView mn(Map<String, Object> model) throws Exception{
        ModelAndView mav = null;
        try {
            mav = new ModelAndView("winus/wmsst/WMSST040", service.selectBox(model));  // 맨 앞에 / 없음에 주의, .vm 없음에 주의

        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
        }
        return mav;
    }
   

    
    /*-
     * Method ID : list
     * Method 설명 : 재고이동 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSST040/list.action")
    public ModelAndView list(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
        } 
        return mav;
    }

    
    /*-
     * Method ID : detail
     * Method 설명 : 재고이동상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    @RequestMapping("/WMSST040/detail.action")
    public ModelAndView detail(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.detail(model));
        } catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
        } 
        return mav;
    }
    
    /*-
     * Method ID : listExcel
     * Method 설명 : 재고이동 엑셀다운
     * 작성자 : 기드온
     * @param request
     * @param response
     * @param model
     */
    @RequestMapping("/WMSST040/excel.action")
    public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcel(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelDown(response, grs);
            }
        }catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
        }
    }
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("상태")		, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("물류센터")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업번호")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일자")		, "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("작업시간")		, "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("FROM창고")	, "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("FROM로케이션")	, "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("TO창고")		, "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("TO로케이션")	, "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("상품코드")		, "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명")		, "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업수량")		, "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품UOM")		, "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호")		, "14", "14", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("사유")		, "15", "15", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_STAT"		, "S"},
                                    {"LC_NM"			, "S"},
                                    {"CUST_NM"			, "S"},
                                    {"WORK_ID"			, "S"},
                                    {"MOVE_DT"			, "S"},
                                    
                                    {"WORK_TM"			, "S"},
                                    {"FROM_WH_NM"		, "S"},
                                    {"FROM_LOC_CD"		, "S"},
                                    {"TO_WH_NM"			, "S"},
                                    {"TO_LOC_CD"		, "S"},
                                    
                                    {"ITEM_CODE"		, "S"},
                                    {"ITEM_NM"			, "S"},
                                    {"MOVE_QTY"			, "S"},
                                    {"UOM_NM"			, "S"},
                                    {"CUST_LOT_NO"		, "S"},
                                    
                                    {"REASON_CD"		, "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("재고이동내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID   : wmsst040E3
	 * Method 설명    : 재고이동 팝업
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E3.action")
	public ModelAndView wmsst040E3(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST040E3", service.selectBox2(model));  // 맨 앞에 / 없음에 주의, .vm 없음에 주의
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pop :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : listDetail
	 * Method 설명    : 재고이동 팝업 리스트
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E3/listDetail.action")
	public ModelAndView listDetail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pop :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : listHeader
	 * Method 설명    : 재고이동 팝업 리스트 (Header)
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E3/listHeader.action")
	public ModelAndView listHeader(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listHeader(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : del
	 * Method 설명    : 재고이동 지시 취소
	 * 작성자               : 김진석
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E3/del.action")
	public ModelAndView del(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.del(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to cancel :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("del.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : work
	 * Method 설명    : 재고이동 지시 팝업
	 * 작성자               : 김진석
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E3/work.action")
	public ModelAndView work(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.work(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to work process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("work.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : save
	 * Method 설명    : 재고이동 팝업 저장
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E3/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : save
	 * Method 설명    : 재고이동 팝업 저장
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E3/save_grn.action")
	public ModelAndView saveGrn(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveGrn(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save grn :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID   : wmsst040E4
	 * Method 설명    : 재고이동 팝업2
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E4.action")
	public ModelAndView wmsst040E4(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsst/WMSST040E4");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to pop :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : listRackDetail
	 * Method 설명    : 재고이동 팝업 리스트2 (lack보충을 통한 재고이동)
	 * 작성자               : chSong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSST040E4/listRackDetail.action")
	public ModelAndView listRackDetail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listRackDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
     * Method ID    : WMSST040T1
     * Method 설명        : 재고이동 통합조회 팝업
     * 작성자                     : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSST040T1.action")
    public ModelAndView wmsst040T1(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsst/WMSST040T1");     
    }
    
    /**
     * Method ID    : WMSST040T2
     * Method 설명        : 재고이동 작업상태조회 팝업
     * 작성자                     : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSST040T2.action")
    public ModelAndView wmsst040T2(Map<String, Object> model) throws Exception {
        return new ModelAndView("winus/wmsst/WMSST040T2");     
    }
    
	/**
     * Method ID    : DetailPop
     * Method 설명        : 재고이동 통합조회 팝업
     * 작성자                     : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSST040/DetailPop.action")
    public ModelAndView DetailPop(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.DetailPop(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID    : workStatPop
     * Method 설명        : 재고이동 통합조회 팝업
     * 작성자                     : chSong
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSST040/workStatPop.action")
    public ModelAndView workStatPop(Map<String, Object> model) {
        ModelAndView mav = null;
        try {           
            mav = new ModelAndView("jqGridJsonView", service.workStatPop(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
     * Method ID : listExcelTotal
     * Method 설명 : 재고이동 통합조회 팝업
     * 작성자 : 기드온
     * @param request
     * @param response
     * @param model
     */
    @RequestMapping("/WMSST040/excelTotal.action")
    public void listExcelTotal(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcelTotal(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelTotalDown(response, grs);
            }
        }catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
        }
    }
    protected void doExcelTotalDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("상태")      , "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업번호")    , "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주")      , "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("CUST_ID")  , "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("CUST_CD")  , "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("작업일자")    , "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업시간")    , "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("FROM로케이션"), "7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("TO로케이션")  , "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품명") 	   , "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("RITEM_ID") , "10", "10", "0", "0", "200"},
                                   {MessageResolver.getMessage("ITEM_CODE"), "11", "11", "0", "0", "200"},
                                   {MessageResolver.getMessage("LOT번호")   , "12", "12", "0", "0", "200"},
                                   {MessageResolver.getMessage("재고수량")    , "13", "13", "0", "0", "200"},
                                   {MessageResolver.getMessage("이동수량")    , "14", "14", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("UOM")      , "15", "15", "0", "0", "200"},
                                   {MessageResolver.getMessage("사유")       , "16", "16", "0", "0", "200"},
                                   {MessageResolver.getMessage("SUB_LOT_ID"), "17", "17", "0", "0", "200"},
                                   {MessageResolver.getMessage("FROM창고")   , "18", "18", "0", "0", "200"},
                                   {MessageResolver.getMessage("TO창고")     , "19", "19", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("POOL_YN")   , "20", "20", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"WORK_STAT"          , "S"},
                                    {"STOCK_MOVE_ID"      , "S"},
                                    {"CUST_NM"            , "S"},
                                    {"CUST_ID"            , "S"},
                                    {"CUST_CD"            , "S"},
                                    
                                    {"ITEM_WORK_DT"       , "S"},
                                    {"ITEM_WORK_TM"       , "S"},
                                    {"FROM_LOC_CD"        , "S"},
                                    {"TO_LOC_CD"          , "S"},
                                    {"ITEM_NM"            , "S"},
                                    
                                    {"RITEM_ID"           , "S"},
                                    {"ITEM_CODE"          , "S"},
                                    {"CUST_LOT_NO"        , "S"},
                                    {"STOCK_QTY"          , "S"},
                                    {"MOVE_QTY"           , "S"},
                                    
                                    {"UOM_NM"              , "S"},
                                    {"REASON_CD"           , "S"},
                                    {"SUB_LOT_ID"          , "S"},
                                    {"FROM_WH_ID"          , "S"},
                                    {"TO_WH_ID"            , "S"},
                                    
                                    {"POOL_YN"             , "S"}
                                   }; 
            
			// 파일명
			String fileName = "WMSST040T1";
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
    
    /*-
	 * Method ID : wmsst040e5
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST040E5.action")
	public ModelAndView wmsst040e5(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsst/WMSST040E5");
	}
	
	/*-
	 * Method ID : uploadInfo
	 * Method 설명 : 엑셀파일업로드
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param txtFile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSST040/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSST040E5, 0, startRow, 10000, 0);

			Map<String, Object> mapBody = new HashMap<String, Object>();
			mapBody.put("LIST", list);
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			//run
			m = service.saveUploadData(mapBody);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
}