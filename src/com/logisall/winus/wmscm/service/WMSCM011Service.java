package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM011Service {

	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> listClient(Map<String, Object> model) throws Exception;
	public Map<String, Object> getTypicalCust(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustLcGb(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustIdByLcId(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustByRestApiKey(Map<String, Object> model) throws Exception;
	public Map<String, Object> listLcSync(Map<String, Object> model) throws Exception;
	public Map<String, Object> listCyclCust(Map<String, Object> model) throws Exception;
	public Map<String, Object> getCustEtcCode(Map<String, Object> model) throws Exception;
}
