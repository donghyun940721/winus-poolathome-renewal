package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM210Service {

    public Map<String, Object> getTp(Map<String, Object> model) throws Exception;
    public Map<String, Object> getResult(Map<String, Object> model) throws Exception;
    public Map<String, Object> getTpGrid(Map<String, Object> model) throws Exception;
    public Map<String, Object> getResultGrid(Map<String, Object> model) throws Exception;
	public Map<String, Object> getTpList(Map<String, Object> model) throws Exception;
	public Map<String, Object> getTpListALL(Map<String, Object> model) throws Exception;
}
