package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM300Service {

	public Map<String, Object> list(Map<String, Object> model) throws Exception;

	public Map<String, Object> defaultList(Map<String, Object> model) throws Exception;

	public Map<String, Object> save(Map<String, Object> model) throws Exception;

	public Map<String, Object> allList(Map<String, Object> model)throws Exception;

	public Map<String, Object> fixColumnSave(Map<String, Object> model) throws Exception;

	public Map<String, Object> templateList(Map<String, Object> model) throws Exception;

	public Map<String, Object> forceUpdate(Map<String, Object> model) throws Exception;

	public Map<String, Object> originList(Map<String, Object> model) throws Exception;

	public Map<String, Object> adminSave(Map<String, Object> model) throws Exception;

	public Map<String, Object> createNewTemplate(Map<String, Object> model) throws Exception;

}
