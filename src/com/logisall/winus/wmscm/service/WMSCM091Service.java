package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM091Service {

    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItemGrn(Map<String, Object> model) throws Exception;
    public Map<String, Object> listLotno(Map<String, Object> model) throws Exception;
    public Map<String, Object> listGrid(Map<String, Object> model) throws Exception;
    public Map<String, Object> listGrp(Map<String, Object> model) throws Exception;
}
