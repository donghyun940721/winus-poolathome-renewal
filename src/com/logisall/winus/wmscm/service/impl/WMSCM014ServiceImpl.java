package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmscm.service.WMSCM014Service;

@Service("WMSCM014Service")
public class WMSCM014ServiceImpl implements WMSCM014Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM014Dao")
    private WMSCM014Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 조직도  조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.list(model));
        return map;
    }
}
