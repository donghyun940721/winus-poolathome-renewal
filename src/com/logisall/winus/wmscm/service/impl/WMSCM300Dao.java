package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM300Dao")
public class WMSCM300Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * Method ID : list
	 * Method 설명 : 그리드 리스트 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmsms300.list", model));
		return wqrs;
	}
	
	/**
	 * Method ID : list
	 * Method 설명 : 그리드 템플릿 카운트
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public long templateCount(Map<String, Object> model) {
		return ((Integer)executeQueryForObject("wmsms300.templateCount", model)).intValue();
	}
	
	/**
	 * Method ID : list
	 * Method 설명 : 그리드 기본 템플릿 삽입
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public long insertDefaultTemplate(Map<String, Object> model) {
		return ((Integer)executeInsert("wmsms300.insertDefaultTemplate", model)).intValue();
	}
	
	/**
	 * Method ID : list
	 * Method 설명 : 그리드 템플릿 기본 컬럼 삽입
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void insertDefaultColumns(Map<String, Object> model) {
		executeInsert("wmsms300.insertDefaultColumns", model);
		
	}
	
	/**
	 * Method ID : list
	 * Method 설명 : 그리드 기본 템플릿 삽입
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public long insertLcIdTemplate(Map<String, Object> model) {
		return ((Integer)executeInsert("wmsms300.insertLcIdTemplate", model)).intValue();
	}
	
	/**
	 * Method ID : list
	 * Method 설명 : 그리드 템플릿 기본 컬럼 삽입
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void insertLcIdColumns(Map<String, Object> model) {
		executeInsert("wmsms300.insertLcIdColumns", model);
		
	}
	
	/**
	 * Method ID : save
	 * Method 설명 : 그리드 템플릿 기본 컬럼 삽입
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void save(Map<String, Object> model) {
		executeUpdate("wmsms300.save", model);
		
	}

	/**
	 * Method ID : list
	 * Method 설명 : 그리드 리스트 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public GenericResultSet allList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmsms300.allList", model));
		return wqrs;
	}

	public void fixColumnSave(Map<String, Object> model) {
		executeUpdate("wmsms300.fixColumnSave", model);
	}

	public int checkLcIdTemplate(Map<String, Object> model) {
		return ((Integer)executeQueryForObject("wmsms300.checkLcIdTemplate", model)).intValue();
	}

	public GenericResultSet templateList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmsms300.templateList", model));
		return wqrs;
	}

	/**
	 * Method ID : list
	 * Method 설명 : 그리드 리스트 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public GenericResultSet defaultList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmsms300.defaultList", model));
		return wqrs;
	}
	
	/**
	 * Method ID : forceUpdate
	 * Method 설명 : 사용자 컬럼 강제 업데이트
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public int forceUpdate(Map<String, Object> model) {
		return ((Integer)executeUpdate("wmsms300.forceUpdate", model)).intValue();
		
	}
	
	/**
	 * Method ID : defaultTemplateCount
	 * Method 설명 : 기본템플릿 여부 체크
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public long defaultTemplateCount(Map<String, Object> model) {
		return ((Integer)executeQueryForObject("wmsms300.defaultTemplateCount", model)).intValue();
	}
	/**
	 * Method ID : createDefaultTemplate
	 * Method 설명 : 기본템플릿 생성
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public long createDefaultTemplate(Map<String, Object> model) {
		return ((Integer)executeInsert("wmsms300.createDefaultTemplate", model)).intValue();
		
	}
	/**
	 * Method ID : createDefaultColumns
	 * Method 설명 : 기본템플릿 컬럼 생성
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void createDefaultColumns(Map<String, Object> model) {
		executeInsert("wmsms300.createDefaultColumns", model);
	}

	/**
	 * Method ID : list
	 * Method 설명 : 그리드 리스트 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public GenericResultSet originList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmsms300.originList", model));
		return wqrs;
	}

	/**
	 * Method ID : adminSave
	 * Method 설명 : 그리드 템플릿 기본 컬럼 업데이트
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void adminSave(Map<String, Object> model) {
		executeUpdate("wmsms300.adminSave", model);
		
	}
	
	/**
	 * Method ID : adminInsert
	 * Method 설명 : 그리드 템플릿 기본 컬럼 업데이트
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void adminInsert(Map<String, Object> model) {
		executeInsert("wmsms300.adminInsert", model);
		
	}
	
	/**
	 * Method ID : createNewTemplate
	 * Method 설명 : 템플릿 추가
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void createNewTemplate(Map<String, Object> model) {
		executeInsert("wmsms300.createNewTemplate", model);
		
	}
	
	/**
	 * Method ID : adminNewInsert
	 * Method 설명 : 그리드 템플릿 기본 컬럼 업데이트
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public void adminNewInsert(Map<String, Object> model) {
		executeInsert("wmsms300.adminNewInsert", model);
		
	}
	
   
}
