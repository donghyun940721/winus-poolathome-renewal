package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmscm.service.WMSCM202Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM202Service")
public class WMSCM202ServiceImpl implements WMSCM202Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM202Dao")
    private WMSCM202Dao dao;

    /**
     * Method ID : list
     * Method 설명 : 파트너 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.list(model));
                        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }

}
