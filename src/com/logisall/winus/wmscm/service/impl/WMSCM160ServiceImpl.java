package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmscm.service.WMSCM160Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM160Service")
public class WMSCM160ServiceImpl implements WMSCM160Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM160Dao")
    private WMSCM160Dao dao;

//    /**
//     * 대체 Method ID   : selectData
//     * 대체 Method 설명    : 상품 목록 필요 데이타셋
//     * 작성자                      : chsong
//     * @param   model
//     * @return
//     * @throws  Exception
//     */
//    @Override
//    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
//        Map<String, Object> map = new HashMap<String, Object>();
//        map.put("ITEMGRP", dao.selectItemGrp(model));
//        return map;
//    }

    /**
     * Method ID     : list
     * Method 설명       : 물류용기목록 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

}
