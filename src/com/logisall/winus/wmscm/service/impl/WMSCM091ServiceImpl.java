package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmscm.service.WMSCM091Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM091Service")
public class WMSCM091ServiceImpl implements WMSCM091Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM091Dao")
    private WMSCM091Dao dao;

    /**
     * 대체 Method ID   : selectData
     * 대체 Method 설명    : 상품 목록 필요 데이타셋
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }

    /**
     * Method ID     : list
     * Method 설명       : 상품목록 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if("20".equals(model.get("SS_USER_GB"))){
                model.put("vrSrchCustId", model.get(ConstantIF.SS_CLIENT_CD));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID  : listItemGrn
     * Method 설명  : 상품조회리스트 (크로스도킹용)
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Map<String, Object> listItemGrn(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listItemGrn(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID     : listLotno
     * Method 설명       : 상품목록 조회 LOTNO 조회조건추가
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listLotno(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if("20".equals(model.get("SS_USER_GB"))){
                model.put("vrSrchCustId", model.get(ConstantIF.SS_CLIENT_CD));
            }
            map.put("LIST", dao.listLotno(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID     : listGrid
     * Method 설명       : Grid 상품목록 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listGrid(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if("20".equals(model.get("SS_USER_GB"))){
                model.put("vrSrchCustId", model.get(ConstantIF.SS_CLIENT_CD));
            }
            map.put("LIST", dao.listGrid(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID     : listGrp
     * Method 설명       : 상품군목록 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.listGrp(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
}