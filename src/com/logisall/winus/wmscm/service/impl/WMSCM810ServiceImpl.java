package com.logisall.winus.wmscm.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmscm.service.WMSCM810Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM810Service")
public class WMSCM810ServiceImpl implements WMSCM810Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM810Dao")
    private WMSCM810Dao dao;

    /**
     * 대체 Method ID   : selectData
     * 대체 Method 설명    : 상품 목록 필요 데이타셋
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }

    /**
     * Method ID     : list
     * Method 설명       : 상품목록 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if("20".equals(model.get("SS_USER_GB"))){
                model.put("vrSrchCustId", model.get(ConstantIF.SS_CLIENT_CD));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : list2
     * Method 설명 : 화주 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            List<String> strUseTransCust = new ArrayList();
            String[] spUseTransCust = model.get("S_USE_TRANS_CUST").toString().split(",");
            for (String keyword : spUseTransCust ){
            	strUseTransCust.add(keyword);
            }
            model.put("vrSrchUseTransCustArr", strUseTransCust);
            
            map.put("LIST", dao.list2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }

        return map;
    }
}