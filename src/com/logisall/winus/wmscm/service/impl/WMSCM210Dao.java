package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM210Dao")
public class WMSCM210Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
    /**
     * Method ID    : getTp
     * Method 설명      : getTp
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object getTp(Map<String, Object> model){
        executeUpdate("wmscm210.pk_wmscm210.sp_sel_tp", model);
        return model;
    }

    //wmscm210.pk_wmscm210.sp_sel_tpcol
    /**
     * Method ID    : getTp
     * Method 설명      : 화주별 컬럼
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object search(Map<String, Object> model){
        executeUpdate("wmscm210.pk_wmscm210.sp_sel_tpcol", model);
        return model;
    }
    
    /**
     * Method ID    : getTpGrid
     * Method 설명      : getTpGrid
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object getTpGrid(Map<String, Object> model){
        executeUpdate("wmscm210.pk_wmscm210.sp_sel_tp_grid", model);
        return model;
    }

    //wmscm210.pk_wmscm210.sp_sel_tpcol
    /**
     * Method ID    : getTpGrid
     * Method 설명      : 화주별 컬럼
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object searchGrid(Map<String, Object> model){
        executeUpdate("wmscm210.pk_wmscm210.sp_sel_tpcol_grid", model);
        return model;
    }
    
	
    /**
     * Method ID    : getTpList
     * Method 설명      : getTpList
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object getTpList(Map<String, Object> model){
        executeUpdate("wmscm210.pk_wmscm210.sp_sel_tp_list", model);
        return model;
    }
    

    /**
     * Method ID    : getTpListALL
     * Method 설명      : getTpListALL
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object getTpListALL(Map<String, Object> model){
    	executeUpdate("wmscm210.pk_wmscm210.sp_sel_tp_list_cust", model);
    	return model;
    }

}
