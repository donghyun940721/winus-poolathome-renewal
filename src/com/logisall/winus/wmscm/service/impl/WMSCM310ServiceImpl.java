package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmscm.service.WMSCM310Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM310Service")
public class WMSCM310ServiceImpl implements WMSCM310Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM310Dao")
    private WMSCM310Dao dao;

    /**
     * Method ID : list
     * Method 설명 :
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
           
            map.put("LIST", dao.list(model));
                        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }

    /**
     * Method ID : list
     * Method 설명 : 
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	String custZoneId = (String)model.get("CUST_ZONE_ID");
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()); i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("CUST_ID", model.get("CUST_ID" + i));
	             modelDt.put("CUST_ZONE_ID", custZoneId);
	             
	             long seq = dao.custZoneCount(modelDt);
	             //System.out.println("seq :::: " + seq);
	             modelDt.put("WORK_SEQ", seq);
	             
	             
	             modelDt.put("WORK_IP", model.get("SS_CLIENT_IP"));
	             modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
	             
	             dao.save(modelDt);
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
}
