package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM011Dao")
public class WMSCM011Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * Method ID : list
	 * Method 설명 : 화주 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsms010.list", model);
	}
	
	   /**
     * Method ID : listStore
     * Method 설명 : 화주 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listStore(Map<String, Object> model) {
        return executeQueryPageWq("wmsms010.listStore", model);
    }
	
    /**
     * Method ID  : getTypicalCust
     * Method 설명  : getTypicalCust
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getTypicalCust(Map<String, Object> model){
        return executeQueryForList("wmsms010.getTypicalCust", model);
    }
    
    /**
     * Method ID  : getCustLcGb
     * Method 설명  : getCustLcGb
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getCustLcGb(Map<String, Object> model){
        return executeQueryForList("wmsms010.getCustLcGb", model);
    }
    
    /**
     * Method ID  : getCustIdByLcId
     * Method 설명  : LC_ID로 해당 코드의 화주ID 가져오기
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getCustIdByLcId(Map<String, Object> model){
        return executeQueryForList("wmsms010.getCustIdByLcId", model);
    }
    
    /**
     * Method ID : lcAllListStore
     * Method 설명 : 화주 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet lcAllListStore(Map<String, Object> model) {
        return executeQueryPageWq("wmsms010.lcAllListStore", model);
    }
    
    /**
     * Method ID  : getCustByRestApiKey
     * Method 설명  : getCustByRestApiKey
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getCustByRestApiKey(Map<String, Object> model){
        return executeQueryForList("wmsms010.getCustByRestApiKey", model);
    }
    
    /**
	 * Method ID : listLcSync
	 * Method 설명 : 화주 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet listLcSync(Map<String, Object> model) {
		return executeQueryPageWq("wmscm011.listLcSync", model);
	}
	
	/**
	 * Method ID : listCyclCust
	 * Method 설명 : 풀앳홈 재고조사 화주 리스트 
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public GenericResultSet listCyclCust(Map<String, Object> model) {
		return executeQueryPageWq("wmsms011.listCyclCust", model);
	}
	
	/**
     * Method ID	: getCustEtcCode
     * Method 설명 	: 화주별 기타마스터 코드 가져오기
     * @param model
     * @return
     */
    public Object getCustEtcCode(Map<String, Object> model){
        return executeQueryForList("wmscm011.getCustEtcCode", model);
    }
    
}
