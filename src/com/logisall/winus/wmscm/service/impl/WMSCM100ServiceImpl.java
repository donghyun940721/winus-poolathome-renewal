package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmscm.service.WMSCM100Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM100Service")
public class WMSCM100ServiceImpl implements WMSCM100Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM100Dao")
    private WMSCM100Dao dao;


    /**
     * Method ID     : list
     * Method 설명       : UOM 조회
     * 작성자                  : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

}
