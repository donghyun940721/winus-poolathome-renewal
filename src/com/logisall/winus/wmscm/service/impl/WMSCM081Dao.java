package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM081Dao")
public class WMSCM081Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
     * Method ID  : list
     * Method 설명  : ZONE조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms081.listZonePop", model);
    }   
    
    /**
     * Method ID  : listCust
     * Method 설명  : 거래처ZONE조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listCust(Map<String, Object> model) {
        return executeQueryPageWq("wmsms085.listZonePop", model);
    }

	public Object save(Map<String, Object> model) {
		executeUpdate("wmsms081.PK_WMSMS080.SP_SAVE_LOC_ZONE", model);
		return model;
	} 
}
