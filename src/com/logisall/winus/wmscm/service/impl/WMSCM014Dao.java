package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM014Dao")
public class WMSCM014Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
     * Method ID  : list
     * Method 설명  : 조직도 리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object list(Map<String, Object> model){
        return executeQueryForList("wmsms014.list", model);
    }
}
