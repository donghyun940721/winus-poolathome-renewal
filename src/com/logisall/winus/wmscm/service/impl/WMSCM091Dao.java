package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM091Dao")
public class WMSCM091Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
    /**
     * Method ID  : selectItemGrp
     * Method 설명  : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 상품조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms091.listItemPop", model);
    }   
    
    /**
     * Method ID  : listItemGrn
     * Method 설명  : 상품조회리스트 (크로스도킹용)
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listItemGrn(Map<String, Object> model) {
        return executeQueryPageWq("wmsms091.listItemGrn", model);
    }   
    
    /**
     * Method ID  : listLotno
     * Method 설명  : 상품조회리스트 LOTNO 조회조건 추가
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listLotno(Map<String, Object> model) {
        return executeQueryPageWq("wmsms091.listItemPopLotno", model);
    }  
    
    /**
     * Method ID  : listGrid
     * Method 설명  : 상품조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listGrid(Map<String, Object> model) {
        return executeQueryPageWq("wmsms091.listItemPopGrid", model);
    } 
    
    /**
     * Method ID  : listGrp
     * Method 설명  : 상품군조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listGrp(Map<String, Object> model) {
        return executeQueryPageWq("wmscm094.listGrpPop", model);
    }
}
