package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM080Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Service("WMSCM080Service")
public class WMSCM080ServiceImpl implements WMSCM080Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM080Dao")
    private WMSCM080Dao dao;
    
    /*-
	 * Method ID : mn
	 * Method 설명 : 로케이션 POP 메인
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCM080.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM080Q1");
	}
	
	 /*-
		 * Method ID : mn09
		 * Method 설명 : 로케이션 POP 메인
		 * 작성자 : 기드온
		 * @param model
		 * @return
		 * @throws Exception
		 */
		@RequestMapping("/WMSCM080Q9.action")
		public ModelAndView mn09(Map<String, Object> model) throws Exception {
			return new ModelAndView("winus/wmscm/WMSCM080Q9");
		}


    /**
     * Method ID    : list
     * Method 설명      : 로케이션 조회
     * 작성자                 : 기드온
     * @param   model
     * @return  
     * @throws Exception 
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            if(!model.get("vrViewOnlyLoc").equals("")){
                map.put("LIST", dao.listLoc(model));
            }else{
                map.put("LIST", dao.list(model));
            }
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID    : list02
     * Method 설명      : 로케이션 조회
     * 작성자                 : 기드온
     * @param   model
     * @return  
     * @throws Exception 
     */
     public Map<String, Object> list02(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.list02(model));

        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID    : list
     * Method 설명      : 로케이션 조회
     * 작성자                 : 기드온
     * @param   model
     * @return  
     * @throws Exception 
     */
    public Map<String, Object> listGroupByLot(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listGroupByLot(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }    
    
    /**
     * Method ID   : listClient
     * Method 설명 : 로케이션  팝업 자동조회
     * 작성자      : 기드온
     * @param 
     * @return
     */
    public Map<String, Object> listClient(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    
    

    /**
     * Method ID    : listGrn
     * Method 설명      : 로케이션 조회2
     * 작성자                 : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    public Map<String, Object> listGrn(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listGrn(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
}
