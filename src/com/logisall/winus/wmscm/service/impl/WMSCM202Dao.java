package com.logisall.winus.wmscm.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCM202Dao")
public class WMSCM202Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * Method ID : list
	 * Method 설명 : 아이템 리스트 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsms202.list", model);
	}
	
	   /**
     * Method ID : listStore
     * Method 설명 : 아이템 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     */
    public GenericResultSet listStore(Map<String, Object> model) {
        return executeQueryPageWq("wmsms202.listStore", model);
    }
	   
    
    /**
     * Method ID : lcAllListStore
     * Method 설명 : 아이템 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     */
    public GenericResultSet lcAllListStore(Map<String, Object> model) {
        return executeQueryPageWq("wmsms202.lcAllListStore", model);
    }
   
}
