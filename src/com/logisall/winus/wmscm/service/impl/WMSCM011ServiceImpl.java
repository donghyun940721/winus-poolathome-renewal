package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmscm.service.WMSCM011Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM011Service")
public class WMSCM011ServiceImpl implements WMSCM011Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM011Dao")
    private WMSCM011Dao dao;

    /**
     * Method ID : list
     * Method 설명 : 화주 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            if("LC_ALL".equals(model.get("S_LC_ALL"))){
            	map.put("LIST", dao.lcAllListStore(model));
            }else{
            	if(!"".equals(model.get("S_CUST_ID"))){
                    if("ALL".equals(model.get("S_LC_ALL"))){
                        map.put("LIST", dao.list(model));
                    }else{
                    	map.put("LIST", dao.listStore(model));
                    }
                }else{
                    map.put("LIST", dao.list(model));
                }
            }
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }

    
    /**
     * Method ID   : listClient
     * Method 설명 : 화주 리스트  팝업 자동조회
     * 작성자      : 기드온
     * @param 
     * @return
     */
    public Map<String, Object> listClient(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /*-
	 * Method ID   : getTypicalCust
	 * Method 설명 : 물류센터 별 대표화주값 조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> getTypicalCust(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("TYPICALCUST")) {
			map.put("TYPICALCUST", dao.getTypicalCust(model));
		}
		return map;
	}
    
    /*-
	 * Method ID   : getCustLcGb
	 * Method 설명 : 
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustLcGb(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("CUSTLC_GB")) {
			map.put("CUSTLC_GB", dao.getCustLcGb(model));
		}
		return map;
	}
    
    /*-
	 * Method ID   : getCustIdByLcId
	 * Method 설명 : LC_ID로 해당 코드의 화주ID 가져오기
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustIdByLcId(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("CUSTID_BY_LC")) {
			map.put("CUSTID_BY_LC", dao.getCustIdByLcId(model));
		}
		return map;
	}
    
    /*-
	 * Method ID   : getCustByRestApiKey
	 * Method 설명 : 화주별 RestApi 키
	 * 작성자      : 
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustByRestApiKey(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("RESTAPICUSTINFO")) {
			map.put("RESTAPICUSTINFO", dao.getCustByRestApiKey(model));
		}
		return map;
	}
    
    /**
     * Method ID : listLcSync
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listLcSync(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listLcSync(model));
             
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : listCyclCust
     * Method 설명 : 풀앳홈 재고조사 화주 리스트 
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listCyclCust(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		map.put("LIST", dao.listCyclCust(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }
    
    
    /*-
	 * Method ID	: getCustEtcCode
	 * Method 설명	: 화주별 기타마스터 코드 가져오기
	 * @param 
	 * @return
	 */
    public Map<String, Object> getCustEtcCode(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String vrCustCd		= (String) model.get("vrCustCd"); 		//화주코드
		String vrCodeTypeCd = (String) model.get("vrCodeTypeCd");	//기준관리 기준코드(유형코드)
		model.put("CODE_TYPE_CD", vrCodeTypeCd.concat(".").concat(vrCustCd));
		map.put("DS_ETC_CODE", dao.getCustEtcCode(model));
		return map;
	}
}
