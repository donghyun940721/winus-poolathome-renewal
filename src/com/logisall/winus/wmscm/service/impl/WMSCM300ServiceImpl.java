package com.logisall.winus.wmscm.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmscm.service.WMSCM300Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCM300Service")
public class WMSCM300ServiceImpl implements WMSCM300Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCM300Dao")
    private WMSCM300Dao dao;

    /**
     * Method ID : list
     * Method 설명 :그리드 컬럼 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            long templateCount = dao.templateCount(model);
            if(templateCount < 1){
            	
            	int lcIdTemplateCount = dao.checkLcIdTemplate(model);
            	
            	if(lcIdTemplateCount == 1){
                	dao.insertLcIdTemplate(model);
                	dao.insertLcIdColumns(model);

            	}else if(lcIdTemplateCount < 1){
            		dao.insertDefaultTemplate(model);
                	dao.insertDefaultColumns(model);
            	}
            	
            	
            }
            map.put("LIST", dao.list(model));
                        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }

    /**
     * Method ID : list
     * Method 설명 :그리드 컬럼 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> allList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		long templateCount = dao.templateCount(model);
    		if(templateCount < 1){
            	
            	int lcIdTemplateCount = dao.checkLcIdTemplate(model);
            	
            	if(lcIdTemplateCount == 1){
                	dao.insertLcIdTemplate(model);
                	dao.insertLcIdColumns(model);

            	}else if(lcIdTemplateCount < 1){
            		dao.insertDefaultTemplate(model);
                	dao.insertDefaultColumns(model);
            	}
            	
            	
            }
    		map.put("LIST", dao.allList(model));
    		
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    		
    	}
    	
    	return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 그리드 설정 저장
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("id", model.get("id" + i));
	             modelDt.put("templateId", model.get("templateId" + i));
	             modelDt.put("columnCode", model.get("columnCode" + i));
	             modelDt.put("columnName", model.get("columnName" + i));
	             modelDt.put("columnType", model.get("columnType" + i));
	             modelDt.put("columnSeq", model.get("columnSeq" + i));
	             modelDt.put("columnSize", model.get("columnSize" + i));
	             modelDt.put("columnColor", model.get("columnColor" + i));
	             modelDt.put("columnAlign", model.get("columnAlign" + i));
	             modelDt.put("columnReadOnly", model.get("columnReadOnly" + i));
	             modelDt.put("columnVisible", model.get("columnVisible" + i));
	             modelDt.put("columnAutoLineChange", model.get("columnAutoLineChange" + i));
	             
	             modelDt.put("vrSrchCustCd", model.get("vrSrchCustCd" + i));
	             modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("REG_NO", model.get(ConstantIF.SS_USER_NO));
	             
	             dao.save(modelDt);
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 :그리드 컬럼 기본 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> defaultList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	if(!model.containsKey("LC_ID")){
    		String lcId = (String)model.get("SS_SVC_NO");
    		model.put("LC_ID", lcId);
    	}
        try {
        	long defaultTemplateCount = dao.defaultTemplateCount(model);
    		if(defaultTemplateCount < 1){
            	
        		dao.createDefaultTemplate(model);
            	dao.createDefaultColumns(model);
            	map.put("LIST", dao.originList(model));
            	
            }
    		else{
    			map.put("LIST", dao.defaultList(model));
    		}
    		
        		        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 :그리드 컬럼 기본 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> originList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
    		map.put("LIST", dao.originList(model));
        		        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : fixedColumn
     * Method 설명 : 그리드 고정 컬럼 저장
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> fixColumnSave(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	            
        	model.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
        	model.put("REG_NO", model.get(ConstantIF.SS_USER_NO));
             
            dao.fixColumnSave(model);
	    	
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
    /**
     * Method ID : templateList
     * Method 설명 :그리드 템플릿 기본 리스트 조회
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> templateList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
        	map.put("LIST", dao.templateList(model));
                        
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : forceUpdate
     * Method 설명 : 사용자 강제 컬럼 변경 업데이트
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> forceUpdate(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("id", model.get("id" + i));
	             modelDt.put("columnCode", model.get("columnCode" + i));
	             modelDt.put("lcId", model.get("lcId"));
	             modelDt.put("vrTemplateType", model.get("vrTemplateType"));
	             modelDt.put("UPD_NO", model.get(ConstantIF.SS_USER_NO));
	             
	             dao.forceUpdate(modelDt);
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    	
    	
    }
    
    /**
     * Method ID : list
     * Method 설명 : 그리드 설정 저장
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> adminSave(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             
	             modelDt.put("id", model.get("id" + i));
	             modelDt.put("templateId", model.get("templateId" + i));
	             modelDt.put("columnCode", model.get("columnCode" + i));
	             modelDt.put("columnName", model.get("columnName" + i));
	             modelDt.put("columnType", model.get("columnType" + i));
	             modelDt.put("columnSeq", model.get("columnSeq" + i));
	             modelDt.put("columnSize", model.get("columnSize" + i));
	             modelDt.put("columnColor", model.get("columnColor" + i));
	             modelDt.put("columnAlign", model.get("columnAlign" + i));
	             modelDt.put("columnReadOnly", model.get("columnReadOnly" + i));
	             modelDt.put("columnVisible", model.get("columnVisible" + i));
	             modelDt.put("columnAutoLineChange", model.get("columnAutoLineChange" + i));
	             modelDt.put("columnCombo", model.get("columnCombo" + i));
	             modelDt.put("columnComboRelated", model.get("columnComboRelated" + i));
	             modelDt.put("columnFunction", model.get("columnFunction" + i));
	             
	             modelDt.put("vrSrchCustCd", model.get("vrSrchCustCd" + i));
	             modelDt.put("templateType", model.get("templateType"));
	             modelDt.put("LC_ID", model.get("lcId"));
	             modelDt.put("REG_NO", model.get(ConstantIF.SS_USER_NO));

	             String stGubun = (String)model.get("stGubun" + i);
	             if("S".equals(stGubun)){
	            	 dao.adminSave(modelDt);
	             }else if("I".equals(stGubun)){
	            	 dao.adminNewInsert(modelDt);
	             }
	             
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
    /**
     * Method ID : createNewTemplate
     * Method 설명 :신규템플릿 추가
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public  Map<String, Object> createNewTemplate(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        try {
        	dao.createNewTemplate(model);
            map.put("templateId", model.get("templateId"));            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    

}
