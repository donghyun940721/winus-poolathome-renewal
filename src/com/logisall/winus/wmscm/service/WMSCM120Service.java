package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM120Service {

    public Map<String, Object> list(Map<String, Object> model) throws Exception;
	
}
