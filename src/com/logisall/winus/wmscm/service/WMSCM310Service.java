package com.logisall.winus.wmscm.service;

import java.util.Map;

public interface WMSCM310Service {

	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> save(Map<String, Object> model) throws Exception;


}
