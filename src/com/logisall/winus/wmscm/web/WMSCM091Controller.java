package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM091Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM091Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM091Service")
	private WMSCM091Service service;

	/*-
	 * Method ID    : wmscm091
	 * Method 설명      : 상품조회 POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091.action")
	public ModelAndView wmscm091(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM091Q1", service.selectData(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 상품조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : wmscm092
	 * Method 설명      : 상품조회 POP 화면(멀티셀렉트)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091Q2.action")
	public ModelAndView wmscm091Q2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM091Q2", service.selectData(model));
	}

	/*-
	 * Method ID    : wmscm091Q5
	 * Method 설명      : 상품조회 POP 화면(멀티셀렉트, LOT번호 조회조건 추가)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091Q5.action")
	public ModelAndView wmscm091Q5(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM091Q5", service.selectData(model));
	}

	/*-
	 * Method ID    : wmscm091Q6
	 * Method 설명      : 상품조회 POP 화면(멀티셀렉트, LOT번호 조회조건 추가)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091Q6.action")
	public ModelAndView wmscm091Q6(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM091Q6", service.selectData(model));
	}

	/*-
	 * Method ID    : wmscm091
	 * Method 설명      : 상품조회 POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091Q7.action")
	public ModelAndView wmscm091q7(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM091Q7");
	}
	
	/*-
	 * Method ID    : listLotno
	 * Method 설명      : 상품조회 LOTNO 조건추가
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091/listLotno.action")
	public ModelAndView listLotno(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listLotno(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : wmscm091Q4
	 * Method 설명      : 상품조회 POP 화면(크로스도킹 상품)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091Q4.action")
	public ModelAndView wmscm091Q4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/wmscm091Q4", service.selectData(model));
	}

	/*-
	 * Method ID    : listItemGrn
	 * Method 설명      : 상품조회  (크로스도킹 상품)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091Q6/list.action")
	public ModelAndView listItemGrn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listItemGrn(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listGrid
	 * Method 설명      : Grid 상품조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM091/listGrid.action")
	public ModelAndView listGrid(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listGrid(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listGrp
	 * Method 설명      : 상품군조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM094/listGrp.action")
	public ModelAndView listGrp(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listGrp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
}
