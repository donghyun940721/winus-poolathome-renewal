package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM080Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM080Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM080Service")
	private WMSCM080Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 로케이션 POP 메인
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCM080.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM080Q1");
	}
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 로케이션 POP 메인
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCM080Q9.action")
	public ModelAndView mn09(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM080Q9");
	}

	/*-
	 * Method ID : mn02
	 * Method 설명 : 로케이션 POP 메인
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCM080Q5.action")
	public ModelAndView mn02(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM080Q5");
	}
		
	/*-
	 * Method ID    : list
	 * Method 설명      : 로케이션 조회
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */

	@RequestMapping("/WMSCM080/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 로케이션 조회
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */

	@RequestMapping("/WMSCM080Q5/list02.action")
	public ModelAndView list02(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list02(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 로케이션 조회
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */

	@RequestMapping("/WMSCM080/listGroupByLot.action")
	public ModelAndView listGroupByLot(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listGroupByLot(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : listClient
	 * Method 설명 : 로케이션  팝업 자동조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM080/listLoc.action")
	public ModelAndView listLoc(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		m = service.list(model);
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : listClient
	 * Method 설명 : 로케이션  팝업 자동조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCM080/listClient.action")
	public ModelAndView listClient(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		m = service.listClient(model);
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmscm080Q2
	 * Method 설명      : 로케이션팝업2
	 * 작성자                 : chSong
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCM080Q2.action")
	public ModelAndView wmscm080Q2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM080Q2");
	}

	/*-
	 * Method ID    : listGrn
	 * Method 설명      : 로케이션팝업2
	 * 작성자                 : chSong
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCM080Q2/listGrn.action")
	public ModelAndView listGrn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listGrn(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : wmscm080Q4
	 * Method 설명      : 로케이션팝업2
	 * 작성자                 : chSong
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCM080Q4.action")
	public ModelAndView wmscm080Q4(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM080Q4");
	}
	
	/*-
	 * Method ID    : wmscm080Q10
	 * Method 설명      : 로케이션팝업10 (재고관리> 명의변경 > 신규 > 로케이션팝업)
	 * 작성자                 : KCR
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCM080Q10.action")
	public ModelAndView WMSCM080Q10(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM080Q10");
	}
}
