package com.logisall.winus.wmscm.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM210Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM210Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM210Service")
	private WMSCM210Service service;

	/*-
	 * Method ID    : getTp
	 * Method 설명      : getTp 템플릿정보??
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSCM210/getTp.action")
	public ModelAndView getTp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.getTp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get template :", e);
			}
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : getTpList
	 * Method 설명      : getTpList 템플릿정보??
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSCM210/getTpList.action")
	public ModelAndView getTpList(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.getTpList(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get template :", e);
			}
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : getTpListAll
	 * Method 설명      : getTpListAll
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSCM210/getTpListALL.action")
	public ModelAndView getTpListAll(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.getTpListALL(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get template :", e);
			}
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : search
	 * Method 설명      : 템플릿정보에 따른 컬럼
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSCM210/search.action")
	public ModelAndView search(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.getResult(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search result :", e);
			}
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : getTpGrid
	 * Method 설명      : getTp 템플릿정보??
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSCM210/getTpGrid.action")
	public ModelAndView getTpGrid(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.getTpGrid(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get template :", e);
			}
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : searchGrid
	 * Method 설명      : 템플릿정보에 따른 컬럼
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSCM210/searchGrid.action")
	public ModelAndView searchGrid(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.getResultGrid(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search result :", e);
			}
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
