package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsms.service.WMSMS094Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM150Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSMS094Service")
	private WMSMS094Service service;

	/*-
	 * Method ID    : wmscm050
	 * Method 설명      : 상품군 POP화면
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM150.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM150Q1", service.selectData(model));
	}

}
