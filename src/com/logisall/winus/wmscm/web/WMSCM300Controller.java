package com.logisall.winus.wmscm.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM300Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM300Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM300Service")
	private WMSCM300Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 그리드 설정 매인화면
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception  {
		return new ModelAndView("winus/wmscm/WMSCM300Q1");
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 그리드 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : alllist
	 * Method 설명 : 그리드전체 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/allList.action")
	public ModelAndView allList(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			//mav = new ModelAndView("jqGridJsonView", service.allList(model));
			mav = new ModelAndView("jqGridJsonView", service.defaultList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 그리드 기본 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/defaultList.action")
	public ModelAndView defaultList(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.defaultList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 그리드 기본 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/originList.action")
	public ModelAndView originList(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.originList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 그리드 수정 저장
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : adminSave
	 * Method 설명 : 그리드 수정 저장
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/adminSave.action")
	public ModelAndView adminSave(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.adminSave(model);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : fixColumnSave
	 * Method 설명 : 그리드 컬럼 고정
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/fixColumnSave.action")
	public ModelAndView fixColumnSave(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.fixColumnSave(model);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : templateList
	 * Method 설명 : 템플릿 종류 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/templateList.action")
	public ModelAndView templateList(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.templateList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : templateList
	 * Method 설명 : 템플릿 종류 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/forceUpdate.action")
	public ModelAndView forceUpdate(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.forceUpdate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : createNewTemplate
	 * Method 설명 : 템플릿 추가
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM300/createNewTemplate.action")
	public ModelAndView createNewTemplate(Map<String, Object> model) throws Exception  {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.createNewTemplate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}
