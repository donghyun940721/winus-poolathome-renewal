package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM200Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM200Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM200Service")
	private WMSCM200Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 파트너 조회 메인화면
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM200.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM200Q1");
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 파트너 리스트 조회
	 * 작성자 : 김기하
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCM200/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

}
