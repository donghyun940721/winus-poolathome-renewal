package com.logisall.winus.wmscm.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmscm.service.WMSCM051Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCM051Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCM051Service")
	private WMSCM051Service service;

	/*-
	 * Method ID    : wmscm051
	 * Method 설명      : 차량 POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM051.action")
	public ModelAndView wmscm051(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmscm/WMSCM051Q1");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 배송기사조회 
	 * 작성자                 : KHKIM
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCM051/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
}
