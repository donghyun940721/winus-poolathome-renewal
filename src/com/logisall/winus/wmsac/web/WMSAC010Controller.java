package com.logisall.winus.wmsac.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsac.service.WMSAC010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSAC010Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC010Service")
	private WMSAC010Service service;

	/*-
	 * Method ID    : wmsac010
	 * Method 설명      : 정산단가계약 화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSAC010.action")
	public ModelAndView wmsac020(Map<String, Object> model) {
		return new ModelAndView("winus/wmsac/WMSAC010");
	}

	/*-
	 * Method ID    : wmscm091
	 * Method 설명      : 상품조회 POP 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC010E4.action")
	public ModelAndView wmscm091(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC010E4", service.selectData(model));
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 상품조회 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC010E4/listE4.action")
	public ModelAndView listE4(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE4(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 정산산출 조회
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : wmsac010e2
	 * Method 설명      : 청구단가계약 팝업화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010E2.action")
	public ModelAndView wmsac010e2(Map<String, Object> model) {
		return new ModelAndView("winus/wmsac/WMSAC010E2");
	}

	/*-
	 * Method ID    : listSubDetail
	 * Method 설명      : 청구단가계약 상단조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC010E2/listSubDetail.action")
	public ModelAndView listSubDetail(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.listSubDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List sub detail :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listSub
	 * Method 설명      : 청구단가계약 그리드 조회
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010E2/listSub.action")
	public ModelAndView listSub(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List(sub) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveSub
	 * Method 설명      : 청구단가계약 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010/saveSub.action")
	public ModelAndView saveSub(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSub(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save(sub) :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : wmsac010e3
	 * Method 설명      : 거래처정산일설정 팝업화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010E3.action")
	public ModelAndView wmsac010e3(Map<String, Object> model) {
		return new ModelAndView("winus/wmsac/WMSAC010E3");
	}

	/*-
	 * Method ID    : listAccDt
	 * Method 설명      : 거래처정산일설정 조회
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010E3/listAccDt.action")
	public ModelAndView listAccDt(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listAccDt(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List(Acc) :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveAccdt
	 * Method 설명      : 거래처정산일 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC010/saveAccdt.action")
	public ModelAndView saveAccdt(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveAccdt(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save acc dt :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
