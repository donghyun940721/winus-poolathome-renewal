package com.logisall.winus.wmsac.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC202Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSAC202Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC202Service")
	private WMSAC202Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 일별재고 화면
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */	
	@RequestMapping("/WINUS/WMSAC202.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsac/WMSAC202", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 인건비내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC202/list0.action")
	public ModelAndView list0(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list0(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	

	/*-
	 * Method ID : listPool
	 * Method 설명 : 부자재내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC202/listPool.action")
	public ModelAndView listPool(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listPool(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : manage
	 * Method 설명 : 관리비내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC202/manage.action")
	public ModelAndView manage(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.manage(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : store
	 * Method 설명 : 입출고내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC202/store.action")
	public ModelAndView store(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.store(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}   
    
    /*-
     * Method ID   : save
     * Method 설명       : 부자재 내역 신규 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC202/save3.action")
    public ModelAndView save3(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save3(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }   
    
    /*-
     * Method ID   : chkCnt
     * Method 설명       : 보관비 개수 체크
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC202/chkCnt.action")
    public ModelAndView chkCnt(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.chkCnt(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : chkDataCnt
     * Method 설명       : 거래명세서 각 메뉴별 데이터 체크
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC202/chkDataCnt.action")
    public ModelAndView chkDataCnt(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.chkDataCnt(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }           
    
    /*-
	 * Method ID    : list15
	 * Method 설명      : 반품내역  목록
	 * 작성자                 : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC202/list15.action")
	public ModelAndView list15(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list15(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save15
	 * Method 설명      : 반품내역  저장
	 * 작성자                 : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC202/save15.action")
    public ModelAndView save15(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save15(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
	
	/*-
	 * Method ID    : list17
	 * Method 설명      : 배송내역  목록
	 * 작성자                 : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC202/list17.action")
	public ModelAndView list17(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list17(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save17
	 * Method 설명      : 배송내역  저장
	 * 작성자                 : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC202/save17.action")
    public ModelAndView save17(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save17(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
	
	/*-
     * Method ID   : save16
     * Method 설명       : 임가공 저장(이관)
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC202/save16.action")
    public ModelAndView save16(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save16(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : save0
     * Method 설명       : 보관비 저장(이관)
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC202/save0.action")
    public ModelAndView save0(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save0(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
	 * Method ID : add
	 * Method 설명 : 부가서비스내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC202/add.action")
	public ModelAndView add(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.add(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID   : save10
     * Method 설명       : 부가서비스 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC202/save10.action")
    public ModelAndView save7(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save10(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : saveReport
     * Method 설명       : 레포트 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC202/saveReport.action")
    public ModelAndView saveReport(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.saveReport(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    } 
    
}