package com.logisall.winus.wmsac.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC203Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSAC203Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC203Service")
	private WMSAC203Service service;

	static final String[] COLUMN_NAME_WMSAC203E2T1 = {
		 "O_DPRT_DT"
		,"O_ARRV_DT"
		,"O_ACC_TYPE"
		,"O_COST_VOLUMN"
		,"O_COST_CONTRACT"
		,"O_ACC_UNIT"
		,"O_DLV_DEPARTURE_A"
		,"O_DLV_DESTINATION_A"
		,"O_DLV_DEPARTURE_B"
		,"O_DLV_DESTINATION_B"
		,"O_CAR_NUM"
		,"O_RTI_CNT"
		,"O_CAR_TON"
		,"O_UNIT_COST"
		,"O_QTY"
		,"O_ADDITIONAL_COST"
		,"O_ETC"
	};
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 일별재고 화면
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */	
	@RequestMapping("/WINUS/WMSAC203.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsac/WMSAC203", service.selectBox(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 인건비내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC203/list0.action")
	public ModelAndView list0(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list0(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID : list
	 * Method 설명 : 인건비내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC203/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : detail
	 * Method 설명 : 운송비내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC203/detail.action")
	public ModelAndView detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : store
	 * Method 설명 : 입출고내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC203/store.action")
	public ModelAndView store(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.store(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID   : save
     * Method 설명       : 인건비 신규 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/save.action")
    public ModelAndView save(Map<String, Object> model){
        ModelAndView mav = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();        
        
        try{
            m = service.save(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : save
     * Method 설명       : 운송비 신규 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/save2.action")
    public ModelAndView save2(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save2(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : save
     * Method 설명       : 부자재 내역 신규 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/save3.action")
    public ModelAndView save3(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save3(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }    
    
    /*-
     * Method ID   : save7
     * Method 설명       : 관리비내역 , 입출고비내역, 사용내역비 저장(공통)
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/save7.action")
    public ModelAndView save7(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save7(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }    
    
    /*-
     * Method ID   : save0
     * Method 설명       : 보관비 저장(이관)
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/save0.action")
    public ModelAndView save0(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save0(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : chkCnt
     * Method 설명       : 보관비 개수 체크
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/chkCnt.action")
    public ModelAndView chkCnt(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.chkCnt(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : chkDataCnt
     * Method 설명       : 거래명세서 각 메뉴별 데이터 체크
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/chkDataCnt.action")
    public ModelAndView chkDataCnt(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.chkDataCnt(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : save0
     * Method 설명       : 보관비 저장(이관)
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/update12.action")
    public ModelAndView update12(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{        	
            m = service.update12(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
	 * Method ID    : excelDown0
	 * Method 설명      : 보관비내역서 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC203/excelDown0.action")
	public void listExcel0(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.list0Excel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown0(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown0(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("구분")    	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("입고")   	, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("출고")    	, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("현재고")		, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("입출고수량")	, "4", "4", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("보관단가")   , "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("보관금액")   , "6", "6", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"SUBUL_DT" 	,"S"},
				            		{"IN_QTY" 		,"S"},
				            		{"OUT_QTY" 		,"S"},
				            		{"STOCK_QTY" 	,"S"},
				            		{"INOUT_QTY" 	,"S"},
				            		
				            		{"COST" 		,"S"},
				            		{"CAL_COST" 	,"S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("보관비내역서");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}	
	
	/*-
	 * Method ID    : WMSAC203E12
	 * Method 설명          : 입출고 수량 팝업 수정 화면 
	 * 작성자                          : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC203E12.action")
	public ModelAndView WMSAC203E12(Map<String, Object> model) {
		return new ModelAndView("winus/WMSAC/WMSAC203E12");
	}
	
	/*-
	 * Method ID    : WMSAC203E17
	 * Method 설명          : 사진추가 팝업 화면 
	 * 작성자                          : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC203E17.action")
	public ModelAndView WMSAC203E17(Map<String, Object> model) {
		return new ModelAndView("winus/WMSAC/WMSAC203E17");
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : cj 운송  목록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC203/list14.action")
	public ModelAndView list14(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list14(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID   : save14
     * Method 설명       : cj택배 저장(이관)
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/save14.action")
    public ModelAndView save14(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save14(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : save14
     * Method 설명       : cj택배 수정(도선료, 제주운임, 기타운임)
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/update14.action")
    public ModelAndView update14(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.update14(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
	 * Method ID    : list15
	 * Method 설명      : 반품내역  목록
	 * 작성자                 : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC203/list15.action")
	public ModelAndView list15(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list15(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save15
	 * Method 설명      : 반품내역  저장
	 * 작성자                 : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC203/save15.action")
    public ModelAndView save15(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save15(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
	
	/*-
	 * Method ID    : list16
	 * Method 설명      : 합포장내역  목록
	 * 작성자                 : hkmin
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC203/list16.action")
	public ModelAndView list16(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list16(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID   : save16
     * Method 설명       : 합포장내역 저장(이관)
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/save16.action")
    public ModelAndView save16(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save16(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
	 * Method ID : saveImg
	 * Method 설명 : 이미지 파일 업로드
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC203/updateFileInfo.action")
	public ModelAndView updateFileInfo(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.updateFileInfo(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
     * Method ID   : saveReport
     * Method 설명       : 레포트 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/saveReport.action")
    public ModelAndView saveReport(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.saveReport(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : saveTaxInfo
     * Method 설명       : 세금계산서 호출 정보 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/saveTaxInfo.action")
    public ModelAndView saveTaxInfo(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.saveTaxInfo(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : selectTaxInfo
     * Method 설명       : 세금계산서 호출 정보 조회
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/selectTaxInfo.action")
    public ModelAndView selectTaxInfo(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.selectTaxInfo(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }   
    
    
    /*-
     * Method ID   : chkReportCnt
     * Method 설명       : 레포트 저장 유무 체크
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/chkReportCnt.action")
    public ModelAndView chkReportCnt(Map<String, Object> model){
    	ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.chkReportCnt(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }   
    
    /*-
	 * Method ID    : excelInsertE2
	 * Method 설명      : 운송비내역서 엑셀입력
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC203E2/WMSAC203E2T1.action")
	public ModelAndView WMSAC203E2T1(Map<String, Object> model) {
		return new ModelAndView("winus/WMSAC/WMSAC203E2T1");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC203E2T1/uploadExcelInfo.action")
	public ModelAndView uploadExcelInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSAC203E2T1 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSAC203E2T1, 0, startRow, 10000, 0);
			m = service.saveExcelInfoE2T1(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
     * Method ID   : selectIssueEmp
     * Method 설명       : 발행자사번 조회
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/selectIssueEmp.action")	
	public ModelAndView selectIssueEmp(Map<String, Object> model){
    	ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.selectIssueEmp(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    
    /*-
     * Method ID   : updatetaxInfo
     * Method 설명       : 세금계산서 발행 이력 정보 업데이트 (성공:RTN_COD:"00")
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/updateTaxInfo.action")
    public ModelAndView updateTaxInfo(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{        	
            m = service.updateTaxInfo(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
     * Method ID   : chkTaxCnt
     * Method 설명       : 레포트 저장 유무 체크
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/chkTaxCnt.action")
    public ModelAndView chkTaxCnt(Map<String, Object> model){
    	ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.chkTaxCnt(model);                        
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
    
    /*-
	 * Method ID : store
	 * Method 설명 : 입출고내역 조회
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC203/list18.action")
	public ModelAndView list18(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list18(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}
	
	/*-
     * Method ID   : save18
     * Method 설명       : 보관비내역(수기) 저장
     * 작성자                       : 민환기
     * @param   model
     * @return  
     */
    @RequestMapping("/WMSAC203/save18.action")
    public ModelAndView save18(Map<String, Object> model){
        ModelAndView mav 	   = new ModelAndView("jsonView");
        Map<String, Object> m  = new HashMap<String, Object>();       
        
        try{
            m = service.save18(model);
        }catch(Exception e){
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
            m.put("MSG", MessageResolver.getMessage("save.error")); 
        }
        mav.addAllObjects(m);
        return mav;
    }
}