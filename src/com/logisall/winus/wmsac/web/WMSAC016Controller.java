package com.logisall.winus.wmsac.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC016Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSAC016Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC016Service")
	private WMSAC016Service service;
	
	static final String[] COLUMN_NAME_WMSAC016 = {
		"APPLY_FR_DT", "APPLY_TO_DT","ACC_STD", "SALE_BUY_GBN", "PAY_DAY",				//5 
		"RITEM_CD", "ITEM_GRP_CD", "UNIT_PRICE", "INOUT_COST", "STOCK_COST",			//10
		"CURRENCY_NAME", "TRANS_CUST_ID", "PAY_CD", "ROUND_CD", "END_CUT_CD",			//15
		"CLOSING_DAY", "CONT_DEPT_ID", "CONT_EMPLOYEE_ID", "CONF_YN", "REAL_DEPT_ID", 	//20
		"REAL_EMPLOYEE_ID", "COLLECT_EMPLOYEE_ID", "APPROVAL_EMP_ID" ,"GROUP_COM_EMP_ID"//24
	};
	
	static final String[] COLUMN_NAME_WMSAC016_PAH = {
		"APPLY_FR_DT", "APPLY_TO_DT", "SALE_BUY_GBN", "RITEM_CD", "ITEM_GRP_CD", 
		"UNIT_PRICE", "CURRENCY_NAME", "PAY_CD", "CONF_YN", "SET_LC_ID",	
		"PER_RTN_RCV", "PER_CNG_SHP", "PER_CNG_RCV", "PER_TNS_RCV", "PER_TNS_SET",
		"PER_SIT_CAN", "PER_FST_BAD", "PER_SRT_CAN", "PER_FUNK"
	};
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 청구단가계약정보관리
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSAC016.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsac/WMSAC016", service.selectBox(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : itemAcList
	 * Method 설명 : 상품 정산 리스트
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/itemAcList.action")
	public ModelAndView itemAcList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.itemAcList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	/*-
	 * Method ID : acMstrList
	 * Method 설명 : 정산 마스터 리스트
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/acMstrList.action")
	public ModelAndView acMstrList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.acMstrList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	/*
	 * Method ID : save
	 * Method 설명 : 정산 마스터 등록/수정 저장
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*
	 * Method ID : savePAH
	 * Method 설명 : 정산 마스터 등록/수정 저장 - 풀앳홈 간편 용도
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/savePAH.action")
	public ModelAndView savePAH(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.savePAH(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*
	 * Method ID : delete
	 * Method 설명 : 정산 마스터 삭제
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/delete.action")
	public ModelAndView delete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	/*
	 * Method ID : itemAcSave
	 * Method 설명 : 정산 마스터 상품 일괄 적용
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/itemAcSave.action")
	public ModelAndView itemAcSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.itemAcSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	
	/*
	 * Method ID : itemAcDelete
	 * Method 설명 : 정산 마스터 상품 일괄 삭제
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/itemAcDelete.action")
	public ModelAndView itemAcDelete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.itemAcDelete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : listExcel1
	 * Method 설명 : 택배발급이력 엑셀다운
	 * 작성자 : yhku
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSAC016/excel.action")
	public void listExcel1(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : yhku
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            					   {MessageResolver.getMessage("작업일자"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화주LOT번호"), "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품코드"), "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품명"), "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("재고수량"), "5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("이동중수량"), "6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("제품상태"), "7", "7", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("로케이션"), "8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션 유형"), "9", "9", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("UOM"), "10", "10", "0", "0", "200"},                                   
                                   {MessageResolver.getMessage("재고중량"), "11", "11", "0", "0", "200"}                           
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"SUBUL_DT"         , "S"},
                                    {"CUST_NM"          , "S"},
                                    {"CUST_LOT_NO"      , "S"},
                                    {"RITEM_CD"         , "S"},
                                    {"RITEM_NM"         , "S"},
                                    
                                    {"STOCK_QTY"        , "N"},
                                    {"NOW_MOVING_QTY"   , "N"},
                                    {"ITEM_STAT"        , "S"},
                                    
                                    {"LOC_CD"        	, "S"},
                                    {"LOC_TYPE"         , "S"},                                    

                                    {"UOM_NM"           , "S"},                                    
                                    {"STOCK_WEIGHT"     , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("택배발급이력_상세");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
    
    /*-
	 * Method ID    : WMSAC010Q1
	 * Method 설명      : 청구단가 엑셀 입력 팝업
	 * 작성자                 : summer H
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC016Q1.action")
	public ModelAndView wmsac016q1(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC016Q1");
	}
    
	/*-
	 * Method ID  : ExcelUpload
	 * Method 설명  : 청구 단가 Excel 입력
	 * 작성자             : summer hyun
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/excelUpload.action")
	public ModelAndView excelUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSAC016, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
			mapBody.put("chkDuplicationYn"			,model.get("chkDuplicationYn"));
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			m = service.saveExcelUpload(mapBody, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
			//mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : ExcelUploadPAH
	 * Method 설명  : 청구 단가 Excel 입력 풀앳홈 용
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC016/excelUploadPAH.action")
	public ModelAndView excelUploadPAH(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSAC016_PAH, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
			mapBody.put("chkDuplicationYn"			,model.get("chkDuplicationYn"));
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			m = service.saveExcelUploadPAH(mapBody, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
			//mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
}