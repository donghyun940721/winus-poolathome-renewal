package com.logisall.winus.wmsac.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC020Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSAC020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC020Service")
	private WMSAC020Service service;

	/*-
	 * Method ID    : wmsac020
	 * Method 설명      : 정산산출화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSAC020.action")
	public ModelAndView wmsac020(Map<String, Object> model) {
		return new ModelAndView("winus/wmsac/WMSAC020");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 정산산출 조회
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC020/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	* Method ID    : save
	* Method 설명      : 정산산출 저장
	* 작성자                 : chSong
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSAC020/save.action")
	public ModelAndView sum(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jsonView", service.save(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
		}
		return mav;
	}
    
    /*-
     * Method ID    : listExcel
     * Method 설명      : 정산산출 엑셀
     * 작성자                 : chsong
     * @param   model
     * @return  
     */
     @RequestMapping("/WMSAC020/excel.action")
     public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
         Map<String, Object> map = new HashMap<String, Object>();
         try {
             map = service.listExcel(model);
             GenericResultSet grs = (GenericResultSet)map.get("LIST");
             if(grs.getTotCnt() > 0){
                 this.doExcelDown(response, grs);
             }
         }catch (Exception e) {
 			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
         }
     }
     
     protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
         try{
             //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
             //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
             String[][] headerEx = {
                                     {MessageResolver.getMessage("적용시작일"), "0", "0", "0", "0", "100"}
                                    ,{MessageResolver.getMessage("적용종료일"), "1", "1", "0", "0", "100"}
                                    ,{MessageResolver.getMessage("화주")     , "2", "2", "0", "0", "100"}
                                    ,{MessageResolver.getMessage("입고료")   , "3", "3", "0", "0", "100"}
                                    ,{MessageResolver.getMessage("출고료")   , "4", "4", "0", "0", "100"}
                                    ,{MessageResolver.getMessage("보관료")   , "5", "5", "0", "0", "100"}
                                    ,{MessageResolver.getMessage("운송료")   , "6", "6", "0", "0", "100"}
                                    ,{MessageResolver.getMessage("셔틀료")   , "7", "7", "0", "0", "100"}
                                    ,{MessageResolver.getMessage("합계")     , "8", "8", "0", "0", "100"}
                                   };
             //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
             
             String[][] valueName = {
                                      {"APPLY_FR_DT"        , "S"}
                                     ,{"APPLY_TO_DT"        , "S"}
                                     ,{"CUST_NM"            , "S"}
                                     ,{"ADJUST_IN_AMT"      , "N"}
                                     ,{"ADJUST_OUT_AMT"     , "N"}
                                     ,{"ADJUST_STORE_AMT"   , "N"}
                                     ,{"ADJUST_TRANS_AMT"   , "N"}
                                     ,{"ADJUST_SHUTTLE_AMT" , "N"}
                                     ,{"ADJUST_TOT_AMT"     , "N"}
                                    }; 
             
             //파일명
             String fileName =MessageResolver.getMessage("정산산출");
             //시트명
             String sheetName = "Sheet1";
             //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
             String marCk = "N";
             //ComUtil코드
             String etc = "";
             ExcelWriter wr = new ExcelWriter();
             wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
             
         } catch(Exception e) {
         	if (log.isErrorEnabled()) {
        		log.error("fail download excel file...", e);
        	}
         }
     }
    
	/*-
	 * Method ID    : wmsac020e2
	 * Method 설명      : 정산산출상세 화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC020E2.action")
	public ModelAndView wmsac020e2(Map<String, Object> model) {
		return new ModelAndView("winus/wmsac/WMSAC020E2");
	}

	/*-
	 * Method ID    : listSub
	 * Method 설명      : 정산산출 조회
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC020E2/listSub.action")
	public ModelAndView listSub(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list sub :", e);
			}
		}
		return mav;
	}
}
