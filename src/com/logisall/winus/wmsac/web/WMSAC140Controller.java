package com.logisall.winus.wmsac.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC140Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSAC140Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC140Service")
	private WMSAC140Service service;

	static final String[] COLUMN_NAME_WMSAC140E2 = {
	  	  "SALES_CUST_ID", "SALES_CUST_NM"   , "ORG_ORD_ID"   , "BUYED_DT" , "PRODUCT_CD"
		, "PRODUCT_NM"   , "SALES_COMPANY_NM", "DLV_ORD_STAT" , "WORK_STAT", "ADJUCT_CAL_DT"
		, "SOURCE_PRICE" , "SALES_PRICE"     , "ADJUCT_FEE_DT", "SERVICE_CHARGE"
	};
	/*-
	 * Method ID    : WMSAC140
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSAC140.action")
	public ModelAndView WMSAC1400(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC140");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC140/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 고객관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC140/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC140/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("KEY")    		, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("수취인")    		, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("고객주문번호")   	, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("주문일")   		, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("상품코드")    	, "4", "4", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("상품명")    		, "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("판매처")    		, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("배송상태") 		, "7", "7", "0", "0", "100"},
				            		{MessageResolver.getText("등록구분")   	, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("산출일자")   	, "9", "9", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("원가")    		, "10", "10", "0", "0", "100"},
				            		{MessageResolver.getText("판매가")    		, "11", "11", "0", "0", "100"},
				            		{MessageResolver.getText("수수료정산일자")   , "12", "12", "0", "0", "100"},
				            		{MessageResolver.getText("수수료")    		, "13", "13", "0", "0", "100"}
				            		/*
				            		{MessageResolver.getText("수취인")    		, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("주소")    		, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("수취인연락처1")   , "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("수취인연락처2")   , "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("주문일")    		, "4", "4", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("상품코드")    	, "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("상품명")    		, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("판매처")    		, "7", "7", "0", "0", "100"},
				            		{MessageResolver.getText("주문자")    		, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("주문자연락처1")   	, "9", "9", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("주문자연락처2")   , "10", "10", "0", "0", "100"},
				            		{MessageResolver.getText("배송일")   		, "11", "11", "0", "0", "100"},
				            		{MessageResolver.getText("배송상태") 		, "12", "12", "0", "0", "100"},
				            		{MessageResolver.getText("등록구분")   	, "13", "13", "0", "0", "100"},
				            		{MessageResolver.getText("산출일자")   	, "14", "14", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("원가")    		, "15", "15", "0", "0", "100"},
				            		{MessageResolver.getText("판매가")    		, "16", "16", "0", "0", "100"},
				            		{MessageResolver.getText("수수료정산일자")   , "17", "17", "0", "0", "100"},
				            		{MessageResolver.getText("수수료")    		, "18", "18", "0", "0", "100"}
				            		*/
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"SALES_CUST_ID"	, "S"},
				            		{"SALES_CUST_NM"	, "S"},
				            		{"ORG_ORD_ID"		, "S"},
				            		{"BUYED_DT"			, "S"},
				            		{"PRODUCT_CD"		, "S"},
				            		
				            		{"PRODUCT_NM"		, "S"},
				            		{"SALES_COMPANY_NM"	, "S"},
				            		{"DLV_ORD_STAT"		, "S"},
				            		{"WORK_STAT"		, "S"},
				            		{"ADJUCT_CAL_DT"	, "S"},
				            		
				            		{"SOURCE_PRICE"		, "S"},
				            		{"SALES_PRICE"		, "S"},
				            		{"ADJUCT_FEE_DT"	, "S"},
				            		{"SERVICE_CHARGE"	, "S"}
				            		/*
				            		{"SALES_CUST_NM"	, "S"},
				            		{"ADDR"				, "S"},
				            		{"PHONE_1"			, "S"},
				            		{"PHONE_2"			, "S"},
				            		{"BUYED_DT"			, "S"},
				            		
				            		{"PRODUCT_CD"		, "S"},
				            		{"PRODUCT_NM"		, "S"},
				            		{"SALES_COMPANY_NM"	, "S"},
				            		{"BUY_CUST_NM"		, "S"},
				            		{"BUY_PHONE_1"		, "S"},
				            		
				            		{"BUY_PHONE_2"		, "S"},
				            		{"DLV_DT"			, "S"},
				            		{"DLV_ORD_STAT"		, "S"},
				            		{"WORK_STAT"		, "S"},
				            		{"ADJUCT_CAL_DT"	, "S"},
				            		
				            		{"SOURCE_PRICE"		, "S"},
				            		{"SALES_PRICE"		, "S"},
				            		{"ADJUCT_FEE_DT"	, "S"},
				            		{"SERVICE_CHARGE"	, "S"}
				            		*/
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("청구작업");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : wmsac140e2
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSAC140E2.action")
	public ModelAndView WMSAC140E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC140E2");
	}
	
	/*-
	 * Method ID : uploadInfo
	 * Method 설명 : 엑셀파일업로드
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param txtFile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSAC140E2/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSAC140E2, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
}
