package com.logisall.winus.wmsac.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC141Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSAC141Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC141Service")
	private WMSAC141Service service;

	/*-
	 * Method ID    : WMSAC141
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSAC141.action")
	public ModelAndView WMSAC1410(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC141");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC141/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 고객관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC141/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : reportSave
	 * Method 설명      : 고객관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC141/reportSave.action")
	public ModelAndView reportSave(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.reportSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC141/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("판매처")    		, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("배송상태") 		, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("등록구분")   	, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("산출일자")   	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("원가합계")    	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("판매가합계")    	, "5", "5", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"SALES_COMPANY_NM"	, "S"},
				            		{"DLV_ORD_STAT"		, "S"},
				            		{"WORK_STAT"		, "S"},
				            		{"ADJUCT_CAL_DT"	, "S"},
				            		{"SOURCE_PRICE"		, "S"},
				            		{"SALES_PRICE"		, "S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("청구산출");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : WMSAC141T1
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC141T1.action")
	public ModelAndView WMSAC141T1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC141T1");
	}
	
	/*-
	 * Method ID    : reportList
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC141T1/reportList.action")
	public ModelAndView reportList(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.reportList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC141T1/excel.action")
	public void listExcelT1(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcelT1(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownT1(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDownT1
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDownT1(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
				            		{MessageResolver.getText("판매처")    		, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("상품명")    		, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("상품코드")    	, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("판매단가") 		, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("판매수량")   	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("판매가합계")   	, "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("원가합계")    	, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("원가단가")    	, "7", "7", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
				            		{"CUST_CD"		, "S"},        		
				            		{"ITEM_CODE"	, "S"},
				            		{"ITEM_CODE2"	, "S"},
				            		{"SALES_PRICE"	, "S"},
				            		{"QTY"			, "S"},
				            		{"DLV_AMT"		, "S"},
				            		{"UNIT_AMT"		, "S"},
				            		{"UNIT_PRICE"	, "S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("거래명세서산출");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID   : getCustInfo
	 * Method 설명 : 
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSAC141T1/getCustInfo.action")
	public ModelAndView getCustInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getCustInfo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
