package com.logisall.winus.wmsac.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC030Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSAC030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC030Service")
	private WMSAC030Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 청구작업 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSAC030.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC030");
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 청구작업 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC030/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 청구작업  저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSAC030/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : listExcel
	 * Method 설명 : 청구작업 엑셀다운
	 * 작성자 : 기드온
	 * @param request
	 * @param response
	 * @param model
	 */
	@RequestMapping("/WMSAC030/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("화주"),	  	"0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("로케이션"), 	"1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("상품"),	  	"2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("현재고수량"), 	"3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("실사수량"),    "4", "4", "0", "0", "200"},
                                   
                                   {MessageResolver.getMessage("차이이유"),  	"5", "5", "0", "0", "200"},
                                   {MessageResolver.getMessage("오류코드"),  	"6", "6", "0", "0", "200"},
                                   {MessageResolver.getMessage("유효기간"),  	"7", "7", "0", "0", "200"},
                                   {MessageResolver.getMessage("확인여부"),  	"8", "8", "0", "0", "200"},
                                   {MessageResolver.getMessage("작업일자"),  	"9", "9", "0", "0", "200"}
                                  
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"CUST_NM"   	, "S"},
                                    {"LOC_CD"   	, "S"},
                                    {"RITEM_NM"  	, "S"},
                                    {"EXP_QTY"  	, "N"},
                                    {"REAL_QTY" 	, "N"},
                                    
                                    {"DIFF_REASON"  , "S"},
                                    {"ERR_CD"   	, "S"},
                                    {"BEST_DATE"  	, "S"},
                                    {"WORK_STAT"  	, "S"},
                                    {"WORK_DT" 		, "S"}
                                   
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("재고조사결과");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
		} catch (Exception e) {
			if (log.isWarnEnabled()) {
				log.warn("fail download Excel file...", e);
			}
		}
	}
    
    /*-
	* Method ID    : saveSp
	* Method 설명      : 청구작업sp 저장
	* 작성자                 : chSong
	* @param   model
	* @return  
	* @throws Exception 
	*/
	@RequestMapping("/WMSAC030/saveSp.action")
	public ModelAndView saveSp(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveSp(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
}
