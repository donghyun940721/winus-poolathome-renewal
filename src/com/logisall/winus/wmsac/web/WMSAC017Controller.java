package com.logisall.winus.wmsac.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.wmsac.service.WMSAC017Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSAC017Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC017Service")
	private WMSAC017Service service;
	
	static final String[] COLUMN_NAME_WMSAC017_PAH = {
		"APPLY_FR_DT", "SALE_BUY_GBN", "RITEM_CD", "UNIT_PRICE", 
		"CURRENCY_NAME", "SET_LC_ID", "ITEM_GRP_CD", 
		"PER_RTN_RCV", "PER_CNG_SHP", "PER_CNG_RCV", "PER_TNS_RCV", "PER_TNS_SET",
		"PER_SIT_CAN", "PER_FST_BAD", "PER_SRT_CAN", "PER_FUNK"
	};
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 청구단가계약정보관리
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSAC017.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsac/WMSAC017", service.selectBox(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : itemAcList
	 * Method 설명 : 상품 정산 리스트
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/itemAcList.action")
	public ModelAndView itemAcList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.itemAcList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	/*-
	 * Method ID : acMstrList
	 * Method 설명 : 센터별 정산목록
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/acMstrList.action")
	public ModelAndView acMstrList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.acMstrList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	/*-
	 * Method ID : acDetailList
	 * Method 설명 : 정산(상세 2탭)
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/acDetailList.action")
	public ModelAndView acDetailList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.acDetailList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}	
	
	
	/*
	 * Method ID : save
	 * Method 설명 : 정산 마스터 등록/수정 저장
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*
	 * Method ID : savePAH
	 * Method 설명 : 정산 마스터 등록/수정 저장 - 풀앳홈 간편 용도
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/savePAH.action")
	public ModelAndView savePAH(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.savePAH(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*
	 * Method ID : delete
	 * Method 설명 : 정산 마스터 삭제
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/delete.action")
	public ModelAndView delete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	/*
	 * Method ID : itemAcSave
	 * Method 설명 : 정산 마스터 상품 일괄 적용
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/itemAcSave.action")
	public ModelAndView itemAcSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.itemAcSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	
	/*
	 * Method ID : itemAcDelete
	 * Method 설명 : 정산 마스터 상품 일괄 삭제
	 * 작성자 : yhku
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/itemAcDelete.action")
	public ModelAndView itemAcDelete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.itemAcDelete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
    /*-
	 * Method ID    : WMSAC017Q1
	 * Method 설명      : 청구단가 엑셀 입력 팝업
	 * 작성자                 : summer H
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSAC017Q1.action")
	public ModelAndView wmsac017q1(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC017Q1");
	}
    
	/*-
	 * Method ID  : ExcelUploadPAH
	 * Method 설명  : 청구 단가 Excel 입력 풀앳홈 용
	 * 작성자             : sing09
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/excelUploadPAH.action")
	public ModelAndView excelUploadPAH(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSAC017_PAH, 0, startRow, 10000, 0);
			
			Map<String, Object> mapBody = new HashMap<String, Object>();
			
			mapBody.put("chkDuplicationYn"			,model.get("chkDuplicationYn"));
			mapBody.put("vrCustId"			,model.get("vrCustId"));
			mapBody.put("SS_SVC_NO"			,model.get("SS_SVC_NO"));
			mapBody.put("SS_CLIENT_IP"		,model.get("SS_CLIENT_IP"));
			mapBody.put("SS_USER_NO"		,model.get("SS_USER_NO"));
			
			m = service.saveExcelUploadPAH(mapBody, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
			//mav = new ModelAndView("jsonView", m);
		}
		return mav;
	}
	
	/*-
	 * Method ID	: checkAllList
	 * Method 설명	: 등록된 사용중인 정산목록 전체 체크
	 * 작성자			: 
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSAC017/checkAllList.action")
	public ModelAndView checkAllList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.checkAllList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*
	 * Method ID : saveCalcUpdate
	 * Method 설명 : 정산 상세 기타단가 저장
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC017/saveCalcUpdate.action")
	public ModelAndView saveCalcUpdate(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveCalcUpdate(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}