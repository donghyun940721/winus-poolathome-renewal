package com.logisall.winus.wmsac.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsac.service.WMSAC090Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSAC090Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC090Service")
	private WMSAC090Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 청구작업 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSAC090.action")
	public ModelAndView wmsac090(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC090");
	}

	/*-
	 * Method ID : mn
	 * Method 설명 : 청구작업 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSAC095.action")
	public ModelAndView wmsac095(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsac/WMSAC095");
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 청구작업 조회
	 * 작성자 : khkim
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC090/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 청구작업 조회
	 * 작성자 : khkim
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC095/feeList.action")
	public ModelAndView feeList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.feeList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	

	/*-
	 * Method ID : save
	 * Method 설명 : 청구작업 조회
	 * 작성자 : khim
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC090/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.save(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : check
	 * Method 설명 : 기존데이터 수량 체크
	 * 작성자 : khim
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC090/check.action")
	public ModelAndView check(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.check(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

}
