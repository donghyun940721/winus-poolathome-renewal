package com.logisall.winus.wmsac.web;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ExcelWriter;
import com.m2m.jdfw5x.egov.database.GenericResultSet;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsac.service.WMSAC201Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSAC201Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC201Service")
	private WMSAC201Service service;

	static final String[] COLUMN_NAME_WMSAC201 = {
						  "O_SEQ"
						, "O_DTDS_BUSINESS_NM"
						, "O_DTDS_BUSINESS_CD"
						, "O_DTDS_OFFICE_NM"
						, "O_DTDS_OFFICE_CD"
						, "O_DTDS_CLT_DLV_NM"
						, "O_DTDS_CLT_DLV_CD"
						, "O_INVC_NO"
						, "O_FRT_DV_CD"
						, "O_WORK_DV_CD"
						
						, "O_RCPT_DV"
						, "O_DTDS_CLT_OFFICE_NM"
						, "O_DTDS_CLT_SITE_NM"
						, "O_DTDS_CLT_SITE_CD"
						, "O_DTDS_CLT_WORKER_CD"
						, "O_DTDS_CLT_WORKER_NM"
						, "O_DTDS_DLV_OFFICE_NM"
						, "O_DTDS_DLV_SITE_NM"
						, "O_DTDS_DLV_SITE_CD"
						, "O_DTDS_DLV_WORKER_CD"
						
						, "O_DTDS_DLV_WORKER_NM"
						, "O_SENDR_NM"
						, "O_SENDR_ADDR"
						, "O_SENDR_TEL_NO1"
						, "O_RCVR_NM"
						, "O_RCVR_ADDR"
						, "O_RCVR_TEL_NO1"
						, "O_GDS_QTY"
						, "O_ETC01"
						, "O_GDS_NM"
						, "O_DEFAULT_FRT_COST"
						
						, "O_SHIPMENT_FRT_COST"
						, "O_DLV_JEJU_FRT_COST"
						, "O_ETC_FRT_COST"
						, "O_TOTAL_FRT_COST"
						, "O_COD_COST"
						, "O_ORI_ORD_NO"
						, "O_CLT_DT"
						, "O_DLV_DT"
						, "O_RECEIVER_NM"
						, "O_FRT_CUST_NM"
						
						, "O_FRT_CUST_CD"
						, "O_SND_CUST_NM"
						, "O_SND_CUST_CD"
	};
	
	/*-
	 * Method ID    : WMSAC201
	 * Method 설명      : 엑셀주문입력
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSAC201.action")
	public ModelAndView WMSAC201(Map<String, Object> model) {
		return new ModelAndView("winus/WMSAC/WMSAC201");
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 공지사항 목록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC201/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : WMSAC201E2
	 * Method 설명      : 공지사항 상세화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSAC201E2.action")
	public ModelAndView WMSAC201E2(Map<String, Object> model) {
		return new ModelAndView("winus/WMSAC/WMSAC201E2");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSAC201/uploadExcelInfo.action")
	public ModelAndView uploadExcelInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSAC201 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSAC201, 0, startRow, 10000, 0);
			m = service.saveExcelInfo(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : excelDown
	 * Method 설명   : 엑셀다운로드
	 */
	@RequestMapping("/WMSAC201/excelDown.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelDown(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
        	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("HK")			, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("PART_NO")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("PART_NAME")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("SUPPILER")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("PURCHASER")		, "4", "4", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"HK"           	, "S"},
                                    {"PART_NO"          , "S"},
                                    {"PART_NAME"       	, "S"},
                                    {"SUPPILER"         , "S"},
                                    {"PURCHASER"        , "S"}
                                   }; 
            
			// 파일명
            String fileName = MessageResolver.getText("부품입고관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
