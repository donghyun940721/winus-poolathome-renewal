package com.logisall.winus.wmsac.service;

import java.util.Map;



public interface WMSAC210Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
