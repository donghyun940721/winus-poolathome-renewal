package com.logisall.winus.wmsac.service;

import java.util.Map;


public interface WMSAC030Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSp(Map<String, Object> model) throws Exception;
}
