package com.logisall.winus.wmsac.service;

import java.util.List;
import java.util.Map;

public interface WMSAC201Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveExcelInfo(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> excelDown(Map<String, Object> model) throws Exception;
}