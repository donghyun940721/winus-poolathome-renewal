package com.logisall.winus.wmsac.service;

import java.util.Map;


public interface WMSAC020Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;

    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
}
