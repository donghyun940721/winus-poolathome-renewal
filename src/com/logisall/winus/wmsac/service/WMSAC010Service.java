package com.logisall.winus.wmsac.service;

import java.util.Map;



public interface WMSAC010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listSubDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listAccDt(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveAccdt(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception;
}
