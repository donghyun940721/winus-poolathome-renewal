package com.logisall.winus.wmsac.service;

import java.util.List;
import java.util.Map;


public interface WMSAC203Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
	public Map<String, Object> save(Map<String, Object> model) throws Exception;
	public Map<String, Object> save2(Map<String, Object> model) throws Exception;
	public Map<String, Object> save3(Map<String, Object> model) throws Exception;
	public Map<String, Object> list0(Map<String, Object> model) throws Exception;
	public Map<String, Object> save7(Map<String, Object> model) throws Exception;
	public Map<String, Object> store(Map<String, Object> model) throws Exception;
	public Map<String, Object> save0(Map<String, Object> model) throws Exception;
	public Map<String, Object> chkCnt(Map<String, Object> model) throws Exception;
	public Map<String, Object> update12(Map<String, Object> model) throws Exception;
	public Map<String, Object> chkDataCnt(Map<String, Object> model) throws Exception;
	public Map<String, Object> save14(Map<String, Object> model) throws Exception;
	public Map<String, Object> list14(Map<String, Object> model) throws Exception;
	public Map<String, Object> list0Excel(Map<String, Object> model) throws Exception;
	public Map<String, Object> list15(Map<String, Object> model) throws Exception;
	public Map<String, Object> update14(Map<String, Object> model) throws Exception;
	public Map<String, Object> save15(Map<String, Object> model) throws Exception;
	public Map<String, Object> list16(Map<String, Object> model) throws Exception;
	public Map<String, Object> save16(Map<String, Object> model) throws Exception;	
	public Map<String, Object> updateFileInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveReport(Map<String, Object> model) throws Exception;
	public Map<String, Object> chkReportCnt(Map<String, Object> model) throws Exception;	
	public Map<String, Object> saveExcelInfoE2T1(Map<String, Object> model, List list) throws Exception;
	public Map<String, Object> saveTaxInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectTaxInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectIssueEmp(Map<String, Object> model) throws Exception;
	public Map<String, Object> updateTaxInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> chkTaxCnt(Map<String, Object> model) throws Exception;	
	public Map<String, Object> list18(Map<String, Object> model) throws Exception;
	public Map<String, Object> save18(Map<String, Object> model) throws Exception;
}
