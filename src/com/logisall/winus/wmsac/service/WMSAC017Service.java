package com.logisall.winus.wmsac.service;

import java.util.List;
import java.util.Map;


public interface WMSAC017Service {
    public Map<String, Object> itemAcList(Map<String, Object> model) throws Exception;    
    public Map<String, Object> acMstrList(Map<String, Object> model) throws Exception;    
    public Map<String, Object> acDetailList(Map<String, Object> model) throws Exception;    
    public Map<String, Object> save(Map<String, Object> model) throws Exception;    
    public Map<String, Object> savePAH(Map<String, Object> model) throws Exception;    
    public Map<String, Object> itemAcSave(Map<String, Object> model) throws Exception;    
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;    
    public Map<String, Object> itemAcDelete(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveExcelUpload(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> saveExcelUploadPAH(Map<String, Object> model, List list) throws Exception;

    public Map<String, Object> checkAllList(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveCalcUpdate(Map<String, Object> model) throws Exception;
}
