package com.logisall.winus.wmsac.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC090Dao")
public class WMSAC090Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
     * Method ID  : list
     * Method 설명  : 상품조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        GenericResultSet genericResultSet = new GenericResultSet();
        List list = executeQueryForList("wmsac090.list", model); 
        genericResultSet.setList(list);
    	return genericResultSet;
    }
    
    /**
     * Method ID  : insert
     * Method 설명  : 상품조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public int insert(Map<String, Object> model) {
    	return (int)executeInsert("wmsac090.insert", model);
    }
    
    /**
     * Method ID  : update
     * Method 설명  : 상품조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public int update(Map<String, Object> model) {
    	return (int)executeUpdate("wmsac090.update", model);
    }
    
    /**
     * Method ID  : check
     * Method 설명  : 상품조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public int check(Map<String, Object> model) {
    	return (int)executeView("wmsac090.check", model);
    }

	public GenericResultSet feeList(Map<String, Object> model) {
		return executeQueryPageWq("wmsac090.feeList", model);
	}
    
    
    
}
