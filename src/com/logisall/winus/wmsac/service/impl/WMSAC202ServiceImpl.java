package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC202Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC202Service")
public class WMSAC202ServiceImpl extends AbstractServiceImpl implements WMSAC202Service {
    
    @Resource(name = "WMSAC202Dao")
    private WMSAC202Dao dao;    
    
    /**
     * Method ID : listPool
     * Method 설명 : 부자재사용내역(일반) 조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listPool(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.listPool(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
            map.put("ORD01", dao.selectOrd01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

	@Override
	public Map<String, Object> save3(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();				
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("SUB_MATLS_COSTS_SEQ", model.get("SUB_MATLS_COSTS_SEQ" + i));
				modelDt.put("CUST_ID", model.get("CUST_ID" + i));
				
				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {
					
					modelDt.put("ITEM_GRP_ID", model.get("ITEM_GRP_ID" + i));
					modelDt.put("RITEM_ID", model.get("RITEM_ID" + i));
					modelDt.put("ACC_DT", model.get("ACC_DT" + i));
					modelDt.put("BEFORE_STOCK_QTY", model.get("BEFORE_STOCK_QTY" + i));
					modelDt.put("IN_QTY", model.get("IN_QTY" + i));
					modelDt.put("USE_QTY", model.get("USE_QTY" + i));
					modelDt.put("STOCK_QTY", model.get("STOCK_QTY" + i));
					modelDt.put("UNIT_COST", model.get("UNIT_COST" + i));
					modelDt.put("COST", model.get("COST" + i));
					modelDt.put("ETC", model.get("ETC" + i));					
					
					modelDt.put("ACC_TYPE", model.get("ACC_TYPE" + i));
					modelDt.put("COST_VOLUMN", model.get("COST_VOLUMN" + i));
					modelDt.put("COST_CONTRACT", model.get("COST_CONTRACT" + i));
					modelDt.put("ACC_UNIT", model.get("ACC_UNIT" + i));					
				}

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert3(modelDt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {					
					dao.update3(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					dao.delete3(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			//m.put("MSG", MessageResolver.getMessage("save.success"));
			if ("DELETE".equals(model.get("ST_GUBUN" + 0))){				
				m.put("MSG", MessageResolver.getMessage("delete.success"));
			} else {				
				m.put("MSG", MessageResolver.getMessage("save.success"));
			}

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}	
	
	/**
     * Method ID : list0
     * Method 설명 : 보관비내역  조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list0(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
            
            model.put("LC_ID",(String)model.get(ConstantIF.SS_SVC_NO));	
            
            //wmsac200테이블에 저장 되었는지 체크
            int intCnt = dao.ChkWMSAC200_keep_Cnt(model);
            
            if (intCnt > 0)
            {            	
            	//wmsac200테이블에 저장
            	map.put("LIST", dao.ChkWMSAC200_keep_list0(model));
            }
            else
            {            	
            	//3개월 전, 후 체크
	            int intCnt2 = dao.ChkWMSAC202Cnt(model);            
	            
	            if (intCnt2 == 1)
	            	map.put("LIST", dao.list0_before(model)); //3개월 미만
	            else
	               	map.put("LIST", dao.list0_after(model)); //3개월 후
            }
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}	
	
	/**
     * Method ID : manage
     * Method 설명 : 임가공내역  조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> manage(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            model.put("LC_ID",(String)model.get(ConstantIF.SS_SVC_NO));
            
            int intCnt = dao.ChkWMSAC207_foundry_Cnt(model);
            
            if (intCnt > 0)
            	map.put("LIST", dao.manage_207(model));
            else
            	map.put("LIST", dao.manage(model));            
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : save16
     * Method 설명 : 임가공 내역 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */	
	@Override
	public Map<String, Object> save16(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		
		//int intCnt = dao.ChkWMSAC207Cnt(model);
		
		//이미 wmsac200에 데이터가 있으면 저장 불가
		//if (intCnt > 0){
		//	map.put("MSG", "해당 정산년월 데이터가 존재하여 저장할 수 없습니다.");
		//} else {		
			
		//session 정보
		model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));		
		
		dao.delete207_foundary(model);
		dao.moveToWMSAC207(model);
		
		map.put("CODE", "SUCCESS");
		map.put("MSG", "저장(이관) 되었습니다.");			
		//}
		
		return map;
	}
	
	/**
     * Method ID : save0
     * Method 설명 :  보관비 저장(이관)
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> save0(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		int intCnt = 0;	
		
		System.err.println(model);
		System.err.println(model.get("GUBUN"));
		System.err.println(model.get("YYYYMMDD"));		
		
		model.put("acc_type", "001");
		
		/*
		//wmsac200테이블에 저장하려는 데이터가 있는지 체크
		if("BEFORE".equals(model.get("GUBUN").toString()))
		{
			System.err.println("1");
			model.put("subul_dt", model.get("YYYYMMDD"));
			intCnt = dao.ChkWMSAC200Cnt_before(model);
		}
		else
		{
			System.err.println("2");
			intCnt = dao.ChkWMSAC200Cnt_after(model);
		}
		*/
		
		//intCnt = dao.ChkWMSAC200Cnt(model);
		
		//이미 wmsac200에 데이터가 있으면 저장 불가
		//if (intCnt > 0){
		//	map.put("MSG", "이미 데이터가 존재하여 저장할 수 없습니다.");
		//} else {		
			
		//session 정보
		model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
		
		dao.delete200_keep(model);
        
		if("BEFORE".equals(model.get("GUBUN").toString()))
        	map.put("LIST", dao.moveToWMSAC200_before(model)); //3개월 미만
        else
           	map.put("LIST", dao.moveToWMSAC200_after(model)); //3개월 후			
		
		//dao.moveToWMSAC200(model);
		
		map.put("CODE", "SUCCESS");
		map.put("MSG", "저장(이관) 되었습니다.");			
		//}
		
		return map;
	}
	
	/**
     * Method ID : store
     * Method 설명 : 입출고비내역  조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> store(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
                map.put("LIST", dao.store(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : chkCnt
     * Method 설명 : wmsac200 테이블 Count 체크
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> chkCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();		
		
		int intCnt = dao.ChkCntRow(model);		
		map.put("cnt", ""+intCnt);
		
		return map;
	}	

	@Override
	public Map<String, Object> chkDataCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();		
		
		model.put("LC_ID",(String)model.get(ConstantIF.SS_SVC_NO));	
		
		map.put("LIST", dao.chkDataCnt(model));			
		
		System.err.println(map);
		
		return map;
	}	
	
	/**
     * Method ID : list15
     * Method 설명 : 반품내역 조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list15(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        model.put("LC_ID",(String)model.get(ConstantIF.SS_SVC_NO));	
        
        int intCnt = dao.ChkWMSAC200_return_Cnt(model);
        
        if (intCnt > 0)
        	map.put("LIST", dao.list15_200(model));
        else
        	map.put("LIST", dao.list15(model));        
        
        return map;
	}	
	
	/**
     * Method ID : save15
     * Method 설명 : 반품내역 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> save15(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		
		model.put("acc_type", "024");
		
		//int intCnt = dao.ChkWMSAC200Cnt_15(model);
		
		//이미 wmsac200에 데이터가 있으면 저장 불가
		//if (intCnt > 0){
		//	map.put("MSG", "해당 정산년월 데이터가 존재하여 저장할 수 없습니다.");
		//} else {		
			
		//session 정보
		model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));		
		
		dao.delete200_return(model);
		dao.moveToWMSAC200_15(model);
		
		map.put("CODE", "SUCCESS");
		map.put("MSG", "저장(이관) 되었습니다.");
		//}
		
		return map;
	}

	@Override
	public Map<String, Object> list17(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        model.put("LC_ID",(String)model.get(ConstantIF.SS_SVC_NO));	
        
        int intCnt = dao.ChkWMSAC200_delivery_Cnt(model);
        
        if (intCnt > 0)
        	map.put("LIST", dao.list17_200(model));
        else
        	map.put("LIST", dao.list17(model));        
        
        return map;
	}

	@Override
	public Map<String, Object> save17(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		
		model.put("acc_type", "012");
		
		//int intCnt = dao.ChkWMSAC200Cnt_17(model);
		
		//이미 wmsac200에 데이터가 있으면 저장 불가
		//if (intCnt > 0){
		//	map.put("MSG", "해당 정산년월 데이터가 존재하여 저장할 수 없습니다.");
		//} else {		
			
		//session 정보
		model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));		
		
		dao.delete200_delivery(model);
		dao.moveToWMSAC200_17(model);
		
		map.put("CODE", "SUCCESS");
		map.put("MSG", "저장(이관) 되었습니다.");
		//}
		
		return map;
	}
	
	/**
     * Method ID : add
     * Method 설명 : 부가서비스 조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> add(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            model.put("LC_ID",(String)model.get(ConstantIF.SS_SVC_NO));
            
            map.put("LIST", dao.add(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	
	/**
     * Method ID : add
     * Method 설명 : 부가서비스 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> save10(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();			
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("CUST_ID",  model.get("CUST_ID" + i));
				modelDt.put("ACC_SEQ", model.get("ACC_SEQ" + i));
				
				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {										
					
					modelDt.put("ACC_TYPE", model.get("ACC_TYPE" + i));
					modelDt.put("ACC_DT", model.get("ACC_DT" + i));
					modelDt.put("QTY", model.get("QTY" + i));
					modelDt.put("ETC", model.get("ETC" + i));
					modelDt.put("COST_VOLUMN", model.get("COST_VOLUMN" + i));
					modelDt.put("COST_CONTRACT", model.get("COST_CONTRACT" + i));
					modelDt.put("ACC_UNIT", model.get("ACC_UNIT" + i));
					modelDt.put("COST", model.get("COST" + i));
					modelDt.put("SERIAL_NO", model.get("SERIAL_NO" + i));
					modelDt.put("WORK_TYPE", model.get("WORK_TYPE" + i));
				} 				

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert10(modelDt);					
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.update10(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {					
					dao.delete10(modelDt);					
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			
			m.put("errCnt", errCnt);		
			
			if ("DELETE".equals(model.get("ST_GUBUN" + 0))){				
				m.put("MSG", MessageResolver.getMessage("delete.success"));
			} else {				
				m.put("MSG", MessageResolver.getMessage("save.success"));
			}
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
     * Method ID : saveReport
     * Method 설명 : 마타주 거래명세서 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> saveReport(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
        try{     
        	
        	System.err.println(model);
        	System.err.println(model.get("ACC_YY_MM"));
        	
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();                	
            	
            //main
            modelIns.put("I_ACC_YY_MM", (String)model.get("ACC_YY_MM"));
            modelIns.put("I_LC_ID",      (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_CUST_ID",    (String)model.get("CUST_ID"));            
            modelIns.put("I_USER_NO", 	 (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("I_WORK_IP", 	 (String)model.get(ConstantIF.SS_CLIENT_IP));                                    

            //dao                
            modelIns = (Map<String, Object>)dao.saveReport(modelIns);
            ServiceUtil.isValidReturnCode("WMSAC202", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                      
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );             
            
        } catch(Exception e){
            throw e;
        }
        return m;
	}
}