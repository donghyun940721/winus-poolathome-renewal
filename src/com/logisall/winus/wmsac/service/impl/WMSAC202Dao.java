package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC202Dao")
public class WMSAC202Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID : ChkWMSAC202Cnt
     * Method 설명 : 보관내역 3개월 지났는지 체크, 리턴이 1이면 3개월 미만, 0이면 3개월 후
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC202Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMAC202Cnt", model);
	}
	
    /**
     * Method ID : list3
     * Method 설명 : 부자재사용내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public GenericResultSet listPool(Map<String, Object> model) {
        return executeQueryPageWq("wmsac202.list3", model);
    } 
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : selectPool
     * Method 설명 : 물류용기군 셀렉트박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectPool(Map<String, Object> model){
        return executeQueryForList("wmspl020.selectPoolGrp", model);
    }
    
    /**
     * Method ID : selectOrd01
     * Method 설명 : 디테일 ORD01 셀렉트 박스
     * 작성자 : 민환기
     * @param model
     * @return
     */
    public Object selectOrd01(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
	/**
     * Method ID : insert3
     * Method 설명 : 부자재내역 등록
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insert3(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.insert3", modelDt);
	}
	
	/**
     * Method ID : list0
     * Method 설명 : 보관비 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public GenericResultSet list0_after(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.list0_after", model);
	}
	
	/**
     * Method ID : update3
     * Method 설명 : 부자재사용 내역3
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void update3(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		executeUpdate("wmsac202.update3", modelDt);
	}
	
	/**
     * Method ID : manage
     * Method 설명 : 관리비 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object manage(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.manage", model);
	}
	
	/**
     * Method ID : store
     * Method 설명 : 입출고비 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object store(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.store", model);
	}
	
	/**
     * Method ID : ChkWMAC200Cnt_before
     * Method 설명 : wmsac200 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200Cnt_before(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMAC200Cnt_before", model);
	}
	
	/**
     * Method ID : ChkWMAC200Cnt_after
     * Method 설명 : wmsac200 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200Cnt_after(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMAC200Cnt_after", model);
	}
	
	/**
     * Method ID : ChkWMAC200Cnt
     * Method 설명 : wmsac200 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200Cnt(Map<String, Object> model) {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMAC200Cnt", model);
	}
	
	/**
     * Method ID : wmsac202_list0
     * Method 설명 : wmsac202 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object list0_before(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.list0_before", model);
	}
	
	/**
     * Method ID : moveToWMSAC200
     * Method 설명 : wmsac200 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC200(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.moveToWMSAC200", model);
	}
	
	/**
     * Method ID : ChkWMAC200Cnt
     * Method 설명 : wmsac200 체크(건바이건)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkCntRow(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkCntRow", model);
	}
    
    /**
     * Method ID : chkDataCnt
     * Method 설명 : 거래명세서 각 화면별 데이터 Cnt 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object chkDataCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryWq("wmsac202.chkDataCnt", model);
	}		
	
	/**
     * Method ID : 부자재비 삭제
     * Method 설명 : 부자재비 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete3(Map<String, Object> modelDt) throws Exception{
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		executeDelete("wmsac202.delete3", modelDt);
	}
	
	/**
     * Method ID : 반품내역 조회
     * Method 설명 : 반품내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object list15(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.list15", model);
	}	
	
	
	
	/**
     * Method ID : 배송내역 조회
     * Method 설명 : 배송내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object list17(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.list17", model);
	}	
	
	
	/**
     * Method ID : 임가공 테이블 체크(wmsac207)
     * Method 설명 : 임가공 테이블 체크(wmsac207)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC207Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMAC207Cnt", model);
	}
	
	/**
     * Method ID : 임가공 테이블 저장(이관)(wmsac207)
     * Method 설명 : 임가공 테이블 저장(이관)(wmsac207)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC207(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.moveToWMSAC207", model);
		
	}
	
	/**
     * Method ID : 보관비 테이블 저장(이관)(wmsac200)_3개월전
     * Method 설명 : 보관비 테이블 저장(이관)(wmsac200)_3개월전
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC200_before(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.moveToWMSAC200_before", model);
	}
	
	/**
     * Method ID : 보관비 테이블 저장(이관)(wmsac200)_3개월후
     * Method 설명 : 보관비 테이블 저장(이관)(wmsac200)_3개월후
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC200_after(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.moveToWMSAC200_after", model);
	}
	
	/**
     * Method ID : 반품비 수량 체크(wmsac200)
     * Method 설명 : 반품비 수량 체크(wmsac200)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200Cnt_15(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMAC200Cnt_15", model);
	}
	
	/**
     * Method ID : 반품비 저장(이관)(wmsac200)
     * Method 설명 : 반품비 저장(이관)(wmsac200)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC200_15(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub		
		return executeInsert("wmsac202.moveToWMSAC200_15", model);
	}
	
	/**
     * Method ID : 배송비 수량 체크(wmsac200)
     * Method 설명 : 배송비 수량 체크(wmsac200)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200Cnt_17(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMAC200Cnt_17", model);
	}
	
	/**
     * Method ID : 배송비 저장(이관)(wmsac200)
     * Method 설명 : 배송비 저장(이관)(wmsac200)
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object moveToWMSAC200_17(Map<String, Object> model) throws Exception  {
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.moveToWMSAC200_17", model);
	}
	
	/**
     * Method ID : 부가서비스 조회
     * Method 설명 : 부가서비스 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object add(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.add", model);
	}
	
	/**
     * Method ID : 부가서비스 조회
     * Method 설명 : 부가서비스 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object insert10(Map<String, Object> modelDt)throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.insert10", modelDt);		
	}
	
	/**
     * Method ID : 부가서비스 등록
     * Method 설명 : 부가서비스 등록
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object update10(Map<String, Object> modelDt) throws Exception {		
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.update10", modelDt);	
	}
	
	/**
     * Method ID : 부가서비스 삭제
     * Method 설명 : 부가서비스 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object delete10(Map<String, Object> modelDt) throws Exception {
		// TODO Auto-generated method stub
		return executeInsert("wmsac202.delete10", modelDt);		
	}
	
	/**
     * Method ID : ChkWMSAC207_keep_Cnt
     * Method 설명 : ChkWMSAC207_keep_Cnt 저장 유무 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200_keep_Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMSAC200_keep_Cnt", model);
	}
	
	/**
     * Method ID : ChkWMSAC207_keep_list0
     * Method 설명 : ChkWMSAC207_keep_list0 :: 저장 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object ChkWMSAC200_keep_list0(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.ChkWMSAC200_keep_list0", model);
	}
	
	/**
     * Method ID : ChkWMSAC207_foundry_Cnt
     * Method 설명 : ChkWMSAC207_foundry_Cnt 임가공  저장 유무 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC207_foundry_Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMSAC207_foundry_Cnt", model);
	}
	
	/**
     * Method ID : manage_207
     * Method 설명 : manage_207 :: 임가공 저장 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object manage_207(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.manage_207", model);
	}
	
	/**
     * Method ID : ChkWMSAC200_delivery_Cnt
     * Method 설명 : ChkWMSAC200_delivery_Cnt :: 배송비 저장 유무 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200_delivery_Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMSAC200_delivery_Cnt", model);
	}
	
	/**
     * Method ID : list17_207
     * Method 설명 : list17_207 :: 배송비 저장 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object list17_200(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.list17_200", model);
	}
	
	/**
     * Method ID : ChkWMSAC200_return_Cnt
     * Method 설명 : ChkWMSAC200_return_Cnt :: 반품비 저장 유무 체크
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public int ChkWMSAC200_return_Cnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return (Integer) executeQueryForObject("wmsac202.ChkWMSAC200_return_Cnt", model);
	}
	
	/**
     * Method ID : list15_200
     * Method 설명 : list15_200 :: 배송비 저장 내역 조회
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Object list15_200(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		return executeQueryPageWq("wmsac202.list15_200", model);
	}
	
	/**
     * Method ID : delete207_foundary
     * Method 설명 : delete207_foundary :: 임가공 내역 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete207_foundary(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac202.delete207_foundary", model);
	}
	
	/**
     * Method ID : delete200_keep
     * Method 설명 : delete200_keep :: 임가공 내역 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete200_keep(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac202.delete200_keep", model);		
	}
	
	/**
     * Method ID : delete200_delivery
     * Method 설명 : delete200_delivery :: 배달비 내역 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete200_delivery(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac202.delete200_delivery", model);		
	}
	
	/**
     * Method ID : delete200_return
     * Method 설명 : delete200_return :: 반품비 내역 삭제
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public void delete200_return(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		executeDelete("wmsac202.delete200_return", model);		
	}
	
	/**
     * Method ID : saveReport
     * Method 설명 : saveReport :: 
     * 작성자 : 민환기
     * @param model
     * @return
     */
	public Map<String, Object> saveReport(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		executeUpdate("wmsac202.pk_wmsac100.sp_save_trxbill", model);
        return model;
	}
}