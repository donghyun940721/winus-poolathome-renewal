package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC141Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC141Service")
public class WMSAC141ServiceImpl extends AbstractServiceImpl implements WMSAC141Service {
	protected Log loger = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSAC141Dao")
	private WMSAC141Dao dao;

   /**
    * 
    * 대체 Method ID   : list
    * 대체 Method 설명    : 고객관리 조회
    * 작성자                      : chsong
    * @param   model
    * @return
    * @throws  Exception
    */
   @Override
   public Map<String, Object> list(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       if(model.get("page") == null) {
           model.put("pageIndex", "1");
       } else {
           model.put("pageIndex", model.get("page"));
       }
       if(model.get("rows") == null) {
           model.put("pageSize", "20");
       } else {
           model.put("pageSize", model.get("rows"));
       }
       System.out.println(">>>>>>>>>");
       //정산기준에 따라 조회처리 분기
       if(model.get("vrSrchAdjuctGb").equals("CAL")) {
    	   map.put("LIST", dao.listCal(model));
       }else if(model.get("vrSrchAdjuctGb").equals("FEE")){
    	   map.put("LIST", dao.listFee(model));
       }
       
       return map;
   }
   
   /**
    * 
    * 대체 Method ID   : save
    * 대체 Method 설명    : 
    * 작성자                      : chsong
    * @param model
    * @return
    * @throws Exception
    */
   @Override
   public Map<String, Object> save(Map<String, Object> model) throws Exception {
   	Map<String, Object> m = new HashMap<String, Object>();
       // log.info(model);
       try{
       	int insCnt = Integer.parseInt(model.get("selectIds").toString());
       	
       	if(insCnt > 0){
       		//저장, 수정         
       		String[] applyFrDt    = new String[insCnt];
       		String[] applyToDt    = new String[insCnt];
       		String[] transCustId    = new String[insCnt];
       		String[] adjuctGb     = new String[insCnt];

       		for(int i = 0 ; i < insCnt ; i ++){        
       			applyFrDt[i]       = (String)model.get("APPLY_FR_DT"+i);
       			applyToDt[i]   = (String)model.get("APPLY_TO_DT"+i);
       			transCustId[i]   = (String)model.get("SALES_COMPANY_ID"+i);
       			adjuctGb[i]   = (String)model.get("ADJUCT_GB"+i);
       		}
       		   
       		//프로시져에 보낼것들 다담는다
       		Map<String, Object> modelIns = new HashMap<String, Object>();
       		modelIns.put("I_APPLY_FR_DT"        , applyFrDt);
       		modelIns.put("I_APPLY_TO_DT"   , applyToDt);
       		modelIns.put("I_TRANS_CUST_ID"   , transCustId);
       		modelIns.put("I_ADJUCT_GB"    , adjuctGb);

       		//session 정보
       		modelIns.put("I_LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
       		modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
       		modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
       		
       		modelIns = (Map<String, Object>)dao.save(modelIns);
       		ServiceUtil.isValidReturnCode("WMSAC141", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
       	}

        m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));
 
       } catch(BizException be) {
           m.put("errCnt", 1);
           m.put("MSG", be.getMessage() );
           
       } catch(Exception e){
           throw e;
       }
       return m;
   }
   
   /**
    * 
    * 대체 Method ID   : reportSave
    * 대체 Method 설명    : 
    * 작성자                      : chsong
    * @param model
    * @return
    * @throws Exception
    */
   @Override
   public Map<String, Object> reportSave(Map<String, Object> model) throws Exception {
   	Map<String, Object> m = new HashMap<String, Object>();
       // log.info(model);
       try{
       	int insCnt = Integer.parseInt(model.get("selectIds").toString());
       	
       	if(insCnt > 0){
       		//저장, 수정         
       		String[] applyFrDt    = new String[insCnt];
       		String[] applyToDt    = new String[insCnt];
       		String[] transCustId    = new String[insCnt];
       		String[] adjuctGb     = new String[insCnt];

       		for(int i = 0 ; i < insCnt ; i ++){        
       			applyFrDt[i]       = (String)model.get("APPLY_FR_DT"+i);
       			applyToDt[i]   = (String)model.get("APPLY_TO_DT"+i);
       			transCustId[i]   = (String)model.get("SALES_COMPANY_ID"+i);
       			adjuctGb[i]   = (String)model.get("ADJUCT_GB"+i);
       		}
       		   
       		//프로시져에 보낼것들 다담는다
       		Map<String, Object> modelIns = new HashMap<String, Object>();
       		modelIns.put("I_APPLY_FR_DT"        , applyFrDt);
       		modelIns.put("I_APPLY_TO_DT"   , applyToDt);
       		modelIns.put("I_TRANS_CUST_ID"   , transCustId);
       		modelIns.put("I_ADJUCT_GB"    , adjuctGb);

       		//session 정보
       		modelIns.put("I_LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
       		modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
       		modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
       		
       		modelIns = (Map<String, Object>)dao.reportSave(modelIns);
       		ServiceUtil.isValidReturnCode("WMSAC141", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
       	}

        m.put("errCnt", 0);
	    m.put("MSG", MessageResolver.getMessage("save.success"));
 
       } catch(BizException be) {
           m.put("errCnt", 1);
           m.put("MSG", be.getMessage() );
           
       } catch(Exception e){
           throw e;
       }
       return m;
   }
   
   /**
    * 대체 Method ID   : listExcel
    * 대체 Method 설명 : 고객관리 엑셀.
    * 작성자      : chsong
    * @param model
    * @return
    * @throws Exception
    */
   public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       
       model.put("pageIndex", "1");
       model.put("pageSize", "60000");
       
      //정산기준에 따라 조회처리 분기
       if(model.get("vrSrchAdjuctGb") == "CAL") {
    	   map.put("LIST", dao.listCal(model));
       }else if(model.get("vrSrchAdjuctGb") == "FEE"){
    	   map.put("LIST", dao.listFee(model));
       }
       return map;
   } 
   
   /**
    * 
    * 대체 Method ID   : reportList
    * 대체 Method 설명    : 고객관리 조회
    * 작성자                      : chsong
    * @param   model
    * @return
    * @throws  Exception
    */
   @Override
   public Map<String, Object> reportList(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       if(model.get("page") == null) {
           model.put("pageIndex", "1");
       } else {
           model.put("pageIndex", model.get("page"));
       }
       if(model.get("rows") == null) {
           model.put("pageSize", "20");
       } else {
           model.put("pageSize", model.get("rows"));
       }
       map.put("LIST", dao.reportList(model));
       return map;
   }
   
   /**
    * 대체 Method ID   : listExcelT1
    * 대체 Method 설명 : 고객관리 엑셀.
    * 작성자      : chsong
    * @param model
    * @return
    * @throws Exception
    */
   public Map<String, Object> listExcelT1(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       
       model.put("pageIndex", "1");
       model.put("pageSize", "60000");
       
       map.put("LIST", dao.reportList(model));
       
       return map;
   }
   
   /*-
	 * Method ID   : getCustInfo
	 * Method 설명 :
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
   public Map<String, Object> getCustInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("CUST_INFO")) {
			map.put("CUST_INFO", dao.getCustInfo(model));
		}
		return map;
	}
}
