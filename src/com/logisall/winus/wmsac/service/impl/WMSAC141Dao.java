package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC141Dao")
public class WMSAC141Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * Method ID : listCal 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listCal(Map<String, Object> model) {
		return executeQueryPageWq("wmsac141.listCal", model);
	}
	
	/**
	 * Method ID : listFee 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listFee(Map<String, Object> model) {
		return executeQueryPageWq("wmsac141.listFee", model);
	}

	/**
     * Method ID    : save
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsac141.pk_wmsac010.sp_calc_peroid_dlv", model);
        return model;
    }
    
	/**
     * Method ID    : reportSave
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object reportSave(Map<String, Object> model){
        executeUpdate("wmsac141.pk_wmsac010.sp_tax_peroid_dlv", model);
        return model;
    }
    
    /**
	 * Method ID : reportList 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet reportList(Map<String, Object> model) {
		return executeQueryPageWq("wmsac141t1.reportList", model);
	}
	
	/**
     * Method ID  : getCustInfo
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getCustInfo(Map<String, Object> model){
        return executeQueryForList("wmsac141t1.getCustInfo", model);
    }
}
