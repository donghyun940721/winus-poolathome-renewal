package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC030Service")
public class WMSAC030ServiceImpl extends AbstractServiceImpl implements WMSAC030Service {
    
    @Resource(name = "WMSAC030Dao")
    private WMSAC030Dao dao;


    /**
     * Method ID : list
     * Method 설명 : 청구작업 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            map.put("LIST", dao.list(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 청구작업 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> main = new HashMap<String, Object>();
        int errCnt = 0;

            try{
                int iuCnt = Integer.parseInt(model.get("selectIds").toString());
                
                if(iuCnt > 0){
                    for(int i=0; i<iuCnt; i++){
                    	main.put("ST_GUBUN", 	model.get("ST_GUBUN"+i));
                    	
                        main.put("IN_QTY", 		model.get("IN_QTY"+i));
                        main.put("OUT_QTY", 	model.get("OUT_QTY"+i));
                        main.put("SUM_INV_QTY", model.get("SUM_INV_QTY"+i));
                        
                        main.put("PLT_IN_QTY",	model.get("PLT_IN_QTY"+i));
                        main.put("PLT_OUT_QTY", model.get("PLT_OUT_QTY"+i));
                        
                        main.put("RITEM_ID", 	model.get("RITEM_ID"+i));
                        main.put("WORK_DT", 	model.get("WORK_DT"+i));
                        
                        main.put("SS_USER_NO",  model.get(ConstantIF.SS_USER_NO));
                        main.put("SS_SVC_NO",   model.get(ConstantIF.SS_SVC_NO));
						
                        if(main.get("ST_GUBUN").equals("UPDATE")){
                            dao.update(main);
                        }else{
                            errCnt++;
                        }
                    }
                }else{
                    errCnt++;
                }
                
                if(errCnt == 0){
                    m.put("MSG", MessageResolver.getMessage("save.success"));
                    m.put("errCnt", errCnt);
                }else{
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                    m.put("errCnt", errCnt);
                }
            }catch (Exception e) {
                throw e;
            }
        return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 청구작업 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
  
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }

    /**
     * 
     * Method ID   : saveSp
     * Method 설명    : 청구작업sp 저장
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveSp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        Map<String, Object> modelIns = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("save.error");
            try{
                int iuCnt = Integer.parseInt(model.get("selectIds").toString());
                
                if(iuCnt > 0){
                    for(int i=0; i<iuCnt; i++){
                        modelIns.put("vrSrchReqDtFrom",  model.get("vrSrchReqDtFrom"+i).toString().replace("-", ""));
                        modelIns.put("vrSrchReqDtTo",  	 model.get("vrSrchReqDtTo"+i).toString().replace("-", ""));
                        modelIns.put("accId", 			 model.get("ACC_ID"+i));
                        
                        modelIns.put("gvLcId",  (String)model.get(ConstantIF.SS_SVC_NO));
                        modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                        modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                        
                        modelIns = (Map<String, Object>)dao.saveSp(modelIns);
                        errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                        errMsg = modelIns.get("O_MSG_NAME").toString();
                    }
                }else{
                    errCnt++;
                }
                
                if(errCnt == 0){
                    m.put("MSG", MessageResolver.getMessage("save.success"));
                    m.put("errCnt", errCnt);
                }else{
                    m.put("MSG", MessageResolver.getMessage("save.error"));
                    m.put("errCnt", errCnt);
                }
            }catch (Exception e) {
                throw e;
            }
        return m;
    }
}
