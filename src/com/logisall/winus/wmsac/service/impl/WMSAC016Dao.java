package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC016Dao")
public class WMSAC016Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : itemAcList
     * Method 설명 : 상품 정산 조회
     * 작성자 : yhku
     * @param model
     * @return
     */
    public GenericResultSet itemAcList(Map<String, Object> model) {
        return executeQueryPageWq("wmsac016.itemAcList", model);
    }   
    
    /**
     * Method ID : acMstrList
     * Method 설명 : 정산 마스터 조회
     * 작성자 :  yhku
     * @param model
     * @return
     */
    public GenericResultSet acMstrList(Map<String, Object> model) {
        return executeQueryPageWq("wmsac016.acMstrList", model);
    }   
    
       
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    
    /**
     * Method ID    : save
     * Method 설명      : 청구단가계약 등록
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){
        executeUpdate("wmsac016.pk_wmsac016.sp_save_cont", model);
        return model;
    }
    
    /**
     * Method ID    : savePAH
     * Method 설명      : 청구단가계약 등록 PAH 분리
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object savePAH(Map<String, Object> model){
    	executeUpdate("wmsac016.pk_wmsac016.sp_save_cont_pah", model);
    	return model;
    }
    
    /**
     * Method ID    : delete
     * Method 설명      : 청구단가계약 삭제
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object delete(Map<String, Object> model){
        executeUpdate("wmsac016.pk_wmsac016.sp_del_cont_detail", model);
        return model;
    }
    

    
    /**
     * Method ID    : itemAcSave
     * Method 설명      : 청구단가계약 상품일괄적용
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object itemAcSave(Map<String, Object> model){
        executeUpdate("wmsac016.pk_wmsac016.sp_save_item_cont", model);
        return model;
    }
    
    
    /**
     * Method ID    : itemAcDelete
     * Method 설명      : 청구단가계약 삭제
     * 작성자                 : yhku
     * @param   model
     * @return
     */
    public Object itemAcDelete(Map<String, Object> model){
        executeUpdate("wmsac016.pk_wmsac016.sp_del_item_cont", model);
        return model;
    }
    
    /**
     * Method ID    : saveSub
     * Method 설명      : 청구단가계약 등록 - 엑셀입력
     * 작성자                 : Summer Hyun
     * @param   model
     * @return
     */
    public Object saveSubExcel(Map<String, Object> model){
        executeUpdate("wmsac016.pk_wmsac016.sp_save_excel_cont", model);
        return model;
    }
    
    /**
     * Method ID    : saveSubExcelPAH
     * Method 설명      : 청구단가계약 등록 - PAH 간편 엑셀입력 
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object saveSubExcelPAH(Map<String, Object> model){
    	executeUpdate("wmsac016.pk_wmsac016.sp_save_excel_cont_pah", model);
    	return model;
    }
    
}

