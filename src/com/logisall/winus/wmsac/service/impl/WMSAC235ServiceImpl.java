package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsac.service.WMSAC235Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSAC235Service")
public class WMSAC235ServiceImpl implements WMSAC235Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSAC235Dao")
    private WMSAC235Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSAC235 = {"ORD_ID", "DLV_ADD_COST"};
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 운임비목록 조회
     * 작성자                      : yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 운임비저장
     * 작성자                      : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
        	 int insCnt = Integer.parseInt(model.get("selectIds").toString());
        	 
        	 if (insCnt > 0) {
        		 // 저장, 수정
                 String[] orgOrdId = new String[insCnt];
                // String[] dlvAddCost = new String[insCnt];
                 String[] defaultCost = new String[insCnt]; //DEFAULT_COST
                 String[] addCost = new String[insCnt];		//ADD_COST
                 String[] addCostRsn = new String[insCnt];	//ADD_COST_RSN
                 String[] vistCost = new String[insCnt];	//VISIT_COST
                 String[] vistCostRsn = new String[insCnt]; //VISIT_COST_RSN
                 String[] etcCost = new String[insCnt]; 	//ETC_COST
                 String[] etcCostRsn = new String[insCnt];  //ETC_COST_RSN
                 String[] payType = new String[insCnt]; 	//PAY_TYPE
                 String[] desc1 = new String[insCnt]; 		//DESC1
                 String[] prevAddCost = new String[insCnt]; 		//PREV_ADD_COST
        	 
                 for(int i = 0 ; i < insCnt ; i ++){
                	 orgOrdId[i]		= (String)model.get("ORG_ORD_ID" + i);
                	 // dlvAddCost[i] 	= (String)model.get("DLV_ADD_COST" + i);
                	 defaultCost[i]		= (String)model.get("DEFAULT_COST" + i);
                	 addCost[i]			= (String)model.get("ADD_COST" + i);
                	 addCostRsn[i]		= (String)model.get("ADD_COST_RSN" + i);
                	 vistCost[i]		= (String)model.get("VISIT_COST" + i);
                	 vistCostRsn[i]		= (String)model.get("VISIT_COST_RSN" + i);
                	 etcCost[i]			= (String)model.get("ETC_COST" + i);
                	 etcCostRsn[i]		= (String)model.get("ETC_COST_RSN" + i);
                	 payType[i]			= (String)model.get("PAY_TYPE" + i);
                	 desc1[i]			= (String)model.get("DESC1" + i);
                	 prevAddCost[i]			= (String)model.get("PREV_ADD_COST" + i);
                	 
                 }
                 
                // 프로시져에 보낼것들 다담는다
                 Map<String, Object> modelIns = new HashMap<String, Object>();
                 
                 modelIns.put("orgOrdId" 	, orgOrdId);
                 //modelIns.put("dlvAddCost" , dlvAddCost);
                 modelIns.put("defaultCost" , defaultCost);
                 modelIns.put("addCost"		, addCost);
                 modelIns.put("addCostRsn"  , addCostRsn);
                 modelIns.put("vistCost" 	, vistCost);
                 modelIns.put("vistCostRsn" , vistCostRsn);
                 modelIns.put("etcCost"  	, etcCost);
                 modelIns.put("etcCostRsn"  , etcCostRsn);
                 modelIns.put("payType"  	, payType);
                 modelIns.put("desc1"  		, desc1);
                 modelIns.put("prevAddCost"  		, prevAddCost);
                 
                 // session 및 등록정보
                 modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                 modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                 modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	              
//                 System.out.println("LC_ID : " + (String)model.get(ConstantIF.SS_SVC_NO));
//                 System.out.println("USER_NO : " + (String)model.get(ConstantIF.SS_USER_NO));
//                 System.out.println("WORK_IP : " + (String)model.get(ConstantIF.SS_CLIENT_IP));
                 
                 // dao
                 modelIns = (Map<String, Object>)dao.saveAddCost(modelIns);
                 ServiceUtil.isValidReturnCode("WMSAC235", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));      
                 
        	 }
               /* if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
               
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                } */
	                
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
        	 m.put("errCnt", -1);
             m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : UOM목록 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀업로드 저장
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.saveUploadData(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
}
