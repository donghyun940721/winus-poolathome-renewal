package com.logisall.winus.wmsac.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC235Dao")
public class WMSAC235Dao extends SqlMapAbstractDAO {

	
	/**
	 * Method ID : saveAddCost
	 * Method 설명 : 추가 운임비 프로시저 
	 * 작성자 : yhku 
	 * 
	 * @param model
	 * @return
	 */
	public Object saveAddCost(Map<String, Object> model) {
		//executeUpdate("wmsac235.pk_wmsif106.sp_homecc_dlv_add_cost", model);
		executeUpdate("wmsac235.pk_wmsif106.sp_homecc_dlv_add_cost_v2", model);
		return model;
	}
	
	
	/**
	 * Method ID : list 
	 * Method 설명 : UOM목록조회 
	 * 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
//		return executeQueryPageWq("wmsac235.outListByKccCost", model);
		return executeQueryPageWq("wmsac235.outListByKccCostV2", model);
	}

	/**
	 * Method ID : insert 
	 * Method 설명 : UOM코드 등록 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmsms100.insert", model);
	}

	/**
	 * Method ID : update 
	 * Method 설명 : UOM코드 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmsms100.update", model);
	}

	/**
	 * Method ID : delete 
	 * Method 설명 : UOM코드 삭제 (DEL_YN 를 Y로 수정) 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmsms100.delete", model);
	}

	/**
	 * Method ID : saveUploadData 
	 * Method 설명 : UOM정보 대용량등록시 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if ((paramMap.get("UOM_NM") != null && StringUtils.isNotEmpty(paramMap.get("UOM_NM").toString())) && (paramMap.get("UOM_CD") != null && StringUtils.isNotEmpty(paramMap.get("UOM_CD").toString()))
						&& (paramMap.get("LC_ID") != null && StringUtils.isNotEmpty(paramMap.get("LC_ID").toString()))) {

					sqlMapClient.insert("wmsms100.insert_many", paramMap);

				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }    
}
