package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC010Dao")
public class WMSAC010Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

	/**
     * Method ID  : wmsac010E4.selectItemGrp
     * Method 설명  : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID  : listE4
     * Method 설명  : 상품조회리스트
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listE4(Map<String, Object> model) {
        return executeQueryPageWq("wmsac010E4.listItemPop", model);
    }
    
    /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsac010.list", model);
    }
    
    /**
     * Method ID    : listSubDetail
     * Method 설명      : 청구단가계약 상단조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object listSubDetail(Map<String, Object> model) {
        return executeView("wmsac010.listSubDetail", model);
    }

    /**
    * Method ID    : listSub
    * Method 설명      : 청구단가계약 그리드조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmsac011.listSub", model);
    }
    
    /**
     * Method ID    : saveSub
     * Method 설명      : 청구단가계약 등록
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSub(Map<String, Object> model){
        executeUpdate("wmsac010.pk_wmsac010.sp_save_cont", model);
        return model;
    }
    
    /**
     * Method ID    : deleteSub
     * Method 설명      : 청구단가계약 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deleteSub(Map<String, Object> model){
        executeUpdate("wmsac010.pk_wmsac010.sp_del_cont_detail", model);
        return model;
    }
    
    /**
    * Method ID    : listAccDt
    * Method 설명      : 거래처정산일설정 조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet listAccDt(Map<String, Object> model) {
        return executeQueryPageWq("wmsac012.list", model);
    }   
    
    /**
     * Method ID    : saveAccday
     * Method 설명      : 거래처정산일설정 등록
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveAccday(Map<String, Object> model){
        executeUpdate("wmsac010.pk_wmsac010.sp_save_accday", model);
        return model;
    }
    
    /**
     * Method ID    : deleteAccday
     * Method 설명      : 거래처정산일설정 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object deleteAccday(Map<String, Object> model){
        executeUpdate("wmsac010.pk_wmsac010.sp_del_accday", model);
        return model;
    }
    
}
