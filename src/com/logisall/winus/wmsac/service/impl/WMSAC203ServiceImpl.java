package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC203Service;
import com.logisall.winus.wmsms.service.impl.WMSMS090Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC203Service")
public class WMSAC203ServiceImpl extends AbstractServiceImpl implements WMSAC203Service {
    
    @Resource(name = "WMSAC203Dao")
    private WMSAC203Dao dao;
    
    @Resource(name = "WMSMS090Dao")
	private WMSMS090Dao dao2;


    /**
     * Method ID : list
     * Method 설명 : 인건비내역 조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.list(model));            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : detail
     * Method 설명 : 운송내역서 조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.detail(model));            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : listExcel
     * Method 설명 : 입별재고 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : detailExcel
     * Method 설명 : 입별재고 상세엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detailExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.detail(model));
        
        return map;
    }
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            map.put("POOLGRP", dao.selectPool(model));
            model.put("inKey", "ORD01");
            map.put("ORD01", dao.selectOrd01(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : save
     * Method 설명 : 인건비 내역 저장 박스 조회 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> save(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {				
			
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();		
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("CUST_ID", model.get("CUST_ID" + i));
				modelDt.put("LABOR_COSTS_SEQ", model.get("LABOR_COSTS_SEQ" + i));
				
				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {					
					modelDt.put("ACC_DT", model.get("ACC_DT" + i));
					modelDt.put("TRANS_CUST_ID", model.get("TRANS_CUST_ID" + i));
					modelDt.put("WORK_CONTENTS", model.get("WORK_CONTENTS" + i));
					modelDt.put("UNIT_COST_MAN", model.get("UNIT_COST_MAN" + i));
					modelDt.put("UNIT_COST_TIME", model.get("UNIT_COST_TIME" + i));
					modelDt.put("USER_CNT", model.get("USER_CNT" + i));
					modelDt.put("WORKING_TIME_HOUR", model.get("WORKING_TIME_HOUR" + i));
					modelDt.put("COST", model.get("COST" + i));
					modelDt.put("ADDITIONAL_COST", model.get("ADDITIONAL_COST" + i));
					modelDt.put("ETC", model.get("ETC" + i));					
					modelDt.put("ACC_TYPE", model.get("ACC_TYPE" + i));
					modelDt.put("COST_VOLUMN", model.get("COST_VOLUMN" + i));
					modelDt.put("COST_CONTRACT", model.get("COST_CONTRACT" + i));
					modelDt.put("ACC_UNIT", model.get("ACC_UNIT" + i));															
					modelDt.put("RATE", model.get("RATE" + i));															
				}

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert(modelDt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {					
					dao.update(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					dao.delete(modelDt);
					
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			
			if ("DELETE".equals(model.get("ST_GUBUN" + 0))){				
				m.put("MSG", MessageResolver.getMessage("delete.success"));
			} else {				
				m.put("MSG", MessageResolver.getMessage("save.success"));
			}
			
			//m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	@Override
	public Map<String, Object> save2(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();			
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("CUST_ID", model.get("CUST_ID" + i));
				
				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {			
										
					modelDt.put("DPRT_DT", model.get("DPRT_DT" + i));
					modelDt.put("TRANSPORT_COSTS_SEQ", model.get("TRANSPORT_COSTS_SEQ" + i));
					modelDt.put("ACC_TYPE", model.get("ACC_TYPE" + i));
					modelDt.put("COST_VOLUMN", model.get("COST_VOLUMN" + i));
					modelDt.put("COST_CONTRACT", model.get("COST_CONTRACT" + i));
					modelDt.put("ACC_UNIT", model.get("ACC_UNIT" + i));
					modelDt.put("QTY", model.get("QTY" + i));
					System.out.println(model.get("QTY" + i));
					modelDt.put("COST", model.get("COST" + i));
					System.out.println(model.get("COST" + i));
					modelDt.put("RATE", model.get("RATE" + i));
					System.out.println(model.get("RATE" + i));
					
				} else {
					modelDt.put("TRANSPORT_COSTS_SEQ", model.get("TRANSPORT_COSTS_SEQ" + i));					
				}				

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert2(modelDt);					
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {					
					dao.update2(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {					
					dao.delete2(modelDt);					
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			
			m.put("errCnt", errCnt);		
			
			if ("DELETE".equals(model.get("ST_GUBUN" + 0))){				
				m.put("MSG", MessageResolver.getMessage("delete.success"));
			} else {				
				m.put("MSG", MessageResolver.getMessage("save.success"));
			}
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}

	@Override
	public Map<String, Object> save3(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();				
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("SUB_MATLS_COSTS_SEQ", model.get("SUB_MATLS_COSTS_SEQ" + i));
				modelDt.put("CUST_ID", model.get("CUST_ID" + i));
				
				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {
					
					modelDt.put("ITEM_GRP_ID", model.get("ITEM_GRP_ID" + i));
					modelDt.put("RITEM_ID", model.get("RITEM_ID" + i));
					modelDt.put("ACC_DT", model.get("ACC_DT" + i));
					modelDt.put("BEFORE_STOCK_QTY", model.get("BEFORE_STOCK_QTY" + i));
					modelDt.put("IN_QTY", model.get("IN_QTY" + i));
					modelDt.put("USE_QTY", model.get("USE_QTY" + i));
					modelDt.put("STOCK_QTY", model.get("STOCK_QTY" + i));
					modelDt.put("UNIT_COST", model.get("UNIT_COST" + i));
					modelDt.put("COST", model.get("COST" + i));
					modelDt.put("ETC", model.get("ETC" + i));					
					
					modelDt.put("ACC_TYPE", model.get("ACC_TYPE" + i));
					modelDt.put("COST_VOLUMN", model.get("COST_VOLUMN" + i));
					modelDt.put("COST_CONTRACT", model.get("COST_CONTRACT" + i));
					modelDt.put("ACC_UNIT", model.get("ACC_UNIT" + i));					
				}

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert3(modelDt);
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {					
					dao.update3(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					dao.delete3(modelDt);
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			m.put("errCnt", errCnt);
			//m.put("MSG", MessageResolver.getMessage("save.success"));
			if ("DELETE".equals(model.get("ST_GUBUN" + 0))){				
				m.put("MSG", MessageResolver.getMessage("delete.success"));
			} else {				
				m.put("MSG", MessageResolver.getMessage("save.success"));
			}

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
     * Method ID : list0
     * Method 설명 : 보관비내역  조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list0(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
            
            int intCnt = dao.ChkWMSAC203Cnt(model);
            
            if (intCnt > 0)
            	map.put("LIST", dao.wmsac203_list0(model));
            else
            	map.put("LIST", dao.list0(model));
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : save7
     * Method 설명 : 관리비내역 , 입출고비내역, 장비사용내역비, 부가서비스, 공제 내역 저장(공통)
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */	
	@Override
	public Map<String, Object> save7(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();			
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("CUST_ID",  model.get("CUST_ID" + i));
				modelDt.put("ACC_SEQ", model.get("ACC_SEQ" + i));
				
				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {										
					
					modelDt.put("ACC_TYPE", model.get("ACC_TYPE" + i));
					modelDt.put("ACC_DT", model.get("ACC_DT" + i));
					modelDt.put("QTY", model.get("QTY" + i));
					modelDt.put("ETC", model.get("ETC" + i));
					modelDt.put("COST_VOLUMN", model.get("COST_VOLUMN" + i));
					modelDt.put("COST_CONTRACT", model.get("COST_CONTRACT" + i));
					modelDt.put("ACC_UNIT", model.get("ACC_UNIT" + i));
					modelDt.put("COST", model.get("COST" + i));
					modelDt.put("SERIAL_NO", model.get("SERIAL_NO" + i));
					modelDt.put("WORK_TYPE", model.get("WORK_TYPE" + i));
					modelDt.put("RATE", model.get("RATE" + i));
				} 				

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert7(modelDt);					
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.update7(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {					
					dao.delete7(modelDt);					
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			
			m.put("errCnt", errCnt);		
			
			if ("DELETE".equals(model.get("ST_GUBUN" + 0))){				
				m.put("MSG", MessageResolver.getMessage("delete.success"));
			} else {				
				m.put("MSG", MessageResolver.getMessage("save.success"));
			}
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
     * Method ID : store
     * Method 설명 : 입출고비내역  조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> store(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.store(model));            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : save0
     * Method 설명 :  보관비 저장(이관)
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> save0(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		
		int intCnt = dao.ChkWMSAC203Cnt(model);
		
		//이미 wmsac203에 데이터가 있으면 저장 불가
		if (intCnt > 0){
			map.put("MSG", "이미 데이터가 존재하여 저장할 수 없습니다.");
		} else {		
			
			//session 정보
			model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));		
			
			dao.moveToWMSAC203(model);
			
			map.put("MSG", "저장(이관) 되었습니다.");
		}
		
		return map;
	}
	
	/**
     * Method ID : chkCnt
     * Method 설명 : wmsac203 테이블 Count 체크
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> chkCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();		
		
		int intCnt = dao.ChkCntRow(model);		
		map.put("cnt", ""+intCnt);
		
		return map;
	}
	
	/**
     * Method ID : update12
     * Method 설명 : 보관비 수량 수정
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> update12(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
        try{               
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();                	
            	
            //main
            modelIns.put("I_LC_ID",      (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_CUST_ID",    (String)model.get("CustId"));
            modelIns.put("I_WORK_TYPE",  (String)model.get("IOtype"));
            modelIns.put("I_WORK_MONTH", (String)model.get("AccYYMM"));
            modelIns.put("I_WORK_DATE",  (String)model.get("Date"));
            modelIns.put("I_RITEM_ID",   (String)model.get("RitemId"));          
            modelIns.put("I_WORK_QTY",   (String)model.get("vrToCnt"));            
            
            modelIns.put("I_REASON", 	 (String)model.get("vrEtc"));
            modelIns.put("I_USER_NO", 	 (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("I_WORK_IP", 	 (String)model.get(ConstantIF.SS_CLIENT_IP));                                    

            //dao                
            modelIns = (Map<String, Object>)dao.update12(modelIns);
            ServiceUtil.isValidReturnCode("WMSAC203", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));           
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );             
            
        } catch(Exception e){
            throw e;
        }
        return m;
	}

	@Override
	public Map<String, Object> chkDataCnt(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();		
		
		model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
		map.put("LIST", dao.chkDataCnt(model));	
		
		return map;
	}
	
	@Override
	public Map<String, Object> save14(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		
		int intCnt = dao.ChkWMSAC202Cnt(model);
		
		//이미 wmsac203에 데이터가 있으면 저장 불가
		if (intCnt > 0){
			map.put("MSG", "해당 정산년월 데이터가 존재하여 저장할 수 없습니다.");
		} else {		
			
			//session 정보
			model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
			model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
			model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));		
			
			dao.moveToWMSAC202(model);
			
			map.put("MSG", "저장(이관) 되었습니다.");
		}
		
		return map;
	}

	@Override
	public Map<String, Object> list14(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list14(model));
        return map;
	}

	@Override
	public Map<String, Object> list0Excel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("pageIndex", "1");
        model.put("pageSize", "60000");
            
        int intCnt = dao.ChkWMSAC203Cnt(model);
        
        if (intCnt > 0)
        	map.put("LIST", dao.wmsac203_list0(model));
        else
        	map.put("LIST", dao.list0(model));	
		
        return map;
	}
	
	/**
     * Method ID : list15
     * Method 설명 : 반품내역 조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list15(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list15(model));
        return map;
	}
	
	/**
     * Method ID : update14
     * Method 설명 : 도선료, 제주운임, 기타운임 수정
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> update14(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();				
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("SEQ", 	    model.get("SEQ" + i));
				modelDt.put("CUST_ID",  model.get("CUST_ID" + i));			
				
				modelDt.put("SHIPMENT_FRT_COST", model.get("SHIPMENT_FRT_COST" + i));
				modelDt.put("DLV_JEJU_FRT_COST", model.get("DLV_JEJU_FRT_COST" + i));
				modelDt.put("ETC_FRT_COST", model.get("ETC_FRT_COST" + i));
				
				dao.update14(modelDt);				
			}
			
			m.put("errCnt", errCnt);								
			m.put("MSG", MessageResolver.getMessage("save.success"));
			

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
     * Method ID : save15
     * Method 설명 : 반품내역 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> save15(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();			
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("CUST_ID",  model.get("CUST_ID" + i));
				modelDt.put("SEQ", model.get("SEQ" + i));
				
				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {					
					modelDt.put("DLV_DT", model.get("DLV_DT" + i));
					modelDt.put("DLV_IN_DT", model.get("DLV_IN_DT" + i));
					modelDt.put("DTDS_COMPANY_NM", model.get("DTDS_COMPANY_NM" + i));
					modelDt.put("INVC_NO", model.get("INVC_NO" + i));
					modelDt.put("RCVR_NM", model.get("RCVR_NM" + i));
					modelDt.put("RCVR_ADDR", model.get("RCVR_ADDR" + i));
					modelDt.put("GDS_CD", model.get("GDS_CD" + i));
					modelDt.put("GDS_NM", model.get("GDS_NM" + i));
					modelDt.put("GDS_QTY", model.get("GDS_QTY" + i));
					modelDt.put("DLV_BAD_TYPE", model.get("DLV_BAD_TYPE" + i));
					modelDt.put("ETC", model.get("ETC" + i));					
				} 				

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert15(modelDt);					
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.update15(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {					
					dao.delete15(modelDt);					
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			
			m.put("errCnt", errCnt);		
			
			if ("DELETE".equals(model.get("ST_GUBUN" + 0))){				
				m.put("MSG", MessageResolver.getMessage("delete.success"));
			} else {				
				m.put("MSG", MessageResolver.getMessage("save.success"));
			}
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
     * Method ID : list16
     * Method 설명 : 합포장내역 조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list16(Map<String, Object> model) throws Exception {		
		
		Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
        
        int intCnt = dao.ChkWMSAC205Cnt(model);
        
        if (intCnt > 0)
        	map.put("LIST", dao.wmsac205_list16(model));
        else
        	map.put("LIST", dao.list16(model));
        
        return map;
	}
	
	/**
     * Method ID : save16
     * Method 설명 : 반품내역 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> save16(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> map = new HashMap<String, Object>();
		
		//int intCnt = dao.ChkWMSAC207Cnt(model);
		
		//이미 wmsac203에 데이터가 있으면 저장 불가
		//if (intCnt > 0){
		//	map.put("MSG", "해당 정산년월 데이터가 존재하여 저장할 수 없습니다.");
		//} else {		
			
		//session 정보
		model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));		
		
		dao.delete205_pack(model);
		dao.moveToWMSAC205(model);
		
		map.put("CODE", "SUCCESS");
		map.put("MSG", "저장(이관) 되었습니다.");			
		//}
		
		return map;
	}	
	
	/**
     * Method ID : UpdateFileInfo
     * Method 설명 : 이미지 파일 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> updateFileInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
				Map<String, Object> modelDt = new HashMap<String, Object>();
				// 확장자 잘라내기
				String str = (String) model.get("D_ATCH_FILE_NAME");
				String ext = "." + str.substring((str.lastIndexOf('.') + 1));
				// 파일 ID 만들기
				int fileSeq = 1;
				String fileId = "";
				fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
				// 저장준비
				modelDt.put("FILE_ID", fileId);
				modelDt.put("ATTACH_GB", "NAMS"); // 통합 HELPDESK 업로드
				modelDt.put("FILE_VALUE", "wmsac203"); // 하드코딩되어 있음 파일구분값?
				modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE")); // 서버저장경로
				modelDt.put("ORG_FILENAME", model.get("HD_FILE_NUM")); // 파일명
				modelDt.put("FILE_EXT", ext); // 파일 확장자
				modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));// 파일 사이즈
				// 저장
				dao.fileUpload(modelDt);
				// 리턴값 파일 id랑 파일경로
				model.put("IMAGE_ID", fileId);
				model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));
				
				String Gubun = "";
				Gubun = (String)model.get("HD_OPENER");
				
				System.err.println("Gubun : " + Gubun);				
				
				//인건비
				if ("list".equals(Gubun))
					dao.insertInfo_201(model);
				else if ("detail".equals(Gubun))
					dao.insertInfo_202(model);
				else if ("listPool".equals(Gubun))
					dao.insertInfo_203(model);
				else
					dao.insertInfo(model);
			}

			map.put("MSG", MessageResolver.getMessage("insert.success"));
		} catch (Exception e) {
			throw e;
		}
		return map;
	}
	
	/**
     * Method ID : saveReport
     * Method 설명 : 이미지 파일 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> saveReport(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
        try{       	
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();                	
            	
            //main
            modelIns.put("I_ACC_YY_MM", (String)model.get("ACC_YY_MM"));
            modelIns.put("I_LC_ID",      (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_CUST_ID",    (String)model.get("CUST_ID"));            
            modelIns.put("I_USER_NO", 	 (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("I_WORK_IP", 	 (String)model.get(ConstantIF.SS_CLIENT_IP));                                    

            //dao                
            modelIns = (Map<String, Object>)dao.saveReport(modelIns);
            ServiceUtil.isValidReturnCode("WMSAC203", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                      
                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );             
            
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * Method ID : chkReportCnt
     * Method 설명 : 거래명세서 저장 이력 테이블 Count 체크
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> chkReportCnt(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();		
		
		int intCnt = dao.chkReportCnt(model);		
		map.put("report_cnt", ""+intCnt);
		
		return map;
	}
	
	/**
     * Method ID : saveExcelInfo
     * Method 설명 : 엑셀읽기저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfoE2T1(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	int insertCnt = (list != null)?list.size():0;
            if(insertCnt > 0){
            	
                Map<String, Object> paramMap = null;
                for (int i=0;i<list.size();i++) {
                	Map<String, Object> modelDt = new HashMap<String, Object>();
            		paramMap = (Map)list.get(i);
            		modelDt.put("DPRT_DT"			, (String)paramMap.get("O_DPRT_DT"));
            		modelDt.put("ARRV_DT"			, (String)paramMap.get("O_ARRV_DT"));
            		modelDt.put("ACC_TYPE"			, (String)paramMap.get("O_ACC_TYPE"));
            		modelDt.put("COST_VOLUMN"		, (String)paramMap.get("O_COST_VOLUMN"));
					modelDt.put("COST_CONTRACT"		, (String)paramMap.get("O_COST_CONTRACT"));
					modelDt.put("ACC_UNIT"			, (String)paramMap.get("O_ACC_UNIT"));
					modelDt.put("DLV_DEPARTURE_A"	, (String)paramMap.get("O_DLV_DEPARTURE_A"));
					modelDt.put("DLV_DESTINATION_A"	, (String)paramMap.get("O_DLV_DESTINATION_A"));
					modelDt.put("DLV_DEPARTURE_B"	, (String)paramMap.get("O_DLV_DEPARTURE_B"));
					modelDt.put("DLV_DESTINATION_B"	, (String)paramMap.get("O_DLV_DESTINATION_B"));
					modelDt.put("CAR_NUM"			, (String)paramMap.get("O_CAR_NUM"));
					modelDt.put("RTI_CNT"			, (String)paramMap.get("O_RTI_CNT"));
					modelDt.put("CAR_TON"			, (String)paramMap.get("O_CAR_TON"));
					modelDt.put("UNIT_COST"			, (String)paramMap.get("O_UNIT_COST"));
					modelDt.put("QTY"				, (String)paramMap.get("O_QTY"));
					modelDt.put("ADDITIONAL_COST"	, (String)paramMap.get("O_ADDITIONAL_COST"));
					modelDt.put("ETC"				, (String)paramMap.get("O_ETC"));
					
					modelDt.put("CUST_ID"			, (String)model.get("custId"));
					modelDt.put("LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
					modelDt.put("WORK_IP"			, (String)model.get(ConstantIF.SS_CLIENT_IP));
					modelDt.put("USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
					
					System.out.println("DPRT_DT"			+" : " + (String)paramMap.get("O_DPRT_DT"));
					System.out.println("ARRV_DT"			+" : " + (String)paramMap.get("O_ARRV_DT"));
					System.out.println("ACC_TYPE"			+" : " + (String)paramMap.get("O_ACC_TYPE"));
					System.out.println("COST_VOLUMN"		+" : " + (String)paramMap.get("O_COST_VOLUMN"));
					System.out.println("COST_CONTRACT"		+" : " + (String)paramMap.get("O_COST_CONTRACT"));
					System.out.println("ACC_UNIT"			+" : " + (String)paramMap.get("O_ACC_UNIT"));
					System.out.println("DLV_DEPARTURE_A"	+" : " + (String)paramMap.get("O_DLV_DEPARTURE_A"));
					System.out.println("DLV_DESTINATION_A"	+" : " + (String)paramMap.get("O_DLV_DESTINATION_A"));
					System.out.println("DLV_DEPARTURE_B"	+" : " + (String)paramMap.get("O_DLV_DEPARTURE_B"));
					System.out.println("DLV_DESTINATION_B"	+" : " + (String)paramMap.get("O_DLV_DESTINATION_B"));
					System.out.println("CAR_NUM"			+" : " + (String)paramMap.get("O_CAR_NUM"));
					System.out.println("RTI_CNT"			+" : " + (String)paramMap.get("O_RTI_CNT"));
					System.out.println("CAR_TON"			+" : " + (String)paramMap.get("O_CAR_TON"));
					System.out.println("UNIT_COST"			+" : " + (String)paramMap.get("O_UNIT_COST"));
					System.out.println("QTY"				+" : " + (String)paramMap.get("O_QTY"));
					System.out.println("ADDITIONAL_COST"	+" : " + (String)paramMap.get("O_ADDITIONAL_COST"));
					System.out.println("ETC"				+" : " + (String)paramMap.get("O_ETC"));
					  
					System.out.println("CUST_ID"			+" : " + (String)model.get("custId"));
					System.out.println("LC_ID"				+" : " + (String)model.get(ConstantIF.SS_SVC_NO));
					System.out.println("WORK_IP"			+" : " + (String)model.get(ConstantIF.SS_CLIENT_IP));
					System.out.println("USER_NO"			+" : " + (String)model.get(ConstantIF.SS_USER_NO));
					
					dao.insert2Excel(modelDt);
            	}
            }
        	
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", errCnt);
            
        } catch(Exception e){
            m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : saveTaxInfo
     * Method 설명 : 세금계산서 생성 정보 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> saveTaxInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
        try{       	
            //프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();                	
            	
            //main            
            modelIns.put("P_LC_ID",        (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("P_CUST_ID",      (String)model.get("CUST_ID"));
            modelIns.put("P_TAX_BILL_SEQ", (String)model.get("TAX_BILL_SEQ"));
            modelIns.put("P_DMD_YM",       (String)model.get("DMD_YM"));
            modelIns.put("P_ISSUE_DAT",    (String)model.get("ISSUE_DAT"));
            modelIns.put("P_ISSUE_EMP",    (String)model.get("ISSUE_EMP"));
            modelIns.put("P_REMARK",       (String)model.get("REMARK"));                                     

            //dao                
            modelIns = (Map<String, Object>)dao.saveTaxInfo(modelIns);
            ServiceUtil.isValidReturnCode("WMSAC203", String.valueOf(modelIns.get("P_RTN_COD")), (String)modelIns.get("P_RTN_MSG"));                      
            
            System.err.println(modelIns.get("P_RTN_COD"));
            System.err.println((String)modelIns.get("P_RTN_MSG"));
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );             
            
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * Method ID : selectTaxInfo
     * Method 설명 : 세금계산서 호출 정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> selectTaxInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        
		model.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
		model.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
		model.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
        
		int intHDCnt = dao.selectTaxHDCnt(model);		
		map.put("TAX_HD_CNT", ""+intHDCnt);
        map.put("LIST", dao.selectTaxInfo(model));        
        
        return map;
	}
	
	/**
     * Method ID : selectIssueEmp
     * Method 설명 : 세금계산서 발행자 사번 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> selectIssueEmp(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();		
		
		String strIssueEmp = dao.selectIssueEmp(model);		
		map.put("ISSUE_EMP", strIssueEmp);
		
		return map;
	}
	
	/**
     * Method ID : updatetaxInfo
     * Method 설명 : 세금계산서 발행 이력 정보 업데이트(성공)
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> updateTaxInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
        try {                                
        	
        	model.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
        	
            dao.updateTaxInfo(model);      
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );             
            
        } catch(Exception e){
            throw e;
        }
        return m;
	}

	/**
     * Method ID : chkTaxCnt
     * Method 설명 : 거래명세서 저장 이력 테이블 Count 체크
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> chkTaxCnt(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();		
		
		int intCnt = dao.chkTaxCnt(model);		
		map.put("tax_cnt", ""+intCnt);
		
		return map;
	}
	
	/**
     * Method ID : store
     * Method 설명 : 입출고비내역  조회
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> list18(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
            map.put("LIST", dao.list18(model));            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : save18
     * Method 설명 : 보관비내역(수기) 저장
     * 작성자 :  
     * @param model
     * @return
     * @throws Exception
     */	
	@Override
	public Map<String, Object> save18(Map<String, Object> model) throws Exception {
		// TODO Auto-generated method stub
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {		
			System.out.println(">>>>>>>>>>>>>>>>>>");
			System.out.println(model);
			System.out.println(model.toString());
			for (int i = 0; i < Integer.parseInt(model.get("selectIds").toString()); i++) {
				Map<String, Object> modelDt = new HashMap<String, Object>();			
				
				//session 정보
				modelDt.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
				modelDt.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
				modelDt.put("LC_ID",   (String)model.get(ConstantIF.SS_SVC_NO));
				modelDt.put("CUST_ID",  model.get("CUST_ID" + i));
//				modelDt.put("ACC_SEQ", model.get("ACC_SEQ" + i));
				
				if (!"DELETE".equals(model.get("ST_GUBUN" + i))) {										
					
					modelDt.put("ACC_TYPE", model.get("ACC_TYPE" + i));
					modelDt.put("ACC_DT", model.get("ACC_DT" + i));
					modelDt.put("STOCK_QTY", model.get("STOCK_QTY" + i));
					modelDt.put("COST_VOLUMN", model.get("COST_VOLUMN" + i));
					modelDt.put("COST_CONTRACT", model.get("COST_CONTRACT" + i));
					modelDt.put("ACC_UNIT", model.get("ACC_UNIT" + i));
					modelDt.put("COST", model.get("COST" + i));
					modelDt.put("RATE", model.get("RATE" + i));
					modelDt.put("RITEM_ID", model.get("RITEM_ID" + i));
				} 				

				if ("INSERT".equals(model.get("ST_GUBUN" + i))) {
					dao.insert18(modelDt);					
				} else if ("UPDATE".equals(model.get("ST_GUBUN" + i))) {
					dao.update18(modelDt);
				} else if ("DELETE".equals(model.get("ST_GUBUN" + i))) {
					modelDt.put("RITEM_ID", model.get("RITEM_ID" + i));
					modelDt.put("ACC_TYPE", model.get("ACC_TYPE" + i));
					modelDt.put("ACC_DT", model.get("ACC_DT" + i));
					dao.delete18(modelDt);					
				} else {
					errCnt++;
					m.put("errCnt", errCnt);
					throw new BizException(MessageResolver.getMessage("save.error"));
				}
			}
			
			m.put("errCnt", errCnt);		
			
			if ("DELETE".equals(model.get("ST_GUBUN" + 0))){				
				m.put("MSG", MessageResolver.getMessage("delete.success"));
			} else {				
				m.put("MSG", MessageResolver.getMessage("save.success"));
			}
		} catch (BizException be) {
			if (log.isInfoEnabled()) {
				log.info(be.getMessage());
			}
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
}