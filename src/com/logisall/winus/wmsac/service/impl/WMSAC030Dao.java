package com.logisall.winus.wmsac.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAC030Dao")
public class WMSAC030Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 청구작업 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsac030.list", model);
    }
    
    /**
     * Method ID    : update
     * Method 설명      : 청구작업 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmsac030.update", model);
    }  
  
    /**
     * Method ID    : saveSp
     * Method 설명      : 정산산출
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveSp(Map<String, Object> model){
        executeUpdate("wmsac030.pk_wmsac010.sp_calc_period_subul", model);
        return model;
    }
}
