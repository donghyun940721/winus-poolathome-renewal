package com.logisall.winus.wmsac.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsac.service.WMSAC016Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSAC016Service")
public class WMSAC016ServiceImpl extends AbstractServiceImpl implements WMSAC016Service {
    
    @Resource(name = "WMSAC016Dao")
    private WMSAC016Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSAC016 = { "CUST_ID", "RITEM_ID", "SALE_BUY_GBN", "CURRENCY_NAME" };
    private final static String[] CHECK_VALIDATE_WMSAC016_DEL = { "CUST_ID", "RITEM_ID", "SALE_BUY_GBN", "SITE_CONT_ID" };
    private final static String[] CHECK_VALIDATE_WMSAC016_ITEM = { "CUST_ID", "RITEM_ID", "SITE_CONT_ID" };
    private final static String[] CHECK_VALIDATE_WMSAC016_ITEM_DEL = { "CUST_ID", "RITEM_ID" };


    /**
     * Method ID : itemAcList
     * Method 설명 : 상품 정산 리스트
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> itemAcList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.itemAcList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * Method ID : acMstrList
     * Method 설명 : 정산 마스터 리스트
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> acMstrList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.acMstrList(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    
    /**
     * Method ID : savePAH
     * Method 설명 : 정산 마스터 등록/수정 저장 - 풀앳홈 간편
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> savePAH(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try {
    		
    		int iuCnt = Integer.parseInt(model.get("I_selectIds").toString());
    		if (iuCnt > 0) {
    			// 저장, 수정
    			String[] siteContId = new String[iuCnt]; // id
    			String[] ritemId = new String[iuCnt]; // 센터화주상품ID
    			String[] custId = new String[iuCnt]; // 화주ID
    			String[] saleBuyGbn = new String[iuCnt]; // 매출/매입구분
    			String[] unitPrice = new String[iuCnt]; 
    			
    			String[] applyFrDt = new String[iuCnt]; 
    			String[] applyToDt = new String[iuCnt]; 
    			String[] itemGrpNm = new String[iuCnt]; 
    			String[] payCd = new String[iuCnt]; 
    			String[] currencyName = new String[iuCnt]; 
    			String[] confYn = new String[iuCnt]; 
    			
    			// PAH 추가 
    			String[]  setLcId     = new String[iuCnt];      
    			String[]  perRtnRcv   = new String[iuCnt];      
    			String[]  perCngShp   = new String[iuCnt];      
    			String[]  perCngRcv   = new String[iuCnt];      
    			String[]  perTnsRcv   = new String[iuCnt];      
    			String[]  perTnsSet   = new String[iuCnt];      
    			String[]  perSitCan   = new String[iuCnt];      
    			String[]  perFstBad   = new String[iuCnt];      
    			String[]  perSrtCan   = new String[iuCnt];      
    			String[]  perFunk     = new String[iuCnt];      
    			
    			String lcId = ""; // 센터ID
    			String workIp = "";
    			String userNo = "";
    			
    			for (int i = 0; i < iuCnt; i++) {
    				
    				String ITEM_GRP_NM = "";
    				
    				siteContId[i] = (String) model.get("SITE_CONT_ID" + i);
    				ritemId[i] = (String) model.get("RITEM_ID" + i);
    				custId[i] = (String) model.get("CUST_ID" + i);
    				saleBuyGbn[i] = (String) model.get("SALE_BUY_GBN" + i);
    				unitPrice[i] = (String) model.get("UNIT_PRICE" + i);
    				
    				applyFrDt[i] = ((String) model.get("APPLY_FR_DT" + i)).replaceAll("-", "");
    				applyToDt[i] = ((String) model.get("APPLY_TO_DT" + i)).replaceAll("-", "");
    				itemGrpNm[i] = ITEM_GRP_NM;
    				payCd[i] = (String) model.get("PAY_CD" + i);
    				currencyName[i] = (String) model.get("CURRENCY_NAME" + i);
    				confYn[i] = (String) model.get("CONF_YN" + i);
    				
    				// PAH 추가 
    				setLcId[i]   = (String) model.get("SET_LC_ID"+i);
    				perRtnRcv[i] = checkNull((String)model.get("PER_RTN_RCV"+i));
    				perCngShp[i] = checkNull((String)model.get("PER_CNG_SHP"+i));
    				perCngRcv[i] = checkNull((String)model.get("PER_CNG_RCV"+i));
    				perTnsRcv[i] = checkNull((String)model.get("PER_TNS_RCV"+i));
    				perTnsSet[i] = checkNull((String)model.get("PER_TNS_SET"+i));
    				perSitCan[i] = checkNull((String)model.get("PER_SIT_CAN"+i));
    				perFstBad[i] = checkNull((String)model.get("PER_FST_BAD"+i));
    				perSrtCan[i] = checkNull((String)model.get("PER_SRT_CAN"+i));
    				perFunk[i]   = checkNull((String)model.get("PER_FUNK"+i));
    			}
    			
    			lcId = (String) model.get("LC_ID");
    			workIp = (String) model.get(ConstantIF.SS_CLIENT_IP);
    			userNo = (String) model.get(ConstantIF.SS_USER_NO);
    			
    			Map<String, Object> modelIu = new HashMap<String, Object>();
    			modelIu.put("LC_ID"				, lcId); 
    			modelIu.put("SITE_CONT_ID"		, siteContId); 
    			modelIu.put("RITEM_ID"			, ritemId); 
    			modelIu.put("CUST_ID"			, custId); 
    			modelIu.put("SALE_BUY_GBN"		, saleBuyGbn); 
    			
    			modelIu.put("UNIT_PRICE"		, unitPrice); 
    			modelIu.put("APPLY_FR_DT"		, applyFrDt); 
    			modelIu.put("APPLY_TO_DT"		, applyToDt); 
    			modelIu.put("ITEM_GRP_NM"		, itemGrpNm); 
    			modelIu.put("PAY_CD"			, payCd); 
    			modelIu.put("CURRENCY_NAME"		, currencyName); 
    			modelIu.put("CONF_YN"			, confYn); 
    			
    			// PAH 추가 
    			modelIu.put("SET_LC_ID"   , setLcId);
    			modelIu.put("PER_RTN_RCV" , perRtnRcv);
    			modelIu.put("PER_CNG_SHP" , perCngShp);
    			modelIu.put("PER_CNG_RCV" , perCngRcv);
    			modelIu.put("PER_TNS_RCV" , perTnsRcv);
    			modelIu.put("PER_TNS_SET" , perTnsSet);
    			modelIu.put("PER_SIT_CAN" , perSitCan);
    			modelIu.put("PER_FST_BAD" , perFstBad);
    			modelIu.put("PER_SRT_CAN" , perSrtCan);
    			modelIu.put("PER_FUNK"    , perFunk);
    			
    			modelIu.put("WORK_IP", workIp);
    			modelIu.put("USER_NO", userNo);
    			
    			ServiceUtil.checkInputValidation2(modelIu, CHECK_VALIDATE_WMSAC016);
				dao.savePAH(modelIu);
    			ServiceUtil.isValidReturnCode("WMSAC016", String.valueOf(modelIu.get("O_MSG_CODE")),(String) modelIu.get("O_MSG_NAME"));
    			
    		}
    		// 등록 수정 끝
    		
    		m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch (BizException be) {
    		if (log.isInfoEnabled()) {
    			log.info(be.getMessage());
    		}
    		m.put("errCnt", 1);
    		m.put("MSG", be.getMessage());
    		
    	} catch (Exception e) {
    		throw e;
    	}
    	return m;
    }
    /**
	 * Method ID : save
     * Method 설명 : 정산 마스터 등록/수정 저장
     * 작성자 : yhku
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> save(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {

			int iuCnt = Integer.parseInt(model.get("I_selectIds").toString());
			if (iuCnt > 0) {
				// 저장, 수정
				String[] siteContId = new String[iuCnt]; // id
				String[] ritemId = new String[iuCnt]; // 센터화주상품ID
				String[] custId = new String[iuCnt]; // 화주ID

				String[] saleBuyGbn = new String[iuCnt]; // 매출/매입구분
				String[] unitPrice = new String[iuCnt]; 
				String[] inoutCost = new String[iuCnt]; 
				String[] stockCost = new String[iuCnt]; 
				String[] applyFrDt = new String[iuCnt]; 
				String[] applyToDt = new String[iuCnt]; 

				String[] itemGrpNm = new String[iuCnt]; 
				String[] accUnit = new String[iuCnt]; 
				String[] payCd = new String[iuCnt]; 
				String[] payDay = new String[iuCnt]; 
				String[] closingDay = new String[iuCnt]; 
				String[] accStd = new String[iuCnt]; 
				String[] roundCd = new String[iuCnt]; 
				String[] endCutCd = new String[iuCnt]; 
				String[] currencyName = new String[iuCnt]; 

				String[] confYn = new String[iuCnt]; 
				String[] realDeptId = new String[iuCnt]; 
				String[] realEmployeeId = new String[iuCnt]; 
				String[] contDeptId = new String[iuCnt]; 
				String[] contEmployeeId = new String[iuCnt]; 

				String[] collectEmployeeId = new String[iuCnt]; 
				String[] approvalEmployeeId = new String[iuCnt]; 
				String[] groupComEmpId = new String[iuCnt]; 
				String[] transCustId = new String[iuCnt]; 
				
				String lcId = ""; // 센터ID
				String workIp = "";
				String userNo = "";

				for (int i = 0; i < iuCnt; i++) {
					
					String ITEM_GRP_NM = "";
					
					siteContId[i] = (String) model.get("SITE_CONT_ID" + i);
					ritemId[i] = (String) model.get("RITEM_ID" + i);
					custId[i] = (String) model.get("CUST_ID" + i);
					saleBuyGbn[i] = (String) model.get("SALE_BUY_GBN" + i);

					unitPrice[i] = (String) model.get("UNIT_PRICE" + i);
					inoutCost[i] = (String) model.get("INOUT_COST" + i);
					stockCost[i] = (String) model.get("STOCK_COST" + i);
					applyFrDt[i] = ((String) model.get("APPLY_FR_DT" + i)).replaceAll("-", "");
					applyToDt[i] = ((String) model.get("APPLY_TO_DT" + i)).replaceAll("-", "");

					itemGrpNm[i] = ITEM_GRP_NM;
					accUnit[i] = (String) model.get("ACC_UNIT" + i);
					payCd[i] = (String) model.get("PAY_CD" + i);
					payDay[i] = (String) model.get("PAY_DAY" + i);
					closingDay[i] = (String) model.get("CLOSING_DAY" + i);
					accStd[i] = (String) model.get("ACC_STD" + i);
					roundCd[i] = (String) model.get("ROUND_CD" + i);
					endCutCd[i] = (String) model.get("END_CUT_CD" + i);
					currencyName[i] = (String) model.get("CURRENCY_NAME" + i);

					confYn[i] = (String) model.get("CONF_YN" + i);
					realDeptId[i] = (String) model.get("REAL_DEPT_ID" + i);
					realEmployeeId[i] = (String) model.get("REAL_EMPLOYEE_ID" + i);
					contDeptId[i] = (String) model.get("CONT_DEPT_ID" + i);
					contEmployeeId[i] = (String) model.get("CONT_EMPLOYEE_ID" + i);
 
					collectEmployeeId[i] = (String) model.get("COLLECT_EMPLOYEE_ID" + i);
					approvalEmployeeId[i] = (String) model.get("APPROVAL_EMP_ID" + i);
					groupComEmpId[i] = (String) model.get("GROUP_COM_EMP_ID" + i);
					transCustId[i] = (String) model.get("TRANS_CUST_ID" + i);

				}

				lcId = (String) model.get("LC_ID");
				workIp = (String) model.get(ConstantIF.SS_CLIENT_IP);
				userNo = (String) model.get(ConstantIF.SS_USER_NO);

				Map<String, Object> modelIu = new HashMap<String, Object>();
				modelIu.put("LC_ID"				, lcId); 
				modelIu.put("SITE_CONT_ID"		, siteContId); 
				modelIu.put("RITEM_ID"			, ritemId); 
				modelIu.put("CUST_ID"			, custId); 
				modelIu.put("SALE_BUY_GBN"		, saleBuyGbn); 
				
				modelIu.put("UNIT_PRICE"		, unitPrice); 
				modelIu.put("INOUT_COST"		, inoutCost); 
				modelIu.put("STOCK_COST"		, stockCost); 
				modelIu.put("APPLY_FR_DT"		, applyFrDt); 
				modelIu.put("APPLY_TO_DT"		, applyToDt); 
				
				modelIu.put("ITEM_GRP_NM"		, itemGrpNm); 
				modelIu.put("PAY_CD"			, payCd); 
				modelIu.put("PAY_DAY"			, payDay); 
				modelIu.put("CLOSING_DAY"		, closingDay); 
				modelIu.put("ACC_STD"			, accStd); 
				modelIu.put("ROUND_CD"			, roundCd); 
				modelIu.put("END_CUT_CD"		, endCutCd); 
				
				modelIu.put("CURRENCY_NAME"		, currencyName); 
				modelIu.put("CONF_YN"			, confYn); 
				modelIu.put("REAL_DEPT_ID"		, realDeptId); 
				modelIu.put("REAL_EMPLOYEE_ID"	, realEmployeeId); 
				modelIu.put("CONT_DEPT_ID"		, contDeptId); 
				modelIu.put("CONT_EMPLOYEE_ID"	, contEmployeeId); 
				
				modelIu.put("COLLECT_EMPLOYEE_ID", collectEmployeeId); 
				modelIu.put("APPROVAL_EMP_ID"	, approvalEmployeeId); 
				modelIu.put("GROUP_COM_EMP_ID"	, groupComEmpId); 
				modelIu.put("TRANS_CUST_ID"		, transCustId); 
				modelIu.put("ACC_UNIT"			, accUnit); 

				modelIu.put("WORK_IP", workIp);
				modelIu.put("USER_NO", userNo);
				
				ServiceUtil.checkInputValidation2(modelIu, CHECK_VALIDATE_WMSAC016);
				// dao
				dao.save(modelIu);
				ServiceUtil.isValidReturnCode("WMSAC016", String.valueOf(modelIu.get("O_MSG_CODE")),(String) modelIu.get("O_MSG_NAME"));

			}
			// 등록 수정 끝

			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	
	/**
	 * Method ID : delete
     * Method 설명 : 정산 마스터 삭제
     * 작성자 : yhku
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> delete(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {

			// 삭제
			int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
			if (delCnt > 0) {
				String[] siteContId = new String[delCnt]; // id
				String[] ritemId = new String[delCnt]; // 센터화주상품ID
				String[] custId = new String[delCnt]; // 화주ID
				String[] saleBuyGbn = new String[delCnt]; // 매출/매입구분
				
				String lcId = ""; // 센터ID
				String workIp = "";
				String userNo = "";
				
				for (int i = 0; i < delCnt; i++) {
					siteContId[i] = (String) model.get("SITE_CONT_ID" + i);
					ritemId[i] = (String) model.get("RITEM_ID" + i);
					custId[i] = (String) model.get("CUST_ID" + i);
					saleBuyGbn[i] = (String) model.get("SALE_BUY_GBN" + i);
				}
				
				lcId = (String) model.get("LC_ID");
				workIp = (String) model.get(ConstantIF.SS_CLIENT_IP);
				userNo = (String) model.get(ConstantIF.SS_USER_NO);
				
				Map<String, Object> modelDel = new HashMap<String, Object>();
				modelDel.put("LC_ID"			, lcId); 
				modelDel.put("SITE_CONT_ID"		, siteContId); 
				modelDel.put("RITEM_ID"			, ritemId); 
				modelDel.put("CUST_ID"			, custId); 
				modelDel.put("SALE_BUY_GBN"		, saleBuyGbn); 
				
				modelDel.put("WORK_IP", workIp);
				modelDel.put("USER_NO", userNo);
				
				ServiceUtil.checkInputValidation2(modelDel, CHECK_VALIDATE_WMSAC016_DEL);
				// dao
				dao.delete(modelDel);
				ServiceUtil.isValidReturnCode("WMSAC016", String.valueOf(modelDel.get("O_MSG_CODE")),(String) modelDel.get("O_MSG_NAME"));
				
			}
			
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
    
    
  
	 /**
	 * Method ID : itemAcSave
     * Method 설명 : 정산 계약정보 상품 일괄 적용
     * 작성자 : yhku
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> itemAcSave(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {

			int iuCnt = Integer.parseInt(model.get("I_selectIds").toString());
			if (iuCnt > 0) {
				String[] ritemId = new String[iuCnt]; // 센터화주상품ID
				String[] custId = new String[iuCnt]; 
				String[] siteContId = new String[iuCnt];
				
				String lcId = ""; // 센터ID
				String workIp = "";
				String userNo = "";

				for (int i = 0; i < iuCnt; i++) {
					ritemId[i]	 	= (String) model.get("RITEM_ID" + i);
					custId[i]		= (String) model.get("CUST_ID" + i);
					siteContId[i]	= (String) model.get("SITE_CONT_ID");
				}

				lcId = (String) model.get("LC_ID");
				workIp = (String) model.get(ConstantIF.SS_CLIENT_IP);
				userNo = (String) model.get(ConstantIF.SS_USER_NO);

				Map<String, Object> modelIu = new HashMap<String, Object>();
				modelIu.put("LC_ID"				, lcId); 
				modelIu.put("CUST_ID"			, custId); 
				modelIu.put("RITEM_ID"			, ritemId); 
				modelIu.put("SITE_CONT_ID"		, siteContId); 
				modelIu.put("WORK_IP", workIp);
				modelIu.put("USER_NO", userNo);
				
				ServiceUtil.checkInputValidation2(modelIu, CHECK_VALIDATE_WMSAC016_ITEM);
				// dao
				dao.itemAcSave(modelIu);
				ServiceUtil.isValidReturnCode("WMSAC016", String.valueOf(modelIu.get("O_MSG_CODE")),(String) modelIu.get("O_MSG_NAME"));

			}
			// 등록 수정 끝

			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
	

	
	/**
	 * Method ID : itemAcDelete
     * Method 설명 : 정산 마스터 상품 일괄 삭제
     * 작성자 : yhku
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> itemAcDelete(Map<String, Object> model) throws Exception {
		Map<String, Object> m = new HashMap<String, Object>();
		int errCnt = 0;
		try {

			// 삭제
			int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
			if (delCnt > 0) {
				String[] ritemId = new String[delCnt]; // 센터화주상품ID
				String[] custId = new String[delCnt]; // 화주ID
				
				String lcId = ""; // 센터ID
				String workIp = "";
				String userNo = "";
				
				for (int i = 0; i < delCnt; i++) {
					ritemId[i] = (String) model.get("RITEM_ID" + i);
					custId[i] = (String) model.get("CUST_ID" + i);
				}
				lcId = (String) model.get("LC_ID");
				workIp = (String) model.get(ConstantIF.SS_CLIENT_IP);
				userNo = (String) model.get(ConstantIF.SS_USER_NO);
				
				Map<String, Object> modelDel = new HashMap<String, Object>();
				modelDel.put("LC_ID"			, lcId); 
				modelDel.put("RITEM_ID"			, ritemId); 
				modelDel.put("CUST_ID"			, custId); 
				
				modelDel.put("WORK_IP", workIp);
				modelDel.put("USER_NO", userNo);
				
				ServiceUtil.checkInputValidation2(modelDel, CHECK_VALIDATE_WMSAC016_ITEM_DEL);
				// dao
				dao.itemAcDelete(modelDel);
				ServiceUtil.isValidReturnCode("WMSAC016", String.valueOf(modelDel.get("O_MSG_CODE")),(String) modelDel.get("O_MSG_NAME"));
				
			}
			m.put("errCnt", errCnt);
			m.put("MSG", MessageResolver.getMessage("save.success"));

		} catch (BizException be) {
			if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
			m.put("errCnt", 1);
			m.put("MSG", be.getMessage());

		} catch (Exception e) {
			throw e;
		}
		return m;
	}
    
    /**
     * Method ID : listExcel
     * Method 설명 : 택배발급이력 엑셀다운
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
     
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.itemAcList(model));
        
        return map;
    }    
   
    /**
     * Method ID : selectBox
     * Method 설명 : 셀렉트 박스 조회 
     * 작성자 : yhku
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("ITEMGRP", dao.selectItem(model));
            model.put("inKey", "ORD01");
            
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : saveExcelUpload saveSub
     * Method 설명 : 청구단가 엑셀 저장 
     * 작성자 : summer H
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelUpload(Map<String, Object> model, List list) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	int insertCnt = (list != null)?list.size():0;
    	try{
    		if(insertCnt > 0){
    			String[] applyToDt       	= new String[insertCnt];
    			String[] applyFromDt       	= new String[insertCnt];
    			String[] accStd     		= new String[insertCnt];
    			String[] saleBuyGbn       	= new String[insertCnt];
    			String[] payDay       		= new String[insertCnt];
    			
    			String[] rItemCd       		= new String[insertCnt];
    			String[] rItemGroupCd       = new String[insertCnt];
    			String[] unitPrice       	= new String[insertCnt];
    			String[] inOutCost       	= new String[insertCnt];
    			String[] stockCost       	= new String[insertCnt];
    			
    			String[] currencyNm       	= new String[insertCnt];
    			String[] transCustId      	= new String[insertCnt];
    			String[] payCd       		= new String[insertCnt];
    			String[] roundCd       		= new String[insertCnt];
    			String[] endCutCd       	= new String[insertCnt];
    			
    			String[] closingDay       	= new String[insertCnt];
    			String[] contDeptId      	= new String[insertCnt];
    			String[] contEmployeeId     = new String[insertCnt];
    			String[] conf_yn       		= new String[insertCnt];
    			String[] realDeptId       	= new String[insertCnt];
    			
    			String[] realEmployeeId     = new String[insertCnt];
    			String[] collectEmployeeId  = new String[insertCnt];
    			String[] approvalEmpId      = new String[insertCnt];
    			String[] groupComEmpId      = new String[insertCnt];
    			
    			
    			for(int i=0; i<insertCnt; i++){
    				Map<String, String> temp = (Map)list.get(i);
    				
    				applyToDt[i]                  = (String)temp.get("APPLY_FR_DT");
    				applyFromDt[i]                = (String)temp.get("APPLY_TO_DT");
    				accStd[i]                     = (String)temp.get("ACC_STD");
    				saleBuyGbn[i]                 = (String)temp.get("SALE_BUY_GBN");
    				payDay[i]                     = (String)temp.get("PAY_DAY");
    				
    				rItemCd[i]                    = (String)temp.get("RITEM_CD");
    				rItemGroupCd[i]               = (String)temp.get("ITEM_GRP_CD");
    				unitPrice[i]                  = (String)temp.get("UNIT_PRICE");
    				inOutCost[i]                  = (String)temp.get("INOUT_COST");
    				stockCost[i]                  = (String)temp.get("STOCK_COST");
    				
    				currencyNm[i]                 = (String)temp.get("CURRENCY_NAME");
    				transCustId[i]                = (String)temp.get("TRANS_CUST_ID");
    				payCd[i]                      = (String)temp.get("PAY_CD");
    				roundCd[i]                    = (String)temp.get("ROUND_CD");
    				endCutCd[i]                   = (String)temp.get("END_CUT_CD");
    				
    				closingDay[i]                 = (String)temp.get("CLOSING_DAY");
    				contDeptId[i]                 = (String)temp.get("CONT_DEPT_ID");
    				contEmployeeId[i]             = (String)temp.get("CONT_EMPLOYEE_ID");
    				conf_yn[i]                    = (String)temp.get("CONF_YN");
    				realDeptId[i]                 = (String)temp.get("REAL_DEPT_ID");
    				
    				realEmployeeId[i]             = (String)temp.get("REAL_EMPLOYEE_ID");
    				collectEmployeeId[i]          = (String)temp.get("COLLECT_EMPLOYEE_ID");
    				approvalEmpId[i]              = (String)temp.get("APPROVAL_EMP_ID");
    				groupComEmpId[i]              = (String)temp.get("GROUP_COM_EMP_ID");
    				
    			}
    			
    			m.put("APPLY_FR_DT"				, applyToDt);                  
    			m.put("APPLY_TO_DT"				, applyFromDt);
    			m.put("ACC_STD"					, accStd);
    			m.put("SALE_BUY_GBN"			, saleBuyGbn);
    			m.put("PAY_DAY"					, payDay);
    			
    			m.put("RITEM_CD"				, rItemCd);
    			m.put("ITEM_GRP_NM"				, rItemGroupCd);
    			m.put("UNIT_PRICE"				, unitPrice);
    			m.put("INOUT_COST"				, inOutCost);
    			m.put("STOCK_COST"				, stockCost);
    			
    			m.put("CURRENCY_NAME"			, currencyNm);
    			m.put("TRANS_CUST_ID"			, transCustId);
    			m.put("PAY_CD"					, payCd);
    			m.put("ROUND_CD"				, roundCd);
    			m.put("END_CUT_CD"				, endCutCd);
    			
    			m.put("CLOSING_DAY"				, closingDay);
    			m.put("CONT_DEPT_ID"			, contDeptId);
    			m.put("CONT_EMPLOYEE_ID"		, contEmployeeId);
    			m.put("CONF_YN"					, conf_yn);
    			m.put("REAL_DEPT_ID"			, realDeptId);
    			
    			m.put("REAL_EMPLOYEE_ID"		, realEmployeeId);
    			m.put("COLLECT_EMPLOYEE_ID"		, collectEmployeeId);
    			m.put("APPROVAL_EMP_ID"			, approvalEmpId);
    			m.put("GROUP_COM_EMP_ID"		, groupComEmpId);
    			m.put("OVERWRITE_YN"		, (String)model.get("chkDuplicationYn"));
    			
    			m.put("CUST_ID"				, (String)model.get("vrCustId"));
    			m.put("LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
    			m.put("WORK_IP"				, (String)model.get(ConstantIF.SS_CLIENT_IP));
    			m.put("USER_NO"				, (String)model.get(ConstantIF.SS_USER_NO));
    			
    			m = (Map<String, Object>)dao.saveSubExcel(m);
    			
    			ServiceUtil.isValidReturnCode("WMSAC016", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
    			m.put("O_CUR", m.get("O_CUR"));  
    		}
    		
    		m.put("errCnt", 0);
    		m.put("MSG_ORA", "");
    		m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    /**
     * Method ID : saveExcelUploadPAH
     * Method 설명 : 청구단가 엑셀 저장 - 풀앳홈 간편
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelUploadPAH(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{
            	if(insertCnt > 0){
        			String[] applyToDt       	= new String[insertCnt];
        			String[] applyFromDt       	= new String[insertCnt];
        			String[] saleBuyGbn       	= new String[insertCnt];
        			String[] rItemCd       		= new String[insertCnt];
        			String[] rItemGroupCd       = new String[insertCnt];
        			
        			String[] unitPrice       	= new String[insertCnt];
        			String[] currencyNm       	= new String[insertCnt];
        			String[] payCd       		= new String[insertCnt];
        			String[] confYn       		= new String[insertCnt];
        			
        			String[]  setLcId     		= new String[insertCnt];      
        			String[]  perRtnRcv   		= new String[insertCnt];      
        			String[]  perCngShp   		= new String[insertCnt];      
        			String[]  perCngRcv   		= new String[insertCnt];      
        			String[]  perTnsRcv   		= new String[insertCnt];   
        			
        			String[]  perTnsSet   		= new String[insertCnt];      
        			String[]  perSitCan   		= new String[insertCnt];      
        			String[]  perFstBad   		= new String[insertCnt];      
        			String[]  perSrtCan   		= new String[insertCnt];      
        			String[]  perFunk     		= new String[insertCnt];      
            		
        			
            		for(int i=0; i<insertCnt; i++){
            			Map<String, String> temp = (Map)list.get(i);
            			
            			applyToDt[i]                  = (String)temp.get("APPLY_FR_DT");
            			applyFromDt[i]                = (String)temp.get("APPLY_TO_DT");
             			saleBuyGbn[i]                 = (String)temp.get("SALE_BUY_GBN");
            			rItemCd[i]                    = (String)temp.get("RITEM_CD");
            			rItemGroupCd[i]               = (String)temp.get("ITEM_GRP_CD");
            			 
            			unitPrice[i]                  = (String)temp.get("UNIT_PRICE");
            			currencyNm[i]                 = (String)temp.get("CURRENCY_NAME");
            			payCd[i]                      = (String)temp.get("PAY_CD");
            			confYn[i]                     = (String)temp.get("CONF_YN");
            			 
     					setLcId[i]   				  = (String) temp.get("SET_LC_ID");
    					perRtnRcv[i] 				  = (String) temp.get("PER_RTN_RCV");
    					perCngShp[i] 				  = (String) temp.get("PER_CNG_SHP");
    					perCngRcv[i] 				  = (String) temp.get("PER_CNG_RCV");
    					perTnsRcv[i] 				  = (String) temp.get("PER_TNS_RCV");
    					
    					perTnsSet[i] 				  = (String) temp.get("PER_TNS_SET");
    					perSitCan[i] 				  = (String) temp.get("PER_SIT_CAN");
    					perFstBad[i] 				  = (String) temp.get("PER_FST_BAD");
    					perSrtCan[i] 				  = (String) temp.get("PER_SRT_CAN");
    					perFunk[i]   				  = (String) temp.get("PER_FUNK");

            		}
            		
            		m.put("APPLY_FR_DT"			, applyToDt);                  
	                m.put("APPLY_TO_DT"			, applyFromDt);
	                m.put("SALE_BUY_GBN"		, saleBuyGbn);
	                m.put("RITEM_CD"			, rItemCd);
	                m.put("ITEM_GRP_NM"			, rItemGroupCd);
	                
	                m.put("UNIT_PRICE"			, unitPrice);
	                m.put("CURRENCY_NAME"		, currencyNm);
	                m.put("PAY_CD"				, payCd);
	                m.put("CONF_YN"				, confYn);
	                
	                m.put("SET_LC_ID"    		, setLcId);
	                m.put("PER_RTN_RCV"  		, perRtnRcv);
	                m.put("PER_CNG_SHP"  		, perCngShp);
	                m.put("PER_CNG_RCV"  		, perCngRcv);
	                m.put("PER_TNS_RCV"  		, perTnsRcv);
	                
	                m.put("PER_TNS_SET"  		, perTnsSet);
	                m.put("PER_SIT_CAN"  		, perSitCan);
	                m.put("PER_FST_BAD"  		, perFstBad);
	                m.put("PER_SRT_CAN"  		, perSrtCan);
	                m.put("PER_FUNK"     		, perFunk);
	                
	                m.put("OVERWRITE_YN"		, (String)model.get("chkDuplicationYn"));
	                m.put("CUST_ID"				, (String)model.get("vrCustId"));
	                m.put("LC_ID"				, (String)model.get(ConstantIF.SS_SVC_NO));
	                m.put("WORK_IP"				, (String)model.get(ConstantIF.SS_CLIENT_IP));
	                m.put("USER_NO"				, (String)model.get(ConstantIF.SS_USER_NO));
	                
	  	            m = (Map<String, Object>)dao.saveSubExcelPAH(m);
	  	            
                  ServiceUtil.isValidReturnCode("WMSAC016", String.valueOf(m.get("O_MSG_CODE")), (String)m.get("O_MSG_NAME"));
                  m.put("O_CUR", m.get("O_CUR"));  
            	}
            	
            	m.put("errCnt", 0);
            	m.put("MSG_ORA", "");
              m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
              
            } catch(Exception e){
          	  throw e;
            }
             return m;
    }
    public static String checkNull(String str) {
		return (str.equals("null") || str.equals("")) ? "" : str;
	}
}
