package com.logisall.winus.tmsbd.service;

import java.util.Map;



public interface TMSBD010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveReply(Map<String, Object> model) throws Exception;
}
