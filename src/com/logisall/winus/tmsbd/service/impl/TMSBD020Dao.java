package com.logisall.winus.tmsbd.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSBD020Dao")
public class TMSBD020Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());

    /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return  
    */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("TMSBD020.list", model);
    }
    
    public Map<String, Object> selectBoardInfo(Map<String, Object> model){
        return (Map<String, Object> )executeView("TMSBD020.detail", model);
    }
    
    public List<Map<String, Object> > selectClientList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("TMSBD020.clientinfo", model);
    }
    
    public List<Map<String, Object> > selectFileInfoList(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("TMSBD020.fileinfo", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 게시판 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("TMSBD020.insert_board", model);
    }
    
    /**
     * Method ID : update
     * Method 설명 : 통합 HelpDesk 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model) {
        return executeInsert("TMSBD020.update_board", model);
    }
    
    /**
     * Method ID : delete
     * Method 설명 : 통합 HelpDesk 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object delete(Map<String, Object> model) {
        return executeDelete("TMSBD020.delete_board", model);
    }
    
    
    /*-
     * Method ID : selectCustInfoTypeC
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     * @throws Exception
     */
    public List<Map<String, Object> > selectCustInfoTypeC(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("TMSBD020.select_custinfo_type_c", model);
    }
    
    /*-
     * Method ID : selectCustInfoTypeP
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     * @throws Exception
     */
    public List<Map<String, Object> > selectCustInfoTypeP(Map<String, Object> model) throws Exception{
        return (List<Map<String, Object> >) getSqlMapClientTemplate().queryForList("TMSBD020.select_custinfo_type_p", model);
    }
    

    /*-
     * Method ID : insertCustInfo
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public Object insertCustInfo(Map<String, Object> model) {
        return executeInsert("TMSBD020.insert_cust_info", model);
    }  
    
    /*-
     * Method ID : deleteCustInfo
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public Object deleteCustInfo(Map<String, Object> model) {
        return executeDelete("TMSBD020.deleete_cust_info", model);
    }
    
    /**
     * Method ID : fileUpload
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileUpload(Map<String, Object> model) {
        return executeInsert("tmsys900.fileInsert", model);
    }    
    
    /**
     * Method ID : selectMngno
     * Method 설명 : 파일 관리번호 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public String selectMngno(Map<String, Object> model) {
        return (String)executeView("tmsys900.select_mngNo", model);
    }

    /**
     * Method ID : fileDelete
     * Method 설명 : 파일 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileDelete(Map<String, Object> model) {
        return executeDelete("tmsys900.fileDelete", model);
    }
    
    /**
     * Method ID : fileUpdate
     * Method 설명 : 파일 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileUpdate(Map<String, Object> model) {
        return executeUpdate("tmsys900.fileUpdate", model);
    }
    
    /*-
     * Method ID : updateCount
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public int updateCount(Map<String, Object> model) {
        return executeUpdate("TMSBD020.updateCount", model);
    }
    
    /*-
     * Method ID : updateReply
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public int updateReply(Map<String, Object> model) {
        return executeUpdate("TMSBD020.update_reply", model);
    }    
    
}
