package com.logisall.winus.wmspd.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmspd.service.WMSPD030Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSPD030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPD030Service")
	private WMSPD030Service service;

	/*-
	 * Method ID    : wmspd030
	 * Method 설명      : 세트상품구성정보 화면
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSPD030.action")
	public ModelAndView wmspd030(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmspd/WMSPD030", service.selectData(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 세트상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD030/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : listSub
	 * Method 설명      : 구성상품 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD030/listSub.action")
	public ModelAndView listSub(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listSub(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : saveSub
	 * Method 설명      : 구성상품 저장수정
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPD030/saveSub.action")
	public ModelAndView saveItem(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveSub(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save Sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD030/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listSubExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to Excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("상품코드")   , "0", "0", "0", "0", "100"},
                                   {MessageResolver.getText("상품명")     , "1", "1", "0", "0", "100"},
                                   {MessageResolver.getText("수량")      , "2", "2", "0", "0", "100"},
                                   {"UOM"                               , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("생성일")     , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("생성자")     , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("수정일")     , "6", "6", "0", "0", "100"},
                                   {MessageResolver.getText("수정자")     , "7", "7", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"ITEM_CODE"        , "S"},
                                    {"PART_RITEM_NM"    , "S"},
                                    {"QTY"              , "N"},
                                    {"UOM_ID"           , "S"},
                                    {"REG_DT"           , "S"},
                                    {"REG_NM"           , "S"},
                                    {"UPD_DT"           , "S"},
                                    {"UPD_NM"           , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("세트상품구성정보");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isWarnEnabled()) {
				log.warn("fail download Excel file...", e);
			}
		}
	}

	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD030/sampleExcelDown.action")
	public void sampleExcelDown(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.getSampleExcelDown(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doSampleExcelDown2(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doSampleExcelDown2(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
		try {
			int iColSize = Integer.parseInt(model.get("Col_Size").toString());
			int iHRowSize = 6; // 고정값
			int iVRowSize = 2; // 고정값
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			String[][] headerEx = new String[iColSize][iHRowSize];
			String[][] valueName = new String[iColSize][iVRowSize];
			for (int i = 0; i < iColSize; i++) {
				String nName = MessageResolver.getText((String) model.get("Name_" + i)).replace("&lt;br&gt;", "").replace("&lt;/font&gt;&lt;br&gt;", "").replace("&lt;/font&gt;", "").replace("&lt;font color=&quot;#B1B1B1&quot;&gt;", "");
				// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
				headerEx[i] = new String[]{nName, i + "", i + "", "0", "0", "100"};
				// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
				// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
				valueName[i] = new String[]{(String) model.get("Col_" + i), "S"};
			}
			// 파일명
			String fileName = MessageResolver.getText("생산관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : genKitWork
	 * Method 설명 : 임가공 조립 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSPD030/genKitWork.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.genKitWork(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : 
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSPD030E2.action")
	public ModelAndView wmspd030E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmspd/WMSPD030E2");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : chSong
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPD030E2/inExcelFileUpload.action")
	public ModelAndView inExcelFileUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");

			String fileName = file.getOriginalFilename();
			String filePaths = "C:/Tmp/";// CONTS에 정의된 변수

			// 디렉토리 존재유무 확인
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);// 디렉토리생성
			}

			File destinationDir = new File(filePaths);

			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int colSize = Integer.parseInt((String) model.get("colSize"));
			String[] cellName = new String[colSize];
			for (int i = 0; i < colSize; i++) {
				cellName[i] = "S_" + i;
			}

			int startRow = Integer.parseInt((String) model.get("startRow"));

			List list = ExcelReader.excelLimitRowRead(destination, cellName, 0, startRow, 1000, 0);
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("LIST", list);

			if (destination.exists()) {
				destination.delete();
			}
			mav = new ModelAndView("jsonView", map);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload excel :", e);
			}
		}
		return mav;
	}
}
