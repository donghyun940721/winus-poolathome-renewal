package com.logisall.winus.wmspd.service;

import java.util.List;
import java.util.Map;

public interface WMSPD020Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> getSampleExcelDown(Map<String, Object> model) throws Exception;
    public Map<String, Object> autoOrderInsert(Map<String, Object> model) throws Exception;
    public Map<String, Object> initChkLoc(Map<String, Object> model) throws Exception;
}
