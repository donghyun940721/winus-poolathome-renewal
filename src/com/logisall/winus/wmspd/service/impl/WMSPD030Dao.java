package com.logisall.winus.wmspd.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPD030Dao")
public class WMSPD030Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectUom
     * Method 설명  : UOM 데이터셋
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectUom(Map<String, Object> model){
        return executeQueryForList("wmsms100.selectUom", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 세트상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsms090.list", model);
    }
    
    /**
     * Method ID  : listSub
     * Method 설명  : 구성상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet listSub(Map<String, Object> model) {
        return executeQueryPageWq("wmspd030.listSub", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 구성상품 등록
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmspd030.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 구성상품 수정
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmspd030.update", model);
    }  
    
    /**
     * Method ID    : update
     * Method 설명      : 구성상품 삭제
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmspd030.delete", model);
    }

    /**
     * Method ID  : genKitWork
     * Method 설명  : 임가공조립 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object genKitWork(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_gen_kit_work", model);
        return model;
    }
}
