package com.logisall.winus.wmsdf.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsdf.service.WMSDF001Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSDF001Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSDF001Service")
	private WMSDF001Service service;
	
	
	static final String[] COLUMN_NAME_WMSDF001 = {
		"UOM_NM", "UOM_CD", "LC_ID", "INHERIT_UOM_ID", "DEL_YN", "WORK_IP", "REG_NO", "UPD_NO"
	};
	/*-
	 * Method ID    : wmsdf001
	 * Method 설명      : 택배기준관리 화면
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSDF001.action")
	public ModelAndView wmsdf0010(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsdf/WMSDF001");
	}
	
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 택배기본정보관리 조회
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDF001/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : listE2
	 * Method 설명      : 택배송장 설정 조회
	 * 작성자                 : dhkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDF001/listE2.action")
	public ModelAndView listE2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listE3
	 * Method 설명      : 택배 단가 조회
	 * 작성자                 : SUMMER H
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDF001/listE3.action")
	public ModelAndView listE3(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    	: selectDlvSeq
	 * Method 설명      	: 택배사 시퀀스 조회
	 * 작성자                 	: dhkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDF001/selectDlvSeq.action")
	public ModelAndView selectDlvSeq(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.selectDlvSeq(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 택배기본정보 저장
	 * 작성자                 : yhku
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDF001/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : saveE3
	 * Method 설명      : 택배송장정보 저장
	 * 작성자                : dhkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDF001/saveE2.action")
	public ModelAndView saveE2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : saveE3
	 * Method 설명      : 택배단가관리 저장 및 업데이트 MERGE
	 * 작성자                : SUMEMR H
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDF001/saveE3.action")
	public ModelAndView saveE3(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			
			m = service.saveE3(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID : wmsdf001E2
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping("/WMSDF001E2.action")
//	public ModelAndView wmsdf001E2(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
//		return new ModelAndView("winus/wmsms/WMSDF001E2");
//	}
	
	/*-
	 * Method ID : uploadInfo
	 * Method 설명 : 엑셀파일업로드
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param txtFile
	 * @return
	 * @throws Exception
	 */
//	@RequestMapping("/WMSDF001/uploadInfo.action")
//	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = null;
//		try {
//			request.setCharacterEncoding("utf-8");
//	
//			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
//			MultipartFile file = multipartRequest.getFile("txtFile");
//			String fileName = file.getOriginalFilename();
//			String filePaths = ConstantIF.TEMP_PATH;
//	
//			if (!FileHelper.existDirectory(filePaths)) {
//				FileHelper.createDirectorys(filePaths);
//			}
//			File destinationDir = new File(filePaths);
//			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
//			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
//	
//			int startRow = Integer.parseInt((String) model.get("startRow"));
//			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSDF001, 0, startRow, 10000, 0);
//			m = service.saveUploadData(model, list);
//	
//			destination.deleteOnExit();
//			mav.addAllObjects(m);
//	
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to upload Excel info :", e);
//			}
//			m = new HashMap<String, Object>();
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//			m.put("MSG_ORA", e.getMessage());
//			m.put("errCnt", "1");
//			mav.addAllObjects(m);
//		}
//		return mav;
//	} 
	
    
}