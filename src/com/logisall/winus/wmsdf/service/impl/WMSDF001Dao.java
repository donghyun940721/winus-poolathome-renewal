package com.logisall.winus.wmsdf.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDF001Dao")
public class WMSDF001Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    

	/**
	 * Method ID : list 
	 * Method 설명 : 택배기본정보관리 조회
	 * 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsdf001.list", model);
	}
	
	
	/**
	 * Method ID : listE2
	 * Method 설명 : 택배송장 설정 조회
	 * 작성자 : dhkim
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listE2(Map<String, Object> model) {
		return executeQueryPageWq("wmsdf001.listE2", model);
	}
	
	/**
	 * Method ID : listE2
	 * Method 설명 : 택배 단가 조회
	 * 작성자 : SUMMER H
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listE3(Map<String, Object> model) {
		return executeQueryPageWq("wmsdf001.listE3", model);
	}
	
	/**
	 * Method ID	: selectDlvSeq
	 * Method 설명 	: 택배사 시퀀스 조회
	 * 작성자 			: dhkim
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet selectDlvSeq(Map<String, Object> model) {
		return executeQueryPageWq("wmsdf001.selectDlvSeq", model);
	}

	
	/**
	 * Method ID : insert 
	 * Method 설명 : 택배기본정보 등록 
	 * 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmsdf001.insert", model);
	}

	/**
	 * Method ID : update 
	 * Method 설명 : 택배기본정보 수정 
	 * 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmsdf001.update", model);
	}

	/**
	 * Method ID : delete 
	 * Method 설명 : 택배기본정보 삭제 (DEL_YN 를 Y로 수정) 
	 * 작성자 : yhku
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmsdf001.delete", model);
	}

	/**
	 * Method ID : saveUploadData 
	 * Method 설명 : 
	 * 작성자 : 
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if ((paramMap.get("UOM_NM") != null && StringUtils.isNotEmpty(paramMap.get("UOM_NM").toString())) && (paramMap.get("UOM_CD") != null && StringUtils.isNotEmpty(paramMap.get("UOM_CD").toString()))
						&& (paramMap.get("LC_ID") != null && StringUtils.isNotEmpty(paramMap.get("LC_ID").toString()))) {

					sqlMapClient.insert("wmsms100.insert_many", paramMap);

				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : yhku
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }    
    
    
    /*-
     * Method ID 	: saveE2
     * Method 설명 	: 택배송장정보 저장
     * 작성자 			: dhkim
     *
     * @param model
     * @return
     */
	public Object saveE2(Object model) {
		return executeUpdate("wmsdf001.saveE2", model);
	}
	
	/*-
     * Method ID 	: saveE3
     * Method 설명 	: 택배단가관리 저장 및 업데이트 MERGE
     * 작성자 			: SUMMER H
     *
     * @param model
     * @return
     */
	public Object saveE3(Object model) {
		return executeUpdate("wmsdf001.saveE3", model);
	}
}

