package com.logisall.winus.wmsdf.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsdf.service.WMSDF001Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSDF001Service")
public class WMSDF001ServiceImpl extends AbstractServiceImpl implements WMSDF001Service {
    
    @Resource(name = "WMSDF001Dao")
    private WMSDF001Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSDF001 = {"LC_ID", "CUST_ID", "PARCEL_COM_TY", "CUSTOMER_KEY_01"};
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 택배기본정보관리 조회
     * 작성자                      : yhku
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    
    /**
     * 
     * 대체 Method ID   : listE2
     * 대체 Method 설명    : 택배송장 설정 조회
     * 작성자                      : dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageSize", "60000");
        map.put("LIST", dao.listE2(model));
        return map;
    }
    
    /**
    * 
    * 대체 Method ID   : listE3
    * 대체 Method 설명    : 택배단가조회
    * 작성자                      : SUMMER H
    * @param   model
    * @return
    * @throws  Exception
    */
   @Override
   public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
       Map<String, Object> map = new HashMap<String, Object>();
       model.put("pageSize", "60000");
       map.put("LIST", dao.listE3(model));
       return map;
   }
    
    /**
     * 
     * 대체 Method ID   	: selectDlvSeq
     * 대체 Method 설명    	: 택배사 시퀀스 조회 
     * 작성자                      : dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectDlvSeq(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageSize", "60000");
        map.put("LIST", dao.selectDlvSeq(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 택배기본정보 저장
     * 작성자                      : yhku
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                
                
                modelDt.put("selectIds"			, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));
                modelDt.put("PARCEL_COM_TY"     , model.get("PARCEL_COM_TY"+i));
                modelDt.put("PARCEL_COM_NM"     , model.get("PARCEL_COM_NM"+i));
                modelDt.put("PARCEL_COM_TY_SEQ" , model.get("PARCEL_COM_TY_SEQ"+i));
                modelDt.put("LC_ID"  		    , model.get("LC_ID"+i));
                modelDt.put("CUST_ID"   		, model.get("CUST_ID"+i));
                modelDt.put("CUSTOMER_KEY_01"   , model.get("CUSTOMER_KEY_01"+i));
                modelDt.put("SHORT_DESC"     	, model.get("SHORT_DESC"+i));
                modelDt.put("CUSTOMER_KEY_02"   , model.get("CUSTOMER_KEY_02"+i));
                modelDt.put("CUSTOMER_KEY_03"   , model.get("CUSTOMER_KEY_03"+i));
                modelDt.put("SENDR_NM"    		, model.get("SENDR_NM"+i));
                modelDt.put("SENDR_TEL_NO1"     , model.get("SENDR_TEL_NO1"+i));
                modelDt.put("SENDR_TEL_NO2"     , model.get("SENDR_TEL_NO2"+i));
                modelDt.put("SENDR_TEL_NO3"     , model.get("SENDR_TEL_NO3"+i));
                modelDt.put("SENDR_ZIP_NO"      , model.get("SENDR_ZIP_NO"+i));
                modelDt.put("SENDR_ADDR1"       , model.get("SENDR_ADDR1"+i));
                modelDt.put("SENDR_ADDR2"       , model.get("SENDR_ADDR2"+i));
                modelDt.put("SENDR_ADDR3"		, model.get("SENDR_ADDR3"+i));
                modelDt.put("COLLECTION_AGENCY" 	, model.get("COLLECTION_AGENCY"+i));
                modelDt.put("COLLECTION_AGENCY_TEL" , model.get("COLLECTION_AGENCY_TEL"+i));
                modelDt.put("COLLECTION_AGENCY_CD" 	, model.get("COLLECTION_AGENCY_CD"+i));
                modelDt.put("COLLECTION_AGENCY_ETC" , model.get("COLLECTION_AGENCY_ETC"+i));
                modelDt.put("DLV_PRICE1"		,model.get("DLV_PRICE1"+i));
                modelDt.put("DLV_PRICE2"		,model.get("DLV_PRICE2"+i));
                modelDt.put("OPT_1"				,model.get("OPT_1"+i));
                modelDt.put("OPT_2"				,model.get("OPT_2"+i));
                modelDt.put("SORT_1"			,model.get("SORT_1"+i));
                modelDt.put("SORT_2"			,model.get("SORT_2"+i));
                modelDt.put("ITEM_ALL_YN"		,model.get("ITEM_ALL_YN"+i));
                modelDt.put("REP_PRINT_TYPE"	,model.get("REP_PRINT_TYPE"+i));
                modelDt.put("RETURN_USE_YN"	,model.get("RETURN_USE_YN"+i));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSDF001);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                	
                	Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_TYPE", "DFM");
					String codeStr = model.get("LC_ID"+i) + "_"
							         + model.get("CUST_ID"+i) + "_"
							         + model.get("PARCEL_COM_TY"+i) + "_"
							         + model.get("PARCEL_COM_TY_SEQ"+i) + "_"
									 + model.get("CUSTOMER_KEY_01"+i);
					modelSP.put("I_CODE", codeStr);			
					String checkExistData = dao.checkExistData(modelSP);
					if (checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
//						throw new BizException( MessageResolver.getMessage("delete.exist.error", new String[]{ 
//								MessageResolver.getMessage("delete.exist." + "WMSMS012")
//						}) );
						throw new BizException( MessageResolver.getMessage("excel.check.confirm", new String[]{ 
								MessageResolver.getMessage("\uC1A1\uC7A5\uBC88\uD638")
						})+"["+ MessageResolver.getMessage("\uC0AD\uC81C\uBD88\uAC00")+"("+checkExistData+")]" );
                    }
					
                    dao.delete(modelDt);
                    strGubun = "D";
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            
            //한번이라도 "D"면 삭제
            if (strGubun == "D")
            	m.put("MSG", MessageResolver.getMessage("delete.success"));
            else
            	m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
 
	/**
     * 
     * 대체 Method ID		: saveE2
     * 대체 Method 설명	: 
     * 작성자				: dhkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> rowData = null;

        try{
        	JSONParser jsonParser 		= new JSONParser();
        	ArrayList<Object> gridData 	= (ArrayList<Object>) jsonParser.parse(model.get("gridData").toString());
        	
        	String lcId		= model.get("SS_SVC_NO").toString();
        	String custId	= model.get("vrSrchCustIdE2").toString();
        	String userNo	= model.get("SS_USER_NO").toString();
        	String workIp	= model.get("SS_CLIENT_IP").toString();
        	

        	for(Object dt : gridData){
        		rowData = new ObjectMapper().convertValue(dt, Map.class);
        		rowData.put("LC_ID", lcId);
        		rowData.put("CUST_ID", custId);
        		rowData.put("USER_NO", userNo);
        		rowData.put("WORK_IP", workIp);
        		
        		dao.saveE2(rowData);  
        	}       	    	        
        	
            map.put("MSG", MessageResolver.getMessage("save.success"));
            map.put("MSG_ORA", "");
            map.put("errCnt", 0);        	
        	
        }
        catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("save.error") );
        }
        
        
        return map;
    }
    
	/**
     * 
     * 대체 Method ID		: saveE3
     * 대체 Method 설명	: 택배단가관리 > 저장 및 업데이트 MERGE
     * 작성자				: SUMMER HYUN
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> saveE3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        

        try{
        	int rowCnt = Integer.parseInt(model.get("ROW_COUNT").toString());
        	System.out.println("ROW COUNT : "+rowCnt);
        	
        	for(int i=0; i < rowCnt; i++){
        		
        		Map<String, Object> modelIns = new HashMap<String, Object>();
        		
        		String parcelType      = model.get(String.format("PARCEL_TYPE_%d", i)).toString();
        		String boxCd     	   = model.get(String.format("BOX_CD_%d", i)).toString();
        		String boxPrice        = model.get(String.format("BOX_PRICE_%d", i)).toString();
        		String useYN      	   = model.get(String.format("USE_YN_%d", i)).toString();
        		
        		
            	String parcelOrdTyNum   = null;
            	
            	if(parcelType.equals("정상")){
            		parcelOrdTyNum = "01";
            	}else{
            		parcelOrdTyNum = "02";
            	}
        		
            	modelIns.put("LC_ID",  model.get("SS_SVC_NO"));
            	modelIns.put("CUST_ID", model.get("vrSrchCustIdE3"));
            	modelIns.put("USER_NO", model.get("SS_USER_NO"));
            	modelIns.put("WORK_IP", model.get("SS_CLIENT_IP"));
        		
            	modelIns.put("PARCEL_COM_TY"			, model.get("vrSrchParcelComCdE3"));
            	modelIns.put("PARCEL_COM_TY_SEQ"		,  model.get("vrSrchParcelSeqE3"));

            	modelIns.put("BOX_CD"			, boxCd);
            	modelIns.put("BOX_PRICE"		, boxPrice);
            	modelIns.put("USE_YN"			, useYN);
            	modelIns.put("PARCEL_ORD_TY"		, parcelOrdTyNum);
        		
        		dao.saveE3(modelIns);  
        	}
        	
            map.put("MSG", MessageResolver.getMessage("save.success"));
            map.put("MSG_ORA", "");
            map.put("errCnt", 0);        	
        	
        }
        catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to get result :", e);
			} 
        	map.put("errCnt", 1);
            map.put("MSG", MessageResolver.getMessage("save.error") );
        }
        
        
        return map;
    }
    
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀업로드 저장
     * 작성자 : 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{            	
                dao.saveUploadData(model, list);
                
                m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
                m.put("MSG_ORA", "");
                m.put("errCnt", errCnt);
                
            } catch(Exception e){
                throw e;
            }
        return m;
    }
}
