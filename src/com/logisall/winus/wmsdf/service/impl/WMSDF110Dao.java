package com.logisall.winus.wmsdf.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSDF110Dao")
public class WMSDF110Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID : list
     * Method 설명 : 택배발급이력 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsdf110.list", model);
    }   
    
    /**
     * Method ID : listP
     * Method 설명 : 배송이력추적 팝업 조회
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public GenericResultSet listP(Map<String, Object> model) {
        return executeQueryPageWq("wmsdf111.list", model);
    }   
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 셀렉트박스
     * 작성자 : 이성중
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
}

