package com.logisall.winus.wmsdf.service;

import java.util.List;
import java.util.Map;


public interface WMSDF001Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE2(Map<String, Object> model) throws Exception;
	public Map<String, Object> listE3(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectDlvSeq(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
}
