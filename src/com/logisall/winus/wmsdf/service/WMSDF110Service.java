package com.logisall.winus.wmsdf.service;

import java.util.Map;


public interface WMSDF110Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listP(Map<String, Object> model) throws Exception;    
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;    
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
}
