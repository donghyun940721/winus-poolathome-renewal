package com.logisall.winus.wmsda.web;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsda.service.WMSDA100Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSDA100Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSDA100Service")
    private WMSDA100Service service;
    
    /*-
	 * Method ID    : WMSCT010
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSDA100.action")
	public ModelAndView WMSDA100(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsda/WMSDA100");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSDA100/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSDA100/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs, (String)model.get("ss_proc_auth"));
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs, String procAuth) {
        try{
        	if(procAuth.equals("02")){ // 배송사 직원전용
        		//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
                String[][] headerEx = {
				                		  {MessageResolver.getText("화주")    		, "0", "0", "0", "0", "100"}
				                		 ,{MessageResolver.getText("등록구분")    		, "1", "1", "0", "0", "100"}
				                		 ,{MessageResolver.getText("배송센터")    		, "2", "2", "0", "0", "100"}
				                		 ,{MessageResolver.getText("발주일")    		, "3", "3", "0", "0", "100"}
				                		 ,{MessageResolver.getText("지정일")    		, "4", "4", "0", "0", "100"}
				
				                		 ,{MessageResolver.getText("배송완료일")    	, "5", "5", "0", "0", "100"}
				                		 ,{MessageResolver.getText("판매처")    		, "6", "6", "0", "0", "100"}
				                		 ,{MessageResolver.getText("고객명")    		, "7", "7", "0", "0", "100"}
				                		 ,{MessageResolver.getText("고객전화번호1")    	, "8", "8", "0", "0", "100"}
				                		 ,{MessageResolver.getText("고객전화번호2")    	, "9", "9", "0", "0", "100"}
				
				                		 ,{MessageResolver.getText("주소")    		, "10", "10", "0", "0", "100"}
				                		 ,{MessageResolver.getText("제품코드")    		, "11", "11", "0", "0", "100"}
				                		 ,{MessageResolver.getText("시리얼")    		, "12", "12", "0", "0", "100"}
				                		 ,{MessageResolver.getText("수량")    		, "13", "13", "0", "0", "100"}
				                		 ,{MessageResolver.getText("고객주문번호")    	, "14", "14", "0", "0", "100"}
				
				                		 ,{MessageResolver.getText("제품명")    		, "15", "15", "0", "0", "100"}
				                		 ,{MessageResolver.getText("배송메모")    		, "16", "16", "0", "0", "100"}
				                		 ,{MessageResolver.getText("배송상태")    		, "17", "17", "0", "0", "100"}
				                		 ,{MessageResolver.getText("설정배송기사")    	, "18", "18", "0", "0", "100"}
				                		 ,{MessageResolver.getText("지정배송기사")    	, "19", "19", "0", "0", "100"}
				
				                		 ,{MessageResolver.getText("배송사")    		, "20", "20", "0", "0", "100"}
				                		 ,{MessageResolver.getText("이미지")    		, "21", "21", "0", "0", "100"}
				                		 ,{MessageResolver.getText("배송타입")    		, "22", "22", "0", "0", "100"}
				                		 ,{MessageResolver.getText("화주청구비")    	, "23", "23", "0", "0", "100"}
				                		 ,{MessageResolver.getText("CASHBACK")    	, "24", "24", "0", "0", "100"}
				
				                		 ,{MessageResolver.getText("화주청구비최종")		, "25", "25", "0", "0", "100"}
				                		 ,{MessageResolver.getText("배송사청구비")    	, "26", "26", "0", "0", "100"}
				                		 ,{MessageResolver.getText("DLV_CITY")    	, "27", "27", "0", "0", "100"}
				                		 ,{MessageResolver.getText("DLV_CITY_NM")   , "28", "28", "0", "0", "100"}
				                		 ,{MessageResolver.getText("DLV_ORD_ID")    , "29", "29", "0", "0", "100"}
                                      };
                
                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
                String[][] valueName = {
				                		  {"TRUST_CUST_NM"    		, "S"}
				                		 ,{"TRN_DLV_ORD_TYPE"    	, "S"}
				                		 ,{"REQ_DLV_JOIN_NM"    	, "S"}
				                		 ,{"BUYED_DT"    			, "S"}
				                		 ,{"DLV_SET_DT"    			, "S"}
				
				                		 ,{"DLV_DT"    				, "S"}
				                		 ,{"SALES_COMPANY_NM"    	, "S"}
				                		 ,{"DLV_CUSTOMER_NM"    	, "S"}
				                		 ,{"DLV_PHONE_1"    		, "S"}
				                		 ,{"DLV_PHONE_2"    		, "S"}
				
				                		 ,{"DLV_ADDR"    			, "S"}
				                		 ,{"DLV_PRODUCT_CD"    		, "S"}
				                		 ,{"SERIAL_NO"    			, "S"}
				                		 ,{"DLV_QTY"    			, "S"}
				                		 ,{"ORG_ORD_ID"    			, "S"}
				
				                		 ,{"DLV_PRODUCT_NM"    		, "S"}
				                		 ,{"DLV_COMMENT"    		, "S"}
				                		 ,{"TRN_DLV_ORD_STAT"    	, "S"}
				                		 ,{"REQ_DRIVER_NM"    		, "S"}
				                		 ,{"SET_DRIVER_NM"    		, "S"}
				
				                		 ,{"REQ_DLV_COM_NM"    		, "S"}
				                		 ,{"IMG_SND_YN"    			, "S"}
				                		 ,{"DLV_SET_TYPE"    		, "S"}
				                		 ,{"MALLTAIL_REQ_COST"    	, "S"}
				                		 ,{"CASHBACK"    			, "S"}
				
				                		 ,{"MALLTAIL_REQ_COST_FINAL", "S"}
				                		 ,{"DELIVERY_COST"    		, "S"}
				                		 ,{"DLV_CITY"    			, "S"}
				                		 ,{"DLV_CITY_NM"    		, "S"}
				                		 ,{"DLV_ORD_ID"    			, "S"}
                                       }; 

    			// 파일명
    			String fileName = MessageResolver.getText("배송정산산출");
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";

    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

        	}else{
        		//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
                //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
                String[][] headerEx = {
					                	  {MessageResolver.getText("화주")    		, "0", "0", "0", "0", "100"}
					               		 ,{MessageResolver.getText("등록구분")    		, "1", "1", "0", "0", "100"}
					               		 ,{MessageResolver.getText("배송센터")    		, "2", "2", "0", "0", "100"}
					               		 ,{MessageResolver.getText("발주일")    		, "3", "3", "0", "0", "100"}
					               		 ,{MessageResolver.getText("지정일")    		, "4", "4", "0", "0", "100"}
					
					               		 ,{MessageResolver.getText("배송완료일")    	, "5", "5", "0", "0", "100"}
					               		 ,{MessageResolver.getText("판매처")    		, "6", "6", "0", "0", "100"}
					               		 ,{MessageResolver.getText("고객명")    		, "7", "7", "0", "0", "100"}
					               		 ,{MessageResolver.getText("고객전화번호1")    	, "8", "8", "0", "0", "100"}
					               		 ,{MessageResolver.getText("고객전화번호2")    	, "9", "9", "0", "0", "100"}
					
					               		 ,{MessageResolver.getText("주소")    		, "10", "10", "0", "0", "100"}
					               		 ,{MessageResolver.getText("제품코드")    		, "11", "11", "0", "0", "100"}
					               		 ,{MessageResolver.getText("시리얼")    		, "12", "12", "0", "0", "100"}
					               		 ,{MessageResolver.getText("수량")    		, "13", "13", "0", "0", "100"}
					               		 ,{MessageResolver.getText("고객주문번호")    	, "14", "14", "0", "0", "100"}
					
					               		 ,{MessageResolver.getText("제품명")    		, "15", "15", "0", "0", "100"}
					               		 ,{MessageResolver.getText("배송메모")    		, "16", "16", "0", "0", "100"}
					               		 ,{MessageResolver.getText("배송상태")    		, "17", "17", "0", "0", "100"}
					               		 ,{MessageResolver.getText("설정배송기사")    	, "18", "18", "0", "0", "100"}
					               		 ,{MessageResolver.getText("지정배송기사")    	, "19", "19", "0", "0", "100"}
					
					               		 ,{MessageResolver.getText("배송사")    		, "20", "20", "0", "0", "100"}
					               		 ,{MessageResolver.getText("이미지")    		, "21", "21", "0", "0", "100"}
					               		 ,{MessageResolver.getText("배송타입")    		, "22", "22", "0", "0", "100"}
					               		 ,{MessageResolver.getText("화주청구비")    	, "23", "23", "0", "0", "100"}
					               		 ,{MessageResolver.getText("CASHBACK")    	, "24", "24", "0", "0", "100"}
					
					               		 ,{MessageResolver.getText("화주청구비최종")		, "25", "25", "0", "0", "100"}
                                      };
                
                //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
                String[][] valueName = {
					                	  {"TRUST_CUST_NM"    		, "S"}
					               		 ,{"TRN_DLV_ORD_TYPE"    	, "S"}
					               		 ,{"REQ_DLV_JOIN_NM"    	, "S"}
					               		 ,{"BUYED_DT"    			, "S"}
					               		 ,{"DLV_SET_DT"    			, "S"}
					
					               		 ,{"DLV_DT"    				, "S"}
					               		 ,{"SALES_COMPANY_NM"    	, "S"}
					               		 ,{"DLV_CUSTOMER_NM"    	, "S"}
					               		 ,{"DLV_PHONE_1"    		, "S"}
					               		 ,{"DLV_PHONE_2"    		, "S"}
					
					               		 ,{"DLV_ADDR"    			, "S"}
					               		 ,{"DLV_PRODUCT_CD"    		, "S"}
					               		 ,{"SERIAL_NO"    			, "S"}
					               		 ,{"DLV_QTY"    			, "S"}
					               		 ,{"ORG_ORD_ID"    			, "S"}
					
					               		 ,{"DLV_PRODUCT_NM"    		, "S"}
					               		 ,{"DLV_COMMENT"    		, "S"}
					               		 ,{"TRN_DLV_ORD_STAT"    	, "S"}
					               		 ,{"REQ_DRIVER_NM"    		, "S"}
					               		 ,{"SET_DRIVER_NM"    		, "S"}
					
					               		 ,{"REQ_DLV_COM_NM"    		, "S"}
					               		 ,{"IMG_SND_YN"    			, "S"}
					               		 ,{"DLV_SET_TYPE"    		, "S"}
					               		 ,{"MALLTAIL_REQ_COST"    	, "S"}
					               		 ,{"CASHBACK"    			, "S"}
					
					               		 ,{"MALLTAIL_REQ_COST_FINAL", "S"}
                                       }; 

    			// 파일명
    			String fileName = MessageResolver.getText("배송정산산출");
    			// 시트명
    			String sheetName = "Sheet1";
    			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
    			String marCk = "N";
    			// ComUtil코드
    			String etc = "";

    			ExcelWriter wr = new ExcelWriter();
    			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
        	}

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
