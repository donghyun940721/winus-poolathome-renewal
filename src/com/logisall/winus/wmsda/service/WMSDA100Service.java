package com.logisall.winus.wmsda.service;

import java.util.Map;

public interface WMSDA100Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
