package com.logisall.winus.tmsys.web;

import java.util.Map;
import javax.annotation.Resource;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.tmsys.service.TMSYS140Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS140Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS140Service")
	private TMSYS140Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 로그내역 조회 화면
	 * 작성자 : 민환기
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS140.action")
	public ModelAndView mn(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			
			System.out.println("hkmin");
			
			mav = new ModelAndView("winus/tmsys/TMSYS140");

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 로그내역 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS140/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			
			System.out.println("model");
			
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}   
}