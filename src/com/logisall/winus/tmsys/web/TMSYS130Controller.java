package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.tmsys.service.TMSYS130Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS130Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS130Service")
	private TMSYS130Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 통합 HelpDesk 화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS130.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/tmsys/TMSYS130", service.selectBox(model));
		// 맨 앞에 / 없음에 주의, .vm 없음에 주의
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 통합 HelpDesk 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS130/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get error list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : save
	 * Method 설명 : 통합 HelpDesk 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS130/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
       
    /*-
     * Method ID : listExcel
     * Method 설명 : 엑셀다운로드
     * 작성자 : 기드온
     * @param request
     * @param response
     * @param model
     */
    @RequestMapping("/TMSYS130/excel.action")
    public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map = service.listExcel(model);
            GenericResultSet grs = (GenericResultSet)map.get("LIST");
            if(grs.getTotCnt() > 0){
                this.doExcelDown(response, grs);
            }
        }catch (Exception e) {
        	if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
        }
    }
    
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("시스템"), "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("요청번호"), "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("화면명"), "2", "2", "0", "0", "300"},
                                   {MessageResolver.getMessage("유형"), "3", "3", "0", "0", "400"},
                                   {MessageResolver.getMessage("진행상황"), "4", "4", "0", "0", "100"},
                                   {MessageResolver.getMessage("오류/요청사항"), "5", "5", "0", "0", "100"},
                                   {MessageResolver.getMessage("요청일"), "6", "6", "0", "0", "100"},
                                   {MessageResolver.getMessage("예정일"), "7", "7", "0", "0", "100"},
                                   {MessageResolver.getMessage("완료일"), "8", "8", "0", "0", "100"},
                                   {MessageResolver.getMessage("요청자"), "9", "9", "0", "0", "100"},
                                   {MessageResolver.getMessage("요청자ID"), "10", "10", "0", "0", "100"},
                                   {MessageResolver.getMessage("담당자"), "11", "11", "0", "0", "100"},
                                   {MessageResolver.getMessage("담당자답변"), "12", "12", "0", "0", "100"},
                                   {MessageResolver.getMessage("비고"), "13", "13", "0", "0", "100"},
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"PROJECT" , "S"},
                                    {"REQ_ID"  , "S"},
                                    {"SUBSYSTEM"    , "S"},
                                    {"REQ_TYPE"    , "S"},
                                    {"STATUS"      , "S"},
                                    {"CONTENTS"  , "S"},
                                    {"REQ_DATE"  , "S"},
                                    {"EXP_DATE"  , "S"},
                                    {"END_DATE"  , "S"},
                                    {"REQ_PERSON"  , "S"},
                                    {"REQ_PERSON_ID"  , "S"},
                                    {"EMPLOYEE"  , "S"},
                                    {"COMMENTS"  , "S"},
                                    {"ETC"  , "S"}
                                   }; 
            
            //파일명
            String fileName ="HELPDESK";
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
        	if (log.isErrorEnabled()) {
        		log.error("fail download excel file...", e);
        	}
        }
    }
    
    
    
    
    /*-
	 * Method ID : docDown
	 * Method 설명 : 위너스 소개자료 다운로드 팝업 
	 * 작성자 : Summer H 2023.02.23
	 * @param model
	 * @return
	 */
	@RequestMapping("/docDown.action")
	public ModelAndView docDown(Map<String, Object> model) {
		return new ModelAndView("winus/tmsys/TMSYS131Q1");
	}
    
    
    
    
    
}
