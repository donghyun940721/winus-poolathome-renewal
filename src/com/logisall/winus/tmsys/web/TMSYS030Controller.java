package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.tmsys.service.TMSYS030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS030Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS030Service")
	private TMSYS030Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 사용자관리 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS030.action")
	public ModelAndView mn(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/tmsys/TMSYS030", service.selectUserGB(model));
	}

	/*-
	 * Method ID : list
	 * Method 설명 : 사용자관리 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : searchUserLc
	 * Method 설명 : 사용자의 물류센터를 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/searchUserLc.action")
	public ModelAndView searchUserLc(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.searchUserLc(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get search info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : detail
	 * Method 설명 : 사용자관리 상세 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/detail.action")
	public ModelAndView detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/tmsys/TMSYS030E2", service.getUserDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : insert
	 * Method 설명 : 사용자관리 등록
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/insert.action")
	public ModelAndView insert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.insert(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : update
	 * Method 설명 : 사용자관리 수정
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/update.action")
	public ModelAndView update(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			mav = new ModelAndView("jsonView", service.udpate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("update.error"));
		}
		if (mav != null)
			mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : delete
	 * Method 설명 : 사용자관리 삭제
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/delete.action")
	public ModelAndView delete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("/winus/tmsys/TMSYS030E2");
		Map<String, Object> m = null;

		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : UserIdCheck
	 * Method 설명 : 사용자ID를 중복 체크한다.
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/UserIdCheck.action")
	public ModelAndView checkUserId(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/tmsys/TMSYS030Pop", service.checkUserId(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to check user id :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : UserPswdReset
	 * Method 설명 : 사용자 비밀번호를 초기화한다.
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/UserPswdReset.action")
	public ModelAndView updateUserPswdReset(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.updateReset(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to reset pwd :", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : userInfo
	 * Method 설명 : 사용자관리 상세 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/userInfo.action")
	public ModelAndView userInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/tmsys/TMSYS030Q1", service.getUserDetail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get user info :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/TMSYS030/localTime.action")
	public ModelAndView localTime(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.localTime(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/TMSYS030/jsonPostCross_uPcRecodeList.action")
	public ModelAndView jsonPostCross_uPcRecodeList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.jsonPostCross_uPcRecodeList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/TMSYS030/jsonPostCross_uPcCallhistory.action")
	public ModelAndView jsonPostCross_uPcCallhistory(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.jsonPostCross_uPcCallhistory(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/**
     * Method ID    : TMSYS030E4
     * Method 설명        : 
     * 작성자                     : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/TMSYS030E4.action")
    public ModelAndView TMSYS030E4(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/tmsys/TMSYS030E4");
    }
    
    /*-
	 * Method ID    : TMSYS030T1
	 * Method 설명      : 고객정보
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS030T1.action")
	public ModelAndView TMSYS030T1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/tmsys/TMSYS030T1");
	}
	
	/*-
	 * Method ID    : callUserInofo
	 * Method 설명      :
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/TMSYS030T1/callUserInofo.action")
	public ModelAndView callUserInofo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.callUserInofo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : callNumSave
	 * Method 설명      : 고객관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/TMSYS030T1/callNumSave.action")
	public ModelAndView callNumSave(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.callNumSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/TMSYS030/getAlertMsg.action")
	public ModelAndView getAlertMsg(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getAlertMsg(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : mn
	 * Method 설명 : 화주 조회 메인화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/itemGrpPop.action")
	public ModelAndView grp(Map<String, Object> model) {
		return new ModelAndView("winus/wmscm/WMSCM094Q1");
	}
	
	/*-
	 * Method ID : OnTimeoutReached
	 * Method 설명 : winus 세션유지 call url
	 * 작성자 : 
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/OnTimeoutReached.action")
	public ModelAndView OnTimeoutReached(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.OnTimeoutReached(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : ediyaMobileInsert
	 * Method 설명 : 모바일어플 회원가입 등록
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS030/insertediya.action")
	public ModelAndView ediyaInsert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.insertediya(model);
			m.put("ERROR", "0");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID    			 : TMSYS030Q3
     * Method 설명        	 : 피킹 작업지시 -> 사용자 조회 팝업 - 화면 
     * 작성자                     : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/TMSYS030Q3.action")
    public ModelAndView TMSYS030Q3(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/tmsys/TMSYS030Q3");
    }
    
    /**
     * Method ID    			 : listQ3
     * Method 설명        	 : 피킹 작업지시 -> 사용자 조회 팝업 - 조회 
     * 작성자                     : KSJ
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/TMSYS030/listQ3.action")
	public ModelAndView listQ3(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listQ3(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
     * Method ID     : doubleClickAuth
     * Method 설명        	 : CS관리 권한 체크 
     * 작성자                     : sing09
     * @param   model
     * @return  
     * @throws Exception 
     */
	@RequestMapping("/TMSYS030/doubleClickAuth.action")
	public ModelAndView doubleClickAuth(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		try {
			m = service.doubleClickAuth(model);
			mav = new ModelAndView("jsonView", m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}
