package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.tmsys.service.TMSYS010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.code.service.CodeMngr;

@Controller
public class TMSYS010Controller {

	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS010Service")
	private TMSYS010Service service;

	/*-
	 * Method ID   : tmsys020
	 * Method 설명 : 권한관리 화면
	 * 작성자      : chsong 04.13.02.31
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/TMSYS010.action")
	public ModelAndView tmsys020(Map<String, Object> model) {
		return new ModelAndView("winus/tmsys/TMSYS010");
	}

	/*-
	 * Method ID   : listAuth
	 * Method 설명 : 권한목록 조회
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS010/listCode.action")
	public ModelAndView listCode(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listCode(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list:", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : listTopMenu
	 * Method 설명 : 프로그램목록 조회
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS010/listDetailCode.action")
	public ModelAndView listDetailCode(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listDetailCode(model));

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list:", e);
			}
		}
		// log.info("[ listDetailCode ] mav :" + mav);
		return mav;
	}

	/*-
	 * Method ID   : saveAuth
	 * Method 설명 : 권한목록 저장
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS010/saveCode.action")
	public ModelAndView saveCode(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveCode(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save code :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		CodeMngr.reload();
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : saveTopMenu
	 * Method 설명 : 권한목록TOP 저장
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS010/saveDetailCode.action")
	public ModelAndView saveTopMenu(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveDetailCode(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save code :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}

		CodeMngr.reload();
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명  : 코드상세목록 엑셀다운로드
	 * 작성자       : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/TMSYS010/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
		try {
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
			String[][] headerEx = {{MessageResolver.getMessage("기초코드"), "0", "0", "0", "0", "200"}, {MessageResolver.getMessage("기초코드명"), "1", "1", "0", "0", "200"}, {MessageResolver.getMessage("값"), "2", "2", "0", "0", "300"},
					{MessageResolver.getMessage("순서"), "3", "3", "0", "0", "400"}};
			// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
			// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {{"BA_CODE_CD", "S"}, {"BA_CODE_NM", "S"}, {"CODE_VALUE", "S"}, {"VIEW_SEQ", "S"}};

			// 파일명 기초코드 목록
			String fileName = MessageResolver.getMessage("기초코드목록");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isWarnEnabled()) {
				log.warn("fail download excel file...", e);
			}
		}
	}
}
