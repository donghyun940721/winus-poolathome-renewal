package com.logisall.winus.tmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.tmsys.service.TMSYS060Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class TMSYS060Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS060Service")
	private TMSYS060Service service;

	/*-
	 * Method ID : mn
	 * Method 설명 : 비밀번호 변경화면
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/OpenPassword.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/tmsys/TMSYS060E1");
	}

	/*-
	 * Method ID : change
	 * Method 설명 : 비밀번호 변경
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/ChangePassword.action")
	public ModelAndView change(Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			mav = new ModelAndView("jsonView", service.save(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to change pwd :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		if (mav != null)
			mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : uFileUpload
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/uFileUpload.action")
	public ModelAndView uFileUpload(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.uFileUpload(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID   : uFileCustInfo
	 * Method 설명 : 물류센터 별 대표화주값 조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/uFileCustInfo.action")
	public ModelAndView uFileCustInfo(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.uFileCustInfo(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : uDlvTrackingInfo
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/uDlvTrackingInfo.action")
	public ModelAndView uDlvTrackingInfo(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.uDlvTrackingInfo(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : uDlvCsatChk
	 * Method 설명      : 고객설문지 제출여부 체크
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/uDlvCsatChk.action")
	public ModelAndView uDlvCsatChk(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.uDlvCsatChk(model));
		} catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to list :", e);
	        }
	    }
		return mav;
	}
	
	/*-
	 * Method ID    : uDlvCsatSave
	 * Method 설명      : 고객설문지 제출
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/uDlvCsatSave.action")
	public ModelAndView uDlvCsatSave(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.uDlvCsatSave(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update loc :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
