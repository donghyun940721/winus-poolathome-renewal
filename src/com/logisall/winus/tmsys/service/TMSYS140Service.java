package com.logisall.winus.tmsys.service;

import java.util.Map;


public interface TMSYS140Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
}
