package com.logisall.winus.tmsys.service;

import java.util.Map;

public interface TMSYS060Service {

	public Map<String, Object> save(Map<String, Object> model) throws Exception;
	public Map<String, Object> uFileUpload(Map<String, Object> model) throws Exception;
	public Map<String, Object> uFileCustInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> uDlvCsatChk(Map<String, Object> model) throws Exception;
	public Map<String, Object> uDlvTrackingInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> uDlvCsatSave(Map<String, Object> model) throws Exception;
	
}
