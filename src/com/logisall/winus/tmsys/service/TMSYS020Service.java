package com.logisall.winus.tmsys.service;

import java.util.Map;


public interface TMSYS020Service {
	/**interfacebody*/
    public Map<String, Object> selectAuthBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> listAuth(Map<String, Object> model) throws Exception;
    public Map<String, Object> listTopMenu(Map<String, Object> model) throws Exception;
    public Map<String, Object> listMenuBtn(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveAuth(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveTopMenu(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveMenuBtn(Map<String, Object> model) throws Exception;

    public Map<String, Object> deleteAuth(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteTopMenu(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteMenuBtn(Map<String, Object> model) throws Exception;
}
