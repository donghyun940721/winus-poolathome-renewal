package com.logisall.winus.tmsys.service;

import java.util.List;
import java.util.Map;

public interface TMSYS030Service {
	/**interfacebody*/
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> searchUserLc(Map<String, Object> model) throws Exception;
	public Map<String, Object> getUserDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> getDetail(Map<String, Object> model) throws Exception;
	public Map<String, Object> getRoll(Map<String, Object> model) throws Exception;
	public Map<String, Object> insert(Map<String, Object> model) throws Exception;
	public Map<String, Object> udpate(Map<String, Object> model) throws Exception;
	public Map<String, Object> delete(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> checkUserId(Map<String, Object> model) throws Exception;
	public Map<String, Object> updateLogin(Map<String, Object> model) throws Exception;
	public Map<String, Object> updateLoginHistory(Map<String, Object> model) throws Exception;
	public Map<String, Object> updateLogout(Map<String, Object> model) throws Exception;
	
	public Map<String, Object> updateReset(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectLc(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectProgramInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectUserGB(Map<String, Object> model) throws Exception;	
	public String selectScreen(Map<String, Object> model) throws Exception;
	
    public Map<String, Object> saveLang(Map<String, Object> model) throws Exception;
    public Map<String, Object> getPopupInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> localTime(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> jsonPostCross_uPcRecodeList(Map<String, Object> model) throws Exception;
    public Map<String, Object> jsonPostCross_uPcCallhistory(Map<String, Object> model) throws Exception;
    public Map<String, Object> callUserInofo(Map<String, Object> model) throws Exception;
    public Map<String, Object> callNumSave(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> getAlertMsg(Map<String, Object> model) throws Exception;
    public Map<String, Object> OnTimeoutReached(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertediya(Map<String, Object> model) throws Exception;

    public Map<String, Object> listQ3(Map<String, Object> model) throws Exception;
    
    public List<Map<String, Object>> list_menu() throws Exception;
    public Map<String, Object> doubleClickAuth(Map<String, Object> model) throws Exception;
}
