package com.logisall.winus.tmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.PDF.model.DocPickingSampleVO;
import com.logisall.winus.frm.common.PDF.model.DrbCertCipTestMasterVO;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.tmsys.service.TMSYS999Service;

@Service("TMSYS999Service")
public class TMSYS999ServiceImpl implements TMSYS999Service {
    protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * 대체 Method ID   : pdf1
     * 대체 Method 설명    : 작업지시서(피킹) sample
     * 작성자                      : chsong
     * @param user_id
     * @return user_name
     * @throws Exception
     */
    public Map<String, Object> getPdf(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        DocPickingSampleVO c = new DocPickingSampleVO();
        c.setFontPath(ConstantIF.FONT_NORMAL);
        c.setTitleFontPath(ConstantIF.FONT_TITLE);
        c.setServerPath(ConstantIF.SERVER_PDF_PATH);
        
        // c.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
        c.setFileName(model.get("langName") + "_" + model.get("dateName"));
        
        // PDFCreate pdf = new PDFCreate();
        // m = pdf.createPdf1(c);
        return m;
    }   

    /**
     * Method ID : preViewConf
     * Method 설명 : PDF 생성
     * 작성자 : 기드온
     * @param model
     * @throws Exception
     */
    public Map<String, Object> getPreViewConf(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        DrbCertCipTestMasterVO c = new DrbCertCipTestMasterVO(); 
        c.setFontPath(ConstantIF.FONT_NORMAL);
        c.setTitleFontPath(ConstantIF.FONT_TITLE);
        c.setServerPath(ConstantIF.SERVER_PDF_PATH);
        
        // c.setFileName(model.get("langName")+""+model.get("dateName")+"_sample");
        c.setFileName(model.get("langName") + "_" + model.get("dateName"));
        
//                new PDFCreate().createCertTest(c);
        // PDFCreate pdf = new PDFCreate();
        // m = pdf.createCertTest(c);
//        m = pdf.createPdf3(c.getServerPath() + c.getFileName() + ".pdf");
        return m;
    }   
    
}
