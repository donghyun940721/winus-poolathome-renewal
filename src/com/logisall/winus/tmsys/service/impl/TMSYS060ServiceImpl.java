package com.logisall.winus.tmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DESUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.tmsys.service.TMSYS060Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("TMSYS060Service")
public class TMSYS060ServiceImpl implements TMSYS060Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSYS060Dao")
    private TMSYS060Dao dao;
    
    @Resource(name = "TMSYS030Dao")
    private TMSYS030Dao dao2;
    /**
     * Method ID : save
     * Method 설명 : 비밀번호 변경
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapPswd = new HashMap<String, Object>();
        
        String pswd = (String)model.get("PWSD"); // 사용자가 입력한 패스워드
        mapPswd = (Map)dao2.detailPswd(model); // DB에 있는 패스워드
        String pswd2 =  DESUtil.dataDecrypt((String)mapPswd.get("USER_PASSWORD")); // 암호화 해제
   
        if(!pswd.equals(pswd2)){
            map.put("ERROR", "1");
            map.put("MSG", MessageResolver.getMessage("chek.idpswd"));
        }else{
            try {
                model.put("USER_PASSWORD", DESUtil.dataEncrypt(pswd2)); //DB에 있던 패스워드 다시 암호화
                model.put("USER_PSWD", DESUtil.dataEncrypt((String)model.get("C_USER_PSWD"))); //변경할 비밀번호 암호화
             
                dao.save(model);
                map.put("ERROR", "0");
                map.put("MSG", MessageResolver.getMessage("save.success"));
            } catch (Exception e) {
                throw e;
            }
        }
        return map;
    }

    /**
	 * Method ID : uFileUpload
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> uFileUpload(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
				Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
				
				String workOrdId = (String) model.get("WORK_ORD_ID");
				for (int i=0; i< ((String[])attachFileInfo).length; i++){
					Map<String, Object> modelDt = new HashMap<String, Object>();
					if(((String[])model.get("D_ATCH_FILE_NAME"))[i].length() > 0){
						// 확장자 잘라내기
						//String str          = (String) model.get("D_ATCH_FILE_NAME");
						//String tranFilename = (String) model.get("TRAN_FILENAME");

						String fileRoute = ConstantIF.FILE_ATTACH_PATH + "ATCH_FILE"+((String[])model.get("D_ATCH_FILE_ROUTE"))[i];
						String fileName  = ((String[])model.get("D_ATCH_FILE_NAME"))[i];
						String fileSize  = ((String[])model.get("FILE_SIZE"))[i];
						String fileNewNm = ((String[])model.get("TRAN_FILENAME"))[i];
						String ext       = "." + fileName.substring((fileName.lastIndexOf('.') + 1));
						String fileId    = "";
						fileId = workOrdId + "_" + CommonUtil.getLocalDateTime().substring(0, 8) + fileNewNm + ext;
						
						modelDt.put("FILE_ID"		, fileId);
						modelDt.put("ATTACH_GB"		, "USER_FILES"); // 통합 HELPDESK 업로드
						modelDt.put("FILE_VALUE"	, "uFileUpload"); // 기존코드 "tmsba230" 로
						modelDt.put("FILE_PATH"		, fileRoute); // 서버저장경로
						modelDt.put("ORG_FILENAME"	, fileNewNm + ext); // 원본파일명
						modelDt.put("FILE_EXT"		, ext); // 파일 확장자
						modelDt.put("FILE_SIZE"		, fileSize);// 파일 사이즈
						modelDt.put("WORK_ORD_ID"	, workOrdId); // 원본파일명
						dao.t2FileInsert(modelDt);
					}
				}
				
				Map<String, Object> modelRst = new HashMap<String, Object>();
	            modelRst.put("WORK_ORD_ID"	, workOrdId);
	            dao.insertInfo(modelRst);
	            
				// 저장
				//String fileMngNo = (String) dao.t2FileInsert(modelDt);
				// 리턴값 파일 id랑 파일경로
				//model.put("IMAGE_ID", fileId);
				//model.put("MNG_NO", fileMngNo);
				//dao.insertInfo(model);
			}

			map.put("MSG", MessageResolver.getMessage("insert.success"));
		} catch (Exception e) {
			throw e;
		}
		return map;
	}
	
	/*-
	 * Method ID   : uFileCustInfo
	 * Method 설명 : 
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> uFileCustInfo(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("CUST_INFO")) {
			map.put("CUST_INFO", dao.uFileCustInfo(model));
		}
		return map;
	}
    
    /**
     * Method ID   : uDlvTrackingInfo
     * Method 설명    : 
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> uDlvTrackingInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try {
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            // session 및 필요정보
            modelIns.put("I_LOGIN_ID"    	, model.get("vrLoginId"));
            modelIns.put("I_PASSWORD"  		, model.get("vrPassword"));
            modelIns.put("I_SALES_CUST_NO"  , model.get("vrSalesCustId"));
            modelIns.put("I_TRACE_ORD_ID"  	, model.get("vrTraceOrdId"));
            
            // dao
            modelIns = (Map<String, Object>)dao.pahDlvTrackingInfo(modelIns);
            ServiceUtil.isValidReturnCode("TMSYS060", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            m.put("O_CURSOR", modelIns.get("O_CURSOR"));
            m.put("errCnt", 0);
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID   : uDlvCsatChk
     * Method 설명    : 고객설문지 제출여부 체크
     * 작성자               : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> uDlvCsatChk(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
		model.put("vrSrchCsatOrdId", model.get("vrSrchCsatOrdId"));
        m.put("LIST", dao.uDlvCsatChk(model));
        return m;
    }

	/*-
	 * Method ID    : uDlvCsatSave
	 * Method 설명      : 고객설문지 제출
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
    @Override
    public Map<String, Object> uDlvCsatSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try {
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            // session 및 필요정보
            modelIns.put("I_DLV_ORD_ID"  		, model.get("vrSrchCsatOrdId"));
            modelIns.put("I_CSAT_GRADE_0"    	, model.get("chkScore"));
            modelIns.put("I_CSAT_GRADE_1"  		, model.get("chkScore1"));
            modelIns.put("I_CSAT_GRADE_2"  		, model.get("chkScore2"));
            modelIns.put("I_CSAT_GRADE_3"  		, model.get("chkScore3"));
            modelIns.put("I_CSAT_GRADE_4"  		, model.get("chkScore4"));
            modelIns.put("I_CSAT_MEMO"  		, model.get("chkCsatMemo"));
            
			dao.uDlvCsatSave(modelIns);
			ServiceUtil.isValidReturnCode("WMSSP090", String.valueOf(modelIns.get("O_MSG_CODE")),(String) modelIns.get("O_MSG_NAME"));

			m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(Exception e){
    		m.put("errCnt", -1);
    		m.put("MSG", e.getMessage());
    		throw e;
    	}
        return m;
    }
}
