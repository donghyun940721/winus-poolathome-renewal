package com.logisall.winus.tmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.tmsys.service.TMSYS010Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("TMSYS010Service")
public class TMSYS010ServiceImpl extends AbstractServiceImpl implements TMSYS010Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSYS010Dao")
    private TMSYS010Dao dao;
    
    private final static String[] CHECK_VALIDATE_TMSYS010 = {"CODE_TYPE_CD", "CODE_TYPE_NM", "CODE_TYPE", "USER_GB", "CODE_LEVEL"};
    private final static String[] CHECK_VALIDATE_TMSYS030 = {"CODE_TYPE_CD", "BA_CODE_CD", "ST_CODE_CD", "BA_CODE_NM"};

    /**
     * 
     * 대체 Method ID   : listCode
     * 대체 Method 설명 : 코드 목록 조회
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listCode(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listCode(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listDetailCode
     * 대체 Method 설명 : 코드 상세목록 조회
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listDetailCode(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.listDetailCode(model));
        return map;
    }
   
    /**
     * 
     * 대체 Method ID   : saveCode
     * 대체 Method 설명 : 코드  등록,수정,삭제
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveCode(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                //STGUBUN
                modelDt.put("selectIds"         , model.get("selectIds"));
                modelDt.put("ST_GUBUN"          , model.get("ST_GUBUN" + i));
                modelDt.put("CODE_TYPE_CD"      , model.get("CODE_TYPE_CD" + i));           // 유형코드
                modelDt.put("ORG_CODE_TYPE_CD"      , model.get("ORG_CODE_TYPE_CD" + i));           // 유형코드
                modelDt.put("CODE_TYPE_NM"      , model.get("CODE_TYPE_NM" + i));           // 유형명
                modelDt.put("CODE_TYPE"         , "B");                                     // 코드구분(기준:S,기초:B)
                modelDt.put("USER_GB"           , model.get("USER_GB" + i));                // 사용자구분(마스터:90,LOGISALL:80,고객사:70,주선사:60,운송사:50,차량:40)
                modelDt.put("CODE_LEVEL"        , model.get("CODE_LEVEL" + i));             // 코드레벨(0:삭제가능,1:마스터삭제가능,2:삭제불가)
                modelDt.put("DEL_YN"            , "N");                                     // 삭제여부
                
                modelDt.put("SS_USER_TYPE"      , model.get(ConstantIF.SS_USER_TYPE));
                modelDt.put("WORK_IP"           , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("SS_USER_NO"        , model.get(ConstantIF.SS_USER_NO));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_TMSYS010);
                    int checkCnt = dao.checkForInsertCode(modelDt);
                    if ( checkCnt > 0 ){
                        errCnt++;
                        throw new BizException(MessageResolver.getMessage("입력값중복"));                        
                        //dao.updateCode(modelDt);
                        
                    } else {
                        dao.insertCode(modelDt);
                    }
                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                	ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_TMSYS010);
                    dao.updateCode(modelDt);
                    dao.updateDetailKeyCode(modelDt);
                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.deleteCode(modelDt);
                    
                }else{
                    errCnt++;
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch (BizException be) {
            if (loger.isWarnEnabled()) {
                loger.warn("BizException Occured... : " + be.getMessage(), be);
            }
            m.put("errCnt", errCnt);
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to save code :", e);
			}
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveDetailCode
     * 대체 Method 설명 : 코드상세 등록,수정,삭제
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveDetailCode(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            String codeTypeCd = (String)model.get("CODE_TYPE_CD");
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                String stCode = ( StringUtils.isEmpty((String)model.get("ST_CODE_CD" + i)) ) ? codeTypeCd : (String)model.get("ST_CODE_CD" + i);
                //STGUBUN
                modelDt.put("selectIds"         , model.get("selectIds"));
                modelDt.put("ST_GUBUN"          , model.get("ST_GUBUN" + i));
                
                modelDt.put("CODE_TYPE_CD"      , codeTypeCd);                      // 유형코드
                modelDt.put("BA_CODE_CD"        , model.get("BA_CODE_CD" + i));     // 기초코드
                modelDt.put("ST_CODE_CD"        , stCode);                          // 기준코드
                modelDt.put("BA_CODE_NM"        , model.get("BA_CODE_NM" + i));     // 기초코드명
                modelDt.put("BA_CODE_CD_O"        , model.get("BA_CODE_CD_O" + i));     // 기초코드명
                modelDt.put("CODE_VALUE"        , model.get("CODE_VALUE" + i));     // 기초코드값
                modelDt.put("VIEW_SEQ"          , model.get("VIEW_SEQ" + i));       // 순번
                modelDt.put("DEL_YN"            , "N");                             // 삭제여부
                
                modelDt.put("SS_USER_TYPE"      , model.get(ConstantIF.SS_USER_TYPE));
                modelDt.put("WORK_IP"           , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("SS_USER_NO"        , model.get(ConstantIF.SS_USER_NO));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                	ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_TMSYS030);
                    
                    int checkCnt = dao.checkForInsertDetailCode(modelDt);
                    if ( checkCnt > 0 ){
                        errCnt++;
                        throw new BizException(MessageResolver.getMessage("입력값중복"));
                        
                    } else {
                        dao.insertDetailCode(modelDt);
                    }
                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                	ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_TMSYS030);
                    dao.updateDetailCode(modelDt);
                    
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.deleteDetailCode(modelDt);
                    
                }else{
                    errCnt++;
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
                
        }catch (BizException be) {
            if (loger.isWarnEnabled()) {
                loger.warn("BizException Occured... : " + be.getMessage(), be);
            }
            m.put("errCnt", errCnt);
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to save code :", e);
			}
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 코드상세목록 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.listDetailCode(model));
        
        return map;
    }
}
