package com.logisall.winus.tmsys.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS150Dao")
public class TMSYS150Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * Method ID : list
	 * Method 설명 : 사용자관리 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("tmsys150.list", model);
	}
	
	/**
     * Method ID : insertHeaderPopup
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertHeaderPopup(Map<String, Object> modelDt) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			
			String getReqId = (String)sqlMapClient.insert("tmsys150.insertHeaderPopup", modelDt);
			modelDt.put("REQ_ID" , getReqId);
			modelDt.put("REQ_SEQ", 0);
			sqlMapClient.insert("tmsys150.insertDetail", modelDt);
    		
			sqlMapClient.endTransaction();
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }
	
	/**
	 * Method ID : insertDetail 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object insertDetail(Map<String, Object> model) {
		return executeUpdate("tmsys150.insertDetail", model);
	}
	
	/**
	 * Method ID : updateHeader 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateHeader(Map<String, Object> model) {
		return executeUpdate("tmsys150.updateHeader", model);
	}
	
	/**
	 * Method ID : updateDetail 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateDetail(Map<String, Object> model) {
		return executeUpdate("tmsys150.updateDetail", model);
	}
	
	/**
	 * Method ID : deleteDetail 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteDetail(Map<String, Object> model) {
		return executeDelete("tmsys150.deleteDetail", model);
	}
}
