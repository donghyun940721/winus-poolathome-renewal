package com.logisall.winus.tmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS010Dao")
public class TMSYS010Dao extends SqlMapAbstractDAO {
    
protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID  : listAuth
     * Method 설명  : 권한코드 조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listCode(Map<String, Object> model) {
        return executeQueryPageWq("tmsys010.selectCode", model);
    }
    
    /**
     * Method ID  : listTopMenu
     * Method 설명  : 권한코드 탑메뉴
     * 작성자             : chsong
     * @param model
     * @return
     */
    public GenericResultSet listDetailCode(Map<String, Object> model) {
        return executeQueryPageWq("tmsys010.selectDetailCode", model);
    }
    
    
    public int checkForInsertCode(Map<String, Object> model) {
        // 중복검사
        return (Integer)executeQueryForObject("tmsys010.checkforinsert_code", model);
    }
    
    /**
     * Method ID  : insertAuth
     * Method 설명  : 권한코드 등록
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertCode(Map<String, Object> model) {
        return executeInsert("tmsys010.insertCode", model);
    }
    
    /**
     * Method ID  : insertAuth
     * Method 설명  : 권한코드 등록
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object updateCode(Map<String, Object> model) {
        return executeInsert("tmsys010.updateCode", model);
    }
    
    /**
     * Method ID  : updateDetailKeyCode
     * Method 설명  : 권한코드 등록 디테일 키
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object updateDetailKeyCode(Map<String, Object> model) {
        return executeInsert("tmsys010.updateDetailKeyCode", model);
    }
    
    /**
     * Method ID  : deleteAuth050
     * Method 설명  : 권한삭제
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object deleteCode(Map<String, Object> model) {
        return executeDelete("tmsys010.deleteCode", model);
    }
    
    public int checkForInsertDetailCode(Map<String, Object> model) {
        // 중복검사
        return (Integer)executeQueryForObject("tmsys010.checkforinsert_datailcode", model);
    }
    
    /**
     * Method ID  : insertAuth060
     * Method 설명  : 권한별TOPMENU 등록
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertDetailCode(Map<String, Object> model){
        return executeInsert("tmsys010.insertDetailCode", model);
    }

    /**
     * Method ID  : updateAuth060
     * Method 설명  : 권한별TOPMENU 수정
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object updateDetailCode(Map<String, Object> model){
        return executeUpdate("tmsys010.updateDetailCode", model);
    }   
    
    /**
     * Method ID  : deleteAuth060
     * Method 설명  : 권한별TOPMENU 삭제
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object deleteDetailCode(Map<String, Object> model) {
        return executeDelete("tmsys010.deleteDetailCode", model);
    }
    
}
