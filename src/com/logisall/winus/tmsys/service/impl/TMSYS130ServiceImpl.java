package com.logisall.winus.tmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.tmsys.service.TMSYS130Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("TMSYS130Service")
public class TMSYS130ServiceImpl implements TMSYS130Service {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS130Dao")
	private TMSYS130Dao dao;

	/**
	 * Method ID : list Method 설명 : 통합 HelpDesk 리스트 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();

		try {
			if (model.get("page") == null) {
				model.put("pageIndex", "1");
			} else {
				model.put("pageIndex", model.get("page"));
			}
			if (model.get("rows") == null) {
				model.put("pageSize", "100");
			} else {
				model.put("pageSize", model.get("rows"));
			}

			model.put("PROJECT_A", "WINUS"); // 기존코드에도 하드코딩되어 있음.

			if (model.get("DATE_TYPE").equals("1")) { // 요청일로 검색해야 될경우 시분초 넣어줘야됨
				String start = (String) model.get("DATE_START");
				String end = (String) model.get("DATE_END");
				start += " 000000";
				end += " 235959";
				model.put("DATE_START", start);
				model.put("DATE_END", end);
			}

			map.put("LIST", dao.list(model));
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}

	/**
	 * Method ID : save Method 설명 : 통합 HelpDesk 저장 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> save(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			String flag = (String) model.get("FLAG");

			if ("".equals(flag)) { // 등록
				if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
					Map<String, Object> modelDt = new HashMap<String, Object>();
					// 확장자 잘라내기
					String str = (String) model.get("D_ATCH_FILE_NAME");
					String ext = "." + str.substring((str.lastIndexOf('.') + 1));
					// 파일 ID 만들기
					int fileSeq = 1;
					String fileId = "";
					fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
					// 저장준비
					modelDt.put("FILE_ID", fileId);
					modelDt.put("ATTACH_GB", "HELPDESK"); // 통합 HELPDESK 업로드
					modelDt.put("FILE_VALUE", "tmsba230"); // 기존코드 "tmsba230" 로
															// 하드코딩되어 있음 파일구분값?
					modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE")); // 서버저장경로
					modelDt.put("ORG_FILENAME", model.get("D_ATCH_FILE_NAME")); // 원본파일명
					modelDt.put("FILE_EXT", ext); // 파일 확장자
					modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));// 파일 사이즈
					// 저장
					String fileMngNo = (String) dao.fileUpload(modelDt);
					// 리턴값 파일 id랑 파일경로
					model.put("IMAGE_ID", fileMngNo);
					model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));
				}

				if ("notice".equals(model.get("NOTICE"))) {

					model.put("D_REQ_TYPE", "6"); // 공지
					model.put("D_PRIORITY", " "); // 기존로직 빈값으로 하드코딩되어있음
					model.put("D_STATUS", "30"); // 기존로직 완료로 하드코딩되어있음
					model.put("D_PROJECT", "WINUS"); // 기존로직 WINUS으로 하드코딩되어있음
					model.put("D_CONFIRM", "N"); // 기존로직 N으로 하드코딩되어있음
					model.put("D_GRADE", "B"); // 기존로직 B로 하드코딩되어있음

					dao.insert(model);
				} else {

					model.put("D_REQ_TYPE", "1"); // 기존로직 --으로 하드코딩되어있음
					model.put("D_PRIORITY", " "); // 기존로직 빈값으로 하드코딩되어있음
					model.put("D_STATUS", "10"); // 기존로직 요청등록으로 하드코딩되어있음
					model.put("D_EMPLOYEE", " "); // 기존로직 빈값으로 하드코딩되어있음
					model.put("D_ETC", " "); // 기존로직 빈값으로 하드코딩되어있음
					model.put("D_PROJECT", "WINUS"); // 기존로직 WINUS으로 하드코딩되어있음
					model.put("D_COMMENTS", " "); // 기존로직 빈값으로 하드코딩되어있음
					model.put("D_CONFIRM", "N"); // 기존로직 N으로 하드코딩되어있음
					model.put("D_GRADE", "B"); // 기존로직 B로 하드코딩되어있음

					dao.insert(model);
				}
				map.put("MSG", MessageResolver.getMessage("insert.success"));
			} else if ("U".equals(flag)) { // 수정

				if (!"".equals(model.get("FILE_STATE"))) { // 파일첨부가 있으면
					Map<String, Object> modelDt = new HashMap<String, Object>();

					if ("".equals((String) model.get("FILE_ID"))) { // 신규
						// 확장자 잘라내기
						String str = (String) model.get("D_ATCH_FILE_NAME");
						String ext = "." + str.substring((str.lastIndexOf('.') + 1));
						// 파일 ID 만들기
						int fileSeq = 1;
						String fileId = "";
						fileId = CommonUtil.getLocalDateTime() + fileSeq + ext;
						// 저장준비
						modelDt.put("FILE_ID", fileId);
						modelDt.put("ATTACH_GB", "HELPDESK"); // 통합 HELPDESK 업로드
						modelDt.put("FILE_VALUE", "tmsba230"); // 기존코드
																// "tmsba230" 로
																// 하드코딩되어 있음
																// 파일구분값?
						modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE")); // 서버
																					// 저장경로
						modelDt.put("ORG_FILENAME", model.get("D_ATCH_FILE_NAME")); // 원본
																					// 파일명
						modelDt.put("FILE_EXT", ext); // 파일 확장자
						modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));// 파일
																			// 사이즈
						// 저장
						dao.fileUpload(modelDt);
						// 리턴값 파일 id랑 파일경로
						model.put("IMAGE_ID", fileId);
						model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));
					} else { // 수정
						String str = (String) model.get("D_ATCH_FILE_NAME");
						String ext = "." + str.substring((str.lastIndexOf('.') + 1));

						model.put("D_ATCH_FILE_NAME", model.get("FILE_ID"));
						String mngNo = dao.selectMngno(model);

						modelDt.put("FILE_PATH", model.get("D_ATCH_FILE_ROUTE"));
						modelDt.put("ORG_FILENAME", str);
						modelDt.put("FILE_EXT", ext);
						modelDt.put("FILE_SIZE", model.get("FILE_SIZE"));

						modelDt.put("MNG_NO", mngNo);
						modelDt.put("FILE_ID", model.get("D_ATCH_FILE_NAME"));
						dao.fileUpdate(modelDt);
						model.put("IMAGE_PATH", modelDt.get("FILE_PATH"));

						map.put("ERROR", "2");
					}
				}
				dao.update(model);

				map.put("MSG", MessageResolver.getMessage("update.success"));

			} else if ("D".equals(flag)) { // 삭제
				if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
					Map<String, Object> modelDt = new HashMap<String, Object>();
					String mngNo = dao.selectMngno(model);
					modelDt.put("MNG_NO", mngNo);
					modelDt.put("FILE_ID", model.get("D_ATCH_FILE_NAME"));
					dao.fileDelete(modelDt);
				}
				dao.delete(model);
				map.put("MSG", MessageResolver.getMessage("delete.success"));
			}
		} catch (Exception e) {
			throw e;
		}
		return map;
	}

	/**
	 * Method ID : listExcel Method 설명 : 통합 HelpDesk 엑셀 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("PROJECT_A", "WINUS"); // 기존코드에도 하드코딩되어 있음.

		if (model.get("DATE_TYPE").equals("1")) { // 요청일로 검색해야 될경우 시분초 넣어줘야됨
			String start = (String) model.get("DATE_START");
			String end = (String) model.get("DATE_END");
			start += " 000000";
			end += " 235959";
			model.put("DATE_START", start);
			model.put("DATE_END", end);
		}

		model.put("pageIndex", "1");
		model.put("pageSize", "60000");

		map.put("LIST", dao.list(model));

		return map;
	}

	/**
	 * Method ID : selectReqPerson Method 설명 : 셀렉트 박스 조회 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			model.put("PROJECT", "WINUS");// 프로젝트명 하드코딩되어 있음.
			map.put("ReqPerson", dao.selectReqPerson(model));
			map.put("Screen", dao.selectScreen(model));
			model.put("inKey", "STATUS"); // 진행상황
			map.put("STATUS", dao.selectSTATUS(model));
			model.put("inKey", "RY"); // 처리유형
			map.put("RY", dao.selectRY(model));
		} catch (Exception e) {
			log.error(e.toString());
			map.put("MSG", MessageResolver.getMessage("list.error"));
		}
		return map;
	}
}
