package com.logisall.winus.tmsys.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DESUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.tmsys.service.TMSYS030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;



import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.json.JSONTokener;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;


@Service("TMSYS030Service")
public class TMSYS030ServiceImpl extends AbstractServiceImpl implements TMSYS030Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSYS030Dao")
    private TMSYS030Dao dao;
    
    private final static String[] CHECK_CALLNUMBER_SAVE_TMSYS030 = {"SALES_CUST_NM", "PHONE_1"};
    
//    @Resource(name = "TMSYS020Dao")
//    private TMSYS020Dao dao2;


    /**
     * Method ID : list
     * Method 설명 : 사용자관리 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }         
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }

    /**
     * Method ID : searchUserLc
     * Method 설명 : 사용자의 물류센터를 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> searchUserLc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        try {
            int rows = dao.searchUserLcCnt(model);
//              System.out.println("mmm");
              if (model.get("page") == null) {
                  model.put("pageIndex", "1");
              } else {
                  model.put("pageIndex", model.get("page"));
              }
   
                model.put("pageSize", String.valueOf(rows));
                model.put("rows", String.valueOf(rows));
          
//                System.out.println("11");
//                System.out.println(model);
            map.put("LIST", dao.searchUserLc(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    /**
     * Method ID : detail
     * Method 설명 : 사용자정보조회(로그인)
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapPswd = new HashMap<String, Object>();
        try {
               String pswd = (String)model.get("PSWD"); // 사용자가 입력한 패스워드
               mapPswd = (Map)dao.detailPswd(model); // DB에 있는 패스워드
               String pswd2 = (mapPswd != null) ?DESUtil.dataDecrypt((String)mapPswd.get("USER_PASSWORD")) : ""; // 암호화 해제
               if(pswd.equals(pswd2)){
                   model.put("PSWD", DESUtil.dataEncrypt(pswd2));
                   map.put("DETAIL", dao.detail(model));
               }
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * Method ID : user_detail
     * Method 설명 : 사용자관리 사용자정보 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getUserDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapPswd = new HashMap<String, Object>();
        try {
            mapPswd = (Map)dao.detailPswd(model); // DB에 있는 패스워드
            String pswd2 =  DESUtil.dataDecrypt((String)mapPswd.get("USER_PASSWORD")); // 암호화 해제
            map.put("DETAIL2", dao.userDetail(model)); // 유저정보 가져오기
            map.put("PSWD2", pswd2);
            model.put("inKey", "COM01");
            model.put("USER_GB", model.get("SS_USER_GB"));
            model.put("USER_NO", model.get("SS_USER_NO"));
            
            map.put("UserGB", dao.selectUserGB(model));
//            Map<String, Object> mapData = (Map<String, Object>)dao2.selectUserAuthLevel(model);
//            model.put("AUTH_LEVEL", mapData.get("AUTH_LEVEL"));
            map.put("UserAuth", dao.selectAuthBox(model));
        }catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
   
    /**
     * Method ID : insert
     * Method 설명 : 사용자관리 등록
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> insert(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        // Map<String, Object> mapPswd = new HashMap<String, Object>();
        
        if(loger.isInfoEnabled()){
            loger.info(model);
        }
        try {
            model.put("USER_PASSWORD", DESUtil.dataEncrypt((String)model.get("PSWD")));
            String isuser = dao.userIdCheck(model);
            if ("0".equals(isuser.substring(0, 1))) {
                // 1.사용자정보를 등록한다.
                  dao.insert(model);
                
                // 2.사용자 물류센터를 등록한다.
                  for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                      Map<String, Object> modelDt = new HashMap<String, Object>();
                      modelDt.put("USER_NO"			, model.get("USER_NO"));
                      modelDt.put("D_LC_ID"			, model.get("D_LC_ID" +i));
                      modelDt.put("D_CONNECT_YN"	, model.get("D_CONNECT_YN" +i));
                      modelDt.put("D_USER_LC_SEQ"	, model.get("D_USER_LC_SEQ" +i));
                      modelDt.put("SS_CLIENT_IP"	, model.get("SS_CLIENT_IP"));
                      modelDt.put("SS_USER_NO"		, model.get("SS_USER_NO"));
                      modelDt.put("D_ITEM_GRP_ID"	, model.get("D_ITEM_GRP_ID" +i));
                      dao.insertDC(modelDt);
                  }

                map.put("MSG", MessageResolver.getMessage("insert.success"));
                map.put("USER_NO", model.get("USER_NO"));
                map.put("USER_ID", model.get("USER_ID"));

            } else {
                map.put("MSG", MessageResolver.getMessage("userid.check"));
            }
        } catch (Exception e) {
            throw e;
        }
        return map;
    }

    /**
     * Method ID : udpate
     * Method 설명 : 사용자관리 수정
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> udpate(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(loger.isInfoEnabled()){
            loger.info(model);
        }
        
        try {
            model.put("USER_PASSWORD", DESUtil.dataEncrypt((String)model.get("PSWD")));
         // 1.사용자정보를 수정한다.
            dao.update(model);
            
         // 2.사용자 물류센터를 삭제한다.
            dao.deleteDC(model);
         // 3.사용자 물류센터를 추가한다.
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                
                modelDt.put("USER_NO"		, model.get("USER_NO"));
                modelDt.put("D_LC_ID"		, model.get("D_LC_ID" +i));
                modelDt.put("D_CONNECT_YN"	, model.get("D_CONNECT_YN" +i));
                modelDt.put("D_USER_LC_SEQ"	, model.get("D_USER_LC_SEQ" +i));
                modelDt.put("SS_CLIENT_IP"	, model.get("SS_CLIENT_IP"));
                modelDt.put("SS_USER_NO"	, model.get("SS_USER_NO"));
                modelDt.put("D_ITEM_GRP_ID"	, model.get("D_ITEM_GRP_ID" +i));
               
                dao.insertDC(modelDt);
                   
            }
            
            map.put("USER_NO", model.get("USER_NO"));
            map.put("USER_ID", model.get("USER_ID"));
        
            map.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (Exception e) {
            throw e;
        }

        return map;
    }

    /**
     * Method ID : delete
     * Method 설명 : 사용자관리 삭제 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        try {
            // 1.사용자아이디에 대한 사용자별윈도우권한(USER_WINDOW_ROLE)을 삭제한다.
            //   dao.deleteUserWindowRole(model);

            // 2.사용자정보를 삭제한다.          
            dao.delete(model);
            map.put("MSG", MessageResolver.getMessage("delete.success"));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("delete.error"));
        }
        return map;
    }
 
    /**
     * Method ID : userIdCheck
     * Method 설명 : 대체 메소드에 대한 설명을 기술하시오.
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> checkUserId(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        String isuser = dao.userIdCheck(model);
        map.put("ISUSER", isuser.substring(0,1));
        map.put("USERID", isuser.substring(1));
        return map;
    }
    
    /**
     * Method ID : loginUpdate
     * Method 설명 : 로그인시 사용자 접속여부 업데이트 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateLogin(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        dao.loginUpdate(model); // 접속 Y 업데이트
        

        return map;
    }
    
    /**
     * Method ID : loginHistory
     * Method 설명 : 접속 히스토리 생성
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateLoginHistory(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        String conYn = dao.selectConnectYN(model);// 접속여부 검색
        model.put("CON_YN", conYn);
        dao.loginInsert(model); // 접속 히스토리 생성

        return map;
    }
    
    /**
     * Method ID : logoutUpdate
     * Method 설명 : 로그인아웃시 사용자 로그정보 업데이트 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateLogout(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        dao.logoutUpdate(model); // 접속 N 업데이트  

        return map;
    }
    
    /**
     * Method ID : reset
     * Method 설명 : 비밀번호 초기화
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateReset(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapPswd = new HashMap<String, Object>();
        String pswd ="1111";
        model.put("USER_PASSWORD", DESUtil.dataEncrypt(pswd));
        dao.reset(model);
        
        mapPswd = (Map)dao.detailPswd(model); // DB에 있는 패스워드
        String pswd2 =  DESUtil.dataDecrypt((String)mapPswd.get("USER_PASSWORD")); // 암호화 해제       
        map.put("PSWD2", pswd2);
        map.put("MSG", MessageResolver.getMessage("비밀번호가초기화되었습니다"));
        return map;
    }
    
    /**
     * Method ID : selectLc
     * Method 설명 : 멀티 물류센터 처리 
     * 작성자 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectLc(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LCINFO", dao.selectLc(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectProgramInfo
     * Method 설명 : 프로그램ID추출 처리 
     * 작성자 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectProgramInfo(Map<String, Object> model) throws Exception{
        Map<String, Object> programInfo = dao.selectProgramInfo(model);
        return programInfo;
    }
    

    /**
     * Method ID : selectUserGB
     * Method 설명 : 셀렉트박스 유저구분
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectUserGB(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            model.put("inKey", "COM01");
            model.put("USER_GB", model.get("SS_USER_GB"));
            map.put("UserGB", dao.selectUserGB(model));
//            Map<String, Object> mapData = (Map<String, Object>)dao2.selectUserAuthLevel(model);
//            model.put("AUTH_LEVEL", mapData.get("AUTH_LEVEL"));
            map.put("UserAuth", dao.selectAuthBox(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : selectScreen
     * Method 설명 : 사용자 초기 화면 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public String selectScreen(Map<String, Object> model) throws Exception {
        String screenId = dao.selectScreen(model);
        return screenId;
    }
    
    /**
     * Method ID : detailRoll
     * Method 설명 : 권한 조회
     * 작성자 : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getRoll(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("C_MENU_ID", model.get("PROGRAM_ID"));
        map.put("P_MENU_ID", model.get("P_MENU_ID"));
        
        try {
            map.put("windowRoll", dao.detailRoll(model));

        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID  : saveLang
     * Method 설명  : 언어변경시 유저정보 언어 체인지
     * 작성자             : chSong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveLang(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        dao.langUpdate(model); // 언어 업데이트  

        return map;
    }
    
    /**
     * Method ID : getPopupInfo
     * Method 설명 : 팝업정보 조회
     * 작성자 : kwt
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> getPopupInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("POPLIST", dao.selectPopInfo(model));

        } catch (Exception e) {
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }    
    
    public Map<String, Object> localTime(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("TIME")) {
			map.put("TIME", dao.localTime(model));
		}else if (srchKey.equals("SSID")) {
			map.put("SSID", dao.getRefreshSessionId(model));
		}
		return map;
	}
    
    public Map<String, Object> jsonPostCross_uPcRecodeList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try{
        	String paramId   = (String) model.get("id");
            String paramPass = (String) model.get("pass");
        	String body      = "id="+paramId+"&pass="+paramPass;
        	URL url          = new URL("https://centrex.uplus.co.kr/RestApi/recordlist");

        	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        	connection.setRequestMethod("POST");
        	connection.setDoInput(true);
        	connection.setDoOutput(true);
        	connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        	OutputStream os = connection.getOutputStream();
        	os.write( body.getBytes("euc-kr") );
        	os.flush();
        	os.close();

        	BufferedReader reader = new BufferedReader( new InputStreamReader( connection.getInputStream()));
        	StringBuffer buffer = new StringBuffer(); 

            int read = 0; 
            char[] cbuff = new char[1024]; 
            
            while ((read = reader.read(cbuff)) > 0){
            	buffer.append(cbuff, 0, read); 
            }
            
        	reader.close();
        	map.put("DS", buffer.toString());
        } catch(Exception e){
        	map.put("DS", "ERR");
        }
    	return map;
	}
    
    public Map<String, Object> jsonPostCross_uPcCallhistory(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try{
        	String paramId       = (String) model.get("id");
            String paramPass     = (String) model.get("pass");
            String paramPage     = (String) model.get("page");
            String paramCallType = (String) model.get("calltype");
        	String body      = "id="+paramId+"&pass="+paramPass+"&page="+paramPage+"&calltype="+paramCallType;
        	URL url          = new URL("https://centrex.uplus.co.kr/RestApi/callhistory");

        	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        	connection.setRequestMethod("POST");
        	connection.setDoInput(true);
        	connection.setDoOutput(true);
        	connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        	OutputStream os = connection.getOutputStream();
        	os.write( body.getBytes("euc-kr"));
        	os.flush();
        	os.close();

        	BufferedReader reader = new BufferedReader( new InputStreamReader( connection.getInputStream()));
        	StringBuffer buffer = new StringBuffer(); 

            int read = 0; 
            char[] cbuff = new char[1024]; 
            
            while ((read = reader.read(cbuff)) > 0){
            	buffer.append(cbuff, 0, read); 
            }
            
        	reader.close();
        	map.put("DS", buffer.toString());
        	//map.put("DS", reader.readLine().toString());
        } catch(Exception e){
        	map.put("DS", "ERR");
        }
    	return map;
	}
    
    /**
     * Method ID    : callUserInofo
     * Method 설명      : 청구단가계약 조회
     * 작성자                 : chsong
     * @param   model
     * @return 
     * @throws Exception 
     */
     @Override
     public Map<String, Object> callUserInofo(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         if(model.get("page") == null) {
             model.put("pageIndex", "1");
         } else {
             model.put("pageIndex", model.get("page"));
         }
         if(model.get("rows") == null) {
             model.put("pageSize", "20");
         } else {
             model.put("pageSize", model.get("rows"));
         }
         map.put("LIST", dao.callUserInofo(model));
         return map;
     }
     
     /**
      * 
      * 대체 Method ID   : callNumSave
      * 대체 Method 설명    : 고객 저장
      * 작성자                      : chsong
      * @param model
      * @return
      * @throws Exception
      */
     @Override
     public Map<String, Object> callNumSave(Map<String, Object> model) throws Exception {
         Map<String, Object> m = new HashMap<String, Object>();
         int errCnt = 0;
         try{
             Map<String, Object> modelDt = new HashMap<String, Object>();
             modelDt.put("selectIds" 		, model.get("selectIds"));
             modelDt.put("SALES_CUST_NM"    , (String) model.get("SALES_CUST_NM"+0));
             modelDt.put("PHONE_1"     		, (String) model.get("PHONE_1"+0));
             modelDt.put("ZIP"     			, (String) model.get("ZIP"+0));
             modelDt.put("ADDR"     		, (String) model.get("ADDR"+0));
             modelDt.put("ETC1"    			, (String) model.get("ETC1"+0));
             modelDt.put("LC_ID"    		, model.get(ConstantIF.SS_SVC_NO));
             modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
             ServiceUtil.checkInputValidation(modelDt, CHECK_CALLNUMBER_SAVE_TMSYS030);
             dao.callNumSave(modelDt);
             m.put("errCnt", errCnt);
             m.put("MSG", MessageResolver.getMessage("save.success"));
             
         } catch (BizException be) {
             if (log.isInfoEnabled()) {
                 log.info(be.getMessage());
             }
             m.put("MSG", be.getMessage());
             
         } catch(Exception e){
             throw e;
         }
         return m;
     }
     
     public Map<String, Object> getAlertMsg(Map<String, Object> model) throws Exception {
 		Map<String, Object> map = new HashMap<String, Object>();
 		Map<String, Object> modelDt = new HashMap<String, Object>();
 		String srchKey = (String) model.get("srchKey");
 		if (srchKey.equals("GETAMSG")) {
 			map.put("GETAMSG", dao.getAlertMsg(model));
 			
 			modelDt.put("KEY" 		, model.get("KEY"));
 			modelDt.put("USER" 		, model.get("USER"));
 			dao.testSaveMsg(modelDt);
 		}
 		return map;
 	}
     
     /**
      * 
      * 대체 Method ID		: OnTimeoutReached
      * 대체 Method 설명	: winus 세션유지 call url
      * 작성자                     	: 
      * @param model
      * @return
      * @throws Exception
      */
	public Map<String, Object> OnTimeoutReached(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		model.put("LC_ID"	, model.get(ConstantIF.SS_SVC_NO));
		model.put("USER_NO"	, model.get(ConstantIF.SS_USER_NO));
		map.put("TIMEOUT", dao.OnTimeoutReached(model));
		return map;
	}
	
	/**
     * 
     * 대체 Method ID	: crossDomainHttpWs
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String cMethod	= (String)model.get("cMethod");
        String cUrl		= (String)model.get("cUrl");
        String sUrl		= "https://52.79.206.98:5101/" + cUrl;
        String jsonString		= (String)model.get("jsonString");
        String escapedJson		= StringEscapeUtils.unescapeHtml(jsonString);
        JSONTokener tokener		= new JSONTokener(escapedJson);
        JSONObject jsonObject 	= new JSONObject(tokener);
        
        try{
        	//ssl disable
        	disableSslVerification();
	        //System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass		= "winus01" + ":" + "winus01";
        	String basicAuth	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod(cMethod);
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = jsonObject.toString();
			//System.out.println("param jsonInputString : " + jsonInputString);
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()){
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            System.out.println(response.toString());
	            
	            //JSONObject jsonData = new JSONObject(response.toString());
	            m.put("RST"	, response.toString());
	        }
	        
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	public Map<String, Object> insertediya(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> resultMap = new HashMap<String, Object>();
        // Map<String, Object> mapPswd = new HashMap<String, Object>();
        
        if(loger.isInfoEnabled()){
            loger.info(model);
        }
        try {
            model.put("USER_PASSWORD", DESUtil.dataEncrypt((String)model.get("PSWD")));
            String isuser = dao.userIdCheck(model);
            if ("0".equals(isuser.substring(0, 1))) {
                // 1.사용자정보를 등록한다.
                
            	map.putAll(model);
                map.put("MSG", MessageResolver.getMessage("insert.success"));
                map.put("USER_NO", model.get("USER_NO"));
                map.put("USER_ID", model.get("USER_ID"));
                map.put("SS_USER_NO", "0000000000");
                map.put("ITEM_FIX_YN", "N");
                
                dao.insert(map);
                resultMap.put("ERROR", 0);
            } else {
            	resultMap.put("ERROR", 1);
            	resultMap.put("MSG", MessageResolver.getMessage("userid.check"));
            }
        } catch (Exception e) {
            throw e;
        }
        return resultMap;
    }
	
    /**
     * Method ID		 : listQ3
     * Method 설명 	 : 피킹 작업지시 -> 사용자 조회 팝업 - 화면 
     * 작성자 			 : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }         
            map.put("LIST", dao.listQ3(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }

        return map;
    }
    
	
	
	/**
	 * 대체 Method ID   : list
	 * 대체 Method 설명 : 메뉴명 딕셔너리
	 * 작성자      : kimzero
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public List<Map<String, Object>> list_menu() throws Exception {
		List<Map<String, Object>> list = new ArrayList<>();
		
		list = dao.list_menu();
		
		return list;
	}
	
	/**
	 * 대체 Method ID   : doubleClickAuth
	 * Method 설명      : CS관리 권한 체크 
     * 작성자                 : sing09
     * @param   model
	 * @return
	 * @throws Exception
	 */
	public  Map<String, Object> doubleClickAuth(Map<String, Object> model) throws Exception {

    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
        	String CheckMsg = dao.doubleClickAuth(model);
        	
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", CheckMsg);
    		m.put("errCnt", 0);
    	} catch(Exception e) {
    		throw e;
    	}
    	return m;
	}
}
