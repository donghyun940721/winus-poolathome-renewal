package com.logisall.winus.tmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS130Dao")
public class TMSYS130Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	   /**
     * Method ID : list
     * Method 설명 : 통합 HelpDesk 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("tmsba900.list", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("tmsba900.help_insert", model);
    }
    
    /**
     * Method ID : update
     * Method 설명 : 통합 HelpDesk 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object update(Map<String, Object> model) {
        return executeInsert("tmsba900.help_update", model);
    }
    
    /**
     * Method ID : delete
     * Method 설명 : 통합 HelpDesk 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object delete(Map<String, Object> model) {
        return executeDelete("tmsba900.help_delete", model);
    }
    
    
    /**
     * Method ID : fileUpload
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileUpload(Map<String, Object> model) {
        return executeInsert("tmsys900.fileInsert", model);
    }

    /**
     * Method ID : selectMngno
     * Method 설명 : 파일 관리번호 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public String selectMngno(Map<String, Object> model) {
        return (String)executeView("tmsys900.select_mngNo", model);
    }

    /**
     * Method ID : fileDelete
     * Method 설명 : 파일 삭제
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileDelete(Map<String, Object> model) {
        return executeDelete("tmsys900.fileDelete", model);
    }
    
    /**
     * Method ID : fileUpdate
     * Method 설명 : 파일 수정
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileUpdate(Map<String, Object> model) {
        return executeUpdate("tmsys900.fileUpdate", model);
    }
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 요청자 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectReqPerson(Map<String, Object> model){
        return executeQueryForList("tmsba900.req_person", model);
    }
    
    /**
     * Method ID : selectReqPerson
     * Method 설명 : 화면명 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectScreen(Map<String, Object> model){
        return executeQueryForList("tmsba900.screen", model);
    }
    
    /**
     * Method ID : selectSTATUS
     * Method 설명 : 진행상황 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectSTATUS(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID : selectRY
     * Method 설명 : 처리유형 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectRY(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
}
