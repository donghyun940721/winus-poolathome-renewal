package com.logisall.winus.tmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS060Dao")
public class TMSYS060Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * Method ID : save
	 * Method 설명 : 비밀번호 변경
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public int save(Map<String, Object> model) {
		return executeUpdate("tmsys090.pswdChange", model);
	}

	/**
     * Method ID : wmssp010t2Tmsys900Insert
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object t2FileInsert(Map<String, Object> model) {
        return executeInsert("wmssp010t2.t2FileInsert", model);
    }
    
    /**
     * Method ID  : uFileCustInfo
     * Method 설명  : uFileCustInfo
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object uFileCustInfo(Map<String, Object> model){
        return executeQueryForList("wmssp010t2.uFileCustInfo", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertInfo(Map<String, Object> model) {
        return executeInsert("wmssp010.etcImgInfoSave", model);
    }
    
    /**
     * Method ID    : uDlvTrackingInfo
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object uDlvTrackingInfo(Map<String, Object> model){
        executeUpdate("pk_wmsif101.sp_sweettracler_dlv_trace_qry_web_ui", model);
        return model;
    }
    
    /**
     * Method ID    : pahDlvTrackingInfo
     * Method 설명      : 
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public Object pahDlvTrackingInfo(Map<String, Object> model){
    	executeUpdate("pk_wmsif101.sp_dlv_trace_qry", model);
    	return model;
    }
    
    /**
     * Method ID    : uDlvCsatChk
  	 * Method 설명 : 고객만족도 서비스 제출 체크
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public GenericResultSet uDlvCsatChk(Map<String, Object> model) {
 	    return executeQueryWq("wmssp090.uDlvCsatChk", model);
 	}
    
  	/**
  	 * Method ID : uDlvCsatSave
  	 * Method 설명 : 고객만족도 서비스 고객 제출 정보
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object uDlvCsatSave(Map<String, Object> model) {
  		return executeUpdate("pk_wmssp090.sp_dlv_csat_save", model);
  	}
}
