package com.logisall.winus.tmsys.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS030Dao")
public class TMSYS030Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * Method ID : list
	 * Method 설명 : 사용자관리 리스트 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("tmsys090.list", model);
	}
	
	/**
	 * Method ID : searchUserLc
	 * Method 설명 : 사용자의 물류센터를 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet searchUserLc(Map<String, Object> model) {
	    return executeQueryPageWq("tmsys091.searchUserLc", model);
	}
	
	/**
     * Method ID : searchUserLcCnt
     * Method 설명 : 사용자의 물류센터를 카운트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Integer searchUserLcCnt(Map<String, Object> model) {
        return (Integer) executeView("tmsys091.searchLcCnt", model);
    }


	/**
	 * Method ID : detail
	 * Method 설명 : 사용자정보조회(로그인)
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object detail(Map<String, Object> model) {
		return executeView("tmsys090.detail", model);
	}
	
	/**
	 * Method ID : detail
	 * Method 설명 : 사용자관리 상세 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object userDetail(Map<String, Object> model) {
	    return executeView("tmsys090.user_detail", model);
	}
	
	
	/**
	 * Method ID : detailPswd
	 * Method 설명 : 사용자 패스워드 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object detailPswd(Map<String, Object> model) {
		return executeView("tmsys090.detailPswd", model);
	}
	
	/**
	 * 
	 * Method ID   : detailPtnr
	 * Method 설명 : 협력업체 정보
	 * 작성자      : Park Tae Young(박태영)
	 * @param model
	 * @return
	 */
	public Object detailPtnr(Map<String, Object> model) {
		return executeView("biz_partner.detail", model);
	}


	/**
	 * Method ID : insert
	 * Method 설명 : 사용자관리 등록
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("tmsys090.userInsert", model);
	}

	
	/**
	 * Method ID : update
	 * Method 설명 : 사용자관리 수정
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public int update(Map<String, Object> model) {
		return executeUpdate("tmsys090.userUpdate", model);
	}

	/**
     * Method ID : reset
     * Method 설명 : 사용자 비밀번호 초기화
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public int reset(Map<String, Object> model) {
        return executeUpdate("tmsys090.pswdReset", model);
    }
    
	/**
	 * Method ID : delete
	 * Method 설명 : 사용자관리 삭제
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public int delete(Map<String, Object> model) {
		return executeUpdate("tmsys090.userDelete", model);
	}
	/**
	 * Method ID : deleteDC
	 * Method 설명 : 사용자물류센터 삭제
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public int deleteDC(Map<String, Object> model) {
	    return executeUpdate("tmsys091.delete", model);
	}
	
	
    /**
     * Method ID : insertDC
     * Method 설명 : 사용자물류센터 추가
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertDC(Map<String, Object> model) {
        return executeInsert("tmsys091.insert", model);
    }
    
 
	/**
	 * 
	 * Method ID   : insert
	 * Method 설명 : 사용자별 윈도우권한관리 등록
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public Object insertUserWindowRole(Map<String, Object> model) {
		return executeInsert("user_window_role.insertUserID", model);
	}

	/**
	 * 
	 * Method ID   : delete
	 * Method 설명 : 사용자별 윈도우권한관리 삭제
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public int deleteUserWindowRole(Map<String, Object> model) {
		return executeDelete("user_window_role.deleteUserID", model);
	}

	/**
	 * 
	 * Method ID   : pwdCheck
	 * Method 설명 : 비밀번호 체크
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public String pwdCheck(Map<String, Object> model) {
		return (String)executeView("user_info.pwdCheck", model);
	}
	
	/**
	 * Method ID : userIdCheck
	 * Method 설명 : 사용자ID 중복 체크
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public String userIdCheck(Map<String, Object> model) {
		return (String)executeView("TMSYS090.userIdCheck", model);
	}

	/**
	 * 
	 * Method ID   : detailRoll
	 * Method 설명 : 로그인 시 첫페이지 권한조회
	 * 작성자      : choi
	 * @param model
	 * @return
	 */
	public Object detailRoll(Map<String, Object> model) {
		return executeView("user_info.detailRoll", model);
	}
	
	public GenericResultSet stndHsCatgCombo(Map<String, Object> model) {
		return executeQueryPageWq("hs_mst.stndHsCatgCombo", model);
	}
	

	/**
	 * Method ID : loginUpdate
	 * Method 설명 : 로그인시 사용자 정보 업데이트(접속 Y)
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public int loginUpdate(Map<String, Object> model) {
		return executeUpdate("tmsys090.loginUpdate", model);
	}	
	
	/**
     * Method ID : logoutUpdate
     * Method 설명 : 로그인아웃시 사용자 정보 업데이트(접속 N)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public int logoutUpdate(Map<String, Object> model) {
        return executeUpdate("tmsys090.logoutUpdate", model);
    }
    
    /**
     * Method ID : loginInsert
     * Method 설명 : 로그인시 사용자 정보 업데이트(로그정보 관련)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public int loginInsert(Map<String, Object> model) {
        return executeUpdate("tmsys099.loginInsert", model);
    }   

    /**
     * Method ID : selectLc
     * Method 설명 : 로그인시 사용자 물류센터 조회
     * 작성자 : chSong
     * @param model
     * @return
     */
    public Object selectLc(Map<String, Object> model){
        return executeQueryForList("tmsys091.selectLc", model);
    }
    
    /**
     * Method ID : selectProgramId
     * Method 설명 : 권한조회에 필요한 프로그램아이디 추출
     * 작성자 : chSong
     * @param model
     * @return
     */
    public Map<String, Object> selectProgramInfo(Map<String, Object> model){
        return (Map<String, Object> )executeView("tmsys080.selectProgramInfo", model);
    }
    
  
    /**
     * Method ID : selectUserGB
     * Method 설명 : 사용자구분 셀렉트 박스
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectUserGB(Map<String, Object> model){
        return executeQueryForList("tmsyms030.selectBox", model);
    }
    
    /**
     * Method ID  : listAuth
     * Method 설명  : 권한 셀렉트 박스
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectAuthBox(Map<String, Object> model) {
        return executeQueryForList("tmsys050.selectAuthBox", model);
    }
    
    /**
     * Method ID : selectScreen
     * Method 설명 : 사용자 초기화면 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public String selectScreen(Map<String, Object> model){
        return (String)executeView("tmsys080.selectScreen", model);
    }
    
    /**
     * Method ID : selectConnectYN
     * Method 설명 : 접속여부 검색
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public String selectConnectYN(Map<String, Object> model) {
        return (String)executeView("TMSYS090.connectYN", model);
    }
    
    /**
     * Method ID   : langUpdate
     * Method 설명    : 언어 업데이트
     * 작성자               : chSong
     * @param model
     * @return
     */
    public int langUpdate(Map<String, Object> model) {
        return executeUpdate("tmsys090.langUpdate", model);
    }
    

    /*-
     * Method ID : selectPopInfo
     * Method 설명 : 사용자에게 보여줄 팝업정보를 취득 
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public Object selectPopInfo(Map<String, Object> model){
        return executeQueryForList("tmsys030.selectPopInfo", model);
    }
    
    /**
     * Method ID  : localTime
     * Method 설명  : localTime
     * 작성자             : 
     * @param model
     * @return
     */
    public Object localTime(Map<String, Object> model){
        return executeQueryForList("tmsys030.localTime", model);
    }
    
    /**
     * Method ID  : getRefreshSessionId
     * Method 설명  : getRefreshSessionId
     * 작성자             : 
     * @param model
     * @return
     */
    public Object getRefreshSessionId(Map<String, Object> model){
        return executeQueryForList("tmsys030.getRefreshSessionId", model);
    }
    
    /**
     * Method ID    : callUserInofo
     * Method 설명      : 청구단가계약 조회
     * 작성자                 : chsong
     * @param   model
     * @return  
     */
     public GenericResultSet callUserInofo(Map<String, Object> model) {
         return executeQueryPageWq("tmsys030t1.callUserInofo", model);
     }
     
     /**
 	 * Method ID : callNumSave 
 	 * Method 설명 : 고객관리 등록 
 	 * 작성자 : chsong
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public Object callNumSave(Map<String, Object> model) {
 		return executeInsert("tmsys030t1.callNumSave", model);
 	}
 	
 	/**
     * Method ID  : getAlertMsg
     * Method 설명  : getAlertMsg
     * 작성자             : 
     * @param model
     * @return
     */
    public Object getAlertMsg(Map<String, Object> model){
        return executeQueryForList("tmsys030.getAlertMsg", model);
    }
   
 	/**
     * Method ID    : testSaveMsg
     * Method 설명      : testSaveMsg
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object testSaveMsg(Map<String, Object> model) {
        return executeUpdate("tmsys030t1.testSaveMsg", model);
    }
    
    /**
     * Method ID  : OnTimeoutReached
     * Method 설명  : winus 세션유지 call url
     * 작성자             : 
     * @param model
     * @return
     */
    public Object OnTimeoutReached(Map<String, Object> model){
        return executeQueryForList("tmsys030.OnTimeoutReached", model);
    }
    
    
	/**
	 * Method ID 		: listQ3
	 * Method 설명   :  피킹 작업지시 -> 사용자 조회 팝업 - 조회 
	 * 작성자			: KSJ
	 * @param model
	 * @return
	 */
	public GenericResultSet listQ3(Map<String, Object> model) {
		return executeQueryPageWq("tmsys090.listQ3", model);
	}
	
    
    public List<Map<String,Object>> list_menu() {
		return executeQueryForList("list_menu.menuDict", null);
	}
    
    /**
     * Method ID    : doubleClickAuth
     * Method 설명      : CS관리 권한 체크 
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public String doubleClickAuth(Map<String, Object> model) {
		return (String)executeView("tmsys030.doubleClickAuth", model);
	}
}
