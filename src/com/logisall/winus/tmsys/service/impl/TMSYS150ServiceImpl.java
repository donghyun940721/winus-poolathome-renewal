package com.logisall.winus.tmsys.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DESUtil;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.tmsys.service.TMSYS150Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

import java.io.BufferedReader;
import java.io.OutputStream;

import java.io.InputStreamReader;

import java.net.HttpURLConnection;

import java.net.URL;


@Service("TMSYS150Service")
public class TMSYS150ServiceImpl extends AbstractServiceImpl implements TMSYS150Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSYS150Dao")
    private TMSYS150Dao dao;
    
    private final static String[] CHECK_CALLNUMBER_SAVE_TMSYS150 = {"SALES_CUST_NM", "PHONE_1"};
    
//    @Resource(name = "TMSYS020Dao")
//    private TMSYS020Dao dao2;


    /**
     * Method ID : list
     * Method 설명 : 사용자관리 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }         
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveT1
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveT1(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("REQ_ID" 			, model.get("REQ_ID"));
            modelDt.put("REQ_SEQ" 			, model.get("REQ_SEQ"));
            
            modelDt.put("PROJECT" 			, model.get("PROJECT"));
            modelDt.put("TITLE" 			, model.get("TITLE"));
            modelDt.put("TITLE_SUB" 		, model.get("TITLE_SUB"));
            modelDt.put("CONTENTS" 			, model.get("CONTENTS"));
            modelDt.put("COMMENTS" 			, model.get("COMMENTS"));
            modelDt.put("EXP_DATE_FROM" 	, model.get("EXP_DATE_FROM"));
            modelDt.put("EXP_DATE_TO" 		, model.get("EXP_DATE_TO"));
            modelDt.put("END_DATE" 			, model.get("END_DATE"));
            modelDt.put("REQ_PERSON" 		, model.get("REQ_PERSON"));
            modelDt.put("EMPLOYEE" 			, model.get("EMPLOYEE"));
            modelDt.put("STATUS" 			, model.get("STATUS"));
            modelDt.put("ETC" 				, model.get("ETC"));
            modelDt.put("PROGRESS" 			, model.get("PROGRESS"));
		    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
            
            if("INSERT".equals(model.get("ST_GUBUN"))){
            	dao.insertHeaderPopup(modelDt);
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("REQ_ID" 			, model.get("REQ_ID"));
            modelDt.put("REQ_SEQ" 			, model.get("REQ_SEQ"));
            
            modelDt.put("PROJECT" 			, model.get("PROJECT"));
            modelDt.put("TITLE" 			, model.get("TITLE"));
            modelDt.put("TITLE_SUB" 		, model.get("TITLE_SUB"));
            modelDt.put("CONTENTS" 			, model.get("CONTENTS"));
            modelDt.put("COMMENTS" 			, model.get("COMMENTS"));
            modelDt.put("EXP_DATE_FROM" 	, model.get("EXP_DATE_FROM"));
            modelDt.put("EXP_DATE_TO" 		, model.get("EXP_DATE_TO"));
            modelDt.put("END_DATE" 			, model.get("END_DATE"));
            modelDt.put("REQ_PERSON" 		, model.get("REQ_PERSON"));
            modelDt.put("EMPLOYEE" 			, model.get("EMPLOYEE"));
            modelDt.put("STATUS" 			, model.get("STATUS"));
            modelDt.put("ETC" 				, model.get("ETC"));
            modelDt.put("PROGRESS" 			, model.get("PROGRESS"));
		    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
            
            if("INSERT".equals(model.get("ST_GUBUN"))){
                dao.insertDetail(modelDt);
            }else if("UPDATE".equals(model.get("ST_GUBUN"))){
            	if("0".equals(model.get("REQ_SEQ"))){
            		dao.updateHeader(modelDt);
            	}else{
            		dao.updateDetail(modelDt);
            	}
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : delete
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("REQ_ID" 			, model.get("REQ_ID"));
            modelDt.put("REQ_SEQ" 			, model.get("REQ_SEQ"));
            
            if("DELETE".equals(model.get("ST_GUBUN"))){
            	dao.deleteDetail(modelDt);
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 고객관리 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    }
}
