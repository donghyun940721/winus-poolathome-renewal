package com.logisall.winus.tmsys.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TMSYS160Dao")
public class TMSYS160Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectZone
     * Method 설명  : Zone 데이터셋
     * 작성자             : MonkeySeok
     * @param model
     * @return
     */
    public Object selectZone(Map<String, Object> model){
        return executeQueryForList("wmsms081.selectZone", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 사용자물류센터관리 조회
     * 작성자             : MonkeySeok
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("tmsys160.list", model);
    }
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 사용자물류센터관리 등록
     * 작성자                 : MonkeySeok
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("tmsys160.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 사용자물류센터관리 수정
     * 작성자                 : MonkeySeok
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("tmsys160.update", model);
    }  
    
    /**
     * Method ID    : delete
     * Method 설명      : 사용자물류센터관리 삭제
     * 작성자                 : MonkeySeok
     * @param   model
     * @return  Object
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("tmsys160.delete", model);
    }
        
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistDataLcId(Map<String, Object> model) {
        return (String)executeView("tmsys160.selectExistLcId", model);
    }	    
}
