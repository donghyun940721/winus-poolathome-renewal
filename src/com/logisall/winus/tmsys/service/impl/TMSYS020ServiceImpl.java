package com.logisall.winus.tmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.tmsys.service.TMSYS020Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("TMSYS020Service")
public class TMSYS020ServiceImpl extends AbstractServiceImpl implements TMSYS020Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "TMSYS020Dao")
    private TMSYS020Dao dao;
    
    /**
     * 대체 Method ID    : selectAuthBox
     * 대체 Method 설명      : 거래처별 권한 동적 selectbox 
     * 작성자                        : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectAuthBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.selectAuthBox(model));
        return map;
    }
    
    /**
     * 대체 Method ID    : listAuth
     * 대체 Method 설명      : 권한코드 목록 조회
     * 작성자                        : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listAuth(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listAuth(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listTopMenu
     * 대체 Method 설명 : 권한코드 목록 조회
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listTopMenu(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listTopMenu(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listMenuBtn
     * 대체 Method 설명 : 권한코드 목록 조회
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listMenuBtn(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }        
        //좌측 하단 메뉴리스트에서 선택한 권한코드에 해당하는 권한 레벨을 가져온다.
        model.put("AUTH_LEVEL", dao.selectAuthLevel(model));
        
        map.put("LIST", dao.listMenuBtn(model));
        return map;
    }

    /**
     * Method ID   : deleteAuth
     * Method 설명    : 권한 삭제
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deleteAuth(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("delete.success");
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"  + i));
                modelDt.put("CLIENT_CD" , model.get("CLIENT_CD" + i));
                modelDt.put("AUTH_CD"   , model.get("AUTH_CD"   + i));
                modelDt.put("AUTH_NM"   , model.get("AUTH_NM"   + i));
                
                modelDt.put("SS_USER_TYPE"  , model.get(ConstantIF.SS_USER_TYPE));
                modelDt.put("SS_AUTH_LEVEL" , model.get(ConstantIF.SS_AUTH_LEVEL));
                modelDt.put("SS_CLIENT_IP"  , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));
                
                dao.deleteAuth060(modelDt);
                dao.deleteAuth070(modelDt);
                dao.deleteAuth050(modelDt);
                dao.updateDelYn(modelDt);
            }
            m.put("errCnt", errCnt);
            m.put("MSG", errMsg);
            
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
            m.put("errCnt", -1);
            m.put("MSG", MessageResolver.getMessage("delete.error"));
        }
        return m;
    }
    
    /**
     * Method ID   : deleteTopMenu
     * Method 설명    : TopMenu 삭제
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deleteTopMenu(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("delete.success");
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ST_GUBUN"     , model.get("ST_GUBUN" + i));
                modelDt.put("BA_CODE_CD"   , model.get("BA_CODE_CD" + i));
                modelDt.put("BA_CODE_NM"   , model.get("BA_CODE_NM" + i));
                modelDt.put("VIEW_SEQ"     , model.get("VIEW_SEQ" + i));
                modelDt.put("AUTH_CD"      , model.get("AUTH_CD" + i));
                modelDt.put("SS_CLIENT_IP" , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("SS_USER_NO"   , model.get(ConstantIF.SS_USER_NO));
                
                dao.deleteAuth060(modelDt);
                dao.deleteAuth070(modelDt);
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", errMsg);
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
            m.put("errCnt", -1);
            m.put("MSG", MessageResolver.getMessage("delete.error"));
        }
        return m;
    }
    
    /**
     * Method ID   : deleteMenuBtn
     * Method 설명    : 메뉴권한 삭제
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deleteMenuBtn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("delete.success");
        try{
            
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ST_GUBUN"     , model.get("ST_GUBUN" + i));
                
                modelDt.put("PROGRAM_ID"   , model.get("PROGRAM_ID" + i));
                modelDt.put("PROGRAM_NM"   , model.get("PROGRAM_NM" + i));
                modelDt.put("TOP_MENU_CD"  , model.get("TOP_MENU_CD" + i));
                modelDt.put("AUTH_CD"      , model.get("AUTH_CD" + i));
                
                modelDt.put("VIEW_MENU_CD" , model.get("VIEW_MENU_CD" + i));
                modelDt.put("INS_AUTH"     , model.get("INS_AUTH" + i));
                modelDt.put("UPD_AUTH"     , model.get("UPD_AUTH" + i));
                modelDt.put("DEL_AUTH"     , model.get("DEL_AUTH" + i));
                modelDt.put("SER_AUTH"     , model.get("SER_AUTH" + i));
                modelDt.put("PRT_AUTH"     , model.get("PRT_AUTH" + i));
                modelDt.put("EXC_AUTH"     , model.get("EXC_AUTH" + i));
                modelDt.put("VIEW_SEQ"     , model.get("VIEW_SEQ" + i));
                
                modelDt.put("SS_CLIENT_IP" , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("SS_USER_NO"   , model.get(ConstantIF.SS_USER_NO));
                modelDt.put("SS_AUTH_LEVEL", model.get(ConstantIF.SS_AUTH_LEVEL));
                
                if(modelDt.get("SS_AUTH_LEVEL").equals("1")){
                    dao.deleteAuth070(modelDt);
                }else{
                    dao.updateBtnAuth070(modelDt);
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", errMsg);
            
        } catch(Exception e){
        	if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("delete.error"));
        }
        return m;
    }
    
    
    
    
    /**
     * 
     * 대체 Method ID   : saveAuth
     * 대체 Method 설명 : 권한  등록,수정,삭제
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveAuth(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ST_GUBUN"  , model.get("ST_GUBUN"  + i));
                modelDt.put("CLIENT_CD" , model.get("CLIENT_CD" + i));
                modelDt.put("AUTH_CD"   , model.get("AUTH_CD"   + i));
                modelDt.put("AUTH_NM"   , model.get("AUTH_NM"   + i));
                
                modelDt.put("SS_USER_TYPE"  , model.get(ConstantIF.SS_USER_TYPE));
                modelDt.put("SS_AUTH_LEVEL" , model.get(ConstantIF.SS_AUTH_LEVEL));
                modelDt.put("SS_CLIENT_IP"  , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("SS_USER_NO"    , model.get(ConstantIF.SS_USER_NO));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insertAuth(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.updateAuth(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.deleteAuth060(modelDt);
                    dao.deleteAuth070(modelDt);
                    dao.deleteAuth050(modelDt);
                    dao.updateDelYn(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch (BizException be) {
            if (loger.isInfoEnabled()) {
                loger.info(be.getMessage(), be);
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveTopMenu
     * 대체 Method 설명 : 권한topmenu  등록,수정,삭제
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveTopMenu(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ST_GUBUN"     , model.get("ST_GUBUN" + i));
                modelDt.put("BA_CODE_CD"   , model.get("BA_CODE_CD" + i));
                modelDt.put("BA_CODE_NM"   , model.get("BA_CODE_NM" + i));
                modelDt.put("VIEW_SEQ"     , model.get("VIEW_SEQ" + i));
                modelDt.put("AUTH_CD"      , model.get("AUTH_CD" + i));
                modelDt.put("SS_CLIENT_IP" , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("SS_USER_NO"   , model.get(ConstantIF.SS_USER_NO));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i)) || "UPDATE".equals(model.get("ST_GUBUN"+i))){
                    int cnt = dao.selectTopCnt(modelDt);
                    if(cnt == 0){
                        dao.insertAuth060(modelDt);
                    }else if (cnt >0){
                        dao.updateAuth060(modelDt);
                    }else{
                        errCnt++;
                        m.put("errCnt", errCnt);
                        throw new BizException(MessageResolver.getMessage("save.error"));
                    }
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.deleteAuth060(modelDt);
                    dao.deleteAuth070(modelDt);
                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch (BizException be) {
            if (loger.isInfoEnabled()) {
                loger.info(be.getMessage(), be);
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveMenuBtn
     * 대체 Method 설명 : 권한프로그램  등록,수정,삭제
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveMenuBtn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
//            Map<String, Object> mapData = (Map<String, Object>)dao.selectUserAuthLevel(model);
//            model.put("USER_CLIENT", mapData.get("CLIENT_CD"));
//            model.put("AUTH_LEVEL", mapData.get("AUTH_LEVEL"));
            
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("ST_GUBUN"     , model.get("ST_GUBUN" + i));
                
                modelDt.put("PROGRAM_ID"   , model.get("PROGRAM_ID" + i));
                modelDt.put("PROGRAM_NM"   , model.get("PROGRAM_NM" + i));
                modelDt.put("TOP_MENU_CD"  , model.get("TOP_MENU_CD" + i));
                modelDt.put("AUTH_CD"      , model.get("AUTH_CD" + i));
                
                modelDt.put("VIEW_MENU_CD" , model.get("VIEW_MENU_CD" + i));
                modelDt.put("INS_AUTH"     , model.get("INS_AUTH" + i));
                modelDt.put("UPD_AUTH"     , model.get("UPD_AUTH" + i));
                modelDt.put("DEL_AUTH"     , model.get("DEL_AUTH" + i));
                modelDt.put("SER_AUTH"     , model.get("SER_AUTH" + i));
                modelDt.put("PRT_AUTH"     , model.get("PRT_AUTH" + i));
                modelDt.put("EXC_AUTH"     , model.get("EXC_AUTH" + i));
                modelDt.put("VIEW_SEQ"     , model.get("VIEW_SEQ" + i));
                
                modelDt.put("SS_CLIENT_IP" , model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("SS_USER_NO"   , model.get(ConstantIF.SS_USER_NO));
                modelDt.put("SS_AUTH_LEVEL", model.get(ConstantIF.SS_AUTH_LEVEL));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i)) || "UPDATE".equals(model.get("ST_GUBUN"+i))){
                    int cnt = dao.selectProgramCnt(modelDt);
                    if(cnt == 0){
                        dao.insertAuth070(modelDt);
                    }else if (cnt >0){
                        dao.updateAuth070(modelDt);
                    }else{
                        errCnt++;
                        m.put("errCnt", errCnt);
                        throw new BizException(MessageResolver.getMessage("save.error"));
                    }
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    if(modelDt.get("SS_AUTH_LEVEL").equals("1")){
                        dao.deleteAuth070(modelDt);
                    }else{
                        dao.updateBtnAuth070(modelDt);
                    }
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
                
        }catch (BizException be) {
            if (loger.isInfoEnabled()) {
                loger.info(be.getMessage(), be);
            }
            m.put("MSG", be.getMessage());

        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
