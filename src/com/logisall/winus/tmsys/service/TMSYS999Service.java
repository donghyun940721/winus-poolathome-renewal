package com.logisall.winus.tmsys.service;

import java.util.Map;

public interface TMSYS999Service {
    public Map<String, Object> getPdf(Map<String, Object> model) throws Exception;    
    public Map<String, Object> getPreViewConf(Map<String, Object> model) throws Exception;//PDF 생성

}
