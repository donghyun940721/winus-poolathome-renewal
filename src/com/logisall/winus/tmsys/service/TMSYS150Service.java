package com.logisall.winus.tmsys.service;

import java.util.Map;

public interface TMSYS150Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveT1(Map<String, Object> model) throws Exception;
	public Map<String, Object> save(Map<String, Object> model) throws Exception;
	public Map<String, Object> delete(Map<String, Object> model) throws Exception;
	public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
