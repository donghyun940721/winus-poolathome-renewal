package com.logisall.winus.tmsys.service;

import java.util.Map;


public interface TMSYS010Service {
	/**interfacebody*/
    public Map<String, Object> listCode(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetailCode(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> saveCode(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveDetailCode(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
