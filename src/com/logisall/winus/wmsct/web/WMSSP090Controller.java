package com.logisall.winus.wmsct.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsct.service.WMSSP090Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
@Controller
public class WMSSP090Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP090Service")
    private WMSSP090Service service;
    
	/*-
	 * Method ID    : WMSSP090
	 * Method 설명      : 고객만족도조회
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSSP090.action")
	public ModelAndView WMSSP090(Map<String, Object> model) throws Exception {
	    return new ModelAndView("winus/wmsct/WMSSP090", service.selectBox(model));
	}

	/*-
	 * Method ID    : listNew
	 * Method 설명      : 고객만족도(고객별)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP090/listNew.action")
	public ModelAndView listNew(Map<String, Object> model) throws Exception {
	    ModelAndView mav = null;
	    try {
	        mav = new ModelAndView("jqGridJsonView", service.listNew(model));
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to list :", e);
	        }
	    }
	    return mav;
	}
	
	/*-
	 * Method ID    : listNew2
	 * Method 설명      : 고객만족도(기사별)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP090/listNew2.action")
	public ModelAndView listNew2(Map<String, Object> model) throws Exception {
	    ModelAndView mav = null;
	    try {
	        mav = new ModelAndView("jqGridJsonView", service.listNew2(model));
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to list :", e);
	        }
	    }
	    return mav;
	}

	/*-
	 * Method ID    : listNew3
	 * Method 설명      : 고객만족도(센터별)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP090/listNew3.action")
	public ModelAndView listNew3(Map<String, Object> model) throws Exception {
	    ModelAndView mav = null;
	    try {
	        mav = new ModelAndView("jqGridJsonView", service.listNew3(model));
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to list :", e);
	        }
	    }
	    return mav;
	}

	/*-
	 * Method ID   : smsInfoSaveCsat
	 * Method 설명      : 고객만족도 서비스 SMS 발송 여부
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP090/smsInfoSaveCsat.action")
	public ModelAndView smsInfoSaveCsat(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.smsInfoSaveCsat(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

}
