package com.logisall.winus.wmsct.web;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSCT101Service;
import com.logisall.ws.interfaces.common.OrderWebService;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSCT101Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT101Service")
    private WMSCT101Service service;
    
    /**
     * Method ID	: wmsct101
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSCT101.action")
    public ModelAndView wmsct101(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsct/WMSCT101");
    }
	
    /**
     * Method ID	: list
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT101/list.action")
    public ModelAndView list(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.list(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT101/listDetail.action")
    public ModelAndView listDetail(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.listDetail(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT101/csList.action")
    public ModelAndView csList(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.csList(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: listDetail
     * Method 설명	: 
     * 작성자			: khkim
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT101/csSave.action")
    public ModelAndView csSave(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.csSave(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /*-
	 * Method ID    : /WMSCT101pop.action
	 * Method 설명      : 
	 * 작성자                 : khkim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT101pop.action")
	public ModelAndView wmsct101pop(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsom/WMSCT101pop");
	}
	
	/*-
	 * Method ID    : /WMSCT101pop2.action
	 * Method 설명      : 
	 * 작성자                 : khKim
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT101pop2.action")
	public ModelAndView wmsct101pop2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSCT101pop2");
	}
	
	/*-
	 * Method ID    : saveE3
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT101/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID	: listCsInfo
     * Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT101/listCsInfo.action")
    public ModelAndView listCsInfo(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        System.out.println(model.get("vrSrchOrdNo").toString());
        try {
            mav = new ModelAndView("jqGridJsonView", service.listCsInfo(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
	/**
     * Method ID	: listCsInfo
     * Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT101/realpacking.action")
    public ModelAndView WMSCT101pop2(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        try {
        	Map<String, Object> realPackingParam = new HashMap<>();
        	Map<String, Object> result;
        	//Map<String, Object> map = service.getFaIssueLabel(model);
        	if(!StringUtils.isEmpty((String)model.get("faIssueLabel"))){
        		//String ordwaybill = (String)((List)map.get("FA_ISSUE_LABEL")).get(0);
            	//System.out.println( ConstantIF.REALPACKING_AUTHKEY + " " + ConstantIF.REALPACKING_URL);
            	
        		realPackingParam.put("auth_key", ConstantIF.REALPACKING_AUTHKEY);
            	realPackingParam.put("time", System.currentTimeMillis() / 1000L);
            	realPackingParam.put("ordwaybill", (String)model.get("faIssueLabel"));
            	realPackingParam.put("sizetype", "percent");
            	realPackingParam.put("frmwidth", "60");
            	realPackingParam.put("frmheight", "80");
            	result = RestApiUtil.getRealPacking(ConstantIF.REALPACKING_URL, realPackingParam);
        	}else{
        		result = new HashMap<>();
        		result.put("error_code", "400");
        		result.put("error_msg", "해당 제품은 영상이 존재하지 않습니다.");
        	}
        	
    		mav = new ModelAndView("jsonView", result);
        	//System.out.println(result);
        	
    		
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: itemList
     * Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT101/itemList.action")
    public ModelAndView itemList(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.itemList(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
    
    /**
     * Method ID	: itemList
     * Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT101/recentOrderList.action")
    public ModelAndView recentOrderList(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
        
        try {
            mav = new ModelAndView("jqGridJsonView", service.recentOrderList(model));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mav;
    }
}
