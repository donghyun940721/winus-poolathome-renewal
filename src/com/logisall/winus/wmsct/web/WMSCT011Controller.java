package com.logisall.winus.wmsct.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsct.service.WMSCT011Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
@Controller
public class WMSCT011Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT011Service")
    private WMSCT011Service service;
    
	/*-
	 * Method ID    : WMSCT011
	 * Method 설명      : 고객관리(신규)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSCT011.action")
	public ModelAndView WMSCT011(Map<String, Object> model) throws Exception {
	    return new ModelAndView("winus/wmsct/WMSCT011", service.selectBox(model));
	}

	/*-
	 * Method ID    : listNew
	 * Method 설명      : 고객관리(신규)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT011/listNew.action")
	public ModelAndView listNew(Map<String, Object> model) throws Exception {
	    ModelAndView mav = null;
	    try {
	        mav = new ModelAndView("jqGridJsonView", service.listNew(model));
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to list :", e);
	        }
	    }
	    return mav;
	}
	
	/*-
	 * Method ID    : listNew2
	 * Method 설명      : 고객관리(CS)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT011/listNew2.action")
	public ModelAndView listNew2(Map<String, Object> model) throws Exception {
	    ModelAndView mav = null;
	    try {
	        mav = new ModelAndView("jqGridJsonView", service.listNew2(model));
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to list :", e);
	        }
	    }
	    return mav;
	}

	/*-
	 * Method ID    : listNew3
	 * Method 설명      : 고객관리(AS)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT011/listNew3.action")
	public ModelAndView listNew3(Map<String, Object> model) throws Exception {
	    ModelAndView mav = null;
	    try {
	        mav = new ModelAndView("jqGridJsonView", service.listNew3(model));
	    } catch (Exception e) {
	        if (log.isErrorEnabled()) {
	            log.error("Fail to list :", e);
	        }
	    }
	    return mav;
	}

	/*-
	 * Method ID    : saveNew
	 * Method 설    : 고객정보관리
	 * 작성자       : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT011/saveNew.action")
	public ModelAndView saveNew(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveNew(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	/*-
	 * Method ID    : saveNewAs
	 * Method 설명      : 고객관리 > AS저장
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT011/saveNewAs.action")
	public ModelAndView saveNewAs(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveNewAs(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	

	/*-
	 * Method ID    : deleteNew
	 * Method 설    : 고객관리(N) 팝업 발주 건 삭제
	 * 작성자       : kijun11
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT011/deleteNew.action")
	public ModelAndView deleteNew(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteNew(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	
	/*-
	 * Method ID    : deleteNew_CS
	 * Method 설    : 고객관리(N) 팝업 발주 건_ CS관리 ROW 삭제
	 * 작성자       : kijun11
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT011/deleteNew_CS.action")
	public ModelAndView deleteNew_CS(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.deleteNew_CS(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

}
