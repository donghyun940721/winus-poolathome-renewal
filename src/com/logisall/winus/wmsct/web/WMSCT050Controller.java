package com.logisall.winus.wmsct.web;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ExcelWriter;
import com.m2m.jdfw5x.egov.database.GenericResultSet;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsct.service.WMSCT050Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSCT050Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSCT050Service")
	private WMSCT050Service service;
	
	/*-
	 * Method ID    : WMSCT050
	 * Method 설명      : 공지사항 화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSCT050.action")
	public ModelAndView WMSCT050(Map<String, Object> model) {
		return new ModelAndView("winus/WMSCT/WMSCT050");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 공지사항 목록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSCT050/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCT050/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            						{MessageResolver.getText("물류센터")    	, "0", "0", "0", "0", "100"},
				            		{MessageResolver.getText("접수번호")    	, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("수취인명")   		, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("모델명")    		, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("접수일자")    	, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("접수내용")    	, "5", "5", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("처리요청")    	, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("접수자")    		, "7", "7", "0", "0", "100"},
				            		{MessageResolver.getText("등록구분")    	, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("처리내용")    	, "9", "9", "0", "0", "100"},
				            		{MessageResolver.getText("유/무상여부")    	, "10", "10", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("지불형태")    	, "11", "11", "0", "0", "100"},
				            		{MessageResolver.getText("비용(접수시)")	, "12", "12", "0", "0", "100"},
				            		{MessageResolver.getText("비용(완료시)")	, "13", "13", "0", "0", "100"},
				            		{MessageResolver.getText("SL_No")   	, "14", "14", "0", "0", "100"},
				            		{MessageResolver.getText("처리일")   		, "15", "15", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("비고(Issue)") 	, "16", "16", "0", "0", "100"},
				            		{MessageResolver.getText("A/S기사")   	, "17", "17", "0", "0", "100"},
				            		{MessageResolver.getText("기사연락처")   	, "18", "18", "0", "0", "100"},
				            		{MessageResolver.getText("방문예정일")    	, "19", "19", "0", "0", "100"},
				            		{MessageResolver.getText("고객주문번호")    	, "20", "20", "0", "0", "100"},
				            		
				            		{MessageResolver.getText("수취인전화번호")   , "21", "21", "0", "0", "100"},
				            		{MessageResolver.getText("수취인핸드폰")    	, "22", "22", "0", "0", "100"},
				            		{MessageResolver.getText("주소")    		, "23", "23", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"REQ_DLV_JOIN_NM"  ,"S"},
				            		{"ASCS_NO" 			,"S"},
				            		{"SALES_CUST_NM" 	,"S"},
				            		{"RITEM_CD" 		,"S"},
				            		{"REG_DT" 			,"S"},
				            		{"REG_CONTENTS" 	,"S"},
				            		
				            		{"MEMO" 			,"S"},
				            		{"REG_NAME" 		,"S"},
				            		{"COM_TYPE_NM" 		,"S"},
				            		{"RESULT" 			,"S"},
				            		{"PAY_REQ_YN_NM" 	,"S"},
				            		
				            		{"COST_TYPE_NM" 	,"S"},
				            		{"REQ_COST" 		,"S"},
				            		{"COM_COST" 		,"S"},
				            		{"SERIAL_NO" 		,"S"},
				            		{"COM_DT" 			,"S"},
				            		
				            		{"ETC1" 			,"S"},
				            		{"REQ_DRIVER_NM" 	,"S"},
				            		{"REQ_DRIVER_TL" 	,"S"},
				            		{"DLV_REQ_DT" 		,"S"},
				            		{"ORG_ORD_ID" 		,"S"},
				            		
				            		{"PHONE_1" 			,"S"},
				            		{"PHONE_2" 			,"S"},
				            		{"PARAM_ADDR" 		,"S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("AS내역");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 해피콜주문관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP050/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
