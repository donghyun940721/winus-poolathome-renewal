package com.logisall.winus.wmsct.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSCT020Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSCT020Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT020Service")
    private WMSCT020Service service;
    
	/*-
	 * Method ID    : /WMSCT020T1.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T1.action")
	public ModelAndView wmsct020t1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSCT020T1", service.selectBox(model));
	}
	
	/*-
	 * Method ID    : /WMSCT020T2.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T2.action")
	public ModelAndView wmsct020t2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSCT020T2", service.selectBox(model));
	}
	
	/*-
	 * Method ID    : saveE3
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T1/saveE3.action")
	public ModelAndView saveE3(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE3(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : saveE4
	 * Method 설명  : 
	 * 작성자       : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T1/saveE4.action")
	public ModelAndView saveE4(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveE4(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*
	 * Method ID    : list
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020/list.action")
	public ModelAndView list1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}

	/*
	 * Method ID    : list
	 * Method 설명   : 
	 * 작성자        : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT021/list.action")
	public ModelAndView listCT021(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listCT021(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : custInfoSave
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T1/custInfoSave.action")
	public ModelAndView custInfoSave(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.custInfoSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : custInfoSave021
	 * Method 설명      : 
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T4/custInfoSave021.action")
	public ModelAndView custInfoSave021(Map<String, Object> model) throws Exception {	
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.custInfoSave021(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/**
     * Method ID    : WMSCT020T1
     * Method 설명        : 사용자정보 상세보기
     * 작성자                     : chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WMSCT020Q1/custAttchDown.action")
    public ModelAndView custAttchDown(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsct/WMSCT020Q1");
    }
    
    /*-
	 * Method ID   : customerInfo
	 * Method 설명 : 고객정보 상세조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSCT020Q1/customerfile.action")
	public ModelAndView customerfile(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.customerfile(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	@RequestMapping("/WMSCT020/selectAsErrList.action")
	public ModelAndView selectAsErrList(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.selectAsErrList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : /WMSCT020T3.action
	 * Method 설명      : 
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T3.action")
	public ModelAndView wmsct020t3(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSCT020T3");
	}
	
	/*-
	 * Method ID    : /WMSCT020T4.action
	 * Method 설명      : 
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T4.action")
	public ModelAndView WMSCT020T4(Map<String, Object> model) throws Exception {
	    return new ModelAndView("winus/wmsct/WMSCT020T4", service.selectBox(model));
	}
	
	/*-
	 * Method ID    : nominalTransfer
	 * Method 설명      : 명의이전 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020T3/nominalTransfer.action")
	public ModelAndView nominalTransfer(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.nominalTransfer(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : wmsct020Q2
	 * Method 설명      : 물류용기 출고주문
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT020Q2.action")
	public ModelAndView wmsct020Q2(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSCT020Q2");
	}
	
	/*-
	 * Method ID    : saveOutOrderQ2
	 * Method 설명      : 출고관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSCT020Q2/saveOutOrderQ2.action")
	public ModelAndView saveOutOrderQ2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveOutOrderQ2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
