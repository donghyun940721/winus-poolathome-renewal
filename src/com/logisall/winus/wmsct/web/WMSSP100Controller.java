package com.logisall.winus.wmsct.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsct.service.WMSSP100Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSSP100Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP100Service")
    private WMSSP100Service service;
    

	/*-
	 * Method ID    : WMSSP100
	 * Method 설명      : 반품관리
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSSP100.action")
	public ModelAndView WMSSP100(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsct/WMSSP100", service.selectBox(model));
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
	}


	/*-
	 * Method ID    : list
	 * Method 설명      : 반품관리
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP100/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	

	/*-
	 * Method ID    : detailList
	 * Method 설명      : 반품관리 디테일
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP100/detailList.action")
	public ModelAndView detailList(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.detailList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
}
