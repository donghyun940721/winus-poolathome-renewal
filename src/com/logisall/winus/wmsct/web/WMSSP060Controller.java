package com.logisall.winus.wmsct.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSSP060Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSSP060Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP060Service")
    private WMSSP060Service service;
    
    /*-
	 * Method ID    : WMSSP060
	 * Method 설명      : 기사피킹리스트
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSSP060.action")
	public ModelAndView WMSSP060(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSSP060", service.selectBox(model));
	}
	
	/*-
	 * Method ID	: pickingList1
	 * Method 설명	: 기사피킹리스트
	 * 작성자			: 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP060/pickingList1.action")
	public ModelAndView pickingList1(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.pickingList1(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID	: pickingList2
	 * Method 설명	: 기사피킹리스트
	 * 작성자			: 
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP060/pickingList2.action")
	public ModelAndView pickingList2(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.pickingList2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : excelPickingPop1
	 * Method 설명      : 기사피킹리스트 엑셀다운로드1
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSSP060/excelPickingPop1.action")
	public void excelPickingPop1(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelPickingPop1(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownPickingPop1(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDownPickingPop1
	 * Method 설명 : 기사피킹리스트 엑셀다운로드1
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDownPickingPop1(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
        	String[][] headerEx = {
				        			 {MessageResolver.getText("기사명")		,"0","0", "0", "0", "100"}
				        			,{MessageResolver.getText("배송예정일")		,"1","1", "0", "0", "100"}
				        			,{MessageResolver.getText("화주")			,"2","2", "0", "0", "100"}
				        			,{MessageResolver.getText("상품명")		,"3","3", "0", "0", "100"}
				        			,{MessageResolver.getText("상품코드")		,"4","4", "0", "0", "100"}
				        			,{MessageResolver.getText("합계수량")		,"5","5", "0", "0", "100"}
			                  	};
			//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {
									 {"DRIVER"				, "S"}
									,{"DLV_REQ_DT"			, "S"}
									,{"CUST_NM"				, "S"}
									,{"DLV_PRODUCT_NM"		, "S"}
									,{"DLV_PRODUCT_CD"		, "S"}
									,{"DLV_QTY"				, "N"}
			                   }; 

			// 파일명
			String fileName = MessageResolver.getText("기사피킹리스트(화주)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : excelPickingPop2
	 * Method 설명      : 기사피킹리스트 엑셀다운로드2
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSSP060/excelPickingPop2.action")
	public void excelPickingPop2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelPickingPop2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDownPickingPop2(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDownPickingPop2
	 * Method 설명 : 기사피킹리스트 엑셀다운로드2
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDownPickingPop2(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
        	String[][] headerEx = {
				        			 {MessageResolver.getText("기사명")		,"0","0", "0", "0", "100"}
				        			,{MessageResolver.getText("배송예정일")		,"1","1", "0", "0", "100"}
				        			,{MessageResolver.getText("고객명")		,"2","2", "0", "0", "100"}
				        			,{MessageResolver.getText("상품명")		,"3","3", "0", "0", "100"}
				        			,{MessageResolver.getText("상품코드")		,"4","4", "0", "0", "100"}
				        			,{MessageResolver.getText("수량")			,"5","5", "0", "0", "100"}
				        			
				        			,{MessageResolver.getText("배송완료")		,"6","6", "0", "0", "100"}
				        			,{MessageResolver.getText("초도불량")		,"7","7", "0", "0", "100"}
				        			,{MessageResolver.getText("반품입고")		,"8","8", "0", "0", "100"}
				        			,{MessageResolver.getText("교환입고")		,"9","9", "0", "0", "100"}
				        			,{MessageResolver.getText("미배송입고")		,"10","10", "0", "0", "100"}
			                  	};
			//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {
									 {"DRIVER"				, "S"}
									,{"DLV_REQ_DT"			, "S"}
									,{"DLV_CUSTOMER_NM"		, "S"}
									,{"DLV_PRODUCT_NM"		, "S"}
									,{"DLV_PRODUCT_CD"		, "S"}
									,{"DLV_QTY"				, "N"}
									
									,{""					, "S"}
									,{""					, "S"}
									,{""					, "S"}
									,{""					, "S"}
									,{""					, "S"}
			                   }; 

			// 파일명
			String fileName = MessageResolver.getText("기사피킹리스트(고객)");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
