package com.logisall.winus.wmsct.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSSP040Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSSP040Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP040Service")
    private WMSSP040Service service;
    
    /*-
	 * Method ID    : WMSSP040
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSSP040.action")
	public ModelAndView WMSSP040(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSSP040", service.selectBox(model));
	}
	
	/*-
	 * Method ID    : WMSSP041
	 * Method 설명      : AS관리 (N)
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSSP041.action")
	public ModelAndView WMSSP041(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSSP041", service.selectBox(model));
	}
	
	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSSP040/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
        	String[][] headerEx = {
			            		{MessageResolver.getText("등록구분")     , "0", "0", "0", "0", "100"},
			            		{MessageResolver.getText("발주일")      , "1", "1", "0", "0", "100"},
			            		{MessageResolver.getText("지정일")      , "2", "2", "0", "0", "100"},
			            		{MessageResolver.getText("배송완료일")    , "3", "3", "0", "0", "100"},
			            		{MessageResolver.getText("판매처")       , "4", "4", "0", "0", "100"},
			            		{MessageResolver.getText("고객명")       , "5", "5", "0", "0", "100"},
			            		{MessageResolver.getText("전화번호1")     , "6", "6", "0", "0", "100"},
			            		{MessageResolver.getText("전화번호2")     , "7", "7", "0", "0", "100"},
			            		{MessageResolver.getText("주소")         , "8", "8", "0", "0", "100"},
			            		{MessageResolver.getText("제품명")        , "9", "9", "0", "0", "100"},
			            		{MessageResolver.getText("제품코드")       , "10", "10", "0", "0", "100"},
			            		{MessageResolver.getText("시리얼번호")     , "11", "11", "0", "0", "100"},
			            		{MessageResolver.getText("수량")         , "12", "12", "0", "0", "100"},
			            		{MessageResolver.getText("고객주문번호")    , "13", "13", "0", "0", "100"},
			            		{MessageResolver.getText("기타1")        , "14", "14", "0", "0", "100"},
			            		{MessageResolver.getText("배송메모")        , "15", "15", "0", "0", "100"},
			            		{MessageResolver.getText("메모")         , "16", "16", "0", "0", "100"},
			            		{MessageResolver.getText("배송상태")      , "17", "17", "0", "0", "100"},
			            		{MessageResolver.getText("배송사")       , "18", "18", "0", "0", "100"},
			            		{MessageResolver.getText("지정배송기사")   , "19", "19", "0", "0", "100"},
			            		{MessageResolver.getText("설정배송기사")   , "20", "20", "0", "0", "100"},
			            		{MessageResolver.getText("주문옵션")      , "21", "21", "0", "0", "100"},
			            		{MessageResolver.getText("기타2")        , "22", "22", "0", "0", "100"},
			            		{MessageResolver.getText("이미지등록")     , "23", "23", "0", "0", "100"},
			            		{MessageResolver.getText("배송센터")      , "24", "24", "0", "0", "100"},
			            		{MessageResolver.getText("주문옵션")      , "25", "25", "0", "0", "100"}
			                  };
			//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
			String[][] valueName = {
			            		{"TRN_DLV_ORD_TYPE"	, "S"},
			            		{"BUYED_DT"		    , "S"},
			            		{"DLV_REQ_DT"		, "S"},
			            		{"DLV_DT"			, "S"},
			            		{"SALES_COMPANY_NM" , "S"},
			            		{"DLV_CUSTOMER_NM"  , "S"},
			            		{"DLV_PHONE_1"		, "S"},
			            		{"DLV_PHONE_2"		, "S"},
			            		{"DLV_ADDR"			, "S"},
			            		{"DLV_PRODUCT_NM"	, "S"},
			            		{"DLV_PRODUCT_CD"	, "S"},
			            		{"SERIAL_NO"		, "S"},
			            		{"DLV_QTY"			, "S"},
			            		{"ORG_ORD_ID"		, "S"},
			            		{"ETC1"				, "S"},
			            		{"DLV_COMMENT"		, "S"},
			            		{"MEMO"				, "S"},
			            		{"TRN_DLV_ORD_STAT"	, "S"},
			            		{"DLV_COMP_NM"		, "S"},
			            		{"REQ_DRIVER_NM"	, "S"},
			            		{"SET_DRIVER_NM"	, "S"},
			            		{"ORD_OPTION"		, "S"},
			            		{"ETC2"				, "S"},
			            		{"IMG_SND_YN"		, "S"},
			            		{"REQ_DLV_JOIN_NM"	, "S"},
			            		{"ORD_OPTION"		, "S"}
			                   }; 

			// 파일명
			String fileName = MessageResolver.getText("배송관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
