package com.logisall.winus.wmsct.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSSP020Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSSP020Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP020Service")
    private WMSSP020Service service;
    
    /*-
	 * Method ID    : WMSSP020
	 * Method 설명      : 배송관리(V2.0)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSSP020.action")
	public ModelAndView WMSSP0100(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSSP020", service.selectBox(model));
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 배송관리(V2.0)
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP020/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
//
//	/*-
//	 * Method ID    : save
//	 * Method 설명      : 고객관리 저장
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/save.action")
//	public ModelAndView save(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.save(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//
//	/*-
//	 * Method ID    : rowConfirm
//	 * Method 설명      : 고객관리 저장
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/rowConfirm.action")
//	public ModelAndView rowConfirm(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.rowConfirm(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : listExcel
//	 * Method 설명      : 엑셀다운로드
//	 * 작성자                 : chsong
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010/excel.action")
//	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			map = service.listExcel(model);
//			GenericResultSet grs = (GenericResultSet) map.get("LIST");
//			if (grs.getTotCnt() > 0) {
//				this.doExcelDown(response, grs);
//			}
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to download excel :", e);
//			}
//		}
//	}
//	
//	/*-
//	 * Method ID : doExcelDown
//	 * Method 설명 : 엑셀다운로드
//	 * 작성자 : kwt
//	 *
//	 * @param response
//	 * @param grs
//	 */
//	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
//        try{
//            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
//            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
//        	String[][] headerEx = {
//				        			 {MessageResolver.getText("화주")			,"0","0", "0", "0", "100"}
//				        			,{MessageResolver.getText("수취인")		,"1","1", "0", "0", "100"}
//				        			,{MessageResolver.getText("이미지여부")		,"2","2", "0", "0", "100"}
//				        			,{MessageResolver.getText("등록구분")		,"3","3", "0", "0", "100"}
//				        			,{MessageResolver.getText("발주일(주문)")	,"4","4", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송예정일")		,"5","5", "0", "0", "100"}
//				        			,{MessageResolver.getText("지정일")		,"6","6", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송완료일")		,"7","7", "0", "0", "100"}
//				        			,{MessageResolver.getText("지정배송기사")	,"8","8", "0", "0", "100"}
//				        			,{MessageResolver.getText("기사전화번호")	,"9","9", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송상태")		,"10","10", "0", "0", "100"}
//				        			,{MessageResolver.getText("취소사유")		,"11","11", "0", "0", "100"}
//				        			,{MessageResolver.getText("판매처")		,"12","12", "0", "0", "100"}
//				        			,{MessageResolver.getText("수취인")		,"13","13", "0", "0", "100"}
//				        			,{MessageResolver.getText("전화번호1")		,"14","14", "0", "0", "100"}
//				        			,{MessageResolver.getText("전화번호2")		,"15","15", "0", "0", "100"}
//				        			,{MessageResolver.getText("주소")			,"16","16", "0", "0", "100"}
//				        			,{MessageResolver.getText("지역")			,"17","17", "0", "0", "100"}
//				        			,{MessageResolver.getText("제품명")		,"18","18", "0", "0", "100"}
//				        			,{MessageResolver.getText("제품코드")		,"19","19", "0", "0", "100"}
//				        			,{MessageResolver.getText("제품설명")		,"20","20", "0", "0", "100"}
//				        			,{MessageResolver.getText("수량")			,"21","21", "0", "0", "100"}
//				        			,{MessageResolver.getText("시리얼번호")		,"22","22", "0", "0", "100"}
//				        			,{MessageResolver.getText("주문옵션")		,"23","23", "0", "0", "100"}
//				        			,{MessageResolver.getText("고객주문번호")	,"24","24", "0", "0", "100"}
//				        			,{MessageResolver.getText("기타1")		,"25","25", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송메모모바일")	,"26","26", "0", "0", "100"}
//				        			,{MessageResolver.getText("인수증메모")		,"27","27", "0", "0", "100"}
//				        			,{MessageResolver.getText("거래배송사")		,"28","28", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송센터")		,"29","29", "0", "0", "100"}
//				        			,{MessageResolver.getText("설치유형(완료)")	,"30","30", "0", "0", "100"}
//				        			,{MessageResolver.getText("설치유형(등록)")	,"31","31", "0", "0", "100"}
//				        			,{MessageResolver.getText("양중여부")		,"32","32", "0", "0", "100"}
//				        			,{MessageResolver.getText("기타작업")		,"33","33", "0", "0", "100"}
//				        			,{MessageResolver.getText("해피콜여부")		,"34","34", "0", "0", "100"}
//				        			,{MessageResolver.getText("해피콜메모")		,"35","35", "0", "0", "100"}
//				        			,{MessageResolver.getText("해피콜일자")		,"36","36", "0", "0", "100"}
//				        			,{MessageResolver.getText("모바일완료일")	,"37","37", "0", "0", "100"}
//				        			,{MessageResolver.getText("인수증출력확인")	,"38","38", "0", "0", "100"}
//				        			,{MessageResolver.getText("모바일완료자")	,"39","39", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송주문ID")		,"40","40", "0", "0", "100"}
//			                  	};
//			//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
//			String[][] valueName = {
//									 {"TRUST_CUST_NM"			, "S"}
//									,{"VIEW_DLV_CUSTOMER_NM"	, "S"}
//									,{"IMG_SND_YN"				, "S"}
//									,{"TRN_DLV_ORD_TYPE"		, "S"}
//									,{"BUYED_DT"				, "S"}
//									,{"DLV_REQ_DT"				, "S"}
//									,{"DLV_SET_DT"				, "S"}
//									,{"DLV_DT"					, "S"}
//									,{"REQ_DRIVER_NM"			, "S"}
//									,{"REQ_DRIVER_TL"			, "S"}
//									,{"TRN_DLV_ORD_STAT"		, "S"}
//									,{"ETC2"					, "S"}
//									,{"SALES_COMPANY_NM"		, "S"}
//									,{"DLV_CUSTOMER_NM"			, "S"}
//									,{"DLV_PHONE_1"				, "S"}
//									,{"DLV_PHONE_2"				, "S"}
//									,{"DLV_ADDR"				, "S"}
//									,{"DLV_CITY_NM"				, "S"}
//									,{"DLV_PRODUCT_NM"			, "S"}
//									,{"DLV_PRODUCT_CD"			, "S"}
//									,{"TEMP_DLV_PRODUCT_NM"		, "S"}
//									,{"DLV_QTY"					, "S"}
//									,{"SERIAL_NO"				, "S"}
//									,{"ORD_OPTION"				, "S"}
//									,{"ORG_ORD_ID"				, "S"}
//									,{"ETC1"					, "S"}
//									,{"DLV_COMMENT"				, "S"}
//									,{"MEMO"					, "S"}
//									,{"DLV_COMP_NM"				, "S"}
//									,{"REQ_DLV_JOIN_NM"			, "S"}
//									,{"DLV_SET_TYPE_NM"			, "S"}
//									,{"ORG_DLV_SET_TYPE_NM"		, "S"}
//									,{"LIFTING_WORK_YN"			, "S"}
//									,{"ETC_WORK"				, "S"}
//									,{"HAPPY_CALL_YN"			, "S"}
//									,{"HAPPY_CALL_MEMO"			, "S"}
//									,{"HAPPY_CALL_DT"			, "S"}
//									,{"PRINT_RECEIPT_YN"		, "S"}
//									,{"MOBILE_COM_DT"			, "S"}
//									,{"MOBILE_COM_ID"			, "S"}
//									,{"DLV_ORD_ID"				, "S"}
//			                   }; 
//
//			// 파일명
//			String fileName = MessageResolver.getText("배송관리");
//			// 시트명
//			String sheetName = "Sheet1";
//			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
//			String marCk = "N";
//			// ComUtil코드
//			String etc = "";
//
//			ExcelWriter wr = new ExcelWriter();
//			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("fail download Excel file...", e);
//			}
//		}
//	}
//	
//	/*-
//	 * Method ID : mn
//	 * Method 설명 : 배송사
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q1.action")
//	public ModelAndView mn(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010Q1");
//	}
//	
//	/*-
//	 * Method ID : mn
//	 * Method 설명 : 배송기사
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q2.action")
//	public ModelAndView mn2(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010Q2");
//	}
//	
//	/*-
//	 * Method ID : mn
//	 * Method 설명 : 배송기사 및 배송예정일
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q3.action")
//	public ModelAndView m3n(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010Q3");
//	}
//	
//	/*-
//	 * Method ID : listQ1
//	 * Method 설명 : 배송사
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q1/list.action")
//	public ModelAndView listQ1(Map<String, Object> model) {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.listQ1(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get List info :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID : listQ2
//	 * Method 설명 : 배송기사
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q2/list.action")
//	public ModelAndView listQ2(Map<String, Object> model) {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.listQ2(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get List info :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : WMSSP010T1
//	 * Method 설명      : 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010T1.action")
//	public ModelAndView wmssp010t1(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010T1");
//	}
//
//	/*-
//	 * Method ID    : WMSSP010T1_9
//	 * Method 설명      : 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010T1_9.action")
//	public ModelAndView wmssp010t1_9(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010T1_9");
//	}
//	
//	/*-
//	 * Method ID    : WMSSP010T1_10
//	 * Method 설명      : 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010T1_10.action")
//	public ModelAndView wmssp010t1_10(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010T1_10");
//	}
//	
//	/*-
//	 * Method ID : list
//	 * Method 설명 : 
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010T1/list.action")
//	public ModelAndView listT1(Map<String, Object> model) {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jsonView", service.listT1(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get Manage Code :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID : list
//	 * Method 설명 : 
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010T1_10/list.action")
//	public ModelAndView listT1_10(Map<String, Object> model) {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jsonView", service.listT1_10(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get Manage Code :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : WMSSP010T2
//	 * Method 설명      : 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010T2.action")
//	public ModelAndView wmssp010t2(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010T2");
//	}
//	
//	/*-
//	 * Method ID : imgSave
//	 * Method 설명 : 
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010T2/saveImg.action")
//	public ModelAndView saveImg(Map<String, Object> model) {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//
//		try {
//			m = service.saveImg(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("ERROR", "1");
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	/*-
//	 * Method ID : mn
//	 * Method 설명 : 배송기사 및 배송예정일
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q4.action")
//	public ModelAndView mn4(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010Q4", service.selectBox2(model));
//	}
//	
//	/*-
//	 * Method ID    : smsInfoSave
//	 * Method 설명      : 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/smsInfoSave.action")
//	public ModelAndView smsInfoSave(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.smsInfoSave(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : smsInfoSave
//	 * Method 설명      : 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/smsSaveConfUpdate.action")
//	public ModelAndView smsSaveConfUpdate(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.smsSaveConfUpdate(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : save
//	 * Method 설명      : 고객관리 저장
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/saveLcOrdMove.action")
//	public ModelAndView saveLcOrdMove(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.saveLcOrdMove(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : savedlvLcTran
//	 * Method 설명      : 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/saveDlvLcTran.action")
//	public ModelAndView savedlvLcTran(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.saveDlvLcTran(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	@RequestMapping("/WMSSP010/asCntView.action")
//	public ModelAndView errCntView(Map<String, Object> model) {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jsonView", service.asCntView(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get Manage Code :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID : mn
//	 * Method 설명 : 배송기사
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q6.action")
//	public ModelAndView mn3(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010Q6");
//	}
//	
//	/*-
//	 * Method ID : listQ6
//	 * Method 설명 : 배송기사
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q6/list.action")
//	public ModelAndView listQ6(Map<String, Object> model) {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.listQ6(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get List info :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : WMSSP010
//	 * Method 설명      : 고객관리
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP030T1.action")
//	public ModelAndView WMSSP030T1(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP030T1", service.selectBox(model));
//	}
//	
//	/*-
//	 * Method ID    : updateSubUserInfo
//	 * Method 설명      : 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/updateSubUserInfo.action")
//	public ModelAndView updateSubUserInfo(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.updateSubUserInfo(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	/*-
//	 * Method ID : mn
//	 * Method 설명 : 배송기사
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q7.action")
//	public ModelAndView mn7(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010Q7");
//	}
//	
//	/*-
//	 * Method ID : listQ7
//	 * Method 설명 : 배송기사
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010Q7/list.action")
//	public ModelAndView listQ7(Map<String, Object> model) {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.listQ7(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get List info :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : wmsop999T1
//	 * Method 설명      : 입고주문 신규 팝업 필요 데이타셋 
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP999T1.action")
//	public ModelAndView wmssp999T1(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP999T1");
//	}
//	
//	/*-
//	 * Method ID   : getTypicalCust
//	 * Method 설명 : 물류센터 별 대표화주값 조회
//	 * 작성자      : 기드온
//	 * @param 
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010/getInOrdByDlv.action")
//	public ModelAndView getInOrdByDlv(Map<String, Object> model) {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jsonView", service.getInOrdByDlv(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to get Manage Code :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : saveInOrder
//	 * Method 설명      : 입고관리 저장
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 */
//	@RequestMapping("/WMSSP999/saveInOrder.action")
//	public ModelAndView saveInOrder(Map<String, Object> model) {
//
//		if (log.isInfoEnabled()) {
//			log.info(model);
//		}
//
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.saveInOrder(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	/*-
//	 * Method ID : unDlvSearchPop
//	 * Method 설명 : -7 미배송 조회
//	 * 작성자 : 기드온
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010/unDlvSearchPop.action")
//	public ModelAndView unDlvSearchPop(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010T3");
//	}
//	
//	/*-
//	 * Method ID	: unDlvList
//	 * Method 설명	: 매배송조회 -7D
//	 * 작성자			: chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/unDlvList.action")
//	public ModelAndView unDlvList(Map<String, Object> model) throws Exception {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.unDlvList(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to list :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : listExcel
//	 * Method 설명      : 엑셀다운로드
//	 * 작성자                 : chsong
//	 * @param model
//	 * @return
//	 */
//	@RequestMapping("/WMSSP010/excelUndlvList.action")
//	public void listExcelUndlvLis(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
//		Map<String, Object> map = new HashMap<String, Object>();
//		try {
//			map = service.listExcelUnDlvList(model);
//			GenericResultSet grs = (GenericResultSet) map.get("LIST");
//			if (grs.getTotCnt() > 0) {
//				this.doExcelDownUndlvLis(response, grs);
//			}
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to download excel :", e);
//			}
//		}
//	}
//	
//	/*-
//	 * Method ID : doExcelDown
//	 * Method 설명 : 엑셀다운로드
//	 * 작성자 : kwt
//	 *
//	 * @param response
//	 * @param grs
//	 */
//	protected void doExcelDownUndlvLis(HttpServletResponse response, GenericResultSet grs) {
//        try{
//            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
//            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
//        	String[][] headerEx = {
//				        			 {MessageResolver.getText("화주")			,"0","0", "0", "0", "100"}
//				        			,{MessageResolver.getText("수취인")		,"1","1", "0", "0", "100"}
//				        			,{MessageResolver.getText("이미지여부")		,"2","2", "0", "0", "100"}
//				        			,{MessageResolver.getText("등록구분")		,"3","3", "0", "0", "100"}
//				        			,{MessageResolver.getText("발주일(주문)")	,"4","4", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송예정일")		,"5","5", "0", "0", "100"}
//				        			,{MessageResolver.getText("지정일")		,"6","6", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송완료일")		,"7","7", "0", "0", "100"}
//				        			,{MessageResolver.getText("지정배송기사")	,"8","8", "0", "0", "100"}
//				        			,{MessageResolver.getText("기사전화번호")	,"9","9", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송상태")		,"10","10", "0", "0", "100"}
//				        			,{MessageResolver.getText("취소사유")		,"11","11", "0", "0", "100"}
//				        			,{MessageResolver.getText("판매처")		,"12","12", "0", "0", "100"}
//				        			,{MessageResolver.getText("수취인")		,"13","13", "0", "0", "100"}
//				        			,{MessageResolver.getText("전화번호1")		,"14","14", "0", "0", "100"}
//				        			,{MessageResolver.getText("전화번호2")		,"15","15", "0", "0", "100"}
//				        			,{MessageResolver.getText("주소")			,"16","16", "0", "0", "100"}
//				        			,{MessageResolver.getText("지역")			,"17","17", "0", "0", "100"}
//				        			,{MessageResolver.getText("제품명")		,"18","18", "0", "0", "100"}
//				        			,{MessageResolver.getText("제품코드")		,"19","19", "0", "0", "100"}
//				        			,{MessageResolver.getText("제품설명")		,"20","20", "0", "0", "100"}
//				        			,{MessageResolver.getText("수량")			,"21","21", "0", "0", "100"}
//				        			,{MessageResolver.getText("시리얼번호")		,"22","22", "0", "0", "100"}
//				        			,{MessageResolver.getText("주문옵션")		,"23","23", "0", "0", "100"}
//				        			,{MessageResolver.getText("고객주문번호")	,"24","24", "0", "0", "100"}
//				        			,{MessageResolver.getText("기타1")		,"25","25", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송메모모바일")	,"26","26", "0", "0", "100"}
//				        			,{MessageResolver.getText("인수증메모")		,"27","27", "0", "0", "100"}
//				        			,{MessageResolver.getText("거래배송사")		,"28","28", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송센터")		,"29","29", "0", "0", "100"}
//				        			,{MessageResolver.getText("설치유형(완료)")	,"30","30", "0", "0", "100"}
//				        			,{MessageResolver.getText("설치유형(등록)")	,"31","31", "0", "0", "100"}
//				        			,{MessageResolver.getText("양중여부")		,"32","32", "0", "0", "100"}
//				        			,{MessageResolver.getText("기타작업")		,"33","33", "0", "0", "100"}
//				        			,{MessageResolver.getText("해피콜여부")		,"34","34", "0", "0", "100"}
//				        			,{MessageResolver.getText("해피콜메모")		,"35","35", "0", "0", "100"}
//				        			,{MessageResolver.getText("해피콜일자")		,"36","36", "0", "0", "100"}
//				        			,{MessageResolver.getText("모바일완료일")	,"37","37", "0", "0", "100"}
//				        			,{MessageResolver.getText("모바일완료자")	,"38","38", "0", "0", "100"}
//				        			,{MessageResolver.getText("배송주문ID")		,"39","39", "0", "0", "100"}
//			                  	};
//			//{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
//			String[][] valueName = {
//									 {"TRUST_CUST_NM"			, "S"}
//									,{"VIEW_DLV_CUSTOMER_NM"	, "S"}
//									,{"IMG_SND_YN"				, "S"}
//									,{"TRN_DLV_ORD_TYPE"		, "S"}
//									,{"BUYED_DT"				, "S"}
//									,{"DLV_REQ_DT"				, "S"}
//									,{"DLV_SET_DT"				, "S"}
//									,{"DLV_DT"					, "S"}
//									,{"REQ_DRIVER_NM"			, "S"}
//									,{"REQ_DRIVER_TL"			, "S"}
//									,{"TRN_DLV_ORD_STAT"		, "S"}
//									,{"ETC2"					, "S"}
//									,{"SALES_COMPANY_NM"		, "S"}
//									,{"DLV_CUSTOMER_NM"			, "S"}
//									,{"DLV_PHONE_1"				, "S"}
//									,{"DLV_PHONE_2"				, "S"}
//									,{"DLV_ADDR"				, "S"}
//									,{"DLV_CITY_NM"				, "S"}
//									,{"DLV_PRODUCT_NM"			, "S"}
//									,{"DLV_PRODUCT_CD"			, "S"}
//									,{"TEMP_DLV_PRODUCT_NM"		, "S"}
//									,{"DLV_QTY"					, "S"}
//									,{"SERIAL_NO"				, "S"}
//									,{"ORD_OPTION"				, "S"}
//									,{"ORG_ORD_ID"				, "S"}
//									,{"ETC1"					, "S"}
//									,{"DLV_COMMENT"				, "S"}
//									,{"MEMO"					, "S"}
//									,{"DLV_COMP_NM"				, "S"}
//									,{"REQ_DLV_JOIN_NM"			, "S"}
//									,{"DLV_SET_TYPE_NM"			, "S"}
//									,{"ORG_DLV_SET_TYPE_NM"		, "S"}
//									,{"LIFTING_WORK_YN"			, "S"}
//									,{"ETC_WORK"				, "S"}
//									,{"HAPPY_CALL_YN"			, "S"}
//									,{"HAPPY_CALL_MEMO"			, "S"}
//									,{"HAPPY_CALL_DT"			, "S"}
//									,{"MOBILE_COM_DT"			, "S"}
//									,{"MOBILE_COM_ID"			, "S"}
//									,{"DLV_ORD_ID"				, "S"}
//			                   }; 
//
//			// 파일명
//			String fileName = MessageResolver.getText("미배송7일이전");
//			// 시트명
//			String sheetName = "Sheet1";
//			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
//			String marCk = "N";
//			// ComUtil코드
//			String etc = "";
//
//			ExcelWriter wr = new ExcelWriter();
//			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
//
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("fail download Excel file...", e);
//			}
//		}
//	}
//	
//	/*-
//	 * Method ID    : confPrintEnd
//	 * Method 설명      : 인수증 출력 확정
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/confPrintEnd.action")
//	public ModelAndView confPrintEnd(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.confPrintEnd(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : WMSSP010NEW
//	 * Method 설명      : 고객관리
//	 * 작성자                 : KHKIM
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WINUS/WMSSP011.action")
//	public ModelAndView WMSSP010NEW(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP011", service.selectBox(model));
//	}
//	
//	/*-
//	 * Method ID    : csList
//	 * Method 설명      : 고객관리
//	 * 작성자                 : KHKIM
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/tsList.action")
//	public ModelAndView tsList(Map<String, Object> model) throws Exception {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.tsList(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to list :", e);
//			}
//		}
//		return mav;
//	}
//	/*-
//	 * Method ID    : csList
//	 * Method 설명      : 고객관리
//	 * 작성자                 : KHKIM
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/punctureList.action")
//	public ModelAndView punctureList(Map<String, Object> model) throws Exception {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.punctureList(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to list :", e);
//			}
//		}
//		return mav;
//	}
//	/*-
//	 * Method ID    : csList
//	 * Method 설명      : 고객관리
//	 * 작성자                 : KHKIM
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/punctureDetailList.action")
//	public ModelAndView punctureDetailList(Map<String, Object> model) throws Exception {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.punctureDetailList(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to list :", e);
//			}
//		}
//		return mav;
//	}
//	/*-
//	 * Method ID    : asList
//	 * Method 설명      : as관리
//	 * 작성자                 : KHKIM
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/asList.action")
//	public ModelAndView asList(Map<String, Object> model) throws Exception {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.asList(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to list :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : tsdList
//	 * Method 설명      : 고객관리 상세 조회
//	 * 작성자                 : seongjun kwon
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/tsdList.action")
//	public ModelAndView tsdList(Map<String, Object> model) throws Exception {
//		ModelAndView mav = null;
//		try {
//			mav = new ModelAndView("jqGridJsonView", service.tsdList(model));
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to list :", e);
//			}
//		}
//		return mav;
//	}
//	
//	/*-
//	 * Method ID    : WMSSP011KCCpop
//	 * Method 설명      : KCC 인수증 출력 (OZ)
//	 * 작성자                 : seongjun kwon
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP011KCCpop.action")
//	public ModelAndView WMSSP011KCCpop(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP011KCCpop");
//	}
//
//	
//	@RequestMapping("/WMSSP010RE.action")
//	public ModelAndView WMSSP010RE(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP010RE");
//	}
//	
//	/*-
//	 * Method ID    : WMSSP011HDpop
//	 * Method 설명      : 인수증헤더디테일 (OZ)
//	 * 작성자                 : KCR
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP011HDpop.action")
//	public ModelAndView WMSSP011HDpop(Map<String, Object> model) throws Exception {
//		return new ModelAndView("winus/wmsct/WMSSP011HDpop");
//	}
//	
//	/*-
//	 * Method ID    : confPrintByOne
//	 * Method 설명      : 인수증 출력 확정
//	 * 작성자                 : chSong
//	 * @param   model
//	 * @return  
//	 * @throws Exception 
//	 */
//	@RequestMapping("/WMSSP010/confPrintByOne.action")
//	public ModelAndView confPrintByOne(Map<String, Object> model) throws Exception {
//		ModelAndView mav = new ModelAndView("jsonView");
//		Map<String, Object> m = new HashMap<String, Object>();
//		try {
//			m = service.confPrintByOne(model);
//		} catch (Exception e) {
//			if (log.isErrorEnabled()) {
//				log.error("Fail to save :", e);
//			}
//			m.put("MSG", MessageResolver.getMessage("save.error"));
//		}
//		mav.addAllObjects(m);
//		return mav;
//	}
}
