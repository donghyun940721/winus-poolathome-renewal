package com.logisall.winus.wmsct.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsct.service.WMSCT010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLEncoder;

import javax.imageio.ImageIO;

import java.io.*;
import java.util.zip.*;
@Controller
public class WMSCT010Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT010Service")
    private WMSCT010Service service;

	static final String[] COLUMN_NAME_WMSCT010E8 = {
		"I_SALES_CUST_ID", "I_SOURCE_PRICE", "I_SALES_PRICE", "I_FEE_CONFIRM_YN"
	};
    
    /*-
	 * Method ID    : WMSCT010
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSCT010.action")
	public ModelAndView WMSCT0100(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsct/WMSCT010", service.selectBox(model));
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT010/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 고객관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT010/save.action")
	public ModelAndView save(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 고객관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSSP010/emptyCustRowSave.action")
	public ModelAndView emptyCustRowSave(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.emptyCustRowSave(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명      : 엑셀다운로드
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCT010/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	/*-
	 * Method ID : doExcelDown
	 * Method 설명 : 엑셀다운로드
	 * 작성자 : kwt
	 *
	 * @param response
	 * @param grs
	 */
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
            						{MessageResolver.getText("화주")    		, "0", "0", "0", "0", "100"},        		
            						{MessageResolver.getText("수취인")    		, "1", "1", "0", "0", "100"},
				            		{MessageResolver.getText("이력")    		, "2", "2", "0", "0", "100"},
				            		{MessageResolver.getText("배송지정일")    	, "3", "3", "0", "0", "100"},
				            		{MessageResolver.getText("배송처")    		, "4", "4", "0", "0", "100"},
				            		{MessageResolver.getText("고객주문번호")		, "5", "5", "0", "0", "100"},
				            		{MessageResolver.getText("판매처")    		, "6", "6", "0", "0", "100"},
				            		{MessageResolver.getText("판매처메모")    	, "7", "7", "0", "0", "100"},
				            		{MessageResolver.getText("제품명")    		, "8", "8", "0", "0", "100"},
				            		{MessageResolver.getText("제품코드")    	, "9", "9", "0", "0", "100"},
				            		{MessageResolver.getText("수량")    		, "10", "10", "0", "0", "100"},
				            		{MessageResolver.getText("주문자")    		, "11", "11", "0", "0", "100"},
				            		{MessageResolver.getText("주문자연락처1")	, "12", "12", "0", "0", "100"},
				            		{MessageResolver.getText("주문자연락처2")	, "13", "13", "0", "0", "100"},
				            		{MessageResolver.getText("수취인") 		, "14", "14", "0", "0", "100"},
				            		{MessageResolver.getText("수취인연락처1")	, "15", "15", "0", "0", "100"},
				            		{MessageResolver.getText("수취인연락처2")	, "16", "16", "0", "0", "100"},
				            		{MessageResolver.getText("주소")    		, "17", "17", "0", "0", "100"},
				            		{MessageResolver.getText("지역")    		, "18", "18", "0", "0", "100"},
				            		{MessageResolver.getText("우편번호")    	, "19", "19", "0", "0", "100"},
				            		{MessageResolver.getText("주문일")    		, "20", "20", "0", "0", "100"},
				            		{MessageResolver.getText("배송완료일")    	, "21", "21", "0", "0", "100"},
				            		{MessageResolver.getText("배송상태")    	, "22", "22", "0", "0", "100"},
				            		{MessageResolver.getText("등록구분")    	, "23", "23", "0", "0", "100"},
				            		{MessageResolver.getText("취소사유(등록구분)")	, "24", "24", "0", "0", "100"},
				            		{MessageResolver.getText("시리얼번호")    	, "25", "25", "0", "0", "100"},
				            		{MessageResolver.getText("보증종료일자")		, "26", "26", "0", "0", "100"},
				            		{MessageResolver.getText("취소사유(배송상태)")	, "27", "27", "0", "0", "100"},
				            		{MessageResolver.getText("기타1")			, "28", "28", "0", "0", "100"}
                                  };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
            						{"TRUST_CUST_NM"		, "S"},        		
            						{"VIEW_SALES_CUST_NM"	, "S"},
				            		{"CALL_CNT"				, "S"},
				            		{"DLV_REQ_DT"			, "S"},
				            		{"REQ_DLV_COM_NM"		, "S"},
				            		{"ORG_ORD_ID"			, "S"},
				            		{"SALES_COMPANY_NM"		, "S"},
				            		{"BIZ_MEMO"				, "S"},
				            		{"PRODUCT_NM"			, "S"},
				            		{"PRODUCT_CD"			, "S"},
				            		{"QTY"					, "S"},
				            		{"BUY_CUST_NM"			, "S"},
				            		{"BUY_PHONE_1"			, "S"},
				            		{"BUY_PHONE_2"			, "S"},
				            		{"SALES_CUST_NM"		, "S"},
				            		{"PHONE_1"				, "S"},
				            		{"PHONE_2"				, "S"},
				            		{"ADDR"					, "S"},
				            		{"CITY"					, "S"},
				            		{"ZIP"					, "S"},
				            		{"BUYED_DT"				, "S"},
				            		{"DLV_DT"				, "S"},
				            		{"TRN_DLV_ORD_STAT"		, "S"},
				            		{"TRN_WORK_STAT"		, "S"},
				            		{"CALCEL_MEMO"			, "S"},
				            		{"SERIAL_NO"			, "S"},
				            		{"GUARANTEE_END_DT"		, "S"},
				            		{"ETC2"					, "S"},
				            		{"ETC1"					, "S"}
                                   }; 

			// 파일명
			String fileName = MessageResolver.getText("고객관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID    : imgDown
	 * Method 설명      : 이미지다운로드
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSCT010/imgDown.action")
	public ModelAndView imgDown(Map<String, Object> model) throws Exception {
		ModelAndView mav        = new ModelAndView("jsonView");
		Map<String, Object> map = new HashMap<String, Object>();
		Map<String, Object> m   = new HashMap<String, Object>();
		
		String zipDirPath   = "";
		try {
			map = service.imgDownList(model);
			
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			List vlist = grs.getList();
			Map exlistMap = null;
			
			//String defualtPath   = "C:/WinusTemp";
			 String defualtPath   = "E:/ATCH_FILE/WINUS_TEMP/"+model.get("USER_ID");
//			String defualtPath   = "C:/ATCH_FILE/WINUS_TEMP/"+model.get("USER_ID");
			
	        try {
				for(int i = 0; i < vlist.size(); i++) {
					exlistMap = (Map)vlist.get(i);
					
					String txt = "http://winus.logisall.com/" + String.valueOf(exlistMap.get("DOWN_URL"));
				    char[] txtChar = txt.toCharArray();
				    for (int j = 0; j < txtChar.length; j++) {
				        if (txtChar[j] >= '\uAC00' && txtChar[j] <= '\uD7A3') {
				            String targetText = String.valueOf(txtChar[j]);
				            try {
				                txt = txt.replace(targetText, URLEncoder.encode(targetText, "UTF-8"));
				            } catch (UnsupportedEncodingException e) {
				                e.printStackTrace();
				            }
				        } 
				    }

					URL url = new URL(txt.replaceAll(" ", "%20"));
		            BufferedImage img = ImageIO.read(url);
		            File destdir      = new File(defualtPath + String.valueOf(exlistMap.get("ZIP_NAME")) + String.valueOf(exlistMap.get("USER_ROOT"))); //디렉토리 가져오기
		            
		            if(!destdir.exists()){
		                destdir.mkdirs();
		            }
		            
		            File file = new File(defualtPath + String.valueOf(exlistMap.get("ZIP_NAME")) + String.valueOf(exlistMap.get("USER_ROOT")) + String.valueOf(exlistMap.get("ORG_FILENAME")));
		            ImageIO.write(img, String.valueOf(exlistMap.get("FILE_EXT")), file);
		            
		            zipDirPath = String.valueOf(exlistMap.get("ZIP_NAME"));
				}
				
				this.createZipFile(defualtPath + zipDirPath, defualtPath, zipDirPath.replaceAll("/", "") + ".zip");
				
	            m.put("errCnt", 0);
	            m.put("MSG", MessageResolver.getMessage("save.success"));
	            m.put("RST", defualtPath + "||" + zipDirPath.replaceAll("/", ""));
	        } catch (IOException e) {
	        	e.printStackTrace();
	        }
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("errCnt", 1);
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	public static void createZipFile(String path, String toPath, String fileName) {
        File dir = new File(path);
        String[] list = dir.list();
        String _path;
        if (!dir.canRead() || !dir.canWrite())
            return;

        int len = list.length;
 
        if (path.charAt(path.length() - 1) != '/')
        	_path = path + "/";
        else
            _path = path;
 
        try {
            ZipOutputStream zip_out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(toPath+"/"+fileName), 2048));
 
            for (int i = 0; i < len; i++)
                zip_folder("",new File(_path + list[i]), zip_out);
 
            zip_out.close();
 
        } catch (FileNotFoundException e) {
            //Log.e("File not found", e.getMessage());
        	e.printStackTrace();
        } catch(Exception e) {
        	e.printStackTrace();
        	//throw e;
	    } finally {
 
        }
    }
	
    /**
	 * ZipOutputStream를 넘겨 받아서 하나의 압축파일로 만든다.
	 * @param parent 상위폴더명
	 * @param file 압축할 파일
	 * @param zout 압축전체스트림
	 * @throws IOException
	 */
	private static void zip_folder(String parent, File file, ZipOutputStream zout) throws IOException {
		byte[] data = new byte[2048];
        int read;
        String ZIP_FROM_PATH = "";
        if (file.isFile()) {
            ZipEntry entry = new ZipEntry(parent + file.getName());
            zout.putNextEntry(entry);
            BufferedInputStream instream = new BufferedInputStream(new FileInputStream(file));
 
            while ((read = instream.read(data, 0, 2048)) != -1)
                zout.write(data, 0, read);
 
            zout.flush();
            zout.closeEntry();
            instream.close();
 
        } else if (file.isDirectory()) {
            String parentString = file.getPath().replace(ZIP_FROM_PATH,"");
            parentString = parentString.substring(0,parentString.length() - file.getName().length());
            ZipEntry entry = new ZipEntry(parentString+file.getName()+"/");
            zout.putNextEntry(entry);
 
            String[] list = file.list();
            if (list != null) {
                int len = list.length;
                for (int i = 0; i < len; i++) {
                    zip_folder(entry.getName(),new File(file.getPath() + "/" + list[i]), zout);
                }
            }
        }
	}
	
	/*-
	 * Method ID    : sampleExcelDown
	 * Method 설명      : 엑셀 샘플 다운
	 * 작성자                 : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSCT010/listExcel2.action")
	public void excelDown2(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.getExcelDown2(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown2(response, grs, model);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	
	protected void doExcelDown2(HttpServletResponse response, GenericResultSet grs, Map<String, Object> model) {
		try {
			int iColSize = Integer.parseInt(model.get("Col_Size").toString());
			int iHRowSize = 6; // 고정값
			int iVRowSize = 2; // 고정값
			// 헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
			String[][] headerEx = new String[iColSize][iHRowSize];
			String[][] valueName = new String[iColSize][iVRowSize];
			for (int i = 0; i < iColSize; i++) {
				String nName = MessageResolver.getText((String) model.get("Name_" + i)).replace("&lt;br&gt;", "").replace("&lt;/font&gt;&lt;br&gt;", "").replace("&lt;/font&gt;", "").replace("&lt;font color=&quot;#B1B1B1&quot;&gt;", "");
				// 헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
				headerEx[i] = new String[]{nName, i + "", i + "", "0", "0", "100"};
				// {쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지
				// 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
				valueName[i] = new String[]{(String) model.get("Col_" + i), "S"};
			}
			// 파일명
			String fileName = MessageResolver.getText("고객정보템플릿입력");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
	
	/*-
	 * Method ID : WMSCT010E8
	 * Method 설명 : 엑셀업로드 화면
	 * 작성자 : kwt
	 *
	 * @param model
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCT010E8.action")
	public ModelAndView WMSOP020E8(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmsct/WMSCT010E8");
	}

	/*-
	 * Method ID : uploadLcoInfo
	 * Method 설명 : 엑셀파일업로드
	 * 작성자 : kwt
	 *
	 * @param request
	 * @param response
	 * @param model
	 * @param txtFile
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSCT010E8/uploadCustInfo.action")
	public ModelAndView uploadLcoInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSCT010E8, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
}
