package com.logisall.winus.wmsct.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsct.service.WMSCT020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;






@Service("WMSCT020Service")
public class WMSCT020ServiceImpl implements WMSCT020Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT000Dao")
    private WMSCT000Dao ctdao000;
    
    @Resource(name = "WMSCT020Dao")
    private WMSCT020Dao dao;

    @Resource(name = "WMSCT021Dao")
    private WMSCT021Dao ct021dao;
    
    @Resource(name = "WMSCT010Dao")
    private WMSCT010Dao ct010dao;
    
    /**
     * Method ID   : selectBoxT1
     * Method 설명    : 
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ASCSLC", dao.selectAsCsLc(model));
        map.put("DRIVERS", dao.selectDrivers(model));
        map.put("ASITEM", dao.selectAsOrdItem(model));
        
        map.put("ASERR_1", dao.selectAsErr(model));
        map.put("DLVCUST", dao.selectDlvCust(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveE3
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveE3(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("vrSalesCustId" 	, model.get("vrSalesCustId"));
            modelDt.put("vrSrchAsCsType" 	, model.get("vrSrchAsCsType"));
            modelDt.put("vrSalesCustNo" 	, model.get("vrSalesCustNo"));
            modelDt.put("vrSrchRitemId" 	, model.get("vrSrchRitemId"));
            modelDt.put("vrSrchAsCsLc" 		, model.get("vrSrchAsCsLc"));
            modelDt.put("vrSrchDrivers" 	, model.get("vrSrchDrivers"));
            modelDt.put("TextReqContents" 	, model.get("TextReqContents"));
            modelDt.put("vrSrchReqMemo" 	, model.get("vrSrchReqMemo"));
            modelDt.put("TextResult" 		, model.get("TextResult"));
            modelDt.put("vrSrchReqCost" 	, model.get("vrSrchReqCost"));
            modelDt.put("vrSrchComCost" 	, model.get("vrSrchComCost"));
            modelDt.put("vrSrchBuyDt" 		, model.get("vrSrchBuyDt"));
            modelDt.put("vrSrchReqDtFrom" 	, model.get("vrSrchReqDtFrom"));
            modelDt.put("vrSrchReqId" 		, model.get("vrSrchReqId"));
            modelDt.put("vrAsCsSeq" 		, model.get("vrAsCsSeq"));
            modelDt.put("vrSrchComType" 	, model.get("vrSrchComType"));
            modelDt.put("vrSrchInOutBound" 	, model.get("vrSrchInOutBound"));
            
            modelDt.put("vrSrchOrdItem" 	, model.get("vrSrchOrdItem"));
            modelDt.put("vrSrchOrdOption" 	, model.get("vrSrchOrdOption"));
            modelDt.put("vrSrchOrdBill" 	, model.get("vrSrchOrdBill"));
            modelDt.put("vrSrchCostType" 	, model.get("vrSrchCostType"));
            modelDt.put("AS_SALES_CUST_ID" 	, model.get("AS_SALES_CUST_ID"));
            modelDt.put("vrSrchDlvCompNm" 	, model.get("vrSrchDlvCompNm"));
            modelDt.put("typicalCustId" 	, model.get("typicalCustId"));
		    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
		    modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
            
            if("INSERT".equals(model.get("ST_GUBUN"))){
                //AS 입력 저장 시 배송관리 주문입력
                if("AS".equals(model.get("vrSrchAsCsType"))){
                	//AS 입력 저장 시 AS주문상품이 있을 경우만
                    Map<String, Object> modelSP = new HashMap<String, Object>();
                    String chkSalestData = dao.chkSalestData(modelSP);
                    modelDt.put("AS_SALES_CUST_ID" 	, chkSalestData);
                    
                    dao.insertE3(modelDt);
                    dao.insertAsOrderWMSCT010(modelDt);
                    dao.insertAsOrderWMSSP010(modelDt);
                }else{
                	dao.insertE3(modelDt);
                }
            }else if("UPDATE".equals(model.get("ST_GUBUN"))){
                //AS 입력 저장 시 배송관리 주문입력
                if("AS".equals(model.get("vrSrchAsCsType"))){
					dao.updateE3(modelDt);
					dao.updateAsOrderWMSCT010(modelDt);
            		dao.updateAsOrderWMSSP010(modelDt);
                }else{
                	dao.updateE3(modelDt);
                }
                
                //IF테이블에 등록되어있는지 체크
                //등록되어있다면 FLAG UPDATE 처리 해야 얼라이언스 주문 호출 시 가져감
                modelDt.put("SALES_CUST_ID" 	, model.get("vrSalesCustId"));
                String IF_CHECK = (String)ct010dao.ifCheckUpdate(modelDt);
                if("Y".equals(IF_CHECK)){
                	modelDt.put("WMS_IF_FLAG", "U");
                	ct010dao.ifUpdate(modelDt);
                }
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }

    /**
     * 
     * 대체 Method ID   : saveE4
     * 대체 Method 설명    : 
     * 작성자            : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveE4(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        //비대면 버튼 사용
        String vrSrchNoContact = (String) model.get("vrSrchNoContact");
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            
            modelDt.put("vrSalesCustId" 	, model.get("vrSalesCustId"));
            modelDt.put("vrSrchAsCsType" 	, model.get("vrSrchAsCsType"));
            modelDt.put("vrSalesCustNo" 	, model.get("vrSalesCustNo"));
            modelDt.put("vrSrchRitemId" 	, model.get("vrSrchRitemId"));
            modelDt.put("vrSrchAsCsLc" 		, model.get("vrSrchAsCsLc"));
            modelDt.put("vrSrchDrivers" 	, model.get("vrSrchDrivers"));
            modelDt.put("TextReqContents" 	, model.get("TextReqContents"));
            modelDt.put("vrSrchReqMemo" 	, model.get("vrSrchReqMemo"));
            modelDt.put("TextResult" 		, model.get("TextResult"));
            modelDt.put("vrSrchReqCost" 	, model.get("vrSrchReqCost"));
            modelDt.put("vrSrchComCost" 	, model.get("vrSrchComCost"));
            modelDt.put("vrSrchBuyDt" 		, model.get("vrSrchBuyDt"));
            modelDt.put("vrSrchReqDtFrom" 	, model.get("vrSrchReqDtFrom"));
            modelDt.put("vrSrchReqId" 		, model.get("vrSrchReqId"));
            modelDt.put("vrAsCsSeq" 		, model.get("vrAsCsSeq"));
            modelDt.put("vrSrchComType" 	, model.get("vrSrchComType"));
            modelDt.put("vrSrchInOutBound"  , model.get("vrSrchInOutBound"));
            modelDt.put("vrSrchTrustCustNm" , model.get("vrSrchTrustCustNm"));
            modelDt.put("vrSrchFirstInsDt" , model.get("vrSrchFirstInsDt"));
            
            modelDt.put("vrSrchOrdItem" 	, model.get("vrSrchOrdItem"));
            modelDt.put("vrSrchOrdOption" 	, model.get("vrSrchOrdOption"));
            modelDt.put("vrSrchOrdBill" 	, model.get("vrSrchOrdBill"));
            modelDt.put("vrSrchCostType" 	, model.get("vrSrchCostType"));
            modelDt.put("AS_SALES_CUST_ID"  , model.get("AS_SALES_CUST_ID"));
            modelDt.put("vrSrchDlvOrdId"    , model.get("vrSrchDlvOrdId"));
            modelDt.put("vrSrchDlvCompNm" 	, model.get("vrSrchDlvCompNm"));
            modelDt.put("vrSrchDlvQty" 		, model.get("vrSrchDlvQty"));
            modelDt.put("typicalCustId" 	, model.get("typicalCustId"));
            modelDt.put("vrSrchDlvSetAs" 	, model.get("vrSrchDlvSetAs"));
            modelDt.put("vrSrchEtc1" 	    , model.get("vrSrchEtc1"));
            modelDt.put("vrSrchEtc2" 	    , model.get("vrSrchEtc2"));
            modelDt.put("vrDlvOrdType" 		, model.get("vrDlvOrdType"));
		    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
		    modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
            	
//            modelDt.put("vrSrchCsCustTy" 	, model.get("vrSrchCsCustTy"));
            modelDt.put("vrSrchCsRecTy" 	, model.get("vrSrchCsRecTy"));
            modelDt.put("vrSrchCsCtgy01" 	, model.get("vrSrchCsCtgy01"));
            modelDt.put("vrSrchCsCtgy02" 	, model.get("vrSrchCsCtgy02"));
            modelDt.put("vrSrchCsCompanyType" , model.get("vrSrchCsCompanyType"));

            //저장
            if("INSERT".equals(model.get("ST_GUBUN"))){

                Map<String, Object> modelSP = new HashMap<String, Object>();
                String chkSalestData = dao.chkSalestData(modelSP);
                modelDt.put("AS_SALES_CUST_ID" 	, chkSalestData);

                if("CS".equals(model.get("vrSrchAsCsType"))){
                	ct021dao.insertE4(modelDt);
                }else if("교환".equals(model.get("vrSrchAsCsType"))){
                    for(int j = 1; j<=2; j++){
                    	//교환설치37, 교환회수27 각각 등록처리
                        if(j == 1){
                        	
                            modelDt.put("PARAM_WORK_STAT" 	, vrSrchNoContact.equals("NON") ? "94" : "37" );
                        }else{
                            modelDt.put("PARAM_WORK_STAT" 	, vrSrchNoContact.equals("NON") ? "93" : "27" );
                            modelDt.put("vrSrchDlvQty" 		, model.get("vrSrchDlvQty2"));
                            modelDt.put("vrSrchRitemId" 	, model.get("vrSrchRitemId2"));
                            chkSalestData = dao.chkSalestData(modelSP);
                            modelDt.put("AS_SALES_CUST_ID" 	, chkSalestData);
                        }
					    insertCt020(modelDt, "Y");
                    }
                }else if("설치".equals(model.get("vrSrchAsCsType"))){
                    modelDt.put("PARAM_WORK_STAT" 	, vrSrchNoContact.equals("NON") ? "91" : "00" );
					insertCt020(modelDt, "Y");
					//	HD 상세상품 존재 시 등록 WMSSP011 
                    for(int j = 2; j<=3; j++){
                        if(((String) model.get("vrSrchRitemId"+j)).length() != 0) {
                            modelDt.put("vrSrchDlvQty" 		, model.get("vrSrchDlvQty"+j));
                            modelDt.put("vrSrchRitemId" 	, model.get("vrSrchRitemId"+j));
                            ct021dao.insertDlvOrderWMSSP011(modelDt);
                        }
                    }
                }else if("반품".equals(model.get("vrSrchAsCsType"))){
                    modelDt.put("PARAM_WORK_STAT" 	, vrSrchNoContact.equals("NON") ? "92" : "23");
					insertCt020(modelDt, "Y");
                }else if("AS".equals(model.get("vrSrchAsCsType"))){
                    modelDt.put("PARAM_WORK_STAT" 	, "05");
					insertCt020(modelDt, "N");
                }
            }
            //수정 
            else if("UPDATE".equals(model.get("ST_GUBUN"))){
                if("CS".equals(model.get("vrSrchAsCsType"))){
                	ct021dao.updateE4(modelDt);
                }else if("설치".equals(model.get("vrSrchAsCsType"))){
                    modelDt.put("vrSrchDlvOrdSubSeq" 	, "1");
					updateCt020(modelDt, "Y");
					// HD 상세상품 존재 시 수정 WMSSP011
                    for(int j = 2; j<=3; j++){
                        if(((String) model.get("vrSrchRitemId"+j)).length() != 0) {
                            modelDt.put("vrSrchDlvOrdSubSeq" 	, j);
                            modelDt.put("vrSrchDlvQty" 	    	, model.get("vrSrchDlvQty"+j));
                            modelDt.put("vrSrchRitemId" 	    , model.get("vrSrchRitemId"+j));
                            ct021dao.updateDlvOrderWMSSP011(modelDt);
                        }
                    }
                }else if("AS".equals(model.get("vrSrchAsCsType"))){
					updateCt020(modelDt, "N");
                }else{
                    modelDt.put("vrSrchDlvOrdSubSeq" 	, "1");
                    //교환회수 상품 파리미터 처리 
                    if("27".equals(model.get("vrDlvOrdType"))) {
                        modelDt.put("vrSrchDlvQty" 		, model.get("vrSrchDlvQty2"));
                        modelDt.put("vrSrchRitemId" 	, model.get("vrSrchRitemId2"));
                    }
					updateCt020(modelDt, "Y");
                }
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }

    /**
     * 
     * 대체 Method ID   : insertCt020
     * 대체 Method 설명    : 
     * 작성자            : sing09
     * @param model, sp011 (Y/N)
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> insertCt020(Map<String, Object> model, String sp011) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // sp011 Y일 경우 WMSSP011 UPDATE 처리
        // 추후 프로시저로 로직 수정 예정
        try{
            ct021dao.insertE4(model);
            ct021dao.insertDlvOrderWMSCT010(model);
            ct021dao.insertDlvOrderWMSSP010(model);
            if("Y".equals(sp011)) { ct021dao.insertDlvOrderWMSSP011(model); }

        } catch(Exception e){
            throw e;
        }
        return m;
    }
    /**
     * 
     * 대체 Method ID   : updateCt020
     * 대체 Method 설명    : 
     * 작성자            : sing09
     * @param model, sp011 (Y/N)
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateCt020(Map<String, Object> model, String sp011) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // sp011 Y일 경우 WMSSP011 UPDATE 처리
        // 추후 프로시저로 로직 수정 예정
        try{
            ct021dao.updateE4(model);
            ct021dao.updateDlvOrderWMSCT010(model);
            ct021dao.updateDlvOrderWMSSP010(model);
            if("Y".equals(sp011)) { ct021dao.updateDlvOrderWMSSP011(model); }

        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }

    /**
     * 
     * 대체 Method ID   : listCT021
     * 대체 Method 설명    : 
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCT021(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", ct021dao.listCT021(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : custInfoSave
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> custInfoSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("vrSalesCustId" 	, model.get("vrSalesCustId"));
            modelDt.put("vrSrchAddr" 		, model.get("vrSrchAddr"));
            modelDt.put("vrSrchZip" 		, model.get("vrSrchZip"));
            modelDt.put("vrSrchBuyCustNm" 	, model.get("vrSrchBuyCustNm"));
            modelDt.put("vrSrchBuyPhone1" 	, model.get("vrSrchBuyPhone1"));
            modelDt.put("vrSrchBuyPhone2" 	, model.get("vrSrchBuyPhone2"));
            modelDt.put("vrSrchSalesCustNm" , model.get("vrSrchSalesCustNm"));
            modelDt.put("vrSrchPhone1" 		, model.get("vrSrchPhone1"));
            modelDt.put("vrSrchPhone2" 		, model.get("vrSrchPhone2"));
            modelDt.put("vrSelectTabsGb" 	, model.get("vrSelectTabsGb"));
            modelDt.put("vrAscsSeq" 		, model.get("vrAscsSeq"));
            System.out.println(" >> " + model.get("vrAscsSeq"));
		    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
            
            if(1 == 1){
            	dao.custInfoSaveWMSCT010(modelDt);
            	dao.custInfoSaveWMSSP010(modelDt);
            	
            	//IF테이블에 등록되어있는지 체크
                //등록되어있다면 FLAG UPDATE 처리 해야 얼라이언스 주문 호출 시 가져감
                modelDt.put("SALES_CUST_ID" 	, model.get("vrSalesCustId"));
                String IF_CHECK = (String)ct010dao.ifCheckUpdate(modelDt);
                if("Y".equals(IF_CHECK)){
                	modelDt.put("WMS_IF_FLAG", "U");
                	ct010dao.ifUpdate(modelDt);
                }
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }

    /**
     * 
     * 대체 Method ID   : custInfoSave021
     * 대체 Method 설명    : 
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
	@Override
    public Map<String, Object> custInfoSave021(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            Map<String, Object> modelSp = new HashMap<String, Object>();
            modelSp.put("I_SALES_CUST_ID" 	, (String)model.get("vrSalesCustId"));
            modelSp.put("I_ADDR" 			, (String)model.get("vrSrchAddr"));
            modelSp.put("I_ZIP" 			, (String)model.get("vrSrchZip"));
            modelSp.put("I_BUY_CUST_NM" 	, (String)model.get("vrSrchBuyCustNm"));
            modelSp.put("I_BUY_PHONE_1" 	, (String)model.get("vrSrchBuyPhone1"));
            
            modelSp.put("I_BUY_PHONE_2" 	, (String)model.get("vrSrchBuyPhone2"));
            modelSp.put("I_SALES_CUST_NM"	, (String)model.get("vrSrchSalesCustNm"));
            modelSp.put("I_PHONE_1" 		, (String)model.get("vrSrchPhone1"));
            modelSp.put("I_PHONE_2" 		, (String)model.get("vrSrchPhone2"));
            modelSp.put("I_ORG_ORD_ID" 		, (String)model.get("vrSrchOrgOrdId"));
            
            modelSp.put("I_SALES_COMPANY_NM", (String)model.get("vrSalesCompanyNm"));
            modelSp.put("I_FIRST_INS_DT" 	, (String)model.get("vrSrchFirstInsDt"));
            
            modelSp.put("I_LC_ID"  			, (String)model.get(ConstantIF.SS_SVC_NO)); 
            modelSp.put("I_USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
            modelSp.put("I_WORK_IP"			, (String)model.get(ConstantIF.SS_CLIENT_IP));
            
		    modelSp = (Map<String, Object>)ctdao000.spCustInfoUpdate(modelSp);
    		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelSp.get("O_MSG_CODE")), (String)modelSp.get("O_MSG_NAME"));
		    
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
        } catch(BizException be) {
			m.put("MSG", MessageResolver.getMessage("save.error"));
    		m.put("errCnt", -1);
    		m.put("MSG_ORA", be.getMessage());
        }catch (Exception e) {
    		throw e;
    	}
        return m;
    }
    
    /*-
	 * Method ID   : customerfile
	 * Method 설명 : 고객정보 상세조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> customerfile(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("CUSTOMR_INFO")) {
			map.put("CUSTOMR_INFO", dao.customerfile(model));
		}
		return map;
	}
    
    /*-
	 * Method ID   : selectAsErrList
	 * Method 설명 : 
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> selectAsErrList(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		
		if (srchKey.equals("ASERR_2")) {
			map.put("ASERR_2", dao.selectAsErrList2(model));
		} else if (srchKey.equals("ASERR_3")) {
			map.put("ASERR_3", dao.selectAsErrList3(model));
		} else if (srchKey.equals("ASERR_4")) {
			map.put("ASERR_4", dao.selectAsErrList4(model));
		}
		
		return map;
	}
    
    /**
     * 
     * 대체 Method ID   : nominalTransfer
     * 대체 Method 설명    : 명의이전 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> nominalTransfer(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
        	int insCnt = Integer.parseInt(model.get("selectIds").toString());
        	if(insCnt > 0){
        		String[] vrTrSalesCustId 	= new String[insCnt];
        		String[] vrTrSrchReqDtFrom 	= new String[insCnt];
        		String[] vrSrchAddr			= new String[insCnt];
        		String[] vrTrSrchZip		= new String[insCnt];
        		String[] vrSrchSalesCustNm	= new String[insCnt];
        		String[] vrSrchPhone1		= new String[insCnt];
        		String[] vrSrchPhone2		= new String[insCnt];
        		String[] vrTrSrchBuyCustNm	= new String[insCnt];
        		String[] vrTrSrchBuyPhone1	= new String[insCnt];
        		String[] vrTrSrchBuyPhone2	= new String[insCnt];

        		for(int i = 0 ; i < insCnt ; i ++){        
        			vrTrSalesCustId[i]   = (String)model.get("vrTrSalesCustId"+i);
        			vrTrSrchReqDtFrom[i] = (String)model.get("vrTrSrchReqDtFrom"+i);
        			vrSrchAddr[i]        = (String)model.get("vrSrchAddr"+i);
        			vrTrSrchZip[i]       = (String)model.get("vrTrSrchZip"+i);
        			vrSrchSalesCustNm[i] = (String)model.get("vrSrchSalesCustNm"+i);
        			vrSrchPhone1[i]      = (String)model.get("vrSrchPhone1"+i);
        			vrSrchPhone2[i]      = (String)model.get("vrSrchPhone2"+i);
        			vrTrSrchBuyCustNm[i] = (String)model.get("vrTrSrchBuyCustNm"+i);
        			vrTrSrchBuyPhone1[i] = (String)model.get("vrTrSrchBuyPhone1"+i);
        			vrTrSrchBuyPhone2[i] = (String)model.get("vrTrSrchBuyPhone2"+i);
        		}
        		
        		//프로시져에 보낼것들 다담는다
        		Map<String, Object> modelIns = new HashMap<String, Object>();
        		modelIns.put("vrTrSalesCustId"  , vrTrSalesCustId);
        		modelIns.put("vrTrSrchReqDtFrom", vrTrSrchReqDtFrom);
        		modelIns.put("vrSrchAddr"       , vrSrchAddr);
        		modelIns.put("vrTrSrchZip"      , vrTrSrchZip);
        		modelIns.put("vrSrchSalesCustNm", vrSrchSalesCustNm);
        		modelIns.put("vrSrchPhone1"     , vrSrchPhone1);
        		modelIns.put("vrSrchPhone2"     , vrSrchPhone2);
        		modelIns.put("vrTrSrchBuyCustNm", vrTrSrchBuyCustNm);
        		modelIns.put("vrTrSrchBuyPhone1", vrTrSrchBuyPhone1);
        		modelIns.put("vrTrSrchBuyPhone2", vrTrSrchBuyPhone2);
        		
        		//session 정보
        		modelIns.put("LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
        		modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
        		modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
        		modelIns = (Map<String, Object>)dao.nominalTransfer(modelIns);
        		ServiceUtil.isValidReturnCode("WMSCT020T3", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
        	}

        	m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
  
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveOutOrderQ2
     * 대체 Method 설명    : 출고주문(저장,수정,삭제)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveOutOrderQ2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        log.info(model);
        try{
        	
            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
                //저장, 수정         
                String[] ordSeq         = new String[insCnt];         
                String[] ritemId        = new String[insCnt];     
                String[] custLotNo      = new String[insCnt];     
                String[] realInQty      = new String[insCnt];     
                
                String[] realOutQty     = new String[insCnt];                     
                String[] makeDt         = new String[insCnt];         
                String[] timePeriodDay  = new String[insCnt];     
                String[] locYn          = new String[insCnt];     
                String[] pdaCd          = new String[insCnt];     
                
                String[] workYn         = new String[insCnt];                
                String[] rjType         = new String[insCnt];         
                String[] realPltQty     = new String[insCnt];     
                String[] realBoxQty     = new String[insCnt];     
                String[] confYn         = new String[insCnt];     
                
                String[] unitAmt        = new String[insCnt];                
                String[] amt            = new String[insCnt];         
                String[] eaCapa         = new String[insCnt];     
                String[] boxBarcode     = new String[insCnt];     
                String[] inOrdUomId     = new String[insCnt];     
                String[] inWorkUomId     = new String[insCnt]; 
                
                String[] outOrdUomId    = new String[insCnt];
                String[] outWorkUomId    = new String[insCnt];  
                String[] minUomId       = new String[insCnt]; 
                String[] inOrdQty       = new String[insCnt];
                String[] inWorkOrdQty       = new String[insCnt];  
                String[] outOrdQty      = new String[insCnt]; 
                String[] outWorkOrdQty       = new String[insCnt];  
                String[] inDspQty       = new String[insCnt]; 
                
                String[] outDspQty      = new String[insCnt];   
                String[] refSubLotId    = new String[insCnt];   
                String[] outOrdWeight   = new String[insCnt];   
                String[] ordDesc        = new String[insCnt];   
                String[] etc2           = new String[insCnt];   
                
                //추가
                String[] itemBestDate       = new String[insCnt];   //상품유효기간     
                String[] itemBestDateEnd    = new String[insCnt];   //상품유효기간만료일
                
                String[] cntrId         = new String[insCnt];
                String[] cntrNo         = new String[insCnt];
                String[] cntrType       = new String[insCnt];
                String[] cntrSealNo     = new String[insCnt];
                                
                for(int i = 0 ; i < insCnt ; i ++){        
                    ordSeq[i]           = (String)model.get("I_ORD_SEQ"+i);          
                    ritemId[i]          = (String)model.get("I_RITEM_ID"+i);      
                    custLotNo[i]        = (String)model.get("I_CUST_LOT_NO"+i);      
                    realInQty[i]        = (String)model.get("I_REAL_IN_QTY"+i);      
                    
                    realOutQty[i]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
                    makeDt[i]           = (String)model.get("I_MAKE_DT"+i);          
                    timePeriodDay[i]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
                    locYn[i]            = (String)model.get("I_LOC_YN"+i);      
                    pdaCd[i]            = (String)model.get("I_PDA_CD"+i);      
                    
                    workYn[i]           = (String)model.get("I_WORK_YN"+i);                 
                    rjType[i]           = (String)model.get("I_RJ_TYPE"+i);          
                    realPltQty[i]       = (String)model.get("I_REAL_PLT_QTY"+i);      
                    realBoxQty[i]       = (String)model.get("I_REAL_BOX_QTY"+i);      
                    confYn[i]           = (String)model.get("I_CONF_YN"+i);      
                    
                    unitAmt[i]          = (String)model.get("I_UNIT_AMT"+i);                 
                    amt[i]              = (String)model.get("I_AMT"+i);          
                    eaCapa[i]           = (String)model.get("I_EA_CAPA"+i);      
                    boxBarcode[i]       = (String)model.get("I_BOX_BARCODE"+i);      
                    inOrdUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i);
                    inWorkUomId[i]       = (String)model.get("I_IN_WORK_UOM_ID"+i); 
                    //inWorkUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i); 
                    outOrdUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);
                    outWorkUomId[i]      = (String)model.get("I_OUT_WORK_UOM_ID"+i);   
                    //outWorkUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i); 
                    minUomId[i]         = (String)model.get("I_MIN_UOM_ID"+i);                
                    inOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);
                    inWorkOrdQty[i]         = (String)model.get("I_IN_WORK_ORD_QTY"+i);
                    //inWorkOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);
                    outOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);    
                    outWorkOrdQty[i]        = (String)model.get("I_OUT_WORK_ORD_QTY"+i);
                    //outWorkOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);
                    inDspQty[i]         = (String)model.get("I_IN_DSP_QTY"+i);
                    
                    outDspQty[i]        = (String)model.get("I_OUT_DSP_QTY"+i);
                    refSubLotId[i]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
                    outOrdWeight[i]     = (String)model.get("I_OUT_ORD_WEIGHT"+i);     
                    ordDesc[i]          = (String)model.get("I_ORD_DESC"+i);     
                    etc2[i]             = (String)model.get("I_ETC2"+i); 
                    
                    //추가 
                    itemBestDate[i]     = (String)model.get("I_ITEM_BEST_DATE"+i);      
                    // itemBestDateEnd[i]  = (String)model.get("I_ITEM_BEST_DATE_END"+i);
                    
                    if ( model.get("I_ITEM_BEST_DATE_END" +i) != null ) { 
                    	itemBestDateEnd[i] = ((String)model.get("I_ITEM_BEST_DATE_END" +i)).trim().replaceAll("-", "");
                    } else {
                    	itemBestDateEnd[i] = (String)model.get("I_ITEM_BEST_DATE_END" +i);
                    }                    
                    
                    cntrId[i]           = (String)model.get("I_CNTR_ID"+i);
                    cntrNo[i]           = (String)model.get("I_CNTR_NO"+i);
                    cntrType[i]         = (String)model.get("I_CNTR_TYPE"+i);
                    cntrSealNo[i]       = (String)model.get("I_CNTR_SEAL_NO"+i);
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("ordId"            , model.get("vrOrdId"));
                modelIns.put("inReqDt"          , model.get("inReqDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
                modelIns.put("inDt"             , model.get("inDt"));
                modelIns.put("custPoid"         , model.get("custPoid"));
                
                modelIns.put("custPoseq"        , model.get("custPoseq"));
                modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));  
                modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
                modelIns.put("inWhId"           , model.get("inWhId"));
                modelIns.put("outWhId"          , model.get("vrOutWhId"));  
                
                modelIns.put("transCustId"      , model.get("vrTransCustId"));   
                modelIns.put("custId"           , model.get("vrSrchCustId"));   
                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));    
                modelIns.put("blNo"             , model.get("vrBlNo"));         
                modelIns.put("workStat"         , model.get("vrWorkStat"));       //하드
                
                modelIns.put("ordType"          , model.get("vrOrdType"));            //하드
                modelIns.put("ordSubtype"       , model.get("vrOrdSubType"));   
                modelIns.put("outReqDt"         , model.get("vrCalOutReqDt").toString().replace("-", ""));  
                modelIns.put("outDt"            , model.get("outDt"));
                modelIns.put("pdaStat"          , model.get("pdaStat"));
                
                modelIns.put("workSeq"          , model.get("workSeq"));
                modelIns.put("capaTot"          , model.get("capaTot"));
                modelIns.put("kinOutYn"         , model.get("vrKinOutYn"));      
                modelIns.put("carConfYn"        , model.get("carConfYn"));
                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
                
                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
                modelIns.put("payYn"            , model.get("vrPayYn"));
                modelIns.put("asnInReqDt"       , model.get("vrCalInReqDt").toString().replace("-", ""));  
                modelIns.put("cntrNoM"          , model.get("vrCntrNoM"));
                modelIns.put("cntrSealNoM"      , model.get("vrCntrSealNoM"));
                
                //sub
                modelIns.put("ordSeq"           , ordSeq);
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("realInQty"        , realInQty);
                modelIns.put("realOutQty"       , realOutQty);
                
                modelIns.put("makeDt"           , makeDt);
                modelIns.put("timePeriodDay"    , timePeriodDay);
                modelIns.put("locYn"            , locYn);
                modelIns.put("pdaCd"            , pdaCd);
                modelIns.put("workYn"           , workYn);
                
                modelIns.put("rjType"           , rjType);
                modelIns.put("realPltQty"       , realPltQty);
                modelIns.put("realBoxQty"       , realBoxQty);
                modelIns.put("confYn"           , confYn);
                modelIns.put("unitAmt"          , unitAmt);
                
                modelIns.put("amt"              , amt);
                modelIns.put("eaCapa"           , eaCapa);
                modelIns.put("boxBarcode"       , boxBarcode);
                modelIns.put("inOrdUomId"       , inOrdUomId);
                modelIns.put("inWorkUomId"      , inWorkUomId);

                modelIns.put("outOrdUomId"      , outOrdUomId);
                modelIns.put("outWorkUomId"     , outWorkUomId);
                modelIns.put("minUomId"         , minUomId);
                modelIns.put("inOrdQty"         , inOrdQty);
                modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
                
                modelIns.put("outOrdQty"        , outOrdQty);
                modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
                modelIns.put("inDspQty"         , inDspQty);
                modelIns.put("outDspQty"        , outDspQty);
                modelIns.put("refSubLotId"      , refSubLotId);
                
                modelIns.put("outOrdWeight"     , outOrdWeight);                
                modelIns.put("cntrId"           , cntrId);
                modelIns.put("cntrNo"           , cntrNo);
                modelIns.put("cntrType"         , cntrType);
                modelIns.put("cntrSealNo"       , cntrSealNo);
                
                modelIns.put("ordDesc"          , ordDesc);             
                modelIns.put("etc2"             , etc2);
                
                //추가된거(아직프로시져는 안탐)
                modelIns.put("itemBestDate"     , itemBestDate);
                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);                
                
                //session 정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveOutOrderQ2(modelIns);
                
                modelIns.put("rstOrdId"      , (String)modelIns.get("O_MSG_NAME"));
                modelIns.put("vrAsCsId"      , model.get("vrAsCsId"));
                modelIns.put("vrAsCsNo"      , model.get("vrAsCsNo"));
                dao.saveAscsRst(modelIns);
                
                ServiceUtil.isValidReturnCode("WMSCT020Q1", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            
            m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
}
