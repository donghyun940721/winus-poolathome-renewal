package com.logisall.winus.wmsct.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCT020Dao")
public class WMSCT020Dao extends SqlMapAbstractDAO {

    /**
     * Method ID  : selectDrivers
     * Method 설명  : 기사정보 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectAsCsLc(Map<String, Object> model){
     	return executeQueryForList("wmsct020.selectAsCsLc", model);
     }
     
     /**
      * Method ID  : selectDrivers
      * Method 설명  : 기사정보 데이터셋
      * 작성자             : 기드온
      * @param model
      * @return
      */
      public Object selectDrivers(Map<String, Object> model){
      	return executeQueryForList("wmssp010.selectDrivers", model);
      }
      
      /**
       * Method ID  : selectAsOrdItem
       * Method 설명  : 기사정보 데이터셋
       * 작성자             : 기드온
       * @param model
       * @return
       */
       public Object selectAsOrdItem(Map<String, Object> model){
       	return executeQueryForList("wmsct020.selectAsOrdItem", model);
       }
      
      /**
  	 * Method ID : insertE3 
  	 * Method 설명 :
  	 * 작성자 : chsong
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertE3(Map<String, Object> model) {
  		return executeInsert("wmsct020.insertE3", model);
  	}

    /**
  	 * Method ID : insertE4
  	 * Method 설명 :
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertE4(Map<String, Object> model) {
  		return executeInsert("wmsct021.insertE4", model);
  	}
  	
  	/**
	 * Method ID : list
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsct020.list", model);
	}

  	/**
	 * Method ID : listCT021
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listCT021(Map<String, Object> model) {
		return executeQueryPageWq("wmsct021.list", model);
	}
	
	/**
	 * Method ID : updateE3 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateE3(Map<String, Object> model) {
		return executeUpdate("wmsct020.updateE3", model);
	}

	/**
	 * Method ID : updateE4
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateE4(Map<String, Object> model) {
		return executeUpdate("wmsct021.updateE4", model);
	}
	
	 /**
  	 * Method ID : insertAsOrderWMSCT010 
  	 * Method 설명 :
  	 * 작성자 : chsong
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertAsOrderWMSCT010(Map<String, Object> model) {
  		return executeInsert("wmsct020.insertAsOrderWMSCT010", model);
  	}
  	
	 /**
  	 * Method ID : insertAsOrderWMSSP010 
  	 * Method 설명 :
  	 * 작성자 : chsong
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertAsOrderWMSSP010(Map<String, Object> model) {
  		return executeInsert("wmsct020.insertAsOrderWMSSP010", model);
  	}

	 /**
  	 * Method ID : insertDlvOrderWMSCT010 
  	 * Method 설명 :
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertDlvOrderWMSCT010(Map<String, Object> model) {
  		return executeInsert("wmsct021.insertDlvOrderWMSCT010", model);
  	}
  	
	 /**
  	 * Method ID : insertDlvOrderWMSSP010 
  	 * Method 설명 :
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertDlvOrderWMSSP010(Map<String, Object> model) {
  		return executeInsert("wmsct021.insertDlvOrderWMSSP010", model);
  	}
  	
  	/*-
     * Method ID : checSalestData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String chkSalestData(Map<String, Object> model) {
        return (String)executeView("wmsct020.chkSalestData", model);
    }   
    
    /**
	 * Method ID : updateAsOrderWMSCT010 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateAsOrderWMSCT010(Map<String, Object> model) {
		return executeUpdate("wmsct020.updateAsOrderWMSCT010", model);
	}

    /**
	 * Method ID : updateDlvOrderWMSCT010 
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateDlvOrderWMSCT010(Map<String, Object> model) {
		return executeUpdate("wmsct021.updateDlvOrderWMSCT010", model);
	}
	
	/**
	 * Method ID : updateAsOrderWMSSP010 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateAsOrderWMSSP010(Map<String, Object> model) {
		return executeUpdate("wmsct020.updateAsOrderWMSSP010", model);
	}

    /**
	 * Method ID : updateDlvOrderWMSSP010 
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateDlvOrderWMSSP010(Map<String, Object> model) {
		return executeUpdate("wmsct021.updateDlvOrderWMSSP010", model);
	}
	
	/**
	 * Method ID : custInfoSave 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object custInfoSaveWMSCT010(Map<String, Object> model) {
		return executeUpdate("wmsct020.custInfoSaveWMSCT010", model);
	}
	public Object custInfoSaveWMSSP010(Map<String, Object> model) {
		return executeUpdate("wmsct020.custInfoSaveWMSSP010", model);
	}

	/**
	 * Method ID : custInfoSave021
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object custInfoSave021WMSCT010(Map<String, Object> model) {
		return executeUpdate("wmsct021.custInfoSave021WMSCT010", model);
	}
	public Object custInfoSave021WMSSP010(Map<String, Object> model) {
		return executeUpdate("wmsct021.custInfoSave021WMSSP010", model);
	}
	
	/**
     * Method ID  : customerfile
     * Method 설명  : 고객정보 상세조회
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object customerfile(Map<String, Object> model){
        return executeQueryForList("wmsct020.customerfile", model);
    }
    
    /**
     * Method ID  : selectAsErr
     * Method 설명  : 기사정보 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectAsErr(Map<String, Object> model){
     	return executeQueryForList("wmsct020.selectAsErr", model);
     }
     
     /**
      * Method ID  : selectAsErrList2
      * Method 설명  : 
      * 작성자             : chsong
      * @param model
      * @return
      */
     public Object selectAsErrList2(Map<String, Object> model){
         return executeQueryForList("wmsct020.selectAsErrList2", model);
     }
     
     /**
      * Method ID  : selectAsErrList3
      * Method 설명  :
      * 작성자             : chsong
      * @param model
      * @return
      */
     public Object selectAsErrList3(Map<String, Object> model){
         return executeQueryForList("wmsct020.selectAsErrList3", model);
     }
     /**
      * Method ID  : selectAsErrList4
      * Method 설명  :
      * 작성자             : chsong
      * @param model
      * @return
      */
     public Object selectAsErrList4(Map<String, Object> model){
         return executeQueryForList("wmsct020.selectAsErrList4", model);
     }
     
     /**
      * Method ID    : nominalTransfer
      * Method 설명      : 
      * 작성자                 : chsong
      * @param   model
      * @return
      */
     public Object nominalTransfer(Map<String, Object> model){
         executeUpdate("wmsct020t3.pk_wmsct020.sp_nominalTransfer", model);
         return model;
     }
     
     /**
      * Method ID    : saveOutOrderQ2
      * Method 설명      : 출고관리 출고주문 등록,수정
      * 작성자                 : chsong
      * @param   model
      * @return
      */
     public Object saveOutOrderQ2(Map<String, Object> model){
         executeUpdate("wmsop030.pk_wmsop030.sp_insert_order", model);
         return model;
     }
     
     /**
 	 * Method ID : saveAscsRst 
 	 * Method 설명 :  
 	 * 작성자 : chsong
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public Object saveAscsRst(Map<String, Object> model) {
 		return executeUpdate("wmsct020Q2.saveAscsRst", model);
 	}
 	
 	/**
     * Method ID  : selectDlvCust
     * Method 설명  : 배송처조회
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectDlvCust(Map<String, Object> model){
     	return executeQueryForList("wmsct020.selectDlvCust", model);
     }
}
