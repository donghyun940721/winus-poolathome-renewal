package com.logisall.winus.wmsct.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsct.service.WMSSP090Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSSP090Service")
public class WMSSP090ServiceImpl implements WMSSP090Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSSP090Dao")
    private WMSSP090Dao dao;
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DRIVERS", dao.selectDrivers(model));
        map.put("ITEMGRP", dao.selectItem(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : listNew
     * 대체 Method 설명    : 고객만족도(고객별)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listNew(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
            vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.listNew(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listNew2
     * 대체 Method 설명    : 고객만족도(기사별)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listNew2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
            vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.listNew2(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listNew3
     * 대체 Method 설명    : 고객만족도(센터별)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listNew3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
       
        List<String> cityArr = new ArrayList();
        String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
        for (String keyword : spSrchAddrLike ){
            cityArr.add(keyword);
        }
        model.put("cityArr", cityArr);
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.listNew3(model));
        return map;
    }
    

    /**
     * 
     * 대체 Method ID   : smsInfoSaveCsat
     * 대체 Method 설명    : 고객만족도 서비스 SMS 발송 여부
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> smsInfoSaveCsat(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();   
    		modelDt.put("RTN_KEY"     	, model.get("vrRtnKey"+1));
    		modelDt.put("REG_NO"    	, model.get(ConstantIF.SS_USER_NO));
			dao.smsInfoSaveCsat(modelDt);
    		
    		m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(Exception e){
    		m.put("errCnt", -1);
    		m.put("MSG", e.getMessage());
    		throw e;
    	}
    	return m;
    }
}
