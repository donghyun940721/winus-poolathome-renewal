package com.logisall.winus.wmsct.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsct.service.WMSCT011Service;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCT011Service")
public class WMSCT011ServiceImpl implements WMSCT011Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT000Dao")
    private WMSCT000Dao ctdao000;
    
    @Resource(name = "WMSCT011Dao")
    private WMSCT011Dao dao;

    @Resource(name = "WMSSP010Dao")
    private WMSSP010Dao dao010;
    
    private final static String[] CHECK_VALIDATE_WMSCT010 = {"SALES_CUST_ID", "SALES_CUST_NM"};
    private final static String[] CHECK_VALIDATE_WMSSP010 = {"DLV_ORD_ID", "SALES_CUST_ID"};
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DRIVERS", dao.selectDrivers(model));
        map.put("ITEMGRP", dao.selectItem(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : listNew
     * 대체 Method 설명    : 고객관리 조회
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listNew(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }

        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
            transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
            vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.listNew(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listNew2
     * 대체 Method 설명    : 고객관리 (CS)
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listNew2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
            transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
            vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.listNew2(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listNew3
     * 대체 Method 설명    : 고객관리 (AS) 조회
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listNew3(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
       
        List<String> cityArr = new ArrayList();
        String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
        for (String keyword : spSrchAddrLike ){
            cityArr.add(keyword);
        }
        model.put("cityArr", cityArr);
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.listNew3(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : saveNew
     * 대체 Method 설명  : 고객정보관리 저장
     * 작성자           : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveNew(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                Map<String, Object> modelSp = new HashMap<String, Object>();
                
                modelDt.put("vrSrchCustId" 		, model.get("vrSrchCustId"));
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));

                modelDt.put("SALES_CUST_NM"     , model.get("SALES_CUST_NM"+i));
                modelDt.put("PHONE_1"           , model.get("PHONE_1"+i));
                modelDt.put("PHONE_2"           , model.get("PHONE_2"+i));
                modelDt.put("SERIAL_NO"         , model.get("SERIAL_NO"+i));
                modelDt.put("BUY_CUST_NM"       , model.get("BUY_CUST_NM"+i));
                modelDt.put("BUY_PHONE_1"       , model.get("BUY_PHONE_1"+i));
                modelDt.put("BUY_PHONE_2"       , model.get("BUY_PHONE_2"+i));
                modelDt.put("ZIP"               , model.get("ZIP"+i));
                modelDt.put("ADDR"              , model.get("ADDR"+i));
                modelDt.put("ORG_ORD_ID"        , model.get("ORG_ORD_ID"+i));
                modelDt.put("DLV_ETC2"          , model.get("DLV_ETC2"+i));
                modelDt.put("GUARANTEE_END_DT"  , model.get("GUARANTEE_END_DT"+i));
                modelDt.put("ETC1"              , model.get("ETC1"+i));
                modelDt.put("MEMO"              , model.get("MEMO"+i));
                modelDt.put("HAPPY_CALL_MEMO"   , model.get("HAPPY_CALL_MEMO"+i));
                modelDt.put("DLV_ORD_ID"        , model.get("DLV_ORD_ID"+i));
                modelDt.put("DLV_SET_DT"        , model.get("DLV_SET_DT"+i));
                modelDt.put("SALES_CUST_ID"     , model.get("SALES_CUST_ID"+i));
                modelDt.put("P_SALES_CUST_ID"   , model.get("P_SALES_CUST_ID"+i));
                modelDt.put("TRUST_CUST_ID"     , model.get("TRUST_CUST_ID"+i));
                modelDt.put("CALCEL_MEMO"       , model.get("CALCEL_MEMO"+i));
                modelDt.put("SOURCE_PRICE"      , model.get("SOURCE_PRICE"+i));
                modelDt.put("SALES_PRICE"       , model.get("SALES_PRICE"+i));
                modelDt.put("FEE_CONFIRM_YN"    , model.get("FEE_CONFIRM_YN"+i));
                modelDt.put("SALES_COMPANY_NM"  , model.get("SALES_COMPANY_NM"+i));
                //컬럼 row 색상
                modelDt.put("COLUMN_COLOR"  	, model.get("COLUMN_COLOR"+i));
                modelDt.put("FIRST_INS_DT"  	, model.get("FIRST_INS_DT"+i));
                modelDt.put("WORK_STAT"  		, model.get("WORK_STAT"+i));
                
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSCT010);
                
                //
                modelSp.put("I_SALES_CUST_ID" 	, (String)model.get("SALES_CUST_ID"+i));
                modelSp.put("I_ADDR" 			, (String)model.get("ADDR"+i));
                modelSp.put("I_ZIP" 			, (String)model.get("ZIP"+i));
                modelSp.put("I_BUY_CUST_NM" 	, (String)model.get("BUY_CUST_NM"+i));
                modelSp.put("I_BUY_PHONE_1" 	, (String)model.get("BUY_PHONE_1"+i));
                
                modelSp.put("I_BUY_PHONE_2" 	, (String)model.get("BUY_PHONE_2"+i));
                modelSp.put("I_SALES_CUST_NM"	, (String)model.get("SALES_CUST_NM"+i));
                modelSp.put("I_PHONE_1" 		, (String)model.get("PHONE_1"+i));
                modelSp.put("I_PHONE_2" 		, (String)model.get("PHONE_2"+i));
                modelSp.put("I_ORG_ORD_ID" 		, (String)model.get("ORG_ORD_ID"+i));
                
                modelSp.put("I_SALES_COMPANY_NM", (String)model.get("SALES_COMPANY_NM"+i));
                modelSp.put("I_FIRST_INS_DT" 	, (String)model.get("FIRST_INS_DT"+i));

                modelSp.put("I_LC_ID"  			, (String)model.get(ConstantIF.SS_SVC_NO)); 
                modelSp.put("I_USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
                modelSp.put("I_WORK_IP"			, (String)model.get(ConstantIF.SS_CLIENT_IP));

                if("INSERT".equals(model.get("ST_GUBUN"+i))){}
                else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                	dao.updateNewAS010(modelDt);
                    dao.updateNewCT010(modelDt);
                	dao.updateNewSP010(modelDt);

        		    modelSp = (Map<String, Object>)ctdao000.spCustInfoUpdate(modelSp);
            		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelSp.get("O_MSG_CODE")), (String)modelSp.get("O_MSG_NAME"));
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    /**
     * 
     * 대체 Method ID   : saveNewAs
	 * Method 설명      : 고객관리 > AS저장
	 * 작성자                 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveNewAs(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	int insCnt = Integer.parseInt(model.get("selectIds").toString());
        	
            for(int i = 0 ; i < insCnt ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("vrSrchCustId" 			, model.get("vrSrchCustId"));
                modelDt.put("selectIds" 			, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  			, model.get("ST_GUBUN"+i));

                modelDt.put("DLV_IN_REQ_DT"     	, model.get("DLV_IN_REQ_DT"+i));
                
                modelDt.put("DLV_SET_DT"     		, model.get("DLV_SET_DT"+i));
                modelDt.put("DLV_REQ_DT"     		, model.get("DLV_REQ_DT"+i));
                modelDt.put("DLV_DT"     			, model.get("DLV_DT"+i));
                modelDt.put("SERIAL_NO"     		, model.get("SERIAL_NO"+i));
                modelDt.put("DLV_ORD_STAT"      	, model.get("DLV_ORD_STAT"+i));
                modelDt.put("DLV_COMP_ID"     		, model.get("DLV_COMP_ID"+i));

                modelDt.put("REQ_DRIVER_ID"     	, model.get("REQ_DRIVER_ID"+i));
                modelDt.put("SET_DRIVER_ID"     	, model.get("SET_DRIVER_ID"+i));
                modelDt.put("DLV_ORD_TYPE"      	, model.get("DLV_ORD_TYPE"+i));
                modelDt.put("DLV_PHONE_1"     		, model.get("DLV_PHONE_1"+i));
                modelDt.put("DLV_PHONE_2"     		, model.get("DLV_PHONE_2"+i));

                modelDt.put("DLV_ADDR"     			, model.get("DLV_ADDR"+i));
                modelDt.put("DLV_CITY"     			, model.get("DLV_CITY"+i));
                modelDt.put("ORD_OPTION"     		, model.get("ORD_OPTION"+i));
                modelDt.put("MEMO"     				, model.get("MEMO"+i));
                modelDt.put("ETC1"     				, model.get("ETC1"+i));
                modelDt.put("ETC2"     				, model.get("ETC2"+i));
                modelDt.put("DLV_SET_TYPE"   		, model.get("DLV_SET_TYPE"+i));
                modelDt.put("HAPPY_CALL_YN"   		, model.get("HAPPY_CALL_YN"+i));
                modelDt.put("HAPPY_CALL_MEMO"   	, model.get("HAPPY_CALL_MEMO"+i));
                modelDt.put("HAPPY_CALL_DT"     	, model.get("HAPPY_CALL_DT"+i));
                
                modelDt.put("DLV_ORD_ID"     		, model.get("DLV_ORD_ID"+i));
                modelDt.put("SALES_CUST_ID"     	, model.get("SALES_CUST_ID"+i));
                modelDt.put("REQ_DLV_JOIN_ID"   	, model.get("REQ_DLV_JOIN_ID"+i));
                
                modelDt.put("DLV_PRODUCT"   		, model.get("DLV_PRODUCT"+i));
                modelDt.put("DLV_PRODUCT_CD"    	, model.get("DLV_PRODUCT_CD"+i));
                modelDt.put("TEMP_DLV_PRODUCT_NM"	, model.get("TEMP_DLV_PRODUCT_NM"+i));
                modelDt.put("TRUST_CUST_ID"			, model.get("TRUST_CUST_ID"+i));
                modelDt.put("LIFTING_WORK_YN"		, model.get("LIFTING_WORK_YN"+i));
                modelDt.put("ORD_LC_ID"				, model.get("ORD_LC_ID"+i));
                
                modelDt.put("COST_NM"				, model.get("COST_NM"+i));
                modelDt.put("REPAIR_PARTS_YN"		, model.get("REPAIR_PARTS_YN"+i));
                modelDt.put("MOVE_DT"				, model.get("MOVE_DT"+i));
                modelDt.put("MOVE_MEMO"				, model.get("MOVE_MEMO"+i));
                modelDt.put("FIRST_INS_DT"			, model.get("FIRST_INS_DT"+i));
                
                modelDt.put("DLV_COMMENT"			, model.get("DLV_COMMENT"+i));
                modelDt.put("REPAIR_PARTS"			, model.get("REPAIR_PARTS"+i));
                modelDt.put("COST_TYPE"				, model.get("COST_TYPE"+i));
                modelDt.put("COM_COST"				, model.get("COM_COST"+i));
                
                modelDt.put("SALES_COMPANY_NM"		, model.get("SALES_COMPANY_NM"+i));
                modelDt.put("DLV_CUSTOMER_NM"		, model.get("VIEW_DLV_CUSTOMER_NM"+i));
                modelDt.put("ORG_ORD_ID"			, model.get("ORG_ORD_ID"+i));
                modelDt.put("PAY_REQ_YN"			, model.get("PAY_REQ_YN"+i));
                modelDt.put("AS_REQ_COST"			, model.get("AS_REQ_COST"+i));
                modelDt.put("AS_COST_MEMO"			, model.get("AS_COST_MEMO"+i));
                modelDt.put("AS_ETC1"				, model.get("AS_ETC1"+i));
                modelDt.put("AS_ETC2"				, model.get("AS_ETC2"+i));
                
                modelDt.put("UPD_NO"    			, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    				, model.get(ConstantIF.SS_SVC_NO));
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSSP010);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                	
            		dao.updateAsSP010(modelDt);
            		dao.updateAsCT010(modelDt); 
            		dao.updateAsAS010(modelDt); 
                    
                    //AS출고 건이 배송완료 일 경우
                    if("05".equals(model.get("DLV_ORD_TYPE"+i)) && "99".equals(model.get("DLV_ORD_STAT"+i))){
                    	if(!"99".equals(model.get("BF_DLV_ORD_STAT"+i))){
                    		System.out.println("AS출고 건이 배송완료 일 경우 updateWMSAS010_DlvComp");
                    		dao010.updateWMSAS010_DlvComp(modelDt);
                    	}
                    }
                    //AS출고 건이 취소요청 일 경우
                    if("05".equals(model.get("DLV_ORD_TYPE"+i)) && "89".equals(model.get("DLV_ORD_STAT"+i))){
                    	if(!"89".equals(model.get("BF_DLV_ORD_STAT"+i))){
                    		System.out.println("AS출고 건이 취소요청 일 경우 updateWMSAS010_DlvCancel");
                    		dao010.updateWMSAS010_DlvCancel(modelDt);
                    	}
                    }
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    
    /**
     * 
     * 대체 Method ID   : deleteNew
     * 대체 Method 설명  : CS관리(N)팝업 발주 건 삭제
     * 작성자           : kijun11
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteNew(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	 
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
              
                
                modelDt.put("vrSrchCustId" 		, model.get("vrSrchCustId"));
                modelDt.put("selectIds" 		, model.get("selectIds"));
   

                modelDt.put("SALES_CUST_NM"     , model.get("SALES_CUST_NM"+i));
                modelDt.put("PHONE_1"           , model.get("PHONE_1"+i));
                modelDt.put("PHONE_2"           , model.get("PHONE_2"+i));
                modelDt.put("ZIP"               , model.get("ZIP"+i));
                modelDt.put("ADDR"              , model.get("ADDR"+i));
                modelDt.put("ORG_ORD_ID"        , model.get("ORG_ORD_ID"+i));
                modelDt.put("DLV_ETC2"          , model.get("DLV_ETC2"+i));
                modelDt.put("GUARANTEE_END_DT"  , model.get("GUARANTEE_END_DT"+i));
                modelDt.put("ETC1"              , model.get("ETC1"+i));
                modelDt.put("MEMO"              , model.get("MEMO"+i));
                modelDt.put("HAPPY_CALL_MEMO"   , model.get("HAPPY_CALL_MEMO"+i));
                modelDt.put("DLV_ORD_ID"        , model.get("DLV_ORD_ID"+i));
                modelDt.put("DLV_SET_DT"        , model.get("DLV_SET_DT"+i));
                modelDt.put("SALES_CUST_ID"     , model.get("SALES_CUST_ID"+i));
                modelDt.put("P_SALES_CUST_ID"   , model.get("P_SALES_CUST_ID"+i)); //
                modelDt.put("TRUST_CUST_ID"     , model.get("TRUST_CUST_ID"+i));
                modelDt.put("CALCEL_MEMO"       , model.get("CALCEL_MEMO"+i));
                modelDt.put("SOURCE_PRICE"      , model.get("SOURCE_PRICE"+i));
                modelDt.put("SALES_PRICE"       , model.get("SALES_PRICE"+i));
                modelDt.put("FEE_CONFIRM_YN"    , model.get("FEE_CONFIRM_YN"+i));
                modelDt.put("SALES_COMPANY_NM"  , model.get("SALES_COMPANY_NM"+i));
                //컬럼 row 색상
                modelDt.put("COLUMN_COLOR"  	, model.get("COLUMN_COLOR"+i));
                modelDt.put("FIRST_INS_DT"  	, model.get("FIRST_INS_DT"+i));
                modelDt.put("WORK_STAT"  		, model.get("WORK_STAT"+i));
                
                //고객주문 ROW 삭제
                modelDt.put("DELETEGUBUN"  		, model.get("DELETEGUBUN"+i));
                
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
                
                if("DELETEALL".equals(model.get("DELETEGUBUN"+i))&&errCnt==0){
                	dao.deleteNewWmssp011(modelDt);
                	dao.deleteNewWmssp010(modelDt); 
                	dao.deleteNewWmsas010(modelDt);
                	dao.deleteNewWmsct010(modelDt);
					//dao.deleteNew(modelDt);  // 통합주문등록부분 수정필요..               
                }else if(errCnt==0){
                	dao.deleteNewWmssp011(modelDt);
                	dao.deleteNewWmssp010(modelDt); 
                	dao.deleteNewWmsas010(modelDt);
                	dao.deleteNewWmsct010(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }   
    
    
    /**
     * 
     * 대체 Method ID   : deleteNew
     * 대체 Method 설명  : CS관리(N)팝업 발주_CS관리 ROW 건 삭제
     * 작성자           : kijun11
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteNew_CS(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	 
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
              
                
                modelDt.put("vrSrchCustId" 		, model.get("vrSrchCustId"));
                modelDt.put("selectIds" 		, model.get("selectIds"));
   

                modelDt.put("SALES_CUST_NM"     , model.get("SALES_CUST_NM"+i));
                modelDt.put("PHONE_1"           , model.get("PHONE_1"+i));
                modelDt.put("PHONE_2"           , model.get("PHONE_2"+i));
                modelDt.put("ZIP"               , model.get("ZIP"+i));
                modelDt.put("ADDR"              , model.get("ADDR"+i));
                modelDt.put("ORG_ORD_ID"        , model.get("ORG_ORD_ID"+i));
                modelDt.put("DLV_ETC2"          , model.get("DLV_ETC2"+i));
                modelDt.put("GUARANTEE_END_DT"  , model.get("GUARANTEE_END_DT"+i));
                modelDt.put("ETC1"              , model.get("ETC1"+i));
                modelDt.put("MEMO"              , model.get("MEMO"+i));
                modelDt.put("HAPPY_CALL_MEMO"   , model.get("HAPPY_CALL_MEMO"+i));
                modelDt.put("DLV_ORD_ID"        , model.get("DLV_ORD_ID"+i));
                modelDt.put("DLV_SET_DT"        , model.get("DLV_SET_DT"+i));
                modelDt.put("SALES_CUST_ID"     , model.get("SALES_CUST_ID"+i));
                modelDt.put("P_SALES_CUST_ID"   , model.get("P_SALES_CUST_ID"+i)); //
                modelDt.put("TRUST_CUST_ID"     , model.get("TRUST_CUST_ID"+i));
                modelDt.put("CALCEL_MEMO"       , model.get("CALCEL_MEMO"+i));
                modelDt.put("SOURCE_PRICE"      , model.get("SOURCE_PRICE"+i));
                modelDt.put("SALES_PRICE"       , model.get("SALES_PRICE"+i));
                modelDt.put("FEE_CONFIRM_YN"    , model.get("FEE_CONFIRM_YN"+i));
                modelDt.put("SALES_COMPANY_NM"  , model.get("SALES_COMPANY_NM"+i));
                //컬럼 row 색상
                modelDt.put("COLUMN_COLOR"  	, model.get("COLUMN_COLOR"+i));
                modelDt.put("FIRST_INS_DT"  	, model.get("FIRST_INS_DT"+i));
                modelDt.put("WORK_STAT"  		, model.get("WORK_STAT"+i));
                
                //CS접수번호
                modelDt.put("ASCS_NO"  			, model.get("ASCS_NO"+i));
                
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));

         
                if(errCnt==0){                	
                	//dao.deleteNewWmsas010(modelDt);
                	dao.deleteCSUpdateWmsas010(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }   
    
    
   
}
