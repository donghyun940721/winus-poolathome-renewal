package com.logisall.winus.wmsct.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCT102Dao")
public class WMSCT102Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsct102.list", model);
    }
    
    /**
	 * Method ID	: insert 
	 * Method 설명	: 
	 * 작성자			:
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmsct102.insert", model);
	}

	/**
	 * Method ID	: update 
	 * Method 설명	: 
	 * 작성자			:
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmsct102.update", model);
	}
}


