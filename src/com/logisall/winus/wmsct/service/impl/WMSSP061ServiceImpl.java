package com.logisall.winus.wmsct.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import javax.annotation.Resource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.stereotype.Service;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsct.service.WMSSP061Service;
import com.logisall.ws.interfaces.common.OrderWebService;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSSP061Service")
public class WMSSP061ServiceImpl extends AbstractServiceImpl implements WMSSP061Service {
    
    @Resource(name = "WMSSP061Dao")
    private WMSSP061Dao dao;
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("MYLC", dao.myLcList(model));
        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 입출고미완료현황 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
             
            List<String> myLcArr = new ArrayList();
            String[] spMyLcListVal = model.get("myLcListVal").toString().split(",");
            for (String keyword : spMyLcListVal ){
            	myLcArr.add(keyword);
            }
            model.put("myLcArr", myLcArr);
            
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * 대체 Method ID   : save
     * 대체 Method 설명    : 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	int temCnt = Integer.parseInt(model.get("selectIds").toString());
            if(temCnt > 0){
            	String[] lcId		= new String[temCnt];
            	String[] custId		= new String[temCnt];
            	String[] custCd		= new String[temCnt];
            	String[] ritemId	= new String[temCnt];
            	String[] ritemCd	= new String[temCnt];
            	String[] ordQty		= new String[temCnt];
            	
            	String[] ordType	= new String[temCnt];
            	String[] ordSubtype	= new String[temCnt];
            	
            	for(int i = 0 ; i < temCnt ; i ++){
            		lcId[i]			= (String)model.get("LC_ID"+i);
            		custId[i]		= (String)model.get("CUST_ID"+i);
            		custCd[i]		= (String)model.get("CUST_CD"+i);
            		ritemId[i]		= (String)model.get("RITEM_ID"+i);
            		ritemCd[i]		= (String)model.get("RITEM_CD"+i);
            		ordQty[i]		= (String)model.get("ORD_QTY"+i);
            	}
        	
            	Map<String, Object> modelDt = new HashMap<String, Object>();
            	modelDt.put("I_LC_ID"			, lcId);
            	modelDt.put("I_CUST_ID"			, custId);
                modelDt.put("I_CUST_CD"			, custCd);
                modelDt.put("I_RITEM_ID"		, ritemId);
                modelDt.put("I_RITEM_CD"		, ritemCd);
                modelDt.put("I_ORD_QTY"			, ordQty);
                
                modelDt.put("I_ORD_TYPE"		, (String)model.get("ORD_TYPE"+0));
                modelDt.put("I_ORD_SUBTYPE"		, (String)model.get("ORD_SUBTYPE"+0));
                
                modelDt.put("I_REQ_LC_ID"	, (String)model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelDt.put("I_USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
                
                modelDt = (Map<String, Object>)dao.save(modelDt);
    			ServiceUtil.isValidReturnCode("WMSSP061", String.valueOf(modelDt.get("O_MSG_CODE")), (String)modelDt.get("O_MSG_NAME"));             
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );            

        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 입출고미완료현황 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();           
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        List<String> myLcArr = new ArrayList();
        String[] spMyLcListVal = model.get("myLcListVal").toString().split(",");
        for (String keyword : spMyLcListVal ){
        	myLcArr.add(keyword);
        }
        model.put("myLcArr", myLcArr);
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 입출고미완료현황 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listHD(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
             
            List<String> myLcArr = new ArrayList();
            String[] spMyLcListVal = model.get("myLcListValHD").toString().split(",");
            for (String keyword : spMyLcListVal ){
            	myLcArr.add(keyword);
            }
            model.put("myLcArr", myLcArr);
            System.out.println("여기");
            map.put("LIST", dao.listHD(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 입출고미완료현황 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();           
    	model.put("pageIndex", "1");
    	model.put("pageSize", "60000");
    	
    	List<String> myLcArr = new ArrayList();
    	String[] spMyLcListVal = model.get("myLcListVal").toString().split(",");
    	for (String keyword : spMyLcListVal ){
    		myLcArr.add(keyword);
    	}
    	model.put("myLcArr", myLcArr);
    	
    	map.put("LIST", dao.list2(model));
    	
    	return map;
    }
    
    /**
     * Method ID : list
     * Method 설명 : 입출고미완료현황 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
             
            List<String> myLcArr = new ArrayList();
            String[] spMyLcListVal = model.get("myLcListVal").toString().split(",");
            for (String keyword : spMyLcListVal ){
            	myLcArr.add(keyword);
            }
            model.put("myLcArr", myLcArr);
            
            map.put("LIST", dao.list2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * Method ID : listExcel
     * Method 설명 : 입출고미완료현황 엑셀다운
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcelHD(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();           
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        List<String> myLcArr = new ArrayList();
        String[] spMyLcListVal = model.get("myLcListValHD").toString().split(",");
        for (String keyword : spMyLcListVal ){
        	myLcArr.add(keyword);
        }
        model.put("myLcArr", myLcArr);
        
        map.put("LIST", dao.listHD(model));
        
        return map;
    }
}
