package com.logisall.winus.wmsct.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSSP100Dao")
public class WMSSP100Dao extends SqlMapAbstractDAO {
	

	/**
    * Method ID  : selectDrivers
    * Method 설명  : 기사정보 데이터셋
    * 작성자             : 
    * @param model
    * @return
    */
    public Object selectDrivers(Map<String, Object> model){
    	return executeQueryForList("wmssp010.selectDrivers", model);
    }
    

    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }

    /**
     * Method ID  : selectAsncl
     * Method 설명  : 
     * 작성자             : 
     * @param model
     * @return
     */
     public Object selectAsncl(Map<String, Object> model){
     	return executeQueryForList("wmssp010.selectAsncl", model);
     }
     

 	/**
 	 * Method ID : list 
 	 * Method 설명 : 반품관리 리스트 
 	 * 작성자 : sing09
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public GenericResultSet list(Map<String, Object> model) {
 		GenericResultSet wqrs = new GenericResultSet();
 		wqrs.setList(list("wmssp100.list", model));
 		return wqrs;
 	}
 	
 	/**
 	 * Method ID : detailList 
 	 * Method 설명 : 반품관리 디테일
 	 * 작성자 : sing09
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public GenericResultSet detailList(Map<String, Object> model) {
 		GenericResultSet wqrs = new GenericResultSet();
 		wqrs.setList(list("wmssp100.detailList", model));
 		return wqrs;
 	}
}
