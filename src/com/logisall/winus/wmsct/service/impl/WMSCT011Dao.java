package com.logisall.winus.wmsct.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCT011Dao")
public class WMSCT011Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : listNew
	 * Method 설명 : 고객정보관리
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listNew(Map<String, Object> model) {
	    return executeQueryPageWq("wmsct011.listNew", model);
	}
	
	/**
	 * Method ID : listNew2
	 * Method 설명 : 고객관리(CS)
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listNew2(Map<String, Object> model) {
	    return executeQueryPageWq("wmsct011.listNew2", model);
	}

	/**
	 * Method ID : listNew3 
	 * Method 설명 : 고객관리(AS)
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listNew3(Map<String, Object> model) {
	    return executeQueryPageWq("wmsct011.listNew3", model);
	}

	/**
	 * Method ID : updateNewAS010 
	 * Method 설명 : 고객관리 수정 AS010 
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateNewAS010(Map<String, Object> model) {
		return executeUpdate("wmsct011.updateNewAS010", model);
	}
	/**
	 * Method ID : updateNewCT010 
	 * Method 설명 : 고객관리 수정 CT010
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateNewCT010(Map<String, Object> model) {
		return executeUpdate("wmsct011.updateNewCT010", model);
	}
	
	/**
	 * Method ID : updateNewSP010 
	 * Method 설명 : 고객관리 수정 SP010
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateNewSP010(Map<String, Object> model) {
		return executeUpdate("wmsct011.updateNewSP010", model);
	}
	
    /**
     * Method ID  : selectDrivers
     * Method 설명  : 기사정보 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectDrivers(Map<String, Object> model){
     	return executeQueryForList("wmssp010.selectDrivers", model);
     }
     
     /**
      * Method ID : selectItem
      * Method 설명 : 상품군 리스트 조회
      * 작성자 : 기드온
      * @param model
      * @return
      */
     public Object selectItem(Map<String, Object> model){
         return executeQueryForList("wmsms094.selectItemGrp", model);
     }
     
     /**
 	 * Method ID : updateAsSP010
 	 * Method 설명 : AS관리 수정  WMSSP010
 	 * 작성자 : sing09
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public Object updateAsSP010(Map<String, Object> model) {
 		return executeUpdate("wmsct011.updateAsSP010", model);
 	}
     
 	/**
 	 * Method ID : updateAsCT010 
 	 * Method 설명 : AS관리 수정  WMSCT010
 	 * 작성자 : sing09
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public Object updateAsCT010(Map<String, Object> model) {
 		return executeUpdate("wmsct011.updateAsCT010", model);
 	}
 	
 	/**
 	 * Method ID : updateAsAS010 
 	 * Method 설명 : AS관리 수정  WMSAS010
 	 * 작성자 : sing09
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public Object updateAsAS010(Map<String, Object> model) {
 		return executeUpdate("wmsct011.updateAsAS010", model);
 	}
 	
 	  /**
     * Method ID : deleteUpdateWmssp011 
     * Method 설명 : kijun11
     * 작성자 : 
     * 
     * @param model
     * @return
     */
    public Object deleteNewWmssp011(Map<String, Object> model) {
    	return executeUpdate("wmsct011.deleteUpdateWmssp011", model);
    }
    
    /**
   	 * Method ID : deleteNewWmssp010 
   	 * Method 설명 : 고객관리 삭제
   	 * 작성자 : kijun11
   	 * 
   	 * @param model
   	 * @return
   	 */
   	public Object deleteNewWmssp010(Map<String, Object> model) {
   		return executeUpdate("wmsct011.deleteUpdateWmssp010", model);
   	}
   	

	/**
	 * Method ID : deleteNewWmsct010 
	 * Method 설명 : 고객관리 삭제
	 * 작성자 : kijun11
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteNewWmsct010(Map<String, Object> model) {
		return executeUpdate("wmsct011.deleteUpdateWmsct010", model);
	}
	
	
	/**
	 * Method ID : deleteNewWmsas010 
	 * Method 설명 : 고객관리 삭제
	 * 작성자 : kijun11
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteNewWmsas010(Map<String, Object> model) {
		return executeUpdate("wmsct011.deleteUpdateWmsas010", model);
	}
	
	
	/**
	 * Method ID : deleteNewWmsas010 
	 * Method 설명 : 고객관리 삭제
	 * 작성자 : kijun11
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteCSUpdateWmsas010(Map<String, Object> model) {
		return executeUpdate("wmsct011.deleteCSUpdateWmsas010", model);
	}
	
	
	 /**
	 * Method ID : deleteNew 
	 * Method 설명 : 고객관리 삭제 (DEL_YN 를 Y로 수정) 
	 * 작성자 : kijun11
	 * 
	 * @param model
	 * @return
	 */
	public Object deleteNew(Map<String, Object> model) {
		return executeUpdate("wmsct011.delete", model);
	}

   	
  
 	
}
