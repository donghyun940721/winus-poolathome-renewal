package com.logisall.winus.wmsct.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsct.service.WMSCT010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCT010Service")
public class WMSCT010ServiceImpl implements WMSCT010Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT000Dao")
    private WMSCT000Dao ctdao000;
    
    @Resource(name = "WMSCT010Dao")
    private WMSCT010Dao dao;

    @Resource(name = "WMSSP010Dao")
    private WMSSP010Dao dao010;
    
    private final static String[] CHECK_VALIDATE_WMSCT010 = {"SALES_CUST_ID", "SALES_CUST_NM"};
    private final static String[] CHECK_VALIDATE_WMSCT010_2 = {"SALES_CUST_ID", "SALES_COMPANY_ID", "SALES_COMPANY_NM"};
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DRIVERS", dao.selectDrivers(model));
        map.put("ITEMGRP", dao.selectItem(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 고객관리 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm3").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 고객관리 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));

                modelDt.put("SALES_CUST_ID"     , model.get("SALES_CUST_ID"+i));
                modelDt.put("SALES_CUST_NO"     , model.get("SALES_CUST_NO"+i));
                modelDt.put("DLV_REQ_DT"     	, model.get("DLV_REQ_DT"+i));
                modelDt.put("PRODUCT"     		, model.get("PRODUCT"+i));
                modelDt.put("QTY"    			, model.get("QTY"+i));
                modelDt.put("BUY_CUST_NM"     	, model.get("BUY_CUST_NM"+i));
                modelDt.put("BUY_PHONE_1"     	, model.get("BUY_PHONE_1"+i));
                modelDt.put("BUY_PHONE_2"     	, model.get("BUY_PHONE_2"+i));
                modelDt.put("SALES_CUST_NM"     , model.get("SALES_CUST_NM"+i));
                modelDt.put("PHONE_1"    		, model.get("PHONE_1"+i));
                modelDt.put("PHONE_2"    		, model.get("PHONE_2"+i));
                modelDt.put("ADDR"     			, model.get("ADDR"+i));
                modelDt.put("CITY"     			, model.get("CITY"+i));
                modelDt.put("ZIP"     			, model.get("ZIP"+i));
                modelDt.put("BUYED_DT"     		, model.get("BUYED_DT"+i));
                modelDt.put("DLV_DT"     		, model.get("DLV_DT"+i));
                modelDt.put("WORK_STAT"     	, model.get("WORK_STAT"+i));
                modelDt.put("CALCEL_MEMO"     	, model.get("CALCEL_MEMO"+i));
                modelDt.put("SERIAL_NO"     	, model.get("SERIAL_NO"+i));
                modelDt.put("REQ_DLV_COM_ID"    , model.get("REQ_DLV_COM_ID"+i));
                modelDt.put("REQ_DRIVER_ID"     , model.get("REQ_DRIVER_ID"+i));
                modelDt.put("GUARANTEE_END_DT"  , model.get("GUARANTEE_END_DT"+i));
                modelDt.put("SOURCE_PRICE"  	, model.get("SOURCE_PRICE"+i));
                modelDt.put("SALES_PRICE"  		, model.get("SALES_PRICE"+i));
                modelDt.put("FEE_CONFIRM_YN"  	, model.get("FEE_CONFIRM_YN"+i));
                modelDt.put("MEMO"     			, model.get("MEMO"+i));
                modelDt.put("PROBLEM"    		, model.get("PROBLEM"+i));
                modelDt.put("ETC1"     			, model.get("ETC1"+i));
                modelDt.put("ETC2"     			, model.get("ETC2"+i));
                modelDt.put("RST_CANCEL_STAT"   , model.get("RST_CANCEL_STAT"+i));
                modelDt.put("ORG_ORD_ID"   		, model.get("ORG_ORD_ID"+i));
                
                modelDt.put("REQ_DLV_JOIN_ID"   , model.get("REQ_DLV_JOIN_ID"+i));
                modelDt.put("SALES_COMPANY_ID"  , model.get("SALES_COMPANY_ID"+i));
                modelDt.put("CHK_WORK_STAT"  	, model.get("CHK_WORK_STAT"+i));
                
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSCT010);
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                    //if("Y".equals(model.get("CHK_CHANGE"+i))){
                    	dao.toUpdateWMSSP010(modelDt);
                    //}
                    
                  //AS출고 건이 취소 일 경우
                    if("05".equals(model.get("CHK_WORK_STAT"+i)) && "999".equals(model.get("WORK_STAT"+i))){
                    	dao.updateWMSAS010_DlvCancel(modelDt);
                    }
                    
                    //IF테이블에 등록되어있는지 체크
                    //등록되어있다면 FLAG UPDATE 처리 해야 얼라이언스 주문 호출 시 가져감
                    String IF_CHECK = (String)dao.ifCheckUpdate(modelDt);
                    if("Y".equals(IF_CHECK)){
                    	modelDt.put("WMS_IF_FLAG", "U");
                    	dao.ifUpdate(modelDt);
                    }
                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : emptyCustRowSave
     * 대체 Method 설명    : 고객관리 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> emptyCustRowSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	System.out.println("111 : " + Integer.parseInt(model.get("selectIds").toString()));
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));
                
                modelDt.put("SALES_CUST_ID"     , model.get("SALES_CUST_ID"+i));
                modelDt.put("SALES_COMPANY_ID"  , model.get("SALES_COMPANY_ID"+i));
                modelDt.put("SALES_COMPANY_NM"  , model.get("SALES_COMPANY_NM"+i));
                
                modelDt.put("LC_ID"       		, model.get(ConstantIF.SS_SVC_NO));
                modelDt.put("REG_NO"     	 	, model.get(ConstantIF.SS_USER_NO));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSCT010_2);
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                	String checkExistData = dao.checkExistEmptyCustRow(modelDt);
    				if (checkExistData != null && !StringUtils.isEmpty(checkExistData)) {
    					modelDt.put("NEW_SALES_COMPANY_ID", checkExistData);
    					
    					dao.emptyCustRowSaveWMSSP010(modelDt);
    					dao.emptyCustRowSave(modelDt);
                    }else{
                    	errCnt++;
                        m.put("errCnt", errCnt);
                        throw new BizException(MessageResolver.getMessage("empty.error"));
                    }
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("empty.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("empty.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }

    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 고객관리 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm3").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * 대체 Method ID   : imgDownList
     * 대체 Method 설명 : 사진다운로드 리스트
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> imgDownList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.imgDownList(model));
        
        return map;
    }
    
    /**
     * Method ID    : getExcelDown2
     * Method 설명      : 엑셀 샘플 다운
     * 작성자                 : chsong
     * @param model
     * @return
     */
    @Override
    public Map<String, Object> getExcelDown2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Map<String, Object> > sample = new ArrayList<Map<String, Object>>();
        int colSize = Integer.parseInt(model.get("Col_Size").toString());
        int rowSize = Integer.parseInt(model.get("Row_Size").toString());
        
        for(int k = 0 ; k < rowSize ; k++){
        	if(k == 0){
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
                    sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
                    sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}else{
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
        			sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
        			sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}
        }
        Map<String, Object> sampleRow = new HashMap<String, Object>();
        sampleRow.put("rowSize", rowSize);
        
        map.put("LIST", getExceList2(sample, sampleRow));
        return map;
    }
        
    private GenericResultSet getExceList2(List<Map<String, Object>> list, Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		int pageIndex = 1;
		int pageSize = 1;
		int pageTotal = 1;
		int pageBlank = (int) Math.ceil(pageTotal / (double) pageSize);
		int rowSize = Integer.parseInt(model.get("rowSize").toString());
		
		List sampleList = new ArrayList<Map<String, Object>>();
		
		for(int k = 0 ; k < rowSize ; k++){
			Map<String, Object> sample = new HashMap<String, Object>();
			for (Map<String, Object> sampleInfo : list) {	
				Object key = sampleInfo.get(k+"_sampleCol");
				if (key != null && StringUtils.isNotEmpty(key.toString())) {
					sample.put(key.toString(), sampleInfo.get(k+"_sampleRow"));
				}
			}
			sampleList.add(sample);
		}
		
		wqrs.setCpage(pageIndex);
		wqrs.setTpage(pageBlank);
		wqrs.setTotCnt(pageTotal);
		wqrs.setList(sampleList);
		return wqrs;
	}

 /**
    * Method ID : saveUploadData
    * Method 설명 : 엑셀업로드 저장
    * 작성자 : kwt
    * @param model
    * @return
    * @throws Exception
    */
   public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       int errCnt = 0;
       int insertCnt = (list != null)?list.size():0;
           try{            	
               dao.saveUploadData(model, list);
               
               m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
               m.put("MSG_ORA", "");
               m.put("errCnt", errCnt);
               
           } catch(Exception e){
               throw e;
           }
       return m;
   }
	   
}
