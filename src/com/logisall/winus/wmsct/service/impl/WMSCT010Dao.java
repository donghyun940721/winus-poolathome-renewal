package com.logisall.winus.wmsct.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCT010Dao")
public class WMSCT010Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : list 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsct010.list", model);
	}
	
	/**
	 * Method ID : update 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmsct010.update", model);
	}
	
	/*-
     * Method ID : checkExistEmptyCustRow
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistEmptyCustRow(Map<String, Object> model) {
        return (String)executeView("wmsct010.checkExistEmptyCustRow", model);
    }
    
	/**
	 * Method ID : update 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object emptyCustRowSave(Map<String, Object> model) {
		return executeUpdate("wmsct010.emptyCustRowSave", model);
	}
	
	/**
	 * Method ID : update 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object emptyCustRowSaveWMSSP010(Map<String, Object> model) {
		return executeUpdate("wmsct010.emptyCustRowSaveWMSSP010", model);
	}
	
	/**
	 * Method ID : toUpdateWMSSP010 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object toUpdateWMSSP010(Map<String, Object> model) {
		return executeUpdate("wmsct010.toUpdateWMSSP010", model);
	}
	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }    
    
    /**
     * Method ID  : selectDrivers
     * Method 설명  : 기사정보 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectDrivers(Map<String, Object> model){
     	return executeQueryForList("wmssp010.selectDrivers", model);
     }
     
     /**
      * Method ID : selectItem
      * Method 설명 : 상품군 리스트 조회
      * 작성자 : 기드온
      * @param model
      * @return
      */
     public Object selectItem(Map<String, Object> model){
         return executeQueryForList("wmsms094.selectItemGrp", model);
     }
     
     /**
 	 * Method ID : imgDownList 
 	 * Method 설명 : 사진다운로드 리스트 
 	 * 작성자 : chsong
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public GenericResultSet imgDownList(Map<String, Object> model) {
 		return executeQueryPageWq("wmsct010.imgDownList", model);
 	}
 	
 	/**
	 * Method ID : updateWMSAS010_DlvCancel 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateWMSAS010_DlvCancel(Map<String, Object> model) {
		return executeUpdate("wmsct010.updateWMSAS010_DlvCancel", model);
	}
	
    /**
	 * Method ID : saveUploadData 
	 * Method 설명 : 엑셀업로드 저장
	 * 작성자 : MonkeySeok
	 * 
	 * @param model
	 * @return
	 */
	public void saveUploadData(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			
			String[] salesCustId  = new String[list.size()];
			String[] salesCustNo  = new String[list.size()];
            String[] sourcePrice = new String[list.size()];
            String[] salesPrice  = new String[list.size()];
            String[] feeConfrimYn  = new String[list.size()];
            
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);

				if ( (paramMap.get("I_SALES_CUST_ID")  != null && StringUtils.isNotEmpty(paramMap.get("I_SALES_CUST_ID").toString()))
				   &&(paramMap.get("I_SALES_CUST_NO") != null && StringUtils.isNotEmpty(paramMap.get("I_SALES_CUST_NO").toString()))
				   &&(paramMap.get("I_SOURCE_PRICE") != null && StringUtils.isNotEmpty(paramMap.get("I_SOURCE_PRICE").toString()))
				   &&(paramMap.get("I_SALES_PRICE")  != null && StringUtils.isNotEmpty(paramMap.get("I_SALES_PRICE").toString())) 
				   &&(paramMap.get("I_FEE_CONFIRM_YN")  != null && StringUtils.isNotEmpty(paramMap.get("I_FEE_CONFIRM_YN").toString()))) {
					
					salesCustId[i]  = (String)paramMap.get("I_SALES_CUST_ID");
					salesCustNo[i]  = (String)paramMap.get("I_SALES_CUST_NO");
					sourcePrice[i] = (String)paramMap.get("I_SOURCE_PRICE");
					salesPrice[i]  = (String)paramMap.get("I_SALES_PRICE");
					feeConfrimYn[i]  = (String)paramMap.get("I_FEE_CONFIRM_YN");
				}
			}
			
			Map<String, Object> modelIns = new HashMap<String, Object>();
            
            modelIns.put("salesCustId" , salesCustId);
            modelIns.put("salesCustNo" , salesCustNo);
            modelIns.put("sourcePrice", sourcePrice);
            modelIns.put("salesPrice" , salesPrice);
            modelIns.put("feeConfrimYn" , feeConfrimYn);

            //session 및 등록정보
            modelIns.put("LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
            
            //dao                
            //modelIns = (Map<String, Object>)dao.autoBestLocSave(modelIns);
            executeUpdate("wmsct010.sp_excel_cust_info", modelIns);
            //ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	/* IF_테이블 등록여부 */
    public String ifCheckUpdate(Map<String, Object> model) {
    	return (String)executeView("wmsct010.ifCheckUpdate", model);
    }
    
    /* IF_테이블 업데이트 */
	public Object ifUpdate(Map<String, Object> model) {
		return executeUpdate("wmsct010.ifUpdate", model);
	}
	
}
