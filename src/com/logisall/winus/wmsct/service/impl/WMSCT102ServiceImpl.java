package com.logisall.winus.wmsct.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsct.service.WMSCT102Service;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCT102Service")
public class WMSCT102ServiceImpl implements WMSCT102Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT102Dao")
    private WMSCT102Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSCS102 = {"REC_DATE", "LC_ID", "CUST_CD"};
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : crossDomainHttps
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	String sUrl = "https://52.79.206.98:5101/invoke/" + (String)model.get("URL");
	        System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        //https ssl
	        disableSslVerification();
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "eaiuser01" + ":" + "eaiuser01";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	
        	int resCode = 0;
        	resCode = con.getResponseCode();
        	System.out.println("resCode: " + resCode);
        	
        	if(resCode < 400){
        		m.put("errCnt", 0);
                m.put("MSG", "처리되었습니다.");
        	}else{
        		//result = con.getResponseMessage();
        		m.put("errCnt", -1);
	            m.put("MSG", "I/F 처리오류. 코드:"+resCode+" (관리자문의)");
        	}
        	
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
	
	/**
     * 
     * 대체 Method ID	: save
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String strGubun = "Y";
        
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO", model.get(ConstantIF.SS_USER_NO));
                
                modelDt.put("selectIds" 	, model.get("selectIds"));

                System.out.println(model.get("ST_GUBUN"+i));
                System.out.println(model.get("REC_TYPE"+i));
                System.out.println(model.get("REC_NAME"+i));
                System.out.println(model.get("REC_DATE"+i));
                System.out.println(model.get("REC_COM_DATE1"+i));
                System.out.println(model.get("CUST_CD"+i));
                System.out.println(model.get("CUST_NM"+i));
                System.out.println(model.get("ETC_CD1"+i));
                System.out.println(model.get("CUST_TYPE"+i));
                System.out.println(model.get("ITEM_CODE"+i));
                System.out.println(model.get("ITEM_NAME"+i));
                System.out.println(model.get("ITEM_SIZE"+i));
                System.out.println(model.get("ITEM_UOM1"+i));
                System.out.println(model.get("ITEM_UOM2"+i));
                System.out.println(model.get("ITEM_QTY"+i));
                System.out.println(model.get("REC_COM_DATE2"+i));
                System.out.println(model.get("ETC1"+i));
                System.out.println(model.get("REC_COM_FLAG"+i));
                System.out.println(model.get("CS_ID"+i));
                System.out.println(model.get("CS_SEQ"+i));
                System.out.println(model.get("LC_ID"+i));
                
                modelDt.put("ST_GUBUN"  	, model.get("ST_GUBUN"+i));
                modelDt.put("REC_TYPE"    	, model.get("REC_TYPE"+i));
                modelDt.put("REC_NAME"    	, model.get("REC_NAME"+i));
                modelDt.put("REC_DATE"    	, model.get("REC_DATE"+i));
                modelDt.put("REC_COM_DATE1"	, model.get("REC_COM_DATE1"+i));
                modelDt.put("CUST_CD"    	, model.get("CUST_CD"+i));
                modelDt.put("CUST_NM"    	, model.get("CUST_NM"+i));
                modelDt.put("ETC_CD1"    	, model.get("ETC_CD1"+i));
                modelDt.put("CUST_TYPE"		, model.get("CUST_TYPE"+i));
                modelDt.put("ITEM_CODE"		, model.get("ITEM_CODE"+i));
                modelDt.put("ITEM_NAME"		, model.get("ITEM_NAME"+i));
                modelDt.put("ITEM_SIZE"		, model.get("ITEM_SIZE"+i));
                modelDt.put("ITEM_UOM1"		, model.get("ITEM_UOM1"+i));
                modelDt.put("ITEM_UOM2"		, model.get("ITEM_UOM2"+i));
                modelDt.put("ITEM_QTY"		, model.get("ITEM_QTY"+i));
                modelDt.put("REC_COM_DATE2"	, model.get("REC_COM_DATE2"+i));
                modelDt.put("ETC1"			, model.get("ETC1"+i));
                modelDt.put("REC_COM_FLAG"	, model.get("REC_COM_FLAG"+i));
                modelDt.put("CS_ID"			, model.get("CS_ID"+i));
                modelDt.put("CS_SEQ"		, model.get("CS_SEQ"+i));
                modelDt.put("LC_ID"			, model.get("LC_ID"+i));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSCS102);
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);                    
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);                    
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            
            //한번이라도 "D"면 삭제
            if (strGubun == "D")
            	m.put("MSG", MessageResolver.getMessage("delete.success"));
            else
            	m.put("MSG", MessageResolver.getMessage("save.success"));            
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
