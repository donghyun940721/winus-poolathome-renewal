package com.logisall.winus.wmsct.service.impl;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsct.service.WMSCT000Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCT000Service")
public class WMSCT000ServiceImpl implements WMSCT000Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT000Dao")
    private WMSCT000Dao dao;

    /**
     * 
     * 대체 Method ID   : custInfoUpdate
     * 대체 Method 설명    : 
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
	@Override
    public Map<String, Object> custInfoUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            Map<String, Object> modelSp = new HashMap<String, Object>();
            modelSp.put("I_SALES_CUST_ID" 	, (String)model.get("I_SALES_CUST_ID"));
            modelSp.put("I_ADDR" 			, (String)model.get("I_ADDR"));
            modelSp.put("I_ZIP" 			, (String)model.get("I_ZIP"));
            modelSp.put("I_BUY_CUST_NM" 	, (String)model.get("I_BUY_CUST_NM"));
            modelSp.put("I_BUY_PHONE_1" 	, (String)model.get("I_BUY_PHONE_1"));
            
            modelSp.put("I_BUY_PHONE_2" 	, (String)model.get("I_BUY_PHONE_2"));
            modelSp.put("I_SALES_CUST_NM"	, (String)model.get("I_SALES_CUST_NM"));
            modelSp.put("I_PHONE_1" 		, (String)model.get("I_PHONE_1"));
            modelSp.put("I_PHONE_2" 		, (String)model.get("I_PHONE_2"));
            
            modelSp.put("I_LC_ID"  			, (String)model.get(ConstantIF.SS_SVC_NO)); 
            modelSp.put("I_USER_NO"			, (String)model.get(ConstantIF.SS_USER_NO));
            modelSp.put("I_WORK_IP"			, (String)model.get(ConstantIF.SS_CLIENT_IP));
            
		    modelSp = (Map<String, Object>)dao.spCustInfoUpdate(modelSp);
    		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelSp.get("O_MSG_CODE")), (String)modelSp.get("O_MSG_NAME"));
		    
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		m.put("MSG_ORA", "");
    		m.put("errCnt", 0);
        } catch(BizException be) {
			m.put("MSG", MessageResolver.getMessage("save.error"));
    		m.put("errCnt", -1);
    		m.put("MSG_ORA", be.getMessage());
        }catch (Exception e) {
    		throw e;
    	}
        return m;
    }
}
