package com.logisall.winus.wmsct.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsct.service.WMSSP060Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;


@Service("WMSSP060Service")
public class WMSSP060ServiceImpl implements WMSSP060Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP060Dao")
    private WMSSP060Dao dao;
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DRIVERS", dao.selectDrivers(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: pickingList1
     * 대체 Method 설명	: 기사피킹리스트
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> pickingList1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        
        model.put("transCustNmArr", transCustNmArr);
        
        if(model.get("vrSrchReqDtFrom4") == null){
            
        }else{
            model.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom4"));
            model.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo4"));
        }
        
        map.put("LIST", dao.pickingList1(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: pickingList2
     * 대체 Method 설명	: 기사피킹리스트
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> pickingList2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        

        if(model.get("vrSrchReqDtFrom4") == null){
            
        }else{
            model.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom4"));
            model.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo4"));
        }
        
        map.put("LIST", dao.pickingList2(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : excelPickingPop1
     * 대체 Method 설명 : 기사피킹리스트 엑셀다운로드1
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> excelPickingPop1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        if(model.get("vrSrchReqDtFrom4") == null){
            
        }else{
            model.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom4"));
            model.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo4"));
        }
        map.put("LIST", dao.pickingList1(model));
        
        return map;
    }
    
    /**
     * 대체 Method ID   : excelPickingPop2
     * 대체 Method 설명 : 기사피킹리스트 엑셀다운로드2
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> excelPickingPop2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        if(model.get("vrSrchReqDtFrom4") == null){
            
        }else{
            model.put("vrSrchReqDtFrom", model.get("vrSrchReqDtFrom4"));
            model.put("vrSrchReqDtTo", model.get("vrSrchReqDtTo4"));
        }
        
        map.put("LIST", dao.pickingList2(model));
        
        return map;
    }
}
