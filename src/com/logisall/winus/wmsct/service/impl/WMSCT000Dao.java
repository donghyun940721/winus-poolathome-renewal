package com.logisall.winus.wmsct.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCT000Dao")
public class WMSCT000Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : spCustInfoInsert 
	 * Method 설명 : 고객발주 등록
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object spCustInfoInsert(Map<String, Object> model){
	    executeUpdate("pk_wmsct000.sp_cust_info_insert", model);
	    return model;
	}
	
    /**
    * Method ID    : spCustInfoUpdate
    * Method 설명  : 고객정보 수정 
    * 작성자       : sing09
    * @param   model
    * @return
    */
   public Object spCustInfoUpdate(Map<String, Object> model){
       executeUpdate("pk_wmsct000.sp_cust_info_update", model);
       return model;
   }

	/**
	 * Method ID : spCustInfoDelete 
	 * Method 설명 : 고객발주 삭제
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
   public Object spCustInfoDelete(Map<String, Object> model){
       executeUpdate("pk_wmsct000.sp_cust_info_delete", model);
       return model;
   }

   /**
	 * Method ID : spDlvCustInfoInsert 
	 * Method 설명 : 배송발주 등록
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object spDlvCustInfoInsert(Map<String, Object> model){
	    executeUpdate("pk_wmsct000.sp_dlv_cust_info_insert", model);
	    return model;
	}
   /**
    * Method ID : spCsCustInfoInsert 
    * Method 설명 : CS발주 등록
    * 작성자 : sing09
    * @param model
    * @return
    */
   public Object spCsCustInfoInsert(Map<String, Object> model){
	   executeUpdate("pk_wmsct000.sp_cs_cust_info_insert", model);
	   return model;
   }
   
   /**
	 * Method ID : spAsCustInfoInsert 
	 * Method 설명 : AS발주 등록
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object spAsCustInfoInsert(Map<String, Object> model){
	    executeUpdate("pk_wmsct000.sp_as_cust_info_insert", model);
	    return model;
	}
}
