package com.logisall.winus.wmsct.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSSP020Dao")
public class WMSSP020Dao extends SqlMapAbstractDAO {
	/**
	 * Method ID : list 
	 * Method 설명 : 고객관리조회 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmssp020.list", model);
	}

	/**
	 * Method ID : insert 
	 * Method 설명 : 고객관리 등록 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmssp010.insert", model);
	}

	/**
	 * Method ID : update 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmssp010.update", model);
	}

	/**
	 * Method ID : rowConfirm 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object rowConfirm(Map<String, Object> model) {
		return executeUpdate("wmssp010.rowConfirm", model);
	}
	
	/**
	 * Method ID : updateWmsct010SerialNo 
	 * Method 설명 : 배송관리 수정 후 고객관리 serial번호 자동 업데이트
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateWmsct010SerialNo(Map<String, Object> model) {
		return executeUpdate("wmssp010.updateWmsct010SerialNo", model);
	}
	
	/**
	 * Method ID : delete 
	 * Method 설명 : 고객관리 삭제 (DEL_YN 를 Y로 수정) 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object delete(Map<String, Object> model) {
		return executeUpdate("wmssp010.delete", model);
	}
	
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }    
    
    /**
	 * Method ID : listQ1
	 * Method 설명 : 배송사
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet listQ1(Map<String, Object> model) {
		return executeQueryPageWq("wmssp010.selectMngCodeDLVCUST", model);
	}
	
	/**
	 * Method ID : listQ2
	 * Method 설명 : 배송기사
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet listQ2(Map<String, Object> model) {
		return executeQueryPageWq("wmssp010.selectMngCodeDRIVERS", model);
	}
	
	/**
    * Method ID  : selectDrivers
    * Method 설명  : 기사정보 데이터셋
    * 작성자             : 기드온
    * @param model
    * @return
    */
    public Object selectDrivers(Map<String, Object> model){
    	return executeQueryForList("wmssp010.selectDrivers", model);
    }
    
    /**
     * Method ID : selectItem
     * Method 설명 : 상품군 리스트 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object selectItem(Map<String, Object> model){
        return executeQueryForList("wmsms094.selectItemGrp", model);
    }
    
    /**
     * Method ID : listT1
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object listT1(Map<String, Object> model){
        return executeQueryForList("wmssp010.listT1", model);
    }
    
    /**
     * Method ID : listT1_10
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object listT1_10(Map<String, Object> model){
    	return executeQueryForList("wmssp010.listT1_10", model);
    }
    
    /**
     * Method ID : wmssp010t2Tmsys900Insert
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object t2FileInsert(Map<String, Object> model) {
        return executeInsert("wmssp010t2.t2FileInsert", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertInfo(Map<String, Object> model) {
        return executeInsert("wmssp010.itemImgInfoSave", model);
    }
    
    /**
     * Method ID  : selectAsncl
     * Method 설명  : 
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectAsncl(Map<String, Object> model){
     	return executeQueryForList("wmssp010.selectAsncl", model);
     }
     
     /**
 	 * Method ID : smsInfoSave 
 	 * Method 설명 :  
 	 * 작성자 : chsong
 	 * 
 	 * @param model
 	 * @return
 	 */
 	public Object smsInfoSave(Map<String, Object> model) {
 		return executeUpdate("wmssp010.smsInfoSave", model);
 	}
 	
 	/**
     * Method ID    : saveLcOrdMove
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveLcOrdMove(Map<String, Object> model){
        executeUpdate("wmssp010.pk_wmsct010.sp_saveOrdMove", model);
        return model;
    }
    
    /**
     * Method ID    : saveDlvLcTran
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveDlvLcTran(Map<String, Object> model){
        executeUpdate("wmssp010.pk_wmsct010.sp_saveDlvLcTran", model);
        return model;
    }
    
    /**
     * Method ID    : dlvFristBadMoveStock
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object dlvFristBadMoveStock(Map<String, Object> model){
        executeUpdate("wmssp010.pk_wmsct010.sp_dlvFristBadMoveStock", model);
        return model;
    }
    
    /**
	 * Method ID : updateWMSAS010_DlvComp 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateWMSAS010_DlvComp(Map<String, Object> model) {
		return executeUpdate("wmssp010.updateWMSAS010_DlvComp", model);
	}
	
	/**
	 * Method ID : updateWMSAS010_DlvCancel 
	 * Method 설명 : 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateWMSAS010_DlvCancel(Map<String, Object> model) {
		return executeUpdate("wmssp010.updateWMSAS010_DlvCancel", model);
	}
	
	/**
     * Method ID  : asCntView
     * Method 설명  : asCntView
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object asCntView(Map<String, Object> model){
        return executeQueryForList("wmssp010.asCntView", model);
    }
    
    /**
	 * Method ID : listQ6
	 * Method 설명 : 배송기사
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet listQ6(Map<String, Object> model) {
		return executeQueryPageWq("wmssp010.selectMngCodeITEMS", model);
	}
	
	/**
     * Method ID    : updateSubUserInfo
     * Method 설명      : 
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object updateSubUserInfo(Map<String, Object> model){
        executeUpdate("wmssp010.pk_wmsct010.sp_updateSubUserInfo", model);
        return model;
    }
    
    /**
	 * Method ID : listQ7
	 * Method 설명 : 시티
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public GenericResultSet listQ7(Map<String, Object> model) {
		return executeQueryPageWq("wmssp010.selectMngCodeCITY7", model);
	}
	
	/**
     * Method ID  : getInOrdByDlv
     * Method 설명  : getInOrdByDlv
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object getInOrdByDlv(Map<String, Object> model){
        return executeQueryForList("wmssp010.getInOrdByDlv", model);
    }
    
    /**
     * Method ID    : saveInOrder
     * Method 설명      : 입고관리 입고주문 등록,수정
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveInOrder(Map<String, Object> model){
        executeUpdate("wmsop020.pk_wmsop020.sp_insert_order", model);
        return model;
    }
    
    /**
	 * Method ID : unDlvList 
	 * Method 설명 : 매배송조회 -7D
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet unDlvList(Map<String, Object> model) {
		return executeQueryPageWq("wmssp010.unDlvList", model);
	}
	
	/**
	 * Method ID : update 
	 * Method 설명 : 고객관리 수정 
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object confPrintEnd(Map<String, Object> model) {
		return executeUpdate("wmssp010.confPrintEnd", model);
	}
	
	/**
	 * Method ID : tslist 
	 * Method 설명 : 배송관리 리스트 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet tsList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmssp010.tsList", model));
		return wqrs;
	}
	/**
	 * Method ID : tslist 
	 * Method 설명 : 배송관리 리스트 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet punctureList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmssp010.punctureList", model));
		return wqrs;
	}
	/**
	 * Method ID : tslist 
	 * Method 설명 : 배송관리 리스트 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet punctureDetailList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmssp010.punctureDetailList", model));
		return wqrs;
	}

		/**
	 * Method ID : aslist 
	 * Method 설명 : as관리 리스트 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet asList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmssp010.asList", model));
		return wqrs;
	}
	
	/**
	 * Method ID : tsdlist 
	 * Method 설명 : 배송관리 상세 리스트 
	 * 작성자 : KHKIM
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet tsdList(Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		wqrs.setList(list("wmssp010.tsdList", model));
		return wqrs;
	}
}
