package com.logisall.winus.wmsct.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsct.service.WMSSP050Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;


@Service("WMSSP050Service")
public class WMSSP050ServiceImpl implements WMSSP050Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP050Dao")
    private WMSSP050Dao dao;

    /**
     * Method ID   : selectBox
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItem(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 고객관리 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 고객관리 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID   : selectBoxT1
     * Method 설명    : 
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBoxT1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ASCSLC", dao.selectAsCsLc(model));
        map.put("DRIVERS", dao.selectDrivers(model));
        map.put("ASITEM", dao.selectAsOrdItem(model));
        
        map.put("ASERR_1", dao.selectAsErr(model));
        map.put("DLVCUST", dao.selectDlvCust(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : listT1
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listT1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listT1(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("vrSalesCustId" 	, model.get("vrSalesCustId"));
            modelDt.put("vrSrchAsCsType" 	, model.get("vrSrchAsCsType"));
            modelDt.put("vrSalesCustNo" 	, model.get("vrSalesCustNo"));
            modelDt.put("vrSrchRitemId" 	, model.get("vrSrchRitemId"));
            modelDt.put("vrSrchAsCsLc" 		, model.get("vrSrchAsCsLc"));
            modelDt.put("vrSrchDrivers" 	, model.get("vrSrchDrivers"));
            modelDt.put("TextReqContents" 	, model.get("TextReqContents"));
            modelDt.put("vrSrchReqMemo" 	, model.get("vrSrchReqMemo"));
            modelDt.put("TextResult" 		, model.get("TextResult"));
            modelDt.put("vrSrchReqCost" 	, model.get("vrSrchReqCost"));
            modelDt.put("vrSrchComCost" 	, model.get("vrSrchComCost"));
            modelDt.put("vrSrchBuyDt" 		, model.get("vrSrchBuyDt"));
            modelDt.put("vrSrchReqDtFrom" 	, model.get("vrSrchReqDtFrom"));
            modelDt.put("vrSrchReqId" 		, model.get("vrSrchReqId"));
            modelDt.put("vrAsCsSeq" 		, model.get("vrAsCsSeq"));
            modelDt.put("vrSrchComType" 	, model.get("vrSrchComType"));
            modelDt.put("vrSrchInOutBound" 	, model.get("vrSrchInOutBound"));
            
            modelDt.put("vrSrchOrdItem" 	, model.get("vrSrchOrdItem"));
            modelDt.put("vrSrchOrdOption" 	, model.get("vrSrchOrdOption"));
            modelDt.put("vrSrchOrdBill" 	, model.get("vrSrchOrdBill"));
            modelDt.put("vrSrchCostType" 	, model.get("vrSrchCostType"));
            modelDt.put("AS_SALES_CUST_ID" 	, model.get("AS_SALES_CUST_ID"));
            modelDt.put("vrSrchDlvCompNm" 	, model.get("vrSrchDlvCompNm"));
            modelDt.put("typicalCustId" 	, model.get("typicalCustId"));
            modelDt.put("vrSrchCancelCode" 	, model.get("vrSrchCancelCode"));
            modelDt.put("vrSrchDlvSetTy" 	, model.get("vrSrchDlvSetTy"));
            modelDt.put("vrConnectUser" 	, model.get("vrConnectUser"));
            modelDt.put("vrCancelReason" 	, model.get("vrCancelReason"));
            modelDt.put("vrWorkType" 		, model.get("vrWorkType"));
		    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
		    modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
            
            if("INSERT".equals(model.get("ST_GUBUN"))){
                dao.insert(modelDt);
                
                if("Y".equals(model.get("vrSrchComType")) || "F".equals(model.get("vrSrchComType"))){
                	dao.toUpdateHcWMSCT010(modelDt);
                	dao.toUpdateHcWMSSP010(modelDt);
                }
            }else if("UPDATE".equals(model.get("ST_GUBUN"))){
                dao.update(modelDt);
                
                if("Y".equals(model.get("vrSrchComType")) || "F".equals(model.get("vrSrchComType"))){
                	dao.toUpdateHcWMSCT010(modelDt);
                	dao.toUpdateHcWMSSP010(modelDt);
                }
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
}
