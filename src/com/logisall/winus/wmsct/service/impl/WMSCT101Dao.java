package com.logisall.winus.wmsct.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCT101Dao")
public class WMSCT101Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsom010.list", model);
    }
    
    /**
     * Method ID  : listDetail
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet listDetail(Map<String, Object> model) {
        return executeQueryPageWq("wmsom010.listDetail", model);
    }
    
    /**
  	 * Method ID : insertCs 
  	 * Method 설명 :
  	 * 작성자 : chsong
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertCs(Map<String, Object> model) {
  		return executeInsert("wmsct101.insertCs", model);
  	}
  	
  	/**
	 * Method ID : updateCs 
	 * Method 설명 :  
	 * 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	public Object updateCs(Map<String, Object> model) {
		return executeUpdate("wmsct101.updateCs", model);
	}
	
  	/**
     * Method ID  : listCsInfo
     * Method 설명   : 
     * 작성자                : 
     * @param   model
     * @return
     */
    public GenericResultSet listCsInfo(Map<String, Object> model) {
        return executeQueryPageWq("wmsct101.listCsInfo", model);
    }

	public List<String> getFaIssueLabel(Map<String, Object> model) {
		return list("wmsct101.faIssueLabel", model);
	}

	public GenericResultSet itemList(Map<String, Object> model) {
		List list = list("wmsct101.itemList", model);
		GenericResultSet genericResultSet = new GenericResultSet();
		genericResultSet.setList(list);
		return genericResultSet;
	}

	public Object recentOrderList(Map<String, Object> model) {
		List list = list("wmsct101.recentOrderList", model);
		GenericResultSet genericResultSet = new GenericResultSet();
		genericResultSet.setList(list);
		return genericResultSet;
	}
}