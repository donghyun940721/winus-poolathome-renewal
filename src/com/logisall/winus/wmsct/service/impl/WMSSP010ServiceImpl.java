package com.logisall.winus.wmsct.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsct.service.WMSSP010Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;


@Service("WMSSP010Service")
public class WMSSP010ServiceImpl implements WMSSP010Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP010Dao")
    private WMSSP010Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSSP010 = {"DLV_ORD_ID", "SALES_CUST_ID"};
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DRIVERS", dao.selectDrivers(model));
        map.put("ITEMGRP", dao.selectItem(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 고객관리 조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        List<String> cityArr = new ArrayList();
        String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
        for (String keyword : spSrchAddrLike ){
            cityArr.add(keyword);
        }
        model.put("cityArr", cityArr);
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 고객관리 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		int insCnt = Integer.parseInt(model.get("selectIds").toString());
    		int arrCnt = 0;
    		String[] lcId        = new String[insCnt];
    		String[] ritemId     = new String[insCnt];
    		String[] custLotNo   = new String[insCnt];
    		String[] workQty     = new String[insCnt];
    		String[] workDesc     = new String[insCnt];
    		
    		String[] IDlvOrdId     	= new String[insCnt];
    		String[] ICustId     	= new String[insCnt];
    		String[] IBfDlvStat  	= new String[insCnt];
    		String[] IDlvReqDt    	= new String[insCnt];
    		String[] IDlvDt     	= new String[insCnt];
    		
    		String   op011		 = "";
    		
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			modelDt.put("vrSrchCustId" 			, model.get("vrSrchCustId"));
    			modelDt.put("selectIds" 			, model.get("selectIds"));
    			modelDt.put("ST_GUBUN"  			, model.get("ST_GUBUN"+i));
    			
    			modelDt.put("DLV_IN_REQ_DT"     	, model.get("DLV_IN_REQ_DT"+i));
    			
    			modelDt.put("DLV_SET_DT"     		, model.get("DLV_SET_DT"+i));
    			modelDt.put("DLV_REQ_DT"     		, model.get("DLV_REQ_DT"+i));
    			modelDt.put("DLV_DT"     			, model.get("DLV_DT"+i));
    			modelDt.put("SERIAL_NO"     		, model.get("SERIAL_NO"+i));
    			modelDt.put("DLV_ORD_STAT"      	, model.get("DLV_ORD_STAT"+i));
    			modelDt.put("DLV_COMP_ID"     		, model.get("DLV_COMP_ID"+i));
    			
    			modelDt.put("REQ_DRIVER_ID"     	, model.get("REQ_DRIVER_ID"+i));
    			modelDt.put("SET_DRIVER_ID"     	, model.get("SET_DRIVER_ID"+i));
    			modelDt.put("DLV_ORD_TYPE"      	, model.get("DLV_ORD_TYPE"+i));
    			modelDt.put("DLV_PHONE_1"     		, model.get("DLV_PHONE_1"+i));
    			modelDt.put("DLV_PHONE_2"     		, model.get("DLV_PHONE_2"+i));
    			
    			modelDt.put("DLV_ADDR"     			, model.get("DLV_ADDR"+i));
    			modelDt.put("DLV_CITY"     			, model.get("DLV_CITY"+i));
    			modelDt.put("ORD_OPTION"     		, model.get("ORD_OPTION"+i));
    			modelDt.put("MEMO"     				, model.get("MEMO"+i));
    			modelDt.put("ETC1"     				, model.get("ETC1"+i));
    			modelDt.put("ETC2"     				, model.get("ETC2"+i));
    			modelDt.put("DLV_SET_TYPE"   		, model.get("DLV_SET_TYPE"+i));
    			modelDt.put("HAPPY_CALL_YN"   		, model.get("HAPPY_CALL_YN"+i));
    			modelDt.put("HAPPY_CALL_MEMO"   	, model.get("HAPPY_CALL_MEMO"+i));
    			modelDt.put("HAPPY_CALL_DT"     	, model.get("HAPPY_CALL_DT"+i));
    			
    			modelDt.put("DLV_ORD_ID"     		, model.get("DLV_ORD_ID"+i));
    			modelDt.put("SALES_CUST_ID"     	, model.get("SALES_CUST_ID"+i));
    			modelDt.put("REQ_DLV_JOIN_ID"   	, model.get("REQ_DLV_JOIN_ID"+i));
    			//modelDt.put("REQ_DLV_JOIN_NM"   	, model.get("REQ_DLV_JOIN_NM"+i));
    			
    			modelDt.put("DLV_PRODUCT"   		, model.get("DLV_PRODUCT"+i));
    			modelDt.put("DLV_PRODUCT_CD"    	, model.get("DLV_PRODUCT_CD"+i));
    			modelDt.put("TEMP_DLV_PRODUCT_NM"	, model.get("TEMP_DLV_PRODUCT_NM"+i));
    			modelDt.put("TRUST_CUST_ID"			, model.get("TRUST_CUST_ID"+i));
    			modelDt.put("LIFTING_WORK_YN"		, model.get("LIFTING_WORK_YN"+i));
    			modelDt.put("ORD_LC_ID"				, model.get("ORD_LC_ID"+i));
    			
    			modelDt.put("UPD_NO"    			, model.get(ConstantIF.SS_USER_NO));
    			modelDt.put("LC_ID"    				, model.get(ConstantIF.SS_SVC_NO));
    			ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSSP010);
    			
    			//OP011(H/D) 존재 여부 체크
    			String OP011_CHECK = (String)dao.ifCheckOP011(modelDt);
    			if("N".equals(OP011_CHECK)){
    			}else{
    				op011 		 = "H/D";
    			}
    			
    			if("INSERT".equals(model.get("ST_GUBUN"+i))){
    			}else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
    				//checkData 배송센터 이관 시 이관대상 센터에 상품이 생성되어있는지 체크.
    				
    				//2022-04-25 주석 
//                	String checkData = dao.checkInsertPossible(modelDt);
//                	if(checkData == null){
    				//AS : 05 일때 패스
//                		if("05".equals(model.get("DLV_ORD_TYPE"+i))){
//                			dao.update(modelDt);
    				//                   		dao.updateWmsct010SerialNo(modelDt); //배송관리 수정 후 고객관리 serial번호 자동 업데이트
    				//               		}else{
    				//               			errCnt++;
    				//               			m.put("errCnt", errCnt);
    				//               			throw new BizException("Exception: 이관 하고자 하는 센터의 상품코드를 확인해주세요. "
    				//               					+"\n이관센터 : "+model.get("REQ_DLV_JOIN_NM"+i)
    				//               					+"\n상품코드 : "+model.get("DLV_PRODUCT_CD"+i)
    				//               					+"\n상품명 : "+model.get("TEMP_DLV_PRODUCT_NM"+i)
    				//              					);
    				//               		}
    				//               	} else {
    				
    				//DLV_ORD_SUBSEQ 공백이거나  null 이면 SP010, CT010 까지 수정. OR  1일 경우에도 동일하게 적용 .. 
    				if("".equals(model.get("DLV_ORD_SUBSEQ"+i)) || model.get("DLV_ORD_SUBSEQ"+i) == null || "1".equals(model.get("DLV_ORD_SUBSEQ"+i))){
    					dao.update(modelDt);
    					dao.updateWmsct010SerialNo(modelDt); //배송관리 수정 후 고객관리 serial번호 자동 업데이트
    					//dao.updateWmsas010(modelDt); //배송관리 수정 후 고객관리 serial번호 자동 업데이트
    				}else if("89".equals(model.get("DLV_ORD_STAT"+i))){
    					//ETC2값만 업데이트. 취소요청 시에만
    					dao.updateWmsct010DlvCancel(modelDt);
    					dao.updateDlvCancel(modelDt);
    				}
    				
    				//초도불량이거나 배송완료일때 따로.. 자동 입,출고 프로시저 파라미터 변수
//                		if("87".equals(model.get("DLV_ORD_STAT"+i)) || "99".equals(model.get("DLV_ORD_STAT"+i))){
//            				IDlvOrdId[arrCnt]   = (String)model.get("DLV_ORD_ID"+i);
//            				ICustId[arrCnt]     = (String)model.get("TRUST_CUST_ID"+i);
//            				IBfDlvStat[arrCnt]  = (String)model.get("BF_DLV_ORD_STAT"+i);
//            				IDlvReqDt[arrCnt]   = (String)model.get("DLV_REQ_DT"+i).toString().replace("-", "");
//            				IDlvDt[arrCnt]     	= (String)model.get("DLV_DT"+i).toString().replace("-", "");
//            				arrCnt++;
//                		}
    				//              	}
    				
    				//배송관리 신규 화면에서 물류센터 수정 일괄입력 시 디테일 데이터도 함께 업데이트
    				if("H/D".equals(op011)){
    					//상품코드 수정
    					modelDt.put("DLV_ORD_SUBSEQ"		, model.get("DLV_ORD_SUBSEQ"+i));
    					dao.update011_ritem(modelDt);
    					dao.update011(modelDt);
    				}
    				
    				//AS출고 건이 배송완료 일 경우
    				if("05".equals(model.get("DLV_ORD_TYPE"+i)) && "99".equals(model.get("DLV_ORD_STAT"+i))){
    					if(!"99".equals(model.get("BF_DLV_ORD_STAT"+i))){
    						System.out.println("AS출고 건이 배송완료 일 경우 updateWMSAS010_DlvComp");
    						dao.updateWMSAS010_DlvComp(modelDt);
    					}
    				}
    				//AS출고 건이 취소요청 일 경우
    				if("05".equals(model.get("DLV_ORD_TYPE"+i)) && "89".equals(model.get("DLV_ORD_STAT"+i))){
    					if(!"89".equals(model.get("BF_DLV_ORD_STAT"+i))){
    						System.out.println("AS출고 건이 취소요청 일 경우 updateWMSAS010_DlvCancel");
    						dao.updateWMSAS010_DlvCancel(modelDt);
    					}
    				}
    				
    				//IF테이블에 등록되어있는지 체크
    				//등록되어있다면 FLAG UPDATE 처리 해야 얼라이언스 주문 호출 시 가져감
//                    String IF_CHECK = (String)dao.ifCheckUpdate(modelDt);
//                    if("Y".equals(IF_CHECK)){
//                    	modelDt.put("WMS_IF_FLAG", "U");
//                    	dao.ifUpdate(modelDt);
//                    }
    				
    			}else{
    				errCnt++;
    				m.put("errCnt", errCnt);
    				throw new BizException(MessageResolver.getMessage("save.error"));
    			}
    		}
    		
    		/* 지산물류센터가 아니면: 지산물류센터 임시 예외처리 */
    		if(!"0000002020".equals((String)model.get(ConstantIF.SS_SVC_NO)) && !"0000001560".equals((String)model.get(ConstantIF.SS_SVC_NO))){
    			
    			if(arrCnt > 0){
    				//프로시져에 보낼것들 다담는다
    				//20220419 주석
//            		Map<String, Object> modelPlIns = new HashMap<String, Object>();
//            		modelPlIns.put("I_DLV_ORD_ID"   , IDlvOrdId);
//            		modelPlIns.put("I_CUST_ID"  	, ICustId);
//            		modelPlIns.put("I_BF_DLV_STAT"  , IBfDlvStat);
//            		modelPlIns.put("I_DLV_REQ_DT"   , IDlvReqDt);
//            		modelPlIns.put("I_DLV_DT"     	, IDlvDt);
//            		
//            		//session 정보
//            		modelPlIns.put("I_LC_ID"        , (String)model.get(ConstantIF.SS_SVC_NO));
//            		modelPlIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
//            		modelPlIns.put("I_USER_NO"  	, (String)model.get(ConstantIF.SS_USER_NO));
//            		modelPlIns = (Map<String, Object>)dao.sp_manual_complete(modelPlIns);
//            		ServiceUtil.isValidReturnCode("WMSIF090", String.valueOf(modelPlIns.get("O_MSG_CODE")), (String)modelPlIns.get("O_MSG_NAME"));
    			}
    		}
    		
    		m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch (BizException be) {
    		if (log.isInfoEnabled()) {
    			log.info(be.getMessage());
    		}
    		m.put("MSG", be.getMessage());
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   : save2
     * 대체 Method 설명    : 권한별 저장 변경
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save2(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		int insCnt = Integer.parseInt(model.get("selectIds").toString());
    		int arrCnt = 0;
    		String   op011		 = "";
    		
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			modelDt.put("vrSrchCustId" 			, model.get("vrSrchCustId"));
    			modelDt.put("selectIds" 			, model.get("selectIds"));
    			modelDt.put("ST_GUBUN"  			, model.get("ST_GUBUN"+i));
    			
    			modelDt.put("DLV_IN_REQ_DT"     	, model.get("DLV_IN_REQ_DT"+i));
    			
    			modelDt.put("DLV_SET_DT"     		, model.get("DLV_SET_DT"+i));
    			modelDt.put("DLV_REQ_DT"     		, model.get("DLV_REQ_DT"+i));
    			modelDt.put("DLV_DT"     			, model.get("DLV_DT"+i));
    			modelDt.put("SERIAL_NO"     		, model.get("SERIAL_NO"+i));
    			modelDt.put("DLV_ORD_STAT"      	, model.get("DLV_ORD_STAT"+i));
    			modelDt.put("DLV_COMP_ID"     		, model.get("DLV_COMP_ID"+i));
    			
    			modelDt.put("REQ_DRIVER_ID"     	, model.get("REQ_DRIVER_ID"+i));
    			modelDt.put("SET_DRIVER_ID"     	, model.get("SET_DRIVER_ID"+i));
    			modelDt.put("DLV_ORD_TYPE"      	, model.get("DLV_ORD_TYPE"+i));
    			modelDt.put("DLV_PHONE_1"     		, model.get("DLV_PHONE_1"+i));
    			modelDt.put("DLV_PHONE_2"     		, model.get("DLV_PHONE_2"+i));
    			
    			modelDt.put("DLV_ZIP"     			, model.get("DLV_ZIP"+i));
    			modelDt.put("DLV_ADDR"     			, model.get("DLV_ADDR"+i));
    			modelDt.put("DLV_CITY"     			, model.get("DLV_CITY"+i));
    			modelDt.put("ORD_OPTION"     		, model.get("ORD_OPTION"+i));
    			modelDt.put("MEMO"     				, model.get("MEMO"+i));
    			modelDt.put("ETC1"     				, model.get("ETC1"+i));
    			modelDt.put("ETC2"     				, model.get("ETC2"+i));
    			modelDt.put("DLV_SET_TYPE"   		, model.get("DLV_SET_TYPE"+i));
    			modelDt.put("HAPPY_CALL_YN"   		, model.get("HAPPY_CALL_YN"+i));
    			modelDt.put("HAPPY_CALL_MEMO"   	, model.get("HAPPY_CALL_MEMO"+i));
    			modelDt.put("HAPPY_CALL_DT"     	, model.get("HAPPY_CALL_DT"+i));
    			
    			modelDt.put("DLV_ORD_ID"     		, model.get("DLV_ORD_ID"+i));
    			modelDt.put("SALES_CUST_ID"     	, model.get("SALES_CUST_ID"+i));
    			modelDt.put("REQ_DLV_JOIN_ID"   	, model.get("REQ_DLV_JOIN_ID"+i));
    			
    			modelDt.put("DLV_PRODUCT"   		, model.get("DLV_PRODUCT"+i));
    			modelDt.put("DLV_PRODUCT_CD"    	, model.get("DLV_PRODUCT_CD"+i));
    			modelDt.put("TEMP_DLV_PRODUCT_NM"	, model.get("TEMP_DLV_PRODUCT_NM"+i));
    			modelDt.put("TRUST_CUST_ID"			, model.get("TRUST_CUST_ID"+i));
    			modelDt.put("LIFTING_WORK_YN"		, model.get("LIFTING_WORK_YN"+i));
    			modelDt.put("ORD_LC_ID"				, model.get("ORD_LC_ID"+i));

    			modelDt.put("DLV_CUSTOMER_NM"		, model.get("DLV_CUSTOMER_NM"+i));
    			modelDt.put("BUY_CUST_NM"			, model.get("BUY_CUST_NM"+i));
    			modelDt.put("BUY_PHONE_1"			, model.get("BUY_PHONE_1"+i));
    			modelDt.put("BUY_PHONE_2"			, model.get("BUY_PHONE_2"+i));
    			modelDt.put("SALES_COMPANY_NM"		, model.get("SALES_COMPANY_NM"+i));
    			modelDt.put("ORG_ORD_ID"			, model.get("ORG_ORD_ID"+i));
    			
                modelDt.put("DLV_COMMENT"			, model.get("DLV_COMMENT"+i));
                modelDt.put("ETC_WORK_MEMO"			, model.get("ETC_WORK_MEMO"+i));
                
    			modelDt.put("UPD_NO"    			, model.get(ConstantIF.SS_USER_NO));
    			modelDt.put("LC_ID"    				, model.get(ConstantIF.SS_SVC_NO));
    			ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSSP010);
    			
    			//OP011(H/D) 존재 여부 체크
    			String OP011_CHECK = (String)dao.ifCheckOP011(modelDt);
    			if("N".equals(OP011_CHECK)){
    			}else{
    				op011 		 = "H/D";
    			}
    			
    			if("INSERT".equals(model.get("ST_GUBUN"+i))){
    			}else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
    				//DLV_ORD_SUBSEQ 공백이거나  null 이면 SP010, CT010 까지 수정. OR  1일 경우에도 동일하게 적용 .. 
    				if("".equals(model.get("DLV_ORD_SUBSEQ"+i)) || model.get("DLV_ORD_SUBSEQ"+i) == null || "1".equals(model.get("DLV_ORD_SUBSEQ"+i))){
    					dao.update2WMSSP010(modelDt);
    					dao.update2WMSCT010(modelDt);
    					dao.update2WMSAS010(modelDt); 
    				}else if("89".equals(model.get("DLV_ORD_STAT"+i))){
    					//ETC2값만 업데이트. 취소요청 시에만
    					dao.updateWmsct010DlvCancel(modelDt);
    					dao.updateDlvCancel(modelDt);
    				}
    				
    				//배송관리 신규 화면에서 물류센터 수정 일괄입력 시 디테일 데이터도 함께 업데이트
    				if("H/D".equals(op011)){
    					//상품코드 수정
    					modelDt.put("DLV_ORD_SUBSEQ"		, model.get("DLV_ORD_SUBSEQ"+i));
    					dao.update011_ritem(modelDt);
    					dao.update011(modelDt);
    				}
    			}else{
    				errCnt++;
    				m.put("errCnt", errCnt);
    				throw new BizException(MessageResolver.getMessage("save.error"));
    			}
    		}
    		m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch (BizException be) {
    		if (log.isInfoEnabled()) {
    			log.info(be.getMessage());
    		}
    		m.put("MSG", be.getMessage());
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveNew
     * 대체 Method 설명    : 배송관리 저장 신규
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveNew(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	int insCnt = Integer.parseInt(model.get("selectIds").toString());
        	int arrCnt = 0;

        	String[] IDlvOrdId     	= new String[insCnt];
        	String[] ICustId     	= new String[insCnt];
        	String[] IBfDlvStat  	= new String[insCnt];
        	String[] IDlvReqDt    	= new String[insCnt];
        	String[] IDlvDt     	= new String[insCnt];
        	
        	String   op011		 = "";
        	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("vrSrchCustId" 			, model.get("vrSrchCustId"));
                modelDt.put("selectIds" 			, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  			, model.get("ST_GUBUN"+i));

                modelDt.put("DLV_IN_REQ_DT"     	, model.get("DLV_IN_REQ_DT"+i));
                
                modelDt.put("DLV_SET_DT"     		, model.get("DLV_SET_DT"+i));
                modelDt.put("DLV_REQ_DT"     		, model.get("DLV_REQ_DT"+i));
                modelDt.put("DLV_DT"     			, model.get("DLV_DT"+i));
                modelDt.put("SERIAL_NO"     		, model.get("SERIAL_NO"+i));
                modelDt.put("DLV_ORD_STAT"      	, model.get("DLV_ORD_STAT"+i));
                modelDt.put("DLV_COMP_ID"     		, model.get("DLV_COMP_ID"+i));

                modelDt.put("REQ_DRIVER_ID"     	, model.get("REQ_DRIVER_ID"+i));
                modelDt.put("SET_DRIVER_ID"     	, model.get("SET_DRIVER_ID"+i));
                modelDt.put("DLV_ORD_TYPE"      	, model.get("DLV_ORD_TYPE"+i));
                modelDt.put("DLV_PHONE_1"     		, model.get("DLV_PHONE_1"+i));
                modelDt.put("DLV_PHONE_2"     		, model.get("DLV_PHONE_2"+i));

                modelDt.put("DLV_ADDR"     			, model.get("DLV_ADDR"+i));
                modelDt.put("DLV_CITY"     			, model.get("DLV_CITY"+i));
                modelDt.put("ORD_OPTION"     		, model.get("ORD_OPTION"+i));
                modelDt.put("MEMO"     				, model.get("MEMO"+i));
                modelDt.put("ETC1"     				, model.get("ETC1"+i));
                modelDt.put("ETC2"     				, model.get("ETC2"+i));
                modelDt.put("DLV_SET_TYPE"   		, model.get("DLV_SET_TYPE"+i));
                modelDt.put("HAPPY_CALL_YN"   		, model.get("HAPPY_CALL_YN"+i));
                modelDt.put("HAPPY_CALL_MEMO"   	, model.get("HAPPY_CALL_MEMO"+i));
                modelDt.put("HAPPY_CALL_DT"     	, model.get("HAPPY_CALL_DT"+i));
                
                modelDt.put("DLV_ORD_ID"     		, model.get("DLV_ORD_ID"+i));
                modelDt.put("SALES_CUST_ID"     	, model.get("SALES_CUST_ID"+i));
                modelDt.put("REQ_DLV_JOIN_ID"   	, model.get("REQ_DLV_JOIN_ID"+i));
                
                modelDt.put("DLV_PRODUCT"   		, model.get("DLV_PRODUCT"+i));
                modelDt.put("DLV_PRODUCT_CD"    	, model.get("DLV_PRODUCT_CD"+i));
                modelDt.put("TEMP_DLV_PRODUCT_NM"	, model.get("TEMP_DLV_PRODUCT_NM"+i));
                modelDt.put("TRUST_CUST_ID"			, model.get("TRUST_CUST_ID"+i));
                modelDt.put("LIFTING_WORK_YN"		, model.get("LIFTING_WORK_YN"+i));
                modelDt.put("ORD_LC_ID"				, model.get("ORD_LC_ID"+i));
                
                modelDt.put("UPD_NO"    			, model.get(ConstantIF.SS_USER_NO));
                modelDt.put("LC_ID"    				, model.get(ConstantIF.SS_SVC_NO));
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSSP010);

                //OP011(H/D) 존재 여부 체크
                String OP011_CHECK = (String)dao.ifCheckOP011(modelDt);
                if("N".equals(OP011_CHECK)){} else{op011 = "H/D";}
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                	//checkData 배송센터 이관 시 이관대상 센터에 상품이 생성되어있는지 체크.
                	String checkData = dao.checkInsertPossible(modelDt);
                	if(checkData == null){
            			errCnt++;
            			m.put("errCnt", errCnt);
            			throw new BizException("Exception: 이관 하고자 하는 센터의 상품정보를 확인해주세요. "
            					+"\n이관센터 : "+model.get("REQ_DLV_JOIN_NM"+i)
            					+"\n상품코드 : "+model.get("DLV_PRODUCT_CD"+i)
            					+"\n상품명 : "+model.get("TEMP_DLV_PRODUCT_NM"+i) );
                	} else {
                		//DLV_ORD_SUBSEQ 공백이거나  null 이면 SP010, CT010 까지 수정. OR  1일 경우에도 동일하게 적용 .. 
	                	if("".equals(model.get("DLV_ORD_SUBSEQ"+i)) || model.get("DLV_ORD_SUBSEQ"+i) == null || "1".equals(model.get("DLV_ORD_SUBSEQ"+i))){
	                		dao.update(modelDt);
	                		dao.updateWmsct010SerialNo(modelDt); 
	                		dao.updateWmsas010(modelDt);
	                	}else if("89".equals(model.get("DLV_ORD_STAT"+i))){
	                		//ETC2값만 업데이트. 취소요청 시에만
	                		dao.updateWmsct010DlvCancel(modelDt);
	                		dao.updateDlvCancel(modelDt);
	                    }
                		
                		//초도불량이거나 배송완료일때 따로.. 자동 입,출고 프로시저 파라미터 변수
                		if("87".equals(model.get("DLV_ORD_STAT"+i)) || "99".equals(model.get("DLV_ORD_STAT"+i))){
            				IDlvOrdId[arrCnt]   = (String)model.get("DLV_ORD_ID"+i);
            				ICustId[arrCnt]     = (String)model.get("TRUST_CUST_ID"+i);
            				IBfDlvStat[arrCnt]  = (String)model.get("BF_DLV_ORD_STAT"+i);
            				IDlvReqDt[arrCnt]   = (String)model.get("DLV_REQ_DT"+i).toString().replace("-", "");
            				IDlvDt[arrCnt]     	= (String)model.get("DLV_DT"+i).toString().replace("-", "");
            				arrCnt++;
                		}
                	}
                	
                    //배송관리 신규 화면에서 물류센터 수정 일괄입력 시 디테일 데이터도 함께 업데이트
                    if("H/D".equals(op011)){
                    	//상품코드 수정
                    	modelDt.put("DLV_ORD_SUBSEQ"		, model.get("DLV_ORD_SUBSEQ"+i));
                    	dao.update011_ritem(modelDt);
                    	dao.update011(modelDt);
                    }
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            /* 지산물류센터가 아니면 하위 프로세스 실행 */
            if(!"0000002020".equals((String)model.get(ConstantIF.SS_SVC_NO)) && !"0000001560".equals((String)model.get(ConstantIF.SS_SVC_NO))){
            	// WEB 배송완료 처리 시 자동출고 프로세스 로직 주석 처리 
            	//            	if(arrCnt > 0){
//            		Map<String, Object> modelPlIns = new HashMap<String, Object>();
//            		modelPlIns.put("I_DLV_ORD_ID"   , IDlvOrdId);
//            		modelPlIns.put("I_CUST_ID"  	, ICustId);
//            		modelPlIns.put("I_BF_DLV_STAT"  , IBfDlvStat);
//            		modelPlIns.put("I_DLV_REQ_DT"   , IDlvReqDt);
//            		modelPlIns.put("I_DLV_DT"     	, IDlvDt);
//            		modelPlIns.put("I_LC_ID"        , (String)model.get(ConstantIF.SS_SVC_NO));
//            		modelPlIns.put("I_WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
//            		modelPlIns.put("I_USER_NO"  	, (String)model.get(ConstantIF.SS_USER_NO));
//            		modelPlIns = (Map<String, Object>)dao.spDlvCompleteWeb(modelPlIns);
//            		ServiceUtil.isValidReturnCode("WMSCT000", String.valueOf(modelPlIns.get("O_MSG_CODE")), (String)modelPlIns.get("O_MSG_NAME"));
//                }
    		}
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : driverReqDtDel
     * 대체 Method 설명    : 기사/예정일 일괄제거
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> driverReqDtDel(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			modelDt.put("DLV_ORD_ID"     		, model.get("DLV_ORD_ID"+i));
				dao.updateDriverReqDt_CT010(modelDt);
				dao.updateDriverReqDt_SP010(modelDt);
    		}
    		
    		m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	}  catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   : rowConfirm
     * 대체 Method 설명    : 고객관리 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> rowConfirm(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("selectIds" 		, model.get("selectIds"));
                modelDt.put("ST_GUBUN"  		, model.get("ST_GUBUN"+i));

                modelDt.put("DLV_ORD_ID"     	, model.get("DLV_ORD_ID"+i));
                modelDt.put("SALES_CUST_ID"     , model.get("SALES_CUST_ID"+i));
                modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
                
                ServiceUtil.checkInputValidation(modelDt, CHECK_VALIDATE_WMSSP010);
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.rowConfirm(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 고객관리 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        List<String> cityArr = new ArrayList();
        String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
        for (String keyword : spSrchAddrLike ){
            cityArr.add(keyword);
        }
        model.put("cityArr", cityArr);
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.list(model));
        
        return map;
    }
    
    /**
     * Method ID : listQ1
     * Method 설명 : 배송사
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listQ1(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : listQ2
     * Method 설명 : 배송기사
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listQ2(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * Method ID : listT1
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listT1(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("IMG")) {
			if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            
			map.put("IMG"   , dao.listT1(model));			
		}
		return map;
	}
    
    /**
     * Method ID : listT1
     * Method 설명 : 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listT1_10(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	String srchKey = (String) model.get("srchKey");
    	if (srchKey.equals("IMG")) {
    		if (model.get("page") == null) {
    			model.put("pageIndex", "1");
    		} else {
    			model.put("pageIndex", model.get("page"));
    		}
    		if (model.get("rows") == null) {
    			model.put("pageSize", "20");
    		} else {
    			model.put("pageSize", model.get("rows"));
    		}
    		
    		map.put("IMG"   , dao.listT1_10(model));			
    	}
    	return map;
    }
    
    /**
	 * Method ID : saveImg
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public Map<String, Object> saveImg(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		try {

			if (!"".equals(model.get("D_ATCH_FILE_NAME"))) { // 파일첨부가 있으면
				Object attachFileInfo = model.get("D_ATCH_FILE_NAME");
				
				for (int i=0; i< ((String[])attachFileInfo).length; i++){
					Map<String, Object> modelDt = new HashMap<String, Object>();
					if(((String[])model.get("D_ATCH_FILE_NAME"))[i].length() > 0){
						// 확장자 잘라내기
						//String str          = (String) model.get("D_ATCH_FILE_NAME");
						//String tranFilename = (String) model.get("TRAN_FILENAME");

						String fileRoute = ConstantIF.FILE_ATTACH_PATH + "ATCH_FILE"+((String[])model.get("D_ATCH_FILE_ROUTE"))[i];
						String fileName  = ((String[])model.get("D_ATCH_FILE_NAME"))[i];
						String fileSize  = ((String[])model.get("FILE_SIZE"))[i];
						String fileNewNm = ((String[])model.get("FILE_NEW_NAME"))[i];
						String workOrdId = (String) model.get("WORK_ORD_ID");
						
						
						String ext    = "." + fileName.substring((fileName.lastIndexOf('.') + 1));
						String fileId = "";
						fileId = workOrdId + "_" + CommonUtil.getLocalDateTime().substring(0, 8) + fileNewNm + ext;
						
						modelDt.put("FILE_ID"		, fileId);
						modelDt.put("ATTACH_GB"		, "INTERFACE"); // 통합 HELPDESK 업로드
						modelDt.put("FILE_VALUE"	, "wmssp010"); // 기존코드 "tmsba230" 로
						modelDt.put("FILE_PATH"		, fileRoute); // 서버저장경로
						modelDt.put("ORG_FILENAME"	, fileNewNm + ext); // 원본파일명
						modelDt.put("FILE_EXT"		, ext); // 파일 확장자
						modelDt.put("FILE_SIZE"		, fileSize);// 파일 사이즈
						modelDt.put("WORK_ORD_ID"	, workOrdId); // 원본파일명
						
						String fileMngNo = (String) dao.t2FileInsert(modelDt);
						dao.insertInfo(model);
					}
				}
				
				// 저장
				//String fileMngNo = (String) dao.t2FileInsert(modelDt);
				// 리턴값 파일 id랑 파일경로
				//model.put("IMAGE_ID", fileId);
				//model.put("MNG_NO", fileMngNo);
				//dao.insertInfo(model);
			}

			map.put("MSG", MessageResolver.getMessage("insert.success"));
		} catch (Exception e) {
			throw e;
		}
		return map;
	}
	
	/**
     * Method ID   : selectBox2
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ASNLC", dao.selectAsncl(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : smsInfoSave
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> smsInfoSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("RTN_CODE"     	, model.get("vrRtnCode"+1));
            modelDt.put("RTN_KEY"     	, model.get("vrRtnKey"+1));
            modelDt.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
            
            if("00".equals(model.get("vrRtnCode"+1))){
                dao.smsInfoSave(modelDt);
            }else if(!"00".equals(model.get("vrRtnCode"+1))){
                dao.smsInfoSave(modelDt);
            }else{
                errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : smsInfoSave2
     * 대체 Method 설명    : 
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> smsInfoSave2(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();   
    		modelDt.put("RTN_CODE"     	, model.get("vrRtnCode"+1));
    		modelDt.put("RTN_KEY"     	, model.get("vrRtnKey"+1));
    		modelDt.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
    		
    		if("00".equals(model.get("vrRtnCode"+1))){
    			dao.smsInfoSave2(modelDt);
    		}else if(!"00".equals(model.get("vrRtnCode"+1))){
    			dao.smsInfoSave2(modelDt);
    		}else{
    			errCnt++;
    			m.put("errCnt", errCnt);
    			throw new BizException(MessageResolver.getMessage("save.error"));
    		}
    		
    		m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch (BizException be) {
    		if (log.isInfoEnabled()) {
    			log.info(be.getMessage());
    		}
    		m.put("MSG", be.getMessage());
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   : smsInfoSave3
     * 대체 Method 설명    : 배송조회 서비스 SMS 발송 여부
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> smsInfoSave3(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	int errCnt = 0;
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();   
    		modelDt.put("RTN_CODE"     	, model.get("vrRtnCode"+1));
    		modelDt.put("RTN_KEY"     	, model.get("vrRtnKey"+1));
    		modelDt.put("UPD_NO"    	, model.get(ConstantIF.SS_USER_NO));
    		
    		if("00".equals(model.get("vrRtnCode"+1))){
    			dao.smsInfoSave3(modelDt);
    		}else if(!"00".equals(model.get("vrRtnCode"+1))){
    			dao.smsInfoSave3(modelDt);
    		}else{
    			errCnt++;
    			m.put("errCnt", errCnt);
    			throw new BizException(MessageResolver.getMessage("save.error"));
    		}
    		
    		m.put("errCnt", errCnt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch (BizException be) {
    		if (log.isInfoEnabled()) {
    			log.info(be.getMessage());
    		}
    		m.put("MSG", be.getMessage());
    		
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   : smsInfoSave4
     * 대체 Method 설명    : 배송조회 서비스 발송 여부 다중건
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> smsInfoSave4(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		int errCnt = 0;
    		
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			modelDt.put("RTN_CODE"		,model.get("vrRtnCode" + i));
    			modelDt.put("RTN_KEY"		,model.get("vrRtnKey" + i));
        		modelDt.put("UPD_NO"    	,model.get(ConstantIF.SS_USER_NO));
				dao.smsInfoSave3(modelDt);
    		}
    		
    		m.put("errCnt", errCnt);
    		m.put("MSG_ORA", "");
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    		
    	} catch(Exception e){
    		m.put("MSG_ORA", e.getMessage());
    		m.put("MSG", MessageResolver.getMessage("save.error"));
    		m.put("errCnt", "1");
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   : smsSaveConfUpdate
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> smsSaveConfUpdate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();        
            String workOrdId = (String) model.get("WORK_ORD_ID");
            modelDt.put("WORK_ORD_ID"	, workOrdId);
            dao.insertInfo(model);
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : saveLcOrdMove
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveLcOrdMove(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
        	int insCnt = Integer.parseInt(model.get("selectIds").toString());
        	if(insCnt > 0){
        		//저장, 수정         
        		String[] stGubun        = new String[insCnt];
        		String[] dlvSetDt       = new String[insCnt];
        		String[] dlvReqDt       = new String[insCnt];
        		String[] dlvDt          = new String[insCnt];
        		String[] serialNo       = new String[insCnt];
        		String[] dlvOrdStat     = new String[insCnt];
        		String[] dlvCompId      = new String[insCnt];
        		String[] reqDriverId    = new String[insCnt];
        		String[] setDriverId    = new String[insCnt];
        		String[] dlvOrdType     = new String[insCnt];
        		String[] dlvPhone1      = new String[insCnt];
        		String[] dlvPhone2      = new String[insCnt];
        		String[] dlvAddr        = new String[insCnt];
        		String[] dlvCity        = new String[insCnt];
        		String[] ordOption      = new String[insCnt];
        		String[] memo           = new String[insCnt];
        		String[] etc1           = new String[insCnt];
        		String[] etc2           = new String[insCnt];
        		String[] happyCallYn    = new String[insCnt];
        		String[] happyCallMemo  = new String[insCnt];
        		String[] dlvOrdId       = new String[insCnt];
        		String[] salesCustId    = new String[insCnt];
        		String[] reqDlvJoinId   = new String[insCnt];  

        		for(int i = 0 ; i < insCnt ; i ++){        
        			stGubun[i]       = (String)model.get("ST_GUBUN"+i);
        			dlvSetDt[i]      = (String)model.get("DLV_SET_DT"+i);
        			dlvReqDt[i]      = (String)model.get("DLV_REQ_DT"+i);
        			dlvDt[i]         = (String)model.get("DLV_DT"+i);
        			serialNo[i]      = (String)model.get("SERIAL_NO"+i);
        			dlvOrdStat[i]    = (String)model.get("DLV_ORD_STAT"+i);
        			dlvCompId[i]     = (String)model.get("DLV_COMP_ID"+i);
        			reqDriverId[i]   = (String)model.get("REQ_DRIVER_ID"+i);
        			setDriverId[i]   = (String)model.get("SET_DRIVER_ID"+i);
        			dlvOrdType[i]    = (String)model.get("DLV_ORD_TYPE"+i);
        			dlvPhone1[i]     = (String)model.get("DLV_PHONE_1"+i);
        			dlvPhone2[i]     = (String)model.get("DLV_PHONE_2"+i);
        			dlvAddr[i]       = (String)model.get("DLV_ADDR"+i);
        			dlvCity[i]       = (String)model.get("DLV_CITY"+i);
        			ordOption[i]     = (String)model.get("ORD_OPTION"+i);
        			memo[i]          = (String)model.get("MEMO"+i);
        			etc1[i]          = (String)model.get("ETC1"+i);
        			etc2[i]          = (String)model.get("ETC2"+i);
        			happyCallYn[i]   = (String)model.get("HAPPY_CALL_YN"+i);
        			happyCallMemo[i] = (String)model.get("HAPPY_CALL_MEMO"+i);
        			dlvOrdId[i]      = (String)model.get("DLV_ORD_ID"+i);
        			salesCustId[i]   = (String)model.get("SALES_CUST_ID"+i);
        			reqDlvJoinId[i]  = (String)model.get("REQ_DLV_JOIN_ID"+i);
        		}
        		   
        		//프로시져에 보낼것들 다담는다
        		Map<String, Object> modelIns = new HashMap<String, Object>();
        		modelIns.put("stGubun"        , stGubun);
        		modelIns.put("dlvSetDt"       , dlvSetDt);
        		modelIns.put("dlvReqDt"       , dlvReqDt);
        		modelIns.put("dlvDt"          , dlvDt);
        		modelIns.put("serialNo"       , serialNo);
        		modelIns.put("dlvOrdStat"     , dlvOrdStat);
        		modelIns.put("dlvCompId"      , dlvCompId);
        		modelIns.put("reqDriverId"    , reqDriverId);
        		modelIns.put("setDriverId"    , setDriverId);
        		modelIns.put("dlvOrdType"     , dlvOrdType);
        		modelIns.put("dlvPhone1"      , dlvPhone1);
        		modelIns.put("dlvPhone2"      , dlvPhone2);
        		modelIns.put("dlvAddr"        , dlvAddr);
        		modelIns.put("dlvCity"        , dlvCity);
        		modelIns.put("ordOption"      , ordOption);
        		modelIns.put("memo"           , memo);
        		modelIns.put("etc1"           , etc1);
        		modelIns.put("etc2"           , etc2);
        		modelIns.put("happyCallYn"    , happyCallYn);
        		modelIns.put("happyCallMemo"  , happyCallMemo);
        		modelIns.put("dlvOrdId"       , dlvOrdId);
        		modelIns.put("salesCustId"    , salesCustId);
        		modelIns.put("reqDlvJoinId"   , reqDlvJoinId);

        		//session 정보
        		modelIns.put("LC_ID"  , (String)model.get(ConstantIF.SS_SVC_NO));
        		modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
        		modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
        		modelIns = (Map<String, Object>)dao.saveLcOrdMove(modelIns);
        	    ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
        	}

        	m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
  
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * 
     * 대체 Method ID   : saveDlvLcTran
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveDlvLcTran(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
    		Map<String, Object> modelIns = new HashMap<String, Object>();

    		//session 정보
    		modelIns.put("LC_ID"  		, (String)model.get(ConstantIF.SS_SVC_NO));
    		modelIns.put("WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
    		modelIns.put("USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
    		modelIns.put("TRUST_CUST_ID", (String)model.get("vrSrchCustId"));
    		
    		if("N/A".equals((String)model.get("tranDlvOrdType"))){
	    		modelIns = (Map<String, Object>)dao.saveDlvLcTran(modelIns);
	    	    ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    		}else{
    			modelIns.put("I_DLV_ORD_TYPE", (String)model.get("tranDlvOrdType"));
	    		modelIns = (Map<String, Object>)dao.saveDlvLcDlvasTran(modelIns);
	    	    ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
    		}

        	m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
	        m.put("rstMsg", (String)modelIns.get("O_MSG_NAME"));
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            m.put("rstMsg", 1);
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    public Map<String, Object> asCntView(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("ASCNTVIEW")) {
			map.put("ASCNTVIEW", dao.asCntView(model));
		}else if (srchKey.equals("CSCNTVIEW")) {
			map.put("CSCNTVIEW", dao.csCntView(model));
		}
		return map;
	}
    
    /**
     * Method ID : listQ6
     * Method 설명 : 배송기사
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ6(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listQ6(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : updateSubUserInfo
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateSubUserInfo(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{
    		Map<String, Object> modelIns = new HashMap<String, Object>();

    		//session 정보
    		//modelIns.put("LC_ID"  		, (String)model.get(ConstantIF.SS_SVC_NO));
    		//modelIns.put("WORK_IP"		, (String)model.get(ConstantIF.SS_CLIENT_IP));
    		//modelIns.put("USER_NO"		, (String)model.get(ConstantIF.SS_USER_NO));
    		modelIns.put("I_USER_ID"		, (String)model.get("vrUserId"));
    		modelIns.put("I_USER_SUB_ID"	, (String)model.get("vrUserSubId"));
    		modelIns.put("I_USER_SUB_PW"	, (String)model.get("vrUserSubPw"));
    		
    		System.out.println((String)model.get("vrUserId") + " // " + (String)model.get("vrUserSubId") + " // " + (String)model.get("vrUserSubPw"));
    		modelIns = (Map<String, Object>)dao.updateSubUserInfo(modelIns);
    	    ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

        	m.put("errCnt", 0);
	        m.put("MSG", MessageResolver.getMessage("save.success"));
	        m.put("rstMsg", (String)modelIns.get("O_MSG_NAME"));
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            m.put("rstMsg", 1);
        } catch(Exception e){
            throw e;
        }
        return m;
    } 
    
    /**
     * Method ID : listQ7
     * Method 설명 : 시티
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listQ7(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.listQ7(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }
    
    /*-
	 * Method ID   : getTypicalCust
	 * Method 설명 : 물류센터 별 대표화주값 조회
	 * 작성자      : 기드온
	 * @param 
	 * @return
	 */
    public Map<String, Object> getInOrdByDlv(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		String srchKey = (String) model.get("srchKey");
		if (srchKey.equals("INORD")) {
			List<String> salesCustIdArr = new ArrayList();
	        String[] spSrchSalesCustId = model.get("vrSalesCustIdval").toString().split(",");
	        for (String keyword : spSrchSalesCustId ){
	        	salesCustIdArr.add(keyword);
	        }
	        model.put("salesCustIdArr", salesCustIdArr);
	        
			map.put("INORD", dao.getInOrdByDlv(model));
		}
		return map;
	}
    
    /**
     * 분할전
     * 대체 Method ID   : saveInOrder
     * 대체 Method 설명    : 입고주문(저장,수정,삭제)
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
            	
	            for(int i = 0 ; i < insCnt ; i ++){
	                //저장, 수정
	                String[] dsSubRowStatus = new String[insCnt];                
	                String[] ordSeq         = new String[insCnt];         
	                String[] ritemId        = new String[insCnt];     
	                String[] custLotNo      = new String[insCnt];     
	                String[] realInQty      = new String[insCnt];     
	                
	                String[] realOutQty     = new String[insCnt];                     
	                String[] makeDt         = new String[insCnt];         
	                String[] timePeriodDay  = new String[insCnt];     
	                String[] locYn          = new String[insCnt];     
	                String[] pdaCd          = new String[insCnt];     
	                
	                String[] workYn         = new String[insCnt];                
	                String[] rjType         = new String[insCnt];         
	                String[] realPltQty     = new String[insCnt];     
	                String[] realBoxQty     = new String[insCnt];     
	                String[] confYn         = new String[insCnt];     
	                
	                String[] unitAmt        = new String[insCnt];                
	                String[] amt            = new String[insCnt];         
	                String[] eaCapa         = new String[insCnt];     
	                String[] boxBarcode     = new String[insCnt];     
	                String[] inOrdUomId     = new String[insCnt];     
	                
	                String[] inWorkUomId = new String[insCnt]; 
	                String[] outOrdUomId    = new String[insCnt];       
	                String[] outWorkUomId= new String[insCnt]; 
	                String[] inOrdQty       = new String[insCnt]; 
	                String[] inWorkOrdQty   = new String[insCnt]; 
	                
	                String[] outOrdQty      = new String[insCnt];     
	                String[] outWorkOrdQty  = new String[insCnt]; 
	                String[] refSubLotId    = new String[insCnt];     
	                String[] dspId          = new String[insCnt];     
	                String[] carId          = new String[insCnt];                
	                
	                String[] cntrId         = new String[insCnt];         
	                String[] cntrNo         = new String[insCnt];     
	                String[] cntrType       = new String[insCnt];     
	                String[] badQty         = new String[insCnt];     
	                String[] uomNm          = new String[insCnt];                
	                
	                String[] unitPrice      = new String[insCnt];         
	                String[] whNm           = new String[insCnt];     
	                String[] itemKorNm      = new String[insCnt];     
	                String[] itemEngNm      = new String[insCnt];     
	                String[] repUomId       = new String[insCnt];                
	                
	                String[] uomCd          = new String[insCnt];         
	                String[] uomId          = new String[insCnt];     
	                String[] repUomCd       = new String[insCnt];     
	                String[] repuomNm       = new String[insCnt];     
	                String[] itemGrpId      = new String[insCnt];                
	                
	                String[] expiryDate     = new String[insCnt];         
	                String[] inOrdWeight    = new String[insCnt];                 
	                String[] unitNo         = new String[insCnt];
	                String[] ordDesc        = new String[insCnt];
	                String[] validDt        = new String[insCnt];
	                
	                String[] etc2           = new String[insCnt];
	                
	                //추가
	                String[] itemBestDate     = new String[insCnt];   //상품유효기간     
	                String[] itemBestDateEnd  = new String[insCnt];   //상품유효기간만료일
	                String[] rtiNm            = new String[insCnt];   //물류기기명
	                
	                
	                dsSubRowStatus[0]  = (String)model.get("I_ST_GUBUN"+i);               
	                ordSeq[0]           = (String)model.get("I_ORD_SEQ"+i);          
	                ritemId[0]          = (String)model.get("I_RITEM_ID"+i);      
	                custLotNo[0]        = (String)model.get("I_CUST_LOT_NO"+i);      
	                realInQty[0]        = (String)model.get("I_REAL_IN_QTY"+i);      
	                
	                realOutQty[0]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
	                makeDt[0]           = (String)model.get("I_MAKE_DT"+i);          
	                timePeriodDay[0]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
	                locYn[0]            = (String)model.get("I_LOC_YN"+i);      
	                pdaCd[0]            = (String)model.get("I_PDA_CD"+i);      
	                
	                workYn[0]           = (String)model.get("I_WORK_YN"+i);                 
	                rjType[0]           = (String)model.get("I_RJ_TYPE"+i);          
	                realPltQty[0]       = (String)model.get("I_REAL_PLT_QTY"+i);      
	                realBoxQty[0]       = (String)model.get("I_REAL_BOX_QTY"+i);      
	                confYn[0]           = (String)model.get("I_CONF_YN"+i);      
	                
	                unitAmt[0]          = (String)model.get("I_UNIT_AMT"+i);                 
	                amt[0]              = (String)model.get("I_AMT"+i);          
	                eaCapa[0]           = (String)model.get("I_EA_CAPA"+i);      
	                boxBarcode[0]       = (String)model.get("I_BOX_BARCODE"+i);      
	                inOrdUomId[0]       = (String)model.get("I_IN_ORD_UOM_ID"+i);      
	                inWorkUomId[0]       = (String)model.get("I_IN_WORK_UOM_ID"+i);
	                outOrdUomId[0]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);                 
	                outWorkUomId[0]      = (String)model.get("I_OUT_WORK_UOM_ID"+i);
	                inOrdQty[0]         = (String)model.get("I_IN_ORD_QTY"+i);          
	                inWorkOrdQty[0]         = (String)model.get("I_IN_WORK_ORD_QTY"+i);
	                outOrdQty[0]        = (String)model.get("I_OUT_ORD_QTY"+i);     
	                outWorkOrdQty[0]        = (String)model.get("I_OUT_WORK_ORD_QTY"+i); 
	                refSubLotId[0]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
	                dspId[0]            = (String)model.get("I_DSP_ID"+i);      
	                
	                carId[0]            = (String)model.get("I_CAR_ID"+i);                 
	                cntrId[0]           = (String)model.get("I_CNTR_ID"+i);          
	                cntrNo[0]           = (String)model.get("I_CNTR_NO"+i);      
	                cntrType[0]         = (String)model.get("I_CNTR_TYPE"+i);      
	                badQty[0]           = (String)model.get("I_BAD_QTY"+i);      
	                
	                uomNm[0]            = (String)model.get("I_UOM_NM"+i);                 
	                unitPrice[0]        = (String)model.get("I_UNIT_PRICE"+i);          
	                whNm[0]             = (String)model.get("I_WH_NM"+i);      
	                itemKorNm[0]        = (String)model.get("I_ITEM_KOR_NM"+i);      
	                itemEngNm[0]        = (String)model.get("I_ITEM_ENG_NM"+i);      
	                
	                repUomId[0]         = (String)model.get("I_REP_UOM_ID"+i);                 
	                uomCd[0]            = (String)model.get("I_UOM_CD"+i);          
	                uomId[0]            = (String)model.get("I_UOM_ID"+i);      
	                repUomCd[0]         = (String)model.get("I_REP_UOM_CD"+i);      
	                repuomNm[0]         = (String)model.get("I_REP_UOM_NM"+i);      
	                
	                itemGrpId[0]        = (String)model.get("I_ITEM_GRP_ID"+i);                 
	                expiryDate[0]       = (String)model.get("I_EXPIRY_DATE"+i);          
	                inOrdWeight[0]      = (String)model.get("I_IN_ORD_WEIGHT"+i);      
	                unitNo[0]           = (String)model.get("I_UNIT_NO"+i);      
	                ordDesc[0]          = (String)model.get("I_ORD_DESC"+i);
	                
	                validDt[0]          = (String)model.get("I_VALID_DT"+i);      
	                
	                etc2[0]             = (String)model.get("I_ETC2"+i); 
	                
	                //추가 
	                itemBestDate[0]       = (String)model.get("I_ITEM_BEST_DATE"+i);      
	                itemBestDateEnd[0]    = (String)model.get("I_ITEM_BEST_DATE_END"+i);   
	                rtiNm[0]              = (String)model.get("I_RTI_NM"+i);
	            
	                //프로시져에 보낼것들 다담는다
	                Map<String, Object> modelIns = new HashMap<String, Object>();
	                
	                //main
	                modelIns.put("dsMain_rowStatus" , model.get("dsMain_rowStatus").toString());
	                modelIns.put("vrOrdId"          , model.get("vrOrdId"));
	                modelIns.put("inReqDt"          , model.get("calInDt").toString().replace("-", ""));  //날짜이니까 아마 - replace 해야할텐데
	                modelIns.put("inDt"             , model.get("inDt"));
	                modelIns.put("custPoid"         , model.get("custPoid"));
	                
	                modelIns.put("custPoseq"        , model.get("custPoseq"));
	                modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));
	                modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
	                modelIns.put("vrWhId"           , model.get("vrSrchWhId"));
	                modelIns.put("outWhId"          , model.get("outWhId"));
	                
	                modelIns.put("transCustId"      , model.get("transCustId"));
	                modelIns.put("vrCustId"         , model.get("vrSrchCustId"));
	                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));
	                modelIns.put("blNo"             , model.get("vrBlNo"));
	                modelIns.put("workStat"         , model.get("workStat"));
	                
	                modelIns.put("ordType"          , model.get("ordType"));
	                modelIns.put("ordSubtype"       , model.get("vrSrchOrderPhase"));
	                modelIns.put("outReqDt"         , model.get("outReqDt"));
	                modelIns.put("outDt"            , model.get("outDt"));
	                modelIns.put("pdaStat"          , model.get("pdaStat"));
	                
	                modelIns.put("workSeq"          , model.get("workSeq"));
	                modelIns.put("capaTot"          , model.get("capaTot"));
	                modelIns.put("kinOutYn"         , model.get("kinOutYn"));
	                modelIns.put("carConfYn"        , model.get("carConfYn"));
	                modelIns.put("gvLcId"           , (String)model.get(ConstantIF.SS_SVC_NO));
	                
	                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
	                modelIns.put("approveYn"        , model.get("vrApproveYn"));
	                modelIns.put("payYn"            , model.get("payYn"));
	                modelIns.put("inCustId"         , model.get("vrInCustId"));
	                modelIns.put("inCustAddr"       , model.get("vrInCustAddr"));
	                
	                modelIns.put("inCustEmpNm"      , model.get("vrInCustEmpNm"));
	                modelIns.put("inCustTel"        , model.get("vrInCustTel"));
	                
	                //sub
	                modelIns.put("dsSub_rowStatus"  , dsSubRowStatus);
	                modelIns.put("ordSeq"           , ordSeq);
	                modelIns.put("ritemId"          , ritemId);
	                modelIns.put("custLotNo"        , custLotNo);
	                modelIns.put("realInQty"        , realInQty);
	                
	                modelIns.put("realOutQty"       , realOutQty);
	                modelIns.put("makeDt"           , makeDt);
	                modelIns.put("timePeriodDay"    , timePeriodDay);
	                modelIns.put("locYn"            , locYn);
	                modelIns.put("pdaCd"            , pdaCd);
	                
	                modelIns.put("workYn"           , workYn);
	                modelIns.put("rjType"           , rjType);
	                modelIns.put("realPltQty"       , realPltQty);
	                modelIns.put("realBoxQty"       , realBoxQty);
	                modelIns.put("confYn"           , confYn);
	                
	                modelIns.put("unitAmt"          , unitAmt);
	                modelIns.put("amt"              , amt);
	                modelIns.put("eaCapa"           , eaCapa);
	                modelIns.put("boxBarcode"       , boxBarcode);
	                modelIns.put("inOrdUomId"       , inOrdUomId);
	                modelIns.put("inWorkUomId"   , inWorkUomId);
	                
	                modelIns.put("outOrdUomId"      , outOrdUomId);
	                modelIns.put("outWorkUomId"  , outWorkUomId);
	                modelIns.put("inOrdQty"         , inOrdQty);
	                modelIns.put("inWorkOrdQty"     , inWorkOrdQty);
	                modelIns.put("outOrdQty"        , outOrdQty);
	                
	                modelIns.put("outWorkOrdQty"    , outWorkOrdQty);
	                modelIns.put("refSubLotId"      , refSubLotId);
	                modelIns.put("dspId"            , dspId);                
	                modelIns.put("carId"            , carId);
	                modelIns.put("cntrId"           , cntrId);
	                
	                modelIns.put("cntrNo"           , cntrNo);
	                modelIns.put("cntrType"         , cntrType);
	                modelIns.put("badQty"           , badQty);
	                modelIns.put("uomNm"            , uomNm);
	                modelIns.put("unitPrice"        , unitPrice);
	                
	                modelIns.put("whNm"             , whNm);
	                modelIns.put("itemKorNm"        , itemKorNm);
	                modelIns.put("itemEngNm"        , itemEngNm);
	                modelIns.put("repUomId"         , repUomId);
	                modelIns.put("uomCd"            , uomCd);
	                
	                modelIns.put("uomId"            , uomId);
	                modelIns.put("repUomCd"         , repUomCd);
	                modelIns.put("repuomNm"         , repuomNm);
	                modelIns.put("itemGrpId"        , itemGrpId);
	                modelIns.put("expiryDate"       , expiryDate);
	                
	                modelIns.put("inOrdWeight"      , inOrdWeight); 
	                modelIns.put("unitNo"           , unitNo);
	                modelIns.put("ordDesc"          , ordDesc);
	                modelIns.put("validDt"          , validDt);                
	                modelIns.put("etc2"             , etc2);
	                
	                //추가된거(아직프로시져는 안탐)
	                modelIns.put("itemBestDate"     , itemBestDate);
	                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);               
	                modelIns.put("rtiNm"            , rtiNm);
	                
	                
	                //session 정보
	                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
	                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
	
	                //dao                
	                modelIns = (Map<String, Object>)dao.saveInOrder(modelIns);
	                ServiceUtil.isValidReturnCode("WMSSP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));  
	            }
                              

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }
            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID		: unDlvList
     * 대체 Method 설명		: 매배송조회 -7D
     * 작성자				: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> unDlvList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.unDlvList(model));
        return map;
    }
    
    /**
     * 대체 Method ID   : listExcelUnDlvList
     * 대체 Method 설명 : 매배송조회 -7D 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcelUnDlvList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.unDlvList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : confPrintEnd
     * 대체 Method 설명    : 인수증 출력 확정
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> confPrintEnd(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("DLV_ORD_ID", model.get("DLV_ORD_ID"+i));
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.confPrintEnd(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", "인수증 출력확인(확정) 되었습니다.");
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : tsList
     * 대체 Method 설명    : 배송관리 조회
     * 작성자                      : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> tsList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
       
        List<String> cityArr = new ArrayList();
        String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
        for (String keyword : spSrchAddrLike ){
            cityArr.add(keyword);
        }
        model.put("cityArr", cityArr);
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.tsList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : HDlist
     * 대체 Method 설명    : H/D 배송관리 조회
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> hdList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> transCustNmArr = new ArrayList();
    	String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
    	for (String keyword : spVrSrchTransCustNmLike ){
    		transCustNmArr.add(keyword);
    	}
    	model.put("transCustNmArr", transCustNmArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.hdList(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : hd016List
     * 대체 Method 설명    : H/D 배송관리 조회 화주용
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> hd016List(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> transCustNmArr = new ArrayList();
    	String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
    	for (String keyword : spVrSrchTransCustNmLike ){
    		transCustNmArr.add(keyword);
    	}
    	model.put("transCustNmArr", transCustNmArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.hd016List(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : hdDetailList
     * 대체 Method 설명    : H/D 배송관리 조회 디테일
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> hdDetailList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> transCustNmArr = new ArrayList();
    	String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
    	for (String keyword : spVrSrchTransCustNmLike ){
    		transCustNmArr.add(keyword);
    	}
    	model.put("transCustNmArr", transCustNmArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.hdDetailList(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : hdDetail016List
     * 대체 Method 설명    : H/D 배송관리 조회 디테일 화주용
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> hdDetail016List(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> transCustNmArr = new ArrayList();
    	String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
    	for (String keyword : spVrSrchTransCustNmLike ){
    		transCustNmArr.add(keyword);
    	}
    	model.put("transCustNmArr", transCustNmArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.hdDetail016List(model));
    	return map;
    }

    /**
     * 
     * 대체 Method ID   : tsList
     * 대체 Method 설명    : 배송관리 조회
     * 작성자                      : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> punctureList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> transCustNmArr = new ArrayList();
    	String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
    	for (String keyword : spVrSrchTransCustNmLike ){
    		transCustNmArr.add(keyword);
    	}
    	model.put("transCustNmArr", transCustNmArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.punctureList(model));
    	return map;
    }
    /**
     * 
     * 대체 Method ID   : tsList
     * 대체 Method 설명    : 배송관리 조회
     * 작성자                      : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> punctureDetailList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("LIST", dao.punctureDetailList(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : asList015
     * 대체 Method 설명    : asList015 AS관리 조회
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> asList015(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
       
        List<String> cityArr = new ArrayList();
        String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
        for (String keyword : spSrchAddrLike ){
            cityArr.add(keyword);
        }
        model.put("cityArr", cityArr);
        
        List<String> transCustNmArr = new ArrayList();
        String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
        for (String keyword : spVrSrchTransCustNmLike ){
        	transCustNmArr.add(keyword);
        }
        model.put("transCustNmArr", transCustNmArr);
        
        List<String> vrSrchRitemCdArr = new ArrayList();
        String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
        for (String keyword : spVrSrchRitemCdLike ){
        	vrSrchRitemCdArr.add(keyword);
        }
        model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
        
        map.put("LIST", dao.asList015(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : asList016
     * 대체 Method 설명    : asList016 AS관리 조회
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> asList016(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> transCustNmArr = new ArrayList();
    	String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
    	for (String keyword : spVrSrchTransCustNmLike ){
    		transCustNmArr.add(keyword);
    	}
    	model.put("transCustNmArr", transCustNmArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.asList016(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : asList
     * 대체 Method 설명    : 배송관리 조회
     * 작성자                      : KHKIM
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> asList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> transCustNmArr = new ArrayList();
    	String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
    	for (String keyword : spVrSrchTransCustNmLike ){
    		transCustNmArr.add(keyword);
    	}
    	model.put("transCustNmArr", transCustNmArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.asList(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : tsdList
     * 대체 Method 설명    : 배송관리 상세 조회
     * 작성자                      : seongjun kwon
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> tsdList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	map.put("LIST", dao.tsdList(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : confPrintByOne
     * 대체 Method 설명    : 인수증 출력 확정 (KCC글라스 배송 단건 출력확인)
     * 작성자                      : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> confPrintByOne(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            dao.confPrintEnd(model);
            m.put("errCnt", errCnt);
            m.put("MSG", "인수증 출력확인(확정) 되었습니다.");
        } catch(Exception e){
    	   if (log.isInfoEnabled()) {
               log.info(e.getMessage());
           }
    	   m.put("MSG", e.getMessage());
            throw e;
        }
        
        return m;
    }
    /**
     * 
     * 대체 Method ID   : confPrintByOneHD
     * 대체 Method 설명    : 인수증 출력 확정
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> confPrintByOneHD(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        		System.out.println(model.get("selectIds").toString());
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("DLV_ORD_ID", model.get("DLV_ORD_ID"+i));
                
                if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.confPrintEnd(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new BizException(MessageResolver.getMessage("save.error"));
                }
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", "인수증 출력확인(확정) 되었습니다.");
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID    : 배송출발(모바일)
	 * Method 설명      : 배송출발(모바일처리)
	 * 작성자                 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> mobileDlvStart(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	int insCnt = Integer.parseInt(model.get("selectIds").toString());
        	String[] userNo        = new String[insCnt];
         	String[] ordId     = new String[insCnt];
         	String[] phoneNo     = new String[insCnt];
         	String[] serialNo   = new String[insCnt];
         	String[] date   = new String[insCnt];
         	String[] memo     = new String[insCnt];
         	String[] timearea     = new String[insCnt];
         	String[] version     = new String[insCnt];
         	String[] comCost     = new String[insCnt];
         	String[] costType     = new String[insCnt];
         	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	//userNo[i]         = (String) model.get(ConstantIF.SS_USER_ID);
            	userNo[i]		  = (String) model.get("USER_ID"+i);
            	ordId[i]     	  = (String) model.get("DLV_ORD_ID"+i);
            	phoneNo[i]        = (String) model.get("PHONE_NO"+i);
            	serialNo[i]       = "WEB";
            	date[i]           = (String) model.get("DLV_REQ_DT"+i);
            	memo[i]           = "WEB 모바일배송출발 전산 수기처리";
            	timearea[i]        = "";
            	version[i]        = "";
            	comCost[i]        = "";
            	costType[i]       = "";
            	
            }
           
            
            //프로시져에 보낼것들 다담는다
    		Map<String, Object> modelPlIns = new HashMap<String, Object>();
    		modelPlIns.put("I_USER_ID" 			  , userNo);
    		modelPlIns.put("I_ORD_ID"  			  , ordId); //DLV_ORD_ID

    		modelPlIns.put("I_PHONE_NO"    	      , phoneNo);
    		modelPlIns.put("I_SERIAL_NO"    	  , serialNo);
    		modelPlIns.put("I_DATE"    	    	  , date);
    		modelPlIns.put("I_MEMO"    	    	  , memo);
    		modelPlIns.put("I_TIMEAREA"    	      , timearea);
    		modelPlIns.put("I_VERSION"    	      , version);
    		modelPlIns.put("I_COM_COST"     	  , comCost);
    		modelPlIns.put("I_COST_TYPE"    	  , costType);
            
            // SP > STRINGARRAY  -> 실제는 단건처리됨.
            modelPlIns = (Map<String, Object>)dao.mobileDlvStart(modelPlIns);
            ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelPlIns.get("O_MSG_CODE")), (String)modelPlIns.get("O_MSG_NAME"));
        	
                
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    
    /**
     * 
     * Method ID    : 배송완료(모바일)
	 * Method 설명      : 배송완료 (모바일처리)
	 * 작성자                 : yhku 
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> mobileDlvComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
        	int insCnt = Integer.parseInt(model.get("selectIds").toString());
        	String[] userNo        = new String[insCnt];
         	String[] ordId     = new String[insCnt];
         	String[] phoneNo     = new String[insCnt];
         	String[] serialNo   = new String[insCnt];
         	String[] date   = new String[insCnt];
         	String[] memo     = new String[insCnt];
         	String[] timearea     = new String[insCnt];
         	String[] version     = new String[insCnt];
         	String[] comCost     = new String[insCnt];
         	String[] costType     = new String[insCnt];
         	String[] setType     = new String[insCnt];
         	String[] dlvOption1     = new String[insCnt];
         	String[] dlvOption2     = new String[insCnt];
         	String[] dlvOption3     = new String[insCnt];
         	String[] dlvOption4     = new String[insCnt];
         	String[] dlvOption5     = new String[insCnt];
         	String[] dlvOption6     = new String[insCnt];
         	String[] dlvOption7     = new String[insCnt];
         	String[] dlvOption8     = new String[insCnt];
         	String[] dlvOption9     = new String[insCnt];
         	String[] dlvOption10     = new String[insCnt];
         	
         	
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            	//userNo[i]         = (String) model.get(ConstantIF.SS_USER_ID);
            	userNo[i]		  = (String) model.get("USER_ID"+i);
            	ordId[i]     	  = (String) model.get("DLV_ORD_ID"+i);
            	phoneNo[i]        = (String) model.get("PHONE_NO"+i);
            	serialNo[i]       = "WEB";
            	date[i]           = "";
            	memo[i]           = "WEB 모바일배송완료 전산 수기처리";
            	timearea[i]        = "";
            	version[i]        = "";
            	comCost[i]        = "";
            	costType[i]       = "";
            	setType[i]        = "N";
            	dlvOption1[i]     = "";
            	dlvOption2[i]     = "N";
            	dlvOption3[i]     = "";
            	dlvOption4[i]     = "";
            	dlvOption5[i]     = "comp_elec";
            	dlvOption6[i]     = "";
            	dlvOption7[i]     = "";
            	dlvOption8[i]     = "";
            	dlvOption9[i]     = "";
            	dlvOption10[i]    = "";
            	
            }
           
            
            //프로시져에 보낼것들 다담는다
    		Map<String, Object> modelPlIns = new HashMap<String, Object>();
    		modelPlIns.put("I_USER_ID" 			  , userNo);
    		modelPlIns.put("I_ORD_ID"  			  , ordId); //DLV_ORD_ID

    		modelPlIns.put("I_PHONE_NO"    	      , phoneNo);
    		modelPlIns.put("I_SERIAL_NO"    	  , serialNo);
    		modelPlIns.put("I_DATE"    	    	  , date);
    		modelPlIns.put("I_MEMO"    	    	  , memo);
    		modelPlIns.put("I_TIMEAREA"    	      , timearea);
    		modelPlIns.put("I_VERSION"    	      , version);
    		modelPlIns.put("I_COM_COST"     	  , comCost);
    		modelPlIns.put("I_COST_TYPE"    	  , costType);
    		modelPlIns.put("I_SET_TYPE"           , setType);
    		modelPlIns.put("I_DLV_OPTION1"        , dlvOption1);// -- 기타비용 값이 있으면 UPDATE
            modelPlIns.put("I_DLV_OPTION2"        , dlvOption2);//-- 양중여부, Y : 양중, N : 양중없음, C  : 양중+기존제품 내림    
            modelPlIns.put("I_DLV_OPTION3"        , dlvOption3);
            modelPlIns.put("I_DLV_OPTION4"        , dlvOption4);
            modelPlIns.put("I_DLV_OPTION5"        , dlvOption5);//--모바일 배송완료 전송 메뉴명
            modelPlIns.put("I_DLV_OPTION6"        , dlvOption6);
            modelPlIns.put("I_DLV_OPTION7"        , dlvOption7);
            modelPlIns.put("I_DLV_OPTION8"        , dlvOption8);
            modelPlIns.put("I_DLV_OPTION9"        , dlvOption9);
            modelPlIns.put("I_DLV_OPTION10"       , dlvOption10);
            
            // SP > STRINGARRAY  -> 실제는 단건처리됨.
            modelPlIns = (Map<String, Object>)dao.mobileDlvComplete(modelPlIns);
            ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelPlIns.get("O_MSG_CODE")), (String)modelPlIns.get("O_MSG_NAME"));
        	
                
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : checkHDRitem
     * Method 설명 : 상품상세조회
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> checkHDRitem(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.checkHDRitem(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }    
    

    /**
     * Method ID   : dlvLcTranCallback
     * Method 설명    : 배송센터 자동이관 후 SMS 발송 데이터 조회
     * 작성자               : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> dlvLcTranCallback(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try {
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();
            
            // session 및 필요정보
            modelIns.put("I_LC_ID"    	, (String)model.get(ConstantIF.SS_SVC_NO));
            modelIns.put("I_WORK_IP"  	, (String)model.get(ConstantIF.SS_CLIENT_IP));
            modelIns.put("I_USER_NO"  	, (String)model.get(ConstantIF.SS_USER_NO));
            modelIns.put("I_TRUST_CUST_ID"  	, (String)model.get("vrSrchCustId"));
            
            // dao
            modelIns = (Map<String, Object>)dao.dlvLcTranCallback(modelIns);
            ServiceUtil.isValidReturnCode("WMSSP010", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            
            m.put("O_CURSOR", modelIns.get("O_CURSOR"));
            m.put("MSG_ORA", (String)modelIns.get("O_MSG_NAME"));
            m.put("errCnt", 0);
        } catch(com.logisall.winus.frm.exception.BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch (Exception e) {
            throw e;
        }
        return m;
    }
    
}