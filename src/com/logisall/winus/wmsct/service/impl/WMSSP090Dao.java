package com.logisall.winus.wmsct.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSSP090Dao")
public class WMSSP090Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID : listNew
	 * Method 설명 : 고객만족도(고객별)
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listNew(Map<String, Object> model) {
	    return executeQueryPageWq("wmssp090.listNew", model);
	}
	
	/**
	 * Method ID : listNew2
	 * Method 설명 : 고객만족도(기사별)
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listNew2(Map<String, Object> model) {
	    return executeQueryPageWq("wmssp090.listNew2", model);
	}

	/**
	 * Method ID : listNew3 
	 * Method 설명 : 고객만족도(센터별)
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listNew3(Map<String, Object> model) {
	    return executeQueryPageWq("wmssp090.listNew3", model);
	}
	
    /**
     * Method ID  : selectDrivers
     * Method 설명  : 기사정보 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object selectDrivers(Map<String, Object> model){
     	return executeQueryForList("wmssp010.selectDrivers", model);
     }
     
     /**
      * Method ID : selectItem
      * Method 설명 : 상품군 리스트 조회
      * 작성자 : 기드온
      * @param model
      * @return
      */
     public Object selectItem(Map<String, Object> model){
         return executeQueryForList("wmsms094.selectItemGrp", model);
     }

  	/**
  	 * Method ID : smsInfoSaveCsat
  	 * Method 설명 : 고객만족도 서비스 SMS 발송 여부
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object smsInfoSaveCsat(Map<String, Object> model) {
  		return executeUpdate("wmssp090.smsInfoSaveCsat", model);
  	}
}
