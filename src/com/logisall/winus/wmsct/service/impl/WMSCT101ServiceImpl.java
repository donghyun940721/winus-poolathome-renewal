package com.logisall.winus.wmsct.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsct.service.WMSCT101Service;
import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSCT101Service")
public class WMSCT101ServiceImpl implements WMSCT101Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSCT101Dao")
    private WMSCT101Dao dao;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listDetail
     * 대체 Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listDetail(model));
        return map;
    }
    
    @Override
    public Map<String, Object> csList(Map<String, Object> model)
    		throws Exception {
    	// TODO Auto-generated method stub
    	return null;
    }
    
    @Override
    public Map<String, Object> csSave(Map<String, Object> model)
    		throws Exception {
    	// TODO Auto-generated method stub
    	return null;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("vrSrchReqId" 		, model.get("vrSrchReqId"));
//            modelDt.put("vrSrchOrdNo" 		, model.get("vrSrchOrdNo"));
            modelDt.put("vrSrchOrdNo" 		, model.get("vrSrchOrgOrdNoNew"));
            modelDt.put("TextReqContents" 	, model.get("TextReqContents"));
            modelDt.put("TextResult" 		, model.get("TextResult"));
            modelDt.put("vrSrchComType" 	, model.get("vrSrchComType"));
            modelDt.put("vrSrchPtnrCd" 		, model.get("vrSrchPtnrCd"));
            modelDt.put("vrSrchDlvClaim" 	, model.get("vrSrchDlvClaim"));
            modelDt.put("ST_CS_ID" 			, model.get("ST_CS_ID"));
            modelDt.put("ST_CS_SEQ" 		, model.get("ST_CS_SEQ"));
            
            modelDt.put("ST_CS_SEQ" 		, model.get("ST_CS_SEQ"));

		    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
		    modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
            
            if("INSERT".equals(model.get("ST_GUBUN"))){
                dao.insertCs(modelDt);
            }else if("UPDATE".equals(model.get("ST_GUBUN"))){
                dao.updateCs(modelDt);
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID	: listCsInfo
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listCsInfo(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listCsInfo(model));
        return map;
    }
    /**
     * 
     * 대체 Method ID	: selectOne
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    
    @Override
    public Map<String, Object> getFaIssueLabel(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("FA_ISSUE_LABEL",dao.getFaIssueLabel(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID	: itemList
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> itemList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.itemList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: itemList
     * 대체 Method 설명	: 
     * 작성자			: 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> recentOrderList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	if(model.get("page") == null) {
    		model.put("pageIndex", "1");
    	} else {
    		model.put("pageIndex", model.get("page"));
    	}
    	if(model.get("rows") == null) {
    		model.put("pageSize", "20");
    	} else {
    		model.put("pageSize", model.get("rows"));
    	}
    	map.put("LIST", dao.recentOrderList(model));
    	return map;
    }
    
}
