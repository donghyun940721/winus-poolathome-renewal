package com.logisall.winus.wmsct.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSSP061Dao")
public class WMSSP061Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
	/**
     * Method ID  : myLcList
     * Method 설명  : 
     * 작성자             : 기드온
     * @param model
     * @return
     */
     public Object myLcList(Map<String, Object> model){
     	return executeQueryForList("wmssp061.myLcList", model);
     }
     
     /**
      * Method ID    : save
      * Method 설명      : 
      * 작성자                 : chsong
      * @param   model
      * @return
      */
     public Object save(Map<String, Object> model){
         executeUpdate("wmssp061.PK_WMSCT010.seq_dlv_stock_order", model);
         return model;
     }
     
	/**
     * Method ID : list
     * Method 설명 : 입출고미완료현황
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmssp061.list", model);
    }
    
    /**
     * Method ID : listHD
     * Method 설명 : 입출고미완료현황
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet listHD(Map<String, Object> model) {
        return executeQueryPageWq("wmssp061.listHD", model);
    }
    
    /**
     * Method ID : list2
     * Method 설명 : 입출고미완료현황
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list2(Map<String, Object> model) {
    	return executeQueryPageWq("wmssp061.list2", model);
    }
}
