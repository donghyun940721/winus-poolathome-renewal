package com.logisall.winus.wmsct.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmsct.service.WMSSP100Service;


@Service("WMSSP100Service")
public class WMSSP100ServiceImpl implements WMSSP100Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSSP100Dao")
    private WMSSP100Dao dao;
    
    private final static String[] CHECK_VALIDATE_WMSSP100 = {"DLV_ORD_ID", "SALES_CUST_ID"};
    
    /**
     * Method ID   : selectBox
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DRIVERS", dao.selectDrivers(model));
        map.put("ITEMGRP", dao.selectItem(model));
        return map;
    }

	/**
     * Method ID   : selectBox2
     * Method 설명    : 배송관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ASNLC", dao.selectAsncl(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : list
     * 대체 Method 설명    : 반품관리
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.list(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : detailList
     * 대체 Method 설명    : 반품관리 디테일
     * 작성자                      : sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> detailList(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	
    	List<String> cityArr = new ArrayList();
    	String[] spSrchAddrLike = model.get("vrSrchAddrLike").toString().split(",");
    	for (String keyword : spSrchAddrLike ){
    		cityArr.add(keyword);
    	}
    	model.put("cityArr", cityArr);
    	
    	List<String> transCustNmArr = new ArrayList();
    	String[] spVrSrchTransCustNmLike = model.get("vrSrchTransCustNm").toString().split(",");
    	for (String keyword : spVrSrchTransCustNmLike ){
    		transCustNmArr.add(keyword);
    	}
    	model.put("transCustNmArr", transCustNmArr);
    	
    	List<String> vrSrchRitemCdArr = new ArrayList();
    	String[] spVrSrchRitemCdLike = model.get("vrSrchRitemCd").toString().split(",");
    	for (String keyword : spVrSrchRitemCdLike ){
    		vrSrchRitemCdArr.add(keyword);
    	}
    	model.put("vrSrchRitemCdArr", vrSrchRitemCdArr);
    	
    	map.put("LIST", dao.detailList(model));
    	return map;
    }
    
}