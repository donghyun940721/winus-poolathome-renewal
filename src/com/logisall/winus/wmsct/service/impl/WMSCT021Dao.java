package com.logisall.winus.wmsct.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSCT021Dao")
public class WMSCT021Dao extends SqlMapAbstractDAO {

    /**
  	 * Method ID : insertE4
  	 * Method 설명 :
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertE4(Map<String, Object> model) {
  		return executeInsert("wmsct021.insertE4", model);
  	}
  	
  	/**
	 * Method ID : listCT021
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet listCT021(Map<String, Object> model) {
		return executeQueryPageWq("wmsct021.list", model);
	}
	
	/**
	 * Method ID : updateE4
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateE4(Map<String, Object> model) {
		return executeUpdate("wmsct021.updateE4", model);
	}
	
	 /**
  	 * Method ID : insertDlvOrderWMSCT010 
  	 * Method 설명 :
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertDlvOrderWMSCT010(Map<String, Object> model) {
  		return executeInsert("wmsct021.insertDlvOrderWMSCT010", model);
  	}
  	
	 /**
  	 * Method ID : insertDlvOrderWMSSP010 
  	 * Method 설명 :
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertDlvOrderWMSSP010(Map<String, Object> model) {
  		return executeInsert("wmsct021.insertDlvOrderWMSSP010", model);
  	}

	 /**
  	 * Method ID : insertDlvOrderWMSSP011
  	 * Method 설명 :
  	 * 작성자 : sing09
  	 * 
  	 * @param model
  	 * @return
  	 */
  	public Object insertDlvOrderWMSSP011(Map<String, Object> model) {
  		return executeInsert("wmsct021.insertDlvOrderWMSSP011", model);
  	}
  	
    /**
	 * Method ID : updateDlvOrderWMSCT010 
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateDlvOrderWMSCT010(Map<String, Object> model) {
		return executeUpdate("wmsct021.updateDlvOrderWMSCT010", model);
	}
	
    /**
	 * Method ID : updateDlvOrderWMSSP010 
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateDlvOrderWMSSP010(Map<String, Object> model) {
		return executeUpdate("wmsct021.updateDlvOrderWMSSP010", model);
	}

    /**
	 * Method ID : updateDlvOrderWMSSP011
	 * Method 설명 :  
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateDlvOrderWMSSP011(Map<String, Object> model) {
		return executeUpdate("wmsct021.updateDlvOrderWMSSP011", model);
	}
}
