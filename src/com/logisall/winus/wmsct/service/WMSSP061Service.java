package com.logisall.winus.wmsct.service;

import java.util.Map;


public interface WMSSP061Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listHD(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelHD(Map<String, Object> model) throws Exception;
    public Map<String, Object> list2(Map<String, Object> model) throws Exception;
}
