package com.logisall.winus.wmsct.service;

import java.util.Map;


public interface WMSCT000Service {
    public Map<String, Object> custInfoUpdate(Map<String, Object> model) throws Exception;
}
