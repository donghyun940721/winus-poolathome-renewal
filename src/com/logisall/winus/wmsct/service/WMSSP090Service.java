package com.logisall.winus.wmsct.service;

import java.util.Map;


public interface WMSSP090Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> listNew(Map<String, Object> model) throws Exception;
    public Map<String, Object> listNew2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listNew3(Map<String, Object> model) throws Exception;
    public Map<String, Object> smsInfoSaveCsat(Map<String, Object> model) throws Exception;
}
