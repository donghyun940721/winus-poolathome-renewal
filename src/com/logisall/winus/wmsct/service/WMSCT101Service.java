package com.logisall.winus.wmsct.service;

import java.util.Map;

public interface WMSCT101Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listDetail(Map<String, Object> model) throws Exception;
    public Map<String, Object> csList(Map<String, Object> model) throws Exception;
    public Map<String, Object> csSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listCsInfo(Map<String, Object> model) throws Exception;
	public Map<String, Object> getFaIssueLabel(Map<String, Object> model) throws Exception;
	public Map<String, Object> itemList(Map<String, Object> model)throws Exception;
	public Map<String, Object> recentOrderList(Map<String, Object> model) throws Exception;
}
