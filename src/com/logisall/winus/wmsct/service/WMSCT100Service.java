package com.logisall.winus.wmsct.service;

import java.util.Map;

public interface WMSCT100Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> toDayInfo(Map<String, Object> model) throws Exception;
}
