package com.logisall.winus.wmsct.service;

import java.util.Map;


public interface WMSSP100Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception;
	public Map<String, Object> list(Map<String, Object> model) throws Exception ;
	public Map<String, Object> detailList(Map<String, Object> model) throws Exception ;
}
