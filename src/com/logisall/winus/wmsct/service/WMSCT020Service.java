package com.logisall.winus.wmsct.service;

import java.util.List;
import java.util.Map;


public interface WMSCT020Service {
    public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE3(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveE4(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertCt020(Map<String, Object> model, String sp011) throws Exception;
    public Map<String, Object> updateCt020(Map<String, Object> model, String sp011) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listCT021(Map<String, Object> model) throws Exception;
    public Map<String, Object> custInfoSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> custInfoSave021(Map<String, Object> model) throws Exception;
    public Map<String, Object> customerfile(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectAsErrList(Map<String, Object> model) throws Exception;
    public Map<String, Object> nominalTransfer(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveOutOrderQ2(Map<String, Object> model) throws Exception;
}
