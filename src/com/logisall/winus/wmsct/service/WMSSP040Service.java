package com.logisall.winus.wmsct.service;

import java.util.List;
import java.util.Map;


public interface WMSSP040Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
