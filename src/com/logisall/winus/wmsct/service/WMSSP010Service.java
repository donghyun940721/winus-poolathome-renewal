package com.logisall.winus.wmsct.service;

import java.util.Map;


public interface WMSSP010Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> save2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveNew(Map<String, Object> model) throws Exception;
    public Map<String, Object> driverReqDtDel(Map<String, Object> model) throws Exception;
    public Map<String, Object> rowConfirm(Map<String, Object> model) throws Exception;
    public Map<String, Object> mobileDlvStart(Map<String, Object> model) throws Exception;
    public Map<String, Object> mobileDlvComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> checkHDRitem(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT1_10(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveImg(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception;
    public Map<String, Object> smsInfoSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> smsInfoSave2(Map<String, Object> model) throws Exception;
    public Map<String, Object> smsInfoSave3(Map<String, Object> model) throws Exception;
    public Map<String, Object> smsInfoSave4(Map<String, Object> model) throws Exception;
    public Map<String, Object> smsSaveConfUpdate(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveLcOrdMove(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveDlvLcTran(Map<String, Object> model) throws Exception;
    public Map<String, Object> asCntView(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ6(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateSubUserInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ7(Map<String, Object> model) throws Exception;
    public Map<String, Object> getInOrdByDlv(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> unDlvList(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelUnDlvList(Map<String, Object> model) throws Exception;
    public Map<String, Object> confPrintEnd(Map<String, Object> model) throws Exception;
	public Map<String, Object> tsList(Map<String, Object> model) throws Exception ;
	public Map<String, Object> hdList(Map<String, Object> model) throws Exception ;
	public Map<String, Object> hdDetailList(Map<String, Object> model) throws Exception ;
	public Map<String, Object> hd016List(Map<String, Object> model) throws Exception ;
	public Map<String, Object> hdDetail016List(Map<String, Object> model) throws Exception ;
	public Map<String, Object> asList(Map<String, Object> model) throws Exception ;
	public Map<String, Object> asList015(Map<String, Object> model) throws Exception ;
	public Map<String, Object> asList016(Map<String, Object> model) throws Exception ;
	public Map<String, Object> punctureList(Map<String, Object> model) throws Exception;
	public Map<String, Object> punctureDetailList(Map<String, Object> model) throws Exception;
	public Map<String, Object> tsdList(Map<String, Object> model) throws Exception ;
	public Map<String, Object> confPrintByOne(Map<String, Object> model) throws Exception;
	public Map<String, Object> confPrintByOneHD(Map<String, Object> model) throws Exception;
	public Map<String, Object> dlvLcTranCallback(Map<String, Object> model) throws Exception;
}
