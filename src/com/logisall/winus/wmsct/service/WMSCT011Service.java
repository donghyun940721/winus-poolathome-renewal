package com.logisall.winus.wmsct.service;

import java.util.Map;


public interface WMSCT011Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> listNew(Map<String, Object> model) throws Exception;
    public Map<String, Object> listNew2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listNew3(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveNew(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteNew(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteNew_CS(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveNewAs(Map<String, Object> model) throws Exception;
}
