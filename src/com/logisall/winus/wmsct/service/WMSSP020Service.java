package com.logisall.winus.wmsct.service;

import java.util.List;
import java.util.Map;


public interface WMSSP020Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> rowConfirm(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT1(Map<String, Object> model) throws Exception;
    public Map<String, Object> listT1_10(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveImg(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectBox2(Map<String, Object> model) throws Exception;
    public Map<String, Object> smsInfoSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> smsSaveConfUpdate(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveLcOrdMove(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveDlvLcTran(Map<String, Object> model) throws Exception;
    public Map<String, Object> asCntView(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ6(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateSubUserInfo(Map<String, Object> model) throws Exception;
    public Map<String, Object> listQ7(Map<String, Object> model) throws Exception;
    public Map<String, Object> getInOrdByDlv(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveInOrder(Map<String, Object> model) throws Exception;
    public Map<String, Object> unDlvList(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcelUnDlvList(Map<String, Object> model) throws Exception;
    public Map<String, Object> confPrintEnd(Map<String, Object> model) throws Exception;
	public Map<String, Object> tsList(Map<String, Object> model) throws Exception ;
	public Map<String, Object> asList(Map<String, Object> model) throws Exception ;
	public Map<String, Object> punctureList(Map<String, Object> model) throws Exception;
	public Map<String, Object> punctureDetailList(Map<String, Object> model) throws Exception;
	public Map<String, Object> tsdList(Map<String, Object> model) throws Exception ;
	public Map<String, Object> confPrintByOne(Map<String, Object> model) throws Exception;
}
