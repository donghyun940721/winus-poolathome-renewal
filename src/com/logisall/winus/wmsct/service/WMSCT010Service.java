
package com.logisall.winus.wmsct.service;

import java.util.List;
import java.util.Map;


public interface WMSCT010Service {
	public Map<String, Object> selectBox(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> emptyCustRowSave(Map<String, Object> model) throws Exception;
    public Map<String, Object> imgDownList(Map<String, Object> model) throws Exception;
    public Map<String, Object> getExcelDown2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
}
