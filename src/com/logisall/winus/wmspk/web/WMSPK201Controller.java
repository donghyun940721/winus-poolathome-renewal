package com.logisall.winus.wmspk.web;

import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ExcelWriter;
import com.m2m.jdfw5x.egov.database.GenericResultSet;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmspk.service.WMSPK201Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSPK201Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSPK201Service")
	private WMSPK201Service service;

	static final String[] COLUMN_NAME_WMSPK201 = {
						  "O_SEQ"
						, "O_HK"
						, "O_PART_NO"
						, "O_PART_NAME"
						, "O_P_PDC"
						, "O_D_PDC"
						, "O_SUPPLIER"
						, "O_SOURCE"
						, "O_SHIP"
						, "O_PO_NO"
						, "O_PO_LINE_NO"
						, "O_SHIPMENT_NO"
						, "O_SHIPMENT_LINE_NO"
						, "O_POI_NO"
						, "O_INVOICE_NO"
						, "O_CASE_NO"
						, "O_SHIPMENT_DATE"
						, "O_INVOICE_DATE"
						, "O_PO_DATE"
						, "O_DUE_DATE"
						, "O_RCV_DATE"
						, "O_PO_QTY"
						, "O_ASN_QTY"
						, "O_DI_QTY"
						, "O_RCV_QTY"
						, "O_U_PRICE"
						, "O_CUR"
						, "O_PO"
						, "O_RCV"
						, "O_RCV_NO"
						, "O_STATUS"
						, "O_BCR_FLAG"
						, "O_POD"
	};
	
	/*-
	 * Method ID    : WMSPK201
	 * Method 설명      : 엑셀주문입력
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WINUS/WMSPK201.action")
	public ModelAndView WMSPK201(Map<String, Object> model) {
		return new ModelAndView("winus/WMSPK/WMSPK201");
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 공지사항 목록
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPK201/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : WMSPK201E2
	 * Method 설명      : 공지사항 상세화면
	 * 작성자                 : chsong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSPK201E2.action")
	public ModelAndView WMSPK201E2(Map<String, Object> model) {
		return new ModelAndView("winus/WMSPK/WMSPK201E2");
	}
	
	/*-
	 * Method ID  : inExcelFileUpload
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPK201/uploadExcelInfo.action")
	public ModelAndView uploadExcelInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;
			
			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));
			
			int startRow = Integer.parseInt((String) model.get("startRow"));
			
			//COLUMN_NAME_WMSPK201 엑셀 컬럼 정의 (엑셀파일 컬럼 순서와 동일)
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSPK201, 0, startRow, 10000, 0);
			m = service.saveExcelInfo(model, list);
			
			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : excelDown
	 * Method 설명   : 엑셀다운로드
	 */
	@RequestMapping("/WMSPK201/excelDown.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.excelDown(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download excel :", e);
			}
		}
	}
	protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
        	//헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getMessage("HK")			, "0", "0", "0", "0", "200"},
                                   {MessageResolver.getMessage("PART_NO")		, "1", "1", "0", "0", "200"},
                                   {MessageResolver.getMessage("PART_NAME")		, "2", "2", "0", "0", "200"},
                                   {MessageResolver.getMessage("SUPPILER")		, "3", "3", "0", "0", "200"},
                                   {MessageResolver.getMessage("PURCHASER")		, "4", "4", "0", "0", "200"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"HK"           	, "S"},
                                    {"PART_NO"          , "S"},
                                    {"PART_NAME"       	, "S"},
                                    {"SUPPILER"         , "S"},
                                    {"PURCHASER"        , "S"}
                                   }; 
            
			// 파일명
            String fileName = MessageResolver.getText("부품입고관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}
}
