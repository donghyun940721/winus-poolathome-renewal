package com.logisall.winus.wmspk.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmspk.service.WMSPK205Service;

@Service("WMSPK205Service")
public class WMSPK205ServiceImpl implements WMSPK205Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPK205Dao")
    private WMSPK205Dao dao;
    
    /**
     * 
     * Method ID   : selectPoolGrp
     * Method 설명    : 물류용기입고관리 화면에서 필요한 데이터
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectPoolGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("POOLGRP", dao.selectPoolGrp(model));
        return map;
    }
    
    /**
     * Method ID   : list
     * Method 설명    : 물류용기입고관리  조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
}
