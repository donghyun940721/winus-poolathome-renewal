package com.logisall.winus.wmspk.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmspk.service.WMSPK206Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSPK206Service")
public class WMSPK206ServiceImpl implements WMSPK206Service{

    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPK206Dao")
    private WMSPK206Dao dao;
    
    /**
     * 
     * Method ID   : selectItemGrp
     * Method �ㅻ�    : ��怨�愿�由� ��硫댁���� ������ �곗�댄��
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> selectItemGrp(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("ITEMGRP", dao.selectItemGrp(model));
        return map;
    }
    
    /**
     * Method ID   : list
     * Method �ㅻ�    : ��怨�愿�由�  議고��
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * Method ID   : lcinfolist
     * Method �ㅻ�    : ��怨�愿�由� �⑷�
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> lcinfolist(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.lcinfolist(model));
        return map;
    }
    
    /**
     * 
     * Method ID   : inOrderCntInit
     * Method �ㅻ�    : 
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> inOrderCntInit(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("DS_CNT", dao.inOrderCntInit(model));
        return map;
    }
    
    /**
     * 
     * Method ID   : deleteOrder
     * Method �ㅻ�    : 二쇰Ц����
     * ���깆��                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 1;
        String errMsg = MessageResolver.getMessage("delete.error");
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];          
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                }
                //��濡����몄�� 蹂대�쇨��� �ㅻ�대����
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                
                //session 諛� �깅���蹂�
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.deleteOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }                        
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("delete.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : listExcel
     * Method 설명      : 엑셀다운로드
     * 작성자                 : smics
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.list(model));
        return map;
    } 
    
    /**
     * 
     * Method ID   : listExcel(PLT)
     * Method 설명      : 엑셀다운로드(PLT)
     * 작성자                 : smics
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listExcelPlt(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        map.put("LIST", dao.listPlt(model));
        return map;
    } 
    
    /**
     * 
     * Method ID   : saveConfirmGrn
     * Method �ㅻ�    : 입하확정
     * ���깆��                      : chsong
     * @param model
     * @return
     * @throws Exception 
     */
    @Override
    public Map<String, Object> saveConfirmGrn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg ="";
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];         
                String[] refSubLotId  = new String[tmpCnt];     
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                    refSubLotId[i]    = (String)model.get("REF_SUB_LOT_ID"+i);  
                }
                //��濡����몄�� 蹂대�쇨��� �ㅻ�대����
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("refSubLotId", refSubLotId);
                
                //session 諛� �깅���蹂� 
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));

                //dao                
                modelIns = (Map<String, Object>)dao.saveConfirmGrn(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }                        
            m.put("errCnt", errCnt);
            // m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG", MessageResolver.getMessage("입하확정이 완료되었습니다."));            
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : saveCancelGrn
     * Method �ㅻ�    : 입하취소
     * ���깆��                      : chsong
     * @param model
     * @return
     * @throws Exception 
     */
    @Override
    public Map<String, Object> saveCancelGrn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg ="";
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];         
                String[] refSubLotId  = new String[tmpCnt];     
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                    refSubLotId[i]    = (String)model.get("REF_SUB_LOT_ID"+i);  
                }
                //��濡����몄�� 蹂대�쇨��� �ㅻ�대����
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("refSubLotId", refSubLotId);
                
                //session 諛� �깅���蹂� 
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));

                //dao                
                modelIns = (Map<String, Object>)dao.saveCancelGrn(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }                        
            m.put("errCnt", errCnt);
            // m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG", MessageResolver.getMessage("입하취소가 완료되었습니다."));            
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : saveInComplete
     * Method �ㅻ�    : ��怨�����
     * ���깆��                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveInComplete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];         
                String[] refSubLotId  = new String[tmpCnt];
                String[] workQty = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    		= (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   		= (String)model.get("ORD_SEQ"+i);         
                    refSubLotId[i]    	= (String)model.get("REF_SUB_LOT_ID"+i);
                    workQty[i]    		= null;
                    
                    Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("I_ORD_ID", (String)model.get("ORD_ID"+i));				
					String checkExistData = dao.checkWorkStatData(modelSP);
					
					//990 해당 ord_id의 db재조회 된 work_stat 입고 완료 일 경우
					if (checkExistData.equals("990")) {
						throw new BizException(MessageResolver.getMessage("save.check.error"));
                    }
                }
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                modelIns.put("refSubLotId", refSubLotId);
                modelIns.put("workQty", workQty);
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveInComplete(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
                
            }            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : saveSimpleIn
     * Method �ㅻ�    : 媛��몄��怨� ����
     * ���깆��               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveSimpleIn(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 1;
        String errMsg = MessageResolver.getMessage("save.error");
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] ordId  = new String[tmpCnt];                
                String[] ordSeq = new String[tmpCnt];          
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    ordId[i]    = (String)model.get("ORD_ID"+i);               
                    ordSeq[i]   = (String)model.get("ORD_SEQ"+i);         
                }
                //��濡����몄�� 蹂대�쇨��� �ㅻ�대����
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);
                
                //session 諛� �깅���蹂�
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.saveSimpleIn(modelIns);
                errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                errMsg = modelIns.get("O_MSG_NAME").toString();
            }
                        
            m.put("errCnt", errCnt);
            if(errCnt == 0){
                m.put("MSG", MessageResolver.getMessage("save.success"));
            }else{
                m.put("MSG", errMsg);
            }
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * Method ID   : checkTest
     * Method �ㅻ�    : ����由우���⑥�깃���
     * ���깆��               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> checkTest(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("save.error");
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                String[] itemCode = new String[tmpCnt];
                String[] whId = new String[tmpCnt];       
                String[] custId  = new String[tmpCnt];  
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                    itemCode[i] = (String)model.get("ITEM_CD"+i);               
                    whId[i]     = (String)model.get("WH_CD"+i);       
                    custId[i]   = (String)model.get("CUST_CD"+i);       
                }
                //��濡����몄�� 蹂대�쇨��� �ㅻ�대����
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("itemCode", itemCode);
                modelIns.put("whId", whId);
                modelIns.put("custId", custId);
                
                //session 諛� �깅���蹂�
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.chkTest(modelIns);
                errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                errMsg = modelIns.get("O_MSG_NAME").toString();
            }
                        
            m.put("errCnt", errCnt);
            m.put("MSG", errMsg);
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    

    /**
     * 
     * ��泥� Method ID   : listInOrderItemASN
     * ��泥� Method �ㅻ�    : ��怨�愿�由� ASN ���몄����議고��
     * ���깆��                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listInOrderItemASN(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.listInOrderItemASN(model));
        return map;
    }
    
    /**
     * ��泥� Method ID   : saveAsnOrder
     * ��泥� Method �ㅻ�    : ��怨�ASN二쇰Ц
     * ���깆��                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveAsnOrder(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            // int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
                //����, ����
                String[] dsSubRowStatus = new String[insCnt];                
                String[] ordSeq         = new String[insCnt];         
                String[] ritemId        = new String[insCnt];     
                String[] custLotNo      = new String[insCnt];     
                String[] realInQty      = new String[insCnt];     
                
                String[] realOutQty     = new String[insCnt];                     
                String[] makeDt         = new String[insCnt];         
                String[] timePeriodDay  = new String[insCnt];     
                String[] locYn          = new String[insCnt];     
                String[] pdaCd          = new String[insCnt];     
                
                String[] workYn         = new String[insCnt];                
                String[] rjType         = new String[insCnt];         
                String[] realPltQty     = new String[insCnt];     
                String[] realBoxQty     = new String[insCnt];     
                String[] confYn         = new String[insCnt];     
                
                String[] unitAmt        = new String[insCnt];                
                String[] amt            = new String[insCnt];         
                String[] eaCapa         = new String[insCnt];     
                String[] boxBarcode     = new String[insCnt];     
                String[] inOrdUomId     = new String[insCnt];     
                
                String[] outOrdUomId    = new String[insCnt];                
                String[] inOrdQty       = new String[insCnt];         
                String[] outOrdQty      = new String[insCnt];     
                String[] refSubLotId    = new String[insCnt];     
                String[] dspId          = new String[insCnt];     
                
                String[] carId          = new String[insCnt];                
                String[] cntrId         = new String[insCnt];         
                String[] cntrNo         = new String[insCnt];     
                String[] cntrType       = new String[insCnt];     
                String[] badQty         = new String[insCnt];     
                
                String[] uomNm          = new String[insCnt];                
                String[] unitPrice      = new String[insCnt];         
                String[] whNm           = new String[insCnt];     
                String[] itemKorNm      = new String[insCnt];     
                String[] itemEngNm      = new String[insCnt];     
                
                String[] repUomId       = new String[insCnt];                
                String[] uomCd          = new String[insCnt];         
                String[] uomId          = new String[insCnt];     
                String[] repUomCd       = new String[insCnt];     
                String[] repuomNm       = new String[insCnt];     
                
                String[] itemGrpId      = new String[insCnt];                
                String[] expiryDate     = new String[insCnt];         
                String[] inOrdWeight    = new String[insCnt];                 
                String[] ordDesc        = new String[insCnt];     
                String[] validDt        = new String[insCnt];     
                
                String[] etc2           = new String[insCnt];
                String[] itemBestDate     = new String[insCnt];   //�������④린媛�     
                // String[] itemBestDateEnd  = new String[insCnt];   //�������④린媛�留�猷���
                String[] rtiNm            = new String[insCnt];   //臾쇰�湲곌린紐�
                String[] itemCode       = new String[insCnt];
                
                for(int i = 0 ; i < insCnt ; i ++){
                    dsSubRowStatus[i]  = (String)model.get("I_ST_GUBUN"+i);               
                    ordSeq[i]           = (String)model.get("I_ORD_SEQ"+i);          
                    ritemId[i]          = (String)model.get("I_RITEM_ID"+i);      
                    custLotNo[i]        = (String)model.get("I_CUST_LOT_NO"+i);      
                    realInQty[i]        = (String)model.get("I_REAL_IN_QTY"+i);      
                    
                    realOutQty[i]       = (String)model.get("I_REAL_OUT_QTY"+i);                      
                    makeDt[i]           = (String)model.get("I_MAKE_DT"+i);          
                    timePeriodDay[i]    = (String)model.get("I_TIME_PERIOD_DAY"+i);      
                    locYn[i]            = (String)model.get("I_LOC_YN"+i);      
                    pdaCd[i]            = (String)model.get("I_PDA_CD"+i);      
                    
                    workYn[i]           = (String)model.get("I_WORK_YN"+i);                 
                    rjType[i]           = (String)model.get("I_RJ_TYPE"+i);          
                    realPltQty[i]       = (String)model.get("I_REAL_PLT_QTY"+i);      
                    realBoxQty[i]       = (String)model.get("I_REAL_BOX_QTY"+i);      
                    confYn[i]           = (String)model.get("I_CONF_YN"+i);      
                    
                    unitAmt[i]          = (String)model.get("I_UNIT_AMT"+i);                 
                    amt[i]              = (String)model.get("I_AMT"+i);          
                    eaCapa[i]           = (String)model.get("I_EA_CAPA"+i);      
                    boxBarcode[i]       = (String)model.get("I_BOX_BARCODE"+i);      
                    inOrdUomId[i]       = (String)model.get("I_IN_ORD_UOM_ID"+i);      
                    
                    outOrdUomId[i]      = (String)model.get("I_OUT_ORD_UOM_ID"+i);                 
                    inOrdQty[i]         = (String)model.get("I_IN_ORD_QTY"+i);          
                    outOrdQty[i]        = (String)model.get("I_OUT_ORD_QTY"+i);      
                    refSubLotId[i]      = (String)model.get("I_REF_SUB_LOT_ID"+i);      
                    dspId[i]            = (String)model.get("I_DSP_ID"+i);      
                    
                    carId[i]            = (String)model.get("I_CAR_ID"+i);                 
                    cntrId[i]           = (String)model.get("I_CNTR_ID"+i);          
                    cntrNo[i]           = (String)model.get("I_CNTR_NO"+i);      
                    cntrType[i]         = (String)model.get("I_CNTR_TYPE"+i);      
                    badQty[i]           = (String)model.get("I_BAD_QTY"+i);      
                    
                    uomNm[i]            = (String)model.get("I_UOM_NM"+i);                 
                    unitPrice[i]        = (String)model.get("I_UNIT_PRICE"+i);          
                    whNm[i]             = (String)model.get("I_WH_NM"+i);      
                    itemKorNm[i]        = (String)model.get("I_ITEM_KOR_NM"+i);      
                    itemEngNm[i]        = (String)model.get("I_ITEM_ENG_NM"+i);      
                    
                    repUomId[i]         = (String)model.get("I_REP_UOM_ID"+i);                 
                    uomCd[i]            = (String)model.get("I_UOM_CD"+i);          
                    uomId[i]            = (String)model.get("I_UOM_ID"+i);      
                    repUomCd[i]         = (String)model.get("I_REP_UOM_CD"+i);      
                    repuomNm[i]         = (String)model.get("I_REP_UOM_NM"+i);      
                    
                    itemGrpId[i]        = (String)model.get("I_ITEM_GRP_ID"+i);                 
                    expiryDate[i]       = (String)model.get("I_EXPIRY_DATE"+i);          
                    inOrdWeight[i]      = (String)model.get("I_IN_ORD_WEIGHT"+i);      
                    ordDesc[i]          = (String)model.get("I_ORD_DESC"+i);      
                    validDt[i]          = (String)model.get("I_VALID_DT"+i);      
                    
                    etc2[i]             = (String)model.get("I_ETC2"+i); 
                    
                    //異�媛� 
                    itemBestDate[i]       = (String)model.get("I_ITEM_BEST_DATE"+i);      
                    // itemBestDateEnd[i]    = (String)model.get("I_ITEM_BEST_DATE_END"+i);   
                    rtiNm[i]              = (String)model.get("I_RTI_NM"+i);
                    itemCode[i]           = (String)model.get("I_ITEM_CODE"+i);
                }
                //��濡����몄�� 蹂대�쇨��� �ㅻ�대����
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("dsMain_rowStatus" , model.get("dsMain_rowStatus").toString());
                modelIns.put("vrOrdId"          , model.get("vrOrdId"));
                modelIns.put("inReqDt"          , model.get("calInDt").toString().replace("-", ""));  //��吏��대��源� ��留� - replace �댁�쇳������
                modelIns.put("inDt"             , model.get("inDt"));
                modelIns.put("custPoid"         , model.get("custPoid"));
                
                modelIns.put("custPoseq"        , model.get("custPoseq"));
                modelIns.put("orgOrdId"         , model.get("vrOrgOrdId"));
                modelIns.put("orgOrdSeq"        , model.get("orgOrdSeq"));
                modelIns.put("vrWhId"           , model.get("vrSrchWhId"));
                modelIns.put("outWhId"          , model.get("outWhId"));
                
                modelIns.put("transCustId"      , model.get("transCustId"));
                modelIns.put("vrCustId"         , model.get("vrSrchCustId"));
                modelIns.put("pdaFinishYn"      , model.get("pdaFinishYn"));
                modelIns.put("blNo"             , model.get("vrBlNo"));
                modelIns.put("workStat"         , model.get("workStat"));
                
                modelIns.put("ordType"          , model.get("ordType"));
                modelIns.put("ordSubtype"       , model.get("vrSrchOrderPhase"));
                modelIns.put("outReqDt"         , model.get("outReqDt").toString().replace("-", ""));
                modelIns.put("outDt"            , model.get("outDt"));
                modelIns.put("pdaStat"          , model.get("pdaStat"));
                
                modelIns.put("workSeq"          , model.get("workSeq"));
                modelIns.put("capaTot"          , model.get("capaTot"));
                modelIns.put("kinOutYn"         , model.get("kinOutYn"));
                modelIns.put("carConfYn"        , model.get("carConfYn"));
                modelIns.put("gvLcId"           , model.get("vrLcId"));
                
                modelIns.put("tplOrdId"         , model.get("tplOrdId"));
                modelIns.put("approveYn"        , model.get("vrApproveYn"));
                modelIns.put("payYn"            , model.get("payYn"));
                modelIns.put("inCustId"         , model.get("vrInCustId"));
                modelIns.put("inCustAddr"       , model.get("vrInCustAddr"));
                
                modelIns.put("inCustEmpNm"      , model.get("vrInCustEmpNm"));
                modelIns.put("inCustTel"        , model.get("vrInCustTel"));
                
                //sub
                modelIns.put("dsSub_rowStatus"  , dsSubRowStatus);
                modelIns.put("ordSeq"           , ordSeq);
                modelIns.put("ritemId"          , ritemId);
                modelIns.put("custLotNo"        , custLotNo);
                modelIns.put("realInQty"        , realInQty);
                
                modelIns.put("realOutQty"       , realOutQty);
                modelIns.put("makeDt"           , makeDt);
                modelIns.put("timePeriodDay"    , timePeriodDay);
                modelIns.put("locYn"            , locYn);
                modelIns.put("pdaCd"            , pdaCd);
                
                modelIns.put("workYn"           , workYn);
                modelIns.put("rjType"           , rjType);
                modelIns.put("realPltQty"       , realPltQty);
                modelIns.put("realBoxQty"       , realBoxQty);
                modelIns.put("confYn"           , confYn);
                
                modelIns.put("unitAmt"          , unitAmt);
                modelIns.put("amt"              , amt);
                modelIns.put("eaCapa"           , eaCapa);
                modelIns.put("boxBarcode"       , boxBarcode);
                modelIns.put("inOrdUomId"       , inOrdUomId);
                
                modelIns.put("outOrdUomId"      , outOrdUomId);
                modelIns.put("inOrdQty"         , inOrdQty);
                modelIns.put("outOrdQty"        , outOrdQty);
                modelIns.put("refSubLotId"      , refSubLotId);
                modelIns.put("dspId"            , dspId);
                
                modelIns.put("carId"            , carId);
                modelIns.put("cntrId"           , cntrId);
                modelIns.put("cntrNo"           , cntrNo);
                modelIns.put("cntrType"         , cntrType);
                modelIns.put("badQty"           , badQty);
                
                modelIns.put("uomNm"            , uomNm);
                modelIns.put("unitPrice"        , unitPrice);
                modelIns.put("whNm"             , whNm);
                modelIns.put("itemKorNm"        , itemKorNm);
                modelIns.put("itemEngNm"        , itemEngNm);
                
                modelIns.put("repUomId"         , repUomId);
                modelIns.put("uomCd"            , uomCd);
                modelIns.put("uomId"            , uomId);
                modelIns.put("repUomCd"         , repUomCd);
                modelIns.put("repuomNm"         , repuomNm);
                
                modelIns.put("itemGrpId"        , itemGrpId);
                modelIns.put("expiryDate"       , expiryDate);
                modelIns.put("inOrdWeight"      , inOrdWeight); 
                
                modelIns.put("ordDesc"          , ordDesc);
                modelIns.put("validDt"          , validDt);                
                modelIns.put("etc2"             , etc2);
                
                //異�媛���嫄�(��吏���濡����몃�� ����)
//                modelIns.put("itemBestDate"     , itemBestDate);
//                modelIns.put("itemBestDateEnd"  , itemBestDateEnd);               
                modelIns.put("rtiNm"            , rtiNm);
                modelIns.put("itemCode"            , itemCode);
                
                //session ��蹂�
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));

                //dao                
                modelIns = (Map<String, Object>)dao.saveAsnOrder(modelIns);                
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                
            }
            //�깅� ���� ��
            
//            if(isProcess){
//                if(delCnt > 0 ){
//                    String[] ordId  = new String[delCnt];         
//                    String[] ordSeq = new String[delCnt]; 
//                    for(int i = 0 ; i < delCnt ; i ++){
//                        ordId[i]  = (String)model.get("D_ORD_ID"+i);               
//                        ordSeq[i] = (String)model.get("D_ORD_SEQ"+i);          
//                    }
//                    
//                    //��濡����몄�� 蹂대�쇨��� �ㅻ�대����
//                    Map<String, Object> modelDel = new HashMap<String, Object>();
//                    modelDel.put("ordId", ordId);
//                    modelDel.put("ordSeq", ordSeq);
//                    
//                    modelDel.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
//                    modelDel.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
//                    
//                    modelDel = (Map<String, Object>)dao.deleteInOrder(modelDel);
//                    errCnt = Integer.parseInt(modelDel.get("O_MSG_CODE").toString());
//                    errMsg = modelDel.get("O_MSG_NAME").toString();
//                }
//                if(errCnt != 0){
//                    m.put("errCnt", errCnt);
//                    m.put("MSG", errMsg);
//                    isProcess = false;
//                }
//            }            
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 분할전
     * 대체 Method ID   : ifInOrdLocMapp
     * 대체 Method 설명    : 
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> ifInOrdLocMapp(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        // log.info(model);
        try{

            int delCnt = Integer.parseInt(model.get("D_selectIds").toString());
            int insCnt = Integer.parseInt(model.get("I_selectIds").toString());
            if(insCnt > 0){
                //저장, 수정
                String[] ifId 			= new String[insCnt];                
                String[] ifDt         	= new String[insCnt];         
                String[] moveNo         = new String[insCnt];     
                String[] materialLotId  = new String[insCnt];   
                
                for(int i = 0 ; i < insCnt ; i ++){
                	ifId[i]  			= (String)model.get("I_IF_ID"+i);               
                	ifDt[i]             = (String)model.get("I_IF_DT"+i);          
                	moveNo[i]           = (String)model.get("I_MOVE_NO"+i);      
                	materialLotId[i]	= (String)model.get("I_MATERIALLOTID"+i);      
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                //main
                modelIns.put("I_IF_ID"        	, ifId);
                modelIns.put("I_IF_DT"        	, ifDt);
                modelIns.put("I_MOVE_NO"        , moveNo);
                modelIns.put("I_MATERIALLOTID"	, materialLotId);
                modelIns.put("I_LC_ID"          , (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("I_CUST_ID"        , model.get("vrSrchCustId"));
                //session 정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));

                //dao                
                modelIns = (Map<String, Object>)dao.ifInOrdLocMapp(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));                

                // errCnt = Integer.parseInt(modelIns.get("O_MSG_CODE").toString());
                // errMsg = modelIns.get("O_MSG_NAME").toString();
            }

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
        	m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID    : getExcelDown2
     * Method 설명      : 엑셀 샘플 다운
     * 작성자                 : chsong
     * @param model
     * @return
     */
    @Override
    public Map<String, Object> getExcelDown2(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        List<Map<String, Object> > sample = new ArrayList<Map<String, Object>>();
        int colSize = Integer.parseInt(model.get("Col_Size").toString());
        int rowSize = Integer.parseInt(model.get("Row_Size").toString());
        
        for(int k = 0 ; k < rowSize ; k++){
        	if(k == 0){
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
                    sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
                    sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}else{
        		for(int i = 0 ; i < colSize ; i++){
        			Map<String, Object> sampleMap = new HashMap<String, Object>();
        			sampleMap.put(k+"_sampleCol", (String)model.get("Col_"+i));
        			sampleMap.put(k+"_sampleRow", (String)model.get(k+"_Row_"+i));
                    sample.add(sampleMap);
                }
        	}
        }
        Map<String, Object> sampleRow = new HashMap<String, Object>();
        sampleRow.put("rowSize", rowSize);
        
        map.put("LIST", getExceList2(sample, sampleRow));
        return map;
    }
    
    private GenericResultSet getExceList2(List<Map<String, Object>> list, Map<String, Object> model) {
		GenericResultSet wqrs = new GenericResultSet();
		int pageIndex = 1;
		int pageSize = 1;
		int pageTotal = 1;
		int pageBlank = (int) Math.ceil(pageTotal / (double) pageSize);
		int rowSize = Integer.parseInt(model.get("rowSize").toString());
		
		List sampleList = new ArrayList<Map<String, Object>>();
		
		for(int k = 0 ; k < rowSize ; k++){
			Map<String, Object> sample = new HashMap<String, Object>();
			for (Map<String, Object> sampleInfo : list) {	
				Object key = sampleInfo.get(k+"_sampleCol");
				if (key != null && StringUtils.isNotEmpty(key.toString())) {
					sample.put(key.toString(), sampleInfo.get(k+"_sampleRow"));
				}
			}
			sampleList.add(sample);
		}
		
		wqrs.setCpage(pageIndex);
		wqrs.setTpage(pageBlank);
		wqrs.setTotCnt(pageTotal);
		wqrs.setList(sampleList);
		return wqrs;
	}
    
	/**
     * 
     * 대체 Method ID   : autoBestLocSave
     * 대체 Method 설명    : 로케이션추천 자동
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	 @Override
    public Map<String, Object> autoBestLocSave(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){           
                String[] ordId   = new String[tmpCnt];
                String[] ordSeq   = new String[tmpCnt];
                
                for(int i = 0 ; i < tmpCnt ; i ++){
                	ordId[i]    = (String)model.get("I_ORD_ID"+i);  
                	ordSeq[i]    = (String)model.get("I_ORD_SEQ"+i);  
                	//log.info("AAAAAAAAAAAAAA:"+(String)model.get("I_ORD_SEQ"+i));
                }
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                
                modelIns.put("ordId", ordId);
                modelIns.put("ordSeq", ordSeq);

                //session 및 등록정보
                modelIns.put("LC_ID", (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
        		
                
                
                //dao                
                modelIns = (Map<String, Object>)dao.autoBestLocSave(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
	 
	 /**
	    * Method ID : saveUploadData
	    * Method 설명 : 엑셀업로드 저장
	    * 작성자 : kwt
	    * @param model
	    * @return
	    * @throws Exception
	    */
	   public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception {
	       Map<String, Object> m = new HashMap<String, Object>();
	       int errCnt = 0;
	       int insertCnt = (list != null)?list.size():0;
	           try{            	
	               dao.saveUploadData(model, list);
	               
	               m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
	               m.put("MSG_ORA", "");
	               m.put("errCnt", errCnt);
	               
	           } catch(Exception e){
	               throw e;
	           }
	       return m;
	   }
	   
   /**
     * Method ID   : asnOutOrd
     * Method 설명    : 
     * 작성자               : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> asnOutOrd(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        //int errCnt = 1;
        String errMsg = MessageResolver.getMessage("delete.error");
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());
            if(tmpCnt > 0){
                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("ORD_ID" , (String)model.get("ORD_ID"+0));
                //session 및 등록정보
                modelIns.put("WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                modelIns.put("USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                
                //dao                
                modelIns = (Map<String, Object>)dao.asnOutOrd(modelIns);
                ServiceUtil.isValidReturnCode("WMSOP020", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }  
}
