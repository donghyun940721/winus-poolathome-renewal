package com.logisall.winus.wmspk.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPK010Dao")
public class WMSPK010Dao extends SqlMapAbstractDAO{
    
    /**
     * Method ID  : list
     * Method 설명  : 세트상품 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspk010.list", model);
    }

    /**
     * Method ID  : genKitWork
     * Method 설명  : 임가공조립 저장
     * 작성자             : 기드온
     * @param model
     * @return
     */   
    public Object genKitWork(Map<String, Object> model){
        executeUpdate("wmsst070.pk_wmsst070.sp_gen_kit_work", model);
        return model;
    }
    
    /**
     * Method ID    : searchTpCol
     * Method 설명      : 화주별 컬럼
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object searchTpCol(Map<String, Object> model){
        executeUpdate("wmscm210.pk_wmscm210.sp_sel_tpcol", model);
        return model;
    }
    
    /**
     * Method ID    : saveExcelOrder
     * Method 설명      : 템플릿 주문 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object saveExcelOrder(Map<String, Object> model){
        executeUpdate("wmspk010.pk_wmsop030p.sp_insert_template", model);
        return model;
    }
}
