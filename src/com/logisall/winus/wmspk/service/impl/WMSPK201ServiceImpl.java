package com.logisall.winus.wmspk.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmspk.service.WMSPK201Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSPK201Service")
public class WMSPK201ServiceImpl extends AbstractServiceImpl implements WMSPK201Service {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPK201Dao")
    private WMSPK201Dao dao;

   /**
    * Method ID    : list
    * Method 설명      : 청구단가계약 조회
    * 작성자                 : chsong
    * @param   model
    * @return 
    * @throws Exception 
    */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
	/**
     * Method ID : saveExcelInfo
     * Method 설명 : 엑셀읽기저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> saveExcelInfo(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        
        try{
        	int insertCnt = (list != null)?list.size():0;
            if(insertCnt > 0){
            	String[] SEQ				= new String[insertCnt];
            	String[] HK					= new String[insertCnt];
            	String[] PART_NO			= new String[insertCnt];
            	String[] PART_NAME			= new String[insertCnt];
            	String[] P_PDC				= new String[insertCnt];
            	String[] D_PDC				= new String[insertCnt];
            	String[] SUPPLIER			= new String[insertCnt];
            	String[] SOURCE				= new String[insertCnt];
            	String[] SHIP				= new String[insertCnt];
            	String[] PO_NO				= new String[insertCnt];
            	String[] PO_LINE_NO			= new String[insertCnt];
            	String[] SHIPMENT_NO		= new String[insertCnt];
            	String[] SHIPMENT_LINE_NO	= new String[insertCnt];
            	String[] POI_NO				= new String[insertCnt];
            	String[] INVOICE_NO			= new String[insertCnt];
            	String[] CASE_NO			= new String[insertCnt];
            	String[] SHIPMENT_DATE		= new String[insertCnt];
            	String[] INVOICE_DATE		= new String[insertCnt];
            	String[] PO_DATE			= new String[insertCnt];
            	String[] DUE_DATE			= new String[insertCnt];
            	String[] RCV_DATE			= new String[insertCnt];
            	String[] PO_QTY				= new String[insertCnt];
            	String[] ASN_QTY			= new String[insertCnt];
            	String[] DI_QTY				= new String[insertCnt];
            	String[] RCV_QTY			= new String[insertCnt];
            	String[] U_PRICE			= new String[insertCnt];
            	String[] CUR				= new String[insertCnt];
            	String[] PO					= new String[insertCnt];
            	String[] RCV				= new String[insertCnt];
            	String[] RCV_NO				= new String[insertCnt];
            	String[] STATUS				= new String[insertCnt];
            	String[] BCR_FLAG			= new String[insertCnt];
            	String[] POD				= new String[insertCnt];
            	
                Map<String, Object> paramMap = null;
                for (int i=0;i<list.size();i++) {
            		paramMap = (Map)list.get(i);
            		SEQ[i]				= (String)paramMap.get("O_SEQ");
            		HK[i]				= (String)paramMap.get("O_HK");
            		PART_NO[i]			= (String)paramMap.get("O_PART_NO");
            		PART_NAME[i]		= (String)paramMap.get("O_PART_NAME");
            		P_PDC[i]			= (String)paramMap.get("O_P_PDC");
            		D_PDC[i]			= (String)paramMap.get("O_D_PDC");
            		SUPPLIER[i]			= (String)paramMap.get("O_SUPPLIER");
            		SOURCE[i]			= (String)paramMap.get("O_SOURCE");
            		SHIP[i]				= (String)paramMap.get("O_SHIP");
            		PO_NO[i]			= (String)paramMap.get("O_PO_NO");
            		PO_LINE_NO[i]		= (String)paramMap.get("O_PO_LINE_NO");
            		SHIPMENT_NO[i]		= (String)paramMap.get("O_SHIPMENT_NO");
            		SHIPMENT_LINE_NO[i] = (String)paramMap.get("O_SHIPMENT_LINE_NO");
            		POI_NO[i]			= (String)paramMap.get("O_POI_NO");
            		INVOICE_NO[i]		= (String)paramMap.get("O_INVOICE_NO");
            		CASE_NO[i]			= (String)paramMap.get("O_CASE_NO");
            		SHIPMENT_DATE[i]	= (String)paramMap.get("O_SHIPMENT_DATE");
            		INVOICE_DATE[i]		= (String)paramMap.get("O_INVOICE_DATE");
            		PO_DATE[i]			= (String)paramMap.get("O_PO_DATE");
            		DUE_DATE[i]			= (String)paramMap.get("O_DUE_DATE");
            		RCV_DATE[i]			= (String)paramMap.get("O_RCV_DATE");
            		PO_QTY[i]			= (String)paramMap.get("O_PO_QTY");
            		ASN_QTY[i]			= (String)paramMap.get("O_ASN_QTY");
            		DI_QTY[i]			= (String)paramMap.get("O_DI_QTY");
            		RCV_QTY[i]			= (String)paramMap.get("O_RCV_QTY");
            		U_PRICE[i]			= (String)paramMap.get("O_U_PRICE");
            		CUR[i]				= (String)paramMap.get("O_CUR");
            		PO[i]				= (String)paramMap.get("O_PO");
            		RCV[i]				= (String)paramMap.get("O_RCV");
            		RCV_NO[i]			= (String)paramMap.get("O_RCV_NO");
            		STATUS[i]			= (String)paramMap.get("O_STATUS");
            		BCR_FLAG[i]			= (String)paramMap.get("O_BCR_FLAG");
            		POD[i]				= (String)paramMap.get("O_POD");
            	}

                //프로시져에 보낼것들 다담는다
                Map<String, Object> modelIns = new HashMap<String, Object>();
                modelIns.put("I_SEQ"				, SEQ);
                modelIns.put("I_HK"					, HK);
                modelIns.put("I_PART_NO"			, PART_NO);
                modelIns.put("I_PART_NAME"			, PART_NAME);
                modelIns.put("I_P_PDC"				, P_PDC);
                modelIns.put("I_D_PDC"				, D_PDC);
                modelIns.put("I_SUPPLIER"			, SUPPLIER);
                modelIns.put("I_SOURCE"				, SOURCE);
                modelIns.put("I_SHIP"				, SHIP);
                modelIns.put("I_PO_NO"				, PO_NO);
                modelIns.put("I_PO_LINE_NO"			, PO_LINE_NO);
                modelIns.put("I_SHIPMENT_NO"		, SHIPMENT_NO);
                modelIns.put("I_SHIPMENT_LINE_NO"	, SHIPMENT_LINE_NO);
                modelIns.put("I_POI_NO"				, POI_NO);
                modelIns.put("I_INVOICE_NO"			, INVOICE_NO);
                modelIns.put("I_CASE_NO"			, CASE_NO);
                modelIns.put("I_SHIPMENT_DATE"		, SHIPMENT_DATE);
                modelIns.put("I_INVOICE_DATE"		, INVOICE_DATE);
                modelIns.put("I_PO_DATE"			, PO_DATE);
                modelIns.put("I_DUE_DATE"			, DUE_DATE);
                modelIns.put("I_RCV_DATE"			, RCV_DATE);
                modelIns.put("I_PO_QTY"				, PO_QTY);
                modelIns.put("I_ASN_QTY"			, ASN_QTY);
                modelIns.put("I_DI_QTY"				, DI_QTY);
                modelIns.put("I_RCV_QTY"			, RCV_QTY);
                modelIns.put("I_U_PRICE"			, U_PRICE);
                modelIns.put("I_CUR"				, CUR);
                modelIns.put("I_PO"					, PO);
                modelIns.put("I_RCV"				, RCV);
                modelIns.put("I_RCV_NO"				, RCV_NO);
                modelIns.put("I_STATUS"				, STATUS);
                modelIns.put("I_BCR_FLAG"			, BCR_FLAG);
                modelIns.put("I_POD"				, POD);
                
                //session 정보
                modelIns.put("I_LC_ID"	, (String)model.get(ConstantIF.SS_SVC_NO));
                modelIns.put("I_USER_NO", (String)model.get(ConstantIF.SS_USER_NO));
                modelIns.put("I_WORK_IP", (String)model.get(ConstantIF.SS_CLIENT_IP));
                
                //dao
                modelIns = (Map<String, Object>)dao.saveOrder(modelIns);
                ServiceUtil.isValidReturnCode("WMSPK201", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));
            }
        	model.put("LC_ID"    , model.get(ConstantIF.SS_SVC_NO));
			model.put("REG_NO"   , model.get(ConstantIF.SS_USER_NO));
			
            m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
            m.put("MSG_ORA", "");
            m.put("errCnt", errCnt);
            
        } catch(Exception e){
            m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID  : excelDown
     * Method 설명    : 엑셀다운로드
     * 작성자                 : chsong
     * @param   model
     * @return 
     * @throws Exception 
     */
     @Override
     public Map<String, Object> excelDown(Map<String, Object> model) throws Exception {
         Map<String, Object> map = new HashMap<String, Object>();
         model.put("pageIndex", "1");
         model.put("pageSize", "60000");
         map.put("LIST", dao.list(model));
         return map;
     }
}
