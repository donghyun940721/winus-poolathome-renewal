package com.logisall.winus.wmspk.service;

import java.util.List;
import java.util.Map;

public interface WMSPK010Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> genKitWork(Map<String, Object> model) throws Exception;
    public Map<String, Object> getResult(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveExcelOrderJava(Map<String, Object> model, Map<String, Object> model2) throws Exception;
}
