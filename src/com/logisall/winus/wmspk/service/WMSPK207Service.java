package com.logisall.winus.wmspk.service;

import java.util.Map;


public interface WMSPK207Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> listPopSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> detailPop(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateComplete(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel2(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel3(Map<String, Object> model) throws Exception;
    public Map<String, Object> getPreViewConf(Map<String, Object> model) throws Exception;//PDF 생성
    public Map<String, Object> getPreViewConfSub(Map<String, Object> model) throws Exception;//PDF 생성
    public Map<String, Object> saveOrderKit(Map<String, Object> model) throws Exception;
    public Map<String, Object> workList(Map<String, Object> model) throws Exception;
    public Map<String, Object> lableDataSearch(Map<String, Object> model) throws Exception;
    public Map<String, Object> workListDataSearch(Map<String, Object> model) throws Exception;
    public Map<String, Object> getWorkData(Map<String, Object> model) throws Exception;
}
