package com.logisall.winus.wmslb.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmslb.service.WMSLB020Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSLB020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSLB020Service")
	private WMSLB020Service service;

 
    /**
     * Method ID   : wmsmo020
     * Method 설명    : RFID입고 화면
     * 작성자               : chSong
     * @param model
     * @return
     */
    @RequestMapping("/WINUS/WMSLB020.action")
    public ModelAndView wmslb020(Map<String, Object> model){
        return new ModelAndView("winus/wmslb/WMSLB020");        // 맨 앞에 / 없음에 주의, .vm 없음에 주의
    }
       
    /*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSLB020/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSLB020/deletedList.action")
	public ModelAndView deletedList(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.deletedList(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSLB020/deleteLabel.action")
	public ModelAndView deleteLabel(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.deleteLabel(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
	/*-
	 * Method ID    : list
	 * Method 설명      : 고객관리
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSLB020/reviveLabel.action")
	public ModelAndView reviveLabel(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.reviveLabel(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to list :", e);
			}
		}
		return mav;
	}
}
