package com.logisall.winus.wmslb.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.wmslb.service.WMSLB010Service;



@Service("WMSLB010Service")
public class WMSLB010ServiceImpl implements WMSLB010Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSLB010Dao")
    private WMSLB010Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 입고RFID관리  조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        int result = dao.check(model);
        if(result > 0){
        	map.put("errCnt", 1);
        }else{
        	dao.save(model);
        	map.put("errCnt", 0);
        }
        
        return map; 
    }
    
    @Override
    public Map<String, Object> expirationDate(Map<String, Object> model)
    		throws Exception {
    	Map<String, Object> map = dao.expirationDate(model);
    	//map.put("expirationDate", expirationDate);
    	return map;
    }
}
