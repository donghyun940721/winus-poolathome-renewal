package com.logisall.winus.wmslb.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;


@Repository("WMSLB020Dao")
public class WMSLB020Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 입고RFID관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
    	GenericResultSet genericResultSet = new GenericResultSet();
    	genericResultSet.setList(list("wmslb020.list", model));
    	return genericResultSet;
    }
    
    /**
     * Method ID    : updateLabel
     * Method 설명      : 입고RFID관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public int updateLabel(Map<String, Object> model) {
    	return executeUpdate("wmslb020.update", model);
    }
    

}
