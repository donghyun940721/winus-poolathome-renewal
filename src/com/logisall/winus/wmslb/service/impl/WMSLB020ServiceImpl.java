package com.logisall.winus.wmslb.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmslb.service.WMSLB020Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;



@Service("WMSLB020Service")
public class WMSLB020ServiceImpl implements WMSLB020Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSLB020Dao")
    private WMSLB020Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 입고RFID관리  조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("LIST", dao.list(model));
        return map; 
    }
    
    /**
     * Method ID   : deletedList
     * Method 설명    : 입고RFID관리  조회
     * 작성자                      : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> deletedList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("delYn", "Y");
        map.put("LIST", dao.list(model));
        return map; 
    }
    
    /**
     * Method ID : list
     * Method 설명 : 그리드 설정 저장
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteLabel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("custId", model.get("custId" + i));
	             modelDt.put("custLotNo", model.get("custLotNo" + i));
	             
	             modelDt.put("delYn", "Y");
	             modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
	             modelDt.put("REG_NO", model.get(ConstantIF.SS_USER_NO));
	             
	             dao.updateLabel(modelDt);
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("update.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    /**
     * Method ID : list
     * Method 설명 : 그리드 설정 저장
     * 작성자 : 김기하
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> reviveLabel(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		int errCnt = 0;
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			modelDt.put("custId", model.get("custId" + i));
    			modelDt.put("custLotNo", model.get("custLotNo" + i));
    			
    			modelDt.put("delYn", "N");
    			modelDt.put("SS_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
    			modelDt.put("REG_NO", model.get(ConstantIF.SS_USER_NO));
    			
    			dao.updateLabel(modelDt);
    		}
    		
    		map.put("errCnt", errCnt);
    		map.put("MSG", MessageResolver.getMessage("update.success"));
    		
    	} catch (Exception e) {
    		throw e;
    		
    	}
    	
    	return map;
    }
    
}
