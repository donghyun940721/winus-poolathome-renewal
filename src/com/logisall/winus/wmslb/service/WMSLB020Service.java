package com.logisall.winus.wmslb.service;

import java.util.Map;

public interface WMSLB020Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> deletedList(Map<String, Object> model) throws Exception;
	public Map<String, Object> deleteLabel(Map<String, Object> model) throws Exception;
	public Map<String, Object> reviveLabel(Map<String, Object> model) throws Exception;
}
