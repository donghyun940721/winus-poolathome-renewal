package com.logisall.winus.wmspm.service;

import java.util.List;
import java.util.Map;

public interface WMSPM090Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> listItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveItem(Map<String, Object> model) throws Exception;
    public Map<String, Object> listUom(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUom(Map<String, Object> model) throws Exception;
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUom2(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> overapCheck(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveItemImg(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertValidate(Map<String, Object> model) throws Exception;
}
