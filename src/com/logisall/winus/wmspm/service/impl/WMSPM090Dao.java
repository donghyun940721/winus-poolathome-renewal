package com.logisall.winus.wmspm.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPM090Dao")
public class WMSPM090Dao extends SqlMapAbstractDAO {

    /**
     * Method ID : selectItemGrp Method 설명 : LCID마다 다른 ITEMGRP selectBox 값 조회
     * 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object selectItemGrp(Map<String, Object> model) {
	return executeQueryForList("wmsms094.selectItemGrp", model);
    }

    /**
     * Method ID : selectUom Method 설명 : 신규생성시 LCID마다 다른 UOM 필요데이터 조회 작성자 :
     * chsong
     * 
     * @param model
     * @return
     */
    public Object selectUom(Map<String, Object> model) {
	return executeQueryForList("wmsms100.selectUom", model);
    }

    /**
     * Method ID : listItem Method 설명 : 상품정보 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listItem(Map<String, Object> model) {
	return executeQueryPageWq("wmsms091.listItem", model);
    }

    /**
     * Method ID : saveItem Method 설명 : 상품정보 저장 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object saveItem(Map<String, Object> model) {
	return executeUpdate("wmspm090.pk_wmsms090e.sp_save_item", model);
    }

    /**
     * Method ID : deleteItem Method 설명 : 상품정보삭제시 wmsms090 삭제 (실질적은 delyn -> y 로
     * 변경) 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object deleteItem(Map<String, Object> model) {
	return executeUpdate("wmspm090.delete", model);
    }

    /**
     * Method ID : deleteRitem Method 설명 : 상품정보삭제시 wmsms091 삭제 (실질적은 delyn -> y
     * 로 변경) 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object deleteRitem(Map<String, Object> model) {
	return executeUpdate("wmsms091.delete", model);
    }

    /**
     * Method ID : listUom Method 설명 : UOM환산이력 조회 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public GenericResultSet listUom(Map<String, Object> model) {
	return executeQueryPageWq("wmsms101.listUom", model);
    }

    /**
     * Method ID : insertUom Method 설명 : UOM환산이력 등록 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object insertUom(Map<String, Object> model) {
	return executeInsert("wmsms101.insert", model);
    }

    /**
     * Method ID : updateUom Method 설명 : UOM환산이력 수정 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object updateUom(Map<String, Object> model) {
	return executeUpdate("wmsms101.update", model);
    }

    /**
     * Method ID : deleteUom Method 설명 : UOM환산이력 삭제 작성자 : chsong
     * 
     * @param model
     * @return
     */
    public Object deleteUom(Map<String, Object> model) {
	return executeUpdate("wmsms101.delete", model);
    }

    /**
     * Method ID : insertUom2 Method 설명 : UOM환산이력 등록 Ritem을 db쿼리에서 찾아서 쓰는방법 작성자
     * : chsong
     * 
     * @param model
     * @return
     */
    public Object insertUom2(Map<String, Object> model) {
	return executeInsert("wmsms101.insert2", model);
    }

    /**
     * Method ID	: saveUploadData
     * Method 설명	: 
     * 작성자			: chsong
     * @param   model
     * @return
     */
    public Object saveUploadData(Map<String, Object> model){
        executeUpdate("wmspm090.pk_wmsms090e.sp_save_packaging_item", model);
        return model;
    }
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }	    
    
    /**
     * Method ID  : overapCheck
     * 작성자             : wdy
     * @param model
     * @return
     */
    public Object overapCheck(Map<String, Object> model){
    	return executeQueryForList("wmspm090.overapCheck", model);
    }
    
    /**
     * Method ID : fileUpload
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object fileUpload(Map<String, Object> model) {
        return executeInsert("tmsys900.fileInsert", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertInfo(Map<String, Object> model) {
        return executeInsert("wmspm090.itemImgInfoSave", model);
    }
    
    /**
     * Method ID  : insertValidate
     * Method 설명  : 
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object insertValidate(Map<String, Object> model){
        return executeQueryForList("wmspm090.insertValidate", model);
    }
}
