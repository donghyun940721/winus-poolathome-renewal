package com.logisall.winus.wmspm.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmspm.service.WMSPM091Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSPM091Service")
public class WMSPM091ServiceImpl implements WMSPM091Service{
    protected Log log = LogFactory.getLog(this.getClass());
    
    @Resource(name = "WMSPM091Dao")
    private WMSPM091Dao dao;
    
    /**
	 * 대체 Method ID : selectData 대체 Method 설명 : 상품 목록 필요 데이타셋 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@Override
	public Map<String, Object> selectData(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("ITEMGRP", dao.selectItemGrp(model));
		return map;
	}
    /**
     * 
     * 대체 Method ID    : list
     * 대체 Method 설명      : 상품군 조회
     * 작성자                        : chsong 
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
      
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 상품군 저장
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        try{
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                Map<String, Object> modelDt = new HashMap<String, Object>();
                modelDt.put("SS_USER_NO"    	, model.get(ConstantIF.SS_USER_NO));    
                modelDt.put("SS_CLIENT_IP"  	, model.get(ConstantIF.SS_CLIENT_IP));    
                modelDt.put("SS_SVC_NO"     	, model.get(ConstantIF.SS_SVC_NO));     //LC_ID
                
                modelDt.put("selectIds"     	, model.get("selectIds"));
                modelDt.put("ST_GUBUN"      	, model.get("ST_GUBUN"+i));
                
                modelDt.put("PACKING_RITEM_ID"	, model.get("PACKING_RITEM_ID"+i));        //INSERT -> SEQ   
                modelDt.put("PACKING_STEP_SEQ"  , model.get("PACKING_STEP_SEQ"+i));           
                modelDt.put("PRINT_COUNT"   	, model.get("PRINT_COUNT"+i));           
                modelDt.put("PRINT_QTY"			, model.get("PRINT_QTY"+i));         
                modelDt.put("ORD_TYPE"			, model.get("ORD_TYPE"+i));
                modelDt.put("COST"				, model.get("COST"+i));
                modelDt.put("CURRENCY_NAME"		, model.get("CURRENCY_NAME"+i));
                
                if("INSERT".equals(model.get("ST_GUBUN"+i))){
                    dao.insert(modelDt);
                }else if("UPDATE".equals(model.get("ST_GUBUN"+i))){
                    dao.update(modelDt);
                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
                    dao.delete(modelDt);
                }else{
                    errCnt++;
                    m.put("errCnt", errCnt);
                    throw new Exception(MessageResolver.getMessage("save.error"));
                }
            }
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
                
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 대체 Method ID     : listExcel
     * 대체 Method 설명        : 상품군 엑셀.
     * 작성자                          : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        
        return map;
    }    
}
