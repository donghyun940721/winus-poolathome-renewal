package com.logisall.winus.wmspm.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPM096Dao")
public class WMSPM096Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectZone
     * Method 설명  : Zone 데이터셋
     * 작성자             : 기드온
     * @param model
     * @return
     */
    public Object selectZone(Map<String, Object> model){
        return executeQueryForList("wmsms081.selectZone", model);
    }
    
    /**
     * Method ID    : insert
     * Method 설명      : 물류용기관리 등록
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmspm096.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 물류용기관리 수정
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmspm096.update", model);
    }  
    
    /**
     * Method ID    : delete
     * Method 설명      : 물류용기관리 삭제
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmspm096.delete", model);
    }  
        
    /**
     * Method ID  : list
     * Method 설명  : 물류용기관리 조회
     * 작성자             : 기드온
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspm096.list", model);
    }
    
    
    /**
     * Method ID    : insert
     * Method 설명      : 상품군 등록 (물류용기군 등록시 상품군화)
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object insertIg(Map<String, Object> model) {
        return executeInsert("wmsms094.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 상품군 수정 (물류용기군 수정시 상품군도 수정??)
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object updateIg(Map<String, Object> model) {
        return executeUpdate("wmsms094.update", model);
    }  
    
    /**
     * Method ID    : delete
     * Method 설명      : 상품군 삭제 (물류용기 삭제시 상품군삭제)
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object deleteIg(Map<String, Object> model) {
        return executeUpdate("wmsms094.delete", model);
    }  
      
    /**
     * Method ID : saveUploadData
     * Method 설명 : 엑셀파일등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void saveUploadData(Map<String, Object> model, List list) throws Exception {
    		
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
    		for (int i=0;i<list.size();i++) {
    			paramMap = (Map)list.get(i);
    			    			
    			if ( paramMap.get("ITEM_GRP_CD") != null && StringUtils.isNotEmpty( (String)paramMap.get("ITEM_GRP_CD")) ) {
    				
    				paramMap.put("ITEM_GRP_TYPE", "P");
					String itemGrpId = (String) sqlMapClient.insert("wmsms094.insert_many", paramMap);
					
					paramMap.put("ITEM_GRP_ID", itemGrpId);
					paramMap.put("POOL_GRP_CD", paramMap.get("ITEM_GRP_CD"));           
					paramMap.put("POOL_GRP_NM", paramMap.get("ITEM_GRP_NM"));
					paramMap.put("IN_ZONE_ID", paramMap.get("IN_ZONE_ID"));			
					
					sqlMapClient.insert("wmspm096.insert_many", paramMap);	    			
	    			
    			}
    		}
    		sqlMapClient.endTransaction();
    		
		} catch(Exception e) {
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
    }     
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }    
   
}
