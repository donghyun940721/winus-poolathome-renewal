package com.logisall.winus.wmspm.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSPM093Dao")
public class WMSPM093Dao extends SqlMapAbstractDAO{
   
    /**
     * Method ID  : selectZone
     * Method 설명  : Zone 데이터셋
     * 작성자             : chsong
     * @param model
     * @return
     */
    public Object selectZone(Map<String, Object> model){
        return executeQueryForList("wmsms081.selectZone", model);
    }
    
    /**
     * Method ID  : list
     * Method 설명  : 상품군 조회
     * 작성자             : chsong
     * @param   model
     * @return  GenericResultSet
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmspm093.list", model);
    }
      
    /**
     * Method ID    : insert
     * Method 설명      : 상품군 등록
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmspm093.insert", model);
    }    
    
    /**
     * Method ID    : update
     * Method 설명      : 상품군 수정
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object update(Map<String, Object> model) {
        return executeUpdate("wmspm093.update", model);
    }  
    
    /**
     * Method ID    : delete
     * Method 설명      : 상품군 삭제
     * 작성자                 : chsong
     * @param   model
     * @return  Object
     */
    public Object delete(Map<String, Object> model) {
        return executeUpdate("wmspm093.delete", model);
    }
    
    /**
     * Method ID : insertCsvItemGroup
     * Method 설명 : 상품군 대용량등록시
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public void insertCsvItemGroup(Map<String, Object> model, List list) throws Exception {
    		
    		SqlMapClient sqlMapClient = getSqlMapClient();
    		try {
    			sqlMapClient.startTransaction();
    			Map<String, Object> paramMap = null;
	    		for (int i=0;i<list.size();i++) {
	    			paramMap = (Map)list.get(i);
	    			    			
	    			if ( (paramMap.get("ITEM_GRP_NM") != null && StringUtils.isNotEmpty( paramMap.get("ITEM_GRP_NM").toString())) ) {
	    				paramMap.put("SS_USER_NO", 		model.get("SS_USER_NO"));
		    			paramMap.put("SS_SVC_NO", 		model.get("SS_SVC_NO"));
		    			paramMap.put("SS_CLIENT_IP", 	model.get("SS_CLIENT_IP"));
		    			
		    			sqlMapClient.insert("wmspm093.insert", paramMap);

	    			}
	    		}
	    		sqlMapClient.endTransaction();
	    		
    		} catch(Exception e) {
    			e.printStackTrace();
    			throw e;
    			
    		} finally {
    			if (sqlMapClient != null) {
    				sqlMapClient.endTransaction();
    			}
    		}
    }    
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsms030.selectExistData", model);
    }	    
}
