package com.logisall.winus.wmspm.service;

import java.util.List;
import java.util.Map;

public interface WMSPM092Service {
    public Map<String, Object> selectData(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveSub(Map<String, Object> model) throws Exception;
    public Map<String, Object> listSubExcel(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveUploadData(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> getItemSubGrid(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectData2(Map<String, Object> model) throws Exception;
    public Map<String, Object> itemPopupList(Map<String, Object> model) throws Exception;
}
