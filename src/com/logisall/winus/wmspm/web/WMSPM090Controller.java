package com.logisall.winus.wmspm.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmspm.service.WMSPM090Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSPM090Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSPM090Service")
    private WMSPM090Service service;
    
	static final String[] COLUMN_NAME_WMSPM090 = {
			"O_ITEM_KOR_NM"
		  , "O_ITEM_ENG_NM"
		  , "O_ITEM_BAR_CD"
		  , "O_ITEM_CODE"
		  , "O_ITEM_WGT"
		  
		  , "O_ITEM_NM"
		  , "O_SIZE_W"
		  , "O_SIZE_H"
		  , "O_SIZE_L"
		  , "O_WORKING_TIME"
	};

	/**
	 * Method ID : wmspm090 Method 설명 : 상품정보관리 화면 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSPM090.action")
	public ModelAndView wmspm090(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmspm/WMSPM090", service.selectData(model));
	}

	/**
	 * Method ID : listItem Method 설명 : 상품정보관리 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/list.action")
	public ModelAndView listItem(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listItem(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list goods :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : saveItem Method 설명 : 상품정보관리 저장수정 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/save.action")
	public ModelAndView saveItem(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveItem(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save goods info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : listUom Method 설명 : UOM환산이력 조회 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/listUom.action")
	public ModelAndView listUom(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listUom(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list uom :", e);
			}
		}
		return mav;
	}

	/**
	 * Method ID : saveSub Method 설명 : UOM환산이력 저장 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/saveSub.action")
	public ModelAndView saveSub(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveUom(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save sub :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : saveSub2 Method 설명 : UOM환산이력 저장2 Ritem_cd를 db에서 찾아서 처리 작성자 :
	 * chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/saveSub2.action")
	public ModelAndView saveSub2(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.saveUom2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save sub2 :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/**
	 * Method ID : listExcel Method 설명 : 엑셀다운로드 작성자 : chsong
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = service.listExcel(model);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");
			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to download Excel file :", e);
			}
		}
	}
	
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                                   {MessageResolver.getText("상품군")      , "0", "0", "0", "0", "200"},
                                   {MessageResolver.getText("상품코드")     , "1", "1", "0", "0", "300"},
                                   {MessageResolver.getText("상품명")      , "2", "2", "0", "0", "400"},
                                   {"UOM"                                 , "3", "3", "0", "0", "100"},
                                   {MessageResolver.getText("입고존")      , "4", "4", "0", "0", "100"},
                                   {MessageResolver.getText("적정재고")     , "5", "5", "0", "0", "100"},
                                   {MessageResolver.getText("유통기간")     , "6", "6", "0", "0", "100"}
                                  };
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                                    {"ITEM_GRP_NM"   , "S"},
                                    {"ITEM_CODE"     , "S"},
                                    {"ITEM_KOR_NM"   , "S"},
                                    {"UOM_YN"        , "S"},
                                    {"IN_ZONE_NM"    , "S"},
                                    {"PROP_QTY"      , "S"},
                                    {"EXPIRY_DATE"   , "S"}
                                   }; 
            
			// 파일명
			String fileName = MessageResolver.getText("상품정보관리");
			// 시트명
			String sheetName = "Sheet1";
			// 구분 병합 여부 (0 : 구분0번째 / 1 : 구분 1번째 / A : 구분 0, 1 번째 / N : 병합 없음 )
			String marCk = "N";
			// ComUtil코드
			String etc = "";

			ExcelWriter wr = new ExcelWriter();
			wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail download Excel file...", e);
			}
		}
	}

	/**
	 * Method ID : wmspm0902 Method 설명 : 상품정보관리 화면 sample 작성자 : chSong
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WINUS/WMSPM0902.action")
	public ModelAndView wmspm0902(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmspm/WMSPM0902", service.selectData(model));
	}

	/**
	 * Method ID : WMSPM090E4 Method 설명 : 엑셀업로드 화면 작성자 : kwt
	 * 
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping("/WMSPM090E4.action")
	public ModelAndView WMSPM090E4(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
		return new ModelAndView("winus/wmspm/WMSPM090E4");
	}

	/**
	 * Method ID : uploadInfo Method 설명 : Excel 파일 읽기 작성자 : kwt
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/uploadInfo.action")
	public ModelAndView uploadInfo(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");

			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSPM090, 0, startRow, 10000, 0);
			m = service.saveUploadData(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}     

	/**
	 * Method ID : 기존 등록 ITEM_CODE 중복 체크
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/overapCheck.action")
	public ModelAndView overapCheck(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.overapCheck(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : save
	 * Method 설명 : 통합 HelpDesk 저장
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSPM090/saveItemImg.action")
	public ModelAndView saveItemImg(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveItemImg(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/WMSPM090/insertValidate.action")
	public ModelAndView insertValidate(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.insertValidate(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
