package com.logisall.winus.frm.exception;

import com.m2m.jdfw5x.message.Msg;

public class BizException extends Exception {

	private static final long serialVersionUID = 606632553491930768L;
	protected Msg msg;
	private String msgCode;
	private Object obj;

	public Msg getMsg() {
		return msg;
	}

	public String getMsgCode() {
		return msgCode;
	}

	public Object getObj() {
		return obj;
	}

	public BizException() {
	}

	public BizException(String message) {
		super(message);
		msgCode = message;
	}

	public BizException(String message, Throwable cause) {
		super(message, cause);
		msgCode = message;
	}

	public BizException(Throwable cause) {
		super(cause);
	}

	public BizException(String message, Object obj) {
		super(message);
		this.obj = obj;
		msgCode = message;
	}

	public BizException(Msg msg) {
		super(msg.getMessage());
		this.msg = msg;
	}

	public BizException(Msg msg, Throwable cause) {
		super(msg.getMessage());
		this.msg = msg;
		initCause(cause);
	}

	public BizException(Msg msg, Object obj) {
		super(msg.getMessage());
		this.msg = msg;
		this.obj = obj;
	}

	public String toString() {
		if (msg == null)
			return super.toString();
		else
			return msg.getMessage();
	}

}