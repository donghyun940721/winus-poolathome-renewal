package com.logisall.winus.frm.exception;

import com.m2m.jdfw5x.message.Msg;

public class UserException extends BizException {

	private static final long serialVersionUID = -7763230769332465817L;
	
	public UserException(Msg msg) {
		super(msg);
	}
	
}
