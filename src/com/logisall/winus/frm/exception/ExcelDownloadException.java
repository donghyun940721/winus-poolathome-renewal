package com.logisall.winus.frm.exception;

import com.m2m.jdfw5x.message.Msg;

/**
 * Excel Download과정에서 오류가 발생한 경우 이용한다.
 * @author kwt
 *
 */
public class ExcelDownloadException extends BizException {
	
    private static final long serialVersionUID = 4220633208016881755L;

    public ExcelDownloadException(Msg msg, Throwable throwable) {
        super(msg, throwable);
    }

    public ExcelDownloadException(Msg msg) {
        super(msg);
    }

}
