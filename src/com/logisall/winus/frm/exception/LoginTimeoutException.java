package com.logisall.winus.frm.exception;

import com.m2m.jdfw5x.message.Message;


/**
 * @author kwt
 * CommonFilter에서 로그인 타임아웃을 판단하여 throws 한다.
 */
public class LoginTimeoutException extends UserException {

	private static final long serialVersionUID = 9051290468137959437L;
	
	public LoginTimeoutException() {
		super(Message.getMsg("login.status.logout.limit"));
	}
	
}
