package com.logisall.winus.frm.exception;

import com.m2m.jdfw5x.message.Message;

/**
 * @author kwt
 * 중복 사용자 발생
 */
public class AuthorityException extends UserException {

	private static final long serialVersionUID = -2682886102225272008L;
	
	public AuthorityException() {
		super(Message.getMsg("login.status.logout"));
	}
	
	
}
