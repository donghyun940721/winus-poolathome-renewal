
package com.logisall.winus.frm.exception;


public class UtilException extends RuntimeException {

    private static final long serialVersionUID = 3256739848870050276L;

    public UtilException(String exMsg) {
        super(exMsg);
    }

    public UtilException(String exMsg, Throwable throwable) {
        super(exMsg, throwable);
    }

}
