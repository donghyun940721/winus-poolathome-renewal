
package com.logisall.winus.frm.interceptor;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.servlet.support.RequestContextUtils;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.tmsys.service.TMSYS030Service;
import com.m2m.jdfw5x.util.SessionUtil;

@Controller("loginInterceptor")
public class LoginInterceptor extends HandlerInterceptorAdapter {

    protected Log log = LogFactory.getLog(LoginInterceptor.class);

    @Resource(name = "TMSYS030Service")
    private TMSYS030Service service;

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
            Object handler) throws Exception {
        String requestURI = (String)request.getRequestURI();
        if (log.isDebugEnabled()) {
            log.debug("@ [ LoginInterceptor ] @ SERVER REQUEST : " + requestURI);
        }
        String strCty = request.getContentType();
        String userid = (String)request.getSession().getAttribute(ConstantIF.SS_USER_ID);
        Date logindt = (Date)request.getSession().getAttribute(ConstantIF.SS_LOGIN_DATE);
        Integer sessiont = (Integer)request.getSession().getAttribute(ConstantIF.SS_LOGIN_SESSION_TIME);
        
        if (log.isDebugEnabled()) {
            log.debug("@ [ LoginInterceptor ] @ userid : " + userid + ", logindt :" + logindt + ", sessiont :" + sessiont);
        }
        Map<String, Object> authMap = null;
        if (StringUtils.isEmpty(userid) || logindt == null) {
            if (log.isInfoEnabled()) {
                log.info("@ [ LoginInterceptor ] @ \t\t ===> NOT LOGIN ...");
            }
            // if (str_cty != null
            // && (str_cty.equals("application/json") || str_cty
            // .equals("application/x-www-form-urlencoded"))) {
            // processNotLoginJson(request, response, true);
            // } else {
            // processNotLogin(request, response, true);
            // }
            processNotLogin(request, response, true);

            return false;

        } else if (sessiont == null) {
            if (log.isDebugEnabled()) {
                log.debug("@ [ LoginInterceptor ] @ \t\t ===> SESSION EXPIRED ...");
            }
            if (strCty != null
                    && (strCty.equals("application/json") || strCty
                            .equals("application/x-www-form-urlencoded"))) {
                processNotLoginJson(request, response, false);
            } else {
                processNotLogin(request, response, false);
            }
            return false;
        } else {
        	if (log.isDebugEnabled()) {
        		log.debug("@ [ LoginInterceptor ] @ \t\t ===> LOAD USER Authority...");
        	}
            Map<String, Object> model = new HashMap<String, Object>();
            // 로그인시 첫페이지 권한설정...
            model.put("SS_SVC_NO", SessionUtil.getStringValue(request, ConstantIF.SS_SVC_NO));
            model.put("USER_NO", SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
            model.put("USER_ID", SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
            model.put("WORK_IP", SessionUtil.getStringValue(request, ConstantIF.SS_CLIENT_IP));
            model.put("Session_ID", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_SESSION_ID)));
            model.put("AUTH_CD", SessionUtil.getStringValue(request, ConstantIF.SS_USER_TYPE));
            model.put("SS_LC_CHANGE_YN", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_LC_CHANGE_YN)));
            model.put("SS_ITEM_FIX_YN", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_ITEM_FIX_YN)));
            model.put("SS_ITEM_GRP_ID", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_ITEM_GRP_ID)));
            model.put("SS_MULTI_USE_GB", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_MULTI_USE_GB)));
            model.put("SS_SUB_PASSWORD", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_SUB_PASSWORD)));
            model.put("URL", requestURI);
            String blankUrl = "/main/blank.action";
            // winus 로고 클릭했을때는 프로그램 권한 조회 필요가없음
            if(!blankUrl.equals(requestURI)){
                Map programInfo = service.selectProgramInfo(model);
	            if (programInfo == null || programInfo.get("PROGRAM_ID") == null ) {
	                // programId 없을때 processNotLogin 으로 보냄
	                processNotLogin(request, response, false);
	            }
	            if(model != null) {
	            	model.put("PROGRAM_ID", programInfo.get("PROGRAM_ID"));
		            model.put("P_MENU_ID", programInfo.get("TOP_MENU_CD"));
		            authMap = service.getRoll(model);
		            
		            /*
			            if (log.isDebugEnabled()) {
			            	log.debug("@ [ LoginInterceptor ] @ \t\t ===>AUTH DATA FROM DB..." + authMap);
			            }
		            */
		            SessionUtil.setValue(request, requestURI, authMap);
	            }
	
	            // }
	            if (authMap != null) {
	                request.setAttribute("windowRoll", authMap);
	            }
            }
        }

        String dialect = (String)request.getSession().getAttribute(ConstantIF.SS_USER_LOCALE);
        if (!StringUtils.isEmpty(dialect)) {
            RequestContextUtils.getLocaleResolver(request).setLocale(request, response,
                    new Locale(dialect));
            // request.getSession().setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME,
            // new Locale(dialect));
        }

        int sessiontime = sessiont.intValue();
        Date currdt = new Date();
        boolean b = currdt.before(DateUtils.addSeconds(logindt, sessiontime));
        if (b) {
            request.getSession().setAttribute(ConstantIF.SS_LOGIN_DATE, currdt);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("@ [ LoginInterceptor ] @ \t\t ===> SESSION EXPIRED 2 ...");
            }
            if (strCty != null
                    && (strCty.equals("application/json") || strCty
                            .equals("application/x-www-form-urlencoded"))) {
                processNotLoginJson(request, response, false);
            } else {
                processNotLogin(request, response, false);
            }
            return false;
        }
        return true;
    }

    /*
	    private void processWebSecurity(final HttpServletRequest request,
	            final HttpServletResponse response) throws Exception {
	        request.getSession().setAttribute("INVALID_LOGIN_MESSAGE", "Web Security Error");
	        response.sendRedirect("/jsp/common/invalidLogin.jsp");
	    }
    */

    private void processNotLogin(HttpServletRequest request, HttpServletResponse response, boolean b)
            throws Exception {

    	if (log.isDebugEnabled()) {
    		log.debug("@ [ LoginInterceptor ] @ \t\t ===> processNotLogin START ...");
    	}

        if (b) {
            request.getSession()
                    .setAttribute("INVALID_LOGIN_MESSAGE", "Please, you need to log in");
        } else {
            request.getSession().setAttribute("INVALID_LOGIN_MESSAGE",
                    "Your login time's expired, please log in again");
        }
        response.sendRedirect("/index.html");
    }

    /**
     * Json용 Response 처리
     * 
     * @param request
     * @param response
     * @param b
     * @throws Exception
     */
    private void processNotLoginJson(HttpServletRequest request, HttpServletResponse response,
            boolean b) throws Exception {
        if (b) {
            request.getSession()
                    .setAttribute("INVALID_LOGIN_MESSAGE", "Please, you need to log in");
        } else {
            request.getSession().setAttribute("INVALID_LOGIN_MESSAGE",
                    "Your login time's expired, please log in again");
        }

        // 에러 코드만 넘길때
        response.sendError(9191);

        // 메시지를 Text방식으로 넘길때
        // response.setStatus(9191);
        // response.setContentType("application/text");
        // response.setCharacterEncoding("UTF-8");
        // response.getWriter().write(msAccessor.getMessage("com.msg.notLogin"));

    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response,
            Object handler, ModelAndView modelAndView) throws Exception {

        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response,
            Object handler, Exception ex) throws Exception {
        if (log.isDebugEnabled()) {
            log.debug("@ [ LoginInterceptor ] @ SERVER RESPONSE : " + request.getRequestURI());
        }
        super.afterCompletion(request, response, handler, ex);
    }

}
