
package com.logisall.winus.frm.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.logisall.winus.frm.common.util.ConstantIF;

@Controller("loginInterceptor2")
public class LoginInterceptor2 extends HandlerInterceptorAdapter {

    protected Log log = LogFactory.getLog(LoginInterceptor.class);


    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
            Object handler) throws Exception {

        String userid = (String)request.getSession().getAttribute(ConstantIF.SS_USER_ID);

        if (userid == null) {
           response.sendError(404);
           return false; 
       }
        return true;
    }
}
