package com.logisall.winus.frm.advisor;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.StopWatch;

public class AroundMgrAdvice implements MethodInterceptor {
	
	protected Log log = LogFactory.getLog(AroundMgrAdvice.class);
	
	@Override
	public Object invoke(MethodInvocation invocation) throws Throwable {
	    // log.info("Before: invocation=[" + invocation + "]");
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        
        Object rval = invocation.proceed();
        
        stopWatch.stop();

        StringBuffer endMessageStringBuffer = new StringBuffer();
//        endMessageStringBuffer.append("Finish method ");
//        endMessageStringBuffer.append(invocation);
//        endMessageStringBuffer.append("(..); execution time: ");
        endMessageStringBuffer.append("Execution time: ");
        endMessageStringBuffer.append(stopWatch.getTotalTimeMillis());
        endMessageStringBuffer.append(" ms;");

        if (log.isDebugEnabled()) {
        	log.debug(endMessageStringBuffer.toString());
        	log.debug("Invocation returned");
        }
        return rval;
    }
}
