package com.logisall.winus.frm.common.util;

import java.util.Calendar;

/**
 * @author minjae
 *
 */
public class LoginInfo {
	private String sessionId;
	private String id;
	private Calendar lastRequestTime;

	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Calendar getLastRequestTime() {
		return lastRequestTime;
	}
	public void setLastRequestTime(Calendar lastRequestTime) {
		this.lastRequestTime = lastRequestTime;
	}
}
