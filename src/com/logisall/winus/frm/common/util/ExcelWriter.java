package com.logisall.winus.frm.common.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

@SuppressWarnings("PMD")
public class ExcelWriter {
	protected Log log = LogFactory.getLog(this.getClass());
	
	private HttpServletResponse response;
	private  String sheetName ="";
	private  List<String> headerTextList =null;
	private GenericResultSet grs = null;

	
	public GenericResultSet getVmrs() {
		return grs;
	}
	public void setVmrs(GenericResultSet grs) {
		this.grs = grs;
	}
	
	public  String getSheetName() {
		return sheetName;
	}
	public  void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	
	public  List<String> getHeaderTextList() {
		return headerTextList;
	}
	
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	public HttpServletResponse getResponse() {
		return response;
	}
	
	
	//헤더 셋팅 1	
	public  void setHeaderTextList() {
		headerTextList =  new ArrayList<String>();
		headerTextList.add("");	
	}
	//헤더 셋팅 2
	public  void setHeaderTextList(String[] HList) {
		headerTextList =  new ArrayList<String>();
		for(int i =0; i<HList.length; i++){
			headerTextList.add(HList[i]);	
		}
	}
	
//
//	public int makeExHeader(String[][] header, HSSFRow row0, int rowCnt, HSSFSheet sheet, HSSFWorkbook wb){
//		int rowMax = 0;
//
//		for(int j =0; j<header.length; j++){
//			sheet.autoSizeColumn(j);
//			/*
//			if(j == 0 || j==7 ||  j==8){
//			sheet.setColumnWidth(j, (sheet.getColumnWidth(j)+(short)512)*2);
//			}else if(j==2 || j == 10 || j==11){
//				sheet.setColumnWidth(j, (sheet.getColumnWidth(j)+(short)512)*3);
//			}*/
//			//cell 넓이 지정
//			sheet.setColumnWidth(j,(sheet.getColumnWidth(j)+(short)Integer.parseInt(header[j][5]))*2);
//			
//			
//			if(rowMax <=Integer.parseInt(header[j][4])){
//				rowMax = Integer.parseInt(header[j][4]);
//			}
//			if(rowCnt != Integer.parseInt(header[j][3])){
//				rowCnt = Integer.parseInt(header[j][3]);
//				row0 = sheet.createRow(rowCnt);
//			}
//			// 시작row, 끝row, 시작col, 끝col
//			CellRangeAddress region = new CellRangeAddress(Integer.parseInt(header[j][3]),(short)Integer.parseInt(header[j][4]),Integer.parseInt(header[j][1]),(short)Integer.parseInt(header[j][2]));
//			if(region.getNumberOfCells() > 1){
//				sheet.addMergedRegion(region);
//			}
//			
//			int sCol = Integer.parseInt(header[j][1]);
//			HSSFCell headerCell = row0.createCell(sCol, CellType.STRING);
//			headerCell.setCellValue(new HSSFRichTextString(header[j][0]));
//
//			HSSFCellStyle headerStyle = wb.createCellStyle();
//			
//			headerStyle.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().index); 
////			headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//
//			HSSFFont font = wb.createFont(); 
//			font.setBold(true); 
//			font.setColor(new HSSFColor.BLACK().index);  
//			headerStyle.setFont(font); 
//			
//			headerStyle.setAlignment(HorizontalAlignment.CENTER);
//			headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
//			headerCell.setCellStyle(headerStyle);
//
//			// border    NO_FILL=라인없음
//			headerStyle.setBorderTop(BorderStyle.NONE);
//			headerStyle.setBorderLeft(BorderStyle.NONE);
//			headerStyle.setBorderRight(BorderStyle.NONE);
//			headerStyle.setBorderBottom(BorderStyle.NONE);
//			
//		}
//		return rowMax;
//	}
//	
//	/*
//	 * 엑셀 다운 (파일생성)
//	 * ( 조회결과 , 헤더 , 데이터 컬럼명, 파일명, 시트명, 병합 여부 )
//	 */
//	public void ExDown(GenericResultSet grs, String[][] header,  String[][] valueEx, String fName , String sName, String marCk, String Etc, HttpServletResponse response){
//
//		BufferedOutputStream bos = null;
//		FileInputStream fin = null;
//		
//		try{
//			String xlsFileName = fName +".xls";
//			File xlsFile = new File(xlsFileName);
//			
//			makeExcelFile(xlsFile , grs, header, valueEx, sName, marCk, Etc);
//
//			fin = new FileInputStream(xlsFile);
//			int ifilesize = (int)xlsFile.length();
//			byte[] b= new byte[ifilesize];
//
//			response.reset();
//			response.setHeader("Content-Disposition", "attachment;filename="
//					+ URLEncoder.encode(xlsFileName, "UTF-8") + ";");
//			response.setHeader("Content-Transfer-Encoding","binary;");
//			response.setHeader("Pragma","no-cache;");
//			response.setHeader("Expires","-1;");
//			response.setContentLength(ifilesize);
//			response.setContentType("application/vnd.ms-excel;charset=UTF-8");
//			
//			
//			bos = new BufferedOutputStream(response.getOutputStream());
//
//			fin.read(b);
//			bos.write(b,0,ifilesize);
//
//			bos.flush();
//			
//		}catch(Exception e){
//			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
//				e.printStackTrace();
//				log.error("download fail");
//			}
//		} finally {
//			if (bos != null) {
//				try {
//					bos.close();
//				} catch (Exception ie) {
//				}
//			}
//			if (fin != null) {
//				try {
//					fin.close();
//				} catch (Exception ise) {
//				}
//			}	
//		}
//	}
//	
//	public void downExcelFile(String path,	GenericResultSet grs, String[][] header,  String[][] valueEx, String fName , String sName, String marCk, String Etc, HttpServletResponse response){
//
//		BufferedOutputStream bos = null;
//		FileInputStream fin = null;
//		
//		String xlsFileName = fName +".xls";
//		
//		try{
//			if(!CommonUtil.isNull(path)){
//				if(!FileUtil.existDirectory(path)){
//				    FileHelper.createDirectorys(path);
//				}
//			}
//			File xlsFile = new File(path,xlsFileName);
//			
//			if(grs.getTotCnt() == 0){
//				makeEmptyExcelFile(xlsFile , grs, header, valueEx, sName, marCk, Etc);
//			}else{
//				makeExcelFile(xlsFile , grs, header, valueEx, sName, marCk, Etc);	
//			}
//			
//
//			fin = new FileInputStream(xlsFile);
//			int ifilesize = (int)xlsFile.length();
//			byte[] b = new byte[ifilesize];
//
//			response.reset();
//			response.setHeader("Content-Disposition", "attachment;filename="
//					+ URLEncoder.encode(xlsFileName, "UTF-8") + ";");
//			response.setHeader("Content-Transfer-Encoding","binary;");
//			response.setHeader("Pragma","no-cache;");
//			response.setHeader("Expires","-1;");
//			response.setContentLength(ifilesize);
//			response.setContentType("application/vnd.ms-excel;charset=UTF-8");
//			
//			
//			bos = new BufferedOutputStream(response.getOutputStream());
//			fin.read(b);
//			bos.write(b,0,ifilesize);
//			bos.flush();
//			
//		}catch(Exception e){
//			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
//				e.printStackTrace();
//				log.error("download fail");
//			}
//			if(FileUtil.existFile(path,xlsFileName)){
//				FileUtil.delFile(path, xlsFileName);
//			}
//		}finally{
//			if (bos != null) {
//				try {
//					bos.close();
//				} catch (Exception ie) {
//				}
//			}
//			if (fin != null) {
//				try {
//					fin.close();
//				} catch (Exception ise) {
//				}
//			}	
//			if(FileUtil.existFile(path,xlsFileName)){
//				FileUtil.delFile(path, xlsFileName);
//			}
//		}
//	}
//	
//	/*
//	 * 엑셀 데이터 생성
//	 */
//	public void makeExcelFile(File file , GenericResultSet grs, String[][] header, String[][] valueName, String sName, String marCk, String etc)throws IOException{
//		HSSFWorkbook wb = new HSSFWorkbook();
//		HSSFSheet sheet = null;
//		FileOutputStream fileOut = null;
//		List vlist = grs.getList();
//		int rowCnt 		=	0;
//		int realRowCnt	=	0;
//		int sheetCnt 	=	0;
//		String sheetText=	"";
//		
//		String marText1 =	"";
//		String marText2 =	"";
//		int marInt1		=	0;
//		int marInt2		=	0;
//		//시트 당 행 제한
//		int rowLime 	= 	5000;
//		
//		// create cellStyle Array
//		HSSFCellStyle[] cellStyleArr = new HSSFCellStyle[8];
//		for (int x = 0; x<cellStyleArr.length ; x++){
//			cellStyleArr[x] = wb.createCellStyle();
//			cellStyleArr[x].setBorderTop(BorderStyle.NONE);
//			cellStyleArr[x].setBorderLeft(BorderStyle.NONE);
//			cellStyleArr[x].setBorderRight(BorderStyle.NONE);
//			cellStyleArr[x].setBorderBottom(BorderStyle.NONE);
//		}
//		// 1: valueName[q][1] == "N" && data[q].indexOf(".") == -1
//		cellStyleArr[1].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
//		cellStyleArr[1].setAlignment(HorizontalAlignment.RIGHT);
//		// 2: valueName[q][1] == "N" && data[q].indexOf(".") != -1
//		cellStyleArr[2].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
//		cellStyleArr[2].setAlignment(HorizontalAlignment.RIGHT);
//		// 3: valueName[q][1] == "NR" && data[q].indexOf(".") == -1
//		cellStyleArr[3].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
//		cellStyleArr[3].setAlignment(HorizontalAlignment.RIGHT);
//		// 4: valueName[q][1] == "NR" && data[q].indexOf(".") != -1
//		cellStyleArr[4].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
//		cellStyleArr[4].setAlignment(HorizontalAlignment.RIGHT);
//		// 5: valueName[q][1] == "D"
//		cellStyleArr[5].setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
//		cellStyleArr[5].setAlignment(HorizontalAlignment.RIGHT);
//		// 6: valueName[q][1] == "DD"
//		cellStyleArr[6].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
//		cellStyleArr[6].setAlignment(HorizontalAlignment.RIGHT);
//		// 7: valueName[q][1] == "F"
//		cellStyleArr[7].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
//		cellStyleArr[7].setAlignment(HorizontalAlignment.RIGHT);
//		
//		
//		try{
//			Map exlistMap = null;
//			for(int i = 0; i < vlist.size(); i++) {
//				if(rowCnt<rowLime){
//					if(rowCnt==0){
////						시트 명 셋팅
//						sheetText = sName +"_" + sheetCnt;
////						 시트 생성
//						sheet = wb.createSheet(sheetText);
////						 행 생성
//						HSSFRow row0 = sheet.createRow(rowCnt);
//						/*
//						 * 헤더 생성
//						 */
//						rowCnt = makeExHeader(header, row0, rowCnt, sheet, wb);
//						realRowCnt = rowCnt;
//					}
//					exlistMap = (Map)vlist.get(i);
//					HSSFRow row  = sheet.createRow(rowCnt+1);
//
//					Double[] Ddata = new Double[valueName.length];
//					Float[] Fdata = new Float[valueName.length];
//					String[] data = new String[valueName.length];
//					
//					for(int col=0; col<valueName.length; col++){
//						
//						if(valueName[col][1].equals("S")){
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));	
//							if ( "null".equals(data[col]) ) {
//							    data[col] = "";
//							}
//						}else if(valueName[col][1].equals("N")){
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
//							//Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
//							//Ddata[col] = decimal.doubleValue();
//						}else if(valueName[col][1].equals("NR")){
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
//							//Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
//							//Ddata[col] = decimal.doubleValue();
//						}else if(valueName[col][1].equals("D")){
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
//							if ( "null".equals(data[col]) ) {
//							    data[col] = "";
//							}
//						}else if(valueName[col][1].equals("DD")){
//							//BigDecimal decimal = (BigDecimal)exlistMap.get(valueName[col][0]);
//							Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
//							Ddata[col] = decimal.doubleValue();
//						}else if(valueName[col][1].equals("F")){
//							//BigDecimal decimal = (BigDecimal)exlistMap.get(valueName[col][0]);
//							Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
//							Fdata[col] = decimal.floatValue();
//						}else if(valueName[col][1].equals("CC")){
//							//data[col] = (String)exlistMap.get(valueName[col][0]);
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
//						}
//					}
//					
//					/*
//					 * 데이터 셋팅
//					 */
//					for(int q=0; q<data.length; q++){
//						HSSFCell RowCell = null;
//						HSSFCellStyle cellStyle = null;
//						
//						if(valueName[q][1].equals("S")){
//							
//							RowCell = row.createCell(q, CellType.STRING);
//							RowCell.setCellValue(data[q]);
//							cellStyle = cellStyleArr[0];
//						}else if(valueName[q][1].equals("N")){
//							String tempNum = data[q];
//							
//							if(tempNum.indexOf(".") == -1) {
//								RowCell = row.createCell(q, CellType.NUMERIC);
//								cellStyle = cellStyleArr[1];
//
//								if(!"".equals(data[q])){
//									RowCell.setCellValue(Double.parseDouble(data[q]));
//								}else{
//									RowCell.setCellValue(data[q]);
//								}
//							} else {
//								DecimalFormat df;
//								double n = Double.parseDouble(data[q]);
//								df = new DecimalFormat("0.##");
//								   
//								RowCell = row.createCell(q, CellType.NUMERIC);
//								cellStyle = cellStyleArr[2];
//								RowCell.setCellValue(df.format(n));
//							}
//						}else if(valueName[q][1].equals("NR")){
//							String tempNum = data[q];
//							
//							if(tempNum.indexOf(".") == -1) {
//								RowCell = row.createCell(q, CellType.NUMERIC);
//								cellStyle = cellStyleArr[3];
//								
//								if(!"".equals(data[q])){
//									RowCell.setCellValue(Double.parseDouble(data[q]));
//								}else{
//									RowCell.setCellValue(data[q]);
//								}
//							} else {
//								RowCell = row.createCell(q, CellType.NUMERIC);
//								cellStyle = cellStyleArr[4];
//								RowCell.setCellValue(Math.round(Double.parseDouble(data[q])));
//							}
//						}else if(valueName[q][1].equals("D")){
//							
//							RowCell = row.createCell(q, CellType.STRING);
//
//							cellStyle = cellStyleArr[5];
//							RowCell.setCellValue(data[q]);
//							
//						}else if(valueName[q][1].equals("DD")){
//
//							RowCell = row.createCell(q, CellType.NUMERIC);
//
//							cellStyle = cellStyleArr[6];
//							RowCell.setCellValue(Ddata[q]);
//							
//						}else if(valueName[q][1].equals("F")){
//							
//							RowCell = row.createCell(q, CellType.NUMERIC);
//							
//							cellStyle = cellStyleArr[7];
//							RowCell.setCellValue(Fdata[q]);
//						
//						}
//						
//						/*
//						 * 셀 병합 
//						 */
//						if(!marCk.equals("N")){
//							if(realRowCnt<rowCnt){
//								if(marCk.equals("0") || marCk.equals("A")){
//									if(q==0){
//										if(!data[q].equals(marText1)){
//											marText1 = data[q];
//											sheet.addMergedRegion(new CellRangeAddress(marInt1,rowCnt,(short)0,(short)0));
//											marInt1	=	rowCnt+1;
//										}
//										
//										if(vlist.size()==(i+1)){
//											if(sheetCnt>0){
//												sheet.addMergedRegion(new CellRangeAddress(marInt1,(rowCnt+1),(short)0,(short)0));
//											}else{
//												sheet.addMergedRegion(new CellRangeAddress(marInt1,(rowCnt+1),(short)0,(short)0));
//											}
//										}
//										
//										if((rowCnt+1)>=rowLime){
//											sheet.addMergedRegion(new CellRangeAddress(marInt1,rowLime,(short)0,(short)0));
//											marInt1	=	rowCnt+1;
//										}
//									}	
//								}
//								
//								if(marCk.equals("1") || marCk.equals("A")){
//									if(q==1){
//										if(!data[q].equals(marText2)){
//											marText2 = data[q];
////											sheet.addMergedRegion(new Region(marInt2,(short)1,rowCnt,(short)1));
//											marInt2	=	rowCnt+1;
//										}
//										
//										if(vlist.size()==(i+1)){
//											if(sheetCnt>0){
//												sheet.addMergedRegion(new CellRangeAddress(marInt2,(rowCnt+1),(short)1,(short)1));
//											}else{
//												sheet.addMergedRegion(new CellRangeAddress(marInt2,(rowCnt+1),(short)1,(short)1));
//											}
//										}
//
//										if((rowCnt+1)>=rowLime){
//											sheet.addMergedRegion(new CellRangeAddress(marInt2,rowLime,(short)1,(short)1));
//											marInt2	=	rowCnt+1;
//										}
//									}	
//								}
//								
//							}else{
//								if(marCk.equals("0") || marCk.equals("A")){
//									if(q==0){
//										marText1 = data[q];
//										marInt1	=	rowCnt+1;
//									}	
//								}
//								if(marCk.equals("1") || marCk.equals("A")){
//									if(q==1){
//										marText2 = data[q];
//										marInt2	=	rowCnt+1;
//									}
//								}
//							}
//						}
//						if( RowCell != null ) RowCell.setCellStyle(cellStyle);
//					}
//					rowCnt++;
//				}else{
//					rowCnt=0;
//					sheetCnt++;
//					//log.info("rowCnt : "+ rowCnt + ", i = " + i);
//					// if(rowCnt==0){
//						sheetText = sName +"_" + sheetCnt;
//						sheet = wb.createSheet(sheetText);
//						HSSFRow row0 = sheet.createRow(rowCnt);
//						rowCnt = makeExHeader(header, row0, rowCnt, sheet, wb);
//						realRowCnt = rowCnt;
//					// }
//					exlistMap = (Map)vlist.get(i);
//					
//					//log.info("Data : "+ vlist.get(i));
//					
//					HSSFRow row  = sheet.createRow(rowCnt+1);
//					
//					Double[] Ddata = new Double[valueName.length];
//					Float[] Fdata = new Float[valueName.length];
//					String[] data = new String[valueName.length];
//					
//					for(int col=0; col<valueName.length; col++){
//						
//						if(valueName[col][1].equals("S")){
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));	
//							if ( "null".equals(data[col]) ) {
//							    data[col] = "";
//							}
//						}else if(valueName[col][1].equals("N")){
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
//							//Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
//							//Ddata[col] = decimal.doubleValue();
//							
//						}else if(valueName[col][1].equals("NR")){
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
//							//Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
//							//Ddata[col] = decimal.doubleValue();
//							
//						}else if(valueName[col][1].equals("D")){
//							
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
//							
//						}else if(valueName[col][1].equals("DD")){
//							Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
//							Ddata[col] = decimal.doubleValue();
//							
//						}else if(valueName[col][1].equals("F")){
//							Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
//							Fdata[col] = decimal.floatValue();
//							
//						}else if(valueName[col][1].equals("CC")){
//							
//							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
//							
//							
//						}
//					}
//					/*
//					 * 데이터 셋팅
//					 */
//					for(int q=0; q<data.length; q++){
//						HSSFCell RowCell = null;
//						HSSFCellStyle cellStyle = null;
//
//						if(valueName[q][1].equals("S")){
//							
//							RowCell = row.createCell(q, CellType.STRING);
//							RowCell.setCellValue(data[q]);
//							cellStyle = cellStyleArr[0];
//						}else if(valueName[q][1].equals("N")){
//							String tempNum = data[q];
//							
//							if(tempNum.indexOf(".") == -1) {
//								RowCell = row.createCell(q, CellType.STRING);
//								cellStyle = cellStyleArr[1];
//								
//								// log.info(exlistMap.get("RITEM_ID") + " Cell ["+ q +"] Data : " + data[q]);
//								if(!"".equals(data[q])){
//									RowCell.setCellValue(Double.parseDouble(data[q]));
//								}else{
//									RowCell.setCellValue(data[q]);
//								}
//								// RowCell.setCellValue(Double.parseDouble(data[q]));
//								
//							} else {
//								DecimalFormat df;
//								double n = Double.parseDouble(data[q]);
//								df = new DecimalFormat("0.##");
//								
//								RowCell = row.createCell(q, CellType.NUMERIC);
//								cellStyle = cellStyleArr[2];
//								RowCell.setCellValue(df.format(n));
//							}
//						}else if(valueName[q][1].equals("NR")){
//							String tempNum = data[q];
//							
//							if(tempNum.indexOf(".") == -1) {
//								RowCell = row.createCell(q, CellType.NUMERIC);
//								cellStyle = cellStyleArr[3];
//								if(!"".equals(data[q])){
//									RowCell.setCellValue(Double.parseDouble(data[q]));
//								}else{
//									RowCell.setCellValue(data[q]);
//								}
//							} else {
//								RowCell = row.createCell(q, CellType.NUMERIC);
//								cellStyle = cellStyleArr[4];
//								RowCell.setCellValue(Math.round(Double.parseDouble(data[q])));
//							}
//						}else if(valueName[q][1].equals("D")){
//							
//							RowCell = row.createCell(q, CellType.NUMERIC);
//							cellStyle = cellStyleArr[5];
//							RowCell.setCellValue(Integer.parseInt(data[q]));
//							
//						}else if(valueName[q][1].equals("DD")){
//
//							RowCell = row.createCell(q, CellType.NUMERIC);
//
//							cellStyle = cellStyleArr[6];
//							RowCell.setCellValue(Ddata[q]);
//							
//						}else if(valueName[q][1].equals("F")){
//							
//							RowCell = row.createCell(q, CellType.NUMERIC);
//							
//							cellStyle = cellStyleArr[7];
//							RowCell.setCellValue(Fdata[q]);
//							
//						}
//						
//						/*
//						 * 셀 병합 
//						 */
//						if(!marCk.equals("N")){
//							
//							if(realRowCnt<rowCnt){
//								if(marCk.equals("0") || marCk.equals("A")){
//									if(q==0){
//										if(!data[q].equals(marText1)){
//											marText1 = data[q];
//											sheet.addMergedRegion(new CellRangeAddress(marInt1,rowCnt,(short)0,(short)0));
//											marInt1	=	rowCnt+1;
//										}
//										if(vlist.size()==(i+1)){
//											sheet.addMergedRegion(new CellRangeAddress(marInt1,(i+1+realRowCnt),(short)0,(short)0));
//										}
//									}	
//								}
//								
//								if(marCk.equals("1") || marCk.equals("A")){
//									if(q==1){
//										if(!data[q].equals(marText2)){
//											marText2 = data[q];
//											sheet.addMergedRegion(new CellRangeAddress(marInt2,rowCnt,(short)1,(short)1));
//											marInt2	=	rowCnt+1;
//										}
//										if(vlist.size()==(i+1)){
//											sheet.addMergedRegion(new CellRangeAddress(marInt2,(i+1+realRowCnt),(short)1,(short)1));
//										}
//									}	
//								}
//								
//							}else{
//								if(marCk.equals("0") || marCk.equals("A")){
//									if(q==0){
//										marText1 = data[q];
//										marInt1	=	rowCnt+1;
//									}	
//								}
//								
//								if(marCk.equals("1") || marCk.equals("A")){
//									if(q==1){
//										marText2 = data[q];
//										marInt2	=	rowCnt+1;
//									}
//								}
//							}
//						}
//						//스타일 적용
//						if(RowCell != null)RowCell.setCellStyle(cellStyle);
//					}
//					//행 증가
//					rowCnt++;
//				}
//			}
//			
//			fileOut = new FileOutputStream(file);
//			
//			wb.write(fileOut);
//			
//		} catch (RuntimeException e) {
//			throw e;
//			
//		} catch (Exception e) {
//			//e.printStackTrace();
//			//throw new Exception(ExceptionMessage.ExcelWritingFailure, e);
//		} finally {
//			wb.close();
//			if (fileOut != null) {
//				try {
//					fileOut.close();
//				} catch (Exception ie) {
//				}
//			}
//			
//		}
//		
//	}
//	
//	/*
//	 * 엑셀 데이터 생성
//	 */
//	public void makeEmptyExcelFile(File file , GenericResultSet grs, String[][] header, String[][] valueName, String sName, String marCk, String etc)throws IOException{
//		HSSFWorkbook wb = new HSSFWorkbook();
//		HSSFSheet sheet = null;
//		FileOutputStream fileOut = null;
//		int rowCnt 		=	0;
//		int sheetCnt 	=	0;
//		String sheetText=	"";
//		
//		try{
////			시트 명 셋팅
//			sheetText = sName +"_" + sheetCnt;
////			 시트 생성
//			sheet = wb.createSheet(sheetText);
////			 행 생성
//			HSSFRow row0 = sheet.createRow(rowCnt);
//			/*
//			 * 헤더 생성
//			 */
//			rowCnt = makeExHeader(header, row0, rowCnt, sheet, wb);
//			
//			fileOut = new FileOutputStream(file);
//			
//			wb.write(fileOut);
//			
//		} catch (RuntimeException e) {
//			throw e;
//			
//		} catch (Exception e) {
//			//e.printStackTrace();
//			//throw new Exception(ExceptionMessage.ExcelWritingFailure, e);
//		} finally {
//			wb.close();
//			if (fileOut != null) {
//				try {
//					fileOut.close();
//				} catch (Exception ie) {
//				}
//			}
//			
//		}
//		
//	}
	
/////////////////////////////////////////// xlsx down
	
	public int makeExHeader(String[][] header, SXSSFRow row0, int rowCnt, SXSSFSheet sheet, SXSSFWorkbook wb){
		int rowMax = 0;

		for(int j =0; j<header.length; j++){
			sheet.trackAllColumnsForAutoSizing();
			sheet.autoSizeColumn(j);
			/*
			if(j == 0 || j==7 ||  j==8){
			sheet.setColumnWidth(j, (sheet.getColumnWidth(j)+(short)512)*2);
			}else if(j==2 || j == 10 || j==11){
				sheet.setColumnWidth(j, (sheet.getColumnWidth(j)+(short)512)*3);
			}*/
			//cell 넓이 지정
			sheet.setColumnWidth(j,(sheet.getColumnWidth(j)+(short)Integer.parseInt(header[j][5]))*2);
			
			
			if(rowMax <=Integer.parseInt(header[j][4])){
				rowMax = Integer.parseInt(header[j][4]);
			}
			if(rowCnt != Integer.parseInt(header[j][3])){
				rowCnt = Integer.parseInt(header[j][3]);
				row0 = sheet.createRow(rowCnt);
			}
			// 시작row, 끝row, 시작col, 끝col
			CellRangeAddress region = new CellRangeAddress(Integer.parseInt(header[j][3]),(short)Integer.parseInt(header[j][4]),Integer.parseInt(header[j][1]),(short)Integer.parseInt(header[j][2]));
			if(region.getNumberOfCells() > 1){
				sheet.addMergedRegion(region);
			}
			
			int sCol = Integer.parseInt(header[j][1]);
			SXSSFCell headerCell = row0.createCell(sCol, CellType.STRING);
			headerCell.setCellValue(new XSSFRichTextString(header[j][0]));

			CellStyle headerStyle = wb.createCellStyle();
			headerStyle.setFillBackgroundColor(new HSSFColor.GREY_25_PERCENT().index); 
//			headerStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);

			Font font = wb.createFont(); 
			font.setBold(true); 
			font.setColor(new HSSFColor.BLACK().index);  
			headerStyle.setFont(font); 
			
			headerStyle.setAlignment(HorizontalAlignment.CENTER);
			headerStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			headerCell.setCellStyle(headerStyle);

			// border    NO_FILL=라인없음
			headerStyle.setBorderTop(BorderStyle.NONE);
			headerStyle.setBorderLeft(BorderStyle.NONE);
			headerStyle.setBorderRight(BorderStyle.NONE);
			headerStyle.setBorderBottom(BorderStyle.NONE);
			
		}
		return rowMax;
	}
	
	/*
	 * 엑셀 다운 (파일생성)
	 * ( 조회결과 , 헤더 , 데이터 컬럼명, 파일명, 시트명, 병합 여부 )
	 */
	public void ExDown(GenericResultSet grs, String[][] header,  String[][] valueEx, String fName , String sName, String marCk, String Etc, HttpServletResponse response){

		BufferedOutputStream bos = null;
		FileInputStream fin = null;
		
		try{
			String xlsFileName = fName +".xlsx";
			File xlsFile = new File(xlsFileName);
			
			makeExcelFile(xlsFile , grs, header, valueEx, sName, marCk, Etc);

			fin = new FileInputStream(xlsFile);
			int ifilesize = (int)xlsFile.length();
			byte[] b= new byte[ifilesize];

			response.reset();
			response.setHeader("Content-Disposition", "attachment;filename="
					+ URLEncoder.encode(xlsFileName, "UTF-8") + ";");
			response.setHeader("Content-Transfer-Encoding","binary;");
			response.setHeader("Pragma","no-cache;");
			response.setHeader("Expires","-1;");
			response.setContentLength(ifilesize);
			response.setContentType("application/vnd.ms-excel;charset=UTF-8");
			
			
			bos = new BufferedOutputStream(response.getOutputStream());

			fin.read(b);
			bos.write(b,0,ifilesize);

			bos.flush();
			
		}catch(Exception e){
			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
				e.printStackTrace();
				log.error("download fail");
			}
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (Exception ie) {
				}
			}
			if (fin != null) {
				try {
					fin.close();
				} catch (Exception ise) {
				}
			}	
		}
	}
	
	public void downExcelFile(String path,	GenericResultSet grs, String[][] header,  String[][] valueEx, String fName , String sName, String marCk, String Etc, HttpServletResponse response){
		BufferedOutputStream bos = null;
		FileInputStream fin = null;
		
		String xlsFileName = fName +".xlsx";
		
		try{
			if(!CommonUtil.isNull(path)){
				if(!FileUtil.existDirectory(path)){
				    FileHelper.createDirectorys(path);
				}
			}
			File xlsFile = new File(path,xlsFileName);
			
			if(grs.getTotCnt() == 0){
				makeEmptyExcelFile(xlsFile , grs, header, valueEx, sName, marCk, Etc);
			}else{
				makeExcelFile(xlsFile , grs, header, valueEx, sName, marCk, Etc);	
			}
			

			fin = new FileInputStream(xlsFile);
			int ifilesize = (int)xlsFile.length();
			byte[] b = new byte[ifilesize];

			response.reset();
			response.setHeader("Content-Disposition", "attachment;filename="
					+ URLEncoder.encode(xlsFileName, "UTF-8") + ";");
			response.setHeader("Content-Transfer-Encoding","binary;");
			response.setHeader("Pragma","no-cache;");
			response.setHeader("Expires","-1;");
			response.setContentLength(ifilesize);
			response.setContentType("application/vnd.ms-excel;charset=UTF-8");
			
			
			bos = new BufferedOutputStream(response.getOutputStream());
			fin.read(b);
			bos.write(b,0,ifilesize);
			bos.flush();
			
		}catch(Exception e){
			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
				e.printStackTrace();
				log.error("download fail");
			}
			if(FileUtil.existFile(path,xlsFileName)){
				FileUtil.delFile(path, xlsFileName);
			}
		}finally{
			if (bos != null) {
				try {
					bos.close();
				} catch (Exception ie) {
				}
			}
			if (fin != null) {
				try {
					fin.close();
				} catch (Exception ise) {
				}
			}	
			if(FileUtil.existFile(path,xlsFileName)){
				FileUtil.delFile(path, xlsFileName);
			}
		}
	}
	
	
	/*
	 * 엑셀 데이터 생성
	 */
	public void makeExcelFile(File file , GenericResultSet grs, String[][] header, String[][] valueName, String sName, String marCk, String etc)throws IOException{
		SXSSFWorkbook wb = new SXSSFWorkbook();
		SXSSFSheet sheet = null;
		FileOutputStream fileOut = null;
		List vlist = grs.getList();
		int rowCnt 		=	0;
		int realRowCnt	=	0;
		int sheetCnt 	=	0;
		String sheetText=	"";
		
		String marText1 =	"";
		String marText2 =	"";
		int marInt1		=	0;
		int marInt2		=	0;
		//시트 당 행 제한
		int rowLime 	= 	100000;
		
		
		CellStyle[] cellStyleArr = new CellStyle[8];
		for (int x = 0; x<cellStyleArr.length ; x++){
			cellStyleArr[x] = wb.createCellStyle();
			cellStyleArr[x].setBorderTop(BorderStyle.NONE);
			cellStyleArr[x].setBorderLeft(BorderStyle.NONE);
			cellStyleArr[x].setBorderRight(BorderStyle.NONE);
			cellStyleArr[x].setBorderBottom(BorderStyle.NONE);
		}
		// 0: valueName[q][1] == "S"
		// cellStyleArr[0]
		// 1: valueName[q][1] == "N" && data[q].indexOf(".") == -1
		cellStyleArr[1].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
		cellStyleArr[1].setAlignment(HorizontalAlignment.RIGHT);
		// 2: valueName[q][1] == "N" && data[q].indexOf(".") != -1
		cellStyleArr[2].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
		cellStyleArr[2].setAlignment(HorizontalAlignment.RIGHT);
		// 3: valueName[q][1] == "NR" && data[q].indexOf(".") == -1
		cellStyleArr[3].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
		cellStyleArr[3].setAlignment(HorizontalAlignment.RIGHT);
		// 4: valueName[q][1] == "NR" && data[q].indexOf(".") != -1
		cellStyleArr[4].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
		cellStyleArr[4].setAlignment(HorizontalAlignment.RIGHT);
		// 5: valueName[q][1] == "D"
		cellStyleArr[5].setDataFormat(HSSFDataFormat.getBuiltinFormat("yyyy-mm-dd"));
		cellStyleArr[5].setAlignment(HorizontalAlignment.RIGHT);
		// 6: valueName[q][1] == "DD"
		cellStyleArr[6].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
		cellStyleArr[6].setAlignment(HorizontalAlignment.RIGHT);
		// 7: valueName[q][1] == "F"
		cellStyleArr[7].setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
		cellStyleArr[7].setAlignment(HorizontalAlignment.RIGHT);
		
		
		try{
			Map exlistMap = null;
			for(int i = 0; i < vlist.size(); i++) {
				if(rowCnt<rowLime){
					if(rowCnt==0){
//						시트 명 셋팅
						sheetText = sName +"_" + sheetCnt;
//						 시트 생성
						sheet = wb.createSheet(sheetText);
//						 행 생성
						SXSSFRow row0 = sheet.createRow(rowCnt);
						/*
						 * 헤더 생성
						 */
						rowCnt = makeExHeader(header, row0, rowCnt, sheet, wb);
						realRowCnt = rowCnt;
					}
					exlistMap = (Map)vlist.get(i);
					SXSSFRow row  = sheet.createRow(rowCnt+1);

					Double[] Ddata = new Double[valueName.length];
					Float[] Fdata = new Float[valueName.length];
					String[] data = new String[valueName.length];
					
					for(int col=0; col<valueName.length; col++){
						
						if(valueName[col][1].equals("S")){
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));	
							if ( "null".equals(data[col]) ) {
							    data[col] = "";
							}
						}else if(valueName[col][1].equals("N")){
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
							//Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
							//Ddata[col] = decimal.doubleValue();
						}else if(valueName[col][1].equals("NR")){
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
							//Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
							//Ddata[col] = decimal.doubleValue();
						}else if(valueName[col][1].equals("D")){
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
						}else if(valueName[col][1].equals("DD")){
							//BigDecimal decimal = (BigDecimal)exlistMap.get(valueName[col][0]);
							Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
							Ddata[col] = decimal.doubleValue();
						}else if(valueName[col][1].equals("F")){
							//BigDecimal decimal = (BigDecimal)exlistMap.get(valueName[col][0]);
							Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
							Fdata[col] = decimal.floatValue();
						}else if(valueName[col][1].equals("CC")){
							//data[col] = (String)exlistMap.get(valueName[col][0]);
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
						}
					}
					
					/*
					 * 데이터 셋팅
					 */
					for(int q=0; q<data.length; q++){
						SXSSFCell RowCell = null;
						CellStyle cellStyle = null;
						
						if(valueName[q][1].equals("S")){
							
							RowCell = row.createCell(q, CellType.STRING);
							RowCell.setCellValue(data[q]);
							cellStyle = cellStyleArr[0];
							
						}else if(valueName[q][1].equals("N")){
							String tempNum = data[q];
							
							if(tempNum.indexOf(".") == -1) {
								RowCell = row.createCell(q, CellType.NUMERIC);
								cellStyle = cellStyleArr[1];

								if(!"".equals(data[q])){
									RowCell.setCellValue(Double.parseDouble(data[q]));
								}else{
									RowCell.setCellValue(data[q]);
								}
							} else {
								DecimalFormat df;
								double n = Double.parseDouble(data[q]);
								df = new DecimalFormat("0.##");
								   
								RowCell = row.createCell(q, CellType.NUMERIC);
								cellStyle = cellStyleArr[2];
								RowCell.setCellValue(df.format(n));
							}
						}else if(valueName[q][1].equals("NR")){
							String tempNum = data[q];
							
							if(tempNum.indexOf(".") == -1) {
								RowCell = row.createCell(q, CellType.NUMERIC);
								cellStyle = cellStyleArr[3];
								
								if(!"".equals(data[q])){
									RowCell.setCellValue(Double.parseDouble(data[q]));
								}else{
									RowCell.setCellValue(data[q]);
								}
							} else {
								RowCell = row.createCell(q, CellType.NUMERIC);
								cellStyle = cellStyleArr[4];
								RowCell.setCellValue(Math.round(Double.parseDouble(data[q])));
							}
						}else if(valueName[q][1].equals("D")){
							
							RowCell = row.createCell(q, CellType.NUMERIC);

							cellStyle = cellStyleArr[5];
							RowCell.setCellValue(Integer.parseInt(data[q]));
							
						}else if(valueName[q][1].equals("DD")){

							RowCell = row.createCell(q, CellType.NUMERIC);

							cellStyle = cellStyleArr[6];
							RowCell.setCellValue(Ddata[q]);
							
						}else if(valueName[q][1].equals("F")){
							
							RowCell = row.createCell(q, CellType.NUMERIC);
							
							cellStyle = cellStyleArr[7];
							RowCell.setCellValue(Fdata[q]);
						
						}
						
						/*
						 * 셀 병합 
						 */
						if(!marCk.equals("N")){
							if(realRowCnt<rowCnt){
								if(marCk.equals("0") || marCk.equals("A")){
									if(q==0){
										if(!data[q].equals(marText1)){
											marText1 = data[q];
											sheet.addMergedRegion(new CellRangeAddress(marInt1,rowCnt,(short)0,(short)0));
											marInt1	=	rowCnt+1;
										}
										
										if(vlist.size()==(i+1)){
											if(sheetCnt>0){
												sheet.addMergedRegion(new CellRangeAddress(marInt1,(rowCnt+1),(short)0,(short)0));
											}else{
												sheet.addMergedRegion(new CellRangeAddress(marInt1,(rowCnt+1),(short)0,(short)0));
											}
										}
										
										if((rowCnt+1)>=rowLime){
											sheet.addMergedRegion(new CellRangeAddress(marInt1,rowLime,(short)0,(short)0));
											marInt1	=	rowCnt+1;
										}
									}	
								}
								
								if(marCk.equals("1") || marCk.equals("A")){
									if(q==1){
										if(!data[q].equals(marText2)){
											marText2 = data[q];
//											sheet.addMergedRegion(new Region(marInt2,(short)1,rowCnt,(short)1));
											marInt2	=	rowCnt+1;
										}
										
										if(vlist.size()==(i+1)){
											if(sheetCnt>0){
												sheet.addMergedRegion(new CellRangeAddress(marInt2,(rowCnt+1),(short)1,(short)1));
											}else{
												sheet.addMergedRegion(new CellRangeAddress(marInt2,(rowCnt+1),(short)1,(short)1));
											}
										}

										if((rowCnt+1)>=rowLime){
											sheet.addMergedRegion(new CellRangeAddress(marInt2,rowLime,(short)1,(short)1));
											marInt2	=	rowCnt+1;
										}
									}	
								}
								
							}else{
								if(marCk.equals("0") || marCk.equals("A")){
									if(q==0){
										marText1 = data[q];
										marInt1	=	rowCnt+1;
									}	
								}
								if(marCk.equals("1") || marCk.equals("A")){
									if(q==1){
										marText2 = data[q];
										marInt2	=	rowCnt+1;
									}
								}
							}
						}
						if( RowCell != null ) RowCell.setCellStyle(cellStyle);
					}
					rowCnt++;
				}else{
					rowCnt=0;
					sheetCnt++;
					//log.info("rowCnt : "+ rowCnt + ", i = " + i);
					// if(rowCnt==0){
						sheetText = sName +"_" + sheetCnt;
						sheet = wb.createSheet(sheetText);
						SXSSFRow row0 = sheet.createRow(rowCnt);
						rowCnt = makeExHeader(header, row0, rowCnt, sheet, wb);
						realRowCnt = rowCnt;
					// }
					exlistMap = (Map)vlist.get(i);
					
					//log.info("Data : "+ vlist.get(i));
					
					SXSSFRow row  = sheet.createRow(rowCnt+1);
					
					Double[] Ddata = new Double[valueName.length];
					Float[] Fdata = new Float[valueName.length];
					String[] data = new String[valueName.length];
					
					for(int col=0; col<valueName.length; col++){
						
						if(valueName[col][1].equals("S")){
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));	
							if ( "null".equals(data[col]) ) {
							    data[col] = "";
							}
						}else if(valueName[col][1].equals("N")){
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
							//Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
							//Ddata[col] = decimal.doubleValue();
							
						}else if(valueName[col][1].equals("NR")){
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
							//Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
							//Ddata[col] = decimal.doubleValue();
							
						}else if(valueName[col][1].equals("D")){
							
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
							
						}else if(valueName[col][1].equals("DD")){
							Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
							Ddata[col] = decimal.doubleValue();
							
						}else if(valueName[col][1].equals("F")){
							Integer decimal = ((Number)exlistMap.get(valueName[col][0])).intValue();
							Fdata[col] = decimal.floatValue();
							
						}else if(valueName[col][1].equals("CC")){
							
							data[col] = String.valueOf(exlistMap.get(valueName[col][0]));
							
							
						}
					}
					/*
					 * 데이터 셋팅
					 */
					for(int q=0; q<data.length; q++){
						SXSSFCell RowCell = null;
						CellStyle cellStyle = null;

						if(valueName[q][1].equals("S")){
							
							RowCell = row.createCell(q, CellType.STRING);
							RowCell.setCellValue(data[q]);
							cellStyle = cellStyleArr[0];
						}else if(valueName[q][1].equals("N")){
							String tempNum = data[q];
							
							if(tempNum.indexOf(".") == -1) {
								RowCell = row.createCell(q, CellType.STRING);
								cellStyle = cellStyleArr[1];
								
								// log.info(exlistMap.get("RITEM_ID") + " Cell ["+ q +"] Data : " + data[q]);
								if(!"".equals(data[q])){
									RowCell.setCellValue(Double.parseDouble(data[q]));
								}else{
									RowCell.setCellValue(data[q]);
								}
								// RowCell.setCellValue(Double.parseDouble(data[q]));
								
							} else {
								DecimalFormat df;
								double n = Double.parseDouble(data[q]);
								df = new DecimalFormat("0.##");
								
								RowCell = row.createCell(q, CellType.NUMERIC);
								cellStyle = cellStyleArr[2];
								RowCell.setCellValue(df.format(n));
							}
						}else if(valueName[q][1].equals("NR")){
							String tempNum = data[q];
							
							if(tempNum.indexOf(".") == -1) {
								RowCell = row.createCell(q, CellType.NUMERIC);
								cellStyle = cellStyleArr[3];
								if(!"".equals(data[q])){
									RowCell.setCellValue(Double.parseDouble(data[q]));
								}else{
									RowCell.setCellValue(data[q]);
								}
							} else {
								RowCell = row.createCell(q, CellType.NUMERIC);
								cellStyle = cellStyleArr[4];
								RowCell.setCellValue(Math.round(Double.parseDouble(data[q])));
							}
						}else if(valueName[q][1].equals("D")){
							
							RowCell = row.createCell(q, CellType.NUMERIC);
							cellStyle = cellStyleArr[5];
							RowCell.setCellValue(Integer.parseInt(data[q]));
							
						}else if(valueName[q][1].equals("DD")){

							RowCell = row.createCell(q, CellType.NUMERIC);

							cellStyle = cellStyleArr[6];
							RowCell.setCellValue(Ddata[q]);
							
						}else if(valueName[q][1].equals("F")){
							
							RowCell = row.createCell(q, CellType.NUMERIC);
							
							cellStyle = cellStyleArr[7];
							RowCell.setCellValue(Fdata[q]);
							
						}
						
						/*
						 * 셀 병합 
						 */
						if(!marCk.equals("N")){
							
							if(realRowCnt<rowCnt){
								if(marCk.equals("0") || marCk.equals("A")){
									if(q==0){
										if(!data[q].equals(marText1)){
											marText1 = data[q];
											sheet.addMergedRegion(new CellRangeAddress(marInt1,rowCnt,(short)0,(short)0));
											marInt1	=	rowCnt+1;
										}
										if(vlist.size()==(i+1)){
											sheet.addMergedRegion(new CellRangeAddress(marInt1,(i+1+realRowCnt),(short)0,(short)0));
										}
									}	
								}
								
								if(marCk.equals("1") || marCk.equals("A")){
									if(q==1){
										if(!data[q].equals(marText2)){
											marText2 = data[q];
											sheet.addMergedRegion(new CellRangeAddress(marInt2,rowCnt,(short)1,(short)1));
											marInt2	=	rowCnt+1;
										}
										if(vlist.size()==(i+1)){
											sheet.addMergedRegion(new CellRangeAddress(marInt2,(i+1+realRowCnt),(short)1,(short)1));
										}
									}	
								}
								
							}else{
								if(marCk.equals("0") || marCk.equals("A")){
									if(q==0){
										marText1 = data[q];
										marInt1	=	rowCnt+1;
									}	
								}
								
								if(marCk.equals("1") || marCk.equals("A")){
									if(q==1){
										marText2 = data[q];
										marInt2	=	rowCnt+1;
									}
								}
							}
						}
						
						//스타일 적용
						if(RowCell != null)RowCell.setCellStyle(cellStyle);
					}
					//행 증가
					rowCnt++;
				}
			}
			
			fileOut = new FileOutputStream(file);
			
			wb.write(fileOut);
			
		} catch (RuntimeException e) {
			throw e;
			
		} catch (Exception e) {
			//e.printStackTrace();
			//throw new Exception(ExceptionMessage.ExcelWritingFailure, e);
		} finally {
			wb.close();
			wb.dispose();
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (Exception ie) {
				}
			}
			

		}
		
	}
	
	/*
	 * 엑셀 데이터 생성
	 */
	public void makeEmptyExcelFile(File file , GenericResultSet grs, String[][] header, String[][] valueName, String sName, String marCk, String etc)throws IOException{
		SXSSFWorkbook wb = new SXSSFWorkbook();
		SXSSFSheet sheet = null;
		FileOutputStream fileOut = null;
		int rowCnt 		=	0;
		int sheetCnt 	=	0;
		String sheetText=	"";
		
		try{
//			시트 명 셋팅
			sheetText = sName +"_" + sheetCnt;
//			 시트 생성
			sheet = wb.createSheet(sheetText);
//			 행 생성
			SXSSFRow row0 = sheet.createRow(rowCnt);
			/*
			 * 헤더 생성
			 */
			rowCnt = makeExHeader(header, row0, rowCnt, sheet, wb);
			
			fileOut = new FileOutputStream(file);
			
			wb.write(fileOut);
			
		} catch (RuntimeException e) {
			throw e;
			
		} catch (Exception e) {
			//e.printStackTrace();
			//throw new Exception(ExceptionMessage.ExcelWritingFailure, e);
		} finally {
			wb.close();
			wb.dispose();
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (Exception ie) {
				}
			}
		}
		
	}
	////////////////// xlsx down
}

