package com.logisall.winus.frm.common.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration.CombinedConfiguration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class PropertiesManager {

	protected Log log = LogFactory.getLog(this.getClass());
	
	private static PropertiesManager manager = new PropertiesManager();
	private final CombinedConfiguration configuration = new CombinedConfiguration();

	private PropertiesManager() {
	}

	public static PropertiesManager getInstance() {
		return manager;
	}

	public BigDecimal getBigDecimal(final String key) {
		return this.configuration.getBigDecimal(key);
	}

	public BigDecimal getBigDecimal(final String key,
			final BigDecimal defaultValue) {
		return this.configuration.getBigDecimal(key, defaultValue);
	}

	public BigInteger getBigInteger(final String key) {
		return this.configuration.getBigInteger(key);
	}

	public BigInteger getBigInteger(final String key,
			final BigInteger defaultValue) {
		return this.configuration.getBigInteger(key, defaultValue);
	}

	public boolean getBoolean(final String key) {
		return this.configuration.getBoolean(key);
	}

	public boolean getBoolean(final String key, final boolean defaultValue) {
		return this.configuration.getBoolean(key, defaultValue);
	}

	public Boolean getBoolean(final String key, final Boolean defaultValue) {
		return this.configuration.getBoolean(key, defaultValue);
	}

	public byte getByte(final String key) {
		return this.configuration.getByte(key);
	}

	public byte getByte(final String key, final byte defaultValue) {
		return this.configuration.getByte(key, defaultValue);
	}

	public Byte getByte(final String key, final Byte defaultValue) {
		return this.configuration.getByte(key, defaultValue);
	}

	public double getDouble(final String key) {
		return this.configuration.getDouble(key);
	}

	public double getDouble(final String key, final double defaultValue) {
		return this.configuration.getDouble(key, defaultValue);
	}

	public Double getDouble(final String key, final Double defaultValue) {
		return this.configuration.getDouble(key, defaultValue);
	}

	public float getFloat(final String key) {
		return this.configuration.getFloat(key);
	}

	public float getFloat(final String key, final float defaultValue) {
		return this.configuration.getFloat(key, defaultValue);
	}

	public Float getFloat(final String key, final Float defaultValue) {
		return this.configuration.getFloat(key, defaultValue);
	}

	public int getInt(final String key) {
		return this.configuration.getInt(key);
	}

	public int getInt(final String key, final int defaultValue) {
		return this.configuration.getInt(key, defaultValue);
	}

	public Integer getInteger(final String key, final Integer defaultValue) {
		return this.configuration.getInteger(key, defaultValue);
	}

	public Iterator getKeys() {
		return this.configuration.getKeys();
	}

	public List getList(final String key) {
		return this.configuration.getList(key);
	}

	public List getList(final String key, final List defaultValue) {
		return this.configuration.getList(key, defaultValue);
	}

	public long getLong(final String key) {
		return this.configuration.getLong(key);
	}

	public long getLong(final String key, final long defaultValue) {
		return this.configuration.getLong(key, defaultValue);
	}

	public Long getLong(final String key, final Long defaultValue) {
		return this.configuration.getLong(key, defaultValue);
	}

	@SuppressWarnings("PMD")
	public short getShort(final String key) {
		return this.configuration.getShort(key);
	}

	@SuppressWarnings("PMD")
	public short getShort(final String key, final short defaultValue) {
		return this.configuration.getShort(key, defaultValue);
	}

	public Short getShort(final String key, final Short defaultValue) {
		return this.configuration.getShort(key, defaultValue);
	}

	public String getString(final String key) {
		return this.configuration.getString(key);
	}

	public String getString(final String key, final String defaultValue) {
		return this.configuration.getString(key, defaultValue);
	}

	public String[] getStringArray(final String key) {
		return this.configuration.getStringArray(key);
	}

	public void setDefaultListDelimiter(final char delimiter) {
		this.configuration.setDefaultListDelimiter(delimiter);
	}

	public Object source() {
		return this.configuration;
	}

	public void initialize(final String path, final String interval)
			throws Exception {
		
		if (log.isInfoEnabled()) {
			log.info("[START] PropertiesManager initialize....");
		}
		
		String[] pnameArr = StringUtils.split(path, ',');
		String[] intervalArr = StringUtils.split(interval, ',');

		Validate.notNull(pnameArr);
		Validate.notNull(intervalArr);
		int pCount = pnameArr.length;
		Validate.isTrue(pCount == intervalArr.length);
		Validate.isTrue(pCount > 0);

		this.configuration.clear();
		for (int pIndex = 0; pIndex < pCount; pIndex++) {
			PropertiesConfiguration config = new PropertiesConfiguration(
					pnameArr[pIndex]);
			FileChangedReloadingStrategy strategy = new FileChangedReloadingStrategy();
			strategy.setRefreshDelay(NumberUtils.toLong(intervalArr[pIndex]));
			config.setReloadingStrategy(strategy);
			this.configuration.addConfiguration(config, String.valueOf(pIndex));
		}
		
		if (log.isInfoEnabled()) {
			log.info("[ END ] PropertiesManager initialize....");
		}		
		
	}
}
