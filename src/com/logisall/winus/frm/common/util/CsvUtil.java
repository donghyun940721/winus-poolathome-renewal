package com.logisall.winus.frm.common.util;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

/**
 * @author chSong ExcelWriter 참고했음
 * 
 */

public class CsvUtil {
	protected Log log = LogFactory.getLog(this.getClass());

	private HttpServletResponse response;

	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}

	public HttpServletResponse getResponse() {
		return response;
	}

	/*
	 * CSV 다운 (파일생성) (경로, 조회값, 헤더, 상세, 파일명, 리스폰)
	 */
	public void downCsv(String path, GenericResultSet grs, String[] header,
			String[] detail, String fName, HttpServletResponse response) {

		BufferedOutputStream bos = null;
		String csvFileName = fName + ".csv";

		try {
			if (!CommonUtil.isNull(path)) { // 수정 : 경로가 null이 아닐경우 경로가 존재하지않으면
											// 경로를 생성한다.
				if (!FileUtil.existDirectory(path)) {
					// throw new Exception("file path unKnow.....");
					FileHelper.createDirectorys(path);
				}
			}
			File csvFile = new File(path, csvFileName);

			FileOutputStream fileOutputStream = new FileOutputStream(csvFile);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					fileOutputStream, "MS949");
			BufferedWriter bufferedWriter = new BufferedWriter(
					outputStreamWriter);

			// CSV 데이터생성
			List vlist = grs.getList();
			Map exlistMap = null;
			/*
				String line = "";
				for (int i = 0; i < vlist.size(); i++) {
					line = "";
					exlistMap = (Map) vlist.get(i);
					// 헤더내용
					if (i == 0) {
						for (int j = 0; j < header.length; j++) {
							line += String.valueOf(exlistMap.get(header[j])) + ",";
						}
						line += "\r\n\n";
						bufferedWriter.write(line);
						line = "";
					}
					// 상세내용
					for (int j = 0; j < detail.length; j++) {
						line += String.valueOf(exlistMap.get(detail[j])) + ",";
					}
					line += "\r\n";
					bufferedWriter.write(line);
				}
			*/
			
			StringBuffer line = null;
			for (int i = 0; i < vlist.size(); i++) {
				line = new StringBuffer();
				exlistMap = (Map) vlist.get(i);
				// 헤더내용
				if (i == 0) {
					for (int j = 0; j < header.length; j++) {
						line.append(String.valueOf(exlistMap.get(header[j]))).append(",");
					}
					line.append("\r\n\n");
					bufferedWriter.write(line.toString());
					line = new StringBuffer();
				}
				// 상세내용
				for (int j = 0; j < detail.length; j++) {
					line.append(String.valueOf(exlistMap.get(detail[j]))).append(",");
				}
				line.append("\r\n");
				bufferedWriter.write(line.toString());
			}
			
			bufferedWriter.flush();
			bufferedWriter.close();
			// CSV데이터생성끝

			FileInputStream fin = new FileInputStream(csvFile);
			int ifilesize = (int) csvFile.length();
			byte b[] = new byte[ifilesize];

			response.reset();

			response.setHeader("Content-Disposition", "attachment;filename="
					+ URLEncoder.encode(csvFileName, "UTF-8") + ";");
			response.setHeader("Content-Type", "text/csv; charset=MS949");

			response.setContentLength(ifilesize);

			bos = new BufferedOutputStream(response.getOutputStream());

			fin.read(b);
			bos.write(b, 0, ifilesize);

			bos.flush();

			if (bos != null) {
				bos.close();
			}
			if (fin != null) {
				fin.close();
			}

		} catch (Exception e) {
			if (!e.getClass()
					.getName()
					.equals("org.apache.catalina.connector.ClientAbortException")) {
				// e.printStackTrace();
				log.error("download fail");
			}
			if (FileUtil.existFile(path, csvFileName)) {
				FileUtil.delFile(path, csvFileName);
			}
		} finally {
			if (FileUtil.existFile(path, csvFileName)) {
				FileUtil.delFile(path, csvFileName);
			}
		}
	}

	public static List<Map<String, String>> readCsvFile(
			Map<String, Object> parameterMap, String fileName, int startReadRow,
			int startReadCol) throws BizException {
		
		File file		= null;
		List<Map<String, String>> mapList 	= null;

		try {		
			String delimter  = (parameterMap.get("delimiter") != null)?parameterMap.get("delimiter").toString():",";
			int columnCnt    = Integer.parseInt((String)parameterMap.get("header_count"));
			
			if(StringUtils.isEmpty(fileName) || columnCnt == 0) {
				throw new BizException(MessageResolver.getMessage("common.csv.read.fail"));
			}	
			file = new File(fileName);
			
			if(!file.exists()) {
				throw new BizException(MessageResolver.getMessage("common.csv.read.fail"));
			}
			
			String[] columnKeys = new String[columnCnt];			
			for(int i = 0; i < columnCnt; i++) {
				columnKeys[i] = (String)parameterMap.get("COLUMN_NAME_" + i);
			}

			List<String> lines = FileUtils.readLines(file, ConstantIF.CSV_FILE_ENCODING);			
			if(!lines.isEmpty()) {					
				mapList = new ArrayList<Map<String, String>>();
				
				for(int i = 0; i < lines.size(); i++) {					
					HashMap<String, String> map = new HashMap();
					
					if(i < startReadRow) {
						continue;
					}	
					String[] cols = null;
					if(ConstantIF.CSV_DELIMITER_TAB.equals(delimter)) {						
						cols = lines.get(i).split("\\t");
						
					} else if(ConstantIF.CSV_DELIMITER_COMMA.equals(delimter)) {						
						cols = lines.get(i).split("\\,");
						
					}
					
					if(cols.length > 0) { 
						for(int j = 0; j < cols.length; j++) {							
							String col = null;							
							if(cols[j].startsWith("\"")) {
								col = cols[j].substring(1);
							} else {
								col = cols[j];
							}
							if(cols[j].endsWith("\"")) {
								col = col.substring(0, col.length() - 1);
							}
							map.put(columnKeys[j], col);
						}
					}					
					mapList.add(map);
				}
				
			}
        }catch(Exception e) {
			throw new BizException(MessageResolver.getMessage("common.csv.read.fail"));
		} finally {
			try {
				if(file != null) {
					file.deleteOnExit();
				}
			} catch(Exception e) {}
		}
		return mapList;		
	}

}
