package com.logisall.winus.frm.common.util;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.m2m.jdfw5x.util.CommonUtil;
import com.m2m.jdfw5x.util.file.FileHelper;
import com.m2m.jdfw5x.util.file.JDFW5XUploader;
//import com.oreilly.servlet.MultipartRequest;

public class JDFW5XUploaderUln4 extends JDFW5XUploader implements Servlet {

	private static final long serialVersionUID = 1L;
	protected Log loger;
	private long totalSizeLimit;
	private String baseSizeLimit;
	private String baseUploadDir;
	private String baseCharSet;

	public JDFW5XUploaderUln4() {
		loger = LogFactory.getLog(getClass());
		totalSizeLimit = 104857600L;
		baseSizeLimit = "20";
		baseUploadDir = ConstantIF.FILE_ATTACH_STATEMENT_PATH + "WINUS";
		baseCharSet = "UTF-8";
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FileItemFactory factory  = null;
		ServletFileUpload upload = null;
		List uploadedItems 		 = null;
		FileItem fileItem        = null;
		FileItem fieldItem       = null;
		
		PrintWriter out    		 = null;
		StringBuffer bf    		 = null;
		StringBuffer subBf 		 = null;	
		
		String filePath    = "";
		String filePath2   = "";
		String fileLimit   = "";
		String filePostion = "";
		String fileIdx     = "";		
		String yyyymm	   = "";
		String lcid		   = "";
		String custid	   = "";
		String filenum	   = "";
		
		boolean isMultipart      = false;		

		try {			
			request.setCharacterEncoding(baseCharSet);
			factory     = new DiskFileItemFactory();
			upload      = new ServletFileUpload(factory);
			isMultipart = ServletFileUpload.isMultipartContent(request);
			
			if (!isMultipart) {
				throw new ServletException("form\uC758 enctype\uC744 multipart/form-data\uB85C \uD558\uC138\uC694...");
			}
			
			out   = response.getWriter();
			bf    = new StringBuffer();
			subBf = new StringBuffer();
			
			response.setContentType("text/html;charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			upload.setSizeMax(totalSizeLimit);
			uploadedItems = upload.parseRequest(request);

			for (Iterator k = uploadedItems.iterator(); k.hasNext();) {
				fieldItem = (FileItem) k.next();
				if (fieldItem.isFormField() && fieldItem.getSize() > 0L) {
					if ("filePath".equals(fieldItem.getFieldName()))
						filePath = fieldItem.getString();					
					if ("fileLimit".equals(fieldItem.getFieldName()))
						fileLimit = fieldItem.getString();
					if ("filePostion".equals(fieldItem.getFieldName()))
						filePostion = fieldItem.getString();
					if ("fileIdx".equals(fieldItem.getFieldName()))
						fileIdx = fieldItem.getString();
					if ("yyyymm".equals(fieldItem.getFieldName()))
						yyyymm = fieldItem.getString();
					if ("lcid".equals(fieldItem.getFieldName()))
						lcid = fieldItem.getString();
					if ("custid".equals(fieldItem.getFieldName()))
						custid = fieldItem.getString();
					if ("filenum".equals(fieldItem.getFieldName()))
						filenum = fieldItem.getString();
				}
			}

			if (CommonUtil.isNull(filePath))
				filePath = baseUploadDir;
			if (CommonUtil.isNull(fileLimit))
				fileLimit = baseSizeLimit;
			
			filePath2 = (new StringBuilder(String.valueOf(filePath))).append("/").append(filePostion).toString();
			// 대분류 폴더 생성
			if (!FileHelper.existDirectory(filePath2))
				FileHelper.createDirectorys(filePath2);			
			
			filePath = (new StringBuilder(String.valueOf(filePath2))).append("/").append("WINUS02").append("/")
					.append(yyyymm).append("/").append(lcid).append("/").append(custid).toString();

			// 중분류 폴더 생성
			if (!FileHelper.existDirectory(filePath))
				FileHelper.createDirectorys(filePath);
			
			Iterator i = uploadedItems.iterator();
			subBf.append("status:0");
			
			while (i.hasNext()) {
				fileItem = (FileItem) i.next();
				if (!fileItem.isFormField() && fileItem.getSize() > 0L) {										
					String fileIdxTemp = "";
					String fileNewNm   = "";
					switch(fileItem.getFieldName()){
						case "txtFile0":
							fileIdxTemp = "0";
							fileNewNm   = filenum;
							break;
						case "txtFile1":
							fileIdxTemp = "1";
							fileNewNm   = filenum;
							break;
						case "txtFile2":
							fileIdxTemp = "2";
							fileNewNm   = filenum;
							break;
						default:
							fileIdxTemp = fileIdx;
							fileNewNm   = filenum;
							break;
					}
					
					long fileSize = fileItem.getSize();
					String fileName = (new File(fileItem.getName())).getName();
					
					String tempFilePosition =  new StringBuilder("/")
							.append(filePostion).append("/").append("WINUS02").append("/").append(yyyymm).append("/").append(lcid).append("/").append(custid)
							.toString();
									
					subBf.append("^").append(tempFilePosition).append("||").append(fileName).append("||").append(fileSize).append("||").append(fileIdxTemp);
					
					File uploadedFile 	  = null;					
					String myFullFileName = fileItem.getName();
					String myFileName 	  = "";
					String slashType 	  = myFullFileName.lastIndexOf("\\") <= 0 ? "/" : "\\";
					int startIndex 		  = myFullFileName.lastIndexOf(slashType);
					myFileName 			  = myFullFileName.substring(startIndex + 1, myFullFileName.length());
					
					String setExt = "";
					int extIndex  = myFileName.lastIndexOf(".");
					setExt 		  = myFileName.substring(extIndex + 1, myFileName.length());					
					uploadedFile  = new File(filePath, fileNewNm + "." + setExt);					
					fileItem.write(uploadedFile);
				}
			}
			
			subBf.append("||").append(fileIdx);
			bf.append("<script type=\"text/javascript\">\n");
			bf.append("<!--//\n");
			bf.append("\t if(typeof parent.uploaderCallback != 'undefined'){\n");
			bf.append((new StringBuilder("\t parent.uploaderCallback('")).append(subBf.toString()).append("')\n")
					.toString());
			bf.append("\t }\n");
			bf.append("//--></script>");
			out.println(bf.toString());
			out.flush();
			out.close();
		} catch (FileUploadException e) {
			FileHelper.deleteSubAll(filePath);
			response.setContentType("text/html;charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			bf.append("<script type=\"text/javascript\">\n");
			bf.append("<!--//\n");
			bf.append("\t if(typeof parent.uploaderCallback != 'undefined'){\n");
			bf.append((new StringBuilder("\t parent.uploaderCallback('status:-1^")).append(e.getMessage())
					.append("')\n").toString());
			bf.append("\t }\n");
			bf.append("//--></script>");
			out.println(bf.toString());
			out.flush();
			out.close();
		} catch (Exception e) {
			FileHelper.deleteSubAll(filePath);
			response.setContentType("text/html;charset=UTF-8");
			response.setHeader("Cache-Control", "no-cache");
			bf.append("<script type=\"text/javascript\">\n");
			bf.append("<!--//\n");
			bf.append("\t if(typeof parent.uploaderCallback != 'undefined'){\n");
			bf.append((new StringBuilder("\t parent.uploaderCallback('status:-1^")).append(e.getMessage())
					.append("')\n").toString());
			bf.append("\t }\n");
			bf.append("//--></script>");
			out.println(bf.toString());
			out.flush();
			out.close();
		}
	}
}