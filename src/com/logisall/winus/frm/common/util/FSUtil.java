package com.logisall.winus.frm.common.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.fasoo.adk.packager.*;

@SuppressWarnings("PMD")
public final class FSUtil {

	protected static final Log logger = LogFactory.getLog(FSUtil.class);
	
	private FSUtil() {
	}

	public static void fileDecrypt(String filefullpath, String newFilefullpath) {

		/*
			filefullpath = "C:\\Project\\workspace\\WINUSJPR_DEV\\temp\\excelTemp6995842844907741027TEST_DES.xls";
			newFilefullpath = "C:\\Project\\workspace\\WINUSJPR_DEV\\temp\\excelTemp6995842844907741027TEST_DES_TEST.xls";
		*/
			logger.info("[ java.library.path    ] :" + System.getProperty("java.library.path"));
					
			logger.info("[ filefullpath    ] :" + filefullpath);
			logger.info("[ newFilefullpath ] :" + newFilefullpath);
				
		// 암호화 파일일 경우 해지한다.		
		try {
			WorkPackager oWorkPackager = new WorkPackager();
			logger.info("[ oWorkPackager    ] :" + oWorkPackager);
			boolean nret = oWorkPackager.IsPackageFile(filefullpath);
			logger.info("[ nret    ] :" + nret);
			if (nret == true) {
				boolean bret = oWorkPackager.DoExtract(
						"C:\\FasooAPI\\fsdinit"
						, "0000000000001221"
						, filefullpath
						, newFilefullpath);
				logger.info("[ bret ] :" + bret);
			}
		} catch (Exception e) {
			if (logger.isErrorEnabled()) {
				logger.error("Fail to Dile Decrypt :", e);
			}
		}
	}

}
