package com.logisall.winus.frm.common.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.logisall.winus.frm.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

public final class ServiceUtil {
    
    private static final String RESULT_CODE_FOR_NO_ERROR                = "0";    
    private static final String RESULT_MSG_FOR_SP_ERROR                	= "ORA-";
    
    private static final Logger LOG = Logger.getLogger(ServiceUtil.class);
    
    private ServiceUtil() {}
    
    public static void checkInputValidation(Map<String, Object> model, String[] checkArray) throws Exception {
        for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
            for(int j = 0 ; j < checkArray.length ; j ++){
                if ( StringUtils.isEmpty((String)model.get(checkArray[j])) ) {
                    throw new BizException( MessageResolver.getMessage("입력값유효성체크", new String[]{checkArray[j]}) );
                }
            }
        }
    }

    public static void checkInputValidation2(Map<String, Object> model, String[] checkArray) throws Exception {
        for(int j = 0 ; j < checkArray.length ; j ++){
            String[] chk = (String[])model.get(checkArray[j]);
            for(int i = 0; i < chk.length ; i ++){
                if ( StringUtils.isEmpty(chk[i]) ) {
                    throw new BizException( MessageResolver.getMessage("입력값유효성체크", new String[]{chk[i]}) );
                }
            }
        }
    }
    
    public static String getComboOptionStr(List<Map<String, Object>> targetList, String key, String value) {
        StringBuffer sb = new StringBuffer();
        sb.append("value:");
        Map<String, Object> elem = null;
        int cnt =0;
        for (int i=0; i<targetList.size(); i++) {
            elem = (Map<String, Object>)targetList.get(i);
            if(cnt > 0){
                sb.append(";").append((String)elem.get(key)).append(":").append((String)elem.get(value));   
            }else{                      
                sb.append("'").append("").append(":").append(";");
                sb.append((String)elem.get(key)).append(":").append((String)elem.get(value));
            }
            cnt++;
        }
        sb.append("'");
        return sb.toString();
    }
    
    // 정산관련 SP결과코드 판정
    public static void isValidReturnCode(String workName, String retCode, String retMsg) throws BizException {
        boolean retFlag = true;
        int retCd = -1;        
        String errCode = "";        
        // Log.info("[ workName ] : " + workName + ", [ retCode ] : " + retCode + ", [ retMsg ] : " + retMsg);
        
        // 결과코드가 NOT Null 이고, 설정값이 있을때만 확인
        if ( retCode != null && retCode.trim().length() > 0 ) {
            
            // 결과코드가 '정상'일때
            if ( RESULT_CODE_FOR_NO_ERROR.equals(retCode) ) {
                retFlag = true;
                
             // 그외 결과코드가 음수가 아닐때  
            } else {                   
                try {
                    retCd = Integer.parseInt(retCode);
                    
                    errCode = String.valueOf(Math.abs(retCd));
                } catch (Exception e) {
                    StringBuffer sb = new StringBuffer()
                                .append("[").append( workName ).append("] ").append( retCode ).append("/").append( retMsg );
                    throw new BizException(sb.toString());
                }                
                // 그외 결과코드가 음수가 아닐때 유효한 결과로 판정
                if ( retCd > -1) {
                    retFlag = true;
                } else {
                	retFlag = false;                	
                }
            }
            
        }
        
        if ( !retFlag ) {
        	/*
	            StringBuffer sb = new StringBuffer()
	                        .append("[").append( workName ).append("] ").append( retCode ).append("/").append( retMsg );
	            throw new BizException(sb.toString());
            */
        	if ( StringUtils.isNotEmpty(retMsg) && retMsg.contains(RESULT_MSG_FOR_SP_ERROR) ) {
        		errCode = retMsg.substring(retMsg.indexOf(RESULT_MSG_FOR_SP_ERROR) + RESULT_MSG_FOR_SP_ERROR.length(), retMsg.indexOf(RESULT_MSG_FOR_SP_ERROR) + RESULT_MSG_FOR_SP_ERROR.length() + 5);
        	} 
        	if (LOG.isDebugEnabled()) {
        		LOG.debug("[ workName ] : " + workName + ", [ errCode ] : " + errCode + ", [ retCode ] : " + retCode + ", [ retMsg ] : " + retMsg);
        	}         	
        	
        	String message = null;
        	try {
        		message = MessageResolver.getMessage( errCode, new String[]{errCode} );
        	} catch(Exception e) {
        		StringBuffer sb = new StringBuffer()
                .append("[").append( workName ).append("] ").append( retCode ).append("/").append( retMsg );
        		message = sb.toString();
        	}        	
        	throw new BizException( message );
        }
    }
}
