package com.logisall.winus.frm.common.util;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

/**
 * @author chSong
 *         ExcelWriter 참고했음
 *
 */

public class CsvWriter {
	protected Log log = LogFactory.getLog(this.getClass());
	
	private HttpServletResponse response;
	
	public void setResponse(HttpServletResponse response) {
        this.response = response;
    }
    public HttpServletResponse getResponse() {
        return response;
    }
	    
    
    /* 
     *  CSV 다운 (파일생성)
     *  (경로, 조회값, 헤더, 상세, 파일명, 리스폰)
     */
    public void downCsv(String path, GenericResultSet grs, String[] header,  String[] detail, String fName, HttpServletResponse response){
        
        BufferedOutputStream bos = null; 
        String csvFileName = fName + ".csv";
        
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter = null; 
        BufferedWriter bufferedWriter = null;
        
        FileInputStream fin = null;
        try{
            if(!CommonUtil.isNull(path)){        // 수정 : 경로가 null이 아닐경우 경로가 존재하지않으면 경로를 생성한다.
                if(!FileUtil.existDirectory(path)){
//                  throw new Exception("file path unKnow.....");
                    FileHelper.createDirectorys(path);
                }
            } 
            File csvFile = new File(path,csvFileName);
            
            fileOutputStream = new FileOutputStream(csvFile);
            outputStreamWriter = new OutputStreamWriter(fileOutputStream, "MS949"); 
            bufferedWriter = new BufferedWriter(outputStreamWriter);

            //CSV 데이터생성
            List vlist = grs.getList();
            Map exlistMap = null;
            /*
	            String line =  "";            
	            for(int i = 0 ; i < vlist.size() ; i++){
	                line =  "";
	                exlistMap = (Map)vlist.get(i);
	                //헤더내용
	                if(i == 0){
	                    for(int j = 0 ; j < header.length ; j++){
	                        line += String.valueOf(exlistMap.get(header[j])) + ",";   
	                    }
	                    line += "\r\n\n";
	                    bufferedWriter.write(line);
	                    line =  "";
	                }
	                //상세내용
	                for(int j = 0 ; j < detail.length ; j ++){
	                    line += String.valueOf(exlistMap.get(detail[j])) + ",";   
	                }
	                line += "\r\n";
	                bufferedWriter.write(line);
	            }
	            bufferedWriter.flush();
            */
            StringBuffer line =  null;            
            for(int i = 0 ; i < vlist.size() ; i++){
                line =  new StringBuffer();
                exlistMap = (Map)vlist.get(i);
                if(i == 0){
                    for(int j = 0 ; j < header.length ; j++){
                        line.append(String.valueOf(exlistMap.get(header[j]))).append(",");   
                    }
                    line.append("\r\n\n");
                    bufferedWriter.write(line.toString());
                    line =  new StringBuffer();
                }
                //상세내용
                for(int j = 0 ; j < detail.length ; j ++){
                    line.append(String.valueOf(exlistMap.get(detail[j]))).append(",");   
                }
                line.append("\r\n");
                bufferedWriter.write(line.toString());
            }
            bufferedWriter.flush();            
            
            fin = new FileInputStream(csvFile);
            int ifilesize = (int)csvFile.length();
            byte[] b = new byte[ifilesize];

            response.reset();
            response.setHeader("Content-Disposition", "attachment;filename="+ URLEncoder.encode(csvFileName, "UTF-8") + ";");
            response.setHeader("Content-Type", "text/csv; charset=MS949");
            response.setContentLength(ifilesize);
            
            bos = new BufferedOutputStream(response.getOutputStream());
            fin.read(b);
            bos.write(b,0,ifilesize);
            bos.flush();
            
        }catch(Exception e){
            if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
                //e.printStackTrace();
                log.error("download fail");
            }
            if(FileUtil.existFile(path,csvFileName)){
                FileUtil.delFile(path, csvFileName);
            }
        }finally{
        	if (bufferedWriter != null) {
				try {
					bufferedWriter.close();
				} catch (Exception e) {
				}
			}
        	if (outputStreamWriter != null) {
				try {
					outputStreamWriter.close();
				} catch (Exception e) {
				}
			}
        	if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (Exception e) {
				}
			}        	
        	if (bos != null) {
				try {
					bos.close();
				} catch (Exception e) {
				}
			}
			if (fin != null) {
				try {
					fin.close();
				} catch (Exception e) {
				}
			}        	
            if(FileUtil.existFile(path,csvFileName)){
                FileUtil.delFile(path, csvFileName);
            }
        }
    }

}
