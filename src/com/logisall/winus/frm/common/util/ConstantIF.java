package com.logisall.winus.frm.common.util;

import java.security.Key;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.net.util.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.SessionUtil;

/**
 * @author indigo(HONG SIK)
 *
 */
public class ConstantIF {
	
	public static final Locale LOCALE 		= Locale.KOREAN;
	
	public static final int LANGUAGE_TYPE_KOREAN       = 0;
	public static final int LANGUAGE_TYPE_JAPANESE     = 1;
	public static final int LANGUAGE_TYPE_CHINESE      = 2;
	public static final int LANGUAGE_TYPE_ENGLISH      = 3;
	public static final int LANGUAGE_TYPE_VIETNAM      = 4;
	
	//FTP관련 임시 추가
	public static final String FTP_SHELL_PATH          = "";
	
	//물류센터 관리자 ULN물류센터
	public static final String LC_ADMIN = "10015";
	
	//회사별 관리자
	public static final String[] USER_ADMIN = {"admin", "JINARAGO", "keun", "MASTER"};
	
	//고객사 ID    USER_GB = 20 
	public static final String CUST_ID ="20";
	
	public static final String ENCODING_TYPE_JSON_FILE ="UTF-8";	
	
	@Value("#{config['path.base.root']}")
	public static String BASE_ROOT = "";
	
	@Value("#{config['path.web.base.root']}")
	public static String WEB_BASE_ROOT = "";
	
	@Value("#{config['path.temp']}")
	public static String TEMP_PATH = "";

	@Value("#{config['path.file.attach']}")
	public static String FILE_ATTACH_PATH = "";
	
	@Value("#{config['path.file.attach.statement']}")
	public static String FILE_ATTACH_STATEMENT_PATH = "";
	
	@Value("#{config['path.file.attach.winustemp']}")
	public static String FILE_ATTACH_WINUSTEMP_PATH = "";
	
	@Value("#{config['path.server.pdf']}")
	public static String SERVER_PDF_PATH = "";
	
	@Value("#{config['path.web.font..title']}")
	public static String FONT_TITLE = "";
	
	@Value("#{config['path.web.font.normal']}")
	public static String FONT_NORMAL = "";	
	
	@Value("#{config['path.report.jasper']}")
	public static String REPORT_FILE_PATH = "";
	
	@Value("#{config['file.report1']}")
	public static String REPORT_FILE_1 = "";	
	
	@Value("#{config['file.report2']}")
	public static String REPORT_FILE_2 = "";	
	
	@Value("#{config['file.report3_godo']}")
	public static String REPORT_FILE_3_GODO = "";	
	
	@Value("#{config['file.report3']}")
	public static String REPORT_FILE_3 = "";	
	
	@Value("#{config['file.report3_v2']}")
	public static String REPORT_FILE_3_V2 = "";	
	
	@Value("#{config['file.report3_v3']}")
	public static String REPORT_FILE_3_V3 = "";	
	
	@Value("#{config['file.report3_v4']}")
	public static String REPORT_FILE_3_V4 = "";
	
	@Value("#{config['file.report4']}")
	public static String REPORT_FILE_4 = "";	
	
	@Value("#{config['file.report5']}")
	public static String REPORT_FILE_5 = "";
	
	@Value("#{config['file.report6']}")
	public static String REPORT_FILE_6 = "";
	
	@Value("#{config['file.report7']}")
	public static String REPORT_FILE_7 = "";
	
	@Value("#{config['file.report8']}")
	public static String REPORT_FILE_8 = "";
	
	@Value("#{config['file.report9']}")
	public static String REPORT_FILE_9 = "";
	
	@Value("#{config['file.report10']}")
	public static String REPORT_FILE_10 = "";	
	
	@Value("#{config['file.report11']}")
	public static String REPORT_FILE_11 = "";

	@Value("#{config['file.wmspk020_01']}")
	public static String REPORT_WMSPK020_01 = "";	
	
	@Value("#{config['file.wmspk020_02']}")
	public static String REPORT_WMSPK020_02 = "";
	
	public void setBASE_ROOT(String BASE_ROOT) {
		ConstantIF.BASE_ROOT = BASE_ROOT;
	}
	
	public void setWEB_BASE_ROOT(String WEB_BASE_ROOT) {
		ConstantIF.WEB_BASE_ROOT = WEB_BASE_ROOT;
	}
	
	public void setTEMP_PATH(String TEMP_PATH) {
		ConstantIF.TEMP_PATH = TEMP_PATH;
	}	
	
	public void setFILE_ATTACH_PATH(String FILE_ATTACH_PATH) {
		ConstantIF.FILE_ATTACH_PATH = FILE_ATTACH_PATH;
	}
	
	public void setFILE_ATTACH_STATEMENT_PATH(String FILE_ATTACH_STATEMENT_PATH) {
		ConstantIF.FILE_ATTACH_STATEMENT_PATH = FILE_ATTACH_STATEMENT_PATH;
	}
	
	public void setFILE_ATTACH_WINUSTEMP_PATH(String FILE_ATTACH_WINUSTEMP_PATH) {
		ConstantIF.FILE_ATTACH_WINUSTEMP_PATH = FILE_ATTACH_WINUSTEMP_PATH;
	}
	
	public void setSERVER_PDF_PATH(String SERVER_PDF_PATH) {
		ConstantIF.SERVER_PDF_PATH = SERVER_PDF_PATH;
	}
	
	public void setFONT_TITLE(String FONT_TITLE) {
		ConstantIF.FONT_TITLE = FONT_TITLE;
	}
	
	public void setFONT_NORMAL(String FONT_NORMAL) {
		ConstantIF.FONT_NORMAL = FONT_NORMAL;
	}
	
	public void setREPORT_FILE_PATH(String REPORT_FILE_PATH) {
		ConstantIF.REPORT_FILE_PATH = REPORT_FILE_PATH;
	}
	
	public void setREPORT_FILE_1(String REPORT_FILE_1) {
		ConstantIF.REPORT_FILE_1 = REPORT_FILE_1;
	}	
	
	public void setREPORT_FILE_2(String REPORT_FILE_2) {
		ConstantIF.REPORT_FILE_2 = REPORT_FILE_2;
	}	
	
	public void setREPORT_FILE_3_GODO(String REPORT_FILE_3_GODO) {
		ConstantIF.REPORT_FILE_3_GODO = REPORT_FILE_3_GODO;
	}
	
	public void setREPORT_FILE_3(String REPORT_FILE_3) {
		ConstantIF.REPORT_FILE_3 = REPORT_FILE_3;
	}	
	
	public void setREPORT_FILE_3_V2(String REPORT_FILE_3_V2) {
		ConstantIF.REPORT_FILE_3_V2 = REPORT_FILE_3_V2;
	}
	
	public void setREPORT_FILE_3_V3(String REPORT_FILE_3_V3) {
		ConstantIF.REPORT_FILE_3_V3 = REPORT_FILE_3_V3;
	}
	
	public void setREPORT_FILE_3_V4(String REPORT_FILE_3_V4) {
		ConstantIF.REPORT_FILE_3_V4 = REPORT_FILE_3_V4;
	}
	
	public void setREPORT_FILE_4(String REPORT_FILE_4) {
		ConstantIF.REPORT_FILE_4 = REPORT_FILE_4;
	}	
	
	public void setREPORT_FILE_5(String REPORT_FILE_5) {
		ConstantIF.REPORT_FILE_5 = REPORT_FILE_5;
	}		
	
	public void setREPORT_FILE_6(String REPORT_FILE_6) {
		ConstantIF.REPORT_FILE_6 = REPORT_FILE_6;
	}	
	
	public void setREPORT_FILE_7(String REPORT_FILE_7) {
		ConstantIF.REPORT_FILE_7 = REPORT_FILE_7;
	}	
	
	public void setREPORT_FILE_8(String REPORT_FILE_8) {
		ConstantIF.REPORT_FILE_8 = REPORT_FILE_8;
	}	
	
	public void setREPORT_FILE_9(String REPORT_FILE_9) {
		ConstantIF.REPORT_FILE_9 = REPORT_FILE_9;
	}	
	
	public void setREPORT_FILE_10(String REPORT_FILE_10) {
		ConstantIF.REPORT_FILE_10 = REPORT_FILE_10;
	}		
	
	public void setREPORT_FILE_11(String REPORT_FILE_11) {
		ConstantIF.REPORT_FILE_11 = REPORT_FILE_11;
	}
	
	public void setREPORT_WMSPK020_01(String REPORT_WMSPK020_01) {
		ConstantIF.REPORT_WMSPK020_01 = REPORT_WMSPK020_01;
	}
	
	public void setREPORT_WMSPK020_02(String REPORT_WMSPK020_02) {
		ConstantIF.REPORT_WMSPK020_02 = REPORT_WMSPK020_02;
	}
	
	// + ready/  전송대기
	// + send/  전송완료
	//세션관련
	public static final String SS_USER_ID 			= "SS_USER_ID";		    // 세션.사용자.아이디
	public static final String SS_USER_NO 			= "SS_USER_NO";		    // 세션.사용자.번호
	public static final String SS_USER_GB 			= "SS_USER_GB";		    // 세션.사용자.구분
	public static final String SS_USER_NAME 		= "SS_USER_NAME";		    // 세션.사용자.이름
	public static final String SS_USER_TYPE 		= "SS_USER_TYPE";		// 세션.사용자.타입
	public static final String SS_SVC_NO   			= "SS_SVC_NO";			// 세션.사용자.서비스번호
	public static final String SS_SVC_NAME   		= "SS_SVC_NAME";		// 세션.사용자.서비스이름
	public static final String SS_CMPY_CODE 		= "SS_CMPY_CODE";		// 세션.사용자.소속회사코드
	public static final String SS_CMPY_ID 		    = "SS_CMPY_ID";		    // 세션.사용자.소속회사아이디
	public static final String SS_CMPY_NAME 		= "SS_CMPY_NAME";		// 세션.사용자.소속회사명
	public static final String SS_SALES_CUST_ID 		= "SS_SALES_CUST_ID";		// 세션.사용자.소속회사명
	
	public static final String SS_CLIENT_IP   		= "SS_CLIENT_IP";		// 세션.사용자.(클라이언트)접속IP
	public static final String SS_LANG   		    = "SS_LANG";		    // 세션.사용자.언어
	public static final String SS_LANG_TYPE   		= "SS_LANG_TYPE";	     // 세션.사용자.언어타입(0,1,2,3, 4)
	public static final String SS_AUTH_LEVEL        = "SS_AUTH_LEVEL";      // 세션.사용자.레벨
	public static final String SS_CLIENT_CD         = "SS_CLIENT_CD";      // 세션.사용자.거래처(업체)코드
	public static final String SS_SESSION_ID   	    = "SS_SESSION_ID";		// 세션.사용자.세션아이디
	public static final String SS_GIRD_NUM   	    = "SS_GIRD_NUM";		// 세션.사용자.세션아이디
	public static final String SS_SVC_USE_TGT   	= "SS_SVC_USE_TGT";		// 물류센터운영대상
	public static final String SS_SVC_USE_TY   		= "SS_SVC_USE_TY";		// 물류센터운영타입
	public static final String SS_LC_CHANGE_YN   	= "SS_LC_CHANGE_YN";		// 물류센터변경타입
	public static final String SS_ITEM_FIX_YN   	= "SS_ITEM_FIX_YN";		// 특정상품조회권한
	public static final String SS_ITEM_GRP_ID   	= "SS_ITEM_GRP_ID";		// 대표상품군
	public static final String SS_SUB_PASSWORD   	= "SS_SUB_PASSWORD";		// 기타 비밀번호
	public static final String SS_PROC_AUTH   		= "SS_PROC_AUTH";		// 저장처리권한
	public static final String SS_USE_TRANS_CUST   	= "SS_USE_TRANS_CUST";		// 
	public static final String SS_TEL_NO   			= "SS_TEL_NO";		// 전화번호
	public static final String SS_LOGIN_DATE               = "SS_LOGIN_DATE";            // 세션.사용자.로그인시간
	public static final String SS_LOGIN_SESSION_TIME       = "SS_LOGIN_SESSION_TIME";    // 세션.사용자.세션시간
	public static final String SS_USER_LOCALE              = "SS_USER_LOCALE";           // 세션.사용자.로케일(다국어설정 관련)
	public static final String SS_MULTI_USE_GB   	       = "SS_MULTI_USE_GB";
	public static final String SS_STOCK_ALERT_YN   	       = "SS_STOCK_ALERT_YN";
	public static final String SS_LC_CUST_NM   	      	   = "SS_LC_CUST_NM";
	
	public static final String SS_M_DO					= "SS_M_DO";
	public static final String SS_DETAIL_PS   	      	= "SS_DETAIL_PS";

	public static final String MAIL_SERVER_ADDRESS = "";//"mail.m2mglobal.co.kr";					//메일 서버 주소	
	
	
	public static final String PROPERTY_FILE_ENCODING          	= "utf-8";
	
    public static final String CSV_FILE_ENCODING          		= "ms949";
    public static final String CSV_DELIMITER_COMMA          	= "COMMA";
    public static final String CSV_DELIMITER_TAB          		= "TAB";
	
	public static final String REALPACKING_AUTHKEY = "6e6acdf95e1057fd33e29289a7ca034842bb1e317b3a9616536e88f5dbb1319f";
	public static final String REALPACKING_URL = "http://www.realpacking.com/api/rp_video_view.php";
    
    public static Key getAESKey() throws Exception {
        String iv;
        Key keySpec;

        String key = "1234567890123456";
        iv = key.substring(0, 16);
        byte[] keyBytes = new byte[16];
        byte[] b = key.getBytes("UTF-8");

        int len = b.length;
        if (len > keyBytes.length) {
           len = keyBytes.length;
        }

        System.arraycopy(b, 0, keyBytes, 0, len);
        keySpec = new SecretKeySpec(keyBytes, "AES");

        return keySpec;
    }

    // 암호화
    public static String encAES(String str) throws Exception {
        Key keySpec = getAESKey();
        String iv = "0987654321654321";
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.ENCRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes("UTF-8")));
        byte[] encrypted = c.doFinal(str.getBytes("UTF-8"));
        String enStr = new String(Base64.encodeBase64(encrypted));

        return enStr;
    }

    // 복호화
    public static String decAES(String enStr) throws Exception {
        Key keySpec = getAESKey();
        String iv = "0987654321654321";
        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.DECRYPT_MODE, keySpec, new IvParameterSpec(iv.getBytes("UTF-8")));
        byte[] byteStr = Base64.decodeBase64(enStr.getBytes("UTF-8"));
        String decStr = new String(c.doFinal(byteStr), "UTF-8");

        return decStr;
    }
}