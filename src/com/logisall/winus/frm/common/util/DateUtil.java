package com.logisall.winus.frm.common.util;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.SimpleTimeZone;
import java.util.TimeZone;

/**
 * <PRE>
 * 일자관련 공통 Util
 * </PRE>
 * 
 * @author yjw
 * 
 * @version 1.0
 */
public class DateUtil {
	private static SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat formatter2 = new SimpleDateFormat("HH:mm:ss");
	private static final String DBDATEFORMAT = "";
	Date today = new Date();

	public java.util.Date now;

	public static void setDBDateFormat(Connection con) {
		String sql = "alter session set nls_date_format ='yyyy-mm-dd HH24:MI:SS'";
		Statement stmt = null;
		try {
			stmt = con.createStatement();
			stmt.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null) {
					stmt.close();
				}
			} catch (Exception xx) {
			}
		}
	}

	/**
	 * <P>
	 * 
	 * 일자관련 공통 Util
	 * <P>
	 * 
	 */
	public DateUtil() {
		now = new java.util.Date();
	}

	/**
	 * <P>
	 * 
	 * 현재(System TimeZone 및 Locale 기준 ) 날짜/시간정보를 얻는다.
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  String  DateStr = getLocalDateTime()
	 * </PRE>
	 * 
	 * @param none
	 * @return String
	 */
	public static String getLocalDateTime() {
		Calendar calLocal = Calendar.getInstance();
		return "" + calLocal.get(Calendar.YEAR) + makeTowDigit(calLocal.get(Calendar.MONTH) + 1)
				+ makeTowDigit(calLocal.get(Calendar.DATE)) + makeTowDigit(calLocal.get(Calendar.HOUR_OF_DAY))
				+ makeTowDigit(calLocal.get(Calendar.MINUTE)) + makeTowDigit(calLocal.get(Calendar.SECOND));
	}

	/**
	 * <P>
	 * 숫자를 문자열로 변환하는데, 2자리수 미만이면 두자리수로 맞춘다.
	 * 
	 * <PRE>
	 * * 사용 예
	 *  String  DateStr = makeTowDigit(num)
	 * </PRE>
	 * 
	 * @param int
	 *            num
	 * @return String
	 */
	protected static String makeTowDigit(int num) {
		return (num < 10 ? "0" : "") + num;
	}

	/**
	 * <P>
	 * YYYY-MM-DD 날짜 형식을 YYYY/MM/DD 형태로 변경
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  String  DateStr = getCurrentDate()
	 * </PRE>
	 * 
	 * @param none
	 * @return String
	 */
	public String getCurrentDate() {
		String cdate = getLocaleDate(now);
		return cdate.substring(0, 10).replace('-', '/').trim();
	}

	/**
	 * <P>
	 * date_ 포멧으로 날짜형식을 변경
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  String  DateStr = getLocaleDate(date_)
	 * </PRE>
	 * 
	 * @param date
	 *            date_
	 * @return String
	 */

	public String getLocaleDate(Date date) {
		Locale locale = Locale.KOREAN;
		String ret = new SimpleDateFormat("yyyy-MM-dd").format(date);
		return ret;
	}

	/**
	 * <P>
	 * 현재날짜를 YYYY-MM-dd 형식으로 반환
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  String  DateStr = getSysDate()
	 * </PRE>
	 * 
	 * @param none
	 * @return String
	 */
	public static String getSysDate() {
		Locale locale = Locale.KOREAN;
		String ret = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		return ret;
	}

	/**
	 * <P>
	 * 현재날짜를 YYYY-MM 형식으로 반환
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  String  DateStr = getSysMonthly()
	 * </PRE>
	 * 
	 * @param none
	 * @return String
	 */
	public static String getSysMonthly() {
		Locale locale = Locale.KOREAN;
		String ret = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
		return ret.substring(0, 7);
	}

	/**
	 * <P>
	 * 현재날짜를 지정된 format 으로 반환
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  String  DateStr = getCurrentDate(format)
	 * </PRE>
	 * 
	 * @param String
	 *            format
	 * @return String
	 */
	public static String getCurrentDate(String format) {
		Locale locale = Locale.KOREAN;
		String ret = new SimpleDateFormat(format).format(new java.util.Date());
		return ret;
	}

	/**
	 * 
	 * @param writeday
	 * @return
	 */
	public String getNewArticle(Date writeday) {
		String newImg = "";
		if (today.getTime() - writeday.getTime() < (1000 * 60 * 60 * 24 * 1)) {
			newImg = "<img src='/kBoard/images/new.gif' border=0>";
		}
		return newImg;
	}

	/**
	 * 
	 * @param writeday
	 * @return
	 */
	public String getDateFormat(Date writeday) {
		String wrtday = "";
		String nowdate = "";
		nowdate = new SimpleDateFormat("yyyy-MM-dd").format(today);
		wrtday = new SimpleDateFormat("yyyy-MM-dd").format(writeday);
		if (nowdate.equals(wrtday)) {
			wrtday = new SimpleDateFormat("HH:mm:ss").format(writeday);
		}
		return wrtday;
	}

	/**
	 * <P>
	 * 날짜(d)를 원하는 format 으로 변경
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  String  DateStr = getTimeFormat(d,format)
	 * </PRE>
	 * 
	 * @param Date
	 *            d
	 * @param String
	 *            format
	 * @return String
	 */
	public static String getDateFormat(Date d, String format) {
		SimpleDateFormat fmt = new SimpleDateFormat(format);
		if (d == null) {
			return "";
		}
		return fmt.format(d);

	}

	/**
	 * <P>
	 * 날짜(writeday)를 HH:mm:ss 형태로 반환
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  String  DateStr = getTimeFormat(writeday)
	 * </PRE>
	 * 
	 * @param date
	 *            writeday
	 * @return String
	 */
	public String getTimeFormat(Date writeday) {
		String wrtday = new SimpleDateFormat("HH:mm:ss").format(writeday);
		return wrtday;
	}

	/**
	 * <P>
	 * String dateStr을 Date 형으로 변환 후 반환
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  Date  DateStr = string2Day("20090130")
	 * </PRE>
	 * 
	 * @param String
	 *            dateStr
	 * @return Date
	 * @throws Exception
	 */
	public Date string2Day(String dateStr) throws Exception {
		Date string2Day;
		try {
			string2Day = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
		} catch (Exception e) {
			throw new Exception("wrong format string");
		}

		return string2Day;
	}

	/**
	 * <P>
	 * 현재날짜와 datediff 날짜 사이의 일수 반환
	 * <P>
	 * 
	 * <PRE>
	 * 사용 예
	 *  long  DateStr = dateDiff("20090130")
	 * </PRE>
	 * 
	 * @param String
	 *            datediff
	 * @return long
	 */
	public long dateDiff(String datediff) throws Exception {
		Date dDate;
		dDate = string2Day(datediff);
		long day2day = 0;
		day2day = (int) ((now.getTime() - dDate.getTime()) / (1000 * 60 * 60 * 24));
		return day2day;
	}

	/**
	 * <P>
	 * 현재 년도(yyyy)를 반환
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  int  DateStr = getYear()
	 * 
	 * </PRE>
	 * 
	 * @param none
	 * @return int
	 */
	public static int getYear() {
		return getNumberByPattern("yyyy");
	}

	/**
	 * <P>
	 * 현재 달(mm)를 반환
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  int  DateStr = getMonth()
	 * 
	 * </PRE>
	 * 
	 * @param none
	 * @return int
	 */
	public static int getMonth() {
		return getNumberByPattern("MM");
	}

	/**
	 * <P>
	 * 현재날짜(dd)를 반환
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  int  DateStr = getDay()
	 * </PRE>
	 * 
	 * @param none
	 * @return int
	 */
	public static int getDay() {
		return getNumberByPattern("dd");
	}

	/**
	 * <P>
	 * 현재날짜(yyyyMMdd)를 지정된 pattern 으로 반환
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  int  DateStr = getNumberByPattern()
	 * 
	 * </PRE>
	 * 
	 * @param String
	 *            pattern
	 * @return int
	 */
	private static int getNumberByPattern(String pattern) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(pattern, java.util.Locale.KOREA);
		String dateString = formatter.format(new java.util.Date());
		return Integer.parseInt(dateString);
	}

	/**
	 * <P>
	 * 현재날짜(yyyyMMdd)를 지정된 pattern 으로 반환
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  String   DateStr = getNumberByPattern()
	 * 
	 * </PRE>
	 * 
	 * @param String
	 *            pattern
	 * @return String
	 */
	public static String getDateByPattern(String pattern) {
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat(pattern, java.util.Locale.KOREA);
		String dateString = formatter.format(new java.util.Date());
		return dateString;
	}

	/**
	 * <P>
	 * yyMMdd형태 반환
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  String  DateStr = getDateByPattern2()
	 * 
	 * </PRE>
	 * 
	 * @param none
	 * @return String
	 */
	public static String getDateByPattern2() {
		StringBuffer sb = new StringBuffer();
		String szTmp = null;
		// 년
		sb.append(Integer.toString(getYear() + 1900).substring(2, 4));
		// 월
		szTmp = Integer.toString(getMonth());
		if (szTmp.length() == 1)
			sb.append("0");
		sb.append(szTmp);
		// 일
		szTmp = Integer.toString(getDay());
		if (szTmp.length() == 1)
			sb.append("0");
		sb.append(szTmp);
		return sb.toString();
	}

	/**
	 * <P>
	 * 입력된 날짜중 그달의 1일의 요일을 반환
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  int  dd = getFirstMonth(2009, 1,4)
	 *  0: Sun, 1: Mon, 2: Tue ...
	 * </PRE>
	 * 
	 * @param int
	 *            year
	 * @param int
	 *            month
	 * @param int
	 *            day
	 * @return int
	 */
	public static int getFirstMonth(int year, int month, int day) {
		Calendar tempDate = Calendar.getInstance();
		Calendar cal = Calendar.getInstance();

		tempDate = Calendar.getInstance();
		tempDate.set(Calendar.YEAR, year);
		tempDate.set(Calendar.MONTH, month - 1);
		tempDate.set(Calendar.DATE, 1);

		cal = tempDate;
		return cal.get(Calendar.DAY_OF_WEEK) - 1;
	}

	/**
	 * <P>
	 * 입력된 year, month 에 속한 마지막 날짜를 반환
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  int  dd = getDaysInMonth(2009, 1)
	 * </PRE>
	 * 
	 * @param String
	 *            year
	 * @param int
	 *            month
	 * @return int
	 */
	public static int getDaysInMonth(int year, int month) {
		Calendar tempDate = Calendar.getInstance();
		Calendar cal = Calendar.getInstance();

		tempDate = Calendar.getInstance();
		tempDate.set(Calendar.YEAR, year);
		tempDate.set(Calendar.MONTH, month);
		tempDate.set(Calendar.DATE, 0);

		cal = tempDate;
		int dayT = cal.get(Calendar.DATE);

		return dayT;
	}

	/**
	 * <PRE>
	 * 숫자(int)를 문자열로 변환하는데, 목표자리수 미만이면 목표두자리수로 맞춘다.
	 * </PRE>
	 * 
	 * @param :
	 *            int - 숫자.
	 * @param :
	 *            int - 목표자리수.
	 * @return : String "00", "000" 형태의 스트링을 반환한다.
	 */
	@SuppressWarnings("PMD")
	public static String makeZeroFillDigit(int num, int len) {
		if (num < 0)
			num = 0;

		String strNum = Integer.toString(num);

		if (Integer.toString(num).length() < len) {
			for (int i = Integer.toString(num).length(); i < len; i++) {
				strNum = "0" + strNum;
			}
		}

		return strNum;
	}

	/**
	 * <P>
	 * 현재(System TimeZone 및 Locale 기준 ) 날짜정보를 얻는다.
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  String DateStr
	 * DateStr= getLocalDate()
	 * </PRE>
	 * 
	 * @param none
	 * @return String
	 */
	public static String getLocalDate() {
		Calendar calLocal = Calendar.getInstance();

		return "" + calLocal.get(Calendar.YEAR) + makeZeroFillDigit(calLocal.get(Calendar.MONTH) + 1, 2)
				+ makeZeroFillDigit(calLocal.get(Calendar.DATE), 2);
	}

	/**
	 * <P>
	 * 현재를 기준(입력날짜는 구분자가 없는 string형 )으로 몇일 후(전) 계산
	 * <P>
	 * 
	 * <pre>
	 * 사용 예
	 *  String DateStr
	 * DateStr= getTargetDate("2007-02-25",2)
	 * </pre>
	 * 
	 * @param str
	 * @param term
	 * @return
	 */
	public static String getTargetDate(String str, int term) {
		int millisPerHour = 60 * 60 * 1000;
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
		SimpleTimeZone timeZone = new SimpleTimeZone(9 * millisPerHour, "KST");
		fmt.setTimeZone(timeZone);

		int year = Integer.parseInt(str.substring(0, 4));
		int month = Integer.parseInt(str.substring(4, 6));
		int day = Integer.parseInt(str.substring(6, 8));

		java.util.Calendar aCal = java.util.Calendar.getInstance();

		aCal.set(year, month - 1, day + term);

		return fmt.format(aCal.getTime());
	}

	/**
	 * <P>
	 * 현재를 기준(입력날짜는 구분자가 있는 string형 )으로 몇일 후(전) 계산
	 * <P>
	 * 
	 * <pre>
	 * 사용 예
	 *  String DateStr
	 * DateStr= getTarget_Date("2007-02-25",2)
	 * </pre>
	 * 
	 * @param str
	 * @param term
	 * @return
	 */
	public static String getTargetDate2(String str, int term) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.KOREA);

		int year = Integer.parseInt(str.substring(0, 4));
		int month = Integer.parseInt(str.substring(5, 7));
		int day = Integer.parseInt(str.substring(8, 10));

		java.util.Calendar aCal = java.util.Calendar.getInstance();

		aCal.set(year, month - 1, day + term);
		return fmt.format(aCal.getTime());
	}

	/**
	 * <P>
	 * 현재를 기준으로 몇달 후(전) 날짜 계산
	 * <P>
	 * 
	 * <pre>
	 * 사용 예
	 *  String DateStr
	 * DateStr= getTarget_Month("20070225",2)
	 * </pre>
	 * 
	 * @param String
	 *            str
	 * @param int
	 *            term
	 * @return String
	 */
	public static String getTargetMonth(String str, int term) {
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd", java.util.Locale.KOREA);

		int year = Integer.parseInt(str.substring(0, 4));
		int month = Integer.parseInt(str.substring(5, 7));
		int day = Integer.parseInt(str.substring(8, 10));

		java.util.Calendar aCal = java.util.Calendar.getInstance();

		aCal.set(year, month - 1 + term, day);
		return fmt.format(aCal.getTime());
	}

	/**
	 * <P>
	 * 날짜문자열을 날짜표시타입으로 변환한다.
	 * <P>
	 * 
	 * <pre>
	 * 사용 예
	 *  String DateStr
	 * DateStr= makeDateType("20070225")
	 * </pre>
	 * 
	 * @param String
	 *            str
	 * @return String
	 */
	public static String makeDateType(String str) {
		if (str == null || str.length() == 0)
			return "";

		String strDate = null;

		if (str.length() == 8) {
			strDate = str.substring(0, 4) + "-" + str.substring(4, 6) + "-" + str.substring(6, 8);
		} else if (str.length() >= 10) {
			strDate = str.substring(0, 10);
		} else {
			strDate = str;
		}

		return strDate;
	}

	/**
	 * <P>
	 * 날짜문자열을 주어진 날짜 형식의 날짜표시타입으로 변환한다.
	 * <P>
	 * 
	 * <pre>
	 * 사용 예
	 *  String DateStr
	 * DateStr= makeDateType("20070225", "yyyy-mm-dd")
	 * </pre>
	 * 
	 * @param String
	 *            str
	 * @param String
	 *            format
	 * @return String
	 */
	public static String makeDateType(String str, String format) {
		if (str == null || str.length() == 0)
			return "";
		SimpleDateFormat sDateFormat = new SimpleDateFormat(format);
		String dateStr = "";
		try {
			/* Parsing Source 가 날짜 형식일 경우 */
			java.util.Date parseDate = DateFormat.getDateInstance().parse(str);
			dateStr = sDateFormat.format(parseDate);

		} catch (ParseException e) {
			/* Parsing Source 가 날짜 형식이 아닌 숫자로만 구성된 문자인 경우 */

			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, Integer.parseInt(str.substring(0, 4)));
			cal.set(Calendar.MONTH, Integer.parseInt(str.substring(4, 6)));
			cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(str.substring(6, 8)));

			if (str.length() > 8) {
				cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(str.substring(8, 10)));
			}
			if (str.length() > 10) {
				cal.set(Calendar.MINUTE, Integer.parseInt(str.substring(10, 12)));
			}

			if (str.length() > 12) {
				cal.set(Calendar.SECOND, Integer.parseInt(str.substring(12, 14)));
			}
			dateStr = sDateFormat.format(cal.getTime());

		}
		return dateStr;

	}

	/**
	 * <P>
	 * 날짜문자열을 주어진 날짜 형식의 날짜표시타입으로 변환한다.
	 * <P>
	 * 
	 * <PRE>
	 * - 사용 예
	 *  String DateStr
	 * DateStr= makeDateType("20070225", "yyyy-mm-dd","yyyy/mm/dd")
	 * </pre>
	 * 
	 * @param String
	 *            str - 날짜문자열 구분자가 존재하지 않는 숫자로만 구성된 날짜 (yyyymmdd) 혹은 Query의 결과로
	 *            얻어온 Pure 날짜타입 문자열 (yyyy-mm-dd hh:mm:ss ...)
	 * @param String
	 *            format - 날짜 형식 (yyyy-mm-dd)
	 * @return String
	 */
	public static String makeDateType(String str, String format, String newFormat) {
		if (str == null || str.length() == 0)
			return "";
		SimpleDateFormat sDateFormat = new SimpleDateFormat(format);
		String dateStr = "";
		try {
			java.util.Date parseDate = sDateFormat.parse(str);
			Calendar cal = Calendar.getInstance(Locale.KOREA);
			cal.setTime(parseDate);
			SimpleDateFormat newDateFormat = new SimpleDateFormat(newFormat);
			dateStr = newDateFormat.format(cal.getTime());

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateStr;

	}

	/**
	 * <P>
	 * 해당 일자의 주어진 주중 일자를 날짜 문자열의 형식으로 반환한다.
	 * <P>
	 * 
	 * <PRE>
	 * - 사용 예
	 * String week =  getDayOfWeek("20070225", "yyyymmdd", "MONDAY")
	 * 				MONDAY=2
	 * 				TUESDAY=3
	 * 				WEDNESDAY=4
	 * 				THURSDAY=5
	 * 				FRIDAY=6
	 * 				SATURDAY=7
	 * 				SUNDAY=1
	 * </pre>
	 * 
	 * @param String
	 *            dateStr
	 * @param String
	 *            format
	 * @param String
	 *            sDayOfWeek
	 * 
	 * @return String
	 */
	@SuppressWarnings("PMD")
	public static String getDayOfWeek(String dateStr, String format, String sDayOfWeek) {

		try {
			int dayOfWeek = 1;
			if (sDayOfWeek.equals("MONDAY")) {
				dayOfWeek = Calendar.MONDAY;
			} else if (sDayOfWeek.equals("TUESDAY")) {
				dayOfWeek = Calendar.TUESDAY;
			} else if (sDayOfWeek.equals("WEDNESDAY")) {
				dayOfWeek = Calendar.WEDNESDAY;
			} else if (sDayOfWeek.equals("THURSDAY")) {
				dayOfWeek = Calendar.THURSDAY;
			} else if (sDayOfWeek.equals("FRIDAY")) {
				dayOfWeek = Calendar.FRIDAY;
			} else if (sDayOfWeek.equals("SATURDAY")) {
				dayOfWeek = Calendar.SATURDAY;
			} else if (sDayOfWeek.equals("SUNDAY")) {
				dayOfWeek = Calendar.SUNDAY;
			}
			dateStr = dateStr.replaceAll("-", "");
			dateStr = dateStr.replaceAll("\\.", "");
			int year = Integer.parseInt(dateStr.substring(0, 4));
			int month = Integer.parseInt(dateStr.substring(4, 6));
			int day = Integer.parseInt(dateStr.substring(6));
			Calendar currentDate = Calendar.getInstance(Locale.KOREA);
			currentDate.set(Calendar.YEAR, year);
			currentDate.set(Calendar.MONTH, month - 1);
			currentDate.set(Calendar.DAY_OF_MONTH, day);
			currentDate.set(Calendar.DAY_OF_WEEK, dayOfWeek);

			SimpleDateFormat sDateFormat = new SimpleDateFormat(format);
			return sDateFormat.format(currentDate.getTime());
		} catch (Exception e) {
			e.printStackTrace();

			return "";
		}

	}

	/**
	 * <P>
	 * 해당 일자의 주어진 주중 일자를 반환한다.
	 * 
	 * <pre>
	 * <P><PRE> - 사용 예
	 *  String week = getDayOfWeek("20070225", "MONDAY")
	 * 				MONDAY=2
	 * 				TUESDAY=3
	 * 				WEDNESDAY=4
	 * 				THURSDAY=5
	 * 				FRIDAY=6
	 * 				SATURDAY=7
	 * 				SUNDAY=1
	 * </pre>
	 * 
	 * @param String
	 *            dateStr
	 * @param String
	 *            sDayOfWeek
	 * @return String
	 */
	@SuppressWarnings("PMD")
	public static String getDayOfWeek(String dateStr, String sDayOfWeek) {

		try {
			int dayOfWeek = 1;
			if (sDayOfWeek.equals("MONDAY")) {
				dayOfWeek = Calendar.MONDAY;
			} else if (sDayOfWeek.equals("TUESDAY")) {
				dayOfWeek = Calendar.TUESDAY;
			} else if (sDayOfWeek.equals("WEDNESDAY")) {
				dayOfWeek = Calendar.WEDNESDAY;
			} else if (sDayOfWeek.equals("THURSDAY")) {
				dayOfWeek = Calendar.THURSDAY;
			} else if (sDayOfWeek.equals("FRIDAY")) {
				dayOfWeek = Calendar.FRIDAY;
			} else if (sDayOfWeek.equals("SATURDAY")) {
				dayOfWeek = Calendar.SATURDAY;
			} else if (sDayOfWeek.equals("SUNDAY")) {
				dayOfWeek = Calendar.SUNDAY;
			}
			dateStr = dateStr.replaceAll("-", "");
			dateStr = dateStr.replaceAll("\\.", "");

			int year = Integer.parseInt(dateStr.substring(0, 4));
			int month = Integer.parseInt(dateStr.substring(4, 6));
			int day = Integer.parseInt(dateStr.substring(6));
			Calendar currentDate = Calendar.getInstance(Locale.KOREA);
			currentDate.set(Calendar.YEAR, year);
			currentDate.set(Calendar.MONTH, month - 1);
			currentDate.set(Calendar.DAY_OF_MONTH, day);
			currentDate.set(Calendar.DAY_OF_WEEK, dayOfWeek);

			return CommonUtil.fillCharFront("0", currentDate.get(Calendar.MONTH) + 1, 2) + "-"
					+ CommonUtil.fillCharFront("0", currentDate.get(Calendar.DAY_OF_MONTH), 2);
		} catch (Exception e) {
			e.printStackTrace();

			return "";
		}

	}

	/**
	 * <P>
	 * 지정된 날짜의 요일을
	 * <P>
	 * 
	 * <PRE>
	 * - 사용 예
	 *       int  dd  = DateUtil.getDayOfWeek(2009 ,1,12)
	 *       return  0: Sun, 1: Mon, 2:Tue
	 * </PRE>
	 * 
	 * @param int
	 *            year
	 * @param int
	 *            month
	 * @param int
	 *            day
	 * @return int
	 */
	public static int getDayOfWeek(int year, int month, int day) {
		// 0~6의 값을 반환한다. 결과가 0이면 일요일이다.
		return convertDatetoDay(year, month, day) % 7;
	}

	/**
	 * <P>
	 * 윤년 체크
	 * <P>
	 * 
	 * <PRE>
	 * - 사용 예
	 *       boolean b = DateUtil.isLeapYear(2009)
	 * </PRE>
	 * 
	 * @param String
	 *            year
	 * @return boolean
	 */
	public static boolean isLeapYear(int year) {
		return ((year % 4 == 0) && (year % 100 != 0) || (year % 400 == 0));
	}

	/**
	 * <P>
	 * fromDAte(yyyy-MM-dd) 에서부터 toDate(yyyy-MM-dd) 사이의 일수 계산
	 * <P>
	 * 
	 * <PRE>
	 * - 사용 예
	 *       int mmdif = DateUtil.dayDiff("20090101","20090110")
	 * </PRE>
	 * 
	 * @param String
	 *            fromDate
	 * @param String
	 *            toDate
	 * @return int
	 */
	public static int dayDiff(String fromDate, String toDate) {
		int year1 = 0, month1 = 0, day1 = 0;
		int year2 = 0, month2 = 0, day2 = 0;

		String[] date1 = fromDate.split("-");
		String[] date2 = toDate.split("-");

		year1 = Integer.parseInt(date1[0]);
		month1 = Integer.parseInt(date1[1]);
		day1 = Integer.parseInt(date1[2]);

		year2 = Integer.parseInt(date2[0]);
		month2 = Integer.parseInt(date2[1]);
		day2 = Integer.parseInt(date2[2]);

		int fromDay = convertDatetoDay(year1, month1, day1);
		int toDay = convertDatetoDay(year2, month2, day2);
		return fromDay - toDay;
	}

	/**
	 * <P>
	 * 지정된 날짜까지의 일수 return
	 * <P>
	 * 
	 * <PRE>
	 * - 사용 예
	 *       String mm = DateUtil.convertDatetoDay(2009,1,1)
	 * </PRE>
	 * 
	 * @param year
	 * @param month
	 * @param day
	 * @return int
	 */
	public static int convertDatetoDay(int year, int month, int day) {
		int numOfLeapYear = 0; // 윤년의 수

		// 전년도까지의 윤년의 수를 구한다.
		for (int i = 0; i < year; i++) {
			if (isLeapYear(i))
				numOfLeapYear++;
		}

		// 전년도까지의 일 수를 구한다.
		int toLastYearDaySum = (year - 1) * 365 + numOfLeapYear;

		// 올해의 현재 월까지의 일수 계산
		int thisYearDaySum = 0;
		// 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12
		int[] endOfMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		for (int i = 0; i < month - 1; i++) {
			thisYearDaySum += endOfMonth[i];
		}

		// 윤년이고, 2월이 포함되어 있으면 1일을 증가시킨다.
		if (month > 2 && isLeapYear(year)) {
			thisYearDaySum++;
		}

		thisYearDaySum += day;

		return toLastYearDaySum + thisYearDaySum - 1;
	}

	/**
	 * <P>
	 * 현재를 기준으로 param날짜간의 개월수 계산
	 * <P>
	 * 
	 * @param str
	 *            (YYYY-MM-DD)
	 * @return String
	 *         <P>
	 * 
	 *         <PRE>
	 * - 사용 예
	 *       String mm = DateUtil.getDiffMonth("20090101")
	 *         </PRE>
	 *         <P>
	 */
	public static String getDiffMonth(String str) {
		if (str != null && !str.equals("")) {

			StringBuffer sb = new StringBuffer();
			sb.append(str.substring(0, 4));
			sb.append(str.substring(5, 7));
			sb.append(str.substring(8, 10));
			String endDate = sb.toString(); // 입력날짜

			String strtDate = getLocalDate();// 현재날짜yyyymmdd형태
			int strtYear = Integer.parseInt(strtDate.substring(0, 4));
			int strtMonth = Integer.parseInt(strtDate.substring(4, 6));

			int endYear = Integer.parseInt(endDate.substring(0, 4));
			int endMonth = Integer.parseInt(endDate.substring(4, 6));

			// int nowDayCnt =getDaysInMonth(endYear,endMonth); //입력날짜의 해당 날짜수
			// int Diffmonth = (endYear - strtYear)* 12 + (endMonth -
			// strtMonth); //현재날짜와 입력날짜간 차이 달 수

			String month = Integer.toString((endYear - strtYear) * 12 + (endMonth - strtMonth));
			return month;
		} else {
			return "0";
		}

	}

	/**
	 * <P>
	 * 날짜를 "yyyy-MM-dd HH:mm:ss" 형식으로 반환
	 * <P>
	 * 
	 * @param Date
	 *            date
	 * @return String
	 */
	public static String getDatetimeString(java.util.Date date) {
		if (date == null)
			return "";
		java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss",
				java.util.Locale.KOREA);
		return formatter.format(date);
	}

	/**
	 * <P>
	 * 입력된 날짜에서 년도만 반환한다.
	 * <P>
	 * <P>
	 * 
	 * <PRE>
	 * - 사용 예
	 *       String mm = DateUtil.getYY("20090101")
	 * </PRE>
	 * <P>
	 * 
	 * @param String
	 *            str
	 * @return String
	 */
	public static String getYY(String str) {
		if (str == null || str.equals(""))
			return "";
		return str.substring(2, 4);
	}

	/**
	 * <P>
	 * 입력된 날짜에서 월만 반환한다.
	 * <P>
	 * <P>
	 * 
	 * <PRE>
	 * - 사용 예
	 *       String mm = DateUtil.getMM("20090101")
	 * </PRE>
	 * <P>
	 * 
	 * @param String
	 *            str
	 * @return String
	 */
	public static String getMM(String str) {
		if (str == null || str.equals(""))
			return "";
		return str.substring(4, 6);
	}

	/**
	 * <P>
	 * 입력된 날짜에서 일만 반환한다.
	 * <P>
	 * 
	 * @param String
	 *            str
	 * @return String
	 *         <P>
	 * 
	 *         <PRE>
	 * - 사용 예
	 *      String dd = DateUtil.getDD("20090101")
	 *         </PRE>
	 *         <P>
	 */
	public static String getDD(String str) {
		if (str == null || str.equals(""))
			return "";
		return str.substring(6, 8);
	}

	/**
	 * <P>
	 * 입력된 날짜가 유효한지 확인한다
	 * <P>
	 * 
	 * @param int
	 *            yyyy
	 * @param int
	 *            mm
	 * @param int
	 *            dd
	 * @return boolean
	 *         <P>
	 * 
	 *         <PRE>
	 * - 사용 예
	 *       boolean b = DateUtil.IsCorrectDate(2009, 1, 1)
	 *         </PRE>
	 *         <P>
	 */

	public static boolean isCorrectDate(int yyyy, int mm, int dd) {
		if (yyyy < 0 || mm < 0 || dd < 0)
			return false;
		if (yyyy > 12 || mm > 31)
			return false;

		int endday = getDaysInMonth(yyyy, mm);

		if (dd > endday)
			return false;
		return true;
	}

	/**
	 * <P>
	 * 입력된 날짜가 유효한지 확인한다
	 * <P>
	 * 
	 * @param String
	 *            yyyymmdd
	 * @return boolean
	 *         <P>
	 * 
	 *         <PRE>
	 * - 사용 예
	 *       boolean b = DateUtil.IsCorrectDate("20090101")
	 *         </PRE>
	 *         <P>
	 */

	public static boolean isCorrectDate(String yyyymmdd) {
		int yyyy = Integer.parseInt(yyyymmdd.substring(0, 4));
		int mm = Integer.parseInt(yyyymmdd.substring(4, 6));
		int dd = Integer.parseInt(yyyymmdd.substring(6));

		if (yyyy < 0 || mm < 0 || dd < 0)
			return false;
		if (mm > 12 || dd > 31)
			return false;

		int endday = getDaysInMonth(yyyy, mm);

		if (dd > endday)
			return false;
		return true;
	}

	/************************************************
	 * 원하는 시점의 날짜를 찾는다.
	 * <p>
	 * 
	 * @param int
	 *            y
	 *            <p>
	 * @param int
	 *            z
	 *            <p>
	 * @param string
	 *            date_type
	 *            <p>
	 * @return String
	 * 
	 *         <PRE>
	 * - 사용 예
	 * getCalsDate(0,1) :오늘
	 *         <p>
	 *         getCalsDate(1,1) :년, -1(1년전 오늘),-2(2년전 오늘)
	 *         <p>
	 *         getCalsDate(2,1) :개월, -1(1개월전 오늘),-2(2개월전 오늘), 1(1개월후 오늘)
	 *         <p>
	 *         getCalsDate(3 or 4 or 8,1) :주, -1(일주일전 같은요일), 1(1주일후 같은요일)
	 *         <p>
	 *         getCalsDate(5 or 6 or 7,1) :하루, -1(오늘부터 하루전), 1(오늘부터 하루후)
	 *         <p>
	 *         getCalsDate(9,1) :12시간, -1(12시간전) 1(12시간후) 2(24시간후)
	 *         <p>
	 * 
	 *         </PRE>
	 *         <P>
	 *************************************************/

	public static String getCalsDate(int y, int z, String dateType) throws Exception {

		Calendar cal = Calendar.getInstance(Locale.KOREAN);
		TimeZone timezone = cal.getTimeZone();
		timezone = TimeZone.getTimeZone("Asia/Seoul");
		cal = Calendar.getInstance(timezone);
		cal.add(y, z);
		java.util.Date currentTime = cal.getTime();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd", Locale.KOREAN);
		String timestr = formatter.format(currentTime);
		return timestr;
	}

	/************************************************
	 * 원하는 시점의 날짜를 찾는다.
	 * <p>
	 * 
	 * @param String
	 *            startDate
	 *            <p>
	 * @param int
	 *            period
	 *            <p>
	 * @return String
	 * 
	 *         <PRE>
	 * - 사용 예
	 * getWeekDay('20070101',14) :기준일부터 2주 후
	 *         <p>
	 * 
	 *         </PRE>
	 *         <P>
	 *************************************************/
	public String getWeekDay(String startDate, int period) throws ParseException {
		String retVal = "";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

		Date startDateD = sdf.parse(startDate);
		Calendar cal = Calendar.getInstance();
		cal.setTime(startDateD);

		cal.add(Calendar.DAY_OF_MONTH, period);

		retVal = sdf.format(cal.getTime());

		cal.add(Calendar.DAY_OF_MONTH, 8);

		return retVal;
	}

	public static Date stringToDay(String dateStr) throws Exception {
		Date string2Day;
		try {
			string2Day = new SimpleDateFormat("yyyy-MM-dd").parse(dateStr);
		} catch (Exception e) {
			throw new Exception("wrong format string");
		}

		return string2Day;
	}

}
