package com.logisall.winus.frm.common.util;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 @ isCharLen : 한글 영문 복합 문자길이 구하기
 @ nullChk : Null인지를 체크하여 Null인 경우에는 디폴트 값을 리턴한다.
 @ getShortString : 한글 존재 여부와 상관없이 일정한 Byte 단위로 String을 잘라주는 메소드  입력된 Byte Size 보다 큰 경우에는 &quot;...&quot;을 추가 한다.
 @ replace : 문자열(s) 중에서 특정 문자열(old)을 찾아서 원하는 문자열(replacement)을 변환한다.
 @ getN2Br : '\n'를  HTML CODE '<br>' 로 변환
 @ ignoreSeparator : 문자열중 특정 문자(sep)를 제거
 @ fillCharFront : int나 Strig 형으로 넘어온 iZero를 String으로 변환하여 그 앞에 sFill을 넣어  길이가 iLen이 되도록 하는 메소드
 @ isNull : null 여부 체크  null 이면 true, null이 아니면  false
 @ setSafeStr : 오라클에 입력할  "'" 으로 감싸진 string으로 만드는 함수
 @ setCheckSingleQuote : Single Quote (') => Double Quote ('') 로 변환
 @ getSafeNumber : 값이 없을경우 "0" 리턴
 @ isNumber : number 여부 체크  숫자이면 true, 아니면 false
 @ setEncryptorString : String을 암호화된 문자열로 Encrypt
 @ getDecryptorString : 암호화된 String을 원래꺼로 decrypt
 @ setOracleDate : string을 정해진 오라클 date 타입(yyyy-mm-dd)으로 변환, null 일경우 'sysdate' 리턴
 @ setOracleDateOnly : string을 정해진 오라클 date 타입(yyyy-mm-dd)으로 변환, null일경우 ''리턴
 @ setOracleDateTime : string을 정해진 오라클 date 타입(yyyy-mm-dd hh24)으로 변환, null일 경우 'sysdate' 리턴
 @ setOracleDatePattern : string을 사용자가 입력한 srt_Pattern의 오라클 date 타입으로 변환, null일 경우 'sysdate' 리턴
 @ setDatePattern : string을 사용자가 입력한 srt_Pattern의 오라클 date 타입으로 변환, null일 경우 'null' 리턴
 @ getStr2int : String 형을 int형으로 변환
 @ getStr2dou : String형을 float형으로 변환
 @ getShotString : 긴문장을 지정 길이로 잘라서 String[] 형태로 리턴
 @ getMoneyType : int나 String형 숫자를 금액표시타입으로 변환한다.
 @ setResCheckScript : "<",">" 를 "&lt;","&gt;" 로 변경
 @ getStrLenBr : asStr을 strSize만큼 자르고 asStr이 strSize보다 더 클경우 strSize만큼 자른후 "<br>" 을 붙임
 @ getSubstr : 문자열 자르기     value1 에서 value2 자리수 까지 자르기
 @ getFormatComa : 문자열을  100,000,000 포멧으로 변환, 문자열이 null 이면 "0" 리턴
 @ getFormatComaDot : 문자열을  100,000,000.00 포멧으로 변환, 문자열이 null 이면 "0"리턴
 @ getFormatComaDot2 : 문자열을 100,000,000.0000 포멧으로 변환, 문자열이 null 이면 "0"리턴
 @ getFormatNone : 문자열을  100,000,000.00 포멧으로 변환, 문자열이 null 이면 "0"리턴
 @ setNoCommaString : 문자열중 콤마제거
 @ getNoDashString : 문자열중 하이픈(-) 제거
 @ getLenchk : int 나 String 형  한자리 숫자값에 0추가 , 두자리이면 그냥 리턴
 @ getSepOneDash : temp 문자열에 지정된 idx 위치에 "-" 삽입
 @ getSepTwoDash : temp 문자열에 지정된 idx1, idx2 위치에 "-" 삽입
 @ getRightString : 문자열 오른쪽부터 result_length길이의 문자열을 리턴한다.
 @ getArrayStrValue : array 의 지정된 index 의 value 값을  String형 으로 return
 @ getRoundIndigo : 지정된 소수점자릿만허용(그이하에선  반올림)
 @ prtLog : log4j대신 로그 출력
 @ getParameterCount : 파라메터 갯수를 반환
 @ getParameterName : 파라메터 이름 리스트를 반환
 @ split : 입력받은 문자를 구분자로 나누어 ArrayList형태로 반환
 @ mod : 나머지를 반환
 @ getErasedTag : 문자열에서 HTML TAG를 지운다.
 */

/**
 * @author minjae
 * 
 */
@SuppressWarnings("PMD")
public final class CommonUtil {

	protected final static Log logger = LogFactory.getLog("CommonUtil");
	private static final String __LEFT_PARENTHESIS = "<";
	
	private CommonUtil() {		
	}
	
	public static Map<String, Object> converObjectToMap(Object obj){
        try {
            Field[] fields = obj.getClass().getDeclaredFields();
            Map<String, Object>  resultMap = new HashMap<String, Object> ();
            for(int i=0; i<=fields.length-1;i++){
                // fields[i].setAccessible(true);
                resultMap.put(fields[i].getName(), fields[i].get(obj));
            }
            return resultMap;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

	public static boolean isNull(String str) {
		return str.toLowerCase().equals("null") || str.trim().equals("");
	}

	public static String setNull(String str) {
		if (str == null || str.toLowerCase().equals("null")
				|| str.trim().equals(""))
			str = "";
		return str;
	}
	
	
	/**
     * 파라메터에서 공백제거한 값 구하기
     * 
     * @param String
     *            aStr : 공백제거할 문자열
     * @return int strlen :  공백제거된 문자열
     */
    public static String getTrimString(String aStr) {

        if (aStr == null) {
            return "";
        }
        return aStr.replaceAll("\\p{Z}", "");    // 코드에서 공백제거;
    }
	

	/**
	 * 한글 영문 복합 문자길이 구하기
	 * 
	 * @param String
	 *            aStr : 길이를 구할 문자열
	 * @return int strlen : 문자열의 길이
	 */
	public static int isCharLen(String aStr) {

		if (aStr == null) {
			return 0;
		}

		int strlen = 0;

		for (int j = 0; j < aStr.length(); j++) {

			char c = aStr.charAt(j);

			if ((c < 0xac00) || (0xd7a3 < c)) {
				strlen++;
			} else {
				strlen += 2; // 한글이다..
			}
		}

		return strlen;
	}

	/**
	 * <pre>
	 * Null인지를 체크하여 Null인경우에는 빈문자열을 리턴한다.
	 * </pre>
	 * 
	 * @param str_value
	 *            변환할 문자
	 * 
	 * @return 'null'문자 또는 null 일경우에는 빈문자열을 리턴하고 그외에는 str_value를 리턴한다.
	 */
	public static String nullChk(Object str_value) {

		if (isNull(str_value)) {
			return "";
		} else {
			return str_value.toString().trim();
		}
	}

	/**
	 * <pre>
	 * Null인지를 체크하여 Null인경우에는 빈문자열을 리턴한다.
	 * </pre>
	 * 
	 * @param str_value
	 *            변환할 문자
	 * 
	 * @return 'null'문자 또는 null 일경우에는 공백(space) 리턴하고 그외에는 str_value를 리턴한다.
	 */
	public static String null2Space(Object str_value) {

		if (isNull(str_value)) {
			return " ";
		} else {
			return str_value.toString().trim();
		}
	}

	/**
	 * Null인지를 체크하여 Null인경우에는 디폴트 값을 리턴한다.
	 * 
	 * @param Object
	 *            str_value : 변환할 문자,
	 * @param String
	 *            default_value : null인경우 리턴할 값
	 * @return String null인 경우 default_value, 아닌경우는 str_value
	 */
	public static String nullChk(Object str_value, String default_value) {

		if (isNull(str_value)) {
			return default_value;
		} else {
			return str_value.toString().trim();
		}
	}

	/**
	 * Null인지를 체크하여 Null인경우에는 디폴트1 값 아닐경우 디폴트2 값을 리턴한다.
	 * 
	 * @param Object
	 *            str_value : 변환할 문자,
	 * @param String
	 *            default_value1 : null인경우 리턴할 값
	 * @param String
	 *            default_value2 : null 아닐 경우 리턴할 값
	 * @return String null인 경우 default_value1, 아닌경우는 default_value2
	 */
	public static String nullChk(Object strValue, String defaultValue1,
			String defaultValue2) {

		if (isNull(strValue)) {
			return defaultValue1;
		} else {
			return defaultValue2;
		}
	}

	/**
	 * <pre>
	 * Null인지를 체크하여 Null인경우에는 0값을 리턴한다.
	 * </pre>
	 * 
	 * @param str_value
	 *            변환할 문자
	 * 
	 * @return 'null'문자 또는 null 일경우에는 0을 리턴하고 그외에는 str_value를 리턴한다.
	 */
	public static String nullChkInt(Object str_value) {

		String str_Return = "";

		if (!isNull(str_value)) {

			str_Return = replace(str_value.toString(), ",", "");
			str_Return = replace(str_Return, ".0", "");

			if (!isNumber(str_Return)) {
				str_Return = "0";
			}
		} else {
			str_Return = "0";
		}

		return str_Return;
	}

	/**
	 * 한글 존재 여부와 상관없이 일정한 Byte 단위로 String을 잘라주는 메소드 입력된 Byte Size 보다 큰 경우에는
	 * &quot;...&quot;을 추가 한다.
	 * 
	 * @param String
	 *            asStr : 변환할 문자열,
	 * @param int aiByteSize : 반환받을 Byte Size
	 * @return String asStr : 일정길이로 잘라진 문자열
	 */
	public static String getShortString(String asStr, int aiByteSize) {

		int iSize = 0;
		int iLen = 0;

		if (asStr == null) {
			return "";
		}

		if (asStr.getBytes().length > aiByteSize) {

			for (iSize = 0; iSize < asStr.length(); iSize++) {

				if (asStr.charAt(iSize) > 0x007F) {
					iLen += 2;
				} else {
					iLen++;
				}

				if (iLen > aiByteSize) {
					break;
				}
			}

			asStr = asStr.substring(0, iSize) + "..";
		}
		return asStr;
	}

	/**
	 * 문자열(s) 중에서 특정 문자열(old)을 찾아서 원하는 문자열(replacement)을 변환한다.
	 * 
	 * @param String
	 *            s : 원본 String 문자열,
	 * @param String
	 *            old : 찾는고 하는 문자열,
	 * @param String
	 *            replacement : 변경할 문자열
	 * @return String r : 바뀐 문자열
	 */
	public static String replace(String s, String old, String replacement) {
		StringBuffer r = new StringBuffer();

		int i = s.indexOf(old);

		if (i == -1) {
			return s;
		}

		r.append(s.substring(0, i) + replacement);

		if (i + old.length() < s.length()) {
			r.append(replace(s.substring(i + old.length(), s.length()), old,
					replacement));
		}

		return r.toString();
	}

	/**
	 * '\n'를 HTML CODE '<br>
	 * ' 로 변환
	 * 
	 * @param String
	 *            s : 원본 String 문자열
	 * @return String Br: "\n"을 "<BR>
	 *         "로 바꾼 값
	 */
	public static String getN2Br(String s) {

		String Br;

		if (s == null) {
			return "";
		}

		Br = replace(s, "\n", "<br/>");

		return Br;
	}

	/**
	 * 문자열중 특정 문자(sep)를 제거
	 * 
	 * @param String
	 *            str : 변경할 문자열,
	 * @param String
	 *            sep : 제거할 문자열
	 * @return String str1 : 변경할 문자열(str)중 제거할 문자열(sep)이 제거된 문자
	 */
	public static String ignoreSeparator(String str, String sep) {

		java.util.StringTokenizer st = new java.util.StringTokenizer(str, sep);

		/*
			String str1 = "";
			while (st.hasMoreTokens()) {
				str1 = str1 + st.nextToken();
			}
			return str1;
		*/
		StringBuffer sb = new StringBuffer();
		while (st.hasMoreTokens()) {
			sb.append(st.nextToken());
		}
		return sb.toString();
	}

	/**
	 * int형으로 넘어온 iZero를 String으로 변환하여 그 앞에 sFill을 넣어 길이가 iLen이 되도록 하는 메소드
	 * 
	 * @param String
	 *            sFill : 길이를 맞추기 위해 삽입할 문자열,
	 * @param int iZero : int형으로 넘어온 원본 데이터,
	 * @param int iLen : 변경할 문자의 길이
	 * @return String sZero : 변경된 최종 문자열
	 */
	public static String fillCharFront(String sFill, int iZero, int iLen) {
		/*
			String sTemp = "";
			String sZero = String.valueOf(iZero);
	
			for (int i = 0; i < (iLen - sZero.length()); i++) {
				sTemp = sTemp + sFill;
			}
	
			return sZero = sTemp + sZero;
		*/

		StringBuffer sb = new StringBuffer();
		String sZero = String.valueOf(iZero);

		for (int i = 0; i < (iLen - sZero.length()); i++) {
			sb.append(sFill);
		}

		sZero = (sb.append(sZero)).toString();
		return sZero;
	}	

	/**
	 * String형으로 넘어온 sZero 앞에 sFill을 넣어 길이가 iLen이 되도록 하는 메소드
	 * 
	 * @param String
	 *            sFill : 길이를 맞추기 위해 삽입할 문자열,
	 * @param String
	 *            sZero : String형으로 넘어온 원본 데이터,
	 * @param int iLen : 변경할 문자의 길이
	 * @return String sZero : 변경된 최종 문자열
	 */
	public static String fillCharFront(String sFill, String sZero, int iLen) {

		/*
			String sTemp = "";
	
			if (sZero == null) {
				sZero = "";
			}
	
			for (int i = 0; i < (iLen - sZero.length()); i++) {
				sTemp = sTemp + sFill;
			}
	
			return sZero = sTemp + sZero;
		*/
		
		StringBuffer sb = new StringBuffer();
		if (sZero == null) {
			sZero = "";
		}
		for (int i = 0; i < (iLen - sZero.length()); i++) {
			sb.append(sFill);
		}
		sZero = (sb.append(sZero)).toString();
		return sZero;
	}

	/**
	 * null 여부 체크 null 이면 true, null이 아니면 false
	 * 
	 * @param Object
	 *            str : 체크할 Object
	 * @return boolean : 체크한 Object의 null 여부. (null 이나 빈 문자열이면 true, null이 아니면
	 *         false)
	 */
	public static boolean isNull(Object str) {

		if (str == null || str.toString().trim().equals("")) {
			return true;
		} else {
			if ( str.toString().toLowerCase().equals("null") ) {
				return true;
			} 
			return false;
		}
	}

	/**
	 * 오라클에 입력할 "'" 으로 감싸진 string으로 만드는 함수
	 * 
	 * @param Object
	 *            str_Src : 원문
	 * @retrun String str_Return : &quot;'&quot; 으로 감싸진 안전한 문자열
	 */

	public static String setSafeStr(Object str_Src) {

		String str_Return = "";

		if (!isNull(str_Src)) {
			str_Return = replace(str_Src.toString(), "'", "''");
		}

		if (str_Src != null && !str_Src.equals("NULL")) {
			str_Return = "'" + str_Return + "'";
		}

		if (str_Src == null) {
			str_Return = "''";
		}

		return str_Return;
	}

	/**
	 * Single Quote (') =&gt; Double Quote ('') 로 변환
	 * 
	 * @param Object
	 *            str_Src : 원문
	 * @retrun String DoubleQuote : "'"를 "''"으로 변환 시킨 문자열
	 */

	public static String setCheckSingleQuote(Object str_Src) {

		String strReturn = "";

		if (!isNull(str_Src)) {
			strReturn = replace(str_Src.toString(), "'", "''");
		}

		return strReturn;
	}

	/**
	 * 값이 없을경우 "0" 리턴
	 * 
	 * @param Object
	 *            str_Src : 원문
	 * @retrun String str_Return : 숫자가 아니거나 값이 없으면 0
	 */

	public static String getSafeNumber(Object str_Src) {

		String str_Return = "";

		if (!isNull(str_Src)) {

			str_Return = replace(str_Src.toString(), ",", "");
			str_Return = replace(str_Return, ".0", "");

			if (!isNumber(str_Return)) {
				str_Return = "0";
			}
		} else {
			str_Return = "0";
		}

		return str_Return;
	}

	/**
	 * number 여부 체크 숫자이면 true, 아니면 false
	 * 
	 * @param Object
	 *            str_Src : 원문
	 * @return boolean : 숫자이면 true, 아니면 false
	 */
	public static boolean isNumber(Object str_Src) {

		if (isNull(str_Src)) {
			return false;
		} else {

			char[] ca_Src = str_Src.toString().toCharArray();

			for (int i = 0; i < ca_Src.length; i++) {
				if (!Character.isDigit(ca_Src[i])) {
					return false;
				}
			}

			return true;
		}

	}

	/**
	 * string을 정해진 오라클 date 타입(yyyy-mm-dd)으로 변환, null 일경우 'sysdate' 리턴
	 * 
	 * @param String
	 *            str_Src : 원문
	 * @return String str_src : 오라클 date 타입(yyyy-mm-dd)으로 변환된 문자열
	 */
	public static String setOracleDate(String str_Src) {

		if (str_Src != null && !str_Src.toUpperCase().equals("NULL")
				&& !str_Src.equals("")) {
			str_Src = " to_date('" + str_Src + "','yyyy-mm-dd') ";
		} else {
			str_Src = " sysdate ";
		}

		return str_Src;

	}

	/**
	 * string을 정해진 오라클 date 타입(yyyy-mm-dd)으로 변환, null일경우 ''리턴
	 * 
	 * @param String
	 *            str_Src : 원문
	 * @return String str_Src : 오라클 date 타입으로 변환된 문자열
	 */
	public static String setOracleDateOnly(String str_Src) {

		if (str_Src != null && !str_Src.toUpperCase().equals("NULL")
				&& !str_Src.equals("")) {
			str_Src = " to_date('" + str_Src + "','yyyy-mm-dd') ";
		} else {
			str_Src = " to_date('') ";
		}
		return str_Src;
	}

	/**
	 * string을 정해진 오라클 date 타입(yyyy-mm-dd hh24)으로 변환, null일 경우 'sysdate' 리턴
	 * 
	 * @param String
	 *            str_Src : 원문
	 * @return String str_Src : 오라클 date 타입(yyyy-mm-dd hh24)으로 변환된 문자열
	 */
	public static String setOracleDateTime(String str_Src) {

		if (str_Src != null && !str_Src.toUpperCase().equals("NULL")
				&& !str_Src.equals("")) {
			str_Src = " to_date('" + str_Src + "','yyyy-mm-dd hh24') ";
		} else {
			str_Src = " sysdate ";
		}

		return str_Src;

	}

	/**
	 * string을 사용자가 입력한 srt_Pattern의 오라클 date 타입으로 변환, null일 경우 'sysdate' 리턴
	 * 
	 * @param String
	 *            str_Src : 원문
	 * @param String
	 *            str_Pattern : 오라클 date 타입으로 변경할 패턴 ex) yyyy-mm-dd
	 * @return String str_Src : 사용자가 입력한 str_Pattern으로 변경된 문자
	 */
	public static String setOracleDatePattern(String str_Src, String str_Pattern) {

		if (str_Src != null && !str_Src.equals("")) {
			str_Src = " to_date('" + str_Src + "','" + str_Pattern + "') ";
		} else {
			str_Src = " sysdate ";
		}

		return str_Src;

	}

	/**
	 * string을 사용자가 입력한 srt_Pattern의 오라클 date 타입으로 변환, null일 경우 'null' 리턴
	 * 
	 * @param String
	 *            str_Src : 원문
	 * @param String
	 *            str_Pattern : 오라클 date 타입으로 변경할 패턴 ex) yyyy-mm-dd
	 * @return String str_Src : 사용자가 입력한 str_Pattern으로 변경된 문자
	 */
	public static String setDatePattern(String str_Src, String str_Pattern) {

		if (str_Src != null && !str_Src.equals("")) {
			str_Src = " to_date('" + str_Src + "','" + str_Pattern + "') ";
		} else {
			str_Src = null;
		}

		return str_Src;

	}

	/**
	 * String 형을 int형으로 변환
	 * 
	 * @param String
	 *            str : 원문
	 * @return int d : String형을 int형으로 변환한 값
	 * @throws Exception
	 */
	public static int getStr2int(String str) throws Exception {

		try {
			if (str == null || str.equals("")) {
				return 0;
			}
			Integer d = Integer.parseInt(str);
			return d.intValue();
		} catch (Exception e) {
			return 0;
		}
	}

	/**
	 * RowNum을 거꾸로 정렬
	 * 
	 * @author r
	 * @param String
	 *            rowNum : 컬럼번호
	 * @param String
	 *            totalRow : 총컬럼갯수
	 * @return int rowNum : 컬럼번호를 거꾸로 변환한 값
	 * @throws Exception
	 */
	public static int rowReversSort(String rowNum, String totalRow)
			throws Exception {

		Integer nRowNum = Integer.parseInt(rowNum);
		Integer nTotalRow = Integer.parseInt(totalRow);
		return nTotalRow - nRowNum + 1;
	}

	/**
	 * String형을 float형으로 변환
	 * 
	 * @param String
	 *            str : 원문
	 * @return float d : String형을 float형으로 변환한 값
	 * @throws Exception
	 */
	public static float getStr2dou(String str) throws Exception {

		if (str == null || str.equals("")) {
			return 0;
		}
		Float d = new Float(str);

		return d.floatValue();
	}

	/**
	 * 긴문장을 지정 길이로 잘라서 String[] 형태로 리턴
	 * 
	 * @param String
	 *            s_Src : 원문
	 * @param int i_Len : 문자열을 자를 크기
	 * @return String[] sa_Return : 지정된 크기로 잘라 String[] 에 넣은 값
	 */
	public static String[] getShotString(String s_Src, int i_Len) {

		byte[] byt_Src = s_Src.getBytes();

		String[] sa_Return;

		int i_ReturnCnt = 0;
		int i_StartIndex = 0;
		int i_EndIndex = 0;

		if ((byt_Src.length % i_Len) == 0) {
			i_ReturnCnt = byt_Src.length / i_Len;
		} else {
			i_ReturnCnt = (byt_Src.length / i_Len) + 1;
		}

		sa_Return = new String[i_ReturnCnt];

		for (int i = 0; i < i_ReturnCnt; i++) {

			i_StartIndex = i_EndIndex;

			if (i < i_ReturnCnt - 1) {
				i_EndIndex = i_EndIndex + i_Len;
			} else {
				i_EndIndex = byt_Src.length - 1;
			}

			if ((char) byt_Src[i_EndIndex] != ' '
					&& i_EndIndex != byt_Src.length - 1) {

				boolean b_Continue = true;

				for (int j = i_EndIndex; j > i_StartIndex && b_Continue; j--) {

					if ((char) byt_Src[j] == ' ') {

						i_EndIndex = j;
						b_Continue = false;
					}

				}
			}

			sa_Return[i] = new String(byt_Src, i_StartIndex, i_EndIndex
					- i_StartIndex);

		}

		return sa_Return;

	}

	/**
	 * 금액문자열을 금액표시타입으로 변환한다. (예) 12345678 --&gt; 12,345,678
	 * 
	 * @param String
	 *            strMoney : 금액문자열
	 * @param String
	 *            delimeter : 금액표시 구분자
	 * @return String : 변경된 금액 문자열
	 */
	public static String getMoneyType(String strMoney, String delimeter) {

		if (strMoney == null || strMoney.length() <= 0) {
			return "";
		}

		DecimalFormat df = new DecimalFormat();
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();

		dfs.setGroupingSeparator(delimeter.charAt(0));
		df.setGroupingSize(3);
		df.setDecimalFormatSymbols(dfs);

		return (df.format(Double.parseDouble(strMoney))).toString();
	}

	/**
	 * 숫자를 금액표시타입으로 변환한다. (예) 12345678 --&gt; 12,345,678
	 * 
	 * @param int intMoney : 금액
	 * @param String
	 *            delimeter : 금액표시 구분자 ex) ","
	 * @return String : 변경된 금액 문자열
	 */
	public static String getMoneyType(int intMoney, String delimeter) {
		return (getMoneyType(Integer.toString(intMoney), delimeter));
	}

	/**
	 * "<",">" 를 "&lt;","&gt;" 로 변경
	 * 
	 * @param String
	 *            arg : 원문
	 * @return String rValue : 바뀐 문자열
	 */
	public static String setResCheckScript(String arg) {

		String rValue = "";

		if ( ! (arg == null || arg.equals("")) ) {
			rValue = arg.replaceAll("<", "&lt;");
			rValue = arg.replaceAll(">", "&gt;");
		}

		return rValue;
	}

	/**
	 * asStr을 strSize만큼 자르고 asStr이 strSize보다 더 클경우 strSize만큼 자른후 "<br>
	 * " 을 붙임
	 * 
	 * @param String
	 *            asStr : 원문
	 * @param int strSize : 자를 문자의 길이
	 * @return String str_tmp : 바뀐 문자열
	 */
	public static String getStrLenBr(String asStr, int strSize) {

		String str_tmp = "";

		if (asStr == null || asStr.equals("")) {
			return "";
		}

		if (asStr.length() > strSize) {
			str_tmp = asStr.substring(0, strSize) + "<br>"
					+ asStr.substring(strSize, asStr.length() - 1);
		} else {
			str_tmp = asStr;
		}

		return str_tmp;
	}

	/**
	 * 문자열 자르기 value1 에서 value2 자리수 까지 자르기
	 * 
	 * @param String
	 *            str : 원문,
	 * @param int value1 : 자를 문자열의 시작 위치,
	 * @param int value2 : 자를 문자열의 마지막 위치
	 * @return String : value1 부터 value2 까지의 문자열
	 */
	public static String getSubstr(String str, int value1, int value2) {

		if (str == null || str.equals("")) {
			return "";
		}

		return str.substring(value1, value1 + value2);
	}

	/**
	 * 문자열을 100,000,000 포멧으로 변환, 문자열이 null 이면 "0" 리턴
	 * 
	 * @param String
	 *            str : 원문
	 * @return String : 지정된 포멧으로 바뀐 문자열
	 */
	public static String getFormatComa(String str) {

		DecimalFormat formatComa = new DecimalFormat("###,###,###,###,##0");

		if (str != null && !str.equals("")) {
			return formatComa.format(Double.parseDouble(str));
		} else {
			return "0";
		}
	}

	/**
	 * 문자열을 100,000,000 포멧으로 변환, 문자열이 null 이면 null 리턴
	 * 
	 * @param String
	 *            str : 원문
	 * @return String : 지정된 포멧으로 바뀐 문자열
	 */
	public static String getFormatComaNull(String str) {

		DecimalFormat formatComa = new DecimalFormat("###,###,###,###,##0");

		if (str != null && !str.equals("")) {
			return formatComa.format(Double.parseDouble(str));
		} else {
			return "";
		}
	}

	/**
	 * 문자열을 100,000,000.00 포멧으로 변환, 문자열이 null 이면 "0"리턴
	 * 
	 * @param String
	 *            str : 원문
	 * @return String : 지정된 포멧으로 바뀐 문자열
	 */
	public static String getFormatComaDot(String str) {

		DecimalFormat formatComaDot = new DecimalFormat(
				"###,###,###,###,##0.##");

		if (str != null && !str.equals("")) {
			return formatComaDot.format(Double.parseDouble(str));
		} else {
			return "0";
		}
	}

	/**
	 * 문자열을 100,000,000.0000 포멧으로 변환, 문자열이 null 이면 "0"리턴
	 * 
	 * @param String
	 *            str : 원문
	 * @return String : 지정된 포멧으로 바뀐 문자열
	 */
	public static String getFormatComaDot2(String str) {

		DecimalFormat formatComaDot = new DecimalFormat(
				"###,###,###,###,##0.####");

		if (str != null && !str.equals("")) {
			return formatComaDot.format(Double.parseDouble(str));
		} else {
			return "0";
		}
	}

	/**
	 * 문자열을 100000000 포멧으로 변환, 문자열이 null이면 "0" 리턴
	 * 
	 * @param String
	 *            str : 원문
	 * @return String : 지정된 포멧으로 바뀐 문자열
	 */
	public static String getFormatNone(String str) {

		DecimalFormat dformatNone = new DecimalFormat("##################");

		if (str != null && !str.equals("")) {
			return dformatNone.format(Double.parseDouble(str));
		} else {
			return "0";
		}
	}

	/**
	 * 문자열중 콤마제거
	 * 
	 * @param String
	 *            str : 원문
	 * @return String : ","가 제거된 문자열
	 */
	public static String setNoCommaString(String str) {

		if (str == null || str.equals("")) {
			str = "0";
		}

		return str.replaceAll("\\,", "").toString().trim();
	}

	/**
	 * 문자열중 하이픈(-) 제거
	 * 
	 * @param String
	 *            str : 원문
	 * @return String : 하이픈(-) 이 제거된 문자열
	 */
	public static String getNoDashString(String str) {

		if (str == null || str.equals("")) {
			str = "";
		}

		return str.replaceAll("-", "").toString().trim();
	}

	/**
	 * int형 한자리 숫자값에 0추가 , 두자리이면 그냥 리턴
	 * 
	 * @param int a : 변경할 숫자값
	 * @return String : 맨 앞에 "0" 이 추가된 두자리 숫자값
	 */
	public static String getLenchk(int a) {

		String temp = Integer.toString(a);

		if (temp.length() == 1) {
			temp = "0" + a;
		}

		return temp;
	}

	/**
	 * String형 한자리 숫자값에 0추가 , 두자리이면 그냥 리턴
	 * 
	 * @param String
	 *            temp : 변경할 숫자값
	 * @return String : 맨 앞에 "0" 이 추가된 두자리 숫자값
	 */
	public static String getLenchk(String temp) {

		if (temp.length() == 1) {
			temp = "0" + temp;
		}

		return temp;
	}

	/**
	 * temp 문자열에 지정된 idx 위치에 "-" 삽입
	 * 
	 * @param String
	 *            temp : 원문,
	 * @param int idx : "-"를 삽입할 위치
	 * @return String : "-"가 삽입된 문자열
	 */
	public static String getSepOneDash(String temp, int idx) {

		StringBuffer sb = new StringBuffer();

		if (temp == null || temp.length() == 0) {
			return "";
		}

		sb.append(temp.substring(0, idx)).append("-")
				.append(temp.substring(idx, temp.length()));

		return sb.toString();
	}

	/**
	 * temp 문자열에 지정된 idx1, idx2 위치에 "-" 삽입
	 * 
	 * @param String
	 *            temp : 원문,
	 * @param int idx1 : "-"이 삽입될 첫번재 위치,
	 * @param int idx2 : "-"이 삽입될 두번재 위치
	 * @return String : dix1, dix2 에 "-"이 삽입된 문자열
	 */
	public static String getSepTwoDash(String temp, int idx1, int idx2) {

		StringBuffer sb = new StringBuffer();

		if (temp == null || temp.length() == 0) {
			return "";
		}

		sb.append(temp.substring(0, idx1)).append("-")
				.append(temp.substring(idx1, idx2)).append("-")
				.append(temp.substring(idx2, temp.length()));

		return sb.toString();
	}

	/**
	 * 문자열 오른쪽부터 result_length길이의 문자열을 리턴한다.
	 * 
	 * @param String
	 *            arg : 원문,
	 * @param int result_length : 문자열의 길이
	 * @return String : 변경된 문자열
	 */
	public static String getRightString(String arg, int result_length) {

		if (arg == null || arg.equals("")) {
			return "";
		}

		int str_tot_len = arg.length();

		return arg.substring(str_tot_len - result_length, str_tot_len);
	}

	/**
	 * array 의 지정된 index 의 value 값을 String형 으로 return
	 * 
	 * @param Object
	 *            [] array : value값을 가져올 Object[],
	 * @param int index : value값을 가져올 Object[]의 index
	 * @return String : String형으로 변경된 Object[] 의 value값
	 */
	public static String getArrayStrValue(Object[] array, int index) {

		if (array == null) {
			return "";
		}

		if (array.length > index) {
			return array[index].toString();
		} else {
			return "";
		}
	}

	/**
	 * 지정된 소수점자릿만허용(그이하에선 반올림)
	 * 
	 * @param double value : 숫자값
	 * @param int precision : 표시할 소수점의 자릿수
	 * @return double : 변경된 숫자값
	 * @author indigo
	 */
	public static double getRoundIndigo(double value, int precision) {

		value = value * Math.pow(10, precision);
		value = Math.round(value);

		return value / Math.pow(10, precision);
	}

	// comMsg.vms 보조용
	/**
	 * 파라메터 갯수를 돌려 준다.
	 * 
	 * @param HttpServletRequest
	 * @return String
	 */
	public static int getParameterCount(HttpServletRequest req) {
		Enumeration a = req.getParameterNames();
		int i = 0;

		for (i = 0; a.hasMoreElements(); i++) {
			a.nextElement();
		}
		return i;
	}

	/**
	 * ii 번째 파라메터 명을 돌려 준다.
	 * 
	 * @param HttpServletRequest
	 * @param int
	 * @return String
	 */
	public static String getParameterName(HttpServletRequest req, int ii) {
		Enumeration a = req.getParameterNames();
		String b = "";
		for (int i = 0; i < ii - 1 && a.hasMoreElements(); i++) {
			a.nextElement();
		}
		if (a.hasMoreElements()) {
			b = (String) a.nextElement();
		}
		return b;
	}

	/**
	 * ICS 코드의 길이에 따라 "."을 삽입한다.
	 * 
	 * @param String
	 * @param String
	 * @return String
	 */
	public static String setIcsCodeFormat(String arg) {
		String str = "";

		if (arg.length() > 2) {
			str = arg.substring(0, 2) + "." + arg.substring(2, 5);
			if (arg.length() > 5) {
				str += "." + arg.substring(5, 7);
			}
		} else {
			str = arg;
		}
		return str;
	}

	/**
	 * 입력받은 문자열 source를 <br>
	 * 로 나누어 ArrayList형태로 반환
	 * 
	 * @param source
	 * @return
	 */
	public static ArrayList split(String source) {
		return split(source, "<br>");
	}

	/**
	 * 입력받은 문자열 source를 구분자 seperator로 나누어 ArrayList형태로 반환
	 * 
	 * @param source
	 * @param seperator
	 * @return
	 */
	public static ArrayList split(String source, String seperator) {
		String[] a = source.split(seperator);
		ArrayList<String> list = new ArrayList<String>();
		for (String element : a) {
			list.add(element);
		}
		return list;
	}

	/**
	 * 서버의 현재 날자를 YYYYMMDD형식의 문자열로 반환
	 * 
	 * @return
	 */
	public static String getToday() {
		return getToday("%04d%02d%02d");
	}

	/**
	 * <pre>
	 * 서버의 현재날자를 주어진 format에 맞는 문자열로 반환
	 * format 예)
	 * %04d%02d%02d
	 * %04d-%02d-%02d
	 * %04d/%02d/%02d
	 * </pre>
	 * 
	 * @param format
	 * @return
	 */
	public static String getToday(String format) {
		Calendar cal = Calendar.getInstance();
		return String.format(format, cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
	}

	/**
	 * 오늘 날짜에 s구분자를 v만큼 더하여 반환한다. s는 년(Y), 월(M), 일(D)이고 해당 구분자가 없으면 기본적으로 일로
	 * 처리한다.
	 * 
	 * @param s
	 * @param v
	 * @return
	 */
	public static String addToday(String s, int v) {
		return addToday("%04d%02d%02d", s, v);
	}

	/**
	 * 오늘 날짜에 s구분자를 v만큼 더하여 format 형태로 반환한다. s는 년(Y), 월(M), 일(D)이고 해당 구분자가 없으면
	 * 기본적으로 일로 처리한다.
	 * 
	 * @param format
	 * @param s
	 * @param v
	 * @return
	 */
	public static String addToday(String format, String s, int v) {
		Calendar cal = Calendar.getInstance();

		// 날짜 가중치 적용
		if (s.equalsIgnoreCase("Y")) {
			cal.add(Calendar.YEAR, v);
		} else if (s.equalsIgnoreCase("M")) {
			cal.add(Calendar.MONTH, v);
		} else {
			cal.add(Calendar.DAY_OF_MONTH, v);
		}

		return String.format(format, cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
	}

	/**
	 * <pre>
	 * date에 s구분자를 v만큼 더하여 반환한다.
	 * date는 YYYYMMDD, YYYY-MM-DD 등의 형태
	 * s는 년(Y), 월(M), 일(D)이고 해당 구분자가 없으면 기본적으로 일로 처리한다.
	 * </pre>
	 * 
	 * @param date
	 * @param s
	 * @param v
	 * @return
	 */
	/*
	public static String addDate(String date, String s, int v) {
		return String.format("%04d%02d%02d", date, s, v);
	}
	*/

	/**
	 * <pre>
	 * date에 s구분자를 v만큼 더하여 format 형태로 반환한다.
	 * date는 YYYYMMDD, YYYY-MM-DD 등의 형태
	 * s는 년(Y), 월(M), 일(D)이고 해당 구분자가 없으면 기본적으로 일로 처리한다.
	 * </pre>
	 * 
	 * @param format
	 * @param date
	 * @param s
	 * @param v
	 * @return
	 */
	public static String addDate(String format, String date, String s, int v) {
		StringBuilder temp = new StringBuilder();
		for (int i = 0; i < date.length(); i++) {
			if (date.charAt(i) >= '0' && date.charAt(i) <= '9') {
				temp.append(date.charAt(i));
			}
		}

		// 입력받은 날짜 세팅
		Calendar cal = Calendar.getInstance();
		// cal.set(Calendar.YEAR, new Integer(temp.toString().substring(0, 4)));
		cal.set(Calendar.YEAR, Integer.parseInt(temp.toString().substring(0, 4)));
		cal.set(Calendar.MONTH, Integer.parseInt(temp.toString().substring(4, 6)) - 1);
		cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(temp.toString().substring(6, 8)));

		// 날짜 가중치 적용
		if (s.equalsIgnoreCase("Y")) {
			cal.add(Calendar.YEAR, v);
		} else if (s.equalsIgnoreCase("M")) {
			cal.add(Calendar.MONTH, v);
		} else {
			cal.add(Calendar.DAY_OF_MONTH, v);
		}

		return String.format(format, cal.get(Calendar.YEAR),
				cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH));
	}

	/**
	 * 나머지를 반환
	 * 
	 * @param a
	 * @param b
	 * @return
	 */
	public static int mod(int a, int b) {
		return a % b;
	}

	/**
	 * eval함수 역활
	 * 
	 * @param pkg
	 *            (패키지명)
	 * @param classNm
	 *            (클래스명) 예) VMResultSet test1 = (VMResultSet)
	 *            CommonUtil.eval("utils.db","VMResultSet"+1);
	 * @return Object
	 * @throws Exception
	 */
	public static Object eval(String pkg, String classNm) throws Exception {
		try {
			Class clazz = Class.forName(pkg + "." + classNm);
			return clazz.newInstance();
		} catch (ClassNotFoundException ce) {
			ce.printStackTrace();
			throw ce;
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			throw e;
		} catch (InstantiationException e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 문자열에서 태그들을 지운다.
	 * 
	 * @param str
	 * @return
	 */
	public static String getErasedTag(String str) {
		String tmp = str;

		tmp = tmp.replaceAll("</?[a-zA-Z]+\\b[^>]*>", "");
		tmp = tmp.replaceAll("&nbsp;", " ");
		tmp = tmp.replaceAll("    ", "");
		tmp = tmp.replaceAll("\n", "");

		return tmp;
	}

	/**
	 * 백과사전 문자열 비교를 위한 처리
	 */
	public static String isCompareWith(String str) {
		str = str.replaceAll("\\ ", "").toString().trim();
		str = ignoreSeparator(str, "'\"");
		str = str.replaceAll("alt=", "");
		return str;
	}

	// 국제물류 정보 - 문자열 포멧
	public static String showText(String content) {
		StringBuffer result = new StringBuffer();
		for (int i = 0; i < content.length(); i++) {
			if (content.charAt(i) == 10) {
				result.append("<BR>");
			} else {
				result.append(content.charAt(i));
			}
		}

		return result.toString();
	}

	/**
	 * <pre>
	 *  문자열 뒤에 소수점을 붙인다. (0 인경우는 처리 안한다)
	 * 
	 *  <tag>param float
	 *  <tag>return String
	 * 
	 * @param float
	 * @return String
	 */
	public static String getDecimal(int size, float num) {
		DecimalFormat df = (DecimalFormat) NumberFormat.getInstance();
		if (num != 0) {
			if (size == 3) {
				df = new DecimalFormat("#,##0.000");
			} else if (size == 2) {
				df = new DecimalFormat("#,##0.00");
			}
		} else {
			num = 0;
		}
		return df.format(num);
	}

	/**
	 * html을 text로 바꾼다.
	 * 
	 * @param String
	 *            str 변환될 스트링
	 * @return String temp 변환된 스트링
	 */
	public static String textToHtml(String str) {
		if (str == null) {
			return null;
		}

		String temp = str;
		temp = strReplace(temp, "\n", "<br>");
		// temp=strReplace(temp, " ", "&nbsp;");
		temp = strReplace(temp, "\"", "&quot;");
		temp = strReplace(temp, "'", "&#39;");
		// temp=strReplace(temp, "&lt;", "<");
		// temp=strReplace(temp, "&gt;", ">");
		return temp;
	}

	/**
	 * 스트링에서 특정 문자열을 원하는 문자열로 바꾼다.
	 * 
	 * <pre>
	 *  특정 스트링에서 어떤 문자열을 원하는 문자열로 바꿀때 사용된다.
	 *  "that is red pig" 라는 문자열에서 "is"를 "was"로 바꾸려 할때는
	 * 
	 *  TAUtil.strReplace("this is red pig", "is", "was")
	 *  와 같이 호출하면 "this was red pig"를 리턴한다.
	 * 
	 * </pre>
	 * 
	 * @param dest
	 *            바꾸는 대상이 되는 스트링
	 * @param src
	 *            찾기를 원하는 스트링
	 * @param rep
	 *            바뀔 문자열
	 * @retrun 특정문자열이 원하는 문자열로 바뀌어진 스트링을 리턴한다.
	 */
	public static String strReplace(String dest, String src, String rep) {
		/*
				String retstr = "";
				String left = "";
				int pos = 0;
				if (dest == null) {
					return retstr;
				}
		
				while (true) {
					if ((pos = dest.indexOf(src)) != -1) {
						left = dest.substring(0, pos);
						dest = dest.substring(pos + src.length(), dest.length());
						retstr = retstr + left + rep;
						pos = pos + src.length();
					} else {
						retstr = retstr + dest;
						break;
					}
				}
				return retstr;
		*/
	
		String left = "";
		int pos = 0;
		if (dest == null) {
			return "";
		}

		StringBuffer sb = new StringBuffer();
		while (true) {
			if ((pos = dest.indexOf(src)) != -1) {
				left = dest.substring(0, pos);
				dest = dest.substring(pos + src.length(), dest.length());
				sb.append(left).append(rep);
				pos = pos + src.length();
			} else {
				sb.append(dest);
				break;
			}
		}
		return sb.toString();
	}	

	/**
	 * <P>
	 * 
	 * 현재(System TimeZone 및 Locale 기준 ) 날짜/시간정보를 얻는다.
	 * <P>
	 * 
	 * <PRE>
	 *  사용 예
	 *  String  DateStr = getLocalDateTime()
	 * </PRE>
	 * 
	 * @param none
	 * @return String
	 */
	public static String getLocalDateTime() {
		Calendar calLocal = Calendar.getInstance();
		return "" + calLocal.get(Calendar.YEAR)
				+ makeTowDigit(calLocal.get(Calendar.MONTH) + 1)
				+ makeTowDigit(calLocal.get(Calendar.DATE))
				+ makeTowDigit(calLocal.get(Calendar.HOUR_OF_DAY))
				+ makeTowDigit(calLocal.get(Calendar.MINUTE))
				+ makeTowDigit(calLocal.get(Calendar.SECOND));
	}

	/**
	 * <P>
	 * 숫자를 문자열로 변환하는데, 2자리수 미만이면 두자리수로 맞춘다.
	 * 
	 * <PRE>
	 * * 사용 예
	 *  String  DateStr = makeTowDigit(num)
	 * </PRE>
	 * 
	 * @param int num
	 * @return String
	 */
	protected static String makeTowDigit(int num) {
		return (num < 10 ? "0" : "") + num;
	}

	/**
	 * <P/>
	 * 특정 DATE 형식의 스트링을 다른 원하는 형식으로 변경
	 * 
	 * <PRE>
	 * String dateStr = "200901011231"
	 * convertDateFormat(dateStr, "yyyyMMddHHmm", "yyyy-MM-dd HH:mm")
	 * </PRE>
	 * 
	 * @param dateStr
	 * @param fromFormat
	 *            - 원본 DATE형식
	 * @param toFormate
	 *            - 바꿀 DATE형식
	 * @return
	 */
	public static String convertDateFormat(String dateStr, String fromFormat,
			String toFormat) {

		if (dateStr == null || "".equals(dateStr.trim())) {
			return "";
		}

		SimpleDateFormat fromDf = new SimpleDateFormat(fromFormat);
		SimpleDateFormat toDf = new SimpleDateFormat(toFormat);

		try {

			return toDf.format(fromDf.parse(dateStr));

		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 백분율 구하기
	 */
	public static int SaleMoneyPro_int(String a, String b) {
		float aa = Float.parseFloat(a);
		float bb = Float.parseFloat(b);
		float c = (aa * bb) / 100;
		int d = (int) Math.floor(c);
		return d;
	}

	/**
	 * 엑셀업로드 DATE 타입 체크
	 */
	public static boolean checkDateType(String oDate) {
		boolean ret = false;
		oDate = oDate.replace("-", "");

		if (oDate.length() == 8) {
			String t1 = oDate.substring(0, 4);
			String t2 = oDate.substring(4, 6);
			String t3 = oDate.substring(6, 8);

			String[] temDate = new String[] { t1, t2, t3 };

			int lastDay = 0;

			if (oDate.length() == 8 && temDate.length == 3) {
				if (temDate[0].length() == 4 && temDate[1].length() == 2
						&& temDate[2].length() == 2) {
					if (Integer.parseInt(temDate[1]) > 0
							&& Integer.parseInt(temDate[1]) < 13) {
						lastDay = DateUtil.getDaysInMonth(
								Integer.parseInt(temDate[0]),
								Integer.parseInt(temDate[1]));
						if (Integer.parseInt(temDate[2]) > 0
								&& Integer.parseInt(temDate[2]) < lastDay + 1) {
							ret = true;
						}
					}
				}
			}
		}

		return ret;
	}

	/**
	 * 
	 * Method ID : convertApostoReal Method 설명 : 작은따옴표 변환 작성자 : jsKim (Mist)
	 * 
	 * @param str
	 * @return
	 */
	public static String convertApostoReal(String str) {
		String temp = "";
		temp = strReplace(str, "&apos;", "'");
		return temp;
	}

	public static String convert(double doubleObj, String pattern)
			throws Exception {
		DecimalFormat df = new DecimalFormat(pattern);
		return df.format(doubleObj).toString();
	}

	public static String getDou2str(Object str_value) {
		if (isNull(str_value)) {
			return "";
		} else {
			return str_value.toString().trim();
		}
	}
	
	public static String convertSafePath(String path) {
		String rtnPath = "";
		if (isNull(path)) {
			return rtnPath;
		} else {
			rtnPath = path.replaceAll("/" , "" );
			rtnPath = path.replaceAll("\\\\" , "" );
			rtnPath = path.replaceAll(".", " " );
			rtnPath = path.replaceAll("&" , " ");
		}
		return rtnPath;
	}

}