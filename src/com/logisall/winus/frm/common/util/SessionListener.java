package com.logisall.winus.frm.common.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import com.logisall.winus.tmsys.service.impl.TMSYS030Dao;

public class SessionListener implements HttpSessionListener {

    // public static int count;
    /**
     * Method ID : sessionCreated
     * Method 설명 : 세션 생성시 실행
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public void sessionCreated(HttpSessionEvent event) {
        // HttpSession session = event.getSession();
        // count++;
        // session.getServletContext().log(session.getId() + " 세션생성 " + ", 접속자수 : " + count);
    }
    
    /**
     * Method ID : checkLogin
     * Method 설명 : 로그인 체크 (중복 접속 가능 유저인지)
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public static boolean checkLogin(String loginYn, String multiUser, String lastContIp, String clientContIp){
        boolean chek = false;

        if(multiUser.equals("Y")){
            chek = true;
        }else if(multiUser.equals("N")){
            if(loginYn.equals("Y")){
            	if(lastContIp.trim().equals(clientContIp.trim())){
            		chek = false;
            	}else{
            		chek = false;
            	}
            }else if(loginYn.equals("N")){
                chek = true;
            }
        }
        return chek;

/*
        if(multiUser.equals("Y")){
            chek = true;
        }else if(multiUser.equals("N")){
            if(loginYn.equals("Y")){
            	if(lastContIp.trim().equals(clientContIp.trim())){
            		chek = true;
            	}else{
            		chek = false;
            	}
            }else if(loginYn.equals("N")){
                chek = true;
            }
        }
        return chek;
*/
    }
    
    /**
     * Method ID : sessionDestroyed
     * Method 설명 : 세션 소멸시 실행
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */    
    public void sessionDestroyed(HttpSessionEvent event) {
        HttpSession session = event.getSession();
        Map<String, Object> model = new HashMap<String, Object>();
        ApplicationContext ctx =  WebApplicationContextUtils.getWebApplicationContext(session.getServletContext());

        // count--;
        
        TMSYS030Dao dao = (TMSYS030Dao)ctx.getBean("TMSYS030Dao"); 
        
        model.put("USER_NO", session.getAttribute("SS_USER_NO"));
        model.put("Session_ID", session.getId());
        model.put("WORK_IP", session.getAttribute("SS_CLIENT_IP"));
  
        if(null != model.get("USER_NO")){
            dao.logoutUpdate(model); // 접속여부 N 처리
            
            String conYn = dao.selectConnectYN(model);// 접속여부 검색
            model.put("CON_YN", conYn);
            
            dao.loginInsert(model); //로그아웃 히스토리 
        }
        
        // session.getServletContext().log(session.getId() + " 세션소멸 " + ", 접속자수 : " + count);
    }


    
}
