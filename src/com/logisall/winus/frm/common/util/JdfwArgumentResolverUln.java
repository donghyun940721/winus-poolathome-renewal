
package com.logisall.winus.frm.common.util;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.IOUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.core.MethodParameter;
import org.springframework.web.context.request.NativeWebRequest;

import com.m2m.jdfw5x.client.service.JDFWClientInfo;
import com.m2m.jdfw5x.egov.adapter.JdfwArgumentResolver;
import com.m2m.jdfw5x.egov.adapter.UiAdaptor;
import com.m2m.jdfw5x.util.SessionUtil;

public class JdfwArgumentResolverUln extends JdfwArgumentResolver {

	protected Log loger;

	private UiAdaptor uiA;

	public JdfwArgumentResolverUln() {
		loger = LogFactory.getLog(getClass());
	}

	public void setUiAdaptor(UiAdaptor uiA) {
		this.uiA = uiA;
	}

	public Object resolveArgument(MethodParameter arg0, NativeWebRequest arg1) throws Exception {
		Class parameterType = arg0.getParameterType();
		String name = arg0.getParameterName();
		HttpServletRequest request = (HttpServletRequest) arg1.getNativeRequest();
		Map map = null;

		// loger.info("[ JdfwArgumentResolverUln ] arg0.getParameterType :" + arg0.getParameterType());
		if (arg0.getParameterType().equals(JSONObject.class)) {
			ServletRequest req = (ServletRequest) arg1.getNativeRequest();
			String inputString = IOUtils.toString(req.getReader());
			
			// loger.info("[ JdfwArgumentResolverUln ] inputString :" + inputString);
			JSONParser jsonParser = new JSONParser();
			JSONObject json = (JSONObject) jsonParser.parse(inputString);
			// loger.info("[ JdfwArgumentResolverUln ] json        :" + json);
			
			return json;
		}
		if (parameterType.equals(uiA.getModelName()) && name.equalsIgnoreCase("model")) {
			try {
				map = (Map) uiA.convert(request);
				map.put("SS_CLIENT_IP", JDFWClientInfo.getClntIP(request));
				setSessionMapData(request, map);
			} catch (Exception ne) {
				map = new HashMap();
				map.put("SS_CLIENT_IP", JDFWClientInfo.getClntIP(request));
				setSessionMapData(request, map);
			}
			return map;
		}
		if (parameterType.equals(uiA.getModelName()) && name.equalsIgnoreCase("model2")) {
			try {
				map = (Map) uiA.convert((HttpServletRequest) arg1.getNativeRequest());
				map.put("SS_CLIENT_IP", JDFWClientInfo.getClntIP(request));
				setSessionMapData(request, map);
			} catch (Exception ne) {
				map = new HashMap();
				map.put("SS_CLIENT_IP", JDFWClientInfo.getClntIP(request));
				setSessionMapData(request, map);
			}
			return map;
		} else {
			return UNRESOLVED;
		}
	}

	public void setSessionMapData(HttpServletRequest request, Map map) {
		HttpSession session = request.getSession();
		map.put(ConstantIF.SS_USER_ID, SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
		map.put(ConstantIF.SS_USER_NO, SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
		map.put(ConstantIF.SS_USER_GB, SessionUtil.getStringValue(request, ConstantIF.SS_USER_GB));
		map.put(ConstantIF.SS_USER_NAME, SessionUtil.getStringValue(request, ConstantIF.SS_USER_NAME));
		map.put(ConstantIF.SS_USER_TYPE, SessionUtil.getStringValue(request, ConstantIF.SS_USER_TYPE));
		map.put(ConstantIF.SS_SVC_NO, SessionUtil.getStringValue(request, ConstantIF.SS_SVC_NO));
		map.put(ConstantIF.SS_SVC_NAME, SessionUtil.getStringValue(request, ConstantIF.SS_SVC_NAME));
		map.put(ConstantIF.SS_CMPY_CODE, SessionUtil.getStringValue(request, ConstantIF.SS_CMPY_CODE));
		map.put(ConstantIF.SS_CMPY_ID, SessionUtil.getStringValue(request, ConstantIF.SS_CMPY_ID));
		map.put(ConstantIF.SS_CMPY_NAME, SessionUtil.getStringValue(request, ConstantIF.SS_CMPY_NAME));
		map.put(ConstantIF.SS_LANG, SessionUtil.getStringValue(request, ConstantIF.SS_LANG));
		map.put(ConstantIF.SS_SESSION_ID, SessionUtil.getStringValue(request, ConstantIF.SS_SESSION_ID));
		map.put(ConstantIF.SS_AUTH_LEVEL, SessionUtil.getStringValue(request, ConstantIF.SS_AUTH_LEVEL));
		map.put(ConstantIF.SS_CLIENT_CD, SessionUtil.getStringValue(request, ConstantIF.SS_CLIENT_CD));
		map.put(ConstantIF.SS_GIRD_NUM, SessionUtil.getStringValue(request, ConstantIF.SS_GIRD_NUM));
		map.put(ConstantIF.SS_SVC_USE_TGT, SessionUtil.getStringValue(request, ConstantIF.SS_SVC_USE_TGT));
		map.put(ConstantIF.SS_SVC_USE_TY, SessionUtil.getStringValue(request, ConstantIF.SS_SVC_USE_TY));
		map.put(ConstantIF.SS_LC_CHANGE_YN, SessionUtil.getStringValue(request, ConstantIF.SS_LC_CHANGE_YN));
		map.put(ConstantIF.SS_ITEM_FIX_YN, SessionUtil.getStringValue(request, ConstantIF.SS_ITEM_FIX_YN));
		map.put(ConstantIF.SS_ITEM_GRP_ID, SessionUtil.getStringValue(request, ConstantIF.SS_ITEM_GRP_ID));
		map.put(ConstantIF.SS_TEL_NO, SessionUtil.getStringValue(request, ConstantIF.SS_TEL_NO));
		map.put(ConstantIF.SS_SUB_PASSWORD, SessionUtil.getStringValue(request, ConstantIF.SS_SUB_PASSWORD));
		map.put(ConstantIF.SS_PROC_AUTH, SessionUtil.getStringValue(request, ConstantIF.SS_PROC_AUTH));
		map.put(ConstantIF.SS_USE_TRANS_CUST, SessionUtil.getStringValue(request, ConstantIF.SS_USE_TRANS_CUST));
		map.put(ConstantIF.SS_MULTI_USE_GB, SessionUtil.getStringValue(request, ConstantIF.SS_MULTI_USE_GB));
		map.put(ConstantIF.SS_M_DO, SessionUtil.getStringValue(request, ConstantIF.SS_M_DO));
		map.put(ConstantIF.SS_DETAIL_PS, SessionUtil.getStringValue(request, ConstantIF.SS_DETAIL_PS));
		map.put(ConstantIF.SS_SALES_CUST_ID, SessionUtil.getStringValue(request, ConstantIF.SS_SALES_CUST_ID));
		
		map.put(ConstantIF.SS_LOGIN_DATE, SessionUtil.getValue(request, ConstantIF.SS_LOGIN_DATE));
		map.put(ConstantIF.SS_LOGIN_SESSION_TIME, SessionUtil.getValue(request, ConstantIF.SS_LOGIN_SESSION_TIME));
		if (request.getAttribute("windowRoll") != null) {
			map.put("windowRoll", request.getAttribute("windowRoll"));
		}
	}
}

