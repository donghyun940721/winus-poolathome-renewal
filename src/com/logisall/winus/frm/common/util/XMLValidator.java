package com.logisall.winus.frm.common.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.HashSet;
import java.util.Set;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import com.m2m.jdfw5x.egov.exception.BizException;

public final class XMLValidator {

	protected static Log log = LogFactory.getLog(XMLValidator.class);
	
	private XMLValidator() {}

	public static void checkValidate(String xmlFileName, String xsdFileName) throws Exception {

		// log.info("[ XML VALIDATION ] xsdFileName :" + xsdFileName);

		// 1. W3C XML Schema language 객체를 생성.
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Validator validator = null;
		try {
			factory.setResourceResolver(new ResourceResolver());
			factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, false);

			// 2. 정의된 xsd 파일을 불러옵니다.
			Schema schema = factory.newSchema(new StreamSource(new File(xsdFileName)));

			// read xml file for validation
			/*
			 * DocumentBuilderFactory documentFactory =
			 * DocumentBuilderFactory.newInstance();
			 * documentFactory.setNamespaceAware(true); DocumentBuilder builder
			 * = documentFactory.newDocumentBuilder(); InputSource is=new
			 * InputSource(new StringReader(xmlFileName)); Document doc =
			 * builder.parse(is); // parse the XML into a document object
			 * validator.validate(new DOMSource(doc));
			 */

			// 3. 가져온 스키마로 유효성 검사 도구를 만듭니다.
			validator = schema.newValidator();

			// 4. xml 파일을 불러옵니다.
			Source source = new StreamSource(xmlFileName);

			// 5. 해당 xml 을 유효성을 검사합니다.
			validator.setErrorHandler(new CustomContentHandler());
			validator.validate(source);

			// log.info(xmlFileName + " is valid."); // 유효성 검사 성공
			// log.info("[ XML VALIDATION ] XML 검증 END ...");

		} catch (IOException e) {
			// e.printStackTrace();
			String msg = "생성된 XML에 오류가 있습니다. : " + e.getMessage();
			if (log.isInfoEnabled()) {
				// log.info(xmlFileName + " is not found "); // 파일을 찾을 수 없습니다.
				// log.info("[ XML VALIDATION ] XML 검증 END ...");
			}
			throw new BizException(msg);

		} catch (SAXException se) {
			// se.printStackTrace();

			String msg = "생성된 XML에 오류가 있습니다. : " + se.getMessage();
			if (log.isInfoEnabled()) {
				// log.info(xmlFileName + " is not valid because "); // 유효성 검사
				// 실패
				// log.info(msg); // 실패한 원인에 대하여 설명합니다.
				// log.info("[ XML VALIDATION ] XML 검증 END ...");
			}
			throw new BizException(msg);
		}

	}
}

// xsd 스키마정의가 include or import 등으로 구성되어있을 경우 이하 Class 필요
class ResourceResolver implements LSResourceResolver {
	private Set<String> resoures = new HashSet<String>();

	@Override
	public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
		if (resoures.contains(systemId)) {
			return null;
		}
		resoures.add(systemId);
		File file = new File(systemId);
		if (file.exists()) {
			LSInput input = new LSInputImpl();
			try {
				input.setByteStream(new FileInputStream(file));

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			return input;
		}
		return null;
	}
}

class LSInputImpl implements LSInput {

	public Reader getCharacterStream() {
		return null;
	}

	public void setCharacterStream(Reader characterStream) {
	}

	public InputStream getByteStream() {
		InputStream retval = null;
		if (byteStream != null) {
			retval = byteStream;
		}
		return retval;
	}

	public void setByteStream(InputStream byteStream) {
		this.byteStream = byteStream;
	}

	public String getStringData() {
		return null;
	}

	public void setStringData(String stringData) {
	}

	public String getSystemId() {
		return systemId;
	}

	public void setSystemId(String systemId) {
		this.systemId = systemId;
	}

	public String getPublicId() {
		return null;
	}

	public void setPublicId(String publicId) {
	}

	public String getBaseURI() {
		return null;
	}

	public void setBaseURI(String baseURI) {
	}

	public String getEncoding() {
		return null;
	}

	public void setEncoding(String encoding) {
	}

	public boolean getCertifiedText() {
		return false;
	}

	public void setCertifiedText(boolean certifiedText) {
	}

	private String systemId = null;
	private InputStream byteStream = null;
}

class CustomContentHandler extends DefaultHandler {

	protected Log log = LogFactory.getLog(CustomContentHandler.class);

	private String element = "";

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

		if (log.isDebugEnabled()) {
			log.debug(uri + " > " + uri);
			log.debug(localName + " > " + localName);
			log.debug(qName + " > " + qName);
		}

		if (localName != null && !localName.isEmpty())
			element = localName;
		else
			element = qName;

	}

	public String getElement() {
		return element;
	}

	@Override
	public void warning(SAXParseException exception) throws SAXException {
		printInfo(exception);
	}

	@Override
	public void error(SAXParseException exception) throws SAXException {
		printInfo(exception);
		// throw new SAXException(exception.getMessage());
	}

	@Override
	public void fatalError(SAXParseException exception) throws SAXException {
		printInfo(exception);
		// throw new SAXException(exception.getMessage());
	}

	private void printInfo(SAXParseException e) {
		if (log.isInfoEnabled()) {
			StringBuffer sb = new StringBuffer();
			sb.append("Line :").append(e.getLineNumber()).append(", Column :").append(e.getColumnNumber())
					.append(", Message :").append(e.getMessage());
			log.info(sb.toString());
		}
	}

}