/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;


/**
 * @author on
 *
 */
@SuppressWarnings("PMD")
public class DrbCertCipTestMasterVO extends DocDefaultVO {

	private static final long serialVersionUID = 1L;
	
	private String docType = "";	//문서타입
	private String DocVersion = "";	//문서버전
	private String docInstanceId = "";//문서식별번호
	private String signKey ="";		//서명키	
    public String getDocType() {
        return docType;
    }
    public void setDocType(String docType) {
        this.docType = docType;
    }
    public String getDocVersion() {
        return DocVersion;
    }
    public void setDocVersion(String docVersion) {
        DocVersion = docVersion;
    }
    public String getDocInstanceId() {
        return docInstanceId;
    }
    public void setDocInstanceId(String docInstanceId) {
        this.docInstanceId = docInstanceId;
    }
    public String getSignKey() {
        return signKey;
    }
    public void setSignKey(String signKey) {
        this.signKey = signKey;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }

	
	
		
}
