/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;


public class DocOrderListDetailVO extends DocDefaultVO {

	private static final long serialVersionUID = 1L;

	private String ritemNm ; 
    private String ritemCd ; 
    private String ordQty ; 
    private String workQty ; 
    private String unitPrice ; 
    private String price ; 
    private String ordDesc ; 
    private String tradeNm ; 
    private String repNm ; 
    private String addr ; 
    private String tel ; 
    private String fax ; 
    private String userNm ; 
    private String userHp ; 
    private String supplyAmt ; 
    private String vatAmt ; 
    private String totAmt ; 
    private String transTradeNm ; 
    private String reqDt ;
    private String seq ; 
    private String transCustAddr ; 
    private String transCustTel ; 
    private String transEmpNm ; 
    private String group1 ; 
    private String group2 ; 
    private String group3 ; 
    private String group4 ; 
    private String group5 ; 
    private String group6 ; 
    private String group7 ; 
    private String group8 ; 
    private String group9 ; 
    private String group10 ; 
    private String group11 ; 
    private String group12 ; 
    private String group13 ; 
    private String group14 ; 
    private String group15 ; 
    private String group16 ; 
    private String group17 ; 
    private String group18 ; 
    private String group19 ; 
    private String group20 ; 
    private String group21 ; 
    private String transCustBizNo ; 
    private String transCustFax ; 
    private String custBizNo ; 
    private String custNm ; 
    private String custRepNm ; 
    private String custAddr ; 
    private String custTel ; 
    private String custFax ; 
    private String outReqDt ; 
    private String eaQty ; 
    private String uomNm ; 
    private String custId ;
    private String ordId ;
    
    
    public String getOrdId() {
        return ordId;
    }
    public void setOrdId(String ordId) {
        this.ordId = ordId;
    }
    public String getRitemNm() {
        return ritemNm;
    }
    public void setRitemNm(String ritemNm) {
        this.ritemNm = ritemNm;
    }
    public String getRitemCd() {
        return ritemCd;
    }
    public void setRitemCd(String ritemCd) {
        this.ritemCd = ritemCd;
    }
    public String getOrdQty() {
        return ordQty;
    }
    public void setOrdQty(String ordQty) {
        this.ordQty = ordQty;
    }
    public String getWorkQty() {
        return workQty;
    }
    public void setWorkQty(String workQty) {
        this.workQty = workQty;
    }
    public String getUnitPrice() {
        return unitPrice;
    }
    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }
    public String getPrice() {
        return price;
    }
    public void setPrice(String price) {
        this.price = price;
    }
    public String getOrdDesc() {
        return ordDesc;
    }
    public void setOrdDesc(String ordDesc) {
        this.ordDesc = ordDesc;
    }
    public String getTradeNm() {
        return tradeNm;
    }
    public void setTradeNm(String tradeNm) {
        this.tradeNm = tradeNm;
    }
    public String getRepNm() {
        return repNm;
    }
    public void setRepNm(String repNm) {
        this.repNm = repNm;
    }
    public String getAddr() {
        return addr;
    }
    public void setAddr(String addr) {
        this.addr = addr;
    }
    public String getTel() {
        return tel;
    }
    public void setTel(String tel) {
        this.tel = tel;
    }
    public String getFax() {
        return fax;
    }
    public void setFax(String fax) {
        this.fax = fax;
    }
    public String getUserNm() {
        return userNm;
    }
    public void setUserNm(String userNm) {
        this.userNm = userNm;
    }
    public String getUserHp() {
        return userHp;
    }
    public void setUserHp(String userHp) {
        this.userHp = userHp;
    }
    public String getSupplyAmt() {
        return supplyAmt;
    }
    public void setSupplyAmt(String supplyAmt) {
        this.supplyAmt = supplyAmt;
    }
    public String getVatAmt() {
        return vatAmt;
    }
    public void setVatAmt(String vatAmt) {
        this.vatAmt = vatAmt;
    }
    public String getTotAmt() {
        return totAmt;
    }
    public void setTotAmt(String totAmt) {
        this.totAmt = totAmt;
    }
    public String getTransTradeNm() {
        return transTradeNm;
    }
    public void setTransTradeNm(String transTradeNm) {
        this.transTradeNm = transTradeNm;
    }
    public String getReqDt() {
        return reqDt;
    }
    public void setReqDt(String reqDt) {
        this.reqDt = reqDt;
    }
    public String getSeq() {
        return seq;
    }
    public void setSeq(String seq) {
        this.seq = seq;
    }
    public String getTransCustAddr() {
        return transCustAddr;
    }
    public void setTransCustAddr(String transCustAddr) {
        this.transCustAddr = transCustAddr;
    }
    public String getTransCustTel() {
        return transCustTel;
    }
    public void setTransCustTel(String transCustTel) {
        this.transCustTel = transCustTel;
    }
    public String getTransEmpNm() {
        return transEmpNm;
    }
    public void setTransEmpNm(String transEmpNm) {
        this.transEmpNm = transEmpNm;
    }
    public String getGroup1() {
        return group1;
    }
    public void setGroup1(String group1) {
        this.group1 = group1;
    }
    public String getGroup2() {
        return group2;
    }
    public void setGroup2(String group2) {
        this.group2 = group2;
    }
    public String getGroup3() {
        return group3;
    }
    public void setGroup3(String group3) {
        this.group3 = group3;
    }
    public String getGroup4() {
        return group4;
    }
    public void setGroup4(String group4) {
        this.group4 = group4;
    }
    public String getGroup5() {
        return group5;
    }
    public void setGroup5(String group5) {
        this.group5 = group5;
    }
    public String getGroup6() {
        return group6;
    }
    public void setGroup6(String group6) {
        this.group6 = group6;
    }
    public String getGroup7() {
        return group7;
    }
    public void setGroup7(String group7) {
        this.group7 = group7;
    }
    public String getGroup8() {
        return group8;
    }
    public void setGroup8(String group8) {
        this.group8 = group8;
    }
    public String getGroup9() {
        return group9;
    }
    public void setGroup9(String group9) {
        this.group9 = group9;
    }
    public String getGroup10() {
        return group10;
    }
    public void setGroup10(String group10) {
        this.group10 = group10;
    }
    public String getGroup11() {
        return group11;
    }
    public void setGroup11(String group11) {
        this.group11 = group11;
    }
    public String getGroup12() {
        return group12;
    }
    public void setGroup12(String group12) {
        this.group12 = group12;
    }
    public String getGroup13() {
        return group13;
    }
    public void setGroup13(String group13) {
        this.group13 = group13;
    }
    public String getGroup14() {
        return group14;
    }
    public void setGroup14(String group14) {
        this.group14 = group14;
    }
    public String getGroup15() {
        return group15;
    }
    public void setGroup15(String group15) {
        this.group15 = group15;
    }
    public String getGroup16() {
        return group16;
    }
    public void setGroup16(String group16) {
        this.group16 = group16;
    }
    public String getGroup17() {
        return group17;
    }
    public void setGroup17(String group17) {
        this.group17 = group17;
    }
    public String getGroup18() {
        return group18;
    }
    public void setGroup18(String group18) {
        this.group18 = group18;
    }
    public String getGroup19() {
        return group19;
    }
    public void setGroup19(String group19) {
        this.group19 = group19;
    }
    public String getGroup20() {
        return group20;
    }
    public void setGroup20(String group20) {
        this.group20 = group20;
    }
    public String getGroup21() {
        return group21;
    }
    public void setGroup21(String group21) {
        this.group21 = group21;
    }
    public String getTransCustBizNo() {
        return transCustBizNo;
    }
    public void setTransCustBizNo(String transCustBizNo) {
        this.transCustBizNo = transCustBizNo;
    }
    public String getTransCustFax() {
        return transCustFax;
    }
    public void setTransCustFax(String transCustFax) {
        this.transCustFax = transCustFax;
    }
    public String getCustBizNo() {
        return custBizNo;
    }
    public void setCustBizNo(String custBizNo) {
        this.custBizNo = custBizNo;
    }
    public String getCustNm() {
        return custNm;
    }
    public void setCustNm(String custNm) {
        this.custNm = custNm;
    }
    public String getCustRepNm() {
        return custRepNm;
    }
    public void setCustRepNm(String custRepNm) {
        this.custRepNm = custRepNm;
    }
    public String getCustAddr() {
        return custAddr;
    }
    public void setCustAddr(String custAddr) {
        this.custAddr = custAddr;
    }
    public String getCustTel() {
        return custTel;
    }
    public void setCustTel(String custTel) {
        this.custTel = custTel;
    }
    public String getCustFax() {
        return custFax;
    }
    public void setCustFax(String custFax) {
        this.custFax = custFax;
    }
    public String getOutReqDt() {
        return outReqDt;
    }
    public void setOutReqDt(String outReqDt) {
        this.outReqDt = outReqDt;
    }
    public String getEaQty() {
        return eaQty;
    }
    public void setEaQty(String eaQty) {
        this.eaQty = eaQty;
    }
    public String getUomNm() {
        return uomNm;
    }
    public void setUomNm(String uomNm) {
        this.uomNm = uomNm;
    }
    public String getCustId() {
        return custId;
    }
    public void setCustId(String custId) {
        this.custId = custId;
    } 
    
    
    

}
