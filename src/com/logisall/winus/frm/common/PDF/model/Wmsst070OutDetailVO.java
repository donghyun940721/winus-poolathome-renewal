/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;

@SuppressWarnings("PMD")
public class Wmsst070OutDetailVO extends DocDefaultVO {

	private static final long serialVersionUID = 1L;
    
    private String seq              = "";   // No
    private String ritem_nm         = "";   // 제품
    private String out_ord_qty      = "";   // 수량
    private String out_ord_uom_id   = "";   // UOM
    private String wh_nm            = "";   // 창고
    private String loc_nm           = "";   // 로케이션
    
    
    public String getSeq() {
        return seq;
    }
    public void setSeq(String seq) {
        this.seq = seq;
    }
    public String getRitem_nm() {
        return ritem_nm;
    }
    public void setRitem_nm(String ritem_nm) {
        this.ritem_nm = ritem_nm;
    }
    public String getOut_ord_qty() {
        return out_ord_qty;
    }
    public void setOut_ord_qty(String out_ord_qty) {
        this.out_ord_qty = out_ord_qty;
    }
    public String getOut_ord_uom_id() {
        return out_ord_uom_id;
    }
    public void setOut_ord_uom_id(String out_ord_uom_id) {
        this.out_ord_uom_id = out_ord_uom_id;
    }
    public String getWh_nm() {
        return wh_nm;
    }
    public void setWh_nm(String wh_nm) {
        this.wh_nm = wh_nm;
    }
    public String getLoc_nm() {
        return loc_nm;
    }
    public void setLoc_nm(String loc_nm) {
        this.loc_nm = loc_nm;
    }
    public static long getSerialversionuid() {
        return serialVersionUID;
    }
    
    
}
