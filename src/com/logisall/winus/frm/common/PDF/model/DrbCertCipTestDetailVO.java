/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;


/**
 * @author indigo(HONG SIK)
 *
 */
public class DrbCertCipTestDetailVO extends DocDefaultVO {

	private static final long serialVersionUID = 1L;
	
	private String rowNo = "";
	private String partNm = "";
	private String splyAmt = "";
	private String recpQty = "";
	private String qtyUnit = "";
	private String trif = "";
	private String domeTax = "";
	private String eduTax = "";
	private String ruspTax = "";
	private String taxSum = "";
	
	public String getRowNo() {
		return rowNo;
	}
	public void setRowNo(String rowNo) {
		this.rowNo = rowNo;
	}
	public String getPartNm() {
		return partNm;
	}
	public void setPartNm(String partNm) {
		this.partNm = partNm;
	}
	public String getSplyAmt() {
		return splyAmt;
	}
	public void setSplyAmt(String splyAmt) {
		this.splyAmt = splyAmt;
	}
	public String getRecpQty() {
		return recpQty;
	}
	public void setRecpQty(String recpQty) {
		this.recpQty = recpQty;
	}
	public String getQtyUnit() {
		return qtyUnit;
	}
	public void setQtyUnit(String qtyUnit) {
		this.qtyUnit = qtyUnit;
	}
	public String getTrif() {
		return trif;
	}
	public void setTrif(String trif) {
		this.trif = trif;
	}
	public String getDomeTax() {
		return domeTax;
	}
	public void setDomeTax(String domeTax) {
		this.domeTax = domeTax;
	}
	public String getEduTax() {
		return eduTax;
	}
	public void setEduTax(String eduTax) {
		this.eduTax = eduTax;
	}
	public String getRuspTax() {
		return ruspTax;
	}
	public void setRuspTax(String ruspTax) {
		this.ruspTax = ruspTax;
	}
	public String getTaxSum() {
		return taxSum;
	}
	public void setTaxSum(String taxSum) {
		this.taxSum = taxSum;
	}
	
	
	
}
