/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;


/**
 * @author indigo(HONG SIK)
 *
 */
public class DocPickingSampleVO extends DocDefaultVO {

    private static final long serialVersionUID = 1L;
    
    private String date = "15년04월23일";
    private String user1 = "ulnsi";
    private String user2 = "샘플_물류";
    private String user3 = "admin";
    
    private String[] number = {"0000417125", "0000417125", "0000417125", "0000417125", "0000417125", "0000417125", "0000417125"};
    private String[] goods = {"연필3", "연필3", "연필3", "연필3", "연필3", "연필3", "연필3"};
    private String[] qty = {"93", "71", "50", "100", "107", "50", "83"};
    private String[] unit = {"EA", "EA", "EA", "EA", "EA", "EA", "EA"};
    private String[] wh = {"","","","","","",""};
    private String[] location =  {"LC03", "LC03", "LC03", "LC03", "LC03", "LC03", "LC03"};
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getUser1() {
        return user1;
    }
    public void setUser1(String user1) {
        this.user1 = user1;
    }
    public String getUser2() {
        return user2;
    }
    public void setUser2(String user2) {
        this.user2 = user2;
    }
    public String getUser3() {
        return user3;
    }
    public void setUser3(String user3) {
        this.user3 = user3;
    }
    public String[] getNumber() {
        return number;
    }
    public void setNumber(String[] number) {
        this.number = number;
    }
    public String[] getGoods() {
        return goods;
    }
    public void setGoods(String[] goods) {
        this.goods = goods;
    }
    public String[] getQty() {
        return qty;
    }
    public void setQty(String[] qty) {
        this.qty = qty;
    }
    public String[] getUnit() {
        return unit;
    }
    public void setUnit(String[] unit) {
        this.unit = unit;
    }
    public String[] getWh() {
        return wh;
    }
    public void setWh(String[] wh) {
        this.wh = wh;
    }
    public String[] getLocation() {
        return location;
    }
    public void setLocation(String[] location) {
        this.location = location;
    }
    
    
    
    
}
