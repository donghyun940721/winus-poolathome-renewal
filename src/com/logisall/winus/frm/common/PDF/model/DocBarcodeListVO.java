/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;


/**
 * @author indigo(HONG SIK)
 *
 */
@SuppressWarnings("PMD")
public class DocBarcodeListVO extends DocDefaultVO {

	private static final long serialVersionUID = 1L;
    private String seq                = "";   // 순서
    private String barcode            = "";   // 바코드
    private String cust_lot_no        = "";   // LOT번호
    private String ritem_cd           = "";   // 상품코드
    private String ritem_nm           = "";   // 상품명
    private String work_qty           = "";   // 작업수량
    private String item_best_date_end = "";   // 유효일자
    private String work_weight        = "";   // 작업무게
    private String print_dt           = "";   // 출력날짜
    
    //seq
    public String getSeq() {
        return seq;
    }
    public void setSeq(String seq) {
        this.seq = seq;
    }
    
    //barcode
    public String getBarcode() {
        return barcode;
    }
    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }
    
    //cust_lot_no
    public String getCust_lot_no() {
        return cust_lot_no;
    }
    
    public void setCust_lot_no(String cust_lot_no) {
        this.cust_lot_no = cust_lot_no;
    }
    
    //ritem_cd
    public String getRitem_cd() {
        return ritem_cd;
    }
    public void setRitem_cd(String ritem_cd) {
        this.ritem_cd = ritem_cd;
    }
    
    //ritem_nm
    public String getRitem_nm() {
        return ritem_nm;
    }
    public void setRitem_nm(String ritem_nm) {
        this.ritem_nm = ritem_nm;
    }

    //work_qty
    public String getWork_qty() {
        return work_qty;
    }
    public void setWork_qty(String work_qty) {
        this.work_qty = work_qty;
    }
    
    //item_best_date_end
    public String getItem_best_date_end() {
        return item_best_date_end;
    }
    public void setItem_best_date_end(String item_best_date_end) {
        this.item_best_date_end = item_best_date_end;
    }
    
    //work_weight
    public String getWork_weight() {
        return work_weight;
    }
    public void setWork_weight(String work_weight) {
        this.work_weight = work_weight;
    }
    
    //print_dt
    public String getPrint_dt() {
        return print_dt;
    }
    public void setPrint_dt(String print_dt) {
        this.print_dt = print_dt;
    }
}
