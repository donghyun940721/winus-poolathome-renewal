/**
 * 
 */
package com.logisall.winus.frm.common.PDF.model;

@SuppressWarnings("PMD")
public class Wmsst070HeardlVO extends DocDefaultVO {

	private static final long serialVersionUID = 1L;


    
    //화면뿌릴꺼
    private String sys_dt           = "";   //발행일자
    private String user_nm          = "";   //발행자
    private String cust_nm          = "";   //화주
    private String flag         = ""; //플래그
    
    public String getFlag() {
        return flag;
    }
    public void setFlag(String flag) {
        this.flag = flag;
    }
    //얜왜???
    private String cust_id          = "";   // 화주id
    
    public String getCust_id() {
        return cust_id;
    }
    public void setCust_id(String cust_id) {
        this.cust_id = cust_id;
    }
    public String getSys_dt() {
        return sys_dt;
    }
    public void setSys_dt(String sys_dt) {
        this.sys_dt = sys_dt;
    }
    public String getUser_nm() {
        return user_nm;
    }
    public void setUser_nm(String user_nm) {
        this.user_nm = user_nm;
    }
    public String getCust_nm() {
        return cust_nm;
    }
    public void setCust_nm(String cust_nm) {
        this.cust_nm = cust_nm;
    }

}
