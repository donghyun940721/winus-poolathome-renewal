package com.logisall.winus.frm.callback;

import java.sql.Array;
import java.sql.Connection;
import java.sql.SQLException;

import oracle.jdbc.OracleTypes;
import oracle.jdbc.driver.OracleConnection;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.jdbc.support.nativejdbc.CommonsDbcpNativeJdbcExtractor;
import org.springframework.jdbc.support.nativejdbc.NativeJdbcExtractor;

import com.ibatis.sqlmap.client.extensions.ParameterSetter;
import com.ibatis.sqlmap.client.extensions.ResultGetter;
import com.ibatis.sqlmap.client.extensions.TypeHandlerCallback;
import com.ibatis.sqlmap.engine.type.JdbcTypeRegistry;

public class CallBackHandler_StringArray implements TypeHandlerCallback {

	protected Log log = LogFactory.getLog(CallBackHandler_StringArray.class);

	protected NativeJdbcExtractor jdbcExtractor;

	static {
		JdbcTypeRegistry.setType("STRING_ARRAY", OracleTypes.ARRAY);
	};

	public void setParameter(ParameterSetter setter, Object parameter)
			throws SQLException {

		String[] strArr = (String[]) parameter;
		Connection conn = setter.getPreparedStatement().getConnection();

		ArrayDescriptor stringArrayDescriptor = null;
		CommonsDbcpNativeJdbcExtractor a = new CommonsDbcpNativeJdbcExtractor();
		OracleConnection conn2 = (OracleConnection) a.getNativeConnection(conn);
		try {
			stringArrayDescriptor = ArrayDescriptor.createDescriptor(
					"STRING_ARRAY", conn2);
		} catch (Exception ex) {
			if( log.isErrorEnabled() ) {
				log.error("fail create descriptor...", ex);
			}
		}
		ARRAY valArray;
		try {
			valArray = new ARRAY(stringArrayDescriptor, conn2, strArr);
			setter.setArray(valArray);
		} catch (SQLException e) {
			if( log.isErrorEnabled() ) {
				log.error("fail set array...", e);
			}
		}
	}

	public Object getResult(ResultGetter getter) throws SQLException {
		Array arr = getter.getArray();

		if (arr == null) {
			return null;
		} else {
			return (String[]) arr.getArray();
		}
	}

	public Object valueOf(String arg0) {
		String[] r = new String[1];
		r[0] = arg0;
		return r;
	}
}
