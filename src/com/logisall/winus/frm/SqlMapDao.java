
package com.logisall.winus.frm;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.springframework.dao.DataAccessException;

import com.ibatis.sqlmap.client.event.RowHandler;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

public class SqlMapDao extends SqlMapAbstractDAO {

    public GenericResultSet executeQueryPageWqCursor(String statementName, Map map, String returnColume) {
        GenericResultSet wqrs = new GenericResultSet();

        if (!map.containsKey("pageIndex"))
            map.put("pageIndex", "1");
        if (!map.containsKey("pageSize"))
            map.put("pageSize", "15");
        Object tmpPageIndex = map.get("pageIndex");
        if (tmpPageIndex == null || tmpPageIndex.equals(""))
            tmpPageIndex = "1";
        int pageIndex = Integer.parseInt(String.valueOf(tmpPageIndex));
        int pageSize = Integer.parseInt((String)map.get("pageSize"));
        int iPageNum = (pageIndex - 1) * pageSize + 1;
        getSqlMapClientTemplate().queryForList(statementName, map);

        List list = (List)map.get(returnColume);
        List rtnList = null;
        Map<String, Object> infoMap = null;

        if (list != null) {
            rtnList = new ArrayList<Map<String, Object>>();

            for (int i = 0; i < list.size(); i++) {
                infoMap = CommonUtil.converObjectToMap(list.get(i));
                rtnList.add(infoMap);
            }
        }

        int iPageTotal = (rtnList != null) ? rtnList.size() : 0;
        int pageBlank = (int)Math.ceil((double)iPageTotal / (double)pageSize);
        if (pageBlank < 1)
            pageBlank = 1;
        map.put("S_PAGE_LEN", map.get("pageSize"));
        map.put("S_PAGE_NUM", Integer.valueOf(iPageNum));
        wqrs.setCpage(pageIndex);
        wqrs.setTpage(pageBlank);
        wqrs.setList(rtnList);
        wqrs.setTotCnt(iPageTotal);

        return wqrs;
    }
/*
    protected GenericResultSet executeQueryExcel(String statementName, Map map) {
        
        ExcelRowHandler rowHandler = null;
        try{
            rowHandler = new ExcelRowHandler(res);
        }catch(Exception e){
            e.printStackTrace();
        }
        
        GenericResultSet wqrs = new GenericResultSet();
        map.put("S_PAGE_LEN", 60000);
        map.put("S_PAGE_NUM", 1);
        wqrs.setList(getSqlMapClientTemplate().queryWithRowHandler(statementName, map, rowHandler);
        return wqrs;
    }
*/
    public static GenericResultSet getGenericResultSetFromList(Map map, List list) {
        GenericResultSet wqrs = new GenericResultSet();

        if (!map.containsKey("pageIndex"))
            map.put("pageIndex", "1");
        if (!map.containsKey("pageSize"))
            map.put("pageSize", "15");
        Object tmpPageIndex = map.get("pageIndex");
        if (tmpPageIndex == null || tmpPageIndex.equals(""))
            tmpPageIndex = "1";

        int pageIndex = Integer.parseInt(String.valueOf(tmpPageIndex));
        Object oPageSize = map.get("pageSize");
        int pageSize = 1;
        if (oPageSize instanceof Integer) {
            pageSize = (Integer)oPageSize;
        } else {
            pageSize = Integer.parseInt((String)map.get("pageSize"));
        }
        
        int iPageNum = (pageIndex - 1) * pageSize;
        int iPageTotal = (list != null) ? list.size() : 0;
        int iPageBlank = (int)Math.ceil((double)iPageTotal / (double)pageSize);
        if (iPageBlank < 1)
            iPageBlank = 1;
        map.put("S_PAGE_LEN", map.get("pageSize"));
        map.put("S_PAGE_NUM", Integer.valueOf(iPageNum));

        wqrs.setCpage(pageIndex);
        wqrs.setTpage(iPageBlank);

        int endPoint = (pageIndex * pageSize < iPageTotal) ? pageIndex * pageSize : iPageTotal;
        if (endPoint < 0)
            endPoint = 0;
        if (list != null) {
        	wqrs.setList(list.subList((pageIndex - 1) * pageSize, endPoint));
        }
        wqrs.setTotCnt(iPageTotal);
        return wqrs;
    }

    class ExcelRowHandler implements RowHandler {
        List<Map<String, String>> cdList = null;

        ExcelRowHandler(List<Map<String, String>> cdList) {
            this.cdList = cdList;
        }

        List<Object> list = new ArrayList<Object>();

        @Override
        public void handleRow(Object object) {
            try {
                for (int i = 0; i < cdList.size(); i++) {
                    Map<String, String> map = (Map<String, String>)cdList.get(i);
                    /*
                     * if (object instanceof Map) {
                     * BeanUtils.setProperty(object, "ORG_" +
                     * map.get("COL_NM").toUpperCase(),
                     * (String)BeanUtils.getProperty(object,
                     * map.get("COL_NM").toUpperCase())); //원컬럼도 저장한다. }
                     */

                    // if (map.get("PRSN_INFO_SCT_CD") == null) {
                    if (object instanceof Map) {
                        BeanUtils.setProperty(object, map.get("COL_NM").toUpperCase(), CommonUtil.nullChk((String)BeanUtils.getProperty(object, map.get("COL_NM").toUpperCase())));
                    } else {
                        BeanUtils.setProperty(object, map.get("COL_NM"), CommonUtil.nullChk((String)BeanUtils.getProperty(object, map.get("COL_NM"))));
                    }
                    // }
                }
            } catch (Exception e) {
                throw new NestedDataAccessException(getClass().toString(), e);
            }
            list.add(object);
        }

        public List<Object> getList() {
            return list;
        }
    }

    class NestedDataAccessException extends DataAccessException {

        // private static final long serialVersionUID = 8522945754441964562L;

        public NestedDataAccessException(String msg, Throwable cause) {
            super(msg, cause);
        }

    }

}
