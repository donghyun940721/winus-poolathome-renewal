package com.logisall.winus.wmsas.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSAS010Dao")
public class WMSAS010Dao extends SqlMapAbstractDAO {

	/**
	 * Method ID 	: list 
	 * Method 설명 	: CS 조회 -> (해당 원주문번호 전체이력)
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("wmsas010.list", model);
	}
	
	/**
	 * Method ID 	: subList 
	 * Method 설명 	: CS 조회 -> CS 조회 (해당 원주분번호 SEQ 이력)
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet subList(Map<String, Object> model) {
		return executeQueryPageWq("wmsas010.subList", model);
	}
	
	/**
	 * Method ID 	: orderList 
	 * Method 설명 	: CS 조회 -> 주문 조회
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet orderList(Map<String, Object> model) {
		return executeQueryPageWq("wmsas010.orderList", model);
	}
	
	/**
	 * Method ID 	: ordSubTypeList 
	 * Method 설명 	: 주문상세유형(출고) 리스트
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public GenericResultSet ordSubTypeList(Map<String, Object> model) {
		return executeQueryPageWq("wmsas010.ordSubTypeList", model);
	}
	

	/**
	 * Method ID 	: insert 
	 * Method 설명 	: CS 등록 
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("wmsas010.insert", model);
	}
	
	/**
	 * Method ID 	: update 
	 * Method 설명 	: CS 수정
	 * 작성자 : KSJ
	 * 
	 * @param model
	 * @return
	 */
	public Object update(Map<String, Object> model) {
		return executeUpdate("wmsas010.update", model);
	}
	
}
