package com.logisall.winus.wmsas.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsas.service.WMSAS010Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSAS010Service")
public class WMSAS010ServiceImpl implements WMSAS010Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSAS010Dao")
    private WMSAS010Dao dao;
    
    
    /**
     * 
     * 대체 Method ID   	: list
     * 대체 Method 설명    	: CS 조회 (해당 원주문번호 전체이력)
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   	: subList
     * 대체 Method 설명    	: CS 조회 (해당 원주분번호 SEQ 이력)
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> subList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.subList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   	: orderList
     * 대체 Method 설명    	: CS 조회창 -> 주문 정보 조회 (wmsop010, wmsop011)
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> orderList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.orderList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   	: ordSubTypeList
     * 대체 Method 설명    	: 주문상세유형(출고) 리스트
     * 작성자               : KSJ
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> ordSubTypeList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        
        map.put("LIST", dao.ordSubTypeList(model));
        return map;
    }
    
    
    /**
     * 
     * 대체 Method ID   	: save
     * 대체 Method 설명    	: CS 저장, 수정
     * 작성자               : KSJ
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
        
        int errCnt = 0;
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            
            /*
             * 1. 최초 등록 
             *   1) 처리 접수 또는 선택 안했을 경우
             *   2) 처리 완료 또는 취소일 경우
             * 
             * 2. 수정일 경우
             */
            if("INSERT".equals(model.get("ST_GUBUN"))){
                if(model.get("vrSrchComType") == null || model.get("vrSrchComType").equals("N")){
                	modelDt.put("UPD_NO", "");
    		    }else{
    		    	modelDt.put("UPD_NO", model.get(ConstantIF.SS_USER_NO));
    		    }
            }else if("UPDATE".equals(model.get("ST_GUBUN"))){
            	modelDt.put("UPD_NO", model.get(ConstantIF.SS_USER_NO));
            }
            
            // web
            //modelDt.put("vrSrchRegNm" 		, model.get("vrSrchRegNm") 		== null ? "": model.get("vrSrchRegNm"));		// 접수자명
            modelDt.put("vrSrchRegNo" 		, model.get("vrSrchRegNo") 		== null ? "": model.get("vrSrchRegNo"));		// 접수자ID
            //modelDt.put("vrSrchRegDt" 		, model.get("vrSrchRegDt") 		== null ? "": model.get("vrSrchRegDt"));		// 접수일시
            // modelDt.put("vrSrchUpdNm" 		, model.get("vrSrchUpdNm") 		== null ? "": model.get("vrSrchUpdNm"));		// 수정자명
            // modelDt.put("vrSrchUpdNo" 		, model.get("vrSrchUpdNo") 		== null ? "": model.get("vrSrchUpdNo"));		// 수정자ID
            
            modelDt.put("vrSrchUpdDt" 		, model.get("vrSrchUpdDt") 		== null ? "": model.get("vrSrchUpdDt"));		// 수정일시
            modelDt.put("vrSrchAsCsId" 		, model.get("vrSrchAsCsId") 	== null ? "": model.get("vrSrchAsCsId")); 		// cs 접수 ID
            modelDt.put("vrSrchAsCsNo" 		, model.get("vrSrchAsCsNo") 	== null ? "": model.get("vrSrchAsCsNo"));		// cs 주문번호
            modelDt.put("vrSrchAsCsSeq" 	, model.get("vrSrchAsCsSeq") 	== null ? "": model.get("vrSrchAsCsSeq"));		// cs 주문번호 순번
            modelDt.put("vrSrchComType" 	, model.get("vrSrchComType") 	== null ? "": model.get("vrSrchComType"));		// 처리상태
            modelDt.put("vrSrchInOutBound" 	, model.get("vrSrchInOutBound") == null ? "": model.get("vrSrchInOutBound")); 	// I/O바운드
            
            modelDt.put("vrSrchRegContents" , model.get("vrSrchRegContents") == null ? "": HtmlUtils.htmlUnescape(HtmlUtils.htmlUnescape(model.get("encodeRegContents").toString())));	// 접수내역
            modelDt.put("vrSrchMemo" 		, model.get("vrSrchMemo") 		 == null ? "": HtmlUtils.htmlUnescape(HtmlUtils.htmlUnescape(model.get("encodeMemo").toString()))); 		// 메모
            modelDt.put("vrSrchResult" 		, model.get("vrSrchResult") 	 == null ? "": HtmlUtils.htmlUnescape(HtmlUtils.htmlUnescape(model.get("encodeResult").toString()))); 		// 처리내역
            modelDt.put("vrSrchEtc1" 		, model.get("vrSrchEtc1") 		 == null ? "": HtmlUtils.htmlUnescape(HtmlUtils.htmlUnescape(model.get("encodeEtc1").toString()))); 		// 비고1
            
            // session
            modelDt.put("REG_NO"    		, model.get(ConstantIF.SS_USER_NO));
		    modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
		    
            
            if("INSERT".equals(model.get("ST_GUBUN"))){
            	dao.insert(modelDt);
            }else if("UPDATE".equals(model.get("ST_GUBUN"))){
            	dao.update(modelDt);
            }else{
            	errCnt++;
                m.put("errCnt", errCnt);
                throw new BizException(MessageResolver.getMessage("save.error"));
            }
            
            m.put("errCnt", errCnt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (BizException be) {
            if (log.isInfoEnabled()) {
                log.info(be.getMessage());
            }
            m.put("MSG", be.getMessage());
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
}
