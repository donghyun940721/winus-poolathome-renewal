package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF204Service {
	public Map<String, Object> turnOnOff(Map<String, Object> model) throws Exception;
	public Map<String, Object> getStatus(Map<String, Object> model) throws Exception;
}
