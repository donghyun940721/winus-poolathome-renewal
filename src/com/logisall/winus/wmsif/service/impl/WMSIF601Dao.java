package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF601Dao")
public class WMSIF601Dao extends SqlMapAbstractDAO{
	
	/**
     * Method ID : list
     * Method 설명 : 오픈몰 주문 리스트 (사방넷)
     * 작성자 : schan
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif601.list", model);
    }
    
    /**
     * Method ID : listE2
     * Method 설명 : 송장송신 (사방넷)
     * 작성자 : sing09
     * @param model
     * @return
     */
    public GenericResultSet listE2(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif601.listE2", model);
    }
    
    /**
     * Method ID  : listE3
     * Method 설명   : 
     * 작성자                : sing09
     * @param   model
     * @return
     */
    public GenericResultSet listE3(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif601.listE3", model);
    }
    
    /**
     * Method ID : parcelList
     * Method 설명 : 송장송신 (사방넷) XML
     * 작성자 : sing09
     * @param model
     * @return
     */
    public GenericResultSet parcelList(Map<String, Object> model) {
    	return executeQueryPageWq("wmsif601.parcelList", model);
    }
    
    /**
     * Method ID : sapbp
     * Method 설명 : sapbp
     * 작성자 : sing09
     * @param model
     * @return
     */
    public GenericResultSet sapbp(Map<String, Object> model) {
        return executeQueryPageWq("wmsif601.sapbp", model);
    }
    
    /**
	 * Method ID : saveSap
	 * Method 설명 : sap 마스터 수정 저장
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void saveSap(Map<String, Object> model) {
		executeUpdate("wmsif601.saveSap", model);
		
	}
	/**
	 * Method ID : insertSap
	 * Method 설명 : sap 마스터 신규 저장
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void insertSap(Map<String, Object> model) {
		executeInsert("wmsif601.insertSap", model);
		
	}
	/**
	 * Method ID : deleteSap
	 * Method 설명 : sap 마스터 삭제
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void deleteSap(Map<String, Object> model) {
		executeInsert("wmsif601.deleteSap", model);
		
	}

    /**
     * Method ID	: getOpenMallApiAuthMaster
     * Method 설명 	: 오픈몰 API AUTH 마스터 리스트
	 * 작성자 : 
     * @param model
     * @return
     */
    public Object getOpenMallApiAuthMaster(Map<String, Object> model){
        return executeQueryForList("wmsif601.getOpenMallApiAuthMaster", model);
    }
    
	/**
	 * Method ID : sapListDelete
	 * Method 설명 : sap 주문 삭제
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void sapListDelete(Map<String, Object> model) throws Exception {
		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			String orgOrdId = (String) model.get("IDX");
			// existRsSet = WMSOP010 원주문번호로 조회 DEL_YN 체크
			String existRsSet = (String)sqlMapClient.queryForObject("wmsif601.selectDelYn", orgOrdId);
			
			// existRsSet NULL 일 경우 OP010에 등록이 안된 상태
			if ((String)existRsSet == null){
//				throw new Exception("주문번호(사방넷) : "+orgOrdId+" 은/는 등록되지 않은 주문입니다. ");
				sqlMapClient.delete("wmsif601.deleteDelYn"	, orgOrdId);
			}else{
				// existRsSet Y 일 경우 삭제 
				if(existRsSet.equals("N")){
					// existRsSet Y 아닐 경우 삭제 X 
					throw new Exception("주문번호(사방넷) : "+orgOrdId+" 의 출고 주문을 먼저 삭제해주세요.");
				}else{
					sqlMapClient.delete("wmsif601.deleteDelYn"	, orgOrdId);
				}
			}
			
    		sqlMapClient.endTransaction();
		} catch(Exception e) {
			sqlMapClient.endTransaction();
			e.printStackTrace();
			throw e;
			
		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
	
	 /**
	 * Method ID : insert 
		 * Method 설명 : 인터페이스 이력 등록
		 * 작성자 : sing09
		 * @param model
		 * @return
		 */
    public Object insert(Map<String, Object> model) {
		return executeInsert("wmsif601.insert", model);
	}
}
