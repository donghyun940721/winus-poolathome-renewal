package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.wmsif.service.OliveyoungDashService;
import com.logisall.winus.wmsif.service.WMSIF301Service;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.ws.interfaces.common.RestApiUtil;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("OliveyoungDashService")
public class OliveyoungDashServiceImpl implements OliveyoungDashService{
    protected Log log = LogFactory.getLog(this.getClass());

//    @Resource(name = "WMSIF301Dao")
//    private WMSIF301Dao dao;
    
    /**
     * 
     * 대체 Method ID   : crossDomainHttps
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        
        try{
        	String sUrl = "https://52.79.206.98:5101/invoke/" + (String)model.get("URL");
	        System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        //https ssl
	        disableSslVerification();
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "eaiuser01" + ":" + "eaiuser01";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	
        	int resCode = 0;
        	resCode = con.getResponseCode();
        	System.out.println("resCode: " + resCode);
        	
        	if(resCode < 400){
        		m.put("errCnt", 0);
                m.put("MSG", "처리되었습니다.");
        	}else{
        		//result = con.getResponseMessage();
        		m.put("errCnt", -1);
	            m.put("MSG", "I/F 처리오류. 코드:"+resCode+" (관리자문의)");
        	}
        	
        	con.disconnect();
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID	: crossDomainHttpWs
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String sUrl = "";
        
    	sUrl = "http://52.79.206.98:5555/DASHBOARD/TODAY_JOB_STATUS";
        
        try{
        	//ssl disable
        	disableSslVerification();
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "eaiuser01" + ":" + "eaiuser01";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			//Json Data
			String jsonInputString = "{\"orderResult\":  {\"fromDt\":\""+(String) model.get("fromDt")+"\""
									+					",\"toDt\":\""+(String) model.get("toDt")+"\"}}";
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()) {
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            JSONObject jsonData = new JSONObject(response.toString());
                m.put("result"	, response.toString());
	        }
	        
        	con.disconnect();
        	//System.out.println(">> end api");
        	
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	@Override
	public Map<String, Object> crossDomainHttpWs2(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String sUrl = "";
        
    	sUrl = "http://52.79.206.98:5555/DASHBOARD/TODAY_JOB_PRODUCTVITY";
        
        try{
        	//ssl disable
        	disableSslVerification();
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        HttpURLConnection con = null;
        	con = (HttpURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "eaiuser01" + ":" + "eaiuser01";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type", "application/json");
			con.setRequestProperty("Accept", "application/json");
			//Json Data
			String jsonInputString = "{\"reqeust\":  {\"selWorkPcnt\":\""+(String) model.get("selWorkPcnt")+"\"}}";
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()) {
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            JSONObject jsonData = new JSONObject(response.toString());
                m.put("result"	, response.toString());
	        }
	        
        	con.disconnect();
        	//System.out.println(">> end api2");
        	
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
}
