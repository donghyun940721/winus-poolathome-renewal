package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsif.service.WMSIF600Service;
import com.logisall.winus.wmsop.service.impl.WMSOP030Dao;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIF600Service")
public class WMSIF600ServiceImpl extends AbstractServiceImpl implements WMSIF600Service{

	@Resource(name = "WMSIF600Dao")
    private WMSIF600Dao dao;
	
	@Resource(name = "WMSIF610Dao")
	private WMSIF610Dao dao610;
	
	@Resource(name = "WMSOP030Dao")
    private WMSOP030Dao dao1;
	
	/**
     * Method ID : list
     * Method 설명 : 오픈몰 주문 리스트 (사방넷)
     * 작성자 : schan
     * @param model
     * @return
     */
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
        	//주문번호(사방넷)
        	List<String> vrSrchIdxArr = new ArrayList();
            String[] spVrSrchIdx = model.get("vrSrchIdx").toString().split(",");
            for (String keyword : spVrSrchIdx ){
            	vrSrchIdxArr.add(keyword.replaceAll("(\r\n|\r|\n|\n\r|\\p{Z}|\\t)", ""));
            }
            model.put("vrSrchIdxArr", vrSrchIdxArr);
            
            //주문번호(쇼핑몰)
            List<String> vrSrchOrdIdArr = new ArrayList();
            String[] spVrSrchOrdId = model.get("vrSrchOrdId").toString().split(",");
            for (String keyword : spVrSrchOrdId ){
            	vrSrchOrdIdArr.add(keyword.replaceAll("(\r\n|\r|\n|\n\r|\\p{Z}|\\t)", ""));
            }
            model.put("vrSrchOrdIdArr", vrSrchOrdIdArr);
            
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}

	/**
     *  Method ID 		 :  
     *  Method 설명  	 : 오픈몰 주문 등록
     *  작성자            	 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveListOrderJavaVX(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{

            int tmpCnt = Integer.parseInt(model.get("selectIds").toString());

            // === 실제 들어가는 데이터 변수
            String[] no             = new String[tmpCnt];
            String[] eIdx 			= new String[tmpCnt];
            String[] eProductName 	= new String[tmpCnt];
            String[] eSapCust 	= new String[tmpCnt];
            String[] eReceiveName 	= new String[tmpCnt];
            String[] eReceiveTel 	= new String[tmpCnt];
            String[] eReceiveCel 	= new String[tmpCnt];
            String[] eReceiveAddr 	= new String[tmpCnt];
            String[] eDelvMsg 		= new String[tmpCnt];
            String[] eOutReqDt 		= new String[tmpCnt];
            String[] eReceiveZipcode	= new String[tmpCnt];
            String[] eCompaynyCoodsCd = new String[tmpCnt];
            String[] eSaleSnt 		= new String[tmpCnt];
            String[] eMallId 		= new String[tmpCnt];
            String[] eOrderId 		= new String[tmpCnt];
            String[] eOrdDegree 	= new String[tmpCnt];
            String[] eQtipTp 		= new String[tmpCnt];
            String[] eCustCd 		= new String[tmpCnt];
            String[] eUnitNo 		= new String[tmpCnt];
            String[] eOutWhCd 		= new String[tmpCnt];

            //== NULL 처리를 위한 데이터 변수
            String[] outReqDt       = new String[tmpCnt];         
            String[] inReqDt        = new String[tmpCnt];     
            String[] custOrdNo      = new String[tmpCnt];     
            String[] custOrdSeq     = new String[tmpCnt];    
            String[] trustCustCd    = new String[tmpCnt];     
            
            String[] transCustCd    = new String[tmpCnt];                     
            String[] transCustTel   = new String[tmpCnt];         
            String[] transReqDt     = new String[tmpCnt];     
            String[] custCd         = new String[tmpCnt];
            String[] ordQty         = new String[tmpCnt];     
            
            String[] uomCd          = new String[tmpCnt];                
            String[] sdeptCd        = new String[tmpCnt];         
            String[] salePerCd      = new String[tmpCnt];     
            String[] carCd          = new String[tmpCnt];     
            String[] drvNm          = new String[tmpCnt];     
            
            String[] dlvSeq         = new String[tmpCnt];                
            String[] drvTel         = new String[tmpCnt];         
            String[] custLotNo      = new String[tmpCnt];     
            String[] blNo           = new String[tmpCnt];     
            String[] recDt          = new String[tmpCnt];     
            
            String[] outWhCd        = new String[tmpCnt];                
            String[] inWhCd         = new String[tmpCnt];         
            String[] makeDt         = new String[tmpCnt];     
            String[] timePeriodDay  = new String[tmpCnt];     
            String[] workYn         = new String[tmpCnt];     
            
            String[] rjType         = new String[tmpCnt];                
            String[] locYn          = new String[tmpCnt];         
            String[] confYn         = new String[tmpCnt];     
            String[] eaCapa         = new String[tmpCnt];     
            String[] inOrdWeight    = new String[tmpCnt];     
            
            String[] itemCd         = new String[tmpCnt];                
            String[] itemNm         = new String[tmpCnt];         
            String[] transCustNm    = new String[tmpCnt];     
            String[] transCustAddr  = new String[tmpCnt];     
            String[] transEmpNm     = new String[tmpCnt];     
            
            String[] remark         = new String[tmpCnt];                
            String[] transZipNo     = new String[tmpCnt];         
            String[] etc2           = new String[tmpCnt];     
            String[] unitAmt        = new String[tmpCnt];     
            String[] transBizNo     = new String[tmpCnt];     
            
            String[] inCustAddr     = new String[tmpCnt];                
            String[] inCustCd       = new String[tmpCnt];         
            String[] inCustNm       = new String[tmpCnt];     
            String[] inCustTel      = new String[tmpCnt];     
            String[] inCustEmpNm    = new String[tmpCnt];     
            
            String[] expiryDate     = new String[tmpCnt];
            String[] salesCustNm    = new String[tmpCnt];
            String[] zip     		= new String[tmpCnt];
            String[] addr     		= new String[tmpCnt];
            String[] addr2     		= new String[tmpCnt];
            String[] phone1    	 	= new String[tmpCnt];
            
            String[] etc1    		= new String[tmpCnt];
            String[] unitNo    		= new String[tmpCnt];               
            String[] timeDate       = new String[tmpCnt];   //상품유효기간     
            String[] timeDateEnd    = new String[tmpCnt];   //상품유효기간만료일
            String[] timeUseEnd     = new String[tmpCnt];   //소비가한만료일
            
            String[] phone2			= new String[tmpCnt];   //고객전화번호2
            String[] buyCustNm		= new String[tmpCnt];   //주문자명
            String[] buyPhone1		= new String[tmpCnt];   //주문자전화번호1
            String[] salesCompanyNm	= new String[tmpCnt];   //salesCompanyNm
            String[] ordDegree		= new String[tmpCnt];   //주문등록차수
            String[] bizCond		= new String[tmpCnt];   //업태
            String[] bizType		= new String[tmpCnt];   //업종
            String[] bizNo			= new String[tmpCnt];   //사업자등록번호
            String[] custType		= new String[tmpCnt];   //화주타입
            
            String[] dataSenderNm		= new String[tmpCnt];   //쇼핑몰
            String[] legacyOrgOrdNo		= new String[tmpCnt];   //사방넷주문번호
            
            String[] custSeq 			=  new String[tmpCnt];  
            String[] ordDesc 			=  new String[tmpCnt];   //ord_desc
            String[] dlvMsg1 			=  new String[tmpCnt];   //배송메세지1
            String[] dlvMsg2 			=  new String[tmpCnt];   //배송메세지2
           
            String[] eSendComId 		=  new String[tmpCnt];   //사방넷 계정
            
            // I_ 로 시작하는 변수는 안담김
            for (int i = 0; i < tmpCnt; i++) {
            	 // === NULL 처리를 위한 데이터 변수
            	String OUT_REQ_DT		= "";
    			String IN_REQ_DT		= "";
    			String CUST_ORD_NO		= "";
    			String CUST_ORD_SEQ		= "";
    			String TRUST_CUST_CD	= "";
    			String TRANS_CUST_CD	= "";
    			String TRANS_CUST_TEL	= "";
    			String TRANS_REQ_DT		= "";
    			String CUST_CD			= "";
    			String ORD_QTY			= "";
    			String UOM_CD			= "";
    			String SDEPT_CD			= "";
    			String SALE_PER_CD		= "";
    			String CAR_CD			= "";
    			String DRV_NM			= "";
    			String DLV_SEQ			= "";
    			String DRV_TEL			= "";
    			String CUST_LOT_NO		= "";
    			String BL_NO			= "";
    			String REC_DT			= "";
    			String OUT_WH_CD		= "";
    			String IN_WH_CD			= "";
    			String MAKE_DT			= "";
    			String TIME_PERIOD_DAY	= "";
    			String WORK_YN			= "";
    			String RJ_TYPE			= "";
    			String LOC_YN			= "";
    			String CONF_YN			= "";
    			String EA_CAPA			= "";
    			String IN_ORD_WEIGHT	= "";
    			String ITEM_CD			= "";
    			String ITEM_NM			= "";
    			String TRANS_CUST_NM	= "";
    			String TRANS_CUST_ADDR	= "";
    			String TRANS_EMP_NM		= "";
    			String REMARK			= "";
    			String TRANS_ZIP_NO		= "";
    			String ETC2				= "";
    			String UNIT_AMT			= "";
    			String TRANS_BIZ_NO		= "";
    			String IN_CUST_ADDR		= "";
    			String IN_CUST_CD		= "";
    			String IN_CUST_NM		= "";
    			String IN_CUST_TEL		= "";
    			String IN_CUST_EMP_NM	= "";
    			String EXPIRY_DATE		= "";
    			String SALES_CUST_NM	= "";
    			String ZIP				= "";
    			String ADDR				= "";
    			String ADDR2			= "";
    			String PHONE_1			= "";
    			String ETC1				= "";
    			String UNIT_NO			= "";
    			String TIME_DATE		= "";
    			String TIME_DATE_END	= "";
    			String TIME_USE_END		= "";
    			String PHONE_2			= "";
    			String BUY_CUST_NM		= "";
    			String BUY_PHONE_1		= "";
    			String SALES_COMPANY_NM	= "";
    			String ORD_DEGREE		= "";
    			String BIZ_COND			= "";
    			String BIZ_TYPE			= "";
    			String BIZ_NO			= "";
    			String CUST_TYPE		= "";
    			String DATA_SENDER_NM 	= "";
    			String LEGACY_ORG_ORD_NO ="";
    			String CUST_SEQ 		= "";
    			String ORD_DESC = "";
    			String DLV_MSG1 = "";
    			String DLV_MSG2 = "";
    			
    			//==== 실제 들어가는 데이터
            	String NO = Integer.toString(i+1);
            	no[i]               	= NO;
            	eIdx[i] 				= (String)model.get("IDX_" + i);
            	eProductName[i] 		= (String)model.get("P_PRODUCT_NAME_" + i);
            	eSapCust[i] 			= (String)model.get("colSAP_CUST" + i);
            	eReceiveName[i] 		= (String)model.get("RECEIVE_NAME_" + i);
            	eReceiveTel[i] 			= (String)model.get("RECEIVE_TEL_" + i);
            	eReceiveCel[i]			= (String)model.get("RECEIVE_CEL_" + i);
            	eReceiveAddr[i] 		= (String)model.get("RECEIVE_ADDR_" + i);
                
            	eDelvMsg[i] 			= (String)model.get("DELV_MSG_" + i);
            	eOutReqDt[i] 			= (String)model.get("OUT_REQ_DT" + i);
            	eReceiveZipcode[i] 		= (String)model.get("RECEIVE_ZIPCODE_" + i);
            	eCompaynyCoodsCd[i] 	= (String)model.get("COMPAYNY_GOODS_CD_" + i);
            	eSaleSnt[i] 			= (String)model.get("SALE_CNT_" + i);
            	eMallId[i] 				= (String)model.get("MALL_ID_" + i);
            	eOrderId[i] 			= (String)model.get("ORDER_ID_" + i);
            	eOrdDegree[i] 			= (String)model.get("colORD_DEGREE_" + i);
            	eCustCd[i] 				= (String)model.get("colCUST_CD_" + i);
            	eUnitNo[i] 				= (String)model.get("colUNIT_NO_" + i);
            	eOutWhCd[i] 			= (String)model.get("colOUT_WH_CD_" + i);
            	eQtipTp[i] 				= (String)model.get("colQTIO_TP_" + i);
            	eSendComId[i] 			= (String)model.get("colSEND_COMPAYNY_ID_" + i);
            	
            	//==== NULL 처리 데이터
            	outReqDt[i]         = OUT_REQ_DT;    
                inReqDt[i]          = IN_REQ_DT;    
                custOrdNo[i]        = CUST_ORD_NO;    
                custOrdSeq[i]       = CUST_ORD_SEQ;    
                trustCustCd[i]      = TRUST_CUST_CD;    
                
                transCustCd[i]      = TRANS_CUST_CD;    
                transCustTel[i]     = TRANS_CUST_TEL;    
                transReqDt[i]       = TRANS_REQ_DT;    
                custCd[i]           = CUST_CD;
                ordQty[i]           = ORD_QTY;    
                
                uomCd[i]            = UOM_CD;    
                sdeptCd[i]          = SDEPT_CD;    
                salePerCd[i]        = SALE_PER_CD;    
                carCd[i]            = CAR_CD;
                drvNm[i]            = DRV_NM;
                
                dlvSeq[i]           = DLV_SEQ;    
                drvTel[i]           = DRV_TEL;    
                custLotNo[i]        = CUST_LOT_NO;    
                blNo[i]             = BL_NO;    
                recDt[i]            = REC_DT;    
                
                outWhCd[i]          = OUT_WH_CD;
                inWhCd[i]           = IN_WH_CD;
                makeDt[i]           = MAKE_DT;
                timePeriodDay[i]    = TIME_PERIOD_DAY;
                workYn[i]           = WORK_YN;
                
                rjType[i]           = RJ_TYPE;    
                locYn[i]            = LOC_YN;    
                confYn[i]           = CONF_YN;    
                eaCapa[i]           = EA_CAPA;    
                inOrdWeight[i]      = IN_ORD_WEIGHT;   
                
                itemCd[i]           = ITEM_CD;    
                itemNm[i]           = ITEM_NM;    
                transCustNm[i]      = TRANS_CUST_NM;    
                transCustAddr[i]    = TRANS_CUST_ADDR;    
                transEmpNm[i]       = TRANS_EMP_NM;    

                remark[i]           = REMARK;    
                transZipNo[i]       = TRANS_ZIP_NO;    
                etc2[i]             = ETC2;    
                unitAmt[i]          = UNIT_AMT;    
                transBizNo[i]       = TRANS_BIZ_NO;    
                
                inCustAddr[i]       = IN_CUST_ADDR;   
                inCustCd[i]         = IN_CUST_CD;    
                inCustNm[i]         = IN_CUST_NM;    
                inCustTel[i]        = IN_CUST_TEL;    
                inCustEmpNm[i]      = IN_CUST_EMP_NM;    
                
                expiryDate[i]       = EXPIRY_DATE;
                salesCustNm[i]      = SALES_CUST_NM;
                zip[i]       		= ZIP;
                addr[i]       		= ADDR;
                addr2[i]       		= ADDR2;
                phone1[i]       	= PHONE_1;
                
                etc1[i]      		= ETC1;
                unitNo[i]      		= UNIT_NO;
                timeDate[i]         = TIME_DATE;      
                timeDateEnd[i]      = TIME_DATE_END;      
                timeUseEnd[i]       = TIME_USE_END;  
                
                phone2[i]       	= PHONE_2;     
                buyCustNm[i]       	= BUY_CUST_NM;     
                buyPhone1[i]       	= BUY_PHONE_1;
                salesCompanyNm[i]   = SALES_COMPANY_NM;
                ordDegree[i]       	= ORD_DEGREE;
                bizCond[i]       	= BIZ_COND;
                bizType[i]       	= BIZ_TYPE;
                bizNo[i]       		= BIZ_NO;
                custType[i]       	= CUST_TYPE;
                
                dataSenderNm[i]     = DATA_SENDER_NM;
                legacyOrgOrdNo[i]   = LEGACY_ORG_ORD_NO;
                custSeq[i]			= CUST_SEQ;

                ordDesc[i]     			  	= ORD_DESC;
                dlvMsg1[i]       			= DLV_MSG1;
                dlvMsg2[i]       			= DLV_MSG2;
                
            }
          
            // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelIns = new HashMap<String, Object>();

            modelIns.put("vrOrdType"		, (String)model.get("colI_ORD_TYPE")); // 주문타입 
            modelIns.put("no"  				, no);
            modelIns.put("reqDt"			, eOutReqDt);
            modelIns.put("whCd"				, eOutWhCd);
            modelIns.put("custOrdNo"		, eIdx);

            modelIns.put("custOrdSeq"   	, custOrdSeq);
            modelIns.put("trustCustCd"  	, trustCustCd); 
            modelIns.put("transCustCd"  	, transCustCd);
            modelIns.put("transCustTel" 	, transCustTel);
            modelIns.put("transReqDt"   	, transReqDt);

            modelIns.put("custCd"			, eCustCd);
            modelIns.put("ordQty"			, eSaleSnt);
            modelIns.put("uomCd"        	, uomCd);
            modelIns.put("sdeptCd"      	, sdeptCd);
            modelIns.put("salePerCd"    	, salePerCd);

            modelIns.put("carCd"        	, carCd);
            modelIns.put("drvNm"        	, drvNm);       
            modelIns.put("dlvSeq"       	, dlvSeq);
            modelIns.put("drvTel"       	, drvTel);
            modelIns.put("custLotNo"    	, custLotNo);

            modelIns.put("blNo"         	, blNo);
            modelIns.put("recDt"        	, recDt);      
            modelIns.put("makeDt"       	, makeDt);
            modelIns.put("timePeriodDay"	, timePeriodDay);
            modelIns.put("workYn"       	, workYn);

            modelIns.put("rjType"       	, rjType);
            modelIns.put("locYn"        	, locYn);    
            modelIns.put("confYn"       	, confYn);     
            modelIns.put("eaCapa"       	, eaCapa);
            modelIns.put("inOrdWeight"  	, inOrdWeight); 

            modelIns.put("itemCd"			, eCompaynyCoodsCd);
            modelIns.put("itemNm"			, eProductName);
            modelIns.put("transCustNm"      , transCustNm);
            modelIns.put("transCustAddr"    , transCustAddr);
            modelIns.put("transEmpNm"       , transEmpNm);

            modelIns.put("remark"           , remark);
            modelIns.put("transZipNo"       , transZipNo);
            modelIns.put("etc2"             , eSapCust); // WMSOM010.ORD_DESC 와 매핑
            modelIns.put("unitAmt"          , unitAmt);
            modelIns.put("transBizNo"       , transBizNo);

            modelIns.put("inCustAddr"       , inCustAddr);
            modelIns.put("inCustCd"         , inCustCd);
            modelIns.put("inCustNm"         , inCustNm);                 
            modelIns.put("inCustTel"        , inCustTel);
            modelIns.put("inCustEmpNm"      , inCustEmpNm);

            modelIns.put("expiryDate"       , expiryDate);
            modelIns.put("salesCustNm"      , eReceiveName);
            modelIns.put("zip"				, eReceiveZipcode);
            modelIns.put("addr"       		, eReceiveAddr);
            modelIns.put("addr2"       		, addr2);

            modelIns.put("phone1"			, eReceiveTel);
            modelIns.put("etc1"				, eDelvMsg);
            modelIns.put("unitNo"			, eUnitNo);
            modelIns.put("phone2"			, eReceiveCel);
            modelIns.put("buyCustNm"		, buyCustNm);  

            modelIns.put("buyPhone1"		, buyPhone1);
            modelIns.put("salesCompanyNm"	, eQtipTp);
            modelIns.put("ordDegree"		, eOrdDegree);
            modelIns.put("bizCond"			, bizCond);
            modelIns.put("bizType"			, bizType);

            modelIns.put("bizNo"			, bizNo);
            modelIns.put("custType"			, custType);
            modelIns.put("dataSenderNm"		, eMallId);
            modelIns.put("legacyOrgOrdNo"	, eOrderId);
            modelIns.put("custSeq"       	, custSeq);
            
            modelIns.put("ordDesc"       	, ordDesc);
            modelIns.put("dlvMsg1"       	, dlvMsg1);
            modelIns.put("dlvMsg2"       	, dlvMsg2);
            
            modelIns.put("LC_ID",  	 (String)model.get("colI_LC_ID"));
            modelIns.put("WORK_IP",  (String)model.get("colI_WORK_IP"));
            modelIns.put("USER_NO",  (String)model.get("colI_USER_NO"));
            
            modelIns = (Map<String, Object>)dao1.saveExcelOrderB2T(modelIns);
            ServiceUtil.isValidReturnCode("WMSIF600", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            if(String.valueOf(modelIns.get("O_MSG_CODE")).equals("0")){
            	//프로시져에 보낼것들 다담는다
                Map<String, Object> modelCallback = new HashMap<String, Object>();
                modelCallback.put("IDX"			, eIdx);
                modelCallback.put("ORDER_ID"	, eOrderId);
                modelCallback.put("SEND_ID"		, eSendComId);
                modelCallback.put("CUST_ID"		, (String)model.get("colCUST_ID"));
                modelCallback.put("WORK_IP"		, (String)model.get("colI_WORK_IP"));
                modelCallback.put("USER_NO"		, (String)model.get("colI_USER_NO"));
                
            	dao.saveSabangnetCallback(modelCallback);
            }
            
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("MSG_ORA", "");
            m.put("errCnt", 0);
            
        }catch(BizException be) {
            m.put("errCnt", -1);
            m.put("MSG_ORA", be.getMessage());
            
        }catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : sapbp
     * Method 설명 : sapbp 마스터
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> sapbp(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		map.put("LIST", dao.sapbp(model));
    	} catch (Exception e) {
    		log.error(e.toString());
    		map.put("MSG", MessageResolver.getMessage("list.error"));
    	}
    	return map;
    }   
    
    /**
     * Method ID : crudSap
     * Method 설명 : sap 마스터 CRUD
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> crudSap(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("USER_ID", 			model.get("USER_ID" + i));
	             modelDt.put("MALL_ID", 			model.get("MALL_ID" + i));
	             modelDt.put("PARTNER_ID", 			model.get("PARTNER_ID" + i));
	             modelDt.put("CUST_CD", 			model.get("CUST_CD" + i));
	             modelDt.put("PLANT_CD", 			model.get("PLANT_CD" + i));
	             modelDt.put("SAP_BP_CODE", 		model.get("SAP_BP_CODE" + i));
	             modelDt.put("SAP_BP_NAME", 		model.get("SAP_BP_NAME" + i));
	             modelDt.put("STORAGE_LOCATION", 	model.get("STORAGE_LOCATION" + i));
	             modelDt.put("ETC", 				model.get("ETC" + i));
	             modelDt.put("WMS_ORD_FLAG", 		model.get("WMS_ORD_FLAG" + i));
	             
	             modelDt.put("BF_USER_ID", 			model.get("BF_USER_ID" + i));
	             modelDt.put("BF_MALL_ID", 			model.get("BF_MALL_ID" + i));
	             modelDt.put("BF_PARTNER_ID", 		model.get("BF_PARTNER_ID" + i));
	             modelDt.put("BF_CUST_CD", 			model.get("BF_CUST_CD" + i));
	             modelDt.put("BF_PLANT_CD", 		model.get("BF_PLANT_CD" + i));
	             modelDt.put("BF_SAP_BP_CODE", 		model.get("BF_SAP_BP_CODE" + i));
	             modelDt.put("BF_STORAGE_LOCATION", model.get("BF_STORAGE_LOCATION" + i));
	             
	             modelDt.put("SYS_REG_USER", 		model.get("SYS_REG_USER"));
	             
	             String stGubun = (String)model.get("ST_GUBUN" + i);
	             if("UPDATE".equals(stGubun)){
	            	 dao.saveSap(modelDt);
	             }else if ("INSERT".equals(stGubun)){
	            	 dao.insertSap(modelDt);
	             }else if ("DELETE".equals(stGubun)){
	            	 dao.deleteSap(modelDt);
	             }
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
    
    /**
     * Method ID : sapListDelete
     * Method 설명 : sap 주문 리스트  Delete
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> sapListDelete(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	try {
    		int errCnt = 0;
    		for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
    			Map<String, Object> modelDt = new HashMap<String, Object>();
    			modelDt.put("IDX", 			model.get("IDX_" + i));
				dao.sapListDelete(modelDt);
    		}
    		
    		map.put("errCnt", errCnt);
    		map.put("MSG_ORA", "");
    		map.put("MSG", MessageResolver.getMessage("delete.success"));
    		
    	} catch (Exception e) {
    		map.put("MSG", MessageResolver.getMessage("save.error"));
    		map.put("MSG_ORA", e.getMessage());
    		map.put("errCnt", "1");
    		throw e;
    		
    	}
    	
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID	: crossDomainHttpWs
     * 대체 Method 설명	:
     * 작성자			: chsong
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String sUrl = "";
        
        String cUrl = (String)model.get("url");
        if("TASK_KAKAOVX_SABANG_NET_CALL".equals(cUrl)){
        	//sUrl = "https://52.79.206.98:5201/KAKAOVX/SABANGNET_CALL"; //운영
        	sUrl = "https://172.31.10.253:5201/KAKAOVX/SABANGNET_CALL"; //개발
        }
        
        try{
        	//ssl disable
        	disableSslVerification();
	        System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass 	= "eaiuser01" + ":" + "eaiuser01";
        	String basicAuth 	= "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			//Json Data
			String jsonInputString = "{\"sendCompanyId\"	:\""+(String) model.get("sendCompanyId")+"\""
									+",\"sendAuthKey\"		:\""+(String) model.get("sendAuthKey")+"\""
									+",\"ordStDate\"		:\""+(String) model.get("ordStDate").toString().replaceAll("-", "")+"\""
									+",\"ordEdDate\"		:\""+(String) model.get("ordEdDate").toString().replaceAll("-", "")+"\""
									+",\"orderStatus\"		:\""+(String) model.get("orderStatus")+"\""
									+ "}";
			
			//Json Data His
			String jsonInputHis = "수집일자 : "+(String) model.get("ordStDate").toString()+" ~ "+(String) model.get("ordEdDate").toString()+""
					+" , 계정 : "+(String) model.get("sendCompanyId")+" , 상태 : "+(String) model.get("orderStatusText");
			String sbnetCount = "";
			
			//JSON 보내는 Output stream
	        try(OutputStream os = con.getOutputStream()) {
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
	        
	        //Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            
	            JSONObject jsonObject = new JSONObject(response.toString());
	            JSONObject json_step1 = jsonObject.getJSONObject("response");
	            JSONObject json_step2 = json_step1.getJSONObject("SABANG_ORDER_LIST");
	            JSONObject json_step3 = json_step2.getJSONObject("HEADER");
	            sbnetCount = json_step3.getString("TOTAL_COUNT");
	            
                m.put("rst", MessageResolver.getMessage("save.success"));
	        }catch(Exception e){
	        	m.put("rst", MessageResolver.getMessage("save.error"));
	        }
	        
	        int resCode = 0;
        	resCode = con.getResponseCode();
        	
        	con.disconnect();

            //종종 사방넷측 API 커넥션이 안되는듯함. EAI는 잘 타지만 Response가 정상적이지 않음.
            if( sbnetCount == "" ) {
                resCode = 400;
            }
            
        	Map<String, Object> modelHis = new HashMap<String, Object>();
        	modelHis.put("LC_ID"				, "0000003200");
        	modelHis.put("CUST_ID"			, "0000295694");
        	modelHis.put("INTERFACE_CODE"	, cUrl); 
			modelHis.put("INTERFACE_ETC1"	, "조건처리"); 
			modelHis.put("gubunHis1"		, "PASS"); 
			modelHis.put("INTERFACE_ETC2"	, jsonInputHis+", 조회건수 : "+sbnetCount+" 건"); 
			modelHis.put("INTERFACE_ETC3"	, resCode); 
			modelHis.put("USER_NO"			, model.get(ConstantIF.SS_USER_NO)); 
			//카카오 VX 인터페이스 수동처리 이력 생성
			dao610.insert(modelHis);
			
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : chsong
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
}
