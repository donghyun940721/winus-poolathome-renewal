package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF001Dao")
public class WMSIF001Dao extends SqlMapAbstractDAO{
	
	/**
     * Method ID : list
     * Method 설명 : 오픈몰 마스터
     * 작성자 : sing09
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif001.list", model);
    }
    
    /**
	 * Method ID : saveMall
	 * Method 설명 : 오픈몰 마스터 저장
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void saveMall(Map<String, Object> model) {
		executeUpdate("wmsif001.saveMall", model);
		
	}
	/**
	 * Method ID : insertSap
	 * Method 설명 : 오픈몰 마스터 생성
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void insertMall(Map<String, Object> model) {
		executeInsert("wmsif001.insertMall", model);
		
	}
	/**
	 * Method ID : deleteSap
	 * Method 설명 : 오픈몰 마스터 삭제
	 * 작성자 : 이성중
	 * @param model
	 * @return
	 */
	public void deleteMall(Map<String, Object> model) {
		executeInsert("wmsif001.deleteMall", model);
		
	}
}
