package com.logisall.winus.wmsif.service.impl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsif.service.WMSIF610Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSIF610Service")
public class WMSIF610ServiceImpl implements WMSIF610Service{
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF610Dao")
    private WMSIF610Dao dao;
    
    /**
     * 
     * 대체 Method ID	: list
     * 대체 Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        if(model.get("page") == null) {
            model.put("pageIndex", "1");
        } else {
            model.put("pageIndex", model.get("page"));
        }
        if(model.get("rows") == null) {
            model.put("pageSize", "20");
        } else {
            model.put("pageSize", model.get("rows"));
        }
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE2
     * 대체 Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE2(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	if(model.get("page") == null) {
    		model.put("pageIndex", "1");
    	} else {
    		model.put("pageIndex", model.get("page"));
    	}
    	if(model.get("rows") == null) {
    		model.put("pageSize", "20");
    	} else {
    		model.put("pageSize", model.get("rows"));
    	}
    	map.put("LIST", dao.listE2(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID	: listE3
     * 대체 Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE3(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	if(model.get("page") == null) {
    		model.put("pageIndex", "1");
    	} else {
    		model.put("pageIndex", model.get("page"));
    	}
    	if(model.get("rows") == null) {
    		model.put("pageSize", "20");
    	} else {
    		model.put("pageSize", model.get("rows"));
    	}
    	map.put("LIST", dao.listE3(model));
    	return map;
    }
    /**
     * 
     * 대체 Method ID	: listE4
     * 대체 Method 설명	: 
     * 작성자			: sing09
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> listE4(Map<String, Object> model) throws Exception {
    	Map<String, Object> map = new HashMap<String, Object>();
    	map.put("LIST", dao.listE4(model));
    	return map;
    }
    
    /**
     * 
     * 대체 Method ID   : crossDomainHttps
     * 대체 Method 설명    :
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
	@Override
	public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        String sUrl = "";
        String cUrl = (String)model.get("URL");
        String gubun = (String)model.get("ordGubun");
        
        //실시간처리, 조건처리 구분자
        String gubunUrl = "";
        
        //이력테이블용 변수
        String gubunHis = "";
        String gubunHis1 = "";
        String gubunHis2 = (String)model.get("interfaceGb");
        
        if("R".equals(gubun)){
        	gubunHis = "실시간처리";
        	gubunHis1 = "REAL";
        }else{
        	gubunUrl = gubunUrl+"_PASS";
        	gubunHis = "조건처리";
        	gubunHis1 = "PASS";
        }
        
        //입고 예정 수신
        if("TASK_WINUS_ORDER_IN".equals(cUrl)){
        	sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_ORDER_IN"+gubunUrl; 
        //입고 실적 송신
        }else if("TASK_WINUS_ORDER_IN_RESULT".equals(cUrl)){
        	sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_ORDER_IN_RESULT"+gubunUrl; 
        //출고 예정 수신
        }else if("TASK_WINUS_ORDER_OUT".equals(cUrl)){
        	sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_ORDER_OUT"+gubunUrl; 
        //출고 실적 송신
        }else if("TASK_WINUS_ORDER_OUT_RESULT".equals(cUrl)){
        	sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_ORDER_OUT_RESULT"+gubunUrl; 
        //세트 작업 요청 수신
        }else if("TASK_WINUS_SET_WORK".equals(cUrl)){
        	sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_SET_WORK"+gubunUrl; 
    	//세트 작업 결과  송신
        }else if("TASK_WINUS_SET_WORK_RESULT".equals(cUrl)){
        	sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_SET_WORK_RESULT"+gubunUrl; 
    	//WMS O2O 창고이동 실적 송신
        }else if("TASK_WINUS_STOCK_MOVE_RESULT".equals(cUrl)){
        	sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_STOCK_MOVE_RESULT"+gubunUrl; 
		//전일마감 재고수불 실적 
		}else if("TASK_DAILY_STOCK_SAVE".equals(cUrl)){
			sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_DAILY_STOCK_SAVE"+gubunUrl; 
	    //B2C 반품입고 실적
		}else if("TASK_RTN_PARCEL_LOOP".equals(cUrl)){
			sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_RTN_PARCEL_LOOP"+gubunUrl; 
		//B2C 출고 실적 
		}else if("TASK_PARCEL_LOOP".equals(cUrl)){
			sUrl = "https://172.31.10.253:5201/KAKAOVX/WINUS_PARCEL_LOOP"+gubunUrl; 
		}
        
        try{
	        System.out.println("sUrl : " + sUrl);
	        
	        URL url = null; 
	        url = new URL(sUrl);
	        
	        //https ssl
	        disableSslVerification();
	        
	        HttpsURLConnection con = null;
        	con = (HttpsURLConnection) url.openConnection();
        	
        	//웹페이지 로그인 권한 적용
        	String userpass = "Administrator" + ":" + "ulndkagh2@";
        	String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes());

        	con.setRequestProperty ("Authorization", basicAuth);
        	con.setDoInput(true);
        	con.setDoOutput(true);  
        	con.setRequestMethod("POST");
        	con.setConnectTimeout(0);
        	con.setReadTimeout(0);
        	con.setRequestProperty("Content-Type"	, "application/json");
			con.setRequestProperty("Accept"			, "application/json");
			
			//Json Data
			String jsonInputString = "{\"ordStDate\"		:\""+(String) model.get("ordStDate").toString().replaceAll("-", "")+"\""
									+",\"ordEdDate\"		:\""+(String) model.get("ordEdDate").toString().replaceAll("-", "")+"\""
									+",\"ordSapDate\"		:\""+(String) model.get("ordSapDate").toString().replaceAll("-", "")+"\""
									+ "}";
			//Json Data His
			String jsonInputHis = "";
			if("Y".equals(gubunHis2)){
				jsonInputHis = "송수신일자 : "+(String) model.get("ordSapDate").toString();
			}else{
				jsonInputHis = "입출고일자 : "+(String) model.get("ordStDate").toString()+" ~ "+(String) model.get("ordEdDate").toString()+""
						+" ,  송수신일자 : "+(String) model.get("ordSapDate").toString();
			}
			//JSON 보내는 Output stream
			try(OutputStream os = con.getOutputStream()) {
	            byte[] input = jsonInputString.getBytes("utf-8");
	            os.write(input, 0, input.length);
	        }
			
			//Response data 받는 부분
	        try(BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"))) {
	            StringBuilder response = new StringBuilder();
	            String responseLine = null;
	            
	            while ((responseLine = br.readLine()) != null) {
	                response.append(responseLine.trim());
	            }
	            br.close();
	            
	            Map<String, Object> mapJson = null;
	            
	            JSONObject jsonObject = new JSONObject(response.toString());
	            mapJson = new ObjectMapper().readValue(jsonObject.toString(), Map.class) ;
	            
	            m.put("json", mapJson.get("OUTPUT"));
	            m.put("rst", MessageResolver.getMessage("save.success"));
	        }catch(Exception e){
	        	m.put("rst", MessageResolver.getMessage("save.error"));
	        }
	        
        	int resCode = 0;
        	resCode = con.getResponseCode();
        	System.out.println("resCode: " + resCode);
//        	System.out.println("con: " + con);
        	if(resCode < 400){
        		m.put("errCnt", 0);
                m.put("MSG", "처리되었습니다.");
        	}else{
        		//result = con.getResponseMessage();
        		m.put("errCnt", -1);
	            m.put("MSG", "I/F 처리오류. 코드:"+resCode+" (관리자문의)");
        	}
        	con.disconnect();
        	
        	Map<String, Object> modelHis = new HashMap<String, Object>();
        	modelHis.put("LC_ID"				, "0000003200");
        	modelHis.put("CUST_ID"			, "0000295694");
        	modelHis.put("INTERFACE_CODE"	, cUrl); 
			modelHis.put("INTERFACE_ETC1"	, gubunHis); 
			modelHis.put("gubunHis1"		, gubunHis1); 
			modelHis.put("INTERFACE_ETC2"	, jsonInputHis); 
//			modelHis.put("INTERFACE_ETC2"	, jsonInputHis.replaceAll("(\r\n|\r|\n|\n\r|\\p{Z}|\\t)", "")); 
			modelHis.put("INTERFACE_ETC3"	, resCode); 
			modelHis.put("USER_NO"			, model.get(ConstantIF.SS_USER_NO)); 
			//카카오 VX 인터페이스 수동처리 이력 생성
			dao.insert(modelHis);
			
        } catch(Exception e){
            throw e;
        }
        return m;
	}
	
	/**
     * 
     * 대체 Method ID   : disableSslVerification
     * 대체 Method 설명    :
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
	private static void disableSslVerification() {
		try{
			// Create a trust manager that does not validate certificate chains
			TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					
				}
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					
				}
			}};

			//Install the all-trusting trust manager
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, trustAllCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

			//Create all-trusting host name verifier
			HostnameVerifier allHostsValid = new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			};

			//Install the all-trusting host verifier
			HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (KeyManagementException e) {
			e.printStackTrace();
		}
	}
}
