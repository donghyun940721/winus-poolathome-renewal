package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsif.service.WMSIF402Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIF402Service")
public class WMSIF402ServiceImpl extends AbstractServiceImpl implements WMSIF402Service{

	@Resource(name = "WMSIF402Dao")
    private WMSIF402Dao dao;
	
	/**
     * Method ID : listE01
     * Method 설명 : KCC 인터페이스 입고 내역
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
	public Map<String, Object> listE01(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE01(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE02
     * Method 설명 : KCC 인터페이스 출고 내역
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
	public Map<String, Object> listE02(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE02(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE03
     * Method 설명 : KCC 인터페이스 입고 실적 내역
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
	public Map<String, Object> listE03(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE03(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE04
     * Method 설명 : KCC 인터페이스 출고 실적 내역
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
	public Map<String, Object> listE04(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE04(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE05
     * Method 설명 : KCC 인터페이스 입고 실적 내역
     * 작성자 : donghyun kim
     * @param model
     * @return
     */
	public Map<String, Object> listE05(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE05(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
	
	/**
     * Method ID : listE06
     * Method 설명 : KCC 인터페이스 출고 실적 내역
     * 작성자 : donghyun kim
     * @param model
     * @return
     */
	public Map<String, Object> listE06(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            
        	if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            
        	if (model.get("rows") == null) {
                model.put("pageSize", "60000");
            } else {
                model.put("pageSize", model.get("rows"));
            }        
        	
            map.put("LIST", dao.listE06(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
}
