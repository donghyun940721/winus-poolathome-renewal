package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF301Dao")
public class WMSIF301Dao extends SqlMapAbstractDAO{

    /**
     * Method ID  : list
     * Method 설명   : 
     * 작성자                : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsif301.list", model);
    }
}


