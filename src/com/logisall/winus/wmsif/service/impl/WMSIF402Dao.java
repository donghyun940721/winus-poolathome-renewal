package com.logisall.winus.wmsif.service.impl;

import java.util.Map;

import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSIF402Dao")
public class WMSIF402Dao extends SqlMapAbstractDAO{
	
	/**
     * Method ID : listE01
     * Method 설명 : KCC 인터페이스 입고 내역
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
    public GenericResultSet listE01(Map<String, Object> model) {
        return executeQueryPageWq("wmsif402.listE01", model);
    }   
    
    /**
     * Method ID : listE02
     * Method 설명 : KCC 인터페이스 출고 내역
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
    public GenericResultSet listE02(Map<String, Object> model) {
        return executeQueryPageWq("wmsif402.listE02", model);
    }   
    
    /**
     * Method ID : listE03
     * Method 설명 : KCC 인터페이스 입고 실적 내역
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
    public GenericResultSet listE03(Map<String, Object> model) {
        return executeQueryPageWq("wmsif402.listE03", model);
    }   
    
    /**
     * Method ID : listE04
     * Method 설명 : KCC 인터페이스 출고 실적 내역
     * 작성자 : Seongjun Kwon
     * @param model
     * @return
     */
    public GenericResultSet listE04(Map<String, Object> model) {
        return executeQueryPageWq("wmsif402.listE04", model);
    }   
    
    /**
     * Method ID : listE05
     * Method 설명 : KCC 인터페이스 출고 실적 내역
     * 작성자 : donghyun kim
     * @param model
     * @return
     */
    public GenericResultSet listE05(Map<String, Object> model) {
        return executeQueryPageWq("wmsif402.listE05", model);
    }   
    
    /**
     * Method ID : listE06
     * Method 설명 : KCC 인터페이스 출고 실적 내역
     * 작성자 : donghyun kim
     * @param model
     * @return
     */
    public GenericResultSet listE06(Map<String, Object> model) {
        return executeQueryPageWq("wmsif402.listE06", model);
    }   

}
