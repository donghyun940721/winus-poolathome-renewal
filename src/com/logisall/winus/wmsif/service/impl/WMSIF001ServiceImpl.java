package com.logisall.winus.wmsif.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.wmsif.service.WMSIF001Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSIF001Service")
public class WMSIF001ServiceImpl extends AbstractServiceImpl implements WMSIF001Service{

	@Resource(name = "WMSIF001Dao")
	private WMSIF001Dao dao;
	
	/**
     * Method ID : list
     * Method 설명 : 오픈몰 마스터 관리
     * 작성자 : sing09
     * @param model
     * @return
     */
	public Map<String, Object> list(Map<String, Object> model) throws Exception {
		Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
	}
	
	/**
     * Method ID : crudOpenMallMaster
     * Method 설명 : 오픈몰 마스터 CRUD
     * 작성자 : 이성중
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> crudOpenMallMaster(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
        	int errCnt = 0;
        	for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	             Map<String, Object> modelDt = new HashMap<String, Object>();
	             modelDt.put("PARAM_LC_ID"				,model.get("LC_ID_" + i ));
	             modelDt.put("PARAM_CUST_ID"			,model.get("CUST_ID_" + i ));
	             modelDt.put("PARAM_OPENMALL_CD"		,model.get("OPENMALL_CD_" + i ));
	             modelDt.put("PARAM_OPENMALL_CD_DESC"	,model.get("OPENMALL_CD_DESC_" + i ));
	             modelDt.put("PARAM_STRT_DT"			,model.get("STRT_DT_" + i ));
	             modelDt.put("PARAM_REP_DELIVERY_VENDOR",model.get("REP_DELIVERY_VENDOR_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_ID1"	,model.get("OPENMALL_API_ID1_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_PW1"	,model.get("OPENMALL_API_PW1_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_KEY1"	,model.get("OPENMALL_API_KEY1_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_ID2"	,model.get("OPENMALL_API_ID2_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_PW2"	,model.get("OPENMALL_API_PW2_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_KEY2"	,model.get("OPENMALL_API_KEY2_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_ID3"	,model.get("OPENMALL_API_ID3_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_PW3"	,model.get("OPENMALL_API_PW3_" + i ));
	             modelDt.put("PARAM_OPENMALL_API_KEY3"	,model.get("OPENMALL_API_KEY3_" + i ));
	             modelDt.put("PARAM_TOKEN1"				,model.get("TOKEN1_" + i ));
	             
	             modelDt.put("BF_OPENMALL_CD", 			model.get("BF_OPENMALL_CD_" + i));
	             modelDt.put("BF_OPENMALL_CD_DESC", 	model.get("BF_OPENMALL_CD_DESC_" + i));
	             modelDt.put("BF_OPENMALL_API_KEY1", 	model.get("BF_OPENMALL_API_KEY1_" + i));
	             
	             String stGubun = (String)model.get("ST_GUBUN_" + i);
	             if("UPDATE".equals(stGubun)){
	            	 dao.saveMall(modelDt);
	             }else if ("INSERT".equals(stGubun)){
	            	 dao.insertMall(modelDt);
	             }else if ("DELETE".equals(stGubun)){
	            	 dao.deleteMall(modelDt);
	             }
	    	}
        	
        	map.put("errCnt", errCnt);
        	map.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch (Exception e) {
        	throw e;

        }

        return map;
    }
}
