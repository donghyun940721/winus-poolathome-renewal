package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface OliveyoungDashService {
	public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttpWs2(Map<String, Object> model) throws Exception;
}
