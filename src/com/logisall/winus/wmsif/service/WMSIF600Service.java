package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF600Service {
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> saveListOrderJavaVX(Map<String, Object> model) throws Exception;	
	public Map<String, Object> sapbp(Map<String, Object> model) throws Exception;    
	public Map<String, Object> crudSap(Map<String, Object> model) throws Exception;    
	public Map<String, Object> sapListDelete(Map<String, Object> model) throws Exception;
	public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception;
}
