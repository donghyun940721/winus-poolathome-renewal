package com.logisall.winus.wmsif.service;

import java.util.Map;

public interface WMSIF301Service {
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttps(Map<String, Object> model) throws Exception;
    public Map<String, Object> crossDomainHttpWs(Map<String, Object> model) throws Exception;
}
