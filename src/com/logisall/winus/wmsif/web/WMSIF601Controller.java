package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsif.service.WMSIF601Service;
import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF601Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF601Service")
	private WMSIF601Service service;
	
	@Resource(name = "WMSOP030Service")
	private WMSOP030Service WMSOP030service;
	/*-
	 * Method ID    : wmsif601
	 * Method 설명      : 오픈몰 주문관리 (사방넷)
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSIF601.action")
	public ModelAndView wmsif601(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF601");
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 오픈몰 주문관리 내역조회 (사방넷)
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	
	@RequestMapping("/WMSIF601/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/**
	 * Method ID : listE2
	 * Method 설명 : 송장송신 (사방넷)
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF601/listE2.action")
	public ModelAndView listE2(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE2(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}

    /**
     * Method ID	: listE3
     * Method 설명	: 사방넷 수집이력
     * 작성자			: sing09
     * @param   model
     * @throws Exception 
     */
    @RequestMapping("/WMSIF601/listE3.action")
    public ModelAndView list3(Map<String, Object> model) throws Exception {
    	ModelAndView mav = null;
    	
    	try {
    		mav = new ModelAndView("jqGridJsonView", service.listE3(model));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	return mav;
    }
    
	/**
	 * Method ID     : saveListOrderJavaVX
	 * Method 설명      	 : 오픈몰 주문 등록
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF601/saveListOrderJavaVX.action")
	public ModelAndView saveListOrderJavaVX(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveListOrderJavaVX(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID    : wmsif601Q1
	 * Method 설명      : SAPBP 마스터 팝업
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSIF601Q1.action")
	public ModelAndView wmsif601Q1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF601Q1");
	}
	
	/*-
	 * Method ID    : wmsif601Q1List
	 * Method 설명      : SAPBP 마스터 조회
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF601Q1/sapbpList.action")
	public ModelAndView wmsif601Q1List(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.sapbp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : crudSap
	 * Method 설명 : sap 마스터 CRUD
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF601Q1/crudSap.action")
	public ModelAndView crudSap(Map<String, Object> model) throws Exception {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.crudSap(model);
		
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : sapListDelete
	 * Method 설명 : sap 주문 리스트 삭제
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF601/sapListDelete.action")
	public ModelAndView sapListDelete(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.sapListDelete(model);
			mav.addAllObjects(m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID	: crossDomainHttpWs
	 * Method 설명	: 
	 * 작성자			: 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF601/crossDomainHttpWs.action")
	public ModelAndView crossDomainHttpWs(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.crossDomainHttpWs(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: crossDomainHttpWs_V2
	 * Method 설명	: 사방넷 주문수집 매입처 파라미터 추가 
	 * 작성자			: sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF601/crossDomainHttpWs_V2.action")
	public ModelAndView crossDomainHttpWs_V2(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.crossDomainHttpWs_V2(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: crossDomainHttpWs_PARCEL
	 * Method 설명	: 사방넷 송장등록 
	 * 작성자			: sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF601/crossDomainHttpWs_PARCEL.action")
	public ModelAndView crossDomainHttpWs_PARCEL(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.crossDomainHttpWs_PARCEL(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID	: getOpenMallApiAuthMaster
	 * Method 설명	: 오픈몰 API AUTH 마스터 리스트
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSIF601/getOpenMallApiAuthMaster.action")
	public ModelAndView getOpenMallApiAuthMaster(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getOpenMallApiAuthMaster(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : sabangnetResetInvoice
	 * Method 설명 : 사방넷 송장송신여부 Flag 초기화
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF601/sabangnetResetInvoice.action")
	public ModelAndView sabangnetResetInvoice(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.sabangnetResetInvoice(model);
			mav.addAllObjects(m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	

	/*-
	 * Method ID : sabangnetListDelete
	 * Method 설명 : 사방넷 주문삭제 
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF601/sabangnetListDelete.action")
	public ModelAndView sabangnetListDelete(Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.sabangnetListDelete(model);
			mav.addAllObjects(m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
}
