package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.logisall.ws.interfaces.wmsif.service.WMSIF000Service;
//import com.logisall.winus.wmsif.service.kccDashService;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

import org.springframework.web.util.HtmlUtils;


@Controller
public class kccDashController {
    protected Log log = LogFactory.getLog(this.getClass());

//    @Resource(name = "kccDashService")
//    private kccDashService service;
    
    @Resource(name = "WMSIF000Service")
	private WMSIF000Service wmsif000service;
    
    /**
     * Method ID	: wmsif401
     * Method 설명	: 
     * 작성자			: kcr
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF401.action")
    public ModelAndView oliveYoungDash(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/dashboard/kcc");
    }
//    
//    /**
//     * Method ID	: wmsif203
//     * Method 설명	: 
//     * 작성자			: chSong
//     * @param   model
//     * @return  
//     * @throws Exception 
//     */
    @RequestMapping("/KCC_TV.action")
    public ModelAndView oliveYoungDashTv(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/dashboard/kcc-tv");
    }
//    
//    /*-
//	 * Method ID    : crossDomainHttpWs
//	 * Method 설명      : 
//	 * 작성자                 : 
//	 * @param   model
//	 * @return  
//	 */
	@RequestMapping(value = "/KCCDASH/crossDomainHttpWs.action", method = RequestMethod.POST)
	public ModelAndView crossDomainHttpWs(@RequestBody JSONObject obj , HttpServletRequest request, HttpServletResponse response){
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> model = new HashMap<String, Object>();
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			String cMethod		= "POST";
			String cUrl ="DASHBOARD/TODAY_JOB_STATUS_KCC";
			String hostUrl = request.getServerName();
			
			String dataJson = obj.toJSONString(); 
			//String dataJson = request.getParameter("jsonData");
			//dataJson =  HtmlUtils.htmlUnescape(dataJson);
			if(dataJson != null && dataJson !=""){
				model.put("data"	    , dataJson);
				model.put("cMethod"		, cMethod);
				model.put("cUrl"		, cUrl);
				model.put("hostUrl"		, hostUrl);
				
				m = wmsif000service.crossDomainHttpWs5200(model);
			}
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
		}
		mav.addAllObjects(m);
		return mav;
	}
    
}
