package com.logisall.winus.wmsif.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.wmsop.service.WMSOP030Service;
import com.logisall.winus.wmsif.service.WMSIF600Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

@Controller
public class WMSIF600Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF600Service")
	private WMSIF600Service service;
	
	@Resource(name = "WMSOP030Service")
	private WMSOP030Service WMSOP030service;
	/*-
	 * Method ID    : wmsif600
	 * Method 설명      : 오픈몰 주문관리 (사방넷)
	 * 작성자                 : schan
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSIF600.action")
	public ModelAndView wmsif600(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF600");
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 오픈몰 주문관리 내역조회 (사방넷)
	 * 작성자 : schan
	 * @param model
	 * @return
	 */
	
	@RequestMapping("/WMSIF600/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID     : saveListOrderJavaVX
	 * Method 설명      	 : 오픈몰 주문 등록
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF600/saveListOrderJavaVX.action")
	public ModelAndView saveListOrderJavaVX(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();

		try {
			m = service.saveListOrderJavaVX(model);
			mav.addAllObjects(m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get picking info :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID    : wmsif600Q1
	 * Method 설명      : SAPBP 마스터 팝업
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSIF600Q1.action")
	public ModelAndView wmsif600Q1(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF600Q1");
	}
	
	/*-
	 * Method ID    : wmsif600Q1List
	 * Method 설명      : SAPBP 마스터 조회
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF600Q1/sapbpList.action")
	public ModelAndView wmsif600Q1List(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.sapbp(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : crudSap
	 * Method 설명 : sap 마스터 CRUD
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF600Q1/crudSap.action")
	public ModelAndView crudSap(Map<String, Object> model) throws Exception {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.crudSap(model);
		
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID : sapListDelete
	 * Method 설명 : sap 주문 리스트 삭제
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF600/sapListDelete.action")
	public ModelAndView sapListDelete(Map<String, Object> model) throws Exception {
		
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.sapListDelete(model);
			mav.addAllObjects(m);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID	: crossDomainHttpWs
	 * Method 설명	: 
	 * 작성자			: 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF600/crossDomainHttpWs.action")
	public ModelAndView crossDomainHttpWs(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.crossDomainHttpWs(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
