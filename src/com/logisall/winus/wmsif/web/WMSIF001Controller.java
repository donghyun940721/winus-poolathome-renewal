package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsif.service.WMSIF001Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF001Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF001Service")
	private WMSIF001Service service;
	
	/*-
	 * Method ID    : wmsif001
	 * Method 설명      : 오픈몰 마스터 관리
	 * 작성자                 : sing09
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSIF001.action")
	public ModelAndView wmsif001(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF001");
	}
	
	/*-
	 * Method ID : list
	 * Method 설명 : 오픈몰 마스터 관리
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	
	@RequestMapping("/WMSIF001/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : crudOpenMallMaster
	 * Method 설명 : 오픈몰 마스터 CRUD
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF001/crudOpenMallMaster.action")
	public ModelAndView crudOpenMallMaster(Map<String, Object> model) throws Exception {

		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.crudOpenMallMaster(model);
		
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
