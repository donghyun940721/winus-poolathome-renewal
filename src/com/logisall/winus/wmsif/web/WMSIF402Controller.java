package com.logisall.winus.wmsif.web;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsif.service.WMSIF402Service;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF402Controller {
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WMSIF402Service")
	private WMSIF402Service service;
	
	/*-
	 * Method ID    : wmsif402
	 * Method 설명      : KCC 인터페이스 입출고 내역 조회
	 * 작성자                 : seongjun kwon
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WINUS/WMSIF402.action")
	public ModelAndView wmsif402(Map<String, Object> model) throws Exception {
		return new ModelAndView("winus/wmsif/WMSIF402");
		/*
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/wmsif/WMSIF402", model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get info :", e);
			}
		}
		return mav;
		*/
	}
	
	/*-
	 * Method ID : listE01
	 * Method 설명 : KCC 인터페이스 입고 내역 조회
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF402/listE01.action")
	public ModelAndView listE01(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE01(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listE02
	 * Method 설명 : KCC 인터페이스 출고 내역 조회
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF402/listE02.action")
	public ModelAndView listE02(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE02(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listE03
	 * Method 설명 : KCC 인터페이스 입고 실적 내역 조회
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF402/listE03.action")
	public ModelAndView listE03(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE03(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listE04
	 * Method 설명 : KCC 인터페이스 출고 실적 내역 조회
	 * 작성자 : seonjun kwon
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF402/listE04.action")
	public ModelAndView listE04(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE04(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listE05
	 * Method 설명 : KCC 인터페이스 입고 실적 내역 조회
	 * 작성자 : donghyun kim
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF402/listE05.action")
	public ModelAndView listE05(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE05(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
	
	/*-
	 * Method ID : listE06
	 * Method 설명 : KCC 인터페이스 출고 실적 내역 조회
	 * 작성자 : donghyun kim
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSIF402/listE06.action")
	public ModelAndView listE06(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listE06(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get list :", e);
			}
		}
		return mav;
	}
}
