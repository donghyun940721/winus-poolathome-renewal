package com.logisall.winus.wmsif.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsif.service.WMSIF204Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSIF204Controller {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSIF204Service")
    private WMSIF204Service service;
    
    /**
     * Method ID	: wmsif203
     * Method 설명	: 
     * 작성자			: chSong
     * @param   model
     * @return  
     * @throws Exception 
     */
    @RequestMapping("/WINUS/WMSIF204.action")
    public ModelAndView wmsif204(Map<String, Object> model) throws Exception {
    	return new ModelAndView("winus/wmsif/WMSIF204");
    }
    
    /*-
	 * Method ID    : crossDomainHttp
	 * Method 설명      : 
	 * 작성자                 : 
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSIF204/turnOnOff.action")
	public ModelAndView crossDomainHttp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		try {
			m = service.turnOnOff(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID   : getStatus
	 * Method 설명 :
	 * 작성자      : 
	 * @param 
	 * @return
	 */
	@RequestMapping("/WMSIF204/getStatus.action")
	public ModelAndView getStatus(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jsonView", service.getStatus(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get Manage Code :", e);
			}
		}
		return mav;
	}
}
