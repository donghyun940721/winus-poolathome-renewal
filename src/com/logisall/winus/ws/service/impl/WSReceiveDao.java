package com.logisall.winus.ws.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WSReceiveDao")
public class WSReceiveDao extends SqlMapAbstractDAO {
	
	protected Log log = LogFactory.getLog(this.getClass());
    
	   /**
     * Method ID : list
     * Method 설명 : 송신문서 내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsys300.list", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 송수신문서 insert
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsys300.insert", model);
    }
    
    /**
     * 
     * Method ID   : update_stat_cd
     * Method 설명 : 처리상태를 업데이트
     * 작성자      : 기드온
     * @param model
     * @return
     */
    public int updateTrStat(Map<String, Object> model) {
        Object obj = executeUpdate("wmsys300.tr_stat.update", model);
        int rtn = 1;
        if(obj !=null){
               rtn = Integer.parseInt(obj.toString());
        }
        return rtn;
    } 
    
    public List<Map<String, Object>> listTestList(Map<String, Object> model) {
        return executeQueryForList("wmsms020.search.e1", model);
    }      
}
