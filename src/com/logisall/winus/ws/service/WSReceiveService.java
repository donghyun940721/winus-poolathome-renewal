package com.logisall.winus.ws.service;

import java.util.Map;

public interface WSReceiveService {

    public Map<String, Object> insertRetry(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveJSONStringToFile(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateState(Map<String, Object> model) throws Exception;
    
    public Map<String, Object> listTestList(Map<String, Object> model) throws Exception;
}
