package com.logisall.winus.ws.web;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.ws.service.WSReceiveService;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.FileUtil;
import com.m2m.jdfw5x.util.file.FileHelper;

import net.sf.sojo.interchange.Serializer;
import net.sf.sojo.interchange.json.JsonSerializer;

@Controller
public class WSReceiveController {
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	@Resource(name = "WSReceiveService")
	private WSReceiveService service;
	
	/**
	 * Method ID : testJson Method 설명 : JSON데이터 수신 작성자 : KWT
	 * 
	 * @param inputJSON
	 * 
	 */
	@RequestMapping("/WSTEST/sendTestJson.action")
	public @ResponseBody ModelAndView sendTestJson(Map<String, Object> model) throws Exception {
		if (log.isInfoEnabled()) {
			log.info("[ SEND TEST ] : ######################################### MODEL :" + model);
		}
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
        try{
            m = service.listTestList(model);
            
        }catch(Exception e){
            e.printStackTrace();
            m = new HashMap<String, Object>();
            m.put("MSG", MessageResolver.getMessage("save.error"));
        }	

		mav.addAllObjects(m);
		if (log.isInfoEnabled()) {
			log.info("[ SEND TEST ] : ######################## RETURN :" + mav);
		}
		
		String pathDirectory = DateUtil.getDateByPattern2();
		String path = (new StringBuffer().append(ConstantWSIF.TEMP_PATH).append(pathDirectory))
				.toString();
		String fileName = (new StringBuffer()
				.append("json_")
				.append(UUID.randomUUID().toString())
				.append(".json")).toString();
		
		// Serializer serializer = new JsonSerializer();
		String mapAsJson = new ObjectMapper().writeValueAsString(mav);
		saveJSONStringToFile(mapAsJson, path, fileName);
		
		return mav;		
	}	
	
	private void saveJSONStringToFile(String jsonString, String path, String fileName) {
		
		FileOutputStream fileOutputStream = null;
		OutputStreamWriter outputStreamWriter = null;
		BufferedWriter bufferedWriter = null;

		try {
			if (!CommonUtil.isNull(path)) {
				if (!FileUtil.existDirectory(path)) {
					FileHelper.createDirectorys(path);
				}
			}
			File jsonFile = new File(path, fileName);

			fileOutputStream = new FileOutputStream(jsonFile);
			// outputStreamWriter = new OutputStreamWriter(fileOutputStream, "MS949");
			outputStreamWriter = new OutputStreamWriter(fileOutputStream, ConstantIF.ENCODING_TYPE_JSON_FILE);
			
			bufferedWriter = new BufferedWriter(outputStreamWriter);

			bufferedWriter.write(jsonString);
			bufferedWriter.flush();
			
			bufferedWriter.close();			
			fileOutputStream.close();
			fileOutputStream.close();
			
		} catch (Exception e) {
			if (log.isInfoEnabled()) {
				log.info("Exception ######################################### :" + e.getMessage());
			}
			e.printStackTrace();
			
		} finally {
			// 자원 해제
			if (bufferedWriter != null) {
				try {
					bufferedWriter.close();
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}

			if (outputStreamWriter != null) {
				try {
					outputStreamWriter.close();
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}

			if (fileOutputStream != null) {
				try {
					fileOutputStream.close();
				} catch (Exception e) {
					// e.printStackTrace();
				}
			}

		}			
		
	}
	
}
