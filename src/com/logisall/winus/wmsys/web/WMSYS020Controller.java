package com.logisall.winus.wmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ExcelWriter;
import com.logisall.winus.wmsys.service.WMSYS020Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSYS020Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSYS020Service")
	private WMSYS020Service service;

	/*-
	 * Method ID   : tmsys020
	 * Method 설명 : 권한관리 화면
	 * 작성자      : chsong 04.13.02.31
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS020.action")
	public ModelAndView wmsys020(Map<String, Object> model, HttpServletRequest request) {
		return new ModelAndView("winus/wmsys/WMSYS020");
	}

	/*-
	 * Method ID   : listAuth
	 * Method 설명 : 권한목록 조회
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/listProperties.action")
	public ModelAndView listProperties(Map<String, Object> model, HttpServletRequest request) {

		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.listProperties(model, request));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID   : save
	 * Method 설명 : 프로그램목록 등록,수정,삭제
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/saveProperties.action")
	public ModelAndView saveProperties(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.saveProperties(model, request);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));

		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID   : save
	 * Method 설명 : 프로그램목록 등록,수정,삭제
	 * 작성자      : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/checkProperties.action")
	public ModelAndView checkProperties(Map<String, Object> model, HttpServletRequest request) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.checkProperties(model, request);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to check...", e);
			}

		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : listExcel
	 * Method 설명  : 코드상세목록 엑셀다운로드
	 * 작성자       : chsong
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS020/excel.action")
	public void listExcel(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			model.put("rows", 60000);
			map = service.listProperties(model, request);
			GenericResultSet grs = (GenericResultSet) map.get("LIST");

			if (grs.getTotCnt() > 0) {
				this.doExcelDown(response, grs);
			}
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to download excel...", e);
			}
		}
	}
    
    /*-
     * Method ID : doExcelDown
     * Method 설명 : 
     * 작성자 : kwt
     *
     * @param response
     * @param grs
     */
    protected void doExcelDown(HttpServletResponse response, GenericResultSet grs) {
        try{
            //헤더와 쿼리컬럼명의 갯수와 순서는 DB호출 데이터와 동일해야한다.
            //헤더 명 , 시작 col , 끝 col , 시작 row, 끝 row, 넓이
            String[][] headerEx = {
                    { MessageResolver.getText("키"), "0", "0", "0", "0", "400"},
                    { MessageResolver.getText("한국어"), "1", "1", "0", "0", "400"},
                    { MessageResolver.getText("일본어"), "2", "2", "0", "0", "400"},
                    { MessageResolver.getText("중국어"), "3", "3", "0", "0", "400"},
                    { MessageResolver.getText("영어"), "4", "4", "0", "0", "400"},
                    { MessageResolver.getText("베트남"), "5", "5", "0", "0", "400"}
            };
            
            //{쿼리컬럼명 , 데이터타입} * S = 문자형 / N = 정수, 음수, 소숫점은 3자리서 반올림하여 2자리까지 표시.. / NR = 정수, 음수, 소숫점은 반올림하여 정수로..
            String[][] valueName = {
                    {"PROP_KEY"     , "S"},
                    {"PROP_VAL_KR"  , "S"},
                    {"PROP_VAL_JA"  , "S"},
                    {"PROP_VAL_CZ"  , "S"},
                    {"PROP_VAL_US"  , "S"},
                    {"PROP_VAL_VN"  , "S"}
            }; 
            
            //파일명 기초코드 목록
            String fileName = MessageResolver.getText("다국어관리목록");
            //시트명
            String sheetName = "Sheet1";
            //구분 병합 여부 (0 : 구분0번째  / 1 : 구분 1번째  / A : 구분 0, 1 번째  / N : 병합 없음 )
            
            String marCk = "N";
            //ComUtil코드
            String etc = "";
            
            ExcelWriter wr = new ExcelWriter();
            wr.downExcelFile(ConstantIF.FILE_ATTACH_PATH, grs, headerEx, valueName, fileName, sheetName, marCk, etc, response);
            
        } catch(Exception e) {
        	if (log.isErrorEnabled()) {
               	log.error("fail to download excel file...", e);
            }
        }
    }      

}
