package com.logisall.winus.wmsys.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.wmsms.service.WMSMS030Service;
import com.logisall.winus.wmsys.service.WMSYS040Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class WMSYS040Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "WMSYS040Service")
	private WMSYS040Service service;
	
	@Resource(name = "WMSMS030Service")
	private WMSMS030Service wmsmsService;
	
	/*-
	 * Method ID   : mn
	 * Method 설명 : 입출고주문 그리드관리 메인
	 * 작성자      : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS040.action")
	public ModelAndView mn(Map<String, Object> model, HttpServletRequest request) {
		return new ModelAndView("winus/wmsys/WMSYS040");
	}

	/*-
	 * Method ID    : list
	 * Method 설명      : 입출고주문 그리드관리 조회
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 * @throws Exception 
	 */
	@RequestMapping("/WMSYS040/list.action")
	public ModelAndView list(Map<String, Object> model) throws Exception {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID    : save
	 * Method 설명      : 입출고주문 그리드관리 저장
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS040/save.action")
	public ModelAndView save(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.save(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to save...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID    : delete
	 * Method 설명      : 입출고주문 그리드관리 삭제
	 * 작성자                 : chSong
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS040/delete.action")
	public ModelAndView delete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to delete...", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID   : setGridDefault
	 * Method 설명 : 신규 그리드관리 메인
	 * 작성자      : 기드온
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS240.action")
	public ModelAndView setGridDefault(Map<String, Object> model, HttpServletRequest request) throws Exception {
		return new ModelAndView("winus/wmsys/WMSYS240", wmsmsService.selectLc(model));
	}
}
