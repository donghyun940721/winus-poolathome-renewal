package com.logisall.winus.wmsys.web;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsys.service.WMSYS300Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.excel.ExcelReader;
import com.m2m.jdfw5x.util.file.FileHelper;

/**
 * @author iskim
 *
 */
@Controller
public class WMSYS300Controller {
	protected Log log = LogFactory.getLog(this.getClass());

	static final String[] COLUMN_NAME_WMSYS300E7 = {
		"CUST_CD", "DLV_ORD_ID", "SERIAL_IMG_PATH", "DLV_IMG_PATH"
	};
	static final String[] COLUMN_NAME_WMSYS300E7_2 = {
		"CUST_CD", "ORG_ORD_ID", "CS_REQ_DT", "SALES_CUST_NM", "TEXT_REQ_CONTENTS", "TEXT_RESULT"
	};
	
	@Resource(name = "WMSYS300Service")
	private WMSYS300Service service;

	/*-
	 * Method ID : mn 
	 * Method 설명 : 인터페이스관리 화면 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WINUS/WMSYS300.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("winus/wmsys/WMSYS300");
	}

	/*-
	 * Method ID : list 
	 * Method 설명 : 송신문서 내역 조회 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300/list.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to get list...", e);
			}
		}
		return mav;
	}

	/*-
	 * Method ID : rinsert 
	 * Method 설명 : 송수신문서 재처리 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300/rinsert.action")
	public ModelAndView rinsert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			m = service.insertRetry(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to insert...", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("errCnt", "1");
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * Method ID : popOrgnXmlUp 
	 * Method 설명 : xml 원문 보기 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/PopOrgnXmlUp.action")
	public ModelAndView popOrgnXmlUp(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("winus/wmscm/WMSCM999Q1");
		Map<String, Object> m = null;
		try {
			m = service.detail(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("fail to pop...", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("list.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	

	/*-
	 * Method ID : pop
	 * Method 설명 : 
	 * 작성자 : kwt
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300E4.action")
	public ModelAndView pop(Map<String, Object> model) {
		return new ModelAndView("winus/wmsys/WMSYS300E4");
	}
	
	/*-
	 * Method ID    : save
	 * Method 설명      : 물류용기군관리 저장
	 * 작성자                 : 기드온
	 * @param   model
	 * @return  
	 */
	@RequestMapping("/WMSYS300/runProc.action")
	public ModelAndView runProc(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		try {
			m = service.runProc(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	/*-
	 * Method ID  : excelUploadRstApiImgGet
	 * Method 설명  : Excel 파일 읽기
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300/excelUploadRstApiImgGet.action")
	public ModelAndView excelUploadRstApiImgGet(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSYS300E7, 0, startRow, 10000, 0);
			m = service.excelUploadRstApiImgGet(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
	
	/*-
	 * Method ID  : excelUploadCsOrdUpload
	 * Method 설명  : 
	 * 작성자             : kwt
	 * @param request
	 * @param response
	 * @param model
	 * @return
	 */
	@RequestMapping("/WMSYS300/excelUploadCsOrdUpload.action")
	public ModelAndView excelUploadCsOrdUpload(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model, @RequestParam("txtFile") MultipartFile txtFile) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = null;
		try {
			request.setCharacterEncoding("utf-8");
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
			MultipartFile file = multipartRequest.getFile("txtFile");
			String fileName = file.getOriginalFilename();
			String filePaths = ConstantIF.TEMP_PATH;

			if (!FileHelper.existDirectory(filePaths)) {
				FileHelper.createDirectorys(filePaths);
			}
			File destinationDir = new File(filePaths);
			File destination = File.createTempFile("excelTemp", fileName, destinationDir);
			FileCopyUtils.copy(file.getInputStream(), new FileOutputStream(destination));

			// FSUtil.fileDecrypt(destination.getAbsolutePath(), destination.getAbsolutePath());

			int startRow = Integer.parseInt((String) model.get("startRow"));
			List<Map> list = ExcelReader.excelLimitRowRead(destination, COLUMN_NAME_WMSYS300E7_2, 0, startRow, 10000, 0);
			m = service.excelUploadCsOrdUpload(model, list);

			destination.deleteOnExit();
			mav.addAllObjects(m);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to upload Excel info :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("save.error"));
			m.put("MSG_ORA", e.getMessage());
			m.put("errCnt", "1");
			mav.addAllObjects(m);
		}
		return mav;
	}
}
