package com.logisall.winus.wmsys.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.SqlMapDao;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.DateUtil;
import com.logisall.winus.wmsys.service.WMSYS020Service;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.exception.BizException;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSYS020Service")
public class WMSYS020ServiceImpl extends AbstractServiceImpl implements WMSYS020Service {
    
    protected Log loger = LogFactory.getLog(this.getClass());
    
    private final static String WEB_APP_PATH_MESSAGE = "/WEB-INF/classes/resource/message";
    
    private final static String EXTENTENTION_FOR_TEMP_FILE = "_temp";

    /**
     * 
     * 대체 Method ID   : listAuth
     * 대체 Method 설명 : 권한코드 목록 조회
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> listProperties(Map<String, Object> model, HttpServletRequest request) throws Exception {
        
        final File propertiesDir = new File(request.getSession().getServletContext().getRealPath(WEB_APP_PATH_MESSAGE));
        
        if (!propertiesDir.exists() || !propertiesDir.isDirectory()) {
            loger.info("Properties Invalid...");
        }
        BufferedReader      bufferedReader      = null;
        
        try {
            List propertiesList = loadPropertiesFiles(propertiesDir, 0);
            
            List rtnList = new ArrayList();
            Map<String, Object> recordMap = null;
            
            boolean isSearchKey = false;
            String serchKey = null;
            
            boolean isSearchValue = false;
            String searchValue = null;
            
            if ( !StringUtils.isEmpty((String)model.get("SEARCH_KEY"))) {
                isSearchKey = true;
                serchKey = (String)model.get("SEARCH_KEY");
            }
            
            
            if ( !StringUtils.isEmpty((String)model.get("SEARCH_VALUE"))) {
                isSearchValue = true;
                searchValue = (String)model.get("SEARCH_VALUE");
            }
            
            String[] propertiesArray = null;
            for (int i = 0; i < propertiesList.size(); i++) {
                propertiesArray = (String[])propertiesList.get(i);
                boolean isTarget = false;

                if (isSearchKey && isSearchValue) {
                    if ( !StringUtils.isEmpty(propertiesArray[0]) && propertiesArray[0].contains(serchKey) ) {
                        if ( (!StringUtils.isEmpty(propertiesArray[1]) && ( propertiesArray[1].contains(searchValue) )) 
                                || (!StringUtils.isEmpty(propertiesArray[2]) && ( propertiesArray[2].contains(searchValue) )) 
                                || (!StringUtils.isEmpty(propertiesArray[3]) && ( propertiesArray[3].contains(searchValue) )) 
                                || (!StringUtils.isEmpty(propertiesArray[4]) && ( propertiesArray[4].contains(searchValue) )) 
                                || (!StringUtils.isEmpty(propertiesArray[5]) && ( propertiesArray[5].contains(searchValue) )) 
                        )  {
                            isTarget = true;
                        }
                    }
                } else if (isSearchKey) {
                    if ( !StringUtils.isEmpty(propertiesArray[0]) && propertiesArray[0].contains(serchKey) ) {
                        isTarget = true;
                    }
                    
                } else if(isSearchValue) {
                    if ( (!StringUtils.isEmpty(propertiesArray[1]) && ( propertiesArray[1].contains(searchValue))) 
                            || (!StringUtils.isEmpty(propertiesArray[2]) && ( propertiesArray[2].contains(searchValue))) 
                            || (!StringUtils.isEmpty(propertiesArray[3]) && ( propertiesArray[3].contains(searchValue))) 
                            || (!StringUtils.isEmpty(propertiesArray[4]) && ( propertiesArray[4].contains(searchValue)))
                            || (!StringUtils.isEmpty(propertiesArray[5]) && ( propertiesArray[5].contains(searchValue)))
                    )  {
                        isTarget = true;
                    }
                    
                } else {
                    if ( !StringUtils.isEmpty(propertiesArray[0]) ) {
                        isTarget = true;
                    }
                }
                
                if (isTarget) {
                    recordMap = new HashMap<String, Object>();
                    recordMap.put("CHK_BOX", "0");
                    recordMap.put("FLAG", "S");                    
                    recordMap.put("PROP_KEY", propertiesArray[0]);
                    recordMap.put("PROP_VAL_KR", propertiesArray[1]);
                    recordMap.put("PROP_VAL_JA", propertiesArray[2]);
                    recordMap.put("PROP_VAL_CZ", propertiesArray[3]);
                    recordMap.put("PROP_VAL_US", propertiesArray[4]);
                    recordMap.put("PROP_VAL_VN", propertiesArray[5]);
                    rtnList.add(recordMap);
                }
            }
            
            Collections.sort(rtnList, new Comparator<Map<String, Object>>() {
                    public int compare( Map<String, Object> map1, Map<String, Object> map2) {
                        return ((String)map1.get("PROP_KEY")).compareTo((String)map2.get("PROP_KEY"));
                    }
                }
            );
            
            Map<String, Object> map = new HashMap<String, Object>();
            if(model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if(model.get("rows") == null) {
                model.put("pageSize", "20");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            GenericResultSet grs = SqlMapDao.getGenericResultSetFromList( model, rtnList ); 
            
            map.put("LIST", grs );
            return map;
        
        } catch (Exception e) {
            if (loger.isInfoEnabled()) {
                loger.info(e.getMessage());
            }
            throw e;
    
        } finally {
			if (bufferedReader != null) {
				try {
					bufferedReader.close();
				} catch (Exception ie) {
					// e.printStackTrace();
				}
			}
        }
        
    }

    private List loadPropertiesFiles(final File propertiesDir, int type) {
        
        File[] files = propertiesDir.listFiles();
        List<String[]> propertiesList = new ArrayList<String[]>();
        List<String> keyList = new ArrayList<String>();

        String filename = null;
        String propFileName = null;
        int fileType = -1;
        int fileLength = (files != null) ? files.length : 0;
        for (int i = 0; i < fileLength; i++) {
            filename         = files[i].getName();
            if ( filename != null ) {
                if ( filename.endsWith("_ja.properties") ) {
                    fileType = 2;
                } else if ( filename.endsWith("_zh.properties") ) {
                    fileType = 3;
                } else if ( filename.endsWith("_en.properties") ) {
                    fileType = 4;
                }else if ( filename.endsWith("_it.properties") ) {
                	//vietnam (it : Italian 인용, 베트남 지원x)
                    fileType = 5;
                } else {
                    fileType = 1;
                }
            }
            
            if ( fileType > -1 ) { 
                propFileName = propertiesDir.getPath() + File.separator + filename;
                /*
                if (loger.isDebugEnabled()) {
                	loger.debug("[ propFileName ] :" + propFileName);
                }
                */
                
                Properties props = loadFileProperties(propFileName);
                if ( props != null && props.keySet() != null) {
                    
                    Iterator<Object> it = props.keySet().iterator();
                    while(it.hasNext()) {
                        
                        String key = (String) it.next();
                        String [] propertiesArray = null;
                        
                        for(int j=0; j<keyList.size(); j++) {
                            if ( key.equals( (keyList.get(j)).toString() ) ) {
                                propertiesArray = (String[]) propertiesList.get(j);
                                break;
                            }
                        }
                        if ( propertiesArray == null) {
                            propertiesArray = new String[]{"", "", "" , "", "", ""};
                            propertiesArray[0] = key;
                            propertiesArray[fileType] = (props.getProperty(key) != null )? props.getProperty(key).toString():"";
                            
                            propertiesList.add(propertiesArray);
                            keyList.add(key);
                        } else {
                            propertiesArray[fileType] = (props.getProperty(key) != null )? props.getProperty(key).toString():"";
                        }
                    }
                } else {
                    loger.info("[ Properties is NULL ] :");
                }
            }
            
        }// 파일 리스트 반복
        if ( type == 1 ) {
            return keyList;
        } else {
            return propertiesList;
        }
    }

    private Properties loadFileProperties(String propFileName) {       
        Properties props = null;
        InputStreamReader propIS = null; 
        try {
            props = new Properties();
            propIS = new InputStreamReader(new FileInputStream(propFileName), ConstantIF.PROPERTY_FILE_ENCODING); 
            props.load(propIS);
            propIS.close();

        } catch (Exception e) {
               e.printStackTrace();               
        } finally{
			if (propIS != null) {
				try {
					propIS.close();
				} catch (Exception e) {
				}
			}
        }
        return props;
    }

    @Override
    public Map<String, Object> saveProperties(Map<String, Object> model, HttpServletRequest request) throws Exception {
        
        Map<String, Object> m = new HashMap<String, Object>();
        File propertiesDir = new File(request.getSession().getServletContext().getRealPath(WEB_APP_PATH_MESSAGE));
        
        File[] files = propertiesDir.listFiles();
        int lisSize = (propertiesDir == null) ? 0 :  files.length;
        String filename = null;
        int errCnt = 0;
        
        Properties popsKr = null;
        Properties popsJa = null;
        Properties popsCz = null;
        Properties popsUs = null;
        Properties popsVn = null;
        
        FileOutputStream fswKr = null;
        FileOutputStream fswJa = null;
        FileOutputStream fswCz = null;
        FileOutputStream fswUs = null;
        FileOutputStream fswVn = null;
        
        // loger.info("[ MAKE BACKUP FILE ] START..........");
        for (int i = 0; i < lisSize; i++) {
            filename = files[i].getName();
            if ( filename != null ) {
                String propsFileName = propertiesDir.getPath() + File.separator + filename;
                fileCopy(propsFileName, propsFileName + EXTENTENTION_FOR_TEMP_FILE);
            }
        }
        // loger.info("[ MAKE BACKUP FILE ] END..........");
        
        try {
            
            for (int i = 0; i < lisSize; i++) {
                filename = files[i].getName();
                if ( filename != null ) {
                    
                    String propsFileName = propertiesDir.getPath() + File.separator + filename;
                    if ( filename.endsWith("_ja.properties") ) {
                        popsJa = this.loadFileProperties( propsFileName );
                        fswJa = new FileOutputStream(propsFileName); 
                        
                    } else if ( filename.endsWith("_zh.properties") ) {
                        popsCz = this.loadFileProperties( propertiesDir.getPath() + File.separator + filename );
                        fswCz = new FileOutputStream(propsFileName); 
                        
                    } else if ( filename.endsWith("_en.properties") ) {
                        popsUs = this.loadFileProperties( propertiesDir.getPath() + File.separator + filename );
                        fswUs = new FileOutputStream(propsFileName); 
                        
                    }else if ( filename.endsWith("_it.properties") ) {
                    	//vietnam (it : Italian 인용, 베트남 지원x)
                        popsVn = this.loadFileProperties( propertiesDir.getPath() + File.separator + filename );
                        fswVn = new FileOutputStream(propsFileName); 
                        
                    } else {
                        popsKr = this.loadFileProperties( propertiesDir.getPath() + File.separator + filename );
                        fswKr = new FileOutputStream(propsFileName);             
                    }
                }
            }       
            
            // loger.info("[ model ] :" + model);
            if (model.get("selectIds") != null) {
	            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
	                
	                String stGubun = (String)model.get("ST_GUBUN" + i);
	                String motoKey = (String)model.get("PROP_KEY" + i);
	                String key = CommonUtil.getTrimString(motoKey);   // 입력값에서 공백제거(properties파일 KEY값)
	                
	                String valKr = (model.get("PROP_VAL_KR" + i) != null && StringUtils.isNotEmpty((String)model.get("PROP_VAL_KR" + i)))?(String)model.get("PROP_VAL_KR" + i): motoKey;
	                String valJp = (model.get("PROP_VAL_JA" + i) != null && StringUtils.isNotEmpty((String)model.get("PROP_VAL_JA" + i)))?(String)model.get("PROP_VAL_JA" + i): "[일]" + valKr;
	                String valCz = (model.get("PROP_VAL_CZ" + i) != null && StringUtils.isNotEmpty((String)model.get("PROP_VAL_CZ" + i)))?(String)model.get("PROP_VAL_CZ" + i): "[중]" + valKr;
	                String valUs = (model.get("PROP_VAL_US" + i) != null && StringUtils.isNotEmpty((String)model.get("PROP_VAL_US" + i)))?(String)model.get("PROP_VAL_US" + i): "[영]" + valKr;
	                String valVn = (model.get("PROP_VAL_VN" + i) != null && StringUtils.isNotEmpty((String)model.get("PROP_VAL_VN" + i)))?(String)model.get("PROP_VAL_VN" + i): "[베]" + valKr;
	                
	                
	                if( "INSERT".equals(stGubun) || "UPDATE".equals(model.get("ST_GUBUN"+i)) ) {
	                    if ( popsKr != null ) { 
	                        popsKr.setProperty(key, valKr);
	                    }
	                    
	                    if ( popsJa != null ) {
	                        popsJa.setProperty(key, valJp);
	                    }
	                    
	                    if ( popsCz != null ) {
	                        popsCz.setProperty(key, valCz);
	                    }
	                    
	                    if ( popsUs != null ) { 
	                        popsUs.setProperty(key, valUs);
	                    }
	
	                    if ( popsVn != null ) { 
	                        popsVn.setProperty(key, valVn);
	                    }
	                    
	                }else if("DELETE".equals(model.get("ST_GUBUN"+i))){
	                    if ( popsKr != null ) 
	                        popsKr.remove(key);
	                    
	                    if ( popsJa != null ) 
	                        popsJa.remove(key);
	                    
	                    if ( popsCz != null ) 
	                        popsCz.remove(key);
	                    
	                    if ( popsUs != null ) 
	                        popsUs.remove(key);
	                    
	                    if ( popsVn != null ) 
	                        popsVn.remove(key);
	                    
	                }else{
	                    errCnt++;
	                    m.put("errCnt", errCnt);
	                }
	            }
            }
            
            m.put("errCnt", errCnt);
            if(errCnt == 0){
            	if (loger.isDebugEnabled()) {
            		loger.debug("errCnt : " + errCnt);
            	}
                if ( popsKr != null ) 
                    popsKr.store(fswKr, DateUtil.getLocalDateTime());
                if ( popsJa != null ) 
                    popsJa.store(fswJa, DateUtil.getLocalDateTime());
                if ( popsCz != null ) 
                    popsCz.store(fswCz, DateUtil.getLocalDateTime());
                if ( popsUs != null ) 
                    popsUs.store(fswUs, DateUtil.getLocalDateTime());
                if ( popsVn != null ) 
                    popsVn.store(fswVn, DateUtil.getLocalDateTime());
                
                /*
                if (loger.isDebugEnabled()) {
                	loger.debug("STORE END....");
                }
                */
                
                m.put("MSG", MessageResolver.getMessage("save.success"));
            }
            
            fswKr.close(); 
            fswJa.close();
            fswCz.close();
            fswUs.close();
            fswVn.close();
            
            
        } catch(Exception e){
            
            // TEMP파일에서 Properties파일을 복원
            // loger.info("[ COPY FROM BACKUP FILE ] START..........");
            File[] file = propertiesDir.listFiles();
            int fileSize = (propertiesDir == null) ? 0 :  file.length;
            for (int i = 0; i < fileSize; i++) {
                String filenameTemp = file[i].getName();
                if ( filenameTemp != null && filenameTemp.endsWith(EXTENTENTION_FOR_TEMP_FILE) ) {
                    
                    String propsFileName = propertiesDir.getPath() + File.separator + filenameTemp;
                    try {
                        File tempFile = new File(propsFileName);
                        if (tempFile.exists()) { 
                            fileCopy(propsFileName, propsFileName.substring(0, (propsFileName.length() - EXTENTENTION_FOR_TEMP_FILE.length())));
                        }
                    } catch (Exception ignored) {}
                }
            }
            // loger.info("[ COPY FROM BACKUP FILE ] END..........");
            throw e;
            
        } finally {
            // 자원 해제
			if (fswKr != null) {
				try {
					fswKr.close();
				} catch (Exception e) {
				}
			}
			if (fswJa != null) {
				try {
					fswJa.close();
				} catch (Exception e) {
				}
			}
			if (fswCz != null) {
				try {
					fswCz.close();
				} catch (Exception e) {
				}
			}
			if (fswUs != null) {
				try {
					fswUs.close();
				} catch (Exception e) {
				}
			}
			if (fswVn != null) {
				try {
					fswVn.close();
				} catch (Exception e) {
				}
			}
            // TEMP파일 삭제
			if (propertiesDir != null) {
	            File[] file = propertiesDir.listFiles();
	            int fileSize = file.length;
	            
	            // loger.info("[ DELETE BACKUP FILE ] START..........");
	            for (int i = 0; i < fileSize; i++) {
	                final String filenameFinal = file[i].getName();
	                if ( filenameFinal != null && filenameFinal.endsWith(EXTENTENTION_FOR_TEMP_FILE) ) {
	                    try {
	                        File tempFile = new File(propertiesDir.getPath() + File.separator + filenameFinal);
	                        if (tempFile.exists()) { 
	                            tempFile.delete();
	                        }
	                    } catch (Exception ignored) {}
	                }
	            }
			}
            // loger.info("[ DELETE BACKUP FILE ] END..........");
        }
        
        return m;
    }
    
    @Override
    public Map<String, Object> checkProperties(Map<String, Object> model, HttpServletRequest request) throws Exception {
        
        Map<String, Object> m = new HashMap<String, Object>();
        
        final File propertiesDir = new File(request.getSession().getServletContext().getRealPath(WEB_APP_PATH_MESSAGE));
        
        int errCnt = 0;
        try {
            List keyList = loadPropertiesFiles(propertiesDir, 1);
            for(int i = 0 ; i < Integer.parseInt(model.get("selectIds").toString()) ; i ++){
                
                String stGubun = (String)model.get("ST_GUBUN" + i);
                String key = (String)model.get("PROP_KEY" + i);
                
                if( "INSERT".equals(stGubun) ) {
                    if (keyList.contains(key)) {
                        errCnt++;
                        throw new BizException( MessageResolver.getMessage("PK입력값중복", new String[]{key}) );
                    }
                }
            }
            m.put("MSG", MessageResolver.getMessage("save.success"));
            m.put("errCnt", errCnt);
     
        } catch(BizException be) {
            m.put("MSG", be.getMessage());
            m.put("errCnt", errCnt);
        
        } catch (Exception e) {
            if (loger.isInfoEnabled()) {
                loger.info("Error Occurred in Check Properties... :", e);
            }
            throw e;
    
        }
        return m;
    }    
    
    /**
     * 대체 Method ID   : listExcel
     * 대체 Method 설명 : 엑셀.
     * 작성자      : chsong
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        loger.info(model);
        
        model.put("pageIndex", "1");
        model.put("pageSize", "60000");

        return map;
    }
    
    @SuppressWarnings("PMD")
    private static void fileCopy(String source, String target) {
        // 복사 대상이 되는 파일 생성 
        File sourceFile = new File( source );

        // 스트림 선언 
        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            // 스트림 생성
            inputStream = new FileInputStream(sourceFile);
            outputStream = new FileOutputStream(target);
    
            int bytesRead = 0;
            //  인풋스트림을 아웃픗스트림에 쓰기
            byte[] buffer = new byte[1024];
            while ((bytesRead = inputStream.read(buffer, 0, 1024)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.flush();
            
        } catch (Exception e) {
            e.printStackTrace();
            
        }finally{
            // 자원 해제
            try{ if (inputStream != null) inputStream.close(); }catch(IOException ioe){}
            try{ if (outputStream != null) outputStream.close(); }catch(IOException ioe){}            
        
        }
    }
}
