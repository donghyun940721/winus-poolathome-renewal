package com.logisall.winus.wmsys.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSYS300Dao")
public class WMSYS300Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
   /**
     * Method ID : list
     * Method 설명 : 송신문서 내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmsys300.list", model);
    }
    
    /**
     * Method ID : insert
     * Method 설명 : 송수신문서 insert
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insert(Map<String, Object> model) {
        return executeInsert("wmsys300.insert", model);
    }
    
    /**
     * 
     * Method ID   : update_stat_cd
     * Method 설명 : 처리상태를 업데이트
     * 작성자      : 기드온
     * @param model
     * @return
     */
    public int updateTrStat(Map<String, Object> model) {
        Object obj = executeUpdate("wmsys300.tr_stat.update", model);
        int rtn = 1;
        if(obj !=null){
               rtn = Integer.parseInt(obj.toString());
        }
        return rtn;
    }   
    
    /**
     * Method ID    : runProc
     * Method 설명      : 물류용기관리 등록
     * 작성자                 : 기드온
     * @param   model
     * @return  Object
     */
    public Object runProc(Map<String, Object> model){
        executeUpdate("wmsys300.PK_WMSST000.SP_FILLSTOCKALL_JOB_MANUAL", model);
        return model;
    }
    
    /*-
     * Method ID : checkExistData
     * Method 설명 : 기준정보 삭제 가능여부 확인
     * 작성자 : kwt
     *
     * @param model
     * @return
     */
    public String checkExistData(Map<String, Object> model) {
        return (String)executeView("wmsys300e7.selectExistData", model);
    }
    
    /**
     * Method ID : wmssp010t2Tmsys900Insert
     * Method 설명 : 파일을 업로드
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object t2FileInsert(Map<String, Object> model) {
        return executeInsert("wmssp010t2.t2FileInsert", model);
    }
	
	/**
     * Method ID : insert
     * Method 설명 : 통합 HelpDesk 등록
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public Object insertInfo(Map<String, Object> model) {
        return executeInsert("wmssp010.itemImgInfoSave", model);
    }
    
    /**
	 * Method ID : excelUploadCsOrdUpload 
	 * Method 설명 : 
	 * 작성자 : 기드온
	 * 
	 * @param model
	 * @return
	 */
	public void excelUploadCsOrdUpload(Map<String, Object> model, List list) throws Exception {

		SqlMapClient sqlMapClient = getSqlMapClient();
		try {
			sqlMapClient.startTransaction();
			Map<String, Object> paramMap = null;
			for (int i = 0; i < list.size(); i++) {
				paramMap = (Map) list.get(i);
				
				Map<String, Object> modelSP = new HashMap<String, Object>();
				modelSP.put("CUST_CD"	, paramMap.get("CUST_CD"));
				modelSP.put("ORG_ORD_ID", paramMap.get("ORG_ORD_ID"));
				String salesCustId = (String)executeView("wmsys300e7_2.selectExistData", modelSP);
				
				if (  (salesCustId      		  	 != null && StringUtils.isNotEmpty(salesCustId.toString()))
				   && (paramMap.get("ORG_ORD_ID")    != null && StringUtils.isNotEmpty(paramMap.get("ORG_ORD_ID").toString()))
				   && (paramMap.get("ORG_ORD_ID")    != null && StringUtils.isNotEmpty(paramMap.get("ORG_ORD_ID").toString()))
				   && (paramMap.get("CS_REQ_DT") 	 != null && StringUtils.isNotEmpty(paramMap.get("CS_REQ_DT").toString()))
				   && (paramMap.get("SALES_CUST_NM") != null && StringUtils.isNotEmpty(paramMap.get("SALES_CUST_NM").toString()))
				   ){
					
					Map<String, Object> modelDt = new HashMap<String, Object>();
		            modelDt.put("vrSalesCustId" 	, salesCustId);
		            modelDt.put("vrSrchAsCsType" 	, "CS");
		            modelDt.put("vrSrchDrivers" 	, "");
		            modelDt.put("TextReqContents" 	, (String)paramMap.get("TEXT_REQ_CONTENTS"));
		            modelDt.put("vrSrchReqMemo" 	, "");
		            modelDt.put("TextResult" 		, (String)paramMap.get("TEXT_RESULT"));
		            modelDt.put("vrSrchReqCost" 	, "");
		            modelDt.put("vrSrchComCost" 	, "");
		            modelDt.put("vrCsReqDt" 		, (String)paramMap.get("CS_REQ_DT"));
		            modelDt.put("vrSrchComType" 	, "Y");
		            modelDt.put("vrSrchInOutBound" 	, "");
		            modelDt.put("vrSrchOrdBill" 	, "");
		            modelDt.put("vrSrchCostType" 	, "");
		            modelDt.put("vrSrchOrdOption" 	, "");
		            
				    modelDt.put("UPD_NO"    		, model.get(ConstantIF.SS_USER_NO));
				    modelDt.put("LC_ID"    			, model.get(ConstantIF.SS_SVC_NO));
		            
					sqlMapClient.insert("wmsct020.insertE3_upload", modelDt);
					//executeUpdate("wmsys300E7_2.excelUploadCsOrdUpload", paramMap);
				}
			}
			sqlMapClient.endTransaction();

		} catch (Exception e) {
			e.printStackTrace();
			throw e;

		} finally {
			if (sqlMapClient != null) {
				sqlMapClient.endTransaction();
			}
		}
	}
}
