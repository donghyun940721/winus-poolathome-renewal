package com.logisall.winus.wmsys.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("WMSYS030Dao")
public class WMSYS030Dao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
    
    /**
     * Method ID    : list
     * Method 설명      : 템플릿관리 조회
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public GenericResultSet list(Map<String, Object> model) {
        return executeQueryPageWq("wmstp010.listTmp", model);
    }
    
    /**
     * Method ID    : selectStartRow
     * Method 설명      : 템플릿 시작행 조회
     * 작성자                 : sing09
     * @param   model
     * @return
     */
    public GenericResultSet selectStartRow(Map<String, Object> model) {
    	return executeQueryPageWq("wmsys030.selectStartRow", model);
    }
    
	/**
	 * Method ID : updateStartRow
	 * Method 설명 : 템플릿 시작행 변경 
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateStartRow(Map<String, Object> model) {
		return executeUpdate("wmsys030.updateStartRow", model);
	}
    
    /**
     * Method ID    : list
     * Method 설명      : 템플릿관리 저장
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object save(Map<String, Object> model){        
        return executeUpdate("wmsys030.pk_wmsys030.sp_save_template", model);
    }
    
    /**
     * Method ID   		 : saveKit
     * Method 설명      	 : 출고(임가공) 템플릿 저장
     * 작성자                 : KSJ
     * @param   model
     * @return
     */
    public Object saveKit(Map<String, Object> model){        
         return executeUpdate("wmsys030.pk_wmsys030.sp_save_template_kit", model);
    }
    
    
    /**
     * Method ID    : delete
     * Method 설명      : 템플릿관리 삭제
     * 작성자                 : chsong
     * @param   model
     * @return
     */
    public Object delete(Map<String, Object> model){
        return executeUpdate("wmsys030.pk_wmsys030.sp_delete_template", model);
    }

	public GenericResultSet defaultTemplate(Map<String, Object> model) {
		return executeQueryPageWq("wmstp010.listDefault", model);
	}

	public GenericResultSet templateList(Map<String, Object> model) {
		
		return executeQueryPageWq("wmstp010.templateList", model);
	}

	public Object updateRemark(Map<String, Object> model) {
		return executeUpdate("wmstp010.updateRemark", model);
		
	}
	
	/**
	 * Method ID : saveTemplate 
	 * Method 설명 : 공란 추가 기능
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object saveTemplate(Map<String, Object> model) {
		return executeInsert("wmsys030.saveTemplate", model);
	}
	
	/**
	 * Method ID : updateTemplateViewSeq 
	 * Method 설명 : 공란 추가시 기존 컬럼들 view_seq 증가
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateTemplateViewSeq(Map<String, Object> model) {
		return executeUpdate("wmsys030.updateTemplateViewSeq", model);
	}
	
	/**
	 * Method ID : updateTemplate 
	 * Method 설명 : 공란 추가시 기존 컬럼들 view_seq 증가
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateTemplate(Map<String, Object> model) {
		return executeUpdate("wmsys030.updateTemplate", model);
	}
	
	/**
	 * Method ID : updateTemplateName
	 * Method 설명 : 템플릿 명칭 변경
	 * 작성자 : sing09
	 * 
	 * @param model
	 * @return
	 */
	public Object updateTemplateName(Map<String, Object> model) {
		return executeUpdate("wmsys030.updateTemplateName", model);
	}
	
	/**
	 * Method ID : insertNewTemplate
	 * Method 설명 : 템플릿 생성
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object insertNewTemplate(Map<String, Object> model) {
		return executeInsert("wmsys030.insertNewTemplate", model);
	}
	
	/**
	 * Method ID : insertTemplateCopy030
	 * Method 설명 : 템플릿 복사
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object insertTemplateCopy030(Map<String, Object> model) {
		return executeInsert("wmsys030.insertTemplateCopy030", model);
	}
	
	/**
	 * Method ID : insertTemplateCopy040
	 * Method 설명 : 템플릿 복사
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object insertTemplateCopy040(Map<String, Object> model) {
		return executeInsert("wmsys030.insertTemplateCopy040", model);
	}
	
	/**
	 * Method ID : deleteTemplate
	 * Method 설명 : 템플릿 삭제 030
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object deleteTemplate030(Map<String, Object> model) {
		return executeDelete("wmsys030.deleteTemplate030", model);
	}
	/**
	 * Method ID : deleteTemplate
	 * Method 설명 : 템플릿 삭제 040
	 * 작성자 : sing09
	 * @param model
	 * @return
	 */
	public Object deleteTemplate040(Map<String, Object> model) {
		return executeDelete("wmsys030.deleteTemplate040", model);
	}
}
