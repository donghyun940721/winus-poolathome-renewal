package com.logisall.winus.wmsys.service.impl;


import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.imageio.ImageIO;


import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.ServiceUtil;
import com.logisall.winus.frm.common.util.XmlTransform;
import com.logisall.winus.frm.exception.BizException;
import com.logisall.winus.wmsys.service.WMSYS300Service;
import com.logisall.ws.interfaces.common.ConstantWSIF;
import com.m2m.jdfw5x.egov.message.MessageResolver;

@Service("WMSYS300Service")
public class WMSYS300ServiceImpl implements WMSYS300Service {
    protected Log log = LogFactory.getLog(this.getClass());

    @Resource(name = "WMSYS300Dao")
    private WMSYS300Dao dao;
    
  
    /**
     * Method ID : list
     * Method 설명 : 송신문서 내역 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        try {
            model.put("S_SVC_NO", 	ConstantWSIF.WS_ADMIN_ID);
            model.put("SS_SVC_NO", 	ConstantWSIF.WS_ADMIN_ID);            
            
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }
    
    /**
     * 
     * Method ID   : insert_R
     * Method 설명 : 송수신문서 재처리
     * 작성자      :  기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> insertRetry(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
                
        try {
            model.put("D_SVC_NO", model.get(ConstantIF.SS_SVC_NO));
            model.put("D_TR_STAT","R");
            model.put("D_ERR_CD","");
            model.put("D_MESSAGE","");
            model.put("INSERT_NO", model.get(ConstantIF.SS_USER_ID));
            dao.insert(model);
            
            Map<String, Object> modelDt = new HashMap<String, Object>();
            // modelDt.put("SVC_NO", model.get(ConstantIF.SS_SVC_NO));
            modelDt.put("SVC_NO", model.get(ConstantIF.LC_ADMIN));
            
            modelDt.put("TR_ID", model.get("D_TR_ID"));
            modelDt.put("TR_STAT", "RS");
            
            dao.updateTrStat(modelDt);
            
            map.put("MSG", MessageResolver.getMessage("complete"));
        } catch (Exception e) {
            e.printStackTrace();
            map.put("MSG", MessageResolver.getMessage("save.error", new String[] {}));
        }
        return map; 
    }
    
    /**
     * 
     * Method ID   : detail
     * Method 설명 : xml 원문보기
     * 작성자      :  기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        XmlTransform xfrm = new XmlTransform();
        
        String pathDirectory = (String)model.get("S_PATH");
        String fileName = (String)model.get("S_FILENAME");
        
        String filePath = (new StringBuffer()
        		.append(ConstantIF.TEMP_PATH)
        		.append(pathDirectory)
        		.append(File.separator)
        		.append(fileName)
        		).toString();
        
        model.put("FILEPATH", filePath);        
        map = (Map)(xfrm.creatXmlString(model));
                
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : save
     * 대체 Method 설명    : 물류용기관리 저장
     * 작성자                      : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> runProc(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();

        try{            
            Map<String, Object> modelIns = new HashMap<String, Object>();
            modelIns.put("I_SUBUL_DT"      , (String)model.get("PARAM_DATE_SET"+1));
            modelIns.put("I_LC_ID"         , (String)model.get("PARAM_LC_ID"+1));
            //modelIns.put("LC_ID"     , (String)model.get(ConstantIF.SS_SVC_NO));
            //modelIns.put("WORD_IP"   , (String)model.get(ConstantIF.SS_CLIENT_IP));
            //modelIns.put("USER_NO"   , (String)model.get(ConstantIF.SS_USER_NO));
            
            modelIns = (Map<String, Object>)dao.runProc(modelIns);
            ServiceUtil.isValidReturnCode("WMSYS300", String.valueOf(modelIns.get("O_MSG_CODE")), (String)modelIns.get("O_MSG_NAME"));

            m.put("errCnt", 0);
            m.put("MSG", MessageResolver.getMessage("save.success"));
            
        } catch(BizException be) {
            m.put("errCnt", 1);
            m.put("MSG", be.getMessage() );
            
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * Method ID : excelUploadRstApiImgGet
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> excelUploadRstApiImgGet(Map<String, Object> model, List list) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        int errCnt = 0;
        int insertCnt = (list != null)?list.size():0;
            try{
                Map<String, Object> paramMap = null;
    			for (int i = 0; i < list.size(); i++) {
    				paramMap = (Map) list.get(i);
    				Map<String, Object> modelSP = new HashMap<String, Object>();
					modelSP.put("CUST_CD"	, paramMap.get("CUST_CD"));
					//modelSP.put("ORG_ORD_ID", paramMap.get("ORG_ORD_ID"));
    				
					//String dlvOrdId  = dao.checkExistData(modelSP);
					String dlvOrdId  = (String)paramMap.get("DLV_ORD_ID");
    				String serialUrl = (String)paramMap.get("SERIAL_IMG_PATH");
    				
    				String[] addDlvImgPath = paramMap.get("DLV_IMG_PATH").toString().split("\n");
		            for (int k = 0; k < addDlvImgPath.length; k++) {
		            	if (  dlvOrdId != null && !StringUtils.isEmpty(dlvOrdId)
		            	   && addDlvImgPath[k] != null && !StringUtils.isEmpty(addDlvImgPath[k])){
							// 1. 원주문번호 검색 후 이미지URL이 있으면 이미지 파일 저장.
		            		String ext       = ".JPEG";
		            		String fileId    = dlvOrdId + "_" + CommonUtil.getLocalDateTime().substring(0, 8) + "photo"+(k+1) + ext;
		            		String fileRoute = "E:/ATCH_FILE/INTERFACE/"+CommonUtil.getLocalDateTime().substring(2, 8)+"/"+dlvOrdId+"_"+CommonUtil.getLocalDateTime().substring(0, 8);
		            		String fileNewNm = "photo"+(k+1);
		            		String fileSize  = "0";
		            		String workOrdId = dlvOrdId;
		            		
		            		String imgUrl = addDlvImgPath[k];
		            		String dir    = "E:\\ATCH_FILE\\INTERFACE\\"+CommonUtil.getLocalDateTime().substring(2, 8)+"\\"+dlvOrdId+"_"+CommonUtil.getLocalDateTime().substring(0, 8);
		            		String serialFile = "photo0"+ext;
		            		String goodsFile  = "photo"+(k+1)+ext;
		            		String serialFileId = dlvOrdId + "_" + CommonUtil.getLocalDateTime().substring(0, 8) + serialFile;
		            		if(k == 0 && serialUrl != null && !StringUtils.isEmpty(serialUrl)){
		            			// 시리얼 이미지가 있으면 저장
		            			binaryImgSave(serialUrl, dir, serialFile);
		            			
		            			// 2. 이미지 파일 저장 완료 후 저장정보 DB Insert.	            		
			            		Map<String, Object> modelDt = new HashMap<String, Object>();
			            		modelDt.put("FILE_ID"		, serialFileId);
								modelDt.put("ATTACH_GB"		, "INTERFACE"); // 통합 HELPDESK 업로드
								modelDt.put("FILE_VALUE"	, "wmssp010"); // 기존코드 "tmsba230" 로
								modelDt.put("FILE_PATH"		, fileRoute); // 서버저장경로
								modelDt.put("ORG_FILENAME"	, serialFile); // 원본파일명
								modelDt.put("FILE_EXT"		, ext); // 파일 확장자
								modelDt.put("FILE_SIZE"		, fileSize);// 파일 사이즈
								modelDt.put("WORK_ORD_ID"	, workOrdId); // 원본파일명
								
								String fileMngNo = (String) dao.t2FileInsert(modelDt);
								dao.insertInfo(modelDt);
		            		}
		            		// 상품 이미지 저장
		            		binaryImgSave(imgUrl, dir, goodsFile);
		            		
		            		// 2. 이미지 파일 저장 완료 후 저장정보 DB Insert.	            		
		            		Map<String, Object> modelDt = new HashMap<String, Object>();
		            		modelDt.put("FILE_ID"		, fileId);
							modelDt.put("ATTACH_GB"		, "INTERFACE"); // 통합 HELPDESK 업로드
							modelDt.put("FILE_VALUE"	, "wmssp010"); // 기존코드 "tmsba230" 로
							modelDt.put("FILE_PATH"		, fileRoute); // 서버저장경로
							modelDt.put("ORG_FILENAME"	, fileNewNm + ext); // 원본파일명
							modelDt.put("FILE_EXT"		, ext); // 파일 확장자
							modelDt.put("FILE_SIZE"		, fileSize);// 파일 사이즈
							modelDt.put("WORK_ORD_ID"	, workOrdId); // 원본파일명
							
							String fileMngNo = (String) dao.t2FileInsert(modelDt);
							dao.insertInfo(modelDt);
							
		                    map.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
		                    map.put("MSG_ORA", "");
		                    map.put("errCnt", errCnt);
	                    }
		            }
    			}
                //dao.excelUploadRstApiImgGet(model, list);
                
            } catch(Exception e){
            	map.put("MSG", MessageResolver.getMessage("save.error"));
            	map.put("MSG_ORA", e.getMessage());
            	map.put("errCnt", "1");
                throw e;
            }
        return map;
    }    
    
	public void binaryImgSave(String imgUrl, String dir, String file) throws Exception{
		//하우저 인증키 정보 필수
        //개발서버
		//String key    = "OpenApiKey";
		//String value  = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiI1ZWVmZGM1My03ZDk4LTQyNzMtOGM5Yi0xMTcyZTE0MmNiNDAiLCJnIjoiZ3JwMTgwODAwMDA2Iiwic3YiOnsiMSI6MX19.pbbcziGaU_oNOSu_NoGXgcMT2kjvshk5isXjLYObsAHshSiWoC6QAxmOqG8fSoO8hwmRHP32Ld8anwnN6uefDDivleocAAZj3S-9RaM_xaKqJUJsbtOEtYaKt4u6ZBBUD8Xy-ZUdLgiID5qw-YfYSY6WeWVmmOuU3v0oc7Gn8e4";
        //운영서버
		String key    = "OpenApiKey";
		String value  = "eyJhbGciOiJSUzI1NiJ9.eyJqdGkiOiIzZWY3YWY5OC0yMzYyLTQwY2MtYmNhYS1hYjE0OGFhYWQ2ZjIiLCJnIjoiZ3JwMTgwODAwMDE1Iiwic3YiOnsiMSI6MX19.UP9_Q1N30vUUTt3pr1EjZ3sybrpekqamlY2yHjl2a9oUuXiqss9T2_iBEh86VQXPCCkTuJYSgjMqjD36WLFaP2qJdFSK1ggdZVkYh-5mbcIA8r1P3XudG4gw1Qvsv9kSd84Jd3G-aF5Vt4Fa1icUpvyLmMBrlb9Jwa1_UX7-UHo";
        String rstUrl = imgUrl;
        
		URL url = new URL(rstUrl);
		HttpURLConnection con = (HttpURLConnection) url.openConnection(); 
		con.setConnectTimeout(50000);	//서버에 연결  Timeout 설정
		con.setReadTimeout(50000);		// InputStream 읽기 Timeout 설정
		con.setRequestMethod("GET");
		con.setDoOutput(false); 
		con.setDoInput(true);
		
		//header 파라미터셋팅
		con.addRequestProperty(key, value);
		con.setRequestProperty("Content-Type", "application/json");
		
		if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
        	int len = con.getContentLength();
            byte[] tmpByte = new byte[len];
            
            File chkDir   = new File(dir);
            File savePath = new File(dir +"\\"+ file);
            
            if(!chkDir.isDirectory()){
            	// 해당 폴더가 없으면 생성
            	chkDir.mkdirs();
			}
            
            InputStream is = con.getInputStream();
            FileOutputStream fos = new FileOutputStream(savePath);

            int read;
            while(true) {
                read = is.read(tmpByte);
                if (read <= 0) {
                    break;
                }
                fos.write(tmpByte, 0, read); //file 생성
            }

            is.close();
            fos.close();
            con.disconnect();
		}
	}
	
	/**
     * Method ID : excelUploadCsOrdUpload
     * Method 설명 : CSV 저장
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
	public Map<String, Object> excelUploadCsOrdUpload(Map<String, Object> model, List list) throws Exception {
       Map<String, Object> m = new HashMap<String, Object>();
       int errCnt = 0;
       int insertCnt = (list != null)?list.size():0;
           try{            	
               dao.excelUploadCsOrdUpload(model, list);
               
               m.put("MSG", MessageResolver.getMessage("엑셀저장성공", new String[]{String.valueOf(insertCnt)}) );
               m.put("MSG_ORA", "");
               m.put("errCnt", errCnt);
               
           } catch(Exception e){
               throw e;
           }
       return m;
   }
}
