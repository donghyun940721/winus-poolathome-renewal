package com.logisall.winus.wmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsys.service.WMSYS040Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSYS040Service")
public class WMSYS040ServiceImpl extends AbstractServiceImpl implements WMSYS040Service {
    
    @Resource(name = "WMSYS040Dao")
    private WMSYS040Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 입출고주문 그리드관리 조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        return map;
    }
    
    
    /**
     * Method ID   : save
     * Method 설명    : 입출고주문 그리드관리 저장
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("save.success");
        try{
            int cnt = Integer.parseInt(model.get("selectIds").toString());
                
            if (cnt > 0) {
                

                for (int i = 0; i < cnt; i++) {
                	//여기 조심해야됨
                    //파라미터가 많아서 여기서 에러날 확률이 높다
                        
                    //배열로만들 변수들 처리해준다
                    String[] formatColSeq       = new String[1];
                    String[] formatViewSeq      = new String[1];
                    String[] formatColWidth     = new String[1];
                    String[] formatBindCol      = new String[1];
                    String[] formatBindColGrid  = new String[1];
                    String[] formatColFormat    = new String[1];

                    String[] formatMustYn       = new String[1];
                    String[] formatDefaultValue = new String[1];
                    String[] formatUseYn        = new String[1];
                    String[] formatColType      = new String[1];
                    String[] formatColNm        = new String[1];
                    
                    String[] stGubun            = new String[1];
                    String[] rowCustSeq          = new String[1];
                    
                    formatColSeq[0]         = (String)model.get("FORMAT_COL_SEQ"        + i);
                    formatViewSeq[0]        = (String)model.get("FORMAT_VIEW_SEQ"       + i);
                    formatColWidth[0]       = (String)model.get("FORMAT_COL_WIDTH"      + i);                    
                    formatBindCol[0]        = (String)model.get("FORMAT_BIND_COL"       + i);
                    formatBindColGrid[0]    = (String)model.get("FORMAT_BIND_COL_GRID"  + i);
                    formatColFormat[0]      = (String)model.get("FORMAT_COL_FORMAT"     + i);

                    formatMustYn[0]         = (String)model.get("FORMAT_MUST_YN"        + i);
                    formatDefaultValue[0]   = (String)model.get("FORMAT_DEFAULT_VALUE"  + i);
                    formatUseYn[0]          = (String)model.get("FORMAT_USE_YN"         + i);
                    formatColType[0]        = (String)model.get("FORMAT_COL_TYPE"       + i);
                    formatColNm[0]          = (String)model.get("FORMAT_COL_NM"         + i);
                    
                    stGubun[0]          	= (String)model.get("ST_GUBUN"         		+ i);
                    rowCustSeq[0]          	= (String)model.get("ROW_CUST_SEQ"         	+ i);
                    
                 // 프로시져에 보낼것들 다담는다
                    Map<String, Object> modelDt = new HashMap<String, Object>();

                    //배열
                    modelDt.put("formatColSeq"      , formatColSeq      );
                    modelDt.put("formatViewSeq"     , formatViewSeq     );
                    modelDt.put("formatColWidth"    , formatColWidth    );
                    modelDt.put("formatBindCol"     , formatBindCol     );
                    modelDt.put("formatBindColGrid" , formatBindColGrid );
                    modelDt.put("formatColFormat"   , formatColFormat   );

                    modelDt.put("formatMustYn"      , formatMustYn      );
                    modelDt.put("formatDefaultValue", formatDefaultValue);
                    modelDt.put("formatUseYn"       , formatUseYn       );
                    modelDt.put("formatColType"     , formatColType     );
                    modelDt.put("formatColNm"       , formatColNm       );
                    
                    modelDt.put("stGubun"      		, stGubun       	);
                    modelDt.put("rowCustSeq"      	, rowCustSeq       	);

                    // session 및 등록정보
                    modelDt.put("vrCustId"      , model.get("vrSrchCustId"));
                    modelDt.put("vrTemplateId"  , model.get("vrTemplateId"));
                    modelDt.put("vrCustSeq"     , model.get("vrCustSeq"));
                    modelDt.put("gvLcId"        , (String)model.get(ConstantIF.SS_SVC_NO));
                    
                    // dao
                    dao.save(modelDt);
                }
                
                
                m.put("MSG", errMsg);
                m.put("errCnt", errCnt);
            }
            
        }catch (Exception e) {
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("errCnt", -1);
        }
        return m;
    }
    
    /**
     * Method ID   : delete
     * Method 설명    : 입출고주문 그리드관리 삭제  (이거 프로시져확인해봐야됨)
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("delete.success");
        try{
            int cnt = Integer.parseInt(model.get("selectIds").toString());
                
            if (cnt > 0) {                    
                //배열로만들 변수들 처리해준다
                String[] formatTemplateId   = new String[cnt];
                String[] formatCustSeq      = new String[cnt];
                String[] formatColSeq       = new String[cnt];

                for (int i = 0; i < cnt; i++) {
                    formatTemplateId[i]   = (String)model.get("FORMAT_TEMPLATE_ID"  + i);
                    formatCustSeq[i]      = (String)model.get("FORMAT_CUST_SEQ"     + i);
                    formatColSeq[i]       = (String)model.get("FORMAT_COL_SEQ"      + i);         
                }
                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelDt = new HashMap<String, Object>();

                //배열
                modelDt.put("formatTemplateId"  , formatTemplateId[0]);
                modelDt.put("formatCustSeq"     , formatCustSeq[0]);
                modelDt.put("formatColSeq"      , formatColSeq    );

                // session 및 등록정보
                modelDt.put("vrCustId"      , model.get("vrSrchCustId"));
                modelDt.put("gvLcId"        , (String)model.get(ConstantIF.SS_SVC_NO));
                
                // dao
                dao.delete(modelDt);
                
                m.put("MSG", errMsg);
                m.put("errCnt", errCnt);
            }
            
        }catch (Exception e) {
            m.put("MSG", MessageResolver.getMessage("delete.error"));
            m.put("errCnt", -1);
        }
        return m;
    }
}
