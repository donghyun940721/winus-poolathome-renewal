package com.logisall.winus.wmsys.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.wmsys.service.WMSYS030Service;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("WMSYS030Service")
public class WMSYS030ServiceImpl extends AbstractServiceImpl implements WMSYS030Service {
    
    @Resource(name = "WMSYS030Dao")
    private WMSYS030Dao dao;

    /**
     * Method ID   : list
     * Method 설명    : 템플릿관리 조회
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.list(model));
        return map;
    }
    
    /**
     * Method ID : selectStartRow
     * Method 설명 : 템플릿 시작행 조회
     * 작성자 : sing09
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectStartRow(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("LIST", dao.selectStartRow(model));
        } catch (Exception e) {
            log.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }   
    
    /**
     * 
     * 대체 Method ID   : updatestartRow
     * 대체 Method 설명    : 템플릿 시작행 변경
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateStartRow(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();
    		modelDt.put("FORMAT_START_ROW"		, model.get("FORMAT_START_ROW"));
    		modelDt.put("FORMAT_TEMPLATE_ID"	, model.get("FORMAT_TEMPLATE_ID"));
    		modelDt.put("FORMAT_CUST_SEQ"   	, model.get("FORMAT_CUST_SEQ"));
    		modelDt.put("FORMAT_CUST_ID"   		, model.get("FORMAT_CUST_ID"));
    		modelDt.put("FORMAT_LC_ID" 			, model.get("FORMAT_LC_ID"));
    		dao.updateStartRow(modelDt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }

    
    
    /**
     * Method ID   : save
     * Method 설명    : 템플릿관리 저장
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> save(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("save.success");
        try{
            int cnt = Integer.parseInt(model.get("selectIds").toString());
         // 프로시져에 보낼것들 다담는다
            Map<String, Object> modelDt = new HashMap<String, Object>();
            
            if (cnt > 0) {
            	 String[] formatColSeq       = new String[cnt];
                 String[] formatViewSeq      = new String[cnt];
                 String[] formatUseSeq      = new String[cnt];
                 String[] formatColWidth     = new String[cnt];
                 String[] formatBindCol      = new String[cnt];
                 String[] formatColFormat    = new String[cnt];

                 String[] formatMustYn       = new String[cnt];
                 String[] formatDefaultValue = new String[cnt];
                 String[] formatTemplateInfo = new String[cnt];
                 String[] formatUseYn        = new String[cnt];
                 String[] formatColType      = new String[cnt];
                 String[] formatColNm        = new String[cnt];
                 
                 String[] stGubun            = new String[cnt];
                 String[] rowCustSeq         = new String[cnt];
                 
                 String[] formatSamValue     = new String[cnt];
            	
                for (int i = 0; i < cnt; i++) {
                	//여기 조심해야됨
                    //파라미터가 많아서 여기서 에러날 확률이 높다
                        
                    //배열로만들 변수들 처리해준다
                   
                    
                    formatColSeq[i]         = (String)model.get("FORMAT_COL_SEQ"        + i);
                    formatUseSeq[i]        = (String)model.get("FORMAT_USE_SEQ"       + i);
                    formatViewSeq[i]        = (String)model.get("FORMAT_VIEW_SEQ"       + i);
                    formatColWidth[i]       = (String)model.get("FORMAT_COL_WIDTH"      + i);                    
                    formatBindCol[i]        = (String)model.get("FORMAT_BIND_COL"       + i);
                    formatColFormat[i]      = (String)model.get("FORMAT_COL_FORMAT"     + i);

                    formatMustYn[i]         = (String)model.get("FORMAT_MUST_YN"        + i);
                    formatDefaultValue[i]   = (String)model.get("FORMAT_DEFAULT_VALUE"  + i);
                    formatTemplateInfo[i]   = (String)model.get("FORMAT_TEMPLATE_INFO"  + i);
                    formatUseYn[i]          = (String)model.get("FORMAT_USE_YN"         + i);
                    formatColType[i]        = (String)model.get("FORMAT_COL_TYPE"       + i);
                    formatColNm[i]          = (String)model.get("FORMAT_COL_NM"         + i);
                    
                    stGubun[i]          	= (String)model.get("ST_GUBUN"         		+ i);
                    rowCustSeq[i]          	= (String)model.get("ROW_CUST_SEQ"         	+ i); // 왜 배열 하나만 입력?
                    
                    formatSamValue[i]     	= (String)model.get("FORMAT_SAM_VALUE"     	+ i);
                    
                 
                }

                //배열
                modelDt.put("formatColSeq"      , formatColSeq      );
                modelDt.put("formatUseSeq"      , formatUseSeq      );
                modelDt.put("formatViewSeq"     , formatViewSeq     );
                modelDt.put("formatColWidth"    , formatColWidth    );
                modelDt.put("formatBindCol"     , formatBindCol     );
                modelDt.put("formatColFormat"   , formatColFormat   );

                modelDt.put("formatMustYn"      , formatMustYn      );
                modelDt.put("formatDefaultValue", formatDefaultValue);
                modelDt.put("formatTemplateInfo", formatTemplateInfo);
                modelDt.put("formatUseYn"       , formatUseYn       );
                modelDt.put("formatColType"     , formatColType     );
                modelDt.put("formatColNm"       , formatColNm       );
                
                modelDt.put("stGubun"      		, stGubun       	);
                modelDt.put("rowCustSeq"      	, rowCustSeq       	);
                
                modelDt.put("formatSamValue"   	, formatSamValue   	);

                // session 및 등록정보
                modelDt.put("vrCustId"      , model.get("vrSrchCustId"));
                modelDt.put("vrTemplateId"  , model.get("vrTemplateId"));
                modelDt.put("vrCustSeq"     , model.get("vrCustSeq"));
                modelDt.put("gvLcId"        , (String)model.get(ConstantIF.SS_SVC_NO));
                
                /*
                 *  ## 중요 ##
                 *   
                 *  요약 	  : 템플릿 업로드 분기 처리
                 *  작성자 : KSJ
                 *  내용	  : USE_SEQ 사용 여부에 따라 프로시저 타는곳이 다름 
                 *  			그 외 컬럼은 공통으로 사용하기떄문에 서비스단에서 분기처리했음
                 */
                if(model.get("vrTemplateId").equals("0000000003")){ // 출고(임가공) 템플릿 업로드일 경우
                	dao.saveKit(modelDt); 
                }else{ 																// 일반 입출고 템플릿 업로드일 경우
                	dao.save(modelDt);
                }
                
                if(model.get("vrCustSeq") == null || "".equals((String)model.get("vrCustSeq"))){
                	modelDt.put("vrTemplateNm", (String)model.get("vrTemplateNm"));
                	dao.updateRemark(modelDt);
                }
                
                m.put("MSG", errMsg);
                m.put("errCnt", errCnt);
            }
            
        }catch (Exception e) {
            m.put("MSG", MessageResolver.getMessage("save.error"));
            m.put("errCnt", -1);
        }
        return m;
    }
    
    /**
     * Method ID   : delete
     * Method 설명    : 템플릿관리 삭제  (이거 프로시져확인해봐야됨)
     * 작성자               : chsong
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        int errCnt = 0;
        String errMsg = MessageResolver.getMessage("delete.success");
        try{
            int cnt = Integer.parseInt(model.get("selectIds").toString());
                
            if (cnt > 0) {                    
                //배열로만들 변수들 처리해준다
                String[] formatTemplateId   = new String[cnt];
                String[] formatCustSeq      = new String[cnt];
                String[] formatColSeq       = new String[cnt];
                String[] formatViewSeq       = new String[cnt];
                
                //컬럼 제거 for문 
                for (int i = 0; i < cnt; i++) {
                    formatTemplateId[i]   = (String)model.get("FORMAT_TEMPLATE_ID"  + i);
                    formatCustSeq[i]      = (String)model.get("FORMAT_CUST_SEQ"     + i);
                    formatColSeq[i]       = (String)model.get("FORMAT_COL_SEQ"      + i);  
                }
                //view_seq 차감
                for (int i = cnt-1; 0 <= i; i--) {
                	formatViewSeq[i]       = (String)model.get("FORMAT_VIEW_SEQ"      + i); 
                	Map<String, Object> modelDt_SEQ = new HashMap<String, Object>();
                	//배열
                	modelDt_SEQ.put("FORMAT_TEMPLATE_ID"  , formatTemplateId[0]);
                	modelDt_SEQ.put("FORMAT_CUST_SEQ"     , formatCustSeq[0]);
                	modelDt_SEQ.put("FORMAT_VIEW_SEQ"      , formatViewSeq[i]);
                	dao.updateTemplateViewSeq(modelDt_SEQ);
                }
                // 프로시져에 보낼것들 다담는다
                Map<String, Object> modelDt = new HashMap<String, Object>();

                //배열
                modelDt.put("formatTemplateId"  , formatTemplateId[0]);
                modelDt.put("formatCustSeq"     , formatCustSeq[0]);
                modelDt.put("formatColSeq"      , formatColSeq    );

                // session 및 등록정보
                modelDt.put("vrCustId"      , model.get("vrSrchCustId"));
                modelDt.put("gvLcId"        , (String)model.get(ConstantIF.SS_SVC_NO));
                
                // dao
                dao.delete(modelDt);
                
                m.put("MSG", errMsg);
                m.put("errCnt", errCnt);
            }
            
        }catch (Exception e) {
            m.put("MSG", MessageResolver.getMessage("delete.error"));
            m.put("errCnt", -1);
        }
        return m;
    }
    
    /**
     * Method ID   : default list
     * Method 설명    : 템플릿관리 조회
     * 작성자               : khkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> defaultTemplate(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        
        model.put("pageSize", "60000");
        
        map.put("LIST", dao.defaultTemplate(model));
        return map;
    }
    
    /**
     * Method ID   : templateList
     * Method 설명    : 템플릿 종류
     * 작성자               : khkim
     * @param   model
     * @return
     * @throws  Exception
     */
    @Override
    public Map<String, Object> templateList(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        model.put("pageSize", "60000");
        map.put("LIST", dao.templateList(model));
        return map;
    }
    
    /**
     * 
     * 대체 Method ID   : saveTemplate
     * 대체 Method 설명    : 공란 저장
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> saveTemplate(Map<String, Object> model) throws Exception {
        Map<String, Object> m = new HashMap<String, Object>();
        try{
            Map<String, Object> modelDt = new HashMap<String, Object>();
            modelDt.put("FORMAT_TEMPLATE_ID"   	, model.get("FORMAT_TEMPLATE_ID"));
            modelDt.put("FORMAT_CUST_SEQ"   	, model.get("FORMAT_CUST_SEQ"));
            modelDt.put("FORMAT_COL_SEQ"   		, model.get("FORMAT_COL_SEQ"));
            modelDt.put("FORMAT_COL_NM"  		, model.get("FORMAT_COL_NM"));
            modelDt.put("FORMAT_USE_YN"  		, model.get("FORMAT_USE_YN"));
            modelDt.put("FORMAT_VIEW_SEQ" 		, model.get("FORMAT_VIEW_SEQ"));
            modelDt.put("FORMAT_COL_WIDTH"   	, model.get("FORMAT_COL_WIDTH"));
            modelDt.put("FORMAT_COL_ALIGN"   	, model.get("FORMAT_COL_ALIGN"));
            modelDt.put("FORMAT_UNIQUE_COL_YN"  , model.get("FORMAT_UNIQUE_COL_YN"));
            dao.saveTemplate(modelDt);
            m.put("MSG", MessageResolver.getMessage("save.success"));
        } catch(Exception e){
            throw e;
        }
        return m;
    }
    
    /**
     * 
     * 대체 Method ID   : updateTemplate
     * 대체 Method 설명    : 공란 저장시 기존 컬럼 view_seq 증가
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateTemplate(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();
    		modelDt.put("FORMAT_TEMPLATE_ID"   	, model.get("FORMAT_TEMPLATE_ID"));
    		modelDt.put("FORMAT_CUST_SEQ"   	, model.get("FORMAT_CUST_SEQ"));
    		modelDt.put("FORMAT_VIEW_SEQ" 		, model.get("FORMAT_VIEW_SEQ"));
    		dao.updateTemplate(modelDt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 
     * 대체 Method ID   : updateTemplateName
     * 대체 Method 설명    : 템플릿 명칭 변경 
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> updateTemplateName(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();
    		modelDt.put("FORMAT_TEMPLATE_ID"	, model.get("FORMAT_TEMPLATE_ID"));
    		modelDt.put("FORMAT_REMARK"			, model.get("FORMAT_REMARK"));
    		modelDt.put("FORMAT_CUST_SEQ"   	, model.get("FORMAT_CUST_SEQ"));
    		modelDt.put("FORMAT_CUST_ID"   		, model.get("FORMAT_CUST_ID"));
    		modelDt.put("FORMAT_LC_ID" 			, model.get("FORMAT_LC_ID"));
    		dao.updateTemplateName(modelDt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 대체 Method ID   : insertTemplateCopy
     * 대체 Method 설명    : 템플릿 복사
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> insertTemplateCopy(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();
    		modelDt.put("FORMAT_TEMPLATE_ID"	, model.get("FORMAT_TEMPLATE_ID"));
    		modelDt.put("FORMAT_CUST_SEQ"   	, model.get("FORMAT_CUST_SEQ"));
    		modelDt.put("FORMAT_CUST_ID"   		, model.get("FORMAT_CUST_ID"));
    		modelDt.put("FORMAT_CUST_COPY_ID"   , model.get("FORMAT_CUST_COPY_ID"));
    		modelDt.put("FORMAT_LC_ID" 			, model.get("FORMAT_LC_ID"));
    		//템플릿 틀 생성
    		dao.insertTemplateCopy030(modelDt);
    		//템플릿 세부 설정
    		dao.insertTemplateCopy040(modelDt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 대체 Method ID   : insertNewTemplate
     * 대체 Method 설명    : 템플릿 생성
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> insertNewTemplate(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();
    		modelDt.put("FORMAT_TEMPLATE_ID"	, model.get("FORMAT_TEMPLATE_ID"));
    		modelDt.put("FORMAT_LC_ID" 			, model.get("FORMAT_LC_ID"));
    		modelDt.put("FORMAT_CUST_ID"   		, model.get("FORMAT_CUST_ID"));
    		modelDt.put("FORMAT_REMARK"   		, model.get("FORMAT_REMARK"));
    		//템플릿 틀 생성
    		dao.insertNewTemplate(modelDt);
    		m.put("MSG", MessageResolver.getMessage("save.success"));
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
    /**
     * 대체 Method ID   : deleteTemplate
     * 대체 Method 설명    : 템플릿 삭제
     * 작성자                      : sing09
     * @param model
     * @return
     * @throws Exception
     */
    @Override
    public Map<String, Object> deleteTemplate(Map<String, Object> model) throws Exception {
    	Map<String, Object> m = new HashMap<String, Object>();
    	try{
    		Map<String, Object> modelDt = new HashMap<String, Object>();
    		modelDt.put("FORMAT_TEMPLATE_ID"	, model.get("FORMAT_TEMPLATE_ID"));
    		modelDt.put("FORMAT_CUST_SEQ"   	, model.get("FORMAT_CUST_SEQ"));
    		dao.deleteTemplate040(modelDt);
    		dao.deleteTemplate030(modelDt);
    		m.put("MSG", MessageResolver.getMessage("delete.success"));
    	} catch(Exception e){
    		throw e;
    	}
    	return m;
    }
    
}
