package com.logisall.winus.wmsys.service;

import java.util.Map;



public interface WMSYS030Service {
	public Map<String, Object> insertNewTemplate(Map<String, Object> model) throws Exception;
    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> selectStartRow(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateStartRow(Map<String, Object> model) throws Exception;
    public Map<String, Object> saveTemplate(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateTemplate(Map<String, Object> model) throws Exception;
    public Map<String, Object> updateTemplateName(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertTemplateCopy(Map<String, Object> model) throws Exception;
    public Map<String, Object> save(Map<String, Object> model) throws Exception;
    public Map<String, Object> delete(Map<String, Object> model) throws Exception;
    public Map<String, Object> deleteTemplate(Map<String, Object> model) throws Exception;
	public Map<String, Object> templateList(Map<String, Object> model) throws Exception;
	public Map<String, Object> defaultTemplate(Map<String, Object> model) throws Exception;
}
