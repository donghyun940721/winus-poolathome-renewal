package com.logisall.winus.wmsys.service;

import java.util.List;
import java.util.Map;

public interface WMSYS300Service {

    public Map<String, Object> list(Map<String, Object> model) throws Exception;
    public Map<String, Object> insertRetry(Map<String, Object> model) throws Exception;
    public Map<String, Object> runProc(Map<String, Object> model) throws Exception;
    // xml 원문보기
    public Map<String, Object> detail(Map<String, Object> model) throws Exception;
    public Map<String, Object> excelUploadRstApiImgGet(Map<String, Object> model, List list) throws Exception;
    public Map<String, Object> excelUploadCsOrdUpload(Map<String, Object> model, List list) throws Exception;
}
