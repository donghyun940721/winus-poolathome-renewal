package com.logisall.winus.wmsys.service;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;


public interface WMSYS020Service {
	/**interfacebody*/
    public Map<String, Object> listProperties(Map<String, Object> model, HttpServletRequest request) throws Exception;
    public Map<String, Object> saveProperties(Map<String, Object> model, HttpServletRequest request) throws Exception;
    public Map<String, Object> checkProperties(Map<String, Object> model, HttpServletRequest request) throws Exception;
    
    public Map<String, Object> listExcel(Map<String, Object> model) throws Exception;
}
