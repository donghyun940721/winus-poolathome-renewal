package com.logisall.winus.meta.web;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.logisall.winus.meta.service.MetaUserService;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;

@Controller
public class MetaUserController {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "metaUserService")
	private MetaUserService service;

	/*-
	 * 
	 * Method ID : mn 
	 * Method 설명 : 사용자관리 메인화면 
	 * 작성자 : Nam Yun Sung(남윤성)
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/metaUserMn2.action")
	public ModelAndView mn(Map<String, Object> model) {
		return new ModelAndView("fta/meta/metaUserMn"); // 맨 앞에 / 없음에 주의, .vm
														// 없음에 주의
	}

	/*-
	 * 
	 * Method ID : list 
	 * Method 설명 : 사용자관리 리스트 조회 
	 * 작성자 : Nam Yun Sung(남윤성)
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/metaUserLi/list2.action")
	public ModelAndView list(Map<String, Object> model) {
		ModelAndView mav = null;

		try {
			mav = new ModelAndView("jqGridJsonView", service.list(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get List info :", e);
			}
		}
		return mav;
	}

	/*-
	 * 
	 * Method ID : detail 
	 * Method 설명 : 사용자관리 상세 조회 
	 * 작성자 : Nam Yun Sung(남윤성)
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/metaUserDt/detail2.action")
	public ModelAndView detail(Map<String, Object> model) {
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("fta/meta/metaUserDt", service.detail(model));
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get detail info :", e);
			}
		}
		return mav;
	}

	/*-
	 * 
	 * Method ID : insert 
	 * Method 설명 : 사용자관리 등록 
	 * 작성자 : Nam Yun Sung(남윤성)
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/metaUserDt/insert2.action")
	public ModelAndView insert(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("/fta/meta/metaUserDt");
		Map<String, Object> m = null;

		try {
			m = service.insert(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to insert :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			m.put("MSG", MessageResolver.getMessage("insert.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * \ 
	 * Method ID : update 
	 * Method 설명 : 사용자관리 수정 
	 * 작성자 : Nam Yun Sung(남윤성)
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/metaUserDt/update2.action")
	public ModelAndView update(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("/fta/meta/metaUserDt");
		Map<String, Object> m = null;

		try {
			m = service.update(model);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to update :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("update.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}

	/*-
	 * 
	 * Method ID : delete 
	 * Method 설명 : 사용자관리 삭제 
	 * 작성자 : Nam Yun Sung(남윤성)
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping("/metaUserDt/delete2.action")
	public ModelAndView delete(Map<String, Object> model) {
		ModelAndView mav = new ModelAndView("/fta/meta/metaUserDt");
		Map<String, Object> m = null;

		try {
			m = service.delete(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to delete :", e);
			}
			m = new HashMap<String, Object>();
			m.put("MSG", MessageResolver.getMessage("delete.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
}
