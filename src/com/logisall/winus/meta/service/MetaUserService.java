package com.logisall.winus.meta.service;

import java.util.Map;

public interface MetaUserService {
	/**interfacebody*/
	public Map<String, Object> list(Map<String, Object> model) throws Exception;
	public Map<String, Object> detail(Map<String, Object> model) throws Exception;
	public Map<String, Object> detailRoll(Map<String, Object> model) throws Exception;
	public Map<String, Object> insert(Map<String, Object> model) throws Exception;
	public Map<String, Object> update(Map<String, Object> model) throws Exception;
	public Map<String, Object> delete(Map<String, Object> model) throws Exception;
	public Map<String, Object> selectHsCatgCombo(Map<String, Object> model) throws Exception;
	public Map<String, Object> updateLogin(Map<String, Object> model) throws Exception;
	public Map<String, Object> updateLogout(Map<String, Object> model) throws Exception;
	
}
