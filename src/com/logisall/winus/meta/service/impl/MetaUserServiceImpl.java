
package com.logisall.winus.meta.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import com.logisall.winus.frm.common.util.DESUtil;
import com.logisall.winus.meta.service.MetaUserService;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;
import com.m2m.jdfw5x.util.StringUtil;

@Service("metaUserService")
public class MetaUserServiceImpl extends AbstractServiceImpl implements MetaUserService {
	
    protected Log loger = LogFactory.getLog(this.getClass());

    @Resource(name = "metaUserDao")
    private MetaUserDao dao;

    /**
     * 대체 Method ID : list 대체 Method 설명 : 사용자관리 리스트 조회 작성자 : Nam Yun Sung(남윤성)
     * 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> list(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            if (model.get("page") == null) {
                model.put("pageIndex", "1");
            } else {
                model.put("pageIndex", model.get("page"));
            }
            if (model.get("rows") == null) {
                model.put("pageSize", "100");
            } else {
                model.put("pageSize", model.get("rows"));
            }

            String userType = (String)model.get("SS_USER_TYPE");
            if (userType.equals("M")) { // 서비스 관리자인 경우 서비스 신청번호를 화면에서 불러온 것을
                                         // 사용해야 하므로...
                model.put("SS_SVC_NO", "");
            }
            map.put("LIST", dao.list(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));

        }

        return map;
    }

   
    /**
     * Method ID : detail
     * Method 설명 : 사용자관리 상세 조회
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detail(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        Map<String, Object> mapPswd = new HashMap<String, Object>();
        try {
               mapPswd = (Map)dao.detailPswd(model);            
               model.put("PSWD_T", DESUtil.dataDecrypt((String)mapPswd.get("USER_PASSWORD")));
               model.put("PSWD", DESUtil.dataEncrypt((String)model.get("PSWD_T")));              
               map.put("DETAIL", dao.detail(model));
               
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    public Map<String, Object> ptnrCmpyInfo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            map.put("DETAIL", dao.detailPtnr(model));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * 대체 Method ID : insert 대체 Method 설명 : 사용자관리 등록 작성자 : Nam Yun Sung(남윤성)
     * 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> insert(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            model.put("PSWD", StringUtil.encryptPassword((String)model.get("PSWD")));
            String isuser = dao.userIdCheck(model);
            if ("0".equals(isuser.substring(0, 1))) {
                // 1.사용자정보를 등록한다.
                dao.insert(model);

                // 2.사용자아이디에 대한 사용자별윈도우권한(USER_WINDOW_ROLE)을 등록한다.
                dao.insertUserWindowRole(model);

                map.put("DETAIL", dao.detail(model));
                map.put("MSG", MessageResolver.getMessage("insert.success"));
            } else {
                map.put("MSG", "사용자ID " + isuser.substring(1) + " 은/는 이미등록된 ID입니다. 다른 ID를 선택하십시요.");
            }
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("insert.error"));
        }
        return map;
    }

    /**
     * 대체 Method ID : udpate 대체 Method 설명 : 사용자관리 수정 작성자 : Nam Yun Sung(남윤성)
     * 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> update(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            String orgUserType = (String)model.get("ORG_USER_TYPE");
            String userType = (String)model.get("USER_TYPE");
            String svcNo = (String)model.get("SVC_NO");
            String userId = (String)model.get("USER_ID");

            String pswd = dao.pwdCheck(model);
            if (!pswd.equals(model.get("PSWD"))) {
                model.put("PSWD", StringUtil.encryptPassword((String)model.get("PSWD")));
            }

            // I. 사용자타입이 변경 되었을 때
            if (!orgUserType.equals(userType)) {
                // 1.사용자아이디에 대한 사용자별윈도우권한(USER_WINDOW_ROLE)을 삭제한다.
                dao.deleteUserWindowRole(model);

                // 2.사용자정보 변경
                /* 반환값에 대한 별도 로직 없음 */
                int cnt = dao.update(model);

                // 3.사용자아이디에 대한 사용자별윈도우권한(USER_WINDOW_ROLE)을 등록한다.
                dao.insertUserWindowRole(model);

                // II. 사용자타입이 변경되지 않았을 경우
            } else {
                // 1. 사용자정보 변경
            	/* 반환값에 대한 별도 로직 없음 */
                int cnt = dao.update(model);
            }

            model.put("SVC_NO", svcNo);
            model.put("USER_ID", userId);

            map.put("DETAIL", dao.detail(model));
            map.put("MSG", MessageResolver.getMessage("save.success"));
        } catch (Exception e) {
        	if (loger.isErrorEnabled()) {
        		loger.error("Error occured in Update :" + e);
        	}
            map.put("MSG", MessageResolver.getMessage("save.error"));
        }

        return map;
    }

    /**
     * 대체 Method ID : delete 대체 Method 설명 : 사용자관리 삭제 작성자 : Nam Yun Sung(남윤성)
     * 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> delete(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {
            // 1.사용자아이디에 대한 사용자별윈도우권한(USER_WINDOW_ROLE)을 삭제한다.
            dao.deleteUserWindowRole(model);

            // 2.사용자정보를 삭제한다.
            /* 반환값에 대한 별도 로직 없음 */
            int cnt = dao.delete(model);
            map.put("MSG", MessageResolver.getMessage("delete.success"));
        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("delete.error"));
        }
        return map;
    }

    /**
     * 대체 Method ID : detailRoll 대체 Method 설명 : 로그인 시 첫페이지 권한조회 작성자 : choi
     * 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> detailRoll(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();
        try {

            String windowRoll = dao.detailRoll(model);
            map.put("windowRoll", windowRoll);

        } catch (Exception e) {
            loger.error(e.toString());
            map.put("MSG", MessageResolver.getMessage("list.error"));
        }
        return map;
    }

    /**
     * 대체 Method ID : stndHsCatgComboBox 대체 Method 설명 : HS_VER 콤보박스 작성자 : KimHT
     * 
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> selectHsCatgCombo(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        // HS_VER 콤보박스
        map.put("HSCOMBO", dao.stndHsCatgCombo(model));

        return map;
    }

    
    /**
     * Method ID : loginUpdate
     * Method 설명 : 로그인시 사용자 로그정보 업데이트 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateLogin(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        dao.loginUpdate(model); // 접속 Y 업데이트
        dao.loginInsert(model); // 접속 히스토리 생성

        return map;
    }
    
    /**
     * Method ID : logoutUpdate
     * Method 설명 : 로그인아웃시 사용자 로그정보 업데이트 
     * 작성자 : 기드온
     * @param model
     * @return
     * @throws Exception
     */
    public Map<String, Object> updateLogout(Map<String, Object> model) throws Exception {
        Map<String, Object> map = new HashMap<String, Object>();

        dao.logoutUpdate(model); // 접속 N 업데이트  

        return map;
    }
}
