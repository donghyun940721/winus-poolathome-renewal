package com.logisall.winus.meta.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;


@Repository("metaUserRoleDao")
public class MetaUserRoleDao extends SqlMapAbstractDAO {
	
	protected Log log = LogFactory.getLog(this.getClass());

	/**
	 * 
	 * Method ID   : list
	 * Method 설명 : 사용자정보리스트를 조회한다.
	 * 작성자      : Administrator
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("user_info.list", model);
	}
	
	/**
	 * 
	 * Method ID   : listDetail
	 * Method 설명 : 사용자에 대한 윈도우 권한리스트를 조회한다.
	 * 작성자      : Administrator
	 * @param model
	 * @return
	 */
	public Object listDetail(Map<String, Object> model) {
        GenericResultSet wqrs = new GenericResultSet();
        wqrs.setList(executeQueryForList("user_window_role.list", model));
        return wqrs;
	}
	
	/**
	 * 
	 * Method ID   : insertDetail
	 * Method 설명 : 사용자에 대한 윈도우 권한을 등록한다.
	 * 작성자      : Administrator
	 * @param model
	 * @return
	 */
	public Object insertDetail(Map<String, Object> model) {
		return executeInsert("user_window_role.insert", model);
	}

	/**
	 * 
	 * Method ID   : updateDetail
	 * Method 설명 : 사용자에 대한 윈도우 권한을 저장한다.
	 * 작성자      : Administrator
	 * @param model
	 * @return
	 */
	public int updateDetail(Map<String, Object> model) {
		return executeUpdate("user_window_role.update", model);
	}

	/**
	 * 
	 * Method ID   : deleteDetail
	 * Method 설명 : 사용자에 대한 윈도우 권한을 삭제한다.
	 * 작성자      : Administrator
	 * @param model
	 * @return
	 */
	public int deleteDetail(Map<String, Object> model) {
		return executeDelete("user_window_role.delete", model);
	}

}
