package com.logisall.winus.meta.service.impl;

import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

import com.m2m.jdfw5x.egov.database.GenericResultSet;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("metaUserDao")
public class MetaUserDao extends SqlMapAbstractDAO {
	protected Log log = LogFactory.getLog(this.getClass());
	
	/**
	 * 
	 * Method ID   : list
	 * Method 설명 : 사용자관리 리스트 조회
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public GenericResultSet list(Map<String, Object> model) {
		return executeQueryPageWq("user_info.list", model);
	}


	/**
	 * Method ID : detail
	 * Method 설명 : 사용자관리 상세 조회
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object detail(Map<String, Object> model) {
		return executeView("tmsys090.detail", model);
	}
	
	
	/**
	 * Method ID : detailPswd
	 * Method 설명 : 사용자 패스워드 조회(관세사 관리모드용)
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public Object detailPswd(Map<String, Object> model) {
		return executeView("tmsys090.detailPswd", model);
	}
	
	/**
	 * 
	 * Method ID   : detailPtnr
	 * Method 설명 : 협력업체 정보
	 * 작성자      : Park Tae Young(박태영)
	 * @param model
	 * @return
	 */
	public Object detailPtnr(Map<String, Object> model) {
		return executeView("biz_partner.detail", model);
	}


	/**
	 * 
	 * Method ID   : insert
	 * Method 설명 : 사용자관리 등록
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public Object insert(Map<String, Object> model) {
		return executeInsert("user_info.insert", model);
	}

	/**
	 * 
	 * Method ID   : update
	 * Method 설명 : 사용자관리 수정
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public int update(Map<String, Object> model) {
		return executeUpdate("user_info.update", model);
	}

	/**
	 * 
	 * Method ID   : delete
	 * Method 설명 : 사용자관리 삭제
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public int delete(Map<String, Object> model) {
		return executeDelete("user_info.delete", model);
	}
	
	/**
	 * 
	 * Method ID   : insert
	 * Method 설명 : 사용자별 윈도우권한관리 등록
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public Object insertUserWindowRole(Map<String, Object> model) {
		return executeInsert("user_window_role.insertUserID", model);
	}

	/**
	 * 
	 * Method ID   : delete
	 * Method 설명 : 사용자별 윈도우권한관리 삭제
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public int deleteUserWindowRole(Map<String, Object> model) {
		return executeDelete("user_window_role.deleteUserID", model);
	}

	/**
	 * 
	 * Method ID   : pwdCheck
	 * Method 설명 : 비밀번호 체크
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public String pwdCheck(Map<String, Object> model) {
		return (String)executeView("user_info.pwdCheck", model);
	}
	
	/**
	 * 
	 * Method ID   : userIdCheck
	 * Method 설명 : 사용자ID 체크
	 * 작성자      : Nam Yun Sung(남윤성)
	 * @param model
	 * @return
	 */
	public String userIdCheck(Map<String, Object> model) {
		return (String)executeView("user_info.userIdCheck", model);
	}

	/**
	 * 
	 * Method ID   : detailRoll
	 * Method 설명 : 로그인 시 첫페이지 권한조회
	 * 작성자      : choi
	 * @param model
	 * @return
	 */
	public String detailRoll(Map<String, Object> model) {
		return (String)executeView("user_info.detailRoll", model);
	}
	
	public GenericResultSet stndHsCatgCombo(Map<String, Object> model) {
		return executeQueryPageWq("hs_mst.stndHsCatgCombo", model);
	}
	

	/**
	 * Method ID : loginUpdate
	 * Method 설명 : 로그인시 사용자 정보 업데이트(접속 Y)
	 * 작성자 : 기드온
	 * @param model
	 * @return
	 */
	public int loginUpdate(Map<String, Object> model) {
		return executeUpdate("tmsys090.loginUpdate", model);
	}	
	
	/**
     * Method ID : logoutUpdate
     * Method 설명 : 로그인아웃시 사용자 정보 업데이트(접속 N)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public int logoutUpdate(Map<String, Object> model) {
        return executeUpdate("tmsys090.logoutUpdate", model);
    }
    
    /**
     * Method ID : loginInsert
     * Method 설명 : 로그인시 사용자 정보 업데이트(로그정보 관련)
     * 작성자 : 기드온
     * @param model
     * @return
     */
    public int loginInsert(Map<String, Object> model) {
        return executeUpdate("tmsys099.loginInsert", model);
    }   
}
