package com.logisall.winus.ulndev;

import java.awt.Color;
import java.io.BufferedOutputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jasperreports.engine.data.JsonData;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.ExtendedColor;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.xssf.streaming.SXSSFCell;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonFactory;

@RestController
@RequestMapping("/Excel")
public class ExcelController {
	
	protected Log log = LogFactory.getLog(this.getClass());
	
	/*
	 * map = {
	 * 		"Title" : excelFile title
	 * 		"Body"	: excelBody List
	 * 		"Head"	: excelHead List
	 * 		"Keys"	: BodyKey List
	 * 	}
	 */
	
	@RequestMapping(value = "/downLoadTemplate.action", method=RequestMethod.POST)
	@ResponseBody
	public void downLoadTemplate(HttpServletResponse response,@RequestParam Map<String,Object> request) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		BufferedOutputStream bos = null;
		SXSSFWorkbook sxssfWorkbook = null;		
		try{
			JSONObject data = new JSONObject(request.get("JsonData").toString());
			String title = data.getString("Title");
			String sheet = null;
			if(!data.isNull("Sheet")){
				sheet = data.getString("Sheet");
			}
			map.put("Sheet", sheet);
			JSONArray head = new JSONArray(data.get("Head").toString());
			JSONArray body = new JSONArray(data.get("Body").toString());
			JSONArray keys = new JSONArray(data.get("Keys").toString());
			List headList = new ArrayList<String>();
			for(int i = 0 ; i < head.length(); i++){
				headList.add(head.get(i).toString());
			}
			map.put("Head", headList);
			
			List bodyList = new ArrayList<ArrayList<String>>();
			for (int i = 0 ; i < body.length(); i++){
				List tempList = new ArrayList<String>();
				JSONObject tempBody = new JSONObject(body.get(i).toString());
				for (int j = 0 ; j < keys.length(); j++){
					if(!tempBody.isNull(keys.getString(j))){
						tempList.add(tempBody.get(keys.getString(j)));
					}
					else{
						tempList.add(null);
					}
				}
				bodyList.add(tempList);
			}
			map.put("Body", bodyList);
			map.put("isHeaderSpace", request.get("isHeaderSpace"));
			sxssfWorkbook = getWorkbook(map);
			
			response.reset();
			response.setHeader("Content-Disposition", "attachment;filename="
					+ URLEncoder.encode(title, "UTF-8") + ".xlsx;");
			response.setHeader("Content-Transfer-Encoding","binary;");
			response.setContentType("application/download;charset=UTF-8");
			
			bos = new BufferedOutputStream(response.getOutputStream());
			
			sxssfWorkbook.write(bos);
			
			bos.flush();
			
		}catch(Exception e){
			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
				e.printStackTrace();
				log.error("download fail");
			}
			
		}finally{
			if(sxssfWorkbook != null){
				sxssfWorkbook.close();
				sxssfWorkbook.dispose();
			}
			if(bos != null){
				try{
					bos.close();
				}catch(Exception e){
					throw e;
				}
			}
		}
		
	}
	
	/*
	 * map = {
	 * 		"Title" : excelFile title
	 * 		"Body"	: excelBody List
	 * 		"Head"	: excelHead List
	 * 	}
	 */
	@RequestMapping(value = "/downLoadExcelReport.action", method=RequestMethod.POST)
	@ResponseBody
	public void downLoadExcelReport(HttpServletResponse response,@RequestParam Map<String,Object> request) throws Exception{
		Map<String,Object> map = new HashMap<String,Object>();
		BufferedOutputStream bos = null;
		SXSSFWorkbook sxssfWorkbook = null;		
		try{
			JSONObject data = new JSONObject(request.get("JsonData").toString());
			String title = data.getString("Title");
			String sheet = null;
			if(!data.isNull("Sheet")){
				sheet = data.getString("Sheet");
			}
			map.put("Sheet", sheet);
			JSONArray head = new JSONArray(data.get("Head").toString());
			JSONArray body = new JSONArray(data.get("Body").toString());
			
			List headList = new ArrayList<String>();
			for(int i = 0 ; i < head.length(); i++){
				headList.add(head.get(i).toString());
			}
			map.put("Head", headList);
			
			List bodyList = new ArrayList<ArrayList<String>>();
			for (int i = 0 ; i < body.length(); i++){
				List tempList = new ArrayList<String>();
				JSONArray tempBody = new JSONArray(body.get(i).toString());
				for (int j = 0 ; j < tempBody.length(); j++){
					if(!tempBody.isNull(j)){
						tempList.add(tempBody.get(j));
					}
					else{
						tempList.add(null);
					}
				}
				bodyList.add(tempList);
			}
			map.put("Body", bodyList);
			map.put("isHeaderSpace", request.get("isHeaderSpace"));
			sxssfWorkbook = getWorkbook(map);
			
			response.reset();
			response.setHeader("Content-Disposition", "attachment;filename="
					+ URLEncoder.encode(title, "UTF-8") + ".xlsx;");
			response.setHeader("Content-Transfer-Encoding","binary;");
			response.setContentType("application/download;charset=UTF-8");
			
			bos = new BufferedOutputStream(response.getOutputStream());
			
			sxssfWorkbook.write(bos);
			
			bos.flush();
			
		}catch(Exception e){
			if(!e.getClass().getName().equals("org.apache.catalina.connector.ClientAbortException")){
				e.printStackTrace();
				log.error("download fail");
			}
			
		}finally{
			if(sxssfWorkbook != null){
				sxssfWorkbook.close();
				sxssfWorkbook.dispose();
			}
			if(bos != null){
				try{
					bos.close();
				}catch(Exception e){
					throw e;
				}
			}
		}
		
	}
	
	public SXSSFWorkbook getWorkbook(Map<String,Object> map){
		SXSSFWorkbook sxssfWorkbook = null;
		SXSSFSheet sxssfSheet = null;
		SXSSFRow sxssfRow = null;
		SXSSFCell sxssfCell = null;
		Boolean isHeaderSpace = false;
		String sheetName = (String) map.get("Sheet");
		if(map.get("isHeaderSpace")!=null && map.get("isHeaderSpace").toString().equals("true")){
			isHeaderSpace = true;
		}
		int rowNo = 0;
		try{
			List head = (List) map.get("Head");
			List<List<String>> body = (List<List<String>>) map.get("Body");
			sxssfWorkbook = new SXSSFWorkbook();
			if (sheetName != null){
				sxssfSheet = sxssfWorkbook.createSheet(sheetName);
			}
			else{
				sxssfSheet = sxssfWorkbook.createSheet();
			}
			sxssfRow = sxssfSheet.createRow(rowNo++);
			if (isHeaderSpace){
				sxssfRow = sxssfSheet.createRow(rowNo++);
			}
			Font titleFont = sxssfWorkbook.createFont();
			titleFont.setBold(true);
			titleFont.setFontHeightInPoints((short)10);
			
			Font bodyFont = sxssfWorkbook.createFont();
			bodyFont.setFontHeightInPoints((short)10);
			
			XSSFCellStyle cellStyle = (XSSFCellStyle) sxssfWorkbook.createCellStyle();
			CellStyle cellBodyStyle = sxssfWorkbook.createCellStyle();
			cellStyle.setBorderBottom(BorderStyle.THIN);
			cellStyle.setBorderTop(BorderStyle.THIN);
			cellStyle.setBorderRight(BorderStyle.THIN);
			cellStyle.setBorderLeft(BorderStyle.THIN);
			cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
			cellBodyStyle.cloneStyleFrom(cellStyle);
			if (isHeaderSpace){
				cellStyle.setBorderBottom(BorderStyle.NONE);
				cellStyle.setBorderTop(BorderStyle.NONE);
				cellStyle.setBorderRight(BorderStyle.NONE);
				cellStyle.setBorderLeft(BorderStyle.NONE);
				XSSFColor color = new XSSFColor(Color.decode("#E9E9E9"));
				cellStyle.setFillForegroundColor(color);
				cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			}
			cellStyle.setAlignment(HorizontalAlignment.CENTER);
			cellStyle.setFont(titleFont);
			cellBodyStyle.setFont(bodyFont);
			for(int i = 0; i < head.size(); i++){
				sxssfCell = sxssfRow.createCell((short) i);
				sxssfCell.setCellStyle(cellStyle);
				sxssfCell.setCellValue(head.get(i).toString());
			}
			int[] headWidth = new int[head.size()];
			for (int i = 0; i < head.size(); i++){
				headWidth[i] = head.get(i).toString().length();
			}
			for(int i = 0; i <body.size();i++){
				sxssfRow = sxssfSheet.createRow(rowNo++);
				for(int j = 0 ; j < body.get(i).size(); j++){
					sxssfCell = sxssfRow.createCell((short) j);
					sxssfCell.setCellStyle(cellBodyStyle);
					sxssfCell.setCellValue(body.get(i).get(j));
				}
			}
			
			sxssfSheet.trackAllColumnsForAutoSizing();
			for (int i = 0; i < head.size(); i++){
				sxssfSheet.autoSizeColumn(i);
				sxssfSheet.setColumnWidth(i, Math.max(sxssfSheet.getColumnWidth(i), (int)Math.round(headWidth[i]*1.8*256))+284);		// max(i,j)+284: max(i or j)+padding
			}
		}catch(Exception e){
			throw e;
		}

		return sxssfWorkbook;
	}

}
