package com.logisall.winus.ulndev;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.ibatis.sqlmap.client.SqlMapClient;
import com.m2m.jdfw5x.egov.database.SqlMapAbstractDAO;

@Repository("TaskDAO")
public class TaskDAO extends SqlMapAbstractDAO {
	
	public Map<String,Object> getRequestList(Map<String,Object> map)throws Exception{
		Map<String,Object> res = new HashMap<String,Object>();
		try{
			
			int count = 0;
			
			List list;
			Map<String,Object> tempMap = new HashMap<String,Object>();

			int row = Integer.parseInt(map.get("rows").toString());
			int page = Integer.parseInt(map.get("page").toString());
			
			if(row == -1){
				row = count;
				page = 1;
			}


			if (map.get("sidx") == ""){
				tempMap.put("sidx", "REQUEST_PK");
				tempMap.put("sord", "DESC");
			}
			else{
				tempMap.put("sidx", map.get("sidx"));
				tempMap.put("sord", map.get("sord"));
			}
			tempMap.put("row1", row * (page-1));
			tempMap.put("row2", row * (page));
			Boolean searchFlag = Boolean.parseBoolean(map.get("search").toString());
			if(searchFlag == true ){
				tempMap.put("request_name", map.get("request_name"));
				tempMap.put("request_start_date", map.get("request_start_date"));
				tempMap.put("request_end_date", map.get("request_end_date"));
				tempMap.put("process_name", map.get("process_name"));
				tempMap.put("process_start_date", map.get("process_start_date"));
				tempMap.put("process_end_date", map.get("process_end_date"));
				tempMap.put("request_state", map.get("request_state"));
				
				List searchProcessList;
				if (map.get("process_name")=="" && (map.get("process_start_date")=="" && map.get("process_end_date")=="")){
					searchProcessList = null;
				}
				else{
					searchProcessList = executeQueryForList("wmsys250.getSearchRequestPK",tempMap);
				}
				tempMap.put("searched_request_pk",searchProcessList);
				
				Map<String,Object> countMap = (Map<String,Object>) executeQueryForObject("wmsys250.getSearchRequestListCount", tempMap);
				count = Integer.parseInt(countMap.get("COUNT").toString());
				list = executeQueryForList("wmsys250.getSearchRequestList", tempMap);
				
			}
			else{
				Map<String,Object> countMap = (Map<String,Object>) executeQueryForObject("wmsys250.getRequestListCount", null);
				count = Integer.parseInt(countMap.get("COUNT").toString());
				list = executeQueryForList("wmsys250.getRequestList", tempMap);
			}
			int total = 0;
			if(row > 0){
				total = (count/row) + 1 ;
			}
			res.put("total", total);
			res.put("records", count);
			res.put("rows", list);
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return res;
	}
	
	public List getProcessList(Map<String,Object> map) throws Exception{

		List list;
		try{
			Map<String,Object> tempMap = new HashMap<String,Object>();
			tempMap.put("request_pk", map.get("requestId"));
			if (map.get("sidx") == ""){
				tempMap.put("sidx", "PROCESS_PK");
				tempMap.put("sord", "DESC");
			}
			else{
				tempMap.put("sidx", map.get("sidx"));
				tempMap.put("sord", map.get("sord"));
			}
			Boolean searchFlag = Boolean.parseBoolean(map.get("search").toString());
			if(searchFlag == true){
				tempMap.put("request_name", map.get("request_name"));
				tempMap.put("request_start_date", map.get("request_start_date"));
				tempMap.put("request_end_date", map.get("request_end_date"));
				tempMap.put("process_name", map.get("process_name"));
				tempMap.put("process_start_date", map.get("process_start_date"));
				tempMap.put("process_end_date", map.get("process_end_date"));
				tempMap.put("request_state", map.get("request_state"));
				list = executeQueryForList("wmsys250.getSearchProcessList", tempMap);
			}
			else{
				list = executeQueryForList("wmsys250.getProcessList", tempMap);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}
		return list;
	}
	
	public Boolean postRequestList(Map<String,String> map) throws Exception{
		SqlMapClient sqlMapClient = getSqlMapClient();
		try{
			sqlMapClient.startTransaction();
			sqlMapClient.insert("wmsys250.postRequest",map);
			sqlMapClient.endTransaction();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;			
		}finally{
			if(sqlMapClient != null){
				sqlMapClient.endTransaction();
			}
		}
	}
	public Boolean postProcessList(Map<String,String> map) throws Exception{
		SqlMapClient sqlMapClient = getSqlMapClient();
		try{
			sqlMapClient.startTransaction();
			sqlMapClient.insert("wmsys250.postProcess",map);
			sqlMapClient.endTransaction();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;			
		}finally{
			if(sqlMapClient != null){
				sqlMapClient.endTransaction();
			}
		}
	}
	public Boolean putRequestList(Map<String,Object>map)throws Exception{
		SqlMapClient sqlMapClient = getSqlMapClient();
		try{
			sqlMapClient.startTransaction();
			sqlMapClient.update("wmsys250.putRequest",map);
			sqlMapClient.endTransaction();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;			
		}finally{
			if(sqlMapClient != null){
				sqlMapClient.endTransaction();
			}
		}
	}
	public Boolean putProcessList(Map<String,Object>map)throws Exception{
		SqlMapClient sqlMapClient = getSqlMapClient();
		try{
			sqlMapClient.startTransaction();
			sqlMapClient.update("wmsys250.putProcess",map);
			sqlMapClient.endTransaction();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;			
		}finally{
			if(sqlMapClient != null){
				sqlMapClient.endTransaction();
			}
		}
	}
	public Boolean deleteRequestList(Map<String,Object>map)throws Exception{
		SqlMapClient sqlMapClient = getSqlMapClient();
		try{
			sqlMapClient.startTransaction();
			sqlMapClient.delete("wmsys250.deleteRequestP",map);
			sqlMapClient.delete("wmsys250.deleteRequestR",map);
			sqlMapClient.endTransaction();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			if(sqlMapClient != null){
				sqlMapClient.endTransaction();
			}
		}
	}
	public Boolean deleteProcessList(Map<String,Object>map)throws Exception{
		SqlMapClient sqlMapClient = getSqlMapClient();
		try{
			sqlMapClient.startTransaction();
			sqlMapClient.delete("wmsys250.deleteProcess",map);
			sqlMapClient.endTransaction();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;			
		}finally{
			if(sqlMapClient != null){
				sqlMapClient.endTransaction();
			}
		}
	}
	public Boolean deleteSelectList(List<Map<String,Object>> mapList)throws Exception{
		SqlMapClient sqlMapClient = getSqlMapClient();
		try{
			List<Map<String, Object>> requestList = new ArrayList<Map<String, Object>>();
			List<Map<String, Object>> processList = new ArrayList<Map<String, Object>>();
			for (int i = 0 ; i < mapList.size() ; i++){
				if(mapList.get(i) != null){
					Map<String,Object> tempMap = new HashMap<String,Object>();
					if(mapList.get(i).get("process_pk") != null){
						tempMap.put("request_pk", mapList.get(i).get("request_pk"));
						tempMap.put("process_pk", mapList.get(i).get("process_pk"));
						processList.add(tempMap);
					}
					else{
						tempMap.put("request_pk", mapList.get(i).get("request_pk"));
						requestList.add(tempMap);
					}
				}
			}
			sqlMapClient.startTransaction();
			if (requestList.size()!=0){
				sqlMapClient.delete("wmsys250.deleteSelectRequestListP",requestList);
				sqlMapClient.delete("wmsys250.deleteSelectRequestListR",requestList);
			}
			if (processList.size()!=0){
				sqlMapClient.delete("wmsys250.deleteSelectProcessList",processList);				
			}
			sqlMapClient.endTransaction();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;			
		}finally{
			if(sqlMapClient != null){
				sqlMapClient.endTransaction();
			}
		}
	}
	
}
