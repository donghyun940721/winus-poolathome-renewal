package com.logisall.winus.ulndev;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import com.logisall.winus.ulndev.TaskService;
import com.m2m.jdfw5x.egov.service.AbstractServiceImpl;

@Service("TaskService")
public class TaskServiceImpl extends AbstractServiceImpl implements TaskService{
	
	@Resource(name = "TaskDAO")
	private TaskDAO dao;

	@Override
	public Map<String,Object> getRequestList(Map<String,Object> map) throws Exception {
		return dao.getRequestList(map);
	}

	@Override
	public List<Map<String, String>> getProcessList(Map<String,Object> map)
			throws Exception {
		return dao.getProcessList(map);
	}

	@Override
	public Boolean postRequest(Map<String,String> map) throws Exception{
		return dao.postRequestList(map);
	}
	@Override
	public Boolean postProcess(Map<String,String> map) throws Exception{
		return dao.postProcessList(map);
	}

	@Override
	public Boolean putRequest(Map<String,Object> map)throws Exception{
		return dao.putRequestList(map);
	}
	@Override
	public Boolean putProcess(Map<String,Object> map)throws Exception{
		return dao.putProcessList(map);
	}
	
	@Override
	public Boolean deleteRequest(Map<String,Object> map)throws Exception{
		return dao.deleteRequestList(map);
	}
	
	@Override
	public Boolean deleteProcess(Map<String,Object> map)throws Exception{
		return dao.deleteProcessList(map);
	}
	
	@Override
	public Boolean deleteSelectList(List<Map<String,Object>> mapList)throws Exception{
		return dao.deleteSelectList(mapList);
	}
}
