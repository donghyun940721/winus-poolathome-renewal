package com.logisall.winus.ulndev;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ResponseBody;

public interface TaskService {
	
	public Map<String,Object> getRequestList(Map<String,Object> map) throws Exception;
	public List<Map<String,String>> getProcessList(Map<String,Object> map) throws Exception;
	
	public Boolean postRequest(Map<String,String> map)throws Exception;
	public Boolean postProcess(Map<String,String> map)throws Exception;
	
	public Boolean putRequest(Map<String,Object> map)throws Exception;
	public Boolean putProcess(Map<String,Object> map)throws Exception;
	
	public Boolean deleteRequest(Map<String,Object> map)throws Exception;
	public Boolean deleteProcess(Map<String,Object> map)throws Exception;
	public Boolean deleteSelectList(List<Map<String,Object>> mapList)throws Exception;
	
}
