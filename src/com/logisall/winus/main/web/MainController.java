package com.logisall.winus.main.web;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONValue;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.google.gson.Gson;
import com.logisall.winus.frm.common.util.CommonUtil;
import com.logisall.winus.frm.common.util.ConstantIF;
import com.logisall.winus.frm.common.util.LoginInfo;
import com.logisall.winus.frm.common.util.SessionListener;
import com.logisall.winus.frm.common.util.SessionMn;
import com.logisall.winus.meta.service.MetaUserRoleService;
import com.logisall.winus.tmsys.service.TMSYS030Service;
import com.m2m.jdfw5x.client.service.JDFWClientInfo;
import com.m2m.jdfw5x.egov.message.MessageResolver;
import com.m2m.jdfw5x.egov.servlet.ModelAndView;
import com.m2m.jdfw5x.util.SessionUtil;

@Controller
public class MainController {
	protected Log log = LogFactory.getLog(this.getClass());

	@Resource(name = "TMSYS030Service")
	private TMSYS030Service service;

	@Resource(name = "metaUserRoleService")
	private MetaUserRoleService service1;
	
	
	

	@RequestMapping("/login.action")
	public ModelAndView mn(Map<String, Object> model) {
		ModelAndView mav = null;
		// String sid = (String)model.get("SID");
		// if(sid == null){
		mav = new ModelAndView("winus/main/login");
		// }else{
		// mav = new ModelAndView("winus/common/comAlert");
		// }

		return mav; // 맨 앞에 / 없음에 주의, .vm 없음에 주의
	}

	@RequestMapping("/main.action")
	public ModelAndView main(HttpServletRequest request, Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			mav = new ModelAndView("winus/main/login");
			String langSetVal = (String) model.get("langSetVal");
			/* 로그아웃하지 않고 홈으로 넘어왔을경우 접속여부 및 세션 초기화 */
			model.put("USER_NO", SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
			SessionMn.init();
			SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
			SessionUtil.sessionClear(request);
			
			//사용자언어 초기셋팅
			SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, langSetVal);// 사용자언어
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to main process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "페이지 로딩에 실패했습니다.관리자에게 문의해주세요.");
			m.put("MSG", MessageResolver.getMessage("페이징로딩실패"));
		}
		if (mav != null)
			mav.addAllObjects(m);

		return mav;
	}

	@RequestMapping("/menuData.action")
	public ModelAndView menuData(HttpServletRequest request) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			HttpSession session = request.getSession(false);

			session.setAttribute(SessionLocaleResolver.LOCALE_SESSION_ATTRIBUTE_NAME, Locale.KOREAN);

			m = new HashMap<String, Object>();
			m.put("S_SVC_NO", session.getAttribute("SS_SVC_NO"));
			m.put("S_USER_ID", session.getAttribute("SS_USER_ID"));
			m.put("S_SVC_TYPE", session.getAttribute("SS_SVC_TYPE"));
			m.put("S_AUTH_CD", session.getAttribute("SS_USER_TYPE"));
			m.put("S_LANG", session.getAttribute("SS_LANG"));

			/* 메뉴정보 Session에서 가져오도록 수정 2015.07.13 */
			/* 프로그램관리에서 메뉴변경시 바로 적용안됨(다음 로그인 시 적용) */
			Map<String, Object> menuInfoMap = null;
			if (session.getAttribute("SS_MENU_INFO") != null) {
				menuInfoMap = (Map<String, Object>) session.getAttribute("SS_MENU_INFO");
			} else {
				menuInfoMap = service1.detail(m);
				session.setAttribute("SS_MENU_INFO", menuInfoMap);
			}
			mav = new ModelAndView("winus/common/menuData", menuInfoMap);

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get menu data :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "페이지 로딩에 실패했습니다.관리자에게 문의해주세요.");
			m.put("MSG", MessageResolver.getMessage("페이징로딩실패"));
		}
		if (mav != null)
			mav.addAllObjects(m);

		return mav;
	}

	@RequestMapping("/login_s.action")
	public ModelAndView login(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = null;
		Map<String, Object> m = null;
		Map<String, Object> user = null;
		ArrayList<Map<String, Object>> lcInfo = null;
		int check = 0;

		try {
			String serviceId = (String) model.get("svcid");
			String operationId = (String) model.get("opid");

			boolean isLoginRequest = "LOGIN".equals(serviceId) && "loginUserConfirm".equals(operationId);

			// 사전처리
			String mySystemId = "WINUS";
			String gvSystemId = "n/a";
			String gvUserAuth = "n/a";

			// 타 시스템 유저 검사
			// boolean isExternalUser = !mySystemId.equals(gvSystemId) ;

			// 외부 사용자 경우
			// if(isExternalUser &&
			// !BlackList.getInstance().canAccess(mySystemId, gvSystemId,
			// gvUserAuth, serviceId, operationId)){
			// appContext.setResultMessage(-200, "허용되지 않은 외부접속입니다");
			// }

			// 로그인 요청이면?
			if (isLoginRequest) {
				// log.info("로그인 모델 ::::::::::: "+model);
				if (null == model.get("USER_ID")) {
					return new ModelAndView("winus/main/login");
				}
				m = service.getDetail(model);
				// log.info("로그인정보:::"+m);

				user = (Map<String, Object>) m.get("DETAIL");
				// log.info("로그인 유저 정보:::"+user);
				if (user != null) {
					// 세션에 값 셋팅
					HttpSession session = request.getSession();
					// String user_ip = request.getRemoteAddr();
					// 인증에 성공한 경우 처리 해야 되는 부분
															//(String) user.get("CONNECT_YN")
					boolean chek = SessionListener.checkLogin((String) user.get("LOGIN_YN"), (String) user.get("MULTI_USE_GB"), (String) user.get("LAST_CONT_IP"), JDFWClientInfo.getClntIP(request));
					if (chek == true) {
						session.setAttribute("SS_USER_ID", user.get("USER_ID")); // 사용자ID
						session.setAttribute("SS_USER_NO", user.get("USER_NO")); // 사용자NO
						session.setAttribute("SS_USER_GB", user.get("USER_GB")); // 사용자구분
						session.setAttribute("SS_USER_NAME", user.get("USER_NM")); // 사용자이름
						session.setAttribute("SS_USER_TYPE", user.get("AUTH_CD")); // 사용자타입
						session.setAttribute("SS_SVC_NO", user.get("LC_ID")); // 서비스NO
						session.setAttribute("SS_SVC_NAME", user.get("LC_NM")); // 서비스이름
						session.setAttribute("SS_CMPY_CODE", user.get("CUST_CD"));// 사용자소속회사코드
						session.setAttribute("SS_CMPY_ID", user.get("CUST_ID"));// 사용자소속회사코드
						session.setAttribute("SS_CMPY_NAME", user.get("CUST_NM"));// 사용자소속회사명
						session.setAttribute("SS_SALES_CUST_ID", user.get("SALES_CUST_ID"));// 사용자소속회사명
						
						session.setAttribute("SS_LANG", user.get("LANG"));// 사용자언어
						session.setAttribute("SS_AUTH_LEVEL", user.get("AUTH_LEVEL").toString());// 레벨
						session.setAttribute("SS_CLIENT_CD", user.get("CLIENT_CD"));// 거래처(업체)코드
						session.setAttribute("SS_GIRD_NUM", user.get("USER_GRID_NUM"));// 그리드
						session.setAttribute("SS_SVC_USE_TGT", user.get("LC_USE_TGT")); // 물류센터운영대상
						session.setAttribute("SS_SVC_USE_TY", user.get("LC_USE_TY")); // 물류센터운영타입
						session.setAttribute("SS_LC_CHANGE_YN", user.get("LC_CHANGE_YN")); // 물류센터변경여부
						session.setAttribute("SS_ITEM_FIX_YN", user.get("ITEM_FIX_YN")); // 특정상품조회권한
						session.setAttribute("SS_ITEM_GRP_ID", user.get("ITEM_GRP_ID")); // 특정상품군
						session.setAttribute("SS_SUB_PASSWORD", user.get("SUB_PASSWORD")); // 기타 비밀번호
						session.setAttribute("SS_PROC_AUTH", user.get("PROC_AUTH")); // 저장처리권한
						session.setAttribute("SS_USE_TRANS_CUST", user.get("USE_TRANS_CUST")); // 저장처리권한
						session.setAttribute("SS_TEL_NO", user.get("TEL_NO")); // 전화번호
						session.setAttribute("SS_MULTI_USE_GB", user.get("MULTI_USE_GB")); // 사용자NO
						session.setAttribute("SS_STOCK_ALERT_YN", user.get("STOCK_ALERT_YN")); // 재고부족알림구분
						session.setAttribute("SS_LC_CUST_NM", user.get("LC_CUST_NM")); // 3PL 물류센터명
						
						session.setAttribute("SS_M_DO", ConstantIF.encAES("KEY2020031312345"));
						session.setAttribute("SS_DETAIL_PS", ConstantIF.encAES("DE2020031354321"));
						
						List<Map<String, Object>> listMenu = service.list_menu();
				    	Gson gson         = new Gson();
				    	Map<String, Object> map = new HashMap<String, Object>();
				    	
				    	for(int i = 0; i < listMenu.size(); i++){
				    		map.put(String.valueOf(listMenu.get(i).get("PROGRAM_ID")), listMenu.get(i).get("PROGRAM_NM"));
				    	}
						String jsonString = gson.toJson(map);
						jsonString = JSONValue.escape(jsonString);
						session.setAttribute("SS_MENU_MAP", jsonString);
						
																						// 검색
																						// 설정
						session.setAttribute("SS_SESSION_ID", request.getSession().getId());// 세션아이디
						session.setAttribute("SS_CLIENT_IP", JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						if ("KR".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_KOREAN);
						} else if ("EN".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_ENGLISH);
						} else if ("JA".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_JAPANESE);
						} else if ("CH".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_CHINESE);
						} else if ("VN".equals(user.get("LANG"))) {
							session.setAttribute("SS_LANG_TYPE", ConstantIF.LANGUAGE_TYPE_VIETNAM);
						}

						SessionUtil.setValue(request, ConstantIF.SS_USER_ID, user.get("USER_ID"));// 사용자ID
						SessionUtil.setValue(request, ConstantIF.SS_USER_NO, user.get("USER_NO"));// 사용자번호
						SessionUtil.setValue(request, ConstantIF.SS_USER_GB, user.get("USER_GB"));// 사용자구분
						SessionUtil.setValue(request, ConstantIF.SS_USER_NAME, user.get("USER_NM")); // 사용자이름
						SessionUtil.setValue(request, ConstantIF.SS_USER_TYPE, user.get("AUTH_CD"));// 사용자타입
						SessionUtil.setValue(request, ConstantIF.SS_SVC_NO, user.get("LC_ID"));// 서비스NO
						SessionUtil.setValue(request, ConstantIF.SS_SVC_NAME, user.get("LC_NM"));// 서비스D이름
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_CODE, user.get("CUST_CD"));// 사용자소속회사코드
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_ID, user.get("CUST_ID"));// 사용자소속회사코드
						SessionUtil.setValue(request, ConstantIF.SS_CMPY_NAME, user.get("CUST_NM"));// 사용자소속회사명
						SessionUtil.setValue(request, ConstantIF.SS_LANG, user.get("LANG"));// 사용자언어
						SessionUtil.setValue(request, ConstantIF.SS_AUTH_LEVEL, user.get("AUTH_LEVEL").toString());// 레벨
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_CD, user.get("CLIENT_CD"));// 거래처(업체)코드
						SessionUtil.setValue(request, ConstantIF.SS_GIRD_NUM, user.get("USER_GRID_NUM"));// 거래처(업체)코드
						SessionUtil.setValue(request, ConstantIF.SS_SVC_USE_TGT, user.get("LC_USE_TGT"));// 
						SessionUtil.setValue(request, ConstantIF.SS_SVC_USE_TY, user.get("LC_USE_TY"));// 
						SessionUtil.setValue(request, ConstantIF.SS_LC_CHANGE_YN, user.get("LC_CHANGE_YN"));// 물류센터변경여부
						SessionUtil.setValue(request, ConstantIF.SS_ITEM_FIX_YN, user.get("ITEM_FIX_YN"));// 특정상품조회권한
						SessionUtil.setValue(request, ConstantIF.SS_ITEM_GRP_ID, user.get("ITEM_GRP_ID"));// 특정상품군
						SessionUtil.setValue(request, ConstantIF.SS_TEL_NO, user.get("TEL_NO"));// 전화번호
						SessionUtil.setValue(request, ConstantIF.SS_SUB_PASSWORD, user.get("SUB_PASSWORD"));// 기타 비밀번호
						SessionUtil.setValue(request, ConstantIF.SS_PROC_AUTH, user.get("PROC_AUTH"));// 저장처리권한
						SessionUtil.setValue(request, ConstantIF.SS_USE_TRANS_CUST, user.get("USE_TRANS_CUST"));// 저장처리권한
						SessionUtil.setValue(request, ConstantIF.SS_MULTI_USE_GB, user.get("MULTI_USE_GB"));// 사용자번호
						SessionUtil.setValue(request, ConstantIF.SS_STOCK_ALERT_YN, user.get("STOCK_ALERT_YN"));// 재고부족알림구분
						SessionUtil.setValue(request, ConstantIF.SS_LC_CUST_NM, user.get("LC_CUST_NM"));// 3PL 물류센터명
						SessionUtil.setValue(request, ConstantIF.SS_SALES_CUST_ID, user.get("SALES_CUST_ID"));// 3PL 물류센터명
						
						
						SessionUtil.setValue(request, ConstantIF.SS_M_DO, ConstantIF.encAES("KEY2020031312345"));
						SessionUtil.setValue(request, ConstantIF.SS_DETAIL_PS, ConstantIF.encAES("DE2020031354321"));
						
						// SessionUtil.setValue(request,ConstantIF.SS_DEPT_NAME,
						// user.get("DEPT_NAME"));//사용자소속부서명
						// SessionUtil.setValue(request,ConstantIF.SS_DEGREE,
						// user.get("DEGREE"));//사용자직급명
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_IP, JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						SessionUtil.setValue(request, ConstantIF.SS_SESSION_ID, request.getSession().getId());// (클라이언트)접속IP

						SessionUtil.setValue(request, ConstantIF.SS_LOGIN_DATE, new Date());
						SessionUtil.setValue(request, ConstantIF.SS_LOGIN_SESSION_TIME, 86400);

						if (user.get("LANG").equals("KR")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_KOREAN);// 사용자언어
																													// 타입
																													// 한국어
						} else if (user.get("LANG").equals("EN")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_ENGLISH);// 사용자언어
																														// 타입
																														// 영어
						} else if (user.get("LANG").equals("JA")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_JAPANESE);// 사용자언어
																														// 타입
																														// 일본어
						} else if (user.get("LANG").equals("CH")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_CHINESE);// 사용자언어
																														// 타입
																														// 중국어
						} else if (user.get("LANG").equals("VN")) {
							SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, ConstantIF.LANGUAGE_TYPE_VIETNAM);// 사용자언어
																														// 타입
																														// 중국어
						}

						// CodeMngr.reload((String)user.get("LC_ID"));//시스템로그인
						// 성공시 해당 svcNo별 설정된 공통코드 메모리적재 2012-03-30 by indigo;
						check = 1;

						// 로그인 여부를 셋팅
						SessionMn.init();
						LoginInfo loginInfo = new LoginInfo();
						loginInfo.setId(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
						loginInfo.setLastRequestTime(Calendar.getInstance());
						SessionMn.getInstance().addInfo(loginInfo);

						// 로그인시 첫페이지 권한설정...
						model.put("SS_SVC_NO", user.get("LC_ID"));
						model.put("USER_NO", user.get("USER_NO"));
						model.put("USER_ID", user.get("USER_ID"));
						model.put("SS_SVC_USE_TGT", user.get("LC_USE_TGT"));
						model.put("SS_SVC_USE_TY", user.get("LC_USE_TY"));
						model.put("SS_LC_CHANGE_YN", user.get("LC_CHANGE_YN"));
						model.put("SS_ITEM_FIX_YN", user.get("ITEM_FIX_YN"));
						model.put("SS_ITEM_GRP_ID", user.get("ITEM_GRP_ID"));
						model.put("SS_TEL_NO", user.get("TEL_NO"));
						model.put("SS_SUB_PASSWORD", user.get("SUB_PASSWORD"));
						model.put("SS_PROC_AUTH", user.get("PROC_AUTH"));
						model.put("SS_USE_TRANS_CUST", user.get("USE_TRANS_CUST"));
						model.put("WORK_IP", SessionUtil.getStringValue(request, ConstantIF.SS_CLIENT_IP));
						model.put("Session_ID", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_SESSION_ID)));

						model.put("AUTH_CD", user.get("AUTH_CD"));
						model.put("PROGRAM_ID", "TMSYS030");

						m.put("windowRoll", service.getRoll(model));

						// 멀티 물류센터 처리
						lcInfo = (ArrayList<Map<String, Object>>) service.selectLc(model).get("LCINFO");
						session.setAttribute("SS_LC_INFO", lcInfo);
						// 접속여부 업데이트
						service.updateLogin(model);
						// 접속 히스토리
						service.updateLoginHistory(model);
					} else {
						check = 15;
						
						SessionUtil.setValue(request, ConstantIF.SS_CLIENT_IP, JDFWClientInfo.getClntIP(request));// (클라이언트)접속IP
						SessionUtil.setValue(request, ConstantIF.SS_SESSION_ID, request.getSession().getId());// (클라이언트)접속IP

						SessionMn.init();
						SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
						SessionUtil.sessionClear(request);
						
						// 로그인시 첫페이지 권한설정...
						model.put("USER_NO", user.get("USER_NO"));
						model.put("WORK_IP", SessionUtil.getStringValue(request, ConstantIF.SS_CLIENT_IP));
						model.put("Session_ID", CommonUtil.nullChk(SessionUtil.getStringValue(request, ConstantIF.SS_SESSION_ID)));

						// 접속여부 업데이트
						service.updateLogout(model);
						// 접속 히스토리
						service.updateLoginHistory(model);
					}
				} else {
					check = 0;
				}
			} // 로그인일 경우 끝
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to login process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "로그인에 실패했습니다.등록되어 있지 않는 사용자입니다.");
			m.put("MSG", MessageResolver.getMessage("로그인실패"));
			check = 0;
		}

		if (check == 1) {
			try {
				// mav = new ModelAndView("winus/main/main"); // 로그인 성공 시 메인
				// 화면으로 이동
				// mav = new ModelAndView("winus/tmsys/TMSY030Mn"); // 로그인 성공 시
				// 메인 화면으로 이동
				// mav = new ModelAndView("fta/stnd/stndHsCatgMn"); // HS검색 화면으로
				// 이동
				// mav = new ModelAndView("winus/stnd/stndHsCatgMn",
				// service.stndHsCatgCombo(model));
				model.put("USER_NO", SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
				String url = service.selectScreen(model);
				// System.out.println(url);
				if (url == null) {
					RequestDispatcher rd = request.getRequestDispatcher("/main/popmain.action");
					rd.forward(request, response);

				} else {
					mav = new ModelAndView("forward:" + url);
				}
				// String cls = (String)model.get("CLS");

				// if(cls == null){
				// mav = new ModelAndView("fta/stnd/stndHsCatgMn"); // HS검색 화면으로
				// 이동
				// mav = new ModelAndView("winus/stnd/stndHsCatgMn",
				// service.stndHsCatgCombo(model));
				// }else{
				// mav = new ModelAndView("winus/common/comAlert");
				// }
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to login process(forward) :", e);
				}
			}
		} else if (check == 15) {
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "이미 접속한 사용자가 있습니다.");
			m.put("MSG", MessageResolver.getMessage("이전에 비정상 종료로 로그아웃 되었습니다. 다시 로그인 하십시오."));
			
			mav = new ModelAndView("winus/main/login"); // 로그인 실패 시 다시 로그인 화면으로
														// 이동

		} else {
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "로그인에 실패했습니다.등록되어 있지 않는 사용자입니다.");
			m.put("MSG", MessageResolver.getMessage("로그인실패"));

			mav = new ModelAndView("winus/main/login"); // 로그인 실패 시 다시 로그인 화면으로
														// 이동

			// String cls = (String)model.get("CLS");
			// if(cls == null){
			// mav = new ModelAndView("winus/main/login"); // 로그인 실패 시 다시 로그인
			// 화면으로 이동
			// }else{
			// mav = new ModelAndView("winus/common/comAlert");
			// }
		}
		// System.out.println(model);
		if (mav != null)
			mav.addAllObjects(m);
		return mav;
	}

	@RequestMapping("/logout.action")
	public ModelAndView logout(HttpServletRequest request, Map<String, Object> model) {
	
		
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			String langSetVal = (String) model.get("langSetVal");
			
			SessionMn.init();
			SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
			SessionUtil.sessionClear(request);

			//사용자언어 초기셋팅
			SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, langSetVal);// 사용자언어
			
			mav = new ModelAndView("winus/main/login"); // 로그아웃 후에 로그인 화면으로
														// 돌아가도록 셋팅

		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to logout process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "로그아웃에 실패했습니다.관리자에게 문의해주세요.");
			m.put("MSG", MessageResolver.getMessage("로그아웃실패"));
		}
		if (mav != null)
			mav.addAllObjects(m);

		return mav;
	}

	@RequestMapping("/svcInfo.action")
	public ModelAndView svcInfo(HttpServletRequest request) {
		/*
			ModelAndView mav = null;
			Map<String, Object> m = null;
	
			try {
				HttpSession session = request.getSession(false);
	
				String msg = "";
				msg = "locales=";
				Enumeration locales = request.getLocales();
				boolean first = true;
				while (locales.hasMoreElements()) {
					Locale locale = (Locale) locales.nextElement();
					if (first)
						first = false;
					else
						msg = msg + ", ";
					msg = msg + locale.toString();
				}
				msg = "";
				Enumeration names = request.getParameterNames();
				while (names.hasMoreElements()) {
					String name = (String) names.nextElement();
					msg = msg + name + "=";
					String[] values = request.getParameterValues(name);
					for (int i = 0; i < values.length; i++) {
						if (i > 0)
							msg = msg + ", ";
						msg = msg + values[i];
					}
					log.info(msg);
				}
	
				Enumeration attributeEnum = request.getAttributeNames();
				while (attributeEnum.hasMoreElements()) {
					String name = (String) attributeEnum.nextElement();
					// log.info(name + "=" + request.getAttribute(name));
				}
	
				Cookie cookies[] = request.getCookies();
				if (cookies == null)
					cookies = new Cookie[0];
				for (int i = 0; i < cookies.length; i++) {
					// log.info(cookies[i].getName() + "=" + cookies[i].getValue());
				}
	
				Enumeration headerEnum = request.getHeaderNames();
				while (headerEnum.hasMoreElements()) {
					String name = (String) headerEnum.nextElement();
					// log.info(name + "=" + request.getHeader(name));
				}
	
				mav = new ModelAndView("winus/main/main"); // 메인화면으로 돌아가도록 셋팅
			} catch (Exception e) {
				if (log.isErrorEnabled()) {
					log.error("Fail to get sys Info :", e);
				}
			}
			if (mav != null)
				mav.addAllObjects(m);
		*/
		
		ModelAndView mav = null;
		try {
			mav = new ModelAndView("winus/main/main");
			
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to get sys Info :", e);
			}
		}
		return mav;
	}

	@RequestMapping("/svcChange.action")
	public ModelAndView svcChange(HttpServletRequest request, Map<String, Object> model) throws Exception {
		ModelAndView mav = new ModelAndView("jsonView");
		HttpSession session = request.getSession();
		ArrayList<Map<String, Object>> lcInfo = (ArrayList<Map<String, Object>>) session.getAttribute("SS_LC_INFO");
		String orgLcId = (String) session.getAttribute("SS_SVC_NO");
		String newLcId = request.getParameter("LC_ID");
		String fullPath = request.getParameter("_fullPath");
		mav.addObject("full_Path", fullPath);
		for (int i = 0; i < lcInfo.size(); i++) {
			String chkLcId = (String) lcInfo.get(i).get("LC_ID");
			if (orgLcId.equals(chkLcId)) {
				Map<String, Object> a = lcInfo.get(i);
				a.put("CONNECT_YN", "N");
				lcInfo.set(i, a);
			}

			if (newLcId != null && newLcId.equals(chkLcId)) {
				String langType = (String) request.getParameter("LANG");
				// 언어 업데이트 처리 2015-09-01
				service.saveLang(model);
				Map<String, Object> a = lcInfo.get(i);
				a.put("CONNECT_YN", "Y");
				lcInfo.set(i, a);
				session.setAttribute("SS_SVC_NO", lcInfo.get(i).get("LC_ID")); // 물류센터ID
				session.setAttribute("SS_SVC_NAME", lcInfo.get(i).get("LC_NM")); // 물류센터이름
				session.setAttribute(ConstantIF.SS_LANG_TYPE, langType); // 선택언어
				session.setAttribute("SS_SVC_USE_TGT", lcInfo.get(i).get("LC_USE_TGT")); // 물류센터운영대상
				session.setAttribute("SS_SVC_USE_TY", lcInfo.get(i).get("LC_USE_TY")); // 물류센터운영타입
				session.setAttribute("SS_LC_CHANGE_YN", lcInfo.get(i).get("LC_CHANGE_YN")); // 물류센터운영타입
				session.setAttribute("SS_ITEM_FIX_YN", lcInfo.get(i).get("ITEM_FIX_YN")); // 특정상품조회권한
				session.setAttribute("SS_ITEM_GRP_ID", lcInfo.get(i).get("ITEM_GRP_ID")); // 특정상품군
				session.setAttribute("SS_TEL_NO", lcInfo.get(i).get("TEL_NO")); // 전화번호
				session.setAttribute("SS_SUB_PASSWORD", lcInfo.get(i).get("SUB_PASSWORD")); // 기타 비밀번호
				session.setAttribute("SS_STOCK_ALERT_YN", lcInfo.get(i).get("STOCK_ALERT_YN")); // 재고부족알림구분
				session.setAttribute("SS_LC_CUST_NM", lcInfo.get(i).get("LC_CUST_NM")); // 3PL 물류센터명
				
				session.setAttribute("SS_M_DO", ConstantIF.encAES("KEY2020031312345"));
				session.setAttribute("SS_DETAIL_PS", ConstantIF.encAES("DE2020031354321"));
				
				session.setAttribute("SS_PROC_AUTH", lcInfo.get(i).get("PROC_AUTH")); // 
				SessionUtil.setValue(request, ConstantIF.SS_SVC_NO, lcInfo.get(i).get("LC_ID")); // 물류센터ID
				SessionUtil.setValue(request, ConstantIF.SS_SVC_NAME, lcInfo.get(i).get("LC_NM")); // 물류센터이름
				SessionUtil.setValue(request, ConstantIF.SS_LANG_TYPE, langType); // 물류센터이름
				SessionUtil.setValue(request, ConstantIF.SS_SVC_USE_TGT, lcInfo.get(i).get("LC_USE_TGT")); // 물류센터운영대상
				SessionUtil.setValue(request, ConstantIF.SS_SVC_USE_TY, lcInfo.get(i).get("LC_USE_TY")); // 물류센터운영타입
				SessionUtil.setValue(request, ConstantIF.SS_LC_CHANGE_YN, lcInfo.get(i).get("LC_CHANGE_YN")); // 물류센터운영타입
				SessionUtil.setValue(request, ConstantIF.SS_ITEM_FIX_YN, lcInfo.get(i).get("ITEM_FIX_YN")); // 특정상품조회권한
				SessionUtil.setValue(request, ConstantIF.SS_ITEM_GRP_ID, lcInfo.get(i).get("ITEM_GRP_ID")); // 특정상품군
				SessionUtil.setValue(request, ConstantIF.SS_TEL_NO, lcInfo.get(i).get("TEL_NO")); // 전화번호
				SessionUtil.setValue(request, ConstantIF.SS_SUB_PASSWORD, lcInfo.get(i).get("SUB_PASSWORD")); // 기타 비밀번호
				SessionUtil.setValue(request, ConstantIF.SS_STOCK_ALERT_YN, lcInfo.get(i).get("STOCK_ALERT_YN")); // 재고부족알림구분
				SessionUtil.setValue(request, ConstantIF.SS_LC_CUST_NM, lcInfo.get(i).get("LC_CUST_NM")); // 3PL 물류센터명
				
				SessionUtil.setValue(request, ConstantIF.SS_M_DO, ConstantIF.encAES("KEY2020031312345"));
				SessionUtil.setValue(request, ConstantIF.SS_DETAIL_PS, ConstantIF.encAES("DE2020031354321"));
				
				SessionUtil.setValue(request, ConstantIF.SS_PROC_AUTH, lcInfo.get(i).get("PROC_AUTH")); // 
			}
		}
		session.setAttribute("SS_LC_INFO", lcInfo);

		return mav;
	}
	
	@RequestMapping("/main/popmain.action")
	public ModelAndView detail(Map<String, Object> model) throws Exception {
		Map<String, Object> popInfo = service.getPopupInfo(model);
		return new ModelAndView("winus/common/tempblank", popInfo);
	}	

	@RequestMapping("/main/blank.action")
	public ModelAndView balankMain(Map<String, Object> model) throws Exception {
		Map<String, Object> popInfo = service.getPopupInfo(model);
		return new ModelAndView("winus/common/tempblank", popInfo);
	}
	/*
	@RequestMapping("/main/blank.action")
	public ModelAndView balankMain(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("winus/common/tempblank");
		return mav;
	}
	*/
	
	@RequestMapping("/errors/404.action")
	public ModelAndView goError(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("winus/common/error");
		return mav;
	}	
	
	@RequestMapping("/uFileUploadPage.action")
	public ModelAndView uFileUpload(HttpServletRequest request, Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			mav = new ModelAndView("winus/wmsct/uFileUpload");
			String custGb = (String) model.get("ufCustGb");
			String ascsId = (String) model.get("ufAscsId");
			/* 로그아웃하지 않고 홈으로 넘어왔을경우 접속여부 및 세션 초기화 */
			//model.put("USER_NO", SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
			//SessionMn.init();
			//SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
			SessionUtil.sessionClear(request);
			//사용자언어 초기셋팅
			SessionUtil.setValue(request, "FILE_CUST_GB", custGb);// 사용자언어
			SessionUtil.setValue(request, "FILE_ASCS_ID", ascsId);// 사용자언어
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to main process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "페이지 로딩에 실패했습니다.관리자에게 문의해주세요.");
			m.put("MSG", MessageResolver.getMessage("페이징로딩실패"));
		}
		if (mav != null)
			mav.addAllObjects(m);

		return mav;
	}
	
	@RequestMapping("/share_warehouse.action")
	public ModelAndView share_warehousr(HttpServletRequest request, Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			mav = new ModelAndView("winus/main/share_warehouse");
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to main process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "페이지 로딩에 실패했습니다.관리자에게 문의해주세요.");
			m.put("MSG", MessageResolver.getMessage("페이징로딩실패"));
		}
		if (mav != null)
			mav.addAllObjects(m);

		return mav;
	}
	
	@RequestMapping("/uDlvTrackingPage.action")
	public ModelAndView uDlvTracking(HttpServletRequest request, Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			mav = new ModelAndView("winus/wmsct/uDlvTracking");
			String dlvOrdId = (String) model.get("uDlvOrdId");
			/* 로그아웃하지 않고 홈으로 넘어왔을경우 접속여부 및 세션 초기화 */
			//model.put("USER_NO", SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
			//SessionMn.init();
			//SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
			SessionUtil.sessionClear(request);
			//사용자언어 초기셋팅
			SessionUtil.setValue(request, "DLV_ORD_ID", dlvOrdId);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to main process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "페이지 로딩에 실패했습니다.관리자에게 문의해주세요.");
			m.put("MSG", MessageResolver.getMessage("페이징로딩실패"));
		}
		if (mav != null)
			mav.addAllObjects(m);

		return mav;
	}

	@RequestMapping("/uDlvCsatPage.action")
	public ModelAndView uDlvCsat(HttpServletRequest request, Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			mav = new ModelAndView("winus/wmsct/uDlvCsat");
			String dlvOrdId = (String) model.get("uDlvOrdId");
			/* 로그아웃하지 않고 홈으로 넘어왔을경우 접속여부 및 세션 초기화 */
			//model.put("USER_NO", SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
			//SessionMn.init();
			//SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
			SessionUtil.sessionClear(request);
			//사용자언어 초기셋팅
			SessionUtil.setValue(request, "DLV_ORD_ID", dlvOrdId);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to main process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "페이지 로딩에 실패했습니다.관리자에게 문의해주세요.");
			m.put("MSG", MessageResolver.getMessage("페이징로딩실패"));
		}
		if (mav != null)
			mav.addAllObjects(m);

		return mav;
	}

	@RequestMapping("/uDlvAsCsatPage.action")
	public ModelAndView uDlvAsCsat(HttpServletRequest request, Map<String, Object> model) {
		ModelAndView mav = null;
		Map<String, Object> m = null;

		try {
			mav = new ModelAndView("winus/wmsct/uDlvAsCsat");
			String dlvOrdId = (String) model.get("uDlvOrdId");
			/* 로그아웃하지 않고 홈으로 넘어왔을경우 접속여부 및 세션 초기화 */
			//model.put("USER_NO", SessionUtil.getStringValue(request, ConstantIF.SS_USER_NO));
			//SessionMn.init();
			//SessionMn.getInstance().delInfo(SessionUtil.getStringValue(request, ConstantIF.SS_USER_ID));
			SessionUtil.sessionClear(request);
			//사용자언어 초기셋팅
			SessionUtil.setValue(request, "DLV_ORD_ID", dlvOrdId);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to main process :", e);
			}
			m = new HashMap<String, Object>();
			m.put("ERROR", "1");
			// m.put("MSG", "페이지 로딩에 실패했습니다.관리자에게 문의해주세요.");
			m.put("MSG", MessageResolver.getMessage("페이징로딩실패"));
		}
		if (mav != null)
			mav.addAllObjects(m);

		return mav;
	}
	
	@RequestMapping("/mobile/crossDomainHttpWs.action")
	public ModelAndView crossDomainHttpWs(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
		ModelAndView mav = new ModelAndView("jsonView");
		Map<String, Object> m = new HashMap<String, Object>();
		
		String cMethod		= request.getParameter("cMethod");
		String cUrl			= request.getParameter("cUrl");
		String jsonString	= request.getParameter("formData");
		model.put("cMethod"		, cMethod);
		model.put("cUrl"		, cUrl);
		model.put("jsonString"	, jsonString);
		
		try {
			m = service.crossDomainHttpWs(model);
		} catch (Exception e) {
			if (log.isErrorEnabled()) {
				log.error("Fail to save :", e);
			}
			m.put("MSG", MessageResolver.getMessage("save.error"));
		}
		mav.addAllObjects(m);
		return mav;
	}
	
	@RequestMapping("/common/OZreport8.action")
	public ModelAndView getOZReport8(Map<String, Object> model) {
		return new ModelAndView("winus/common/OZSimpleViewer");
	}
	
	@RequestMapping("/common/OZreport8PrintOnce.action")
	public ModelAndView getOZReport8PrintOnce(Map<String, Object> model) {
		return new ModelAndView("winus/common/OZSimpleViewerPrintOnce");
	}
}
