// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#292b2c';

// Area Chart Example
var ctx = document.getElementById("myAreaChart").getContext('2d');
var myLineChart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: ["5", "10", "15", "20", "25", "30"],
    datasets: [{
      label: "Sessions",
      lineTension: 0.3,
      backgroundColor: "rgba(105, 0, 132, .2)",
      borderColor: "rgba(200, 99, 132, .7)",
      pointRadius: 5,
      pointBackgroundColor: "rgba(105, 0, 132, .2)",
      pointBorderColor: "rgba(200, 99, 132, .7)",
      pointHoverRadius: 5,
      pointHoverBackgroundColor: "rgba(105, 0, 132, .2)",
      pointHitRadius: 50,
      pointBorderWidth: 2,
      data: [0, 0, 0, 0, 0, 0]
    },
    {
        label: "Sessions2",
        lineTension: 0.3,
        backgroundColor: "rgba(34,116,28, .2)",
        borderColor: "rgba(34,116,28, .7)",
        pointRadius: 5,
        pointBackgroundColor: "rgba(34,116,28, .2)",
        pointBorderColor: "rgba(34,116,28, .7)",
        pointHoverRadius: 5,
        pointHoverBackgroundColor: "rgba(34,116,28, .2)",
        pointHitRadius: 50,
        pointBorderWidth: 2,
        data: [0, 0, 0, 0, 0, 0]
      }
    ],
  },
  options: {
    scales: {
      xAxes: [{
        time: {
          unit: 'date'
        },
        gridLines: {
          display: false
        },
        ticks: {
          maxTicksLimit: 7
        }
      }],
      yAxes: [{
        ticks: {
          min: 0,
          max: 40000,
          maxTicksLimit: 5
        },
        gridLines: {
          color: "rgba(0, 0, 0, .125)",
        }
      }],
    },
    legend: {
      display: false
    }
  }
});