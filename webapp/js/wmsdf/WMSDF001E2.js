import { WinusGrid }                from '../utility/WinusGrid.js';
import { SessionKey, SessionUtil }  from '../utility/userStorageManger.js';
import { Component, COMP_VALUE_TYPE, COMP_CATEGORY} from '../component/contentFilter.js';

export class WMSDF001E2{

    #parcelCd       = null;
    #parcelSeq      = null;
    #custId         = null;
    #formId         = 'frm_listE2';


    /** UI Component */
    #compCust = new Component({
        code        : 'vrSrchCustCdE2',
        id          : 'vrSrchCustIdE2',
        name        : 'vrSrchCustNmE2',
        category    : COMP_CATEGORY.CUST
    });


    /** 그룹헤더 정보 */
    #headerMappingInfo = [
        {
            name: '항목순번',                     // 그룹헤더에 출력될 명칭
            code: 'SEQ_SORT',                 // 그룹헤더 Key값 (아직 사용저 없음)
            startColumnKey: 'SORT_NUM',       // 그룹지정 시작 열의 Key값
            endColumnKey: 'DOWN'            // 그룹지정 마지막 열의 Key값
        }
    ];

    #grid = new WinusGrid({ 
        spdListDiv          :   'spdListE2',                             
        gridTemplate        :   'WMSDF001E2',                            
        headerHeight        :   '27',                             // 헤더 높이
        rowHeight           :   '27',                             // 데이터 행 높이
        frozenColIdx        :   '2',                             // 열 틀 고정할 열 인덱스
        setColor            :   false,
        useFilter           :   true,
        headerMappingInfo   :   this.#headerMappingInfo, 
        // usedFooter          :   true
    });


    #defaultRowSet = [
        { 'INVC_ITEMNM_GB' : 'LOC'       , 'SORT_NUM' : '1' ,   'USE_YN' : 'Y' },
        { 'INVC_ITEMNM_GB' : 'QTY'       , 'SORT_NUM' : '2' ,   'USE_YN' : 'Y' },
        { 'INVC_ITEMNM_GB' : 'ITEMCD'    , 'SORT_NUM' : '3' ,   'USE_YN' : 'Y' },
        { 'INVC_ITEMNM_GB' : 'ITEMNM'    , 'SORT_NUM' : '4' ,   'USE_YN' : 'Y' }
    ]


    constructor(args){

        let vm = this;

        if(args != undefined){
            
        }

        this.Init();                        // !(필수) 화면 초기화
        this.AddEvent();                    // !(필수) 화면 이벤트 바인딩

        this.#grid.Init();                  // !(필수) 그리드 초기화
        this.#grid.SetGrid();               // !(필수) 그리드 생성
    }


    /**
     * 컴포넌트 초기화 및 기본 값 설정
     */
    Init(){

    }


    /**
     * 첫 번째 기본 설정 탭인 경우, 기본 설정값 셋팅
     * 
     * ex) 화주팝업호출
     */
    DefaultSetting(){

    }


    /**
     * 컴포넌트 이벤트 바인딩
     * 
     * @param {*} args 
     */
    AddEvent(args){

        let vm = this;

        // # 화주
        document.getElementById("vrSrchCustCdE2").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.CODE, e.target.value);
        });

        jQuery("#vrSrchCustImgE2").off().on("click", (e)=>{
            StandardPopUp.call(this, COMP_CATEGORY.CUST);
        });
        
        document.getElementById("vrSrchCustNmE2").addEventListener('change', (e)=>{
            this.CustChangedEventCallBack(COMP_VALUE_TYPE.NAME, e.target.value);
        });

        // # '택배구분' 변경 이벤트
        document.getElementById("vrSrchParcelComCdE2").addEventListener('change', (e)=>{
            this.ParcelChagnedCallBack(e);
        });


        // # '택배SEQ' 변경 이벤트
        document.getElementById("vrSrchParcelSeqE2").addEventListener('change', (e)=>{
            this.SearchClickEventCallback();
        });


        // # '신규' 버튼 클릭 이벤트
        jQuery("#btn_addE2").off().on("click", (e)=>{
            this.AddRowEventCallBack(e);
        });


        // # '저장' 버튼 클릭 이벤트
        jQuery("#btn_saveE2").off().on("click", (e)=>{
            this.SaveClickEventCallBack(e);
        });


        // # '검색' 버튼 클릭 이벤트
        jQuery("#btnSrchE2").off().on("click", (e)=>{
            this.SearchClickEventCallback(e);
        });


        this.#grid.CellEditedCustomEvent = function(sender, args){
            vm.#grid.SetCheckedUnitRow(args.row, args.col, true);
        }


        this.#grid.ButtonClickCustomEvent = function(sender, args){

            let rowIdx = args.row;
            let colIdx = args.col;
            
            let dlvSeq  = jQuery("#vrSrchParcelSeqE2").val();
            let dlvCd   = jQuery('#vrSrchParcelComCdE2').val();
            let custId  = jQuery('#vrSrchCustIdE2').val();

            if(cfn_isEmpty(custId) || cfn_isEmpty(dlvCd) || cfn_isEmpty(dlvSeq)){
                alert(`택배구분, 택배SEQ, 화주 정보를 선택하세요.`);
                return 
            }
    

            switch(colIdx){
                case vm.#grid.GetColumnIdx('UP') : 
                    if(rowIdx > 0 ){
                        vm.RowUp(rowIdx);
                    }
                    break;

                case vm.#grid.GetColumnIdx('DOWN') : 
                    if(rowIdx < (vm.#grid.GetRowCount() -1)){
                        vm.RowDown(rowIdx);
                    }
                    break;
            }
        }
    }


    /**
     * 탭 전환 등 화면전환에 대한 이벤트 콜백
     * - 초기값 설정
     * 
     * @param {*} args 
     */
    ActiveViewEventCallBack(args){

        // Set Cust Info
        if(cfn_isEmpty(this.#compCust.GetValue(COMP_VALUE_TYPE.ID))){

            // Set Session Value
            this.#compCust.SetValue(SessionUtil.GetValue(SessionKey.CUST_CD), SessionUtil.GetValue(SessionKey.CUST_ID), SessionUtil.GetValue(SessionKey.CUST_NM));

            this.#custId = SessionUtil.GetValue(SessionKey.CUST_ID);
            
            //화주사 택배 정보 셋팅
            this.fn_getCustDlvCompInfo(this.#custId);
        }

        // 횡 스크롤 새로고침
        this.#grid.ShowHorizontalScrollbar(true);
    }


//#region   :: Component Set 
    
    fn_getCustDlvCompInfo(custId){
    	let vm = this;
    	var urlSend		= "/WMSOP642/dlvDlvCompInfo.action";
    	var paramSend   = "vrSrchCustId=" + custId;
    	
    	
    	jQuery.post(urlSend, paramSend, function(data){
    		var data = data.DLV_COMP_INFO;
    		var cnt  = data.size();
    		
    		jQuery("#vrSrchParcelComCdE2").children('option').remove();
    		jQuery("#vrSrchParcelSeqE2").children('option').remove();
    		
    		if(cnt > 0){
    			for(var i = 0; i < cnt; i++){
    				jQuery("#vrSrchParcelComCdE2").append("<option value='" + data[i].PARCEL_COM_TY + "'>" + data[i].PARCEL_COM_TY_NM + "</option>");
    			}
    			
    			jQuery("#vrSrchParcelComCdE2 option:eq(0)").prop("selected", true);
    			 
    			vm.SetParcelSeq();
    		}
    		
    		
    	}).fail(function(xhr, textStatus){
    		if(xhr.status == 404){	
    			location.href=('/index.html');
    		}
    	});
    }


    SetParcelSeq(){
        let vm = this;
        let formData = new FormData(document.getElementById(this.#formId));
        
        jQuery.ajax({
            url : "/WMSDF001/selectDlvSeq.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                cfn_viewProgress();
            },
            complete: function(){
                cfn_closeProgress();
            },
            success : function(data){

                jQuery("#vrSrchParcelSeqE2 option").remove();

                if(data == undefined || data == null || data.rows == null || data.rows.length == 0){
                    alert(`택배기본정보를 찾을 수 없습니다. (택배사 정보없음.)`);
                    return ;
                }

                for(let parcelInfo of data.rows){

                    let option = new Option();

                    option.value = parcelInfo['PARCEL_COM_TY_SEQ'];
                    option.text = parcelInfo['PARCEL_COM_TY_SEQ']+" : "+parcelInfo['SHORT_DESC'];

                    jQuery("#vrSrchParcelSeqE2").append(option);
                }
                
                vm.SearchClickEventCallback();
            }
        });
    }


//#endregion


//#region   :: CallBack Event

    /**
     * 기준정보 공통팝업 이벤트 콜백함수.
     * 
     * ! 팝업 사용시 정의 필수 
     * [commonUtil.js]
     * 
     * @param {*} type  : 팝업 호출 타입
     * @param {*} data  : 팝업 반환 값
     */
    StandardPopUpCallBack = (type, data) =>{

        const vm = this;

        switch(type)
        {
            //* 화주
            case COMP_CATEGORY.CUST :
                vm.#compCust.SetValue(data[0][0].cust_cd, data[0][0].cust_id, data[0][0].cust_nm);

                SessionUtil.Insert(SessionKey.CUST_CD, data[0][0].cust_cd);
                SessionUtil.Insert(SessionKey.CUST_NM, data[0][0].cust_nm);
                SessionUtil.Insert(SessionKey.CUST_ID, data[0][0].cust_id);

                vm.#custId = data[0][0].cust_id;

                /** 그리드, 컴포넌트 초기화 */
                vm.#grid.Clear();                                    // 그리드 초기화
                jQuery("#vrSrchParcelComCdE2").val('');             // 택배구분 초기화
                
                //화주사 택배 정보 셋팅 
				this.fn_getCustDlvCompInfo(vm.#custId);	
                
                this.#parcelCd = null;

                break;
        }

    }


    async CustChangedEventCallBack(type, refValue, id){

        const vm = this;

        if(cfn_isEmpty(refValue)){
            this.#compCust.Init();
            return;
        }

        await this.#compCust.Search(type, refValue)
                .then((data)=>{
                    if(data.length == 1){
                        SessionUtil.Insert(SessionKey.CUST_CD, data[0][COMP_VALUE_TYPE.CODE]);
                        SessionUtil.Insert(SessionKey.CUST_NM, data[0][COMP_VALUE_TYPE.NAME]);
                        SessionUtil.Insert(SessionKey.CUST_ID, data[0][COMP_VALUE_TYPE.ID]);

                        vm.#custId = data[0][COMP_VALUE_TYPE.ID];
                        
                        //화주사 택배 정보 셋팅 
        				this.fn_getCustDlvCompInfo(vm.#custId);	
                    }
                    else{
                        let popParam = {};
                        popParam[type == COMP_VALUE_TYPE.CODE ? 'CUST_CD' : 'CUST_NM'] = refValue;
            
                        StandardPopUp.call(vm, COMP_CATEGORY.CUST, popParam);
                    }
                })
                .catch((error)=>{
                    alert(error);
                });
    }


    ParcelChagnedCallBack(e){

        this.#parcelCd = e.target.value;

        this.#grid.Clear();

        if(!cfn_isEmpty(this.#parcelCd) && !cfn_isEmpty(this.#custId)){
            this.SetParcelSeq();
        }
    }


    AddRowEventCallBack(){

        let dlvSeq  = jQuery("#vrSrchParcelSeqE2").val();
        let custNm  = this.#compCust.GetValue(COMP_VALUE_TYPE.NAME);
        let custId  = this.#compCust.GetValue(COMP_VALUE_TYPE.ID);
        let dlvCd   = jQuery('#vrSrchParcelComCdE2').val();
        let dlvNm   = jQuery('#vrSrchParcelComCdE2 option:selected').text();

        if(cfn_isEmpty(custId) || cfn_isEmpty(dlvCd) || cfn_isEmpty(dlvSeq)){
            alert(`택배구분, 택배SEQ, 화주 정보를 선택하세요.`);
            return 
        }

        if(this.#grid.GetRowCount() > 0){
            alert(`동일 택배SEQ로 중복된 값을 생성할 수 없습니다.`);
            return 
        }
        
        for(let defaultInfo of this.#defaultRowSet){

            let newRowIdx = this.#grid.RowAddEventCallBack(false, null, true);  // Add New Row

            /** Set Default Value */
            for(let colKey of Object.keys(defaultInfo)){
                this.#grid.setValue(newRowIdx, this.#grid.GetColumnIdx(colKey), defaultInfo[colKey]);
            }

            /** Set Selected Value */
            this.#grid.setValue(newRowIdx, this.#grid.GetColumnIdx('CUST_ID'), custId);             // 화주사 ID
            this.#grid.setValue(newRowIdx, this.#grid.GetColumnIdx('CUST_NM'), custNm);             // 화주사 ID
            this.#grid.setValue(newRowIdx, this.#grid.GetColumnIdx('PARCEL_COM_TY'), dlvNm);        // 택배사명
            this.#grid.setValue(newRowIdx, this.#grid.GetColumnIdx('PARCEL_COM_TY_SEQ'), dlvSeq);   // 택배사시퀀스
        }
    }


    SaveClickEventCallBack(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));
        let targetData = this.#grid.GetAllRowData();

        try {
            if(cfn_isArray(targetData) && targetData.length == 0){
                throw `1개 이상 행을 선택해야 합니다.`;
            }

            // 1. 사용여부 기준 정렬
            targetData.sort((item1, item2)=>{
                if(item1['USE_YN'] == 'N'){
                    return 1;
                } else {
                    return -1;
                }
            });

            // 2. 사용항목 중 내부 순번 정렬
            targetData.sort((item1, item2)=>{
                if(item1['USE_YN'] == 'Y' && item1['USE_YN'] == 'Y'){
                    if(item1['SORT_NUM'] < item2['SORT_NUM']){
                        return -1;
                    } else {
                        return 1;
                    }
                }
            });

            //3. 순번값 업데이트
            for(let i=0; i < targetData.length; i++){
                targetData[i]['SORT_NUM'] = (i+1);
            }


            formData.append('gridData', JSON.stringify(targetData));

            jQuery.ajax({
                url : "/WMSDF001/saveE2.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                beforeSend: function(xhr){
                    if(isProgressBar) cfn_viewProgress();
                },
                complete: function(){
                    if(isProgressBar) cfn_closeProgress();
                },
                success : function(data){
                    // Fail ...
                    if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                        alert(data["MSG"]);
                    }
                    // Success ...
                    else{
                        alert(DICTIONARY['save_success']);
                        vm.SearchClickEventCallback();
                    }
                }
            });

        } catch (error) {
            alert(error);
        }

    }


    /**
     * 데이터조회 버튼 클릭 이벤트
     * 
     * @param {*} e 
     * @param {boolean} isProgressBar   : 조회 간 프로그레스 바 표출 여부
     */
    SearchClickEventCallback(e, isProgressBar=true){

        let vm = this;
        let formData = new FormData(document.getElementById(`${this.#formId}`));

        return jQuery.ajax({
            url : "/WMSDF001/listE2.action",
            type : 'post',
            data : formData,
            contentType : false,
            processData : false,
            beforeSend: function(xhr){
                if(isProgressBar) cfn_viewProgress();
            },
            complete: function(){
                if(isProgressBar) cfn_closeProgress();
            },
            success : function(data){

                // Fail ...
                if(!cfn_isEmpty(data) && ("errCnt" in data) && (Number(data["errCnt"]) > 0)){
                    throw data["MSG"];
                }
                // Success ...
                else{
                    vm.#grid.SetData(data);
                }

            }
        });

    }


//#endregion


//#region   :: Validation
    


//#endregion


//#region   :: Utility


    RowUp(row){

        let spdList         = this.#grid.spdList;
        let seqColumnIdx    = this.#grid.GetColumnIdx('SORT_NUM');
        let upColumnIdx     = this.#grid.GetColumnIdx('UP');

        //seq와 바꿔주고
        //오름차순정렬
        var fromSeq = spdList.getValue(row, seqColumnIdx);
        var toSeq   = spdList.getValue(row - 1, seqColumnIdx);
        spdList.setValue(row, seqColumnIdx, toSeq);
        spdList.setValue(row - 1, seqColumnIdx, fromSeq);
        
        var sortInfo = [ { index: seqColumnIdx, ascending: true } ] ;
        spdList.sortRange(row - 1, 0, 2, spdList.getColumnCount(), true, sortInfo);
        spdList.setActiveCell(row - 1, upColumnIdx);

        this.#grid.SetCheckedUnitRow(row, null, true);
        this.#grid.SetCheckedUnitRow(row - 1, null, true);
        this.#grid.targetRow = row - 1;
        this.#grid.FocusedRowHightLight();
    }


    RowDown(row){

        let spdList         = this.#grid.spdList;
        let seqColumnIdx    = this.#grid.GetColumnIdx('SORT_NUM');
        let downColumnIdx   = this.#grid.GetColumnIdx('DOWN');

        var fromSeq = spdList.getValue(row, seqColumnIdx);
        var toSeq   = spdList.getValue(row + 1, seqColumnIdx);
        spdList.setValue(row, seqColumnIdx, toSeq);
        spdList.setValue(row + 1, seqColumnIdx, fromSeq);
        
        var sortInfo = [ { index: seqColumnIdx, ascending: true } ];
        spdList.sortRange(row, 0, 2, spdList.getColumnCount(), true, sortInfo);
        spdList.setActiveCell(row + 1, downColumnIdx);

        this.#grid.SetCheckedUnitRow(row, null, true);
        this.#grid.SetCheckedUnitRow(row + 1, null, true);
        this.#grid.targetRow = row + 1;
        this.#grid.FocusedRowHightLight();
    }


//#endregion


}