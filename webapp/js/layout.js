/**
 * 화면의 레이아웃에 관련된 스크립트
 */


/**
 * 좌측 메뉴를 여닫는 함수
 * 좌측 메뉴 영역의 넓이가 달라지면 넓이 부분을 재정의 해야 함.
 */
/**
 * 화면의 레이아웃에 관련된 스크립트
 */

var Layout = function () {


	var parent_menu_id = jQuery("#_parent_menu").val();
	var current_menu_id = jQuery("#_current_menu").val();

	// console.log('parent menu id :' + parent_menu_id);
	// console.log('current_menu_id :' + current_menu_id);

    // Set proper height for sidebar and content. The content and sidebar height must be synced always.
    var handleSidebarAndContentHeight = function () {
        var sidebar = jQuery('.page-sidebar');
        var height;
    };

    // Handle sidebar menu links
    var handleSidebarMenuActiveLink = function(mode, el) {
        var url = location.hash.toLowerCase();
        var slideSpeed = parseInt(menu.data("slide-speed"));

        // disable active states
        menu.find('li.active').removeClass('active');
        menu.find('li.active.open').removeClass('open');
        menu.find('li > a > .selected').remove();

        if (menu.hasClass('page-sidebar-menu-hover-submenu') === false) {
            menu.find('li.open').each(function(){
                if (jQuery(this).children('.sub-menu').size() === 0) {
                    jQuery(this).removeClass('open');
                    jQuery(this).find('> a > .arrow.open').removeClass('open');
					// console.log("dd");
                }
            });
        } else {
             menu.find('li.open').removeClass('open');
        }

        el.parents('li').each(function () {
            jQuery(this).addClass('active');
            jQuery(this).find('> a > span.arrow').addClass('open');

            if (jQuery(this).parent('ul.page-sidebar-menu').size() === 1) {
                jQuery(this).find('> a').append('<span class="selected"></span>');
            }

            if (jQuery(this).children('ul.sub-menu').size() === 1) {
                jQuery(this).addClass('open');
            }
        });

    };

    // Handle sidebar menu
    var handleSidebarMenu = function () {
        // handle sidebar link click
        jQuery('.page-sidebar').on('click', 'li > a', function (e) {
            var hasSubMenu = jQuery(this).next().hasClass('sub-menu');


            var parent = jQuery(this).parent().parent();
            var the = jQuery(this);
            var menu = jQuery('.page-sidebar-menu');
            var sub = jQuery(this).next();

            var slideSpeed = parseInt(menu.data("slide-speed"));
            var keepExpand = menu.data("keep-expanded");

            if (keepExpand !== true) {
                parent.children('li.open').children('a').children('.arrow').removeClass('open');
                parent.children('li.open').children('.sub-menu:not(.always-open)').slideUp(slideSpeed);
                parent.children('li.open').removeClass('open');
            }

            var slideOffeset = -200;

            if (sub.is(":visible")) {
                jQuery('.arrow', jQuery(this)).removeClass("open");
                jQuery(this).parent().removeClass("open");
            } else if (hasSubMenu) {
                jQuery('.arrow', jQuery(this)).addClass("open");
                jQuery(this).parent().addClass("open");
                sub.slideDown(slideSpeed, function () { });
            }
        });

        // handle ajax links within sidebar menu
        jQuery('.page-sidebar').on('click', ' li > a.ajaxify', function (e) {
            e.preventDefault();
            Logisall.scrollTop();

            var url = jQuery(this).attr("href");
            var menuContainer = jQuery('.page-sidebar ul');

            menuContainer.children('li.active').removeClass('active');
            menuContainer.children('arrow.open').removeClass('open');

            jQuery(this).parents('li').each(function () {
                jQuery(this).addClass('active');
                jQuery(this).children('a > span.arrow').addClass('open');
            });
            jQuery(this).parents('li').addClass('active');

        });

    };

    // Handles fixed sidebar
    var handleFixedSidebar = function () {
        var menu = jQuery('.page-sidebar-menu');
        if (jQuery('.page-sidebar-fixed').size() === 0) {
            handleSidebarAndContentHeight();
            return;
        }

    };

    // Handles sidebar toggler to close/hide the sidebar.
    var handleFixedSidebarHoverEffect = function () {
        var body = jQuery('body');
        if (body.hasClass('page-sidebar-fixed')) {
            jQuery('.page-sidebar').on('mouseenter', function () {
                if (body.hasClass('page-sidebar-closed')) {
                    jQuery(this).find('.page-sidebar-menu').removeClass('page-sidebar-menu-closed');
                }
            }).on('mouseleave', function () {
                if (body.hasClass('page-sidebar-closed')) {
                    jQuery(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
                }
            });
        }
    };

    // Hanles sidebar toggler
    var handleSidebarToggler = function () {
        var body = jQuery('body');
        if (jQuery.cookie && jQuery.cookie('sidebar_closed') === '1') {
            jQuery('body').addClass('page-sidebar-closed');
            jQuery('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
            if (jQuery.cookie) {
            	jQuery(window).trigger('resize');
            }
        }

        // handle sidebar show/hide
        jQuery('body').on('click', '.sidebar-toggler', function (e) {
            var sidebar = jQuery('.page-sidebar');
            var sidebarMenu = jQuery('.page-sidebar-menu');
            jQuery(".sidebar-search", sidebar).removeClass("open");

            if (body.hasClass("page-sidebar-closed")) {
                body.removeClass("page-sidebar-closed");
                sidebarMenu.removeClass("page-sidebar-menu-closed");
                if (jQuery.cookie) {
                    jQuery.cookie('sidebar_closed', '0');
                }
            } else {
                body.addClass("page-sidebar-closed");
                sidebarMenu.addClass("page-sidebar-menu-closed");
                if (body.hasClass("page-sidebar-fixed")) {
                    sidebarMenu.trigger("mouseleave");
                }
                if (jQuery.cookie) {
                    jQuery.cookie('sidebar_closed', '1');
                }
            }

            jQuery(window).trigger('resize');
            
            //html force event window resizing
            gfn_forceEventResize();
        });
    };


    return {
        initSidebar: function() {
            //layout handlers
            handleFixedSidebar(); // handles fixed sidebar menu
            handleSidebarMenu(); // handles main menu
            handleSidebarToggler(); // handles sidebar hide/show
        },

        init: function () {
            this.initSidebar();
        }
    };

}();


var Logisall= function() {

    // IE mode
    var isRTL = false;
    var isIE8 = false;
    var isIE9 = false;
    var isIE10 = false;

    var resizeHandlers = [];


    // theme layout color set


    // initializes main settings
    var handleInit = function() {

        if (jQuery('body').css('direction') === 'rtl') {
            isRTL = true;
        }

        isIE8 = !!navigator.userAgent.match(/MSIE 8.0/);
        isIE9 = !!navigator.userAgent.match(/MSIE 9.0/);
        isIE10 = !!navigator.userAgent.match(/MSIE 10.0/);

        if (isIE10) {
            jQuery('html').addClass('ie10'); // detect IE10 version
        }

        if (isIE10 || isIE9 || isIE8) {
            jQuery('html').addClass('ie'); // detect IE10 version
        }
    };



    // Handles Bootstrap Accordions.
    var handleAccordions = function() {};
    // Handles Bootstrap Dropdowns
    var handleDropdowns = function() { };




    //* END:CORE HANDLERS *//

    return {

        //main function to initiate the theme
        init: function() {
            //UI Component handlers
            handleDropdowns(); // handle dropdowns
            handleAccordions(); //handles accordions
        },

        //main function to initiate core javascript after ajax complete
        initAjax: function() {
            handleDropdowns(); // handle dropdowns
            handleAccordions(); //handles accordions
        },

        //init main components
        initComponents: function() {
            this.initAjax();
        },

        // check IE8 mode
        isIE8: function() {
            return isIE8;
        },

        // check IE9 mode
        isIE9: function() {
            return isIE9;
        }

    };

}();

/**
 * 기존 소스
 */
//function closeMenu() {

	// if(jQuery("#pane_left").css("display") == "none") {
	//if(jQuery("#iconPane a span").hasClass("ui-icon-arrowstop-1-e")) {
		// jQuery("#pane_left").show();
		//jQuery("#pane_left").css("width", "210px").removeClass("pane_close").addClass("pane_open");
		//jQuery("body").css("background-position","left top");
		//if(jQuery.browser.version == "6.0") {
		//	jQuery("#pane_right").css("margin-left", "210px");
		//	resizeGrid(185);
		//} else {
//			jQuery("#pane_right").css("margin-left", "225px");
		//	resizeGrid(null);
		//}
		//jQuery("#iconPane a span").removeClass("ui-icon-arrowstop-1-e").addClass("ui-icon-arrowstop-1-w");
		//resizeContentObject();

	//} else {
		// jQuery("#pane_left").hide();
		//jQuery("#pane_left").css("width", "57px").addClass("pane_close").removeClass("pane_open");
		//jQuery("body").css("background-position","-153px top");
		//if(jQuery.browser.version == "6.0") {
		//	jQuery("#pane_right").css("margin-left", "63px");
		//	resizeGrid(-221);
		//} else {
//			jQuery("#pane_right").css("margin-left", "25px");
		//	resizeGrid();
		//}
		//jQuery("#iconPane a span").addClass("ui-icon-arrowstop-1-e").removeClass("ui-icon-arrowstop-1-w");
		//resizeContentObject();
	//}
//}



/**
 * jqgrid의 폭을 조절하는 함수
 * 파라메터 weight는 ie6.0일때만 쓰이며 7이상에서는 null값을 넣어서 호출.
 * @param weight
 */
function resizeGrid(weight) {
	jQuery('.ui-jqgrid-btable:visible').each(function() {
		gridId = jQuery(this).attr('id');
		if(jQuery.browser.version == "6.0") {
			jQuery('#' + gridId).setGridWidth(jQuery('#' + gridId).width() - weight);
		} else {
			//jQuery('#' + gridId).setGridWidth(100);
			//jQuery('#' + gridId).css("border", "1px solid red");
			jQuery('#' + gridId).setGridWidth("100%");

			gridParentWidth = jQuery('#gbox_' + gridId).parent().width() - 2;
			jQuery('#' + gridId).setGridWidth(gridParentWidth);
		}
	});
}


/**
 * 컨텐츠의 우측 부분의 폭을 조절
 */
function resizeContentObject() {
	var width = "100%";
	if(jQuery.browser.version == "6.0") {
		jQuery("#pane_right").width("width", jQuery("#pane_right").width("width", "100%") - 20);
	} else {
		jQuery("#pane_right table").css("width", width);
	}
}
