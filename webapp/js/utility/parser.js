/**
 * Velocity로 치환된 콤보박스 문자열 값을 파싱. 
 * $!comCode.getGridComboComCode
 * 
 * @param {*} name 	: 콤보박스 Key 값
 * @param {*} arg 	: 치환된 문자열 Object
 * @returns 
 */
function ParseComboString(name, arg){
	var rtn  = {};
	var all = [];
	var keys = [];
	var values = [];

	var comboStringList = arg.split(';');

	for(var i = 0; i < comboStringList.length; i++) {
		var comboArr = comboStringList[i].split(':');
		all.push([cfn_trim(comboArr[0]), cfn_trim(comboArr[1])]);
		keys.push(cfn_trim(comboArr[0]));
		values.push(cfn_trim(comboArr[1]));
	}
	rtn[name] = {};
	rtn[name].all = all;
	rtn[name].keys = keys;
	rtn[name].values = values;

	return {
		'all' 		: all,
		'keys'		: keys,
		'values'	: values
	}
}


/**
 * 사전정의된 콤보타입 문자열의 Key, Value 맵 구성 및 반환
 * 
 * $!comCode.getGridComboComCode
 * 
 * @param {*} category : 사전정의된 코드값
 * @returns {Map}
 */
function ParseComCodeMap(category){

	var rtn  = new Map;

	let codeString = COL_COMBO_LIST[category].value.split(';');

	for(var i = 0; i < codeString.length; i++) {
		var comboArr = codeString[i].split(':');
		rtn.set(cfn_trim(comboArr[0]), cfn_trim(comboArr[1]));
	}

	return rtn;
}