const GridUtility = (function(){

    /**
     * Grid Format 바인딩
     * 
     * @param {Number} 		colIdx 
     * @param {function} 	formatter 
     */
    const AddFormatter = function(spdList, colIdx, formatter, parse){

        var cell = spdList.getCell(-1, colIdx, GC.Spread.Sheets.SheetArea.viewport);

        var customFormatterTest = {};
        customFormatterTest.prototype = GC.Spread.Formatter.FormatterBase;
        customFormatterTest.format = formatter;
        customFormatterTest.parse  = parse;
        
        cell.formatter(customFormatterTest);
    }


    return {
        AddFormatter : AddFormatter
    }
})();
