export class WinusGrid {

    /* Instance Variable */
    #created            = false;            // Grid 생성완료 여부
    #spdList            = null;
    #spdSheet           = null;
    #spdListDiv         = null;
    #gridTemplate       = null;
    #columnInfo         = null;             // 컬럼 옵션정보
    #colMap             = null;             // 컬럼 배열정보 (Map)
    #colComboList       = new Map()         // [Style] 컬럼 콤보박스 구성정보 (기준코드 참조)
    #columnStyles       = new Map();        // [Style] 컬럼 서식정보
    #spdColInfos        = new Array();      // Spread JS 규격에 맞는 컬럼 옵션정보
    #dataSource         = new Array();      // Sheet에 Display 될 조회 데이터
    #targetRow          = null;             // 현재 위치한 행 인덱스
    #targetCol          = null;             // 현재 위치한 열 인덱스
    #oldSelections      = null;             // 이전 선택 정보 (복수선택)
    #newSelections      = null;             // 현재 선택 정보 (복수선택)
    #footerLineIdx      = null;             // [Footer] 통계치 표시라인 인덱스
    #sortingToggle      = false;            // 헤더 오름/내림 차순 정렬 토글


    /** Options */
    #checkFirstIdx      = null;             // [Select]       Display 시 기준이 되는 첫번째(Root) Check Box 선택 열
    #multiCheckRange    = new Array();      // [Select]       복수열로 구성된 체크박스들의 Index 모음
    #useFilter          = false;            // [Style]        Filter 사용여부
    #useSort            = false;            // [Style]        Sorting 사용여부
    #rowHeight          = null;             // [Style]        행 높이
    #headerHeight       = null;             // [Style]        컬럼명 높이
    #frozenColIdx       = null;             // [Style]        틀 고정 열 순번 (Default : 2)
    #focuedRowIdx       = null;             // [Style]        포커싱된 행의 순번 (이력)
    #rowSpanColIdx      = null;             // [Span]         행 병합 기준범위를 설정할 컬럼 인덱스
    #rowSpanColArr      = new Array();      // [Span]         행 병합 대상 컬럼의 인덱스
    #rowSpanExIdx       = new Array();      // [Span]         행 병합 기준이 아닌 행들의 값
    #exRowSpanColIVal   = null;             // [Span]         병합기준 인덱스의 병합 예외값
    #setColor           = true;             // [Color]        지정배경색 적용여부(true/false)(신규그리드리관리)
    #usedFooter         = false;            // [Footer]       통계치 표시 사용여부
    #usedContextMenu    = false;            // [ContextMenu]  컨텍스트 메뉴 사용여부
    #contextMenuInfo    = new Array();      // [ContextMenu]  컨텍스트 메뉴 구성정보
    #headerMappingInfo  = new Array();      // [Header]       그룹 헤더설정
    #exFileExportArr    = new Array();      // [Excel]        엑셀 내보내기 제외 컬럼 목록


    /** Custom Event */
    #cellClickCustomEvent          = null;  // Cell Click Event Binding 변수
    #cellChangedCustomEvent        = null;  // Cell Changed Event Binding 변수
    #cellEditedCustomEvent         = null;  // Cell Edited Event Binding 변수
    #cellDoubleClickEvent          = null;  // Cell Double Click Event Binding 변수
    #buttonClickCustomEvent        = null;  // Cell ButtonClick Event Binding 변수
    #selectionChangedCustomEvent   = null;  // Selection Change Event Binding 변수


    constructor(args){

        if(args != undefined){
            this.#spdListDiv         = "spdListDiv" in args ? args.spdListDiv : "";
            this.#gridTemplate       = "gridTemplate" in args ? args.gridTemplate : "";
            this.#checkFirstIdx      = "checkFirstIdx" in args ? args.checkFirstIdx : 0; 
            this.#multiCheckRange    = "multiCheckRange" in args ? args.multiCheckRange : null; 
            this.#rowHeight          = "rowHeight" in args ? args.rowHeight : ""; 
            this.#headerHeight       = "headerHeight" in args ? args.headerHeight : ""; 
            this.#frozenColIdx       = "frozenColIdx" in args ? Number(args.frozenColIdx) : 0; 
            this.#exRowSpanColIVal   = "exRowSpanColIVal" in args ? args.exRowSpanColIVal : null; 
            this.#rowSpanColIdx      = "rowSpanColIdx" in args ? args.rowSpanColIdx : null; 
            this.#rowSpanColArr      = "rowSpanColArr" in args ? args.rowSpanColArr : null;
            this.#setColor           = "setColor" in args ? args.setColor : true;
            this.#usedFooter         = "usedFooter" in args ? args.usedFooter : false;
            this.#usedContextMenu    = "usedContextMenu" in args ? args.usedContextMenu : false;
            this.#contextMenuInfo    = "contextMenuInfo" in args ? args.contextMenuInfo : [];
            this.#headerMappingInfo  = "headerMappingInfo" in args ? args.headerMappingInfo : [];
            this.#useFilter          = "useFilter" in args ? args.useFilter : false;
            this.#useSort            = "useSort" in args ? args.useSort : false;
            this.#exFileExportArr    = "exFileExportArr" in args ? args.exFileExportArr : [];
        }
        else{
            console.log(`Not Found Argument, Create Normal Grid ... `);
        }

    }

    set targetRow(row)  { this.#targetRow = row; }
    set targetCol(col)  { this.#targetCol = col; }

    get targetRow()     { return this.#targetRow;   }
    get targetCol()     { return this.#targetCol;   }

    get colMap()        { return this.#colMap;      }
    get columnInfo()    { return this.#columnInfo;  }
    get spdList()       { return this.#spdList;     }
    get spdSheet()      { return this.#spdSheet;    }
    get gridTemplate()  { return this.#gridTemplate }
    get DataSource()    { return this.#dataSource }

    get created()       { return this.#created      }

    set CellClickCustomEvent(event)         { this.#cellClickCustomEvent  = event; }
    set CellChangedCustomEvent(event)       { this.#cellChangedCustomEvent  = event; }
    set CellEditedCustomEvent(event)        { this.#cellEditedCustomEvent   = event; }
    set CellDoubleClickEvent(event)         { this.#cellDoubleClickEvent    = event; }
    set ButtonClickCustomEvent(event)       { this.#buttonClickCustomEvent  = event; }
    set SelectionChangedCustomEvent(event)  { this.#selectionChangedCustomEvent  = event; }

    
    Init(){
        this.#spdSheet = new GC.Spread.Sheets.Workbook(jQuery(`#${this.#spdListDiv}`).get(0), {sheetCount : 1});

        this.#spdList = this.#spdSheet.getSheet(0);

		this.#spdSheet.options.allowUserDragDrop    = true;
		this.#spdSheet.options.selectionBorderColor = 'Accent 7';
		this.#spdSheet.options.selectionBackColor   = 'transparent'
		this.#spdSheet.options.referenceStyle       = GC.Spread.Sheets.ReferenceStyle.r1c1;

        ps_setSpreadSheet_init_new(this.#spdSheet);

        // 전역변수 Level의 필수함수 정의
        // ps_setSpreadEvents_Bind_Multi(this.#spdList, this.#spdSheet);
        this.AddEvent();
        
    }


    AddEvent(){

        let vm = this;

        this.#spdSheet.bind(GC.Spread.Sheets.Events.ButtonClicked, function (sender, args) {
            vm.ButtonClickEventCallBack(sender, args);
        });

        //Events : ValueChanged
        this.#spdList.bind(GC.Spread.Sheets.Events.ValueChanged, function (sender, args) {
            // ps_setSpreadEvents_ValueChanged(args);
        });

        //Events : CellChanged
        this.#spdList.bind(GC.Spread.Sheets.Events.CellChanged, function (sender, args) {

        });

        //Events : CellClick
        this.#spdList.bind(GC.Spread.Sheets.Events.CellClick, function (sender, args) {
            vm.CellClickEventCallBack(sender, args);
        });

        //Events : CellDoubleClick
        this.#spdList.bind(GC.Spread.Sheets.Events.CellDoubleClick, function (sender, args) {
            // ps_setSpreadEvents_CellDoubleClick(args);
            vm.CellDoubleClickEventCallBack(sender, args);
        });

        //Events : EditEnded
        this.#spdList.bind(GC.Spread.Sheets.Events.EditEnded, function (sender, args) {
            if(vm.#created){
                vm.CellEditedEventCallBack.call(vm, sender, args);
            }
        });

        //Events : LeaveCell
        this.#spdList.bind(GC.Spread.Sheets.Events.LeaveCell, function (sender, args) {
            // ps_setSpreadEvents_LostFocus(args);
        });

        //Events : ClipboardPasted
        this.#spdList.bind(GC.Spread.Sheets.Events.ClipboardPasted, function (sender, args) {
            // ps_setSpreadEvents_Pasted(args);
        });
        
        // Events : Filter로 인한 Repaint
        this.#spdList.bind(GC.Spread.Sheets.Events.RangeFiltered, function (sender, args) {
            // vm.SetSelectAll(0, false);     // 체크박스 모두 해제
        });

        // Events : Cell 선택 시 이벤트 발생, 전/후 정보 획득
        this.#spdList.bind(GC.Spread.Sheets.Events.SelectionChanged , function (sender, args) {
            vm.SelectionChangedEventCallBack(sender, args);
        });
        
        //Events : RowChanged
        /*this.#spdList.bind(GC.Spread.Sheets.Events.RowChanged, function (sender, args) {
            ps_setSpreadEvents_RowChanged(args);
        });*/
    }


    /**
     * 컬럼정보 중 '콤보리스트명'에 해당하는 구성요소들에 대한 파싱
     * - Velocity 템플릿 변환된 전역변수 "COL_COMBO_LIST" 필수.
     * 
     * @param {Array} COL_COMBO_LIST : Velocity 템플릿 전환간에 구성되는 전역변수
     * 
     */
    ParseComboList(){
        if(typeof(COL_COMBO_LIST) == 'undefined' || Object.keys(COL_COMBO_LIST).length == 0){
            return ;
        }

        let columnComboKeys = this.#columnInfo.map((dt)=>{return dt.COLUMN_COMBO})
                                                .filter((value, index, self)=>{ return ((self.indexOf(value) === index) && (value != '')); });

        for(let comboKey of columnComboKeys){

            if(!Object.keys(COL_COMBO_LIST).includes(comboKey)){
                /**
                 * 전역변수 COL_COMBO_LIST에 기준코드 템플릿 값 누락됨.
                 */
                alert(`그리드 구성에 필요한 콤보박스 정보가 없습니다. (${comboKey})`);
                return ;
            }
            

            // parseComboString : commonUtil.js
            this.#colComboList.set(comboKey, ParseComboString(comboKey, COL_COMBO_LIST[comboKey].value));

            // ParseComboString : commonUtil.js
            this.#colComboList.set(comboKey, ParseComboString(comboKey, COL_COMBO_LIST[comboKey].value));
        }
    }


    SetGrid(){
        let that = this;

        jQuery.ajax({
            url : "/WMSCM300/allList.action?vrTemplateType=" + this.#gridTemplate,
            type : 'GET',
            async : false,
            contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
            dataType : 'html',
            error : function(_, textStatus, errorThrown) {
                alert("$!lang.getText('처리 중 오류.')" + "($!lang.getText('관리자 문의. (조회 오류)'))");
            },
            success : function(data) {

                that.#spdList.suspendPaint();

                that.#colMap = that.SetColumnInfo(data);        // Set Column Info
                that.SetColumnFormat();                         // Set Column Style
                that.SetSheetStyle();                           // Set Header Style

                if(that.#usedContextMenu) that.SetContextMenu();

                that.#spdList.resumePaint();
                
            },
            complete: function(){
                that.#created = true;
            }
            
        });
    }


    /**
     * Grid 데이터 바인딩
     * 
     * 1. data 포맷
     * {total: , page: , records: , msg: , rows: }
     * 
     * @param {*} data
     * @param {*} callback 
     */
    SetData(data, callback){

        this.Clear();

        this.#dataSource = data.rows;

        this.#spdList.suspendPaint();

        this.#spdList.autoGenerateColumns = false;
        this.#spdList.setDataSource(data.rows);                 // [Warning] 바인딩 당시 reset 옵션을 주면 서식이 날아감.
        this.#spdList.bindColumn(this.#spdColInfos);

        this.#columnInfo.map((col)=>{

            // 컬럼정의(1~n), 그리드 배열순번(0~m)
            this.#spdList.bindColumn(col.COLUMN_SEQ-1, col.COLUMN_CODE);


            // TODO :: Style 다시적용해야??..


            // 첫번째 Check Box 타입 컬럼 ('선택')인 경우 RePaint
            if([1,2].includes(Number(col.COLUMN_SEQ)) && col.COLUMN_TYPE == 'CHECK'){
                ps_setSpreadCol_CHECKBOX_Property(this.#spdList, 0, 'CENTER', 'CENTER', 8, false, true, "");
            }
        });

        this.#spdList.resumePaint();

        this.SetSheetStyle();
        
        if( this.#useFilter )   this.SetFilter(0,1, this.GetRowCount(), this.GetColumnCount());
        if( this.#usedFooter )  this.SetFooterFormat() ;
    }


    Clear(){
        this.#spdList.deleteRows(0, this.#spdList.getRowCount());
    }


    /**
     * Cell 입력
     * 
     * @param {*} rowIdx : 행 번호 (0부터 시작)
     * @param {*} colIdx : 열 번호 (1부터 시작)
     * @param {*} value  : 입력값
     */
    setValue(rowIdx, colIdx, value){
        this.#spdList.setValue(rowIdx, colIdx, value);
    }


    /**
     * Cell 값을 가져온다.
     * 
     * @param {*} rowIdx : 행 번호
     * @param {*} colIdx : 열 번호
     * @returns 
     */
    getValue(rowIdx, colIdx){
        return this.#spdList.getValue(rowIdx, colIdx);
    }


    /**
     * 특정행의 컬럼을 활성 또는 비활성화.
     * 
     * @param {*} rowIdx : 행 번호
     * @param {*} colIdx : 열 번호
     * @param {boolean} isChecked     : 특정행 Lock 여부
     */
    SetCellLock(rowIdx, colIdx, isChecked){
        this.#spdList.getCell(rowIdx, colIdx).locked(isChecked);
    }


    /**
     * 특정 행, 열 위치의 Cell 활성화
     * 
     * @param {*} rowIdx 
     * @param {*} colIdx 
     */
    SetActiveCell(rowIdx, colIdx){
        this.#spdList.setActiveCell(rowIdx, colIdx);
    }


//#region   ::  Set Data


    SetValueByColKey(rowIdx, colKey, value){
        this.#spdList.setValue(rowIdx, this.GetColumnIdx(colKey), value);
    }


    /**
     * 특정 CheckBox 선택 열 기준 모든 행에 값 반영
     * Filter 설정으로 출력되지 않는 행은 배제
     * 
     * @param {*} colIdx 
     * @param {*} value 
     */
    SetSelectAll(colIdx=0, value){

        let rowCnt              = this.GetRowCount();
        let filterInfos         = this.#spdList.rowFilter();

        let isFilterUsed        = filterInfos != null && filterInfos.isFiltered();                      // Filter 사용이력 있는 지
        let isMulitCheckBox     = (this.#multiCheckRange != null) && (this.#rowSpanColIdx != null);     // 2열 이상 CheckBox 구성인 Grid인지

        this.#spdList.suspendPaint();

        this.#spdList.setValue(0, colIdx, value, GC.Spread.Sheets.SheetArea.colHeader);        // Header


        // 선택한 Header의 열 하위 레벨의 CheckBox 열 조회
        const checkColList = (this.#multiCheckRange != null) 
                                ? this.#multiCheckRange.filter((colIdx)=>{ return colIdx >= Number(colIdx) })
                                : [this.#checkFirstIdx] ;

        for(let targetColIdx of checkColList){

            const spanInfos = this.#spdList.getSpans().filter(dt=> {return dt.col == targetColIdx } );

            if(spanInfos.length > 0){

                // 병합 행 처리
                for(let spanInfo of spanInfos){
                    if(!(isFilterUsed && filterInfos.isRowFilteredOut(spanInfo.row)) && (!this.CheckFooterIdx(spanInfo.row))){
                        this.#spdList.setValue(spanInfo.row, targetColIdx, value, GC.Spread.Sheets.SheetArea.viewport);
                    }
                }

                // TODO :: CheckBox 복수열 레벨에 따라 병합 제외 행을 관리하는 자료형 필요
                // 병합아닌 행 처리
                for(let nonSpanRowIdx of this.#rowSpanExIdx){
                    if(!(isFilterUsed && filterInfos.isRowFilteredOut(nonSpanRowIdx)) && (!this.CheckFooterIdx(nonSpanRowIdx))){
                        this.#spdList.setValue(nonSpanRowIdx, targetColIdx, value, GC.Spread.Sheets.SheetArea.viewport);
                    }
                }

            }
            else{
                for(let rowIdx=0; rowIdx < rowCnt; rowIdx++){
            
                    if(!(isFilterUsed && filterInfos.isRowFilteredOut(rowIdx)) && (!this.CheckFooterIdx(rowIdx))){
        
                        this.#spdList.setValue(rowIdx, targetColIdx, value, GC.Spread.Sheets.SheetArea.viewport);
        
                    }
                }
            }
        }
        

        this.#spdList.resumePaint();
    }


    /**
     * Grid 통계(Footer) 영역에 값 바인딩
     * 
     * @param {*} colKey 
     * @param {*} value 
     * @param {*} callback
     */
    SetFooterValue(colKey, value){
        // [Footer] 행추가로 인한 변경된 통계치 표시라인 인덱스
        let changeFooterLineIdx= this.GetRowCount();             

        //행추가로 인한 통계치 라인 변경 값을 비교
        if(this.#footerLineIdx != changeFooterLineIdx - 1)
            this.setValue(changeFooterLineIdx - 1, this.GetColumnIdx(colKey), value);
        else
            this.setValue(this.#footerLineIdx, this.GetColumnIdx(colKey), value);
    }


    /**
     * 그리드설정에 따른 기본 컬럼 서식 적용
     * 
     * @param {*} rowIdx 
     */
    SetColumnDefaultStyle(rowIdx){
        
        for(let colIdx=0; colIdx < this.GetColumnCount(); colIdx++){

            if(colIdx != this.#checkFirstIdx){
                this.#spdList.setStyle(rowIdx, colIdx, this.#columnStyles.get(colIdx), GC.Spread.Sheets.SheetArea.viewport);
            }
        }

    }


    /**
     * 포커싱 서식 적용 (행 단위)
     * 
     * @param {*} rowIdx 
     */
    SetColumnFocusStyle(rowIdx){
        
        for(let colIdx=0; colIdx < this.GetColumnCount(); colIdx++){
            let style = this.#columnStyles.get(colIdx).clone();     //! 객체형 참조로 clone을 이용한 Deep Copy 필요.

            if(colIdx != this.#checkFirstIdx){
                style.backColor = BKC_SK;
                this.#spdList.setStyle(rowIdx, colIdx, style, GC.Spread.Sheets.SheetArea.viewport);
            }
        }

    }


//#endregion


//#region   ::  Get Data


    /**
     * 그리드관리 정보에 기반한 Column Index 값 조회
     * 
     * @param {*} key  : Column Key
     */
    GetColumnIdx(key){
        return (this.#colMap.get(key)-1);
    }


    /**
     * 그리드관리 정보에 기반한 Column Name 조회 (Code 조건)
     * 
     * @param {*} colKey 
     * @returns 
     */
    GetColumnNameByCode(colKey){

        let columnInfo = this.#columnInfo.filter((dt)=>{

            return dt["COLUMN_CODE"] == colKey;

        });

        return columnInfo[0]["COLUMN_NAME"];

    }


    /**
     * 그리드관리 정보에 기반한 Column Code(key값) 조회 (Index 조건)
     * 
     * @param {*} colIdx : 배열기준 (0~n)
     */
    GetColumnCodeByIdx(colIdx){

        let columnInfo = this.#columnInfo.filter((dt)=>{

            return (Number(dt["COLUMN_SEQ"]) -1) == colIdx ;

        });

        return columnInfo[0]["COLUMN_CODE"];
    }


    /**
     * Cell 값 획득
     * 
     * @param {*} colKey 
     * @param {*} rowIdx 
     */
    GetCellValue(colKey, rowIdx){

        rowIdx = rowIdx ?? this.#targetRow;

        return this.#spdList.getValue(rowIdx, this.GetColumnIdx(colKey));
    }


    /**
     * Get Grid Row Total Count
     * 
     * @returns 
     */
    GetRowCount(){
        return this.#spdList.getRowCount();
    }


    /**
     * 특정 열의 값을 기준으로 중복되지 않은 행 개수를 반환한다.
     * - 주문 수, 상품 수
     * 
     * @param {*} colKey 
     * @returns 
     */
    GetRowCountByDistinctKey(colKey){

        return this.#dataSource.map((dt)=>{return dt[colKey]}).filter((value, index, self)=>{
            return self.indexOf(value) === index;
        }).length;
    }


    /**
     * 특정 열의 값을 기준으로 총 합계를 계산한다.
     * ! 열은 반드시 숫자타입으로 함.
     * - 주문수량, 상품수량
     * 
     * @param {*} colKey 
     * @returns 
     */
    GetRowTotalValue(colKey){
        
        return this.#dataSource.map((dt)=>{return dt[colKey]}).reduce(function add(sum, currValue){
            return sum + parseInt(currValue);
        }, 0);
    }


    /** 
     * Get Grid Column Total Count
     * 
     * @returns 
     */
    GetColumnCount(){
        return this.#spdList.getColumnCount();
    }


    /**
     * Grid에 반영된 모든 행 데이터를 반환.
     * 
     */
    GetAllRowData(){

        let rtn = new Array();
        
        for(let rowIdx=0; rowIdx < this.GetRowCount(); rowIdx++){
            if(!this.CheckFooterIdx(rowIdx)){
                rtn.push(this.GetRowData(rowIdx));
            }
        }

        return rtn;
    }


    /**
     * 특정 행의 데이터를 모두 반환한다.
     * 
     * @param {*} rowIdx 
     * @returns : {'colKey' : colVal ...}
     */
    GetRowData(rowIdx){

        // let rowData = new Object();
        // let columnInfo = new Object();

        // for(let colIdx=0; colIdx < this.GetColumnCount(); colIdx++){
        //     let key = this.GetColumnCodeByIdx(colIdx);
        //     columnInfo[key] = this.getValue(rowIdx, colIdx);

        //     rowData = {...rowData, ...columnInfo};
        // }

        // return rowData;

        return this.spdList.getDataItem(rowIdx);
    }
    

    /**
     * 선택된 모든 행의 데이터를 반환한다.
     * 
     * @param {*} checkBoxIdx : 기준 체크박스 인덱스
     * @returns {Array}
     */
    GetSelectedRowData(checkBoxIdx=0){

        let rtn = new Array();
        let seletedIdx = this.GetCheckedList(checkBoxIdx);

        for(let rowIdx of seletedIdx){

            if(!this.CheckFooterIdx(rowIdx)){
                rtn.push({...{'ROW_IDX' : rowIdx}, ...this.GetRowData(rowIdx)});
            }

        }

        return rtn;
    }


    /**
     * 특정 열의 CheckBox에 선택된 행들의 Index를 얻는다.
     * 
     * @param {Numbber} checkBoxColIdx    : 가져올 행 Index의 기준이되는 열(Check Box Index)
     * @returns {Array} selectIds
     */
    GetCheckedList(checkBoxColIdx=0){

        // TODO :: (1) 예외처리 / (2) N개 이상 체크박스 핸들링
        if( !('COLUMN_TYPE' in this.#columnInfo[checkBoxColIdx]) || !(this.#columnInfo[checkBoxColIdx]['COLUMN_TYPE'] == 'CHECK') ){
            throw `열 인덱스 정보가 잘못됨. (관리자 문의)`;
        }
        
        var selectIds = new Array();	

        this.#spdList.suspendPaint();

        for(var i = 0; i < this.GetRowCount(); i++){
            if((!this.CheckFooterIdx(i)) && this.#spdList.getValue(i, checkBoxColIdx)){
                selectIds.push(i);
            }
        }

        this.#spdList.resumePaint();

        return selectIds;
    }


    /**
     * 컬럼별 콤보박스 바인딩값 기준 변환값 반환 
     * (Value -> Key)
     * 
     * @param {*} colKey        : 컬럼 키값
     * @param {*} comboValue    : 콤보박스 컬럼의 반환값 (value)
     */
    GetComboKeyByValue(colKey, comboValue){

        let columnInfo  = this.#columnInfo.filter((dt)=>{ return dt["COLUMN_CODE"] == colKey; });
        let comboKey    = columnInfo[0]['COLUMN_COMBO'];
        let bindingData = this.#colComboList.get(comboKey);
        let targetIdx   = null;


        targetIdx = bindingData.values.indexOf(comboValue);
        
        if(targetIdx < 0){
            console.log(`[ERROR] Not Found Combo Key... : Column(${colKey}), ComboKey(${comboKey})`);
            return null;
        }

        return bindingData.keys[targetIdx];
    }


//#endregion


//#region   ::  Grid Style


    /**
     * Grid Filter 적용
     * 
     * @param {*} stRow : 시작 행 번호 (0)
     * @param {*} stCol : 시작 열 번호 (1)
     * @param {*} edRow : 종료 행 번호
     * @param {*} edCol : 종료 열 번호
     */
    SetFilter(stRow, stCol, edRow, edCol){
        this.#spdList.rowFilter(new GC.Spread.Sheets.Filter.HideRowFilter(new GC.Spread.Sheets.Range(stRow, stCol, edRow, edCol)));
    }

    /**
     * Grid Sort Range 적용
     * 
     * @param {*} stRow : 시작 행 번호 (0)
     * @param {*} stCol : 시작 열 번호 (1)
     * @param {*} rowCount : 행 수
     * @param {*} colCount : 컬럼 수
     * @param {*} byRows : 행 정렬 설정(true)
     * @param {*} colIdx : 선택한 컬럼 Index
     */
    SetSortRange(stRow, stCol, rowCount, colCount, byRows, colIdx){
        this.#spdList.sortRange(stRow,stCol,rowCount,colCount,byRows,[{index:colIdx, ascending:byRows}]);
    }


    /**
     * 특정 열 값을 기준으로 행 범위 내 동일값은 병합한다.
     * 
     * @param {*} stdColIdx 
     * @param {*} targetColArr 
     */
    SetRowSpan(stdColIdx ,targetColArr){

        var currRow     = "";       // 기준키
        var nextRow     = "";       // 비교키
        var startVal    = [];       // 묶음 시작점
        var countVal    = [];       // 묶음 카운트
        var hitVal      = [];       // 묶음 가운데랑 마지막 합친거 (체크박스 없앨려고)
        var cnt ;	                //i증감 수 담을 변수
        var serOk ;                 //작업했는지 안했는지 판단
        
        this.#spdList.suspendPaint();
        
        var nRow = this.#spdList.getRowCount();
        
        for(var i = 0 ; i < nRow ; ){
            serOk       = false;
            cnt         = 1;        // i 증감 기본값은 1
            currRow      = i; 	    // 현재 행 번호
            nextRow      = i+1;	    // 다음 행 번호

            var hitValue = this.#spdList.getValue(currRow, stdColIdx);  // 기준 열의 값
            
            // 기준 열의 현재 행과 다음 행 값을 비교
            if(hitValue == this.#spdList.getValue(nextRow, stdColIdx)){
                startVal.push(currRow);
                for(var j = i+1 ; j < nRow ; j++){

                    /* 찾으면 가운데묶음에 다넣는다 */
                    if(hitValue == this.#spdList.getValue(j, stdColIdx)){
                        serOk = true;
                        hitVal.push(j);
                        cnt++;
                    }else{
                        /*
                            찾았던 적이있는데 다음꺼랑다르면 가운데묶음 마지막에넣은게 동일값의 마지막부분이라는것을 알게됨
                            가운데묶음의 마지막에 넣은것을 묶음 마지막에 넣어주고 가운데묶음에서는 지운다. 
                        */
                        if(serOk){
                            serOk = false;	        //처리했으니 다시 false로 지정
                            countVal.push(cnt);
                            break;
                        }
                    }
                }
                if(serOk){
                    countVal.push(cnt);
                }
            }
            else{
                // this.#spdList.addSpan(i, 0, 1, 1);
                this.#rowSpanExIdx.push(i);
            }
            //찾은수 만큼 건너뜀
            i = i + cnt;
        }

        // 서식 ?
        for (var i = 0 ; i < startVal.size(); i++){
            for(var j = 0 ; j < targetColArr.length; j++){

                let rowIdx = startVal[i];
                let colIdx = targetColArr[j];

                this.#spdList.addSpan(rowIdx, colIdx, countVal[i], 1);
                this.#spdList.setStyle(rowIdx, colIdx, this.#columnStyles.get(colIdx), GC.Spread.Sheets.SheetArea.viewport);
            }
        }


        this.#spdList.resumePaint();
    }

    /**
     * 특정 열 값을 기준으로 행 범위 내 동일값은 병합제거한다.
     * 
     * @param {*} stdColIdx 
     * @param {*} targetColArr 
     */
    SetRowRsmoveSpan(stdColIdx ,targetColArr){

        var currRow     = "";       // 기준키
        var nextRow     = "";       // 비교키
        var startVal    = [];       // 묶음 시작점
        var countVal    = [];       // 묶음 카운트
        var hitVal      = [];       // 묶음 가운데랑 마지막 합친거 (체크박스 없앨려고)
        var cnt ;	                //i증감 수 담을 변수
        var serOk ;                 //작업했는지 안했는지 판단
        
        this.#spdList.suspendPaint();
        
        var nRow = this.#spdList.getRowCount();
        
        for(var i = 0 ; i < nRow ; ){
            serOk       = false;
            cnt         = 1;        // i 증감 기본값은 1
            currRow      = i; 	    // 현재 행 번호
            nextRow      = i+1;	    // 다음 행 번호

            var hitValue = this.#spdList.getValue(currRow, stdColIdx);  // 기준 열의 값
            
            // 기준 열의 현재 행과 다음 행 값을 비교
            if(hitValue == this.#spdList.getValue(nextRow, stdColIdx)){
                startVal.push(currRow);
                for(var j = i+1 ; j < nRow ; j++){

                    /* 찾으면 가운데묶음에 다넣는다 */
                    if(hitValue == this.#spdList.getValue(j, stdColIdx)){
                        serOk = true;
                        hitVal.push(j);
                        cnt++;
                    }else{
                        /*
                            찾았던 적이있는데 다음꺼랑다르면 가운데묶음 마지막에넣은게 동일값의 마지막부분이라는것을 알게됨
                            가운데묶음의 마지막에 넣은것을 묶음 마지막에 넣어주고 가운데묶음에서는 지운다. 
                        */
                        if(serOk){
                            serOk = false;	        //처리했으니 다시 false로 지정
                            countVal.push(cnt);
                            break;
                        }
                    }
                }
                if(serOk){
                    countVal.push(cnt);
                }
            }
            //찾은수 만큼 건너뜀
            i = i + cnt;
        }

        // 서식 ?
        for (var i = 0 ; i < startVal.size(); i++){
            for(var j = 0 ; j < targetColArr.length; j++){

                let rowIdx = startVal[i];
                let colIdx = targetColArr[j];

                this.#spdList.removeSpan(rowIdx, colIdx);

                this.#spdList.setStyle(rowIdx, colIdx, this.#columnStyles.get(colIdx), GC.Spread.Sheets.SheetArea.viewport);
            }
        }

        this.#spdList.resumePaint();
    }

    /**
     * 선택 행(Checked) 강조서식 적용
     * 
     * @param {*} rowIdx 
     */
    SelectedRowHighLight(rowIdx){
        // ps_setSpreadBackColor_Row(this.#spdList, rowIdx, BKC_SK);

        // TODO :: 서식이 있는 Cell의 경우 덮어씌워야함.
        // for(let colIdx of editColList)
        // {
        //     ps_setSpreadBackColor_Cell(activeSheet, rowIdx, colIdx, BKC_SK);
        // }
    }


    /**
     * 포커싱된 행 강조서식 적용
     * 
     * @requieres : Winus12.js
     */
    FocusedRowHightLight(){

        /** Footer 행은 서식적용하지 않음. */
        if(this.#targetRow == this.#footerLineIdx){
            return ;
        }

        this.#spdList.suspendPaint();

        this.SetColumnFocusStyle(this.#targetRow);          // 강조서식 적용

        if(this.#focuedRowIdx != null && this.#focuedRowIdx != this.#targetRow){

            this.SetColumnDefaultStyle(this.#focuedRowIdx);         // 서식 원복
        }

        this.#spdList.resumePaint();
        this.#focuedRowIdx = this.#targetRow;
    }


    /**
     * Grid 구성정보를 기준으로 컬럼정보(Map) 객체 생성
     * 
     * @param {*} data 
     * @returns 
     */
    SetColumnInfo(data){
	
        var gridSettingList = JSON.parse(data);
        var gridRows = gridSettingList.rows;
        
        this.#columnInfo = gridRows;
    
        var inputMap = new Map();
        for(let item of gridRows){
            inputMap.set(item["COLUMN_CODE"], item["COLUMN_SEQ"]);
            inputMap.set(item["COLUMN_SEQ"], item["COLUMN_CODE"]);

            this.#spdColInfos.push({name : item["COLUMN_NAME"]});
        }

        this.ParseComboList();

    
        return inputMap;
    }


    /**
     * 생성된 컬럼정보(Map)의 각 Sheet서식 적용
     * 
     * (ps_setSpreadColumnIndex)
     * 
     * @param {boolean} isInit : 컬럼별 서식 초기화 여부
     */
    SetColumnFormat(isInit=true){

        let columnCount = this.#columnInfo.length;

        if(isInit){
            ps_setSpreadList_init(this.#spdList, 1, columnCount, 0, this.#frozenColIdx);
        }


        for(var colIdx = 0; colIdx < columnCount; colIdx++){

            let columnData = this.#columnInfo[colIdx];
            let color = "";
            let wrapWord = false;
            let readOnly = columnData.COLUMN_READ_ONLY === 'Y' ? true : false;


            if(this.#setColor)
            {
                // 1. Option Color
                if(readOnly){
                    color = COLOR_PALETTE.get('READONLY');
                }
                // 2. BackGround Color
                else{
                    color = COLOR_PALETTE.get(columnData.COLUMN_COLOR);
                    this.#spdList.getRange(0,-1, colIdx, 1).locked(false);
                }
            }
            else{
                color = COLOR_PALETTE.get('DEFAULT');
            }

            switch(columnData.COLUMN_TYPE){
                case 'CHECK':
                    ps_setSpreadCol_CHECKBOX_Property(this.#spdList, colIdx, 'CENTER', 'CENTER', 8, false, true, "");
                    break;
                case 'TEXT':
                    ps_setSpreadCol_TEXT_Property(this.#spdList, colIdx, 'CENTER', columnData.COLUMN_ALIGN, 8, wrapWord, readOnly, color);
                    break;
                case 'NUMBER':
                    ps_setSpreadCol_NUMBER_Property(this.#spdList, colIdx, 'CENTER', columnData.COLUMN_ALIGN, 8, wrapWord, readOnly, color);
                    break;
                case 'COMBO':
                    color = COLOR_PALETTE.get('COMBO');
                    ps_setSpreadCol_COMBO_Property(this.#spdList, colIdx, 'CENTER', columnData.COLUMN_ALIGN, this.#colComboList.get(columnData.COLUMN_COMBO).values, readOnly, color);
                    break;
                case 'BUTTON':
                    // TODO :: Default Value 설정
                    ps_setSpreadCol_BUTTON_Property(this.#spdList, colIdx, 'CENTER', columnData.COLUMN_ALIGN, 8, false, true, null, columnData.COLUMN_NAME);
                    break;
                case 'TEXT_YN':
                    ps_setSpreadCol_YN_Property(this.#spdList, colIdx, 'CENTER', columnData.COLUMN_ALIGN, 8, false, true, null, '●', '●');
                    break;
            }

            this.AppendColumnStyle(colIdx, wrapWord, readOnly, color, 'CENTER', columnData.COLUMN_ALIGN);
        }
    }


    /**
     * 생성된 컬럼정보(Map)의 각 Header(Title) 서식 적용
     * (ps_setSpreadStyle)
     */
    SetSheetStyle() {

        var spd = this.#spdList;
        var columnCount = this.#columnInfo.length;
        let groupHeaderRange = new Array();
        let useGroupHeader = false;

        groupHeaderRange = this.GetGroupHeaderRange(this.#headerMappingInfo);


        /** Header Depth Setting */
        if(this.#headerMappingInfo.length != 0){
            this.#spdList.setRowCount(2, GC.Spread.Sheets.SheetArea.colHeader);
            useGroupHeader = true;
        }


        /** Header Setting */
        for(let colIdx = 0; colIdx < columnCount; colIdx++){

            let columnData  = this.#columnInfo[colIdx];
            let visible     = columnData.COLUMN_VISIBLE === 'Y';
            let groupInfo   = this.GetGroupHeaderInfo(groupHeaderRange, colIdx);

            if( useGroupHeader ) {
                if(!cfn_isEmpty(groupInfo)) {
                    if( Number(groupInfo['startColIdx']) === colIdx ) {
                        // Column Merge
                        ps_setSpreadTitle_Span(this.#spdList, 0, groupInfo['startColIdx'], 1, groupInfo['mergeCnt']);
                        ps_setSpreadTitle_Named(spd, 0, colIdx, groupInfo['name'], 0, true); // [Winus12.js]
                    } 
                    
                    ps_setSpreadTitle_Named(spd, 1, colIdx, columnData.COLUMN_NAME, columnData.COLUMN_SIZE, visible); // [Winus12.js]
                }
                else {
                    /** 그룹헤더 X : 1,2행 병합 */
                    ps_setSpreadTitle_Span(this.#spdList, 0, colIdx, 2, 1); // Row Merge
                    ps_setSpreadTitle_Named(spd, 0, colIdx, columnData.COLUMN_NAME, columnData.COLUMN_SIZE, visible);
                    ps_setSpreadTitle_Named(spd, 1, colIdx, columnData.COLUMN_NAME, columnData.COLUMN_SIZE, visible);
                }
            }
            else {
                ps_setSpreadTitle_Named(spd, 0, colIdx, columnData.COLUMN_NAME, columnData.COLUMN_SIZE, visible); // [Winus12.js] 
            }


            // ps_setSpreadTitle_Named(spd, 0, colIdx, columnData.COLUMN_NAME, columnData.COLUMN_SIZE, visible); // [Winus12.js]

            // TODO :: AutoFix Option 개발 - 데이터의 최소크기로 고정되지만, 헤더 영역크기 무시되는 듯.
            // if(['0', '1', '2'].includes(String(colIdx))){
            //     this.#spdList.autoFitColumn(colIdx);
            // }
        }
    
        this.#spdList.defaults.colHeaderRowHeight   = this.#headerHeight;
        this.#spdList.defaults.rowHeight            = this.#rowHeight;

        var setHearderRow = spd.getRange(0, -1, 2, -1, GC.Spread.Sheets.SheetArea.colHeader);
        
        setHearderRow.backColor("#d5dce6");             // Grid Header 배경색상
        setHearderRow.foreColor("#4e5661");             // Grid Header 글자색상
        setHearderRow.font("bold 13px Arial");          // Grid Header 글자서식


        // [Contents] 행 병합
        if(this.#rowSpanColArr != null && this.#rowSpanColArr.length != 0){
            this.SetRowSpan(this.#rowSpanColIdx, this.#rowSpanColArr);
        }

        // this.AddRowStateRule();
    }


    // AddRowStateRule(){
    //     var cfs = this.#spdList.conditionalFormats;
    //     // var ruleType = GC.Spread.Sheets.ConditionalFormatting.RuleType.rowStateRule;
    //     // var hoverstate = GC.Spread.Sheets.RowColumnStates.hover;
    //     var style = new GC.Spread.Sheets.Style("yellow");
    //     var ranges = [new GC.Spread.Sheets.Range(-1, -1, -1, -1)];
    //     var rule = new GC.Spread.Sheets.ConditionalFormatting.StateRule(32, style, ranges);
    //     cfs.addRule(rule);
    // }


    /**
     * Footer 포맷 설정 (출력행 생성)
     * @todo : 통계량 계산값 반영 함수 (통계치는 Service or Query에서 계산되어야함.)
     * 
     * @param {*} footerText 
     */
    SetFooterFormat(footerText='합계'){

        let footerRowStyle = new GC.Spread.Sheets.Style();
        footerRowStyle.font = 'bold 14px arial';
        footerRowStyle.locked = true;
        this.#footerLineIdx = this.GetRowCount();

        this.#spdList.suspendPaint();

        // 행 추가
        this.#spdList.addRows((this.#footerLineIdx + 1), 1);

        // 텍스트란 설정
        this.#spdList.setCellType(this.#footerLineIdx, this.#checkFirstIdx, new GC.Spread.Sheets.CellTypes.Text());		
		this.#spdList.setValue(this.#footerLineIdx, this.#checkFirstIdx, footerText);

        // 틀고정
        // this.#spdList.frozenTrailingRowCount(1);
        
        // 서식지정
        this.#spdList.setStyle(this.#footerLineIdx, -1, footerRowStyle, GC.Spread.Sheets.SheetArea.viewport);
        ps_setSpreadBackColor_Row(this.#spdList, this.#footerLineIdx, COLOR_PALETTE.get('DFC_LINE'));

        // 컬럼별 Cell 서식 초기화 (버튼, Combo ...)
        this.spdList.setCellType(this.#footerLineIdx, -1, new GC.Spread.Sheets.CellTypes.Text(), GC.Spread.Sheets.SheetArea.viewport);


        this.#spdList.resumePaint();	
    }


    /**
     * 컨텍스트 메뉴 생성 (구성/서식/이벤트)
     * 
     * [Custom Option]
     * 1. type      : 메뉴서식 타입 (header / text/ childText)
     * 2. action    : 메뉴클릭 이벤트
     * 
     * @returns 
     */
    SetContextMenu(){

        let menuNameList = new Array();
        let menuMappingInfo = new Map();
        let commandManager = this.#spdSheet.commandManager();
        let vm = this;

        if (this.#contextMenuInfo.length === 0) {
            console.log('컨텍스트 메뉴 정보 없음.(contextMenuInfo)');
            return;
        }

        this.#spdSheet.options.allowContextMenu = this.#usedContextMenu;        // 컨텍스트 메뉴 출력 허용

        function ContextMenu(){}
        ContextMenu.prototype = new GC.Spread.Sheets.ContextMenu.ContextMenu(this.#spdSheet);
        ContextMenu.prototype.onOpenMenu = function(menuData, itemsDataForShown,hitInfo,spread){
            
        }

        this.#spdSheet.contextMenu = new ContextMenu();       // Custom 형태로 재생성
        this.#spdSheet.contextMenu.menuData.clear();


        /** 주메뉴 정보 처리 맵핑정보 생성 */
        for(let info of this.#contextMenuInfo){
            
            menuMappingInfo.set(info.name, { 'event': info.action, 'type': info.type ?? 'text', depth: '0' } );
            menuNameList.push(info.name);

            delete info.action;

            /** 소메뉴 정보 처리 맵핑정보 생성 */
            if('subMenu' in info){
                for(let subMenu of info['subMenu']){
                    
                    menuMappingInfo.set(subMenu.name, { 'event': subMenu.action, 'type': subMenu.type ?? 'text', depth: '1' });
                    menuNameList.push(subMenu.name);

                    delete subMenu.action;
                }
            }
        }


        /** Item Binding */
        this.#contextMenuInfo.map(dt => this.#spdSheet.contextMenu.menuData.push(dt));


        /** Event Binding  */
        let callback = function(args, target, setShortcutKey){
            let eventFnc = menuMappingInfo.get(target.cmd)['event'];
            if(eventFnc != undefined){
                eventFnc.call(vm, target);
            }
        }

        // const events = callback;
        // function* callback(args, target, setShortcutKey){
        //     let eventFnc = menuMappingInfo.get(target.cmd)['event'];
        //     if(eventFnc != undefined){
        //         // eventFnc.call(this, target.cmd);
        //         yield eventFnc(target.cmd);
        //     }
        // }

        for(let menuNm of menuNameList){
            commandManager.register(menuNm, callback, null, false, false, false, false);
        }
        

        /** 메뉴별 서식지정  */
        function CustomMenuView(){}
        CustomMenuView.prototype = new GC.Spread.Sheets.ContextMenu.MenuView();
        CustomMenuView.prototype.createMenuItemElement = function (menuItemData) {

            var self = this;
            let type = menuMappingInfo.get(menuItemData.name)['type'];
            let depth = menuMappingInfo.get(menuItemData.name)['depth'];

            var menuItemView = GC.Spread.Sheets.ContextMenu.MenuView.prototype.createMenuItemElement.call(self, menuItemData);

            switch(type){

                case 'header' :
                    menuItemView[0].style.backgroundColor = '#d5dce6';
                    menuItemView[0].style.textTransform = 'none';
                    break;

                case 'childText' :
                    menuItemView[0].style.marginLeft = '1em';
                    break;

                case 'text' : 
                    break;
            }

            return menuItemView;
        }

        this.#spdSheet.contextMenu.menuView = new CustomMenuView();
    }


    /**
     * Sheet의 가로 스크롤 바 출력여부 설정
     * 
     * @desc : 활성화된 탭에서 검색할 경우 횡 스크롤이 사라지는 현상이 있음.
     * @param {*} visible 
     */
    ShowHorizontalScrollbar(visible){
        this.#spdSheet.options.showHorizontalScrollbar = null;
        this.#spdSheet.options.showHorizontalScrollbar = visible;
    }


    /**
     * Grid 컬럼 서식, 값에 대한 format을 커스텀값으로 재설정
     * ! Cell의 Value값이 반드시 있어야함.
     * 
     * @param {*} colKey                    : 컬럼 키
     * @param {function} formatFunction     : 커스텀 함수 (paramters {obj: cell 현재정보, formatterData: cell의 format 정보 })
     */
    SetCustomColumn(colKey, formatFunction){

        let colIdx  = this.GetColumnIdx(colKey);
        let cell    = this.#spdList.getCell(-1, colIdx, GC.Spread.Sheets.SheetArea.viewport);


        var customFormatterTest = {};
        customFormatterTest.prototype = GC.Spread.Formatter.FormatterBase;

        if((formatFunction != null) && (typeof(formatFunction) == 'function')){
            customFormatterTest.format = formatFunction;
        }

        // customFormatterTest.format = function (obj, formatterData) {

        //     let value = null;

        //     formatterData.conditionalForeColor = '';                // 색상지정
        //     valeu = '';                                             // 값 지정
                
        //     return valeu;
        // };

        customFormatterTest.parse = function (str) {
            if (!str) {
                return "";
            }
            return str;
        }

        cell.formatter(customFormatterTest);
    }


    /**
     * 특정 열의 셀 서식(Style)을 적용한다.
     * 
     * @param {*} rowIdx : 행 인덱스
     * @param {*} colIdx : 열 인덱스
     * @param {GC.Spread.Sheets.Style} style  : 열 서식
     */
    SetColumnStyle(rowIdx, colIdx, style){
        this.#spdList.setStyle(rowIdx, colIdx, style, GC.Spread.Sheets.SheetArea.viewport);
    }


    /**
     * 특정 열의 셀 서식(Style)을 가져온다.
     * 
     * @param {*} colIdx 
     * @returns {GC.Spread.Sheets.Style}
     */
    GetColumnStyle(colIdx){
        return this.#columnStyles.get(colIdx).clone();
    }


//#endregion


//#region   ::  Utility


    ExcelDownLoad(fileName, isSpan=false){
        
        var excelIo = new GC.Spread.Excel.IO();
        var fileName = fileName + ".xlsx";
        var serializationOption = {
                includeBindingSource : true,
                ignoreStyle : false,
                ignoreFormula : true,
                rowHeadersAsFrozenColumns : false,
                columnHeadersAsFrozenRows : true
        }
        
        let serializeData = this.#spdSheet.toJSON(serializationOption);
        let tempJson = JSON.parse(JSON.stringify(serializeData));           // Deep Copy
        let columnInfo        = new Array();
        let columnDataArray   = new Array();
        let rowDataArray      = new Array();
        let dataArray         = new Array();

        columnInfo       = tempJson.sheets.Sheet1.columns.clone();               // 컬럼 정보
        columnDataArray  = tempJson.sheets.Sheet1.data.columnDataArray.clone();  // 열 서식
        rowDataArray     = tempJson.sheets.Sheet1.data.rowDataArray.clone();     // 행 서식
        dataArray        = tempJson.sheets.Sheet1.data.dataTable;                // 데이터

        var defaultDataNode = tempJson.sheets.Sheet1.data.defaultDataNode;
        let headerDepth     = this.#headerMappingInfo.length === 0 ? 0 : 1;      // 헤더그룹 사용여부에 따른 헤더 행 개수
        let spanInfos       = tempJson.sheets.Sheet1.spans;

        /** Options */
        tempJson.sheets.Sheet1.isProtected = false;         // 읽기전용 (true / false)

        /** 셀 병합 적용 여부 (false : 헤더 제외 span 정보 제거) */
        if(!isSpan && spanInfos != null){
            tempJson.sheets.Sheet1.spans = spanInfos.filter(dt => { return dt.row <= headerDepth} );
        }

        /** Cell 단위 Style 적용 (열 > 행 순서로 덮어쓰기) */
        for(var i = 0 ; i < rowDataArray.length ; i++){
            for(var j = 0 ; j < columnDataArray.length ; j++){

                if((dataArray[i][j] != null)){

                    if(dataArray[i][j].style == null){
                        dataArray[i][j].style = {};
                        Object.assign(dataArray[i][j].style, defaultDataNode.style);    // 기본서식적용
                    }
                    
                    if(columnDataArray[j] != null && columnDataArray[j].style != null){
                        Object.assign(dataArray[i][j].style, columnDataArray[j].style); // 열 서식 적용
                    }

                    if(rowDataArray[i] != null && rowDataArray[i].style != null){
                        Object.assign(dataArray[i][j].style, rowDataArray[i].style);    // 행 서식 적용
                    }

                    if(headerDepth >= i){
                        Object.assign(dataArray[i][j].style, { 'vAlign' : 1, 'hAlign' : 1 } );  // Header의 경우 가운데 정렬
                    }
                }
            }
        }

        // 엑셀 다운로드 시, 제외 행 (숨김처리)
        if(this.#exFileExportArr.length > 0 ){
            for(const exColIdx of this.#exFileExportArr){
                columnInfo[exColIdx].visible = false;
            }
            
            tempJson.sheets.Sheet1.columns = columnInfo;
        }

        var json = JSON.stringify(tempJson);
        excelIo.save(json,function(blob){
            saveAs(blob,fileName);
        }, function(e){
            console.log(e);
        });
    }


    GetGroupHeaderRange(headerInfo){

        let groupHeaderInfos = new Array();
    
        
        for( let info of headerInfo ) {

            let startColIdx = this.GetColumnIdx(info['startColumnKey']);
            let endColIdx   = this.GetColumnIdx(info['endColumnKey']);
            let mergeCnt    = Number(endColIdx) - Number(startColIdx) + 1;

            groupHeaderInfos.push({ ...info, ...{ 'startColIdx': startColIdx, 'endColIdx': endColIdx, 'mergeCnt': mergeCnt }});
        }

    
        return groupHeaderInfos;
    }


    GetGroupHeaderInfo(groupHeaderRange, headerIdx){

        let groupHeaderInfo = groupHeaderRange.filter(dt => {
            if((headerIdx >= Number(dt['startColIdx'])) && (headerIdx <= Number(dt['endColIdx']))){

                return dt;
            }
        });


        return groupHeaderInfo[0];
    }


    AppendColumnStyle(colIdx, wrapWord, readOnly, color, vAlign, hAlign){
        
        let style = new GC.Spread.Sheets.Style();
        let vAlignValues = ['TOP', 'CENTER', 'BOTTOM'];
        let hAlignValues = ['LEFT', 'CENTER', 'RIGHT'];
        
        style.backColor         = color;
        style.borderTop         = new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin);
        style.borderBottom      = new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin);
        style.borderRight       = new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin);
        style.borderLeft        = new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin);
        style.vAlign            = vAlignValues.indexOf(vAlign);
        style.hAlign            = hAlignValues.indexOf(hAlign);
        style.locked            = readOnly;

        this.#columnStyles.set(colIdx, style);
    }


    /**
     * Footer 영역 행인지 판별 
     * 
     * @param {*} rowIdx    : 행 Index
     * @returns {boolean} rtn
     */
    CheckFooterIdx(rowIdx){
        // [Footer] 행추가로 인한 변경된 통계치 표시라인 인덱스
        let changeFooterLineIdx= this.GetRowCount();     
        //행추가로 인한 통계치 라인 변경 값을 비교
        if(this.#footerLineIdx != changeFooterLineIdx - 1)
            return (this.#usedFooter && (changeFooterLineIdx - 1 == rowIdx));
        else
            return (this.#usedFooter && (this.#footerLineIdx == rowIdx));
    }


    /**
     * 행 목록 정렬
     * 
     * @param {*} colIdx : 선택한 컬럼 Index
     */
    SortRowData(startRowIdx, colIdx){
        let fixRow = 0;
        // let fixRow = startRowIdx + 1; // 고정 행 값 (헤더에 해당하는 행)

        // this.#spdList.sortRange(fixRow, 0, (this.GetRowCount() - fixRow), this.GetColumnCount(), true, [{index:colIdx, ascending:false}]);

        // [Contents] 행 병합 제거
        if(this.#rowSpanColArr != null && this.#rowSpanColArr.length != 0){
            this.SetRowRemoveSpan(this.#rowSpanColIdx, this.#rowSpanColArr);
        }

        if(this.#footerLineIdx != null){
            fixRow++;
        }

        this.#spdList.sortRange(0, 0, (this.GetRowCount() - fixRow), this.GetColumnCount(), true, [{index:colIdx, ascending:this.#sortingToggle}]);

        this.#sortingToggle = !this.#sortingToggle;

        // [Contents] 행 병합
        if(this.#rowSpanColArr != null && this.#rowSpanColArr.length != 0){
            this.SetRowSpan(this.#rowSpanColIdx, this.#rowSpanColArr);
        }
    }


//#endregion


//#region   ::  Grid Event CallBack


    /**
     * 행 추가
     * 
     * @param {boolean} isLocked      : ReadOnly 컬럼 잠금해제
     * @param {Array} unlockColumns   : ReadOnly 해제 컬럼 인덱스 배열
     * @param {boolean} isChecked     : 추가 행의 선택열(CheckBox) 선택값 여부
     */
    RowAddEventCallBack(isLocked=false, unlockColumns, isChecked){

        let lastCheckBoxIdx = 0;
        let latRowIdx = this.#spdList.getRowCount();

        this.#spdList.addRows(latRowIdx, 1);

        // 특정 열만 잠금 해제
        if(unlockColumns != undefined && unlockColumns != null && unlockColumns.length > 0){
            for(let unlockColIdx of unlockColumns){
                this.#spdList.getCell(latRowIdx, unlockColIdx).locked(isLocked);
            }
        }
        // 모든 열 잠금 해제
        else{
            for(let colIdx=0; colIdx < this.GetColumnCount(); colIdx++){
                this.#spdList.getCell(latRowIdx, colIdx).locked(isLocked);
            }
        }

        if(this.#multiCheckRange != null && this.#multiCheckRange.length > 0){
            lastCheckBoxIdx = Math.max(...this.#multiCheckRange);       // 선택열이 복수개인 경우 가장 마지막 열만 선택
        }

        this.setValue(latRowIdx, lastCheckBoxIdx, isChecked);

        return latRowIdx;
    }


    /**
     * 특정 행 추가
     * 
     * @param {*} rowIdx              : 선택한 행 Index
     * @param {boolean} isLocked      : ReadOnly 컬럼 잠금해제
     * @param {Array} unlockColumns   : ReadOnly 해제 컬럼 인덱스 배열
     * @param {boolean} isChecked     : 추가 행의 선택열(CheckBox) 선택값 여부
     */
    SelectedRowAddEventCallBack(rowIdx, isLocked=false, unlockColumns, isChecked){

        let lastCheckBoxIdx = 0;

        let latRowIdx = rowIdx + 1;

        this.#spdList.addRows(latRowIdx, 1);

        // 특정 열만 잠금 해제
        if(unlockColumns != undefined && unlockColumns != null && unlockColumns.length > 0){
            for(let unlockColIdx of unlockColumns){
                this.#spdList.getCell(latRowIdx, unlockColIdx).locked(isLocked);
            }
        }
        // 모든 열 잠금 해제
        else{
            for(let colIdx=0; colIdx < this.GetColumnCount(); colIdx++){
                this.#spdList.getCell(latRowIdx, colIdx).locked(isLocked);
            }
        }

        if(this.#multiCheckRange != null && this.#multiCheckRange.length > 0){
            lastCheckBoxIdx = Math.max(...this.#multiCheckRange);       // 선택열이 복수개인 경우 가장 마지막 열만 선택
        }

        this.setValue(latRowIdx, lastCheckBoxIdx, isChecked);

        return rowIdx;
    }


    RowDeleteAllEventCallBack(){

        this.#spdList.deleteRows(0, this.#spdList.getRowCount());

    }


    /**
     * Grid 셀 클릭 이벤트 콜백함수
     * 
     * @param {*} sender 
     * @param {*} args
     */
    CellClickEventCallBack(sender, args){

        this.#targetRow = args.row;
        this.#targetCol = args.col;
        
        let value = this.getValue(args.row, args.col);

        /* 전체선택 체크박스 클릭 */
        if((args.row == 0) && (args.col == this.#checkFirstIdx)){
            this.SetSelectAll(args.col, !value);
        }

        // Custom Event
        if(this.#cellClickCustomEvent != null){
            this.#cellClickCustomEvent.call(this, sender, args);
        }
    }


    CellDoubleClickEventCallBack(sender, args){

        let headerDepth = this.#headerMappingInfo.length === 0 ? 0 : 1;         // 헤더그룹 사용여부에 따른 헤더 행 개수

        /** Grid Header 영역 */
        if(args.row <= headerDepth){
            if(this.#useSort) this.SortRowData(headerDepth, args.col);
        }

        // Custom Event
        if(this.#cellDoubleClickEvent != null){
            this.#cellDoubleClickEvent.call(this, sender, args);
        }
    }


    ButtonClickEventCallBack(sender, args){

        this.#targetRow = args.row;
        this.#targetCol = args.col;

        const columnType = this.#columnInfo.filter((dt)=> {return dt.COLUMN_SEQ == (args.col + 1)})[0].COLUMN_TYPE;
        let value = (!this.getValue(args.row, args.col)) ?? true;


        /* 1. 행 선택 Check Box 클릭 */
        if(columnType == 'CHECK' || args.col == this.#checkFirstIdx){
            this.setValue(args.row, args.col, value);
            console.log(value);
        }

        /* 2. 행 선택 하위 레벨 Check Box 값 반영 */
        if((this.#multiCheckRange != null) && (this.#rowSpanColIdx != null)){
            
            // 행 선택 Check Box 열인지 확인
            if(this.#multiCheckRange.includes(Number(args.col))){
                
                let childrenColIdx  = this.#multiCheckRange.filter((colIdx)=>{ return colIdx > Number(args.col) }); // 선택된 CheckBox열의 하위 CheckBox 열 인덱스 추출
                let spanInfo = this.#spdList.getSpans().filter(dt=> {return (dt.row == args.row && dt.col == args.col) })[0];

                // [Loop] 선택 Check Box 하위 열 순회
                for(let colIdx of childrenColIdx){

                    if(spanInfo == null || spanInfo == undefined){
                        this.setValue(args.row, colIdx, value);
                    }

                    else{
                        // [Loop] 병합 범위의 하위 행 순회
                        for(let rowIdx=0; rowIdx < spanInfo.rowCount; rowIdx++){
                            this.setValue((spanInfo.row + rowIdx), colIdx, value);
                        }
                    }
                }
            }
        }

        if(this.#buttonClickCustomEvent != null){
            this.#buttonClickCustomEvent.call(this, sender, args);
        }
    }


    CellChangedEventCallBack(sender, args){

        let rowIdx = args.row;
        let colIdx = args.col;

        if(this.#cellChangedCustomEvent != null){
            this.#cellChangedCustomEvent.call(this, sender, args);
        }

    }


    CellEditedEventCallBack(sender, args){

        let rowIdx = args.row;
        let colIdx = args.col;

        this.#targetRow = args.row;
        this.#targetCol = args.col;

        if(this.#cellEditedCustomEvent != null){
            this.#cellEditedCustomEvent.call(this, sender, args);
        }
    }


    SelectionChangedEventCallBack(sender, args){

        this.#oldSelections = args.oldSelections;
        this.#newSelections = args.newSelections;

        this.FocusedRowHightLight();

        // Custom Event
        if(this.#selectionChangedCustomEvent != null){
            this.#selectionChangedCustomEvent(sender, args);
        }
    }


//#endregion


//#region   :: Optional Function


    /**
     * 특정 행의 행 단위 CheckBox를 선택한다.
     * (최하위 레벨의 행)
     * 
     * - Grid Custom Event와 함께 사용
     * 
     * @param {*} rowIdx 
     * @param {*} colIdx 
     * @param {boolean} value     : 체크박스 선택여부
     */
    SetCheckedUnitRow(rowIdx, colIdx, value){
        let lastCheckBoxIdx = this.#multiCheckRange ? Math.max(...this.#multiCheckRange) : this.#checkFirstIdx;
        this.setValue(rowIdx, lastCheckBoxIdx, value);
    }


    SetCheckedSpanRowAll(){
        // this.#spdList.getSpans(new GC.Spread.Sheets.Range(1,0,1,1))
    }


//#endregion


}


/** Grid Cell Color (by WinusCode.js) */
const COLOR_PALETTE = new Map([
    // 1. Option Color
    ['DEFAULT'   ,   BKC_WT]
    ,['READONLY'  ,   BKC_LCK]
    , ['COMBO', '#FFFBF7']

    // 2. Custom Color
    ,['RED'       ,   BKC_ALT]
    ,['BLUE'      ,   BKC_PRI]
    ,['GREEN'     ,   BKC_SUC]

    // 3. Footer
    ,['DFC_LINE'  ,   DFC_LINE]
]);


/** 하단 합계 취합 타입 */
const STATISTICS_TYPE = {
    'COUNT' : 'COUNT',
    'SUM'   : 'SUM'
}


const CELL_V_ALIIGN = {
    'TOP'       :   0,
    'CENTER'    :   1,
    'BOTTOM'    :   2
}


const CELL_H_ALIIGN = {
    'LEFT'      :   0,
    'CENTER'    :   1,
    'RIGHT'     :   2
}