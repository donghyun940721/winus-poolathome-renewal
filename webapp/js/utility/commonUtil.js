function WareHousePopUpEventCallBack(){}
function CustPopUpEventCallBack(){}
function CustShopPopUpEventCallBack(){}
function ItemPopUpEventCallBack(){}
function LocPopUpEventCallBack(){}
function LocPopUpEventCallBack2(){}


function fn_Popup(type, param={}, callBack){
	var strParameter = [];
	var url = '';
	var width = "800";
	var height = "530";
    let callback = null;
	
	switch(type){

		//* 창고
		case 'WH':
			callback = 'WareHousePopUpEventCallBack';
			param = `?func=${callback}`;
			url = "/WMSMS040/pop.action" + param;
			break;

        //* 화주
        case 'CUST':
            callback = "CustPopUpEventCallBack";
			strParameter.push('CUST_CD' in param ? param['CUST_CD'] : null);			// 화주코드
			strParameter.push('CUST_NM' in param ? param['CUST_NM'] : null);			// 화주명
			strParameter.push('');														// 거래처 ID
			strParameter.push('');														// LC_ID
			strParameter.push('');														// LC_ALL
			param = `?func=${callback}&custType=12&strParameter=${strParameter}`;
			url = "/WMSCM011.action" + param;
            break;

		//* 거래처
		//! 거래처 코드(custType)에 따라 조회 조건이 다름
		case 'CUST_SHOP' : 
			callback = 'CustShopPopUpEventCallBack';
			strParameter.push('CUST_CD' in param ? param['CUST_CD'] : null);				// 검색코드 (거래처)
			strParameter.push('CUST_NM' in param ? param['CUST_NM'] : null);				// 검색명칭 (거래처)
			strParameter.push('TRANS_CUST_ID' in param ? param['TRANS_CUST_ID'] : null);	// 거래처 ID
			strParameter.push('LC_ID' in param ? param['LC_ID'] : null);					// 센터 ID
			strParameter.push('LC_ALL');													// LC_ALL
			param  = `?func=${callback}&custType=3&strParameter=${strParameter}&stBtnYn=N`;
			url    = "/WMSCM011.action" + param;

			break;

		//* 로케이션
		case 'LOCATION': 
			callback = "LocPopUpEventCallBack";
			strParameter.push('');
			strParameter.push('');
			strParameter.push('');
			strParameter.push('');
			strParameter.push('');
			strParameter.push('');  
			strParameter.push('');
			strParameter.push('Y');
			param = `?func=${callback}&strParameter=${strParameter}`;
			url = "/WMSCM080.action" + param;

			break;

        //  // 입고처 (화주조회)
		// case 'INCUST' : 
		// 	if("" == jQuery("form[name='frm_list'] input[name='vrSrchCustCd']").val()){
		// 		alert("$!message.getMessage('0가없음', '" + custCd_Text + "')");
		// 		fn_Popup('CUST');
		// 		return false;
		// 	}else{
		// 		strParameter.push('');
		// 		strParameter.push('');
		// 		strParameter.push('108');
		// 		strParameter.push('');
		// 		strParameter.push(jQuery("form[name='frm_list'] input[name='vrSrchCustId']").val());
		// 		strParameter.push('1');
		// 		param = `?func=${selectedInCustCallBack.name}&strParameter=${strParameter}`;
		// 		url = "/TMSCM010.action" + param;
		// 	}	
        //     break;	

		//* 상품코드
        case 'ITEM':	
            callback = "ItemPopUpEventCallBack";
            strParameter.push('RITEM_CD' in param ? param['RITEM_CD'] : null);		// 상품코드
            strParameter.push('RITEM_NM' in param ? param['RITEM_NM'] : null);		// 상품명
            strParameter.push('CUST_ID' in param ? param['CUST_ID'] : null);		// 화주ID
            strParameter.push('');													// 창고ID
            strParameter.push('');													// SET상품을 보여줄 것인지 여부
            strParameter.push('Y');													//재고유무 관계없이 표시여부
            param = `?func=${callback}&strParameter=${strParameter}`;
            url = "/WMSCM091.action" + param;
            break;
		
		//* 로케이션
		case 'LOC': 
			callback = "LocPopUpEventCallBack2";
			strParameter.push('LOC_CD' in param ? param['LOC_CD'] : null);
			strParameter.push('LOC_ID' in param ? param['LOC_ID'] : null);
			param = `?func=${callback}&strParameter=${strParameter}`;
			url = "/WMSCM080.action" + param;
			break;	
	}
	
    var asppop = cfn_openPop2(url, "fn_Popup", width, height);
	asppop.focus();
}


/**
 * Velocity로 치환된 콤보박스 문자열 값을 파싱. 
 * $!comCode.getGridComboComCode
 * 
 * @param {*} name 	: 콤보박스 Key 값
 * @param {*} arg 	: 치환된 문자열 Object
 * @returns 
 */
function ParseComboString(name, arg){
	var rtn  = {};
	var all = [];
	var keys = [];
	var values = [];

	var comboStringList = arg.split(';');

	for(var i = 0; i < comboStringList.length; i++) {
		var comboArr = comboStringList[i].split(':');
		all.push([cfn_trim(comboArr[0]), cfn_trim(comboArr[1])]);
		keys.push(cfn_trim(comboArr[0]));
		values.push(cfn_trim(comboArr[1]));
	}
	rtn[name] = {};
	rtn[name].all = all;
	rtn[name].keys = keys;
	rtn[name].values = values;

	return {
		'all' 		: all,
		'keys'		: keys,
		'values'	: values
	}
}