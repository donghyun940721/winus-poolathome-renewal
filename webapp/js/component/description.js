/** 숫자목록 (원형테두리) */
export const ROUND_NUMBER = {
    '1' : '①',
    '2' : '②',
    '3' : '③',
    '4' : '④'
}


/** 글자서식 */
export const HIGHLIGHT = {
    'DEFAULT'   : `style='font-size:16px;'`,
    'BOLD_BLUE' : `style='font-size:16px; font-weight:bold;' color='blue'`,
    'BOLD_RED'  : `style='font-size:16px; font-weight:bold;' color='red'`
}


/**
 * 안내툴팁 Display 토글 이벤트
 * 
 * @param {*} id            : 이벤트 바인딩 Component ID
 * @param {*} contentId     : Display 대상 영역 Container ID
 */
export let BindHoldingEvent = function(id, contentId){

    document.getElementById(id).addEventListener('click', (e)=>{

        let childNode = document.getElementById(contentId).children;
        let display = childNode[0].style.display == 'none';

        for(let node of childNode){
            display ? node.show() : node.hide();
        }
        
    });
}


/**
 * 텍스트라인 추가
 * 
 * @param {*} id            : 추가 영역태그 ID
 * @param {*} number        : 목록 숫자 (ROUND_NUMBER)
 * @param {*} text1         : 입력 텍스트 1 
 * @param {*} text2         : 입력 텍스트 2
 * @param {*} highlight1    : 텍스트1의 style 속성 정의 (HIGHLIGHT)
 * @param {*} highlight2    : 텍스트2의 style 속성 정의 (HIGHLIGHT)
 */
export let Add = function(id, number, text1, text2, highlight1, highlight2){

    let textContainer = document.getElementById(id);
    let content = document.createElement('div');

    let textline = `<font style='font-size:16px;'>${number ?? ''}</font> 
                    <font ${highlight1}>${text1 ?? ''}</font> 
                    <font ${highlight2}>${text2 ?? ''}</font>
                    <br>`;
    
    content.innerHTML = textline;
    textContainer.appendChild(content);
}