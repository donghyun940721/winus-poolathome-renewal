/**
 * UI 공통 Compoent
 * 
 * CODE/ID/NAME 으로 구성된 컴포넌트 셋에 대한 인스턴스 생성
 * Changed 이벤트를 통한 키워드 검색기능 제공 (Service 호출)
 * 
 * !필수 정의요소
 * UI       : 'COMP_CATEGORY', 'COMP_PARAM_MAP'
 * SERVICE  : 'CmBeanServiceImpl.java(selectMngCode)' 외 각 호출 쿼리
 * 
 */
export class Component {

    #code       = null;     // 태그 ID값
    #id         = null;     // 태그 ID값
    #name       = null;     // 태그 ID값
    #category   = null;     // 'COMP_CATEGORY'에 정의된 구분값


    get Code()      { return this.#code;         }
    get Id()        { return this.#id;           }
    get Name()      { return this.#name;         }
    get Category()  { return this.#category;     }


    constructor(args){

        if(args != undefined){

            this.#code          = "code"        in args     ?    args.code         : "";
            this.#id            = "id"          in args     ?    args.id           : "";
            this.#name          = "name"        in args     ?    args.name         : "";
            this.#category      = "category"    in args     ?    args.category     : "";

        }

    }


    Init () {
        
        jQuery(`#${this.#code}`).val("");
        jQuery(`#${this.#id}`).val("");
        jQuery(`#${this.#name}`).val("");

    }


    SetValue(code, id, name) {

        jQuery(`#${this.#code}`).val(code);
        jQuery(`#${this.#id}`).val(id);
        jQuery(`#${this.#name}`).val(name);

    }


    /**
     * 컴포넌트의 타입에 맞는 값 획득
     * 
     * @param {*} type : COMP_VALUE_TYPE 상수값
     * @returns 
     */
    GetValue(type) {

        let value = null;

        switch(type)
        {
            case COMP_VALUE_TYPE.CODE : 
                value = jQuery(`#${this.#code}`).val();
                break;

            case COMP_VALUE_TYPE.ID : 
                value = jQuery(`#${this.#id}`).val();
                break;

            case COMP_VALUE_TYPE.NAME : 
                value = jQuery(`#${this.#name}`).val();
                break;
        }

        return value;
    }


    /**
     * 키워드 검색
     * 
     * @param {String} type     : 코드 혹은 명칭 (COMP_VALUE_TYPE)
     * @param {*} refValue      : 입력값
     */
    Search(type=COMP_VALUE_TYPE.CODE, refValue='') {

        const vm = this;
        const refKey = COMP_PARAM_MAP.get(this.#category)[type];
        let formData  = new FormData();

        formData.append('srchKey', this.#category);
        formData.append(refKey, refValue);

        return new Promise((resolve, reject)=>{

            jQuery.ajax({
                url : "/selectMngCode.action",
                type : 'post',
                data : formData,
                contentType : false,
                processData : false,
                success : function(result){

                    // TODO :: Service에서 동일 명칭으로 리턴됨. (Service 반환Key 수정 필요)
                    const type = (vm.#category == COMP_CATEGORY.USER2) ? COMP_CATEGORY.USER : vm.#category;

                    result = result[type];

                    // 값 없음.
                    if(result == null || result.length == 0) { 
                        vm.Init();
                        reject(result);
                    }
                    // 단일 검색결과 획득 시 즉시 적용
                    else if(result.length == 1){
                        vm.SetValue(
                            result[0][COMP_VALUE_TYPE.CODE], 
                            result[0][COMP_VALUE_TYPE.ID], 
                            result[0][COMP_VALUE_TYPE.NAME] ?? result[0][COMP_VALUE_TYPE.CODE]
                        )
                    }
    
                    resolve(result);
                },
                error : function(xhr, textStatus, err){
                    reject(err);
                }

            });

        });
    }


    GetCustomProperty(category){

        switch(category){

            case COMP_CATEGORY.CUST : 
                break;

        }

    }

}


export const Utility = (function(){


    const GetData = (category) => {

        const url      = '/selectMngCode.action';
        let param      = `srchKey=${category}`;

        return jQuery.post(url, param);
    }


    return {
        GetData : GetData
    }

})();


/**
 * Component 입/출력 구분
 * 
 */
export const COMP_VALUE_TYPE = {
    'CODE'  : 'CODE',
    'ID'    : 'ID',
    'NAME'  : 'NAME',
}


/**
 * Component 카테고리 (조회항목)
 * 
 */
export const COMP_CATEGORY = {
    'CAR'        : 'CAR',
    'CENTER'     : 'CENTER',
    'CLIENT'     : 'CLIENT',
    'CUST'       : 'CUST',
    'CUST_ZONE'  : 'CUST_ZONE',
    'STORE'      : 'STORE',
    'DEPT'       : 'DEPT',
    'DOCK'       : 'DOCK',
    'EMPLOY'     : 'EMPLOY',
    'ITEM'       : 'ITEM',
    'LOCATION'   : 'LOCATION',
    'UOM'        : 'UOM',
    'USER'       : 'USER',
    'USER2'      : 'USER2',
    'WH'         : 'WH',
    'ZONE'       : 'ZONE',
    'POOL'       : 'POOL',
    'ITEMGRP'    : 'ITEMGRP',
    'ADMIN'      : 'ADMIN',
    'DLVCUST'    : 'DLVCUST',
    'PARTNER'    : 'PARTNER',
    'TEL'        : 'TEL',
    'ITEMIF'     : 'ITEMIF',
    'DRIVER'     : 'DRIVER',
    'COUNTRY'    : 'COUNTRY'
}


/**
 * Component 맵핑정보 
 * Service 호출 시, MyBatis 매개변수명과의 맵핑
 * 
 * (CmBeanController.java 참조)
 * 
 */
export const COMP_PARAM_MAP = new Map([

    ['CUST'         , {'CODE' : 'vrSrchCustCd'  , 'NAME' : 'vrSrchCustNm'}]
    , ['STORE'      , {'CODE' : 'vrSrchCustCd'  , 'NAME' : 'vrSrchCustNm'}]
    , ['WH'         , {'CODE' : 'vrSrchWhCd'    , 'NAME' : 'vrSrchWhNm'}]
    , ['LOCATION'   , {'CODE' : 'vrSrchLocCd'   , 'NAME' : 'vrSrchLocCd'}]     // 로케이션 명칭 X
    , ['ITEM'       , {'CODE' : 'vrSrchItemCd'  , 'NAME' : 'vrSrchItemNm'}]
    , ['USER2'      , {'CODE' : 'vrSrchUserNo'  , 'NAME' : 'vrSrchUserNm'}]
    
]);


// export {Component, Utility, COMP_VALUE_TYPE, COMP_CATEGORY, COMP_PARAM_MAP}