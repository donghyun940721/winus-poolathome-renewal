//******************************
//
// 남동정산관리 파일 업로드 : fileControl.js
//
//******************************

// 이벤트 발생 로우 인덱스 찾기
function _getObj() {
   var obj = event.srcElement;
   while (obj.tagName != 'TD') obj = obj.parentElement
   return obj
}


//파일 찾기
function _fn_fileSearch(filePostion, pstrDate, pstrLC_ID, pstrCUST_ID, pstrFILE_NUM) {
		
	var idxTemp = _getObj().parentElement.rowIndex;
	
		filechecklist.jdfwForm.action="/JDFW5XUploader4";
		filechecklist.jdfwForm.formname.value    = "D_ATCH_FILE_NAME";
		filechecklist.jdfwForm.filePostion.value = filePostion;
		filechecklist.jdfwForm.yyyymm.value      = pstrDate;
		filechecklist.jdfwForm.lcid.value 		 = pstrLC_ID;
		filechecklist.jdfwForm.custid.value 	 = pstrCUST_ID;
		filechecklist.jdfwForm.filenum.value 	 = pstrFILE_NUM;
		filechecklist.jdfwForm.fileIdx.value 	 = idxTemp;
		filechecklist.jdfwForm.rowIdx.value 	 = idxTemp;
		filechecklist.jdfwForm.txtFile.click();
}

//파일 열기
function _fn_fileOpen() {
	var idxTemp = _getObj().parentElement.rowIndex;
	
	var temp_form1 = document.getElementsByName("D_ATCH_FILE_ROUTE");
	var temp_form2 = document.getElementsByName("D_ATCH_FILE_NAME");
	var tempP1 = temp_form1[idxTemp].value;
	var tempP2 = temp_form2[idxTemp].value;

	if(tempP1 == "") {
		alert(strings['file.error.nosuchfile']);
		return;
	} else {
		filechecklist.jdfwForm.action = "/jsp/filedownloadMt.jsp?file_path="+tempP1+"&file_name="+tempP2;
		filechecklist.jdfwForm.submit();
	}
}


//파일 삭제(기본)
function _fn_fileDelete(idxTemp) {	
	if(idxTemp == 0){
		var idxTemp = 0;
	}else{
		var idxTemp = _getObj().parentElement.rowIndex;	
	}
	
	if(idxTemp == 0){ // 수정
		var temp_form1 = document.getElementsByName("C_ORG_ATCH_FILE_ROUTE"); //기존
		var temp_form2 = document.getElementsByName("C_ORG_FILENAME");	//기존
	}else{ // 삭제
		var temp_form1 = document.getElementsByName("D_ATCH_FILE_ROUTE");
		var temp_form2 = document.getElementsByName("ORG_FILENAME");
	}
	
	var tempP1 = temp_form1[idxTemp].value;
	var tempP2 = temp_form2[idxTemp].value;

	filechecklist.jdfwForm.action = "/jsp/filedeleteMt.jsp?file_path="+tempP1 +"&file_name="+tempP2;
	filechecklist.jdfwForm.submit();
	
}

// 파일 전송 리턴값 셋팅
function _fn_fileprocess(result, realfilepath, filename, filesize, idxTemp) {
	var form1 = document.getElementsByName("D_ATCH_FILE_NAME");
	var form2 = document.getElementsByName("D_ATCH_FILE_ROUTE");
	var form3 = document.getElementsByName("FILE_SIZE");

	if(result == "0") {
		form1[idxTemp].value = filename;
		form2[idxTemp].value = realfilepath;
		form3[idxTemp].value = filesize;
		
		jQuery("#FILE_STATE").trigger('input');
	}
}

// 파일첨부폼 리셋
function _fn_resetform(parm1, parm2, comboStyle) {
	jQuery("#tb_fileup").html("");
	jQuery("#tb_filedel").html("");
	document.formDetl.FILE_DEL_CNT.value = "0";
	document.formDetl.FILE_CNT.value = "0";
	
	//ATCH_CODE_GRP 선택 없는 경우
	if(comboStyle == "1") {
		jQuery("#tb_fileup").html("<colgroup><col width='30%'/><col width='/'/></colgroup>");
		_fn_addfile("noData", "", "", parm1, parm2, atchCodeValNone, "", comboStyle);
	} 
	//ATCH_CODE_GRP 선택 있는 경우
	else {
		jQuery("#tb_fileup").html("<colgroup><col width='17%'/><col width='25%'/><col width='/'/></colgroup>");
		_fn_addfile("noData", "", "", "", "", "", "", comboStyle);
	}
}


function _fn_addfilelineready(idxTemp, parm) {
	
	var temp_form1 = document.getElementsByName("ORG_FILENAME");
	var temp_form2 = document.getElementsByName("D_ATCH_FILE_NAME");
	var temp_form3 = document.getElementsByName("FILE_STATE");
	
	temp_form1[idxTemp].value = parm;
	temp_form2[idxTemp].value = parm;
	temp_form3[idxTemp].value = 'I';
}

//파일 전송
function _fn_addfileline(idxTemp, parm) {
	// 이미 첨부된 파일이 있을때 기존 파일 삭제
	var temp_form1 = document.getElementsByName("D_ATCH_FILE_ROUTE");
	var temp_form2 = document.getElementsByName("D_ATCH_FILE_NAME");

	var tempP1 = temp_form1[idxTemp].value;
	var tempP2 = temp_form2[idxTemp].value;

	// 새로운 파일 첨부
	temp_form2[idxTemp].value = parm;
	
	var temp_form = document.getElementsByName("D_ATCH_FILE_NAME");
	var file_full_path = temp_form[idxTemp].value;
	
	// 파일명 길이, 파일 확장자 체크
	if(file_full_path != "") {	
		var file_lastname = file_full_path.substr(file_full_path.lastIndexOf('.') + 1);
		var namelen = (file_full_path.substr(file_full_path.lastIndexOf('\\') + 1)).byteLength();
	
		// 오라클 바이트 계산 (영문, 숫자, 한글 상관없이 2바이트로 계산 60바이트 max)
		if(namelen>30) {
			alert(strings['file.error.maxfilename']);
			return false;		
		}
		
		file_lastname = file_lastname.toUpperCase();
		
		if (file_lastname == "JPG" || file_lastname == "JPEG" || file_lastname == "PNG" || file_lastname == "GIF" 
			|| file_lastname == "HWP" || file_lastname == "PDF" || file_lastname == "XLS" || file_lastname == "XLSX"  || file_lastname == "TXT"
			|| file_lastname == "DOC" || file_lastname == "DOCX" || file_lastname == "PPT" || file_lastname == "PPTX" || file_lastname == "ZIP") { }
		else if (file_lastname == "") { }
		else {
			alert(strings['file.error.extension']);
			temp_form[idxTemp].value = "";
			
			var filetag = filechecklist.jdfwForm.txtFile;
			var parent_filetag = filetag.parentNode;
			var new_filetag = filetag.cloneNode(true);
			
			parent_filetag.removeChild(filetag);
			parent_filetag.appendChild(new_filetag);
			
			return false;
		}				
		
		filechecklist.jdfwForm.action = "/JDFW5XUploader4";		
		filechecklist.jdfwForm.submit();
	} 
}

