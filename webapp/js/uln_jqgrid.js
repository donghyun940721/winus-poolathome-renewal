function ULNSI_jQGrid(objectid) {

    var _this = this;
    var _inited = false;

    var grid = null;
    var gridId = null;

    var title = null;
    var height = null;
    var width = null;
    var option = null;
    var pager = null;
    var rowList = null;

    var addRowEventCallback = null;
    var dblClickRowEventCallback = null;
    var loadCompleteEventCallback = null;
    var selectRowEventCallback = null;
    var beforeSelectRowEventCallback = null;

    var loadErrorCallback2 = function(xhr, st, err){
		if(xhr.status == 404){
			location.href=('/index.html');
		}
	};

    var selectAllEventCallback = function(id, status, pkCol) {
        var grid = _this.grid;
        var ids = grid.jqGrid('getDataIDs');
        if (ids != '') {
        	
        	// 서브 콤보 박스가 있는지 확인 
        	// 주의 : 단 서브 콤보박스는 index에서 cb2로 지정해야된다.
            var colModelList = grid.jqGrid('getGridParam', 'colModel');
            var subCb = false;
            for(var i = 0 ; i < colModelList.size() ; i++){
            	if(colModelList[i].index == 'cb2'){
            		subCb = true;
            		break;
            	}
            }
        	
            for (var i = 0; i < ids.length; i++) {
                var idx = ids[i];
                if (status) {
                    grid.setCell(idx, 'CHK_BOX', "Yes"); //모든 셀 checked
                    grid.editRow(idx);

                    if ("" == cfn_trim( grid.getCell(idx, 'ST_GUBUN')) && "" == cfn_trim( grid.getCell(id, 'FLAG'))) {
                        _this.setStatus(idx, "INSERT");

                    } else if ("" != cfn_trim( grid.getCell(id, 'FLAG')) ) {
                        _this.setStatus(idx, "UPDATE");

                    }

                    // 서브 콤보박스 전체 선택 
                    if(subCb){
                    	grid.setCell(idx, 'cb2', '1');
    					grid.setCell(idx, 'CHK_BOX2', 'Yes');
                    }
                    
                } else {
                    grid.setCell(idx, 'CHK_BOX', 'No');
                    grid.restoreRow(idx);
                    _this.setStatus(idx, " ");

                    // 서브 콤보박스 전체 해제 
                    if(subCb){
                    	grid.setCell(idx, 'cb2', '0');
    					grid.setCell(idx, 'CHK_BOX2', 'No');
                    }
                }
            }
        }
    };

    var selectCellEventCallback = function(id, col) {
        // // console.log("[ selectCellEventCallback ] id :" + id + ", col :" + col);
        var grid = _this.grid;
        if(col == 1){
            if(grid.getCell(id, 'CHK_BOX') == "Yes"){
                jQuery('#jqg_' + _this.gridId + '_' + id).attr("checked",false);
                grid.setCell(id, 'CHK_BOX', "No");
                grid.restoreRow(id);
                _this.setStatus(id, " ");

            }else{
                jQuery('#jqg_' + _this.gridId + '_' + id).attr("checked",true);
                grid.setCell(id, 'CHK_BOX', "Yes");
                var rowdata = grid.getRowData(id);
                grid.editRow(id);
                if("" == cfn_trim(grid.getCell(id, 'ST_GUBUN')) && "" == cfn_trim(grid.getCell(id, 'FLAG'))){
                    _this.setStatus(id, "INSERT");

                }else if("" != cfn_trim(grid.getCell(id, 'FLAG'))){
                    _this.setStatus(id, "UPDATE");

                }
            }
        }else{
            if(grid.getCell(id, 'CHK_BOX') == "Yes"){
                grid.setCell(id, 'CHK_BOX', 'Yes');
                grid.setSelection(id, false);

            }else{
                grid.setCell(id, 'CHK_BOX', 'NO');
                grid.setSelection(id, true);

            }
        }
    };

    var afterSaveCellEventCallback = null;
    var afterEditCellEventCallback = null;

    var defaultHeight = 460;
    var defaultWidth = '100%';
    var defaultOption = {
        'title' : '',
        'height' : 140,
        'width' : '96%',
        'colName' : [],
        'colModel' : [],
        'pager' : jQuery("#pjmap1"),
        'autowidth': true,
        'multiselect': false,
        'viewrecords': true,
        'gridview': true,
        'sortable': true,
        'rowList': [100, 200, 300],
        'rownumbers': true,
        'shrinkToFit': true,
        'rownumWidth': 40,
        'scroll' : false,
		'footerrow' 	: false,
		'userDataOnFooter' : false,
		'sortname' : '',
		'sortorder': 'asc'
		, 'hidegrid' : false
    };

    this.init = function(objectid, option, url, loadCompleteEventCallback, loadErrorCallback) {

        if (_inited)
            return;

        _this.grid = typeof objectid === 'string' ? $('#' + objectid) : objectid;
        _this.gridId = typeof objectid === 'string' ? objectid : $(objectid).attr('id');

        // // console.log('grid : ' + grid + ' , gridId :'+gridId);

        _this.option = option != null ? option : defaultOption;
        // _this.pager = typeof pager === 'string' ? document.getElementById(pager) : pager;
        // _this.rowList = rowArray instanceof Array ? rowArray : defaultRowArray;

        if (url == null && loadErrorCallback == null) {
            _this.grid.jqGrid({
                ajaxGridOptions: {
                    type: "POST"
                },
                datatype: ('datatype' in _this.option) ? _this.option.datatype : "json",
                datastr	: ('datastr' in _this.option) ? _this.option.datastr : false,
                height: ('height' in _this.option) ? _this.option.height : defaultOption.height,
                width: ('width' in _this.option) ? _this.option.width : defaultOption.width,
                colNames: ('colName' in _this.option) ? _this.option.colName : defaultOption.colName,
                colModel: ('colModel' in _this.option) ? _this.option.colModel : defaultOption.colModel,
                mtype: "POST",
                pager: ('pager' in _this.option) ? _this.option.pager : defaultOption.pager,
                jsonReader: {
                    repeatitems: false
                }, // json 리더
                autowidth: ('autowidth' in _this.option) ? _this.option.autowidth : defaultOption.autowidth, // 자동 넓이
                multiselect: ('multiselect' in _this.option) ? _this.option.multiselect : defaultOption.multiselect, // 다중 선택 여부
                viewrecords: ('viewrecords' in _this.option) ? _this.option.viewrecords : defaultOption.viewrecords, // 페이저 우측의 레코드 갯수
                gridview: ('gridview' in _this.option) ? _this.option.gridview : defaultOption.gridview, // 그리드 뷰모드
                sortable: ('sortable' in _this.option) ? _this.option.sortable : defaultOption.sortable, // 컬럼 재정렬(이동)
                rownumbers: ('rownumbers' in _this.option) ? _this.option.rownumbers : defaultOption.rownumbers, // 줄번호
                rownumWidth: ('rownumWidth' in _this.option) ? _this.option.rownumWidth : defaultOption.rownumWidth, // 줄번호 넓이
                rowNum: ('rowList' in _this.option) ? _this.option.rowList[0] : defaultOption.rowList[0], // 한 페이지에 보여줄 데이터
                rowList: ('rowList' in _this.option) ? _this.option.rowList : defaultOption.rowList, // 한 페이지에 보여줄 데이터
                shrinkToFit: ('shrinkToFit' in _this.option) ? _this.option.shrinkToFit : defaultOption.shrinkToFit,
                caption: ('title' in _this.option) ? _this.option.title : defaultOption.title,
               	scroll: ('scroll' in _this.option) ? _this.option.scroll : defaultOption.scroll,
       			footerrow: ('footerrow' in _this.option) ? _this.option.footerrow : defaultOption.footerrow,
				userDataOnFooter: ('userDataOnFooter' in _this.option) ? _this.option.userDataOnFooter : defaultOption.userDataOnFooter,
				sortname: ('sortname' in _this.option) ? _this.option.sortname : defaultOption.sortname,
				sortorder: ('sortorder' in _this.option) ? _this.option.sortorder : defaultOption.sortorder
				, hidegrid : false

				, loadComplete: loadCompleteEventCallback
				, loadError : loadErrorCallback2
                /* 공통 전체선택 이벤트 시작 */
                , onSelectAll: selectAllEventCallback
                , onCellSelect: selectCellEventCallback
                /*
                , onRightClickRow: function (rowid, iRow, iCol, e) {
                    _this.grid.contextMenu('rightClickMenu', {
                        bindings: {
                            'DisplayiRow': function(t) {
                                alert('DisplayiRow: ' + iRow);
                            },
                            'DisplayiCol': function(t) {
                                alert('DisplayiCol: ' + iRow);
                            }
                        }
                    })
                }
*/
                /* 공통 전체선택 이벤트 끝 */
            });
        } else if(loadErrorCallback == null){
        	_this.grid.jqGrid({
                url: url,
                datatype: "json",
                height: ('height' in _this.option) ? _this.option.height : defaultOption.height,
                width: ('width' in _this.option) ? _this.option.width : defaultOption.width,
                colNames: ('colName' in _this.option) ? _this.option.colName : defaultOption.colName,
                colModel: ('colModel' in _this.option) ? _this.option.colModel : defaultOption.colModel,
                mtype: "POST",
                pager: ('pager' in _this.option) ? _this.option.pager : defaultOption.pager,
                jsonReader: {
                    repeatitems: false
                }, // json 리더
                autowidth: ('autowidth' in _this.option) ? _this.option.autowidth : defaultOption.autowidth, // 자동 넓이
                multiselect: ('multiselect' in _this.option) ? _this.option.multiselect : defaultOption.multiselect, // 다중 선택 여부
                viewrecords: ('viewrecords' in _this.option) ? _this.option.viewrecords : defaultOption.viewrecords, // 페이저 우측의 레코드 갯수
                gridview: ('gridview' in _this.option) ? _this.option.gridview : defaultOption.gridview, // 그리드 뷰모드
                sortable: ('sortable' in _this.option) ? _this.option.sortable : defaultOption.sortable, // 컬럼 재정렬(이동)
                rownumbers: ('rownumbers' in _this.option) ? _this.option.rownumbers : defaultOption.rownumbers, // 줄번호
                rownumWidth: ('rownumWidth' in _this.option) ? _this.option.rownumWidth : defaultOption.rownumWidth, // 줄번호 넓이
                rowNum: ('rowList' in _this.option) ? _this.option.rowList[0] : defaultOption.rowList[0], // 한 페이지에 보여줄 데이터
                rowList: ('rowList' in _this.option) ? _this.option.rowList : defaultOption.rowList, // 한 페이지에 보여줄 데이터
                shrinkToFit: ('shrinkToFit' in _this.option) ? _this.option.shrinkToFit : defaultOption.shrinkToFit,
                caption: ('title' in _this.option) ? _this.option.title : defaultOption.title,
                scroll: ('scroll' in _this.option) ? _this.option.scroll : defaultOption.scroll,
        		footerrow: ('footerrow' in _this.option) ? _this.option.footerrow : defaultOption.footerrow,
				userDataOnFooter: ('userDataOnFooter' in _this.option) ? _this.option.userDataOnFooter : defaultOption.userDataOnFooter,
				sortname: ('sortname' in _this.option) ? _this.option.sortname : defaultOption.sortname,
				sortorder: ('sortorder' in _this.option) ? _this.option.sortorder : defaultOption.sortorder
						
				, hidegrid : false
        		, loadComplete: loadCompleteEventCallback
				, loadError : loadErrorCallback2
                /* 공통 전체선택 이벤트 시작 */
                , onSelectAll: selectAllEventCallback
                , onCellSelect: selectCellEventCallback
        	});
        } else {
            _this.grid.jqGrid({
                url: url,
                datatype: "json",
                height: ('height' in _this.option) ? _this.option.height : defaultOption.height,
                width: ('width' in _this.option) ? _this.option.width : defaultOption.width,
                colNames: ('colName' in _this.option) ? _this.option.colName : defaultOption.colName,
                colModel: ('colModel' in _this.option) ? _this.option.colModel : defaultOption.colModel,
                mtype: "POST",
                pager: ('pager' in _this.option) ? _this.option.pager : defaultOption.pager,
                jsonReader: {
                    repeatitems: false
                }, // json 리더
                autowidth: ('autowidth' in _this.option) ? _this.option.autowidth : defaultOption.autowidth, // 자동 넓이
                multiselect: ('multiselect' in _this.option) ? _this.option.multiselect : defaultOption.multiselect, // 다중 선택 여부
                viewrecords: ('viewrecords' in _this.option) ? _this.option.viewrecords : defaultOption.viewrecords, // 페이저 우측의 레코드 갯수
                gridview: ('gridview' in _this.option) ? _this.option.gridview : defaultOption.gridview, // 그리드 뷰모드
                sortable: ('sortable' in _this.option) ? _this.option.sortable : defaultOption.sortable, // 컬럼 재정렬(이동)
                rownumbers: ('rownumbers' in _this.option) ? _this.option.rownumbers : defaultOption.rownumbers, // 줄번호
                rownumWidth: ('rownumWidth' in _this.option) ? _this.option.rownumWidth : defaultOption.rownumWidth, // 줄번호 넓이
                rowNum: ('rowList' in _this.option) ? _this.option.rowList[0] : defaultOption.rowList[0], // 한 페이지에 보여줄 데이터
                rowList: ('rowList' in _this.option) ? _this.option.rowList : defaultOption.rowList, // 한 페이지에 보여줄 데이터
                shrinkToFit: ('shrinkToFit' in _this.option) ? _this.option.shrinkToFit : defaultOption.shrinkToFit,
                caption: ('title' in _this.option) ? _this.option.title : defaultOption.title,
                scroll: ('scroll' in _this.option) ? _this.option.scroll : defaultOption.scroll,
        		footerrow: ('footerrow' in _this.option) ? _this.option.footerrow : defaultOption.footerrow,
				userDataOnFooter: ('userDataOnFooter' in _this.option) ? _this.option.userDataOnFooter : defaultOption.userDataOnFooter,
				sortname: ('sortname' in _this.option) ? _this.option.sortname : defaultOption.sortname,
				sortorder: ('sortorder' in _this.option) ? _this.option.sortorder : defaultOption.sortorder,
				hidegrid : false,
        		loadComplete: loadCompleteEventCallback,
                loadError: loadErrorCallback,
                /* 공통 전체선택 이벤트 시작 */
                onSelectAll: selectAllEventCallback,
                onCellSelect: selectCellEventCallback
                /* 공통 전체선택 이벤트 끝 */
            });
        }

        // // console.log(' messageList  :' + messageList);
        // if (messageList) {
        //     _this.setMessageList(messageList);
        // }

        // 전체선택 히든처리하던거 주석처리
//        jQuery('#cb_' + _this.gridId ).remove();
        _inited = true;
    }

    this.setAddRowEventCallback = function(callbackFunc) {
        this.addRowEventCallback = callbackFunc;
    };

    this.setDblClickRowEventCallback = function(callbackFunc) {
        this.dblClickRowEventCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
            ondblClickRow: this.dblClickRowEventCallback
        });
    };

    this.setBeforeSelectRowEventCallback = function(callbackFunc) {
        this.beforeSelectRowEventCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
        	beforeSelectRow: this.beforeSelectRowEventCallback
        });
    };

    this.setLoadCompleteEventCallback = function(callbackFunc) {
        this.loadCompleteEventCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
            loadComplete: this.loadCompleteEventCallback
        });
    };

    this.setLoadErrorCallback = function(callbackFunc) {
    	// console.log("에러테스트");
    	// console.log(callbackFunc);
        this.loadErrorCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
        	loadError: this.loadErrorCallback
        });
    };


    this.setSelectRowEventCallback = function(callbackFunc) {
        this.selectRowEventCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
            onSelectRow: this.selectRowEventCallback
        });
    };

    this.setSelectCellEventCallback = function(callbackFunc) {
        this.selectCellEventCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
            onCellSelect: this.selectCellEventCallback
        });
    };

    this.setAfterSaveCellEventCallback = function(callbackFunc) {
        this.afterSaveCellEventCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
            afterSaveCell: this.afterSaveCellEventCallback
        });
    };

    this.setAfterEditCellEventCallback = function(callbackFunc) {
        this.afterEditCellEventCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
            afterEditCell: this.afterEditCellEventCallback
        });
    };

    /* 전체선택 이벤트 */
    this.setSelectAllEventCallback = function(callbackFunc) {
        this.selectAllEventCallback = callbackFunc;
        _this.grid.jqGrid('setGridParam', {
            onSelectAll: this.selectAllEventCallback
        });
    }

    this.setColProp = function(col, value) {
        _this.grid.jqGrid('setColProp', col, value);
    }

    this.search = function(param, url, op, op1) {
        var page = 0; // 변수 선언
        if (op1 == 1) { // 저장 및 삭제 후 페이징 된 상태에서 재조회를 위해서
            page = op == 0 ? 1 : _this.grid.jqGrid().getGridParam("page");
        } else { // 검색 시 페이징 초기화
            page = 1;
        }
        var opts = {
            url: url,
            datatype: "json",
            page: page
        };
        this.reload(opts);
    }

    this.clear = function () {
        _this.grid.clearGridData();
    }

    this.reload = function(opts) {
        _this.grid.clearGridData();
        if (opts != null) {
            _this.grid.setGridParam(opts);
        }
        _this.grid.trigger("reloadGrid");
    }

    this.addRow = function(datarow) {
        var _grid = _this.grid;
        var ids = _grid.jqGrid("getDataIDs");
        var id = null;
        var selectIds = _grid.getGridParam('selarrrow').toString().split(',');
        if (ids == null || ids == "") {
            id = "1";
        } else {
            id = Number(ids[ids.length - 1]) + 1;
        }

        if(datarow ==null){
	        datarow = {
	            ST_GUBUN: "INSERT",
	            ST_GUBUN_IMG: "<img src='/img/exam/btn/add.gif'>",
	            CHK_BOX: "Yes"
	        };
        }
        var su = _grid.jqGrid("addRowData", id, datarow);
        _grid.setSelection(id, true);

        jQuery('#jqg_' + _this.gridId + '_' + id).attr("checked", true);
        if (this.addRowEventCallback != null)
            this.addRowEventCallback();

        _grid.editRow(id);

        var gridScroll = jQuery('div.ui-jqgrid-bdiv', jQuery('#gview_' + _this.gridId) );
        gridScroll.scrollTop(gridScroll[0].scrollHeight - gridScroll.height());
    }

    this.deleteRow = function() {
        var _grid = _this.grid;
        var ids = _grid.getDataIDs();
        var selectIds = _grid.getGridParam('selarrrow').toString().split(',');
        var plntCnt = 0;
        if (selectIds != "") {
            for (var i = 0; i < selectIds.length; i++) {
                if ("" == cfn_trim(_grid.getCell(selectIds[i], "FLAG"))) {
                    _grid.delRowData(selectIds[i]);
                } else {
                    this.setStatus(selectIds[i], "DELETE");
                }
            }
        } else {
            alert(strings['grid.no_selected_data_for_delete']);
            return false;
        }
    }
    
    this.deleteGrid = function(url, targetForm, formDataDiv, deleteProcessFunc, saveProcessCallbackFunc, saveParam) {
        var _grid = _this.grid;
        var ids = _grid.getDataIDs();
        var selectIds = _grid.getGridParam('selarrrow').toString().split(',');
        for (var i = 0; i < selectIds.length; i++) {
            if ("" == cfn_trim(_grid.getCell(selectIds[i], "FLAG"))) {
                _grid.delRowData(selectIds[i]);
            } else {
                this.setStatus(selectIds[i], "DELETE");
            }
            
            
        }
        
        var $form = jQuery("#" + targetForm);
        var $formDataDiv = jQuery("#" + formDataDiv);
        var tmpCnt = 0 ;
        
        if(selectIds != ""){
            $formDataDiv.html("");
            if( jQuery("input[name='selectIds']", $form).val() == undefined){
                jQuery($formDataDiv).append("<input type='text' name='selectIds' value='0'/><br>");
                
                if( jQuery("input[name='I_selectIds']", $form).val() == undefined){
                	jQuery($formDataDiv).append("<input type='text' name='I_selectIds' value='0'/><br>");
                }
                if( jQuery("input[name='D_selectIds']", $form).val() == undefined){
                	jQuery($formDataDiv).append("<input type='text' name='D_selectIds' value='0'/><br>");
                }
                
            }else{
                tmpCnt = parseInt(jQuery("input[name='selectIds']", $form).val());
            }

            for(var i = 0 ; i < selectIds.length ; i++){
                var st_status = _grid.getCell(selectIds[i], "ST_GUBUN");
                var st_flag =  _grid.getCell(selectIds[i], "FLAG");
                if( "DELETE" == cfn_trim(st_status) && "" != cfn_trim(st_flag) ) {
                    
                    if ( deleteProcessFunc ) {
                    	deleteProcessFunc($formDataDiv, _grid, tmpCnt, selectIds[i]);
                    }
                    jQuery($formDataDiv).append("<input type='text' name='ST_GUBUN"+tmpCnt+"' value='"+ st_status +"'/>");
                    tmpCnt ++;
                }
            }
            jQuery("input[name='selectIds']", $form).val( tmpCnt );
            jQuery("input[name='D_selectIds']", $form).val( tmpCnt );
        }else{
            if(tmpCnt == 0){
                alert(strings['grid.no_selected_data_for_save']);
                return false;
            }
        }
        // // console.log("[ 222 $formDataDiv.html() ] :" + $formDataDiv.html());
        var param = (saveParam) ? jQuery($form).serialize() + saveParam : jQuery($form).serialize();
        if ( saveProcessCallbackFunc ) {
            jQuery.post(url, param, saveProcessCallbackFunc).fail(function(xhr, textStatus){
        		if(xhr.status == 404){
        			location.href=('/index.html');
        		}
            });
        }        
        
    } 
    
    this.saveGrid = function(url, targetForm, formDataDiv, saveProcessFunc, saveProcessCallbackFunc, saveParam) {
        var _grid = _this.grid;
        var ids = _grid.jqGrid("getDataIDs");
        var id = null;
        var isProcess = false;
        var tmpCnt = 0 ;

        var selectIds = _grid.getGridParam('selarrrow').toString().split(',').sort(function(a, b){return a-b});
        if( selectIds == "" ){
            alert(strings['grid.no_check_data_for_save']);
            return false;
        }

        //저장시작
        if(confirm(strings['grid.confirm_save'])){
            var $form = jQuery("#" + targetForm);
            var $formDataDiv = jQuery("#" + formDataDiv);
            if(selectIds != ""){
                $formDataDiv.html("");
                //input의 selectIds에는 선택된 데이터중 저장처리될 데이터들의 갯수를 넣는다.
                if( jQuery("input[name='selectIds']", $form).val() == undefined){
                    jQuery($formDataDiv).append("<input type='text' name='selectIds' value='0'/><br>");
                }else{
                    tmpCnt = parseInt(jQuery("input[name='selectIds']", $form).val());
                }

                for(var i = 0 ; i < selectIds.length ; i++){
                    var st_status = _grid.getCell(selectIds[i], "ST_GUBUN");
                    var st_flag =  _grid.getCell(selectIds[i], "FLAG");
                    if( "DELETE" == cfn_trim(st_status) && "" != cfn_trim(st_flag) ) {
                        //st_gubun이 삭제이고, flag가 빈값이아닌경우
                        jQuery($formDataDiv).append("<input type='text' name='ST_GUBUN"+tmpCnt+"' value='"+ st_status +"'/>");
                        isProcess = true;
                    }else if( "UPDATE" == cfn_trim(st_status) && "" != cfn_trim(st_flag) ) {
                        //st_gubun이 수정이고, flag가 빈값이 아닌경우
                        jQuery($formDataDiv).append("<input type='text' name='ST_GUBUN"+tmpCnt+"' value='"+ st_status +"'/>");
                        isProcess = true;
                    }else if( "INSERT" == cfn_trim(st_status) && "" == cfn_trim(st_flag) ) {
                        //st_gubun이 등록이고, flag가 빈값이 아닌경우 (즉, db에서 정보를 읽어온것이 아닌, 신규눌러서 row가 추가된경우 )
                        jQuery($formDataDiv).append("<input type='text' name='ST_GUBUN"+tmpCnt+"' value='"+ st_status +"'/>");
                        isProcess = true;
                    }
                    if(isProcess){
                        //저장을 수행함수를 수행
                        if ( saveProcessFunc ) {
                            // // console.log('[ saveGrid ] selectIds[i] :' + selectIds[i] + ', CODE_TYPE_NM :'+ _grid.getCell(selectIds[i], "CODE_TYPE_NM"));
                            saveProcessFunc($formDataDiv, _grid, tmpCnt, selectIds[i]);
                        }
                        tmpCnt ++; //db작업할 갯수 카운트
                    }
                }
                // // console.log("[ $formDataDiv.html() ] :" + $formDataDiv.html());
                //db작업할 갯수 카운트 한것을 파라미터로 넘긴다. (for문 사용시 필요한 값)
                jQuery("input[name='selectIds']", $form).val( tmpCnt );
            }else{
                if(tmpCnt == 0){
                    alert(strings['grid.no_selected_data_for_save']);
                    return false;
                }
            }
            // // console.log("[ 222 $formDataDiv.html() ] :" + $formDataDiv.html());
            var param = (saveParam) ? jQuery($form).serialize() + saveParam : jQuery($form).serialize();
            if ( saveProcessCallbackFunc ) {
                jQuery.post(url, param, saveProcessCallbackFunc).fail(function(xhr, textStatus){
            		if(xhr.status == 404){
            			location.href=('/index.html');
            		}
                });
            }
        }
    }

    this.setStatus = function(idx, st) {
        var _grid = _this.grid;
        var st_img = " ";
        if (st == "DELETE") {
            st_img = "<img src='/img/exam/btn/del.gif'>";
        } else if (st == "UPDATE") {
            st_img = "<img src='/img/exam/btn/modify.gif'>";
        } else if (st == "INSERT") {
            st_img = "<img src='/img/exam/btn/add.gif'>";
        }
        _grid.setCell(idx, 'ST_GUBUN', st);
        _grid.setCell(idx, 'ST_GUBUN_IMG', st_img);
    }

    this.getGrid = function() {
        return _this.grid;
    }

    this.getCell = function(rowid, colName) {
        return _this.grid.getCell(rowid, colName);
    }

    this.setCell = function(rowid, colName, colValue) {
        _this.grid.setCell(rowid, colName, colValue);
    }

    this.restoreRow = function(rowid) {
        _this.grid.restoreRow(rowid);
    }

    this.editRow = function(rowid) {
        _this.grid.editRow(rowid);
    }

    this.getRowData = function() {
        return _this.grid.getRowData();
    }
    
    this.resetSelection = function(id) {
        var _grid = _this.grid;		    
    	var selectIds = _grid.getGridParam('selarrrow').toString().split(',');			
        if (selectIds != "") {
            for (var i = 0; i < selectIds.length; i++) {	                
                jQuery('#jqg_' + _this.gridId + '_' + id).attr("checked",false);
                _grid.setCell(selectIds[i], 'CHK_BOX', "No");
                _grid.restoreRow(selectIds[i]);
                _this.setStatus(selectIds[i], " ");
            }
        } 
        _grid.jqGrid('resetSelection');
    }

}