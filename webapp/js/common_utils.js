/*******************************************************************************
 * 일반적인 자바 스크립트의 기초가 되는 스크립트 모음
 *******************************************************************************
 * 함수 목록 :
 * cfn_isObject(object) - 객체 여부를 반환
 * cfn_isArray(array) - 배열 여부를 반환
 * cfn_isBoolean(boolean) - 부울대수 여부를 반환
 * cfn_isEmpty(object) - 빈 값 여부를 반환
 * cfn_isFunction(function) - 함수 여부를 반환
 * cfn_isNull(object) - Null 여부를 반환
 * cfn_isNumber(number) - 수 여부를 반환
 * cfn_isString(string) - 문자열 여부를 반환
 * cfn_isUndefined(object) - Undefined여부를 반환
 * cfn_isNumberString(string) - 수로 변경 가능한 문자여부를 반환
 * cfn_isDate(object) - Date 객체 여부를 바환
 * cfn_trim(string) - 좌우 공백을 제거
 *
 ******************************************************************************
 * 문자열을 처리하는 스크립트 모음(String 객체의 프로퍼티로 실행)
 *******************************************************************************
 * 함수 목록 :
 *  trim                좌/우 공백 제거
 *  ltrim               좌측 공백 제거
 *  rtrim               우측 공백 제거
 *  isEmpty             빈값 여부
 *  parseInt            integer로 벼환
 *  toDate              Date 형으로 변환
 *  byteLength          byte length 반환
 *  getNumber           숫만 반환
 *  getFigure           숫자만 반환
 *  meta                정규식 특수문자 escape
 *  removeRegExpChar    인수로 입력받은 문자를 제거하고 반환
 *  isFigure            숫자로인지 여부
 *  isAlphabet          알파벳인지 여부
 *  isAlphaNum          알파벳, 숫자로만 구성 여부
 *  isNumber            수인지 여부
 *  isUserId            알파벳으로 시작하고 알파벳, 숫자 여부
 *  isDate              날자로 변환 가능한지 체크
 *  lpad                좌측 padding
 *  rpad                우측 padding
 *  removeComma         , 콤마 제거
 *  replaceAll          문자열 교환
 *
 *******************************************************************************
 * 날짜를 입력받아 처리하는 스크립트 모음(Date 객체의 프로퍼티로 실행)
 *******************************************************************************
 * 함수 목록 :
 * getFormattedString(string) - 지정된 포멧으로 날짜를 반환한다.
 * addDate(number[, number[, number[, number[, number[, number[, number[, number]]]]]]])
 *      - 현재 날자에서 입력받은 시간을 더한 날자를 반환
 * addDay(number) - 현재 날자에서 입력받은 일을 더한 날자를 반환
 * addMonth(number) - 현재 날자에서 입력받은 월을 더한 날자를 반환
 * addYear(number) - 현재 날자에서 입력받은 년을 더한 날자를 반환
 * compareTo(date[, string]) - 날자를 비교할 날자와 비교한다.
 * getDateGap(date) - 대상 날자와의 차이 일수를 계산한다.
 *
 *******************************************************************************
 * 숫자를 입력받아 처리하는 스크립트 모음
 *******************************************************************************
 * 함수 목록 :
 * toCurrency(obj) - 입력란의 값의 3자리 마다 콤마를 찍는다.
 *
 * *****************************************************************************
 * cfn_setFldToNumberFld(strId) - 입력란을 숫자 필드로 세팅. 키 입력이벤트를 받아서
 *      3자리마다 자동으로 콤마를 추가한다.
 * cfn_setTelFld(strId) -전화번호 포메팅, 해당 입력란에 키 입력이 있을 때마다 전화
 *      번호 포메팅
 * cfn_setAllCheckbox(strId, strName) - 전체 체크 체크박스를 클릭시 모든 대상
 *      체크박스의 값을 변경한다.
 * cfn_isOnlyOneChecked(str) - 대상 체크박스 중 하나의 값만 체크 되었는지 체크한다.
 * cfn_clearForm(str) - 폼 내의 입력값들을 초기화한다.
 * cfn_openPop(url, target, width, height) - 팝업 창을 연다. url만 입력해도 됨.
 * cfn_markRow(object) - 테이블에선 선택한 row의 스타일을 변경한다.
 * cfn_fckModeSwitch(name, key) - FCK 에디터의 편집 모드를 변경한다. 소스 <->WYSWYG
 * cfn_startLoading() - 화면에 진행중임을 나타내는 레이어를 띄운다.
 * cfn_endLoading() - 화면에서 진행중임을 나타내는 레이어를 지운다.
 * cfn_popReturn(contGubun, passiveId, keyNm1, keyVal1) - 신고하기 팝업을 연다.(글로벌물류)
 * cfn_avoidBackspaceInReadonly(evt) - 읽기전용인 입력란에서 백스페이스를 눌렀을때 히스토리백 방지.
 * cfn_checkString(value)-특수문자입력체크)
 * cfn_clearTagString(value)-특수문자포함시 특수문자만제거한 나머지만리턴)
 * cfn_isValidDate(Object) - 입력한 날짜가 형식에 맞는지 체크한다.
 * cfn_maxLen(object,maxValue) - onKey이벤트시 체크    = 오브젝트와 제한길이(byte값)을 넣어준다.
 ******************************************************************************/

/**
 * 1. 이    름 : cfn_isObject
 * 2. 설    명 : 객체일 경우 true값을 반환한다. 문자열이나, 숫자, 배열, 함수,
                 Boolean, null 이나 undefined인 경우 false를 반환한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - 객체일 경우
 *               Boolean false  - 문자열, 숫자, 함수, Boolean, null,
                                  undefined일 경우
 * 5. 사 용 예 : alert("배열은 객체가 아니다. : " + cfn_isObject(new Array()));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isObject(a) {
    return (typeof a == 'object' && !!a && !cfn_isFunction(a));
}


/**
 * 1. 이    름 : cfn_isArray
 * 2. 설    명 : Array 생성자나 [] 배열 문장으로로 만든 배열인지 여부를 체크하고
 *               배열일 경우 true값을 반환한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean True  - 배열이면
 *               Boolean False  - 배열이 아니면
 * 5. 사 용 예 : var arr = new Array();
 *               alert("arr은 배열이다 : " + cfn_isArray(arr));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isArray(a) {
    return Object.prototype.toString.call(a) == '[object Array]';
}


/**
 * 1. 이    름 : cfn_isBoolean
 * 2. 설    명 : Boolean 값 중 true 나 false 인지 체크한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - false 나 true 중 하나인 경우
 *               Boolean false  - false 나 true 가 아닌 값인 경우
 * 5. 사 용 예 : alert("false는 Boolean값이다 : "+cfn_isBoolean(false));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isBoolean(a) {
      return typeof a == 'boolean';
}


/**
 * 1. 이    름 : cfn_isEmpty
 * 2. 설    명 : 셀만한 것이 없는 object 이거나 array 이거나 함수일 경우
                 true값을 반환한다. 선언이 되지 않았거나 생성 되지 않았으면 true를 반환
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - 비었다.
 *               Boolean false  - 안 비었다.
 * 5. 사 용 예 : alert("배열을 Array 생성자를 사용해 새로 생성하면 비었다 : " +cfn_isEmpty(new Array()));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isEmpty(o) {
    if(cfn_isObject(o)) {
        for (var i in o) {
             return false;
        }
    } else {
        if(cfn_isUndefined(o)) {
            return true;
        } else if(cfn_isNull(o)) {
            return true;
        } else if(cfn_trim(o) != "") {
            return false;
        }
    }
    return true;
}


/**
 * 1. 이    름 : cfn_isFunction
 * 2. 설    명 : 함수인지 확인하고 함수이면 true값을 반환한다.
 *               IE 7, firefox 3.1, chrome 동작 확인
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - 함수이면
 *               Boolean false  - 함수가 아니면
 * 5. 사 용 예 : alert("cfn_isEmpty()는 함수이다 " + cfn_isFunction(cfn_isEmpty));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isFunction(a) {
      return typeof a == 'function';
}


/**
 * 1. 이    름 : cfn_isNull
 * 2. 설    명 : null 값인 경우 true값을 반환한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - null값이면
 *               Boolean false  - null값이 아니면
 * 5. 사 용 예 : alert("널 값이다 : " + cfn_isNull(null));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isNull(a) {
    return typeof a == 'object' && !a;
}


/**
 * 1. 이    름 : cfn_isNumber
 * 2. 설    명 : 유한수일 경우 true를 반환한다. NaN이나 무한수일 경우는 false를
                 반환한다. 또한, 숫자로 변환 가능한 문자열이라 할지라도 false값을
                 반환한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - 숫자이면
 *               Boolean false  - 숫자가 아니면
 * 5. 사 용 예 : alert("숫자이다 : " + cfn_isNumber(0123));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isNumber(a) {
    return typeof a == 'number' && isFinite(a);
}


/**
 * 1. 이    름 : cfn_isString
 * 2. 설    명 : 문자열일 경우 true를 반환한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - 문자열이면
 *               Boolean false  - 문자열이이 아니면
 * 5. 사 용 예 : alert("\"1234\"는 문자열이다 :" + cfn_isString("1234"));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isString(a) {
    return typeof a == 'string';
}


/**
 * 1. 이    름 : cfn_isUndefined
 * 2. 설    명 : undefined 인 경우 true를 반환한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - undefined이면
 *               Boolean false  - undefined가 아니면
 * 5. 사 용 예 : var a;
 *               alert("a는 undefined이다 : " + cfn_isUndefined(a));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isUndefined(a) {
    return typeof a == 'undefined';
}


/**
 * 1. 이    름 : cfn_isNumberString
 * 2. 설    명 : 숫자이거나 숫자로 변환 가능한 문자열인 경우 true를 반환한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - 숫자이거나 숫자로 변환 가능한 문자열이면
 *               Boolean false  - 숫자이거나 숫자로 변환 가능한 문자열이 아니면
 * 5. 사 용 예 : var a = "1234";
 *               alert("a는 숫자로 변환가능한 문자열이다 : " + cfn_isNumberString(a));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isNumberString(a) {
    if(cfn_isNumber(a)) {
        return true;
    } else {
        return !isNaN(a);
    }
}


/**
 * 1. 이    름 : cfn_isDate
 * 2. 설    명 : Date 객체인 경우 true를 반환한다.
 * 3. 인    자 : Object
 * 4. 반 환 값 : Boolean true  - Date 객체이면
 *               Boolean false  - Date 객체가 아니면
 * 5. 사 용 예 : alert(cfn_isDate(new Date()));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_isDate(a) {

    if(cfn_isObject(a)) {
        if(a.getDate() > -1) {
            return true;
        }
    } else if(a.getFigure().toDate() > -1) {
        return true;
    }

    return false;
}


/**
 * 1. 이    름 : cfn_trim
 * 2. 설    명 : 값의 앞뒤 공백을 제거하고 반환한다.
 * 3. 인    자 : String
 * 4. 반 환 값 : String - 좌/우측 공백이 제거된 문자열
 * 5. 사 용 예 : alert("["+cfn_trim("    좌/우 공백 모두 제거됨     ")+"]");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_trim(a) {
    if(cfn_isObject(a) && !cfn_isArray(a) && !cfn_isFunction(a)) {
        return a.value.replace(/(^\s*)|(\s*$)/g, "");
    } else if(cfn_isString(a) || cfn_isNumber(a)) {
        return a.replace(/(^\s*)|(\s*$)/g, "");
    } else {
        return "";
    }
}


function cfn_onlyEditKey() {
    if(event.keyCode == 8 || event.keyCode == 46 || (event.keyCode >= 37 && event.keycode <= 40)) {
        return true;
    }

    return false;
}
/**
 * 1. 이    름 : cfn_onlyNumber
 * 2. 설    명 : 숫자만 입력 받는다.
 * 3. 인    자 :
 * 4. 반 환 값 :
 * 5. 사 용 예 : ... onkeypress="cfn_onlyNumber()" ...
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 02. 26    이민재     최초 등록
 */
function cfn_onlyNumber()
{
  if((event.keyCode < 48)||(event.keyCode > 57)) {
     event.returnValue=false;
  }
}

/*******************************************************************************
 * 문자열을 처리하는 스크립트 모음
 *******************************************************************************
 *
 * 함수 목록 :
 *  trim                좌/우 공백 제거
 *  ltrim               좌측 공백 제거
 *  rtrim               우측 공백 제거
 *  isEmpty             빈값 여부
 *  parseInt            integer로 벼환
 *  toDate              Date 형으로 변환
 *  byteLength          byte length 반환
 *  getNumber           숫만 반환
 *  getFigure           숫자만 반환
 *  meta                정규식 특수문자 escape
 *  removeRegExpChar    인수로 입력받은 문자를 제거하고 반환
 *  isFigure            숫자로인지 여부
 *  isAlphabet          알파벳인지 여부
 *  isAlphaNum          알파벳, 숫자로만 구성 여부
 *  isNumber            수인지 여부
 *  isUserId            알파벳으로 시작하고 알파벳, 숫자 여부
 *  isDate              날자로 변환 가능한지 체크
 *  lpad                좌측 padding
 *  rpad                우측 padding
 *  removeComma         , 콤마 제거
 *  replaceAll          문자열 교환
 *
 ******************************************************************************/



/**
 * 1. 이    름 : trim
 * 2. 설    명 : 문자의 좌/우측 공백을 제거한다.
 * 3. 인    자 :
 * 4. 반 환 값 : String - 좌/우측 공백이 제거된 문자열
 * 5. 사 용 예 : alert("["+"    좌/우 공백 모두 제거됨     ".trim()+"]");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.trim = function() {
    return this.replace(/(^\s*)|(\s*$)/g, "");
}

/**
 * 1. 이    름 : ltrim
 * 2. 설    명 : 문자의 좌측 공백을 제거한다.
 * 3. 인    자 :
 * 4. 반 환 값 : String - 좌측 공백이 제거된 문자열
 * 5. 사 용 예 : alert("["+"     좌측 공백만 제거됨     ".ltrim()+"]");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.ltrim = function() {
    return this.replace(/(^\s*)/, "");
}

/**
 * 1. 이    름 : rtrim
 * 2. 설    명 : 문자의 우측 공백을 제거한다.
 * 3. 인    자 :
 * 4. 반 환 값 : String - 우측 공백이 제거된 문자열
 * 5. 사 용 예 : alert("["+"     우측 공백만 제거됨     ".rtrim()+"]");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.rtrim = function() {
    return this.replace(/(\s*$)/, "");
}

/**
 * 1. 이    름 : isEmpty
 * 2. 설    명 : 문자값이 빈 값인지 체크한다.
 * 3. 인    자 :
 * 4. 반 환 값 : Boolean True  - 빈 값
 *               Boolean False - 값 존재
 * 5. 사 용 예 : var a = "예제 문자열  ";
 *               if(a.isEmpty()) {alert("비었음");} else {alert("안 비었음");}
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.isEmpty = function() {
    return cfn_isEmpty(this);
}

/**
 * 1. 이    름 : parseInt
 * 2. 설    명 : 숫자로 변환 가능한 문자열을 int로 변환한다.
 * 3. 인    자 :
 * 4. 반 환 값 : Number
 * 5. 사 용 예 : alert("200".parseInt());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.parseInt = function() {
    if(!isNaN(this)) {
        return parseInt(this);
    } else {
        return null;
    }
}


/**
 * 1. 이    름 : toDate
 * 2. 설    명 : 문자열을 Date 객체로 변환하여 반환한다.
 * 3. 인    자 : String - 날자 문자열의 패턴
 * 4. 반 환 값 : Date
 * 5. 사 용 예 : alert("20090120".toDate());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.toDate = function(pattern) {
    var index = -1;
    var year;
    var month;
    var day;
    var hour = 0;
    var min  = 0;
    var sec  = 0;
    var ms   = 0;
    var newDate;

    if (pattern == null) {
        pattern = "YYYYMMDD";
    }

    if ((index = pattern.indexOf("YYYY")) == -1 ) {
        index = pattern.indexOf("YY");
        year = "20" + this.substr(index, 2);
    } else {
        year = this.substr(index, 4);
    }

    if ((index = pattern.indexOf("MM")) != -1 ) {
        month = this.substr(index, 2);
    } else {
        month = 1;
    }

    if ((index = pattern.indexOf("DD")) != -1 ) {
        day = this.substr(index, 2);
    } else {
        day = 1;
    }

    if ((index = pattern.indexOf("HH")) != -1 ) {
        hour = this.substr(index, 2);
    }

    if ((index = pattern.indexOf("mm")) != -1 ) {
        min = this.substr(index, 2);
    }

    if ((index = pattern.indexOf("ss")) != -1 ) {
        sec = this.substr(index, 2);
    }

    if ((index = pattern.indexOf("SS")) != -1 ) {
        ms = this.substr(index, 2);
    }

    newDate = new Date(year, month - 1, day, hour, min, sec, ms);
    if (month > 12) {
        newDate.setFullYear(year + 1);
    } else {
        newDate.setFullYear(year);
    }

    return newDate;
}


/**
 * 1. 이    름 : byteLength
 * 2. 설    명 : 바이트 길이를 반환한다.
 * 3. 인    자 :
 * 4. 반 환 값 : Number - 문자열의 바이트 길이
 * 5. 사 용 예 : alert("팔바이트".byteLength());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.byteLength = function() {
    var cnt = 0;
    for (var i = 0; i < this.length; i++) {
        if (this.charCodeAt(i) > 127)
            cnt += 2;
        else
            cnt++;
    }
    return cnt;
}

/**
 * 1. 이    름 : getNumber
 * 2. 설    명 : 수만 반환한다.
 * 3. 인    자 :
 * 4. 반 환 값 : String - 수인 문자열
 * 5. 사 용 예 : alert("-1,300".getNumber());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.getNumber = function() {
    if(this.charAt(0) == "-") {
        return "-"+ this.replace(/[^\d\.]*/g, "");
      } else {
        return this.replace(/[^\d\.]*/g, "");
      }
}


/**
 * 1. 이    름 : getFigure
 * 2. 설    명 : 숫자만 반환한다.
 * 3. 인    자 :
 * 4. 반 환 값 : String - 숫자로만 이루어진 문자열
 * 5. 사 용 예 : alert("-1,300".getFigure());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.getFigure = function() {
    return (this.trim().replace(/[^0-9]/g, ""));
}


/**
 * 1. 이    름 : meta
 * 2. 설    명 : 정규식에 쓰이는 특수한 문자열을 escape한 뒤 반환한다.
 *               removeChar 함수에 필요
 * 3. 인    자 :
 * 4. 반 환 값 : String - 정규식에 쓰이는 특수문자가 escape된 문자열
 * 5. 사 용 예 : alert("$abc".meta());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.meta = function() {
    var str = this;
    var result = ""
    for(var i = 0; i < str.length; i++) {
        if((/([\$\(\)\*\+\.\[\]\?\\\^\{\}\|]{1})/).test(str.charAt(i))) {
            result += str.charAt(i).replace((/([\$\(\)\*\+\.\[\]\?\\\^\{\}\|]{1})/), "\\$1");
        } else {
            result += str.charAt(i);
        }
    }
    return result;
}


/**
 * 1. 이    름 : removeRegExpChar
 * 2. 설    명 : 인수로 입력받은 문자들을 제거한 문자열을 반환한다.
 *               meta 함수 필수
 * 3. 인    자 : String - 제거할 문자들
 * 4. 반 환 값 : String - 입력받은 문자들이 제거된 문자열
 * 5. 사 용 예 : alert("$abc".removeRegExpChar());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.removeRegExpChar = function(pattern) {
    return (pattern == null) ? this : eval("this.replace(/[" + pattern.meta() + "]/g, \"\")");
}


/**
 * 1. 이    름 : isFigure
 * 2. 설    명 : 숫자로만 이루어졌는지 여부를 반환한다.
 * 3. 인    자 : String - 제외할 문자들
 * 4. 반 환 값 : Boolean - 숫자로만 이루어진 문자열 여부
 * 5. 사 용 예 : alert("-1,300".isFigure("-,"));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.isFigure = function() {
    return (/^[0-9]+$/).test(this.removeRegExpChar(arguments[0])) ? true : false;
}


/**
 * 1. 이    름 : isAlphabet
 * 2. 설    명 : 영문자로만 이루어졌는지 여부를 반환한다.
 * 3. 인    자 : String - 제외할 문자들
 * 4. 반 환 값 : Boolean - 영문자로만 이루어진 문자열 여부
 * 5. 사 용 예 : alert("se7en".isAlphabet("7"));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.isAlphabet = function() {
    return (/^[a-zA-Z]+$/).test(this.removeRegExpChar(arguments[0])) ? true : false;
}


/**
 * 1. 이    름 : isAlphaNum
 * 2. 설    명 : 영문자와 숫자로만 이루어졌는지 여부를 반환한다.
 * 3. 인    자 :
 * 4. 반 환 값 : Boolean - 염문자와 숫자로만 이루어진 문자열 여부
 * 5. 사 용 예 : alert("window2003".isAlphaNum());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.isAlphaNum = function() {
    return (/^[0-9a-zA-Z]+$/).test(this.removeRegExpChar(arguments[0])) ? true : false;
}


/**
 * 1. 이    름 : isNumber
 * 2. 설    명 : 수인지 여부를 반환한다.
 * 3. 인    자 :
 * 4. 반 환 값 : Boolean - 수 여부
 * 5. 사 용 예 : alert("-123".isNumber());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 */
String.prototype.isNumber = function() {
    return (/^(\-)?[0-9]*(\.[0-9]*)?$/).test(this.removeRegExpChar(arguments[0])) ? true : false;
}


/**
 * 1. 이    름 : isUserid
 * 2. 설    명 : 영문자로시작하고 영문자와 숫자로만 이루어졌는지 여부를 반환한다.
 * 3. 인    자 :
 * 4. 반 환 값 : Boolean - 영문자로 시작하고 영문자와 숫자로만 이루어진 문자열 여부
 * 5. 사 용 예 : alert("window2003".isAlphanumeric());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.isUserid = function() {
    return (/^[a-zA-z]{1}[0-9a-zA-Z]+$/).test(this.remove(arguments[0])) ? true : false;
}


/**
 * 1. 이    름 : isDate
 * 2. 설    명 : 날자로 변환 간능한지 체크
 * 3. 인    자 : String - 날짜 표시 패천
 * 4. 반 환 값 : Boolean - 날자로 변환 여부
 * 5. 사 용 예 : alert("2008/10/10".isDate(YYYY/MM/DD));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.isDate = function(pattern) {
    if(pattern == null || pattern.trim() == "") {
        pattern = "YYYYMMDD";
    }
    var d = this.toDate(pattern);
    if(this.getFigure() != d.getFullYear()+(d.getMonth()+1 +"").lpad("0", 2)+(d.getDate() +"").lpad("0",2)) {
        return false;
    } else {
        return true;
    }
}


/**
 * 1. 이    름 : lpad
 * 2. 설    명 : 문자열이 l의 길이가 될 때까지 왼쪽에 str을 붙이고 반환한다.
 * 3. 인    자 : Char, Number
 * 4. 반 환 값 : String - 길이가 l인 문자열
 * 5. 사 용 예 : alert("1234".lpad('0', 10));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.lpad = function(c, l) {
    var len = this.length;
    var s = this;

    while(len < l) {
        s = c + s;
        len++;
    }

    return s;
}


/**
 * 1. 이    름 : rpad
 * 2. 설    명 : 문자열이 l의 길이가 될 때까지 오른쪽에 str을 붙이고 반환한다.
 * 3. 인    자 : Char, Number
 * 4. 반 환 값 : String - 길이가 l인 문자열
 * 5. 사 용 예 : alert("1234".rpad('0', 10));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.rpad = function(c, l) {
    var len = this.length;
    var s = this;

    while(len < l) {
        s += c;
        len++;
    }

    return s;
}


/*
 * 1. 이    름 : removeComma
 * 2. 설    명 : 콤마(',')가 포함된 객체의 값에서 콤마를 제거하고 반환한다
 * 3. 인    자 :
 * 4. 반 환 값 : String - 콤마(',')가 제거된 문자열
 * 5. 사 용 예 : alert("1234,567,890".removeComma());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.removeComma = function() {
    return this.replace(/,/gi, "");
}


/*
 * 1. 이    름 : replaceAll
 * 2. 설    명 : 문자열의 모든 일부 문자를 다른 문자로 모두 변경한다.
 * 3. 인    자 : String strForm - 원 문자열
 *               String strTo - 변경 문자열
 * 4. 반 환 값 : String - 모든 문자열 strForm이 strTo으로 변경된 문자열
 * 5. 사 용 예 : alert("1234,567,890".removeComma());
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
String.prototype.replaceAll = function(strFrom, strTo) {
    if(strTo.isEmpty()) {
        strTo = "\"\"";
    }

    return eval("this.replace(/" + strFrom + "/g, "+ strTo +")");
}


/*******************************************************************************
 *
 * 날짜를 입력받아 처리하는 스크립트 모음
 *
 *******************************************************************************
 *
 * 함수 목록 :
 * getFormattedString(string) - 지정된 포멧으로 날짜를 반환한다.
 * addDate(number[, number[, number[, number[, number[, number[, number[, number]]]]]]])
 *      - 현재 날자에서 입력받은 시간을 더한 날자를 반환
 * addDay(number) - 현재 날자에서 입력받은 일을 더한 날자를 반환
 * addMonth(number) - 현재 날자에서 입력받은 월을 더한 날자를 반환
 * addYear(number) - 현재 날자에서 입력받은 년을 더한 날자를 반환
 * compareTo(date[, string]) - 날자를 비교할 날자와 비교한다.
 * getDateGap(date) - 대상 날자와의 차이 일수를 계산한다.
 *
 ******************************************************************************/

var GLB_MONTH_IN_YEAR       = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var GLB_SHORT_MONTH_IN_YEAR = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
var GLB_DAY_IN_WEEK         = ["일요일", "월요일", "화요일", "수요일", "목요일", "금요일", "토요일"];
var GLB_SHORT_DAY_IN_WEEK   = ["일", "월", "화", "수", "목", "금", "토"];
//var GLB_DAYS_IN_MONTH       = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

/*
 * 1. 이    름 : getFormattedString
 * 2. 설    명 : 지정된 포멧으로 날짜를 반환한다.
 * 3. 인    자 : String - 날짜 포멧
 * 4. 반 환 값 : String - 포메팅된 날짜
 * 5. 사 용 예 : alert((new Date()).getFormattedString("YYYY/MM/DD"));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
Date.prototype.getFormattedString = function(pattern) {

    var year      = this.getFullYear();
    var month     = this.getMonth() + 1;
    var day       = this.getDate();
    var dayInWeek = this.getDay();
    var hour24    = this.getHours();
    var ampm      = (hour24 < 12) ? "AM" : "PM";
    var hour12    = (hour24 > 12) ? (hour24 - 12) : hour24;
    var min       = this.getMinutes();
    var sec       = this.getSeconds();

    var YYYY = "" + year;
    var YY   = YYYY.substr(2);
    var MM   = (("" + month).length == 1) ? "0" + month : "" + month;
    var MON  = GLB_MONTH_IN_YEAR[month-1];
    var mon  = GLB_SHORT_MONTH_IN_YEAR[month-1];
    var DD   = (("" + day).length == 1) ? "0" + day : "" + day;
    var DAY  = GLB_DAY_IN_WEEK[dayInWeek];
    var day  = GLB_SHORT_DAY_IN_WEEK[dayInWeek];
    var HH   = (("" + hour24).length == 1) ? "0" + hour24 : "" + hour24;
    var hh   = (("" + hour12).length == 1) ? "0" + hour12 : "" + hour12;
    var mm   = (("" + min).length == 1) ? "0" + min : "" + min;
    var ss   = (("" + sec).length == 1) ? "0" + sec : "" + sec;
    var SS   = "" + this.getMilliseconds();

    var dateStr;
    var index = -1;

    if (typeof(pattern) == "undefined") {
        dateStr = "YYYYMMDD";
    } else {
        dateStr = pattern;
    }

    dateStr = dateStr.replace(/YYYY/g, YYYY);
    dateStr = dateStr.replace(/YY/g,   YY);
    dateStr = dateStr.replace(/MM/g,   MM);
    dateStr = dateStr.replace(/MON/g,  MON);
    dateStr = dateStr.replace(/mon/g,  mon);
    dateStr = dateStr.replace(/DD/g,   DD);
    dateStr = dateStr.replace(/DAY/g,  DAY);
    dateStr = dateStr.replace(/day/g,  day);
    dateStr = dateStr.replace(/hh/g,   hh);
    dateStr = dateStr.replace(/HH/g,   HH);
    dateStr = dateStr.replace(/mm/g,   mm);
    dateStr = dateStr.replace(/ss/g,   ss);
    dateStr = dateStr.replace(/(\s+)a/g, "$1" + ampm);

    return dateStr;
}


/*
 * 1. 이    름 : addDate
 * 2. 설    명 : 현재 날자에서 입력받은 시간을 더한 날자를 반환
 * 3. 인    자 : Number
 * 4. 반 환 값 : Date
 * 5. 사 용 예 : alert((new Date()).addDate(0,0,1));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
Date.prototype.addDate = function(years, months, dates, hours, miniutes, seconds, mss) {

    if (years == null)    years    = 0;
    if (months == null)   months   = 0;
    if (dates == null)    dates    = 0;
    if (hours == null)    hours    = 0;
    if (miniutes == null) miniutes = 0;
    if (seconds == null)  seconds  = 0;
    if (mss == null)      mss      = 0;

    return new Date(this.getFullYear() + years,
                    this.getMonth() + months,
                    this.getDate() + dates,
                    this.getHours() + hours,
                    this.getMinutes() + miniutes,
                    this.getSeconds() + seconds,
                    this.getMilliseconds() + mss
                   );
}


/*
 * 1. 이    름 : compareTo
 * 2. 설    명 : 날자를 비교할 날자와 비교한다.
 * 3. 인    자 : Date, String(날자 패턴)
 * 4. 반 환 값 : Number 1   - 비교값보다 큰 경우
 *               Number 0   - 같은 경우
 *               Number -1  - 비교값보다 작은 경우
 * 5. 사 용 예 : alert((new Date()).compare(1));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
Date.prototype.compareTo = function(targetDate, pattern) {
    var d = this;

    if(cfn_isString(targetDate)) {
        targetDate = targetDate.getFigure().toDate();
    }

    if(!cfn_isDate(targetDate)) {
        return;
    }

    if(pattern == null) {
        pattern = "YYYYMMDD";
    }

    d = d.getFormattedString(pattern);
    targetDate = targetDate.getFormattedString(pattern);

    d = d.toDate();
    targetDate = targetDate.toDate();

    if (d > targetDate) {
        return 1;
    } else if(d < targetDate) {
        return -1;
    } else {
        return 0;
    }
}

/*
 * 1. 이    름 : addDay
 * 2. 설    명 : 현재 날자에서 입력받은 일을 더한 날자를 반환
 * 3. 인    자 : Number
 * 4. 반 환 값 : Date
 * 5. 사 용 예 : alert((new Date()).addDay(1));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 02. 17    이민재     최초 등록
 */
Date.prototype.addDay = function(day) {
    return this.addDate(0, 0, day);
}

/*
 * 1. 이    름 : addMonth
 * 2. 설    명 : 현재 날자에서 입력받은 월을 더한 날자를 반환
 * 3. 인    자 : Number
 * 4. 반 환 값 : Date
 * 5. 사 용 예 : alert((new Date()).addMonth(1));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 02. 17    이민재     최초 등록
 */
Date.prototype.addMonth = function(month) {
    return this.addDate(0, month);
}

/*
 * 1. 이    름 : addYear
 * 2. 설    명 : 현재 날자에서 입력받은 년을 더한 날자를 반환
 * 3. 인    자 : Number
 * 4. 반 환 값 : Date
 * 5. 사 용 예 : alert((new Date()).addYear(1));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 02. 17    이민재     최초 등록
 */
Date.prototype.addYear = function(year) {
    return this.addDate(year);
}

/*
 * 1. 이    름 : getDateGap
 * 2. 설    명 : 대상 날자와의 차이 일수를 계산한다.
 * 3. 인    자 : Date - 비교할 날자
 * 4. 반 환 값 : Number - 차이 일 수
 * 5. 사 용 예 : alert("오늘과 한달 전의 일자 차이는?  "+(new Date()).getDateGap((new Date().addDate(0,1))), 'YYYYMM');
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
Date.prototype.getDateGap = function(targetDate) {
     var d = this;

    if(cfn_isString(targetDate)) {
        targetDate = targetDate.getFigure().toDate();
    }

    if(!cfn_isDate(targetDate)) {
        return;
    }

    d = d.valueOf()/(24*60*60*1000);
    targetDate = targetDate.valueOf()/(24*60*60*1000);

    return (d - targetDate);

}




/*******************************************************************************
 *
 * 숫자를 입력받아 처리하는 스크립트 모음
 *
 *******************************************************************************
 *
 * 함수 목록 :
 * toCurrency(obj) - 입력란의 값의 3자리 마다 콤마를 찍는다.
 * setFldToNumberFld(obj) - 입력란을 숫자 필드로 세팅. 키 입력이벤트를 받아서
 *      3자리마다 자동으로 콤마를 추가한다.
 *
 ******************************************************************************/

/*
 * 1. 이    름 : toCurrency
 * 2. 설    명 : 소수점이 포함된 객체의 값에 3자리마다 콤마(',')를 추가하고 그
 *               값을 반환한다
 * 3. 인    자 : Object
 * 4. 반 환 값 : String - 3자리마다 콤마(',')가 추가된 문자열
 * 5. 사 용 예 : alert(toCurrency(1234567890));
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function toCurrency(obj) {
    var tmp = 0;

    if(cfn_isObject(obj)) {
        tmp = $(obj).value.split(".");
    } else if(cfn_isNumber(obj) || obj.isNumber()) {
        tmp = new String(obj).split(".");
    } else {
        tmp = obj.getNumber().split(".");
    }

    var number = tmp[0];
    var length_of_number = number.length;
    var new_number = "";
    var minusFlag = false;
    for (position = 0; position < length_of_number; position++) {
        if(number.charAt(position) == "-") {
            minusFlag = true;
            continue;
        }
        new_number += number.substring(position, position + 1);
        if (((length_of_number - position - 1) % 3) === 0 && (length_of_number - position - 1) > 0) {
            new_number += ",";
        }
    }
    if(minusFlag) {
        new_number = "-" + new_number;
    }

    if (cfn_isUndefined(tmp[1]) || cfn_isNull(tmp[1])) {
       return new_number;
    } else {
        return new_number + "." + tmp[1];
    }
}

/*******************************************************************************
 *
 * 기타
 *
 ******************************************************************************/

/*
 * 1. 이    름 : cfn_setFldToCurrencyFld
 * 2. 설    명 : 입력란을 통화 필드로 세팅. 키 입력이벤트를 받아서 3자리마다
 *               자동으로 콤마를 추가한다.
 * 3. 인    자 : String - 입력란의 ID
 * 4. 반 환 값 : String - 3자리마다 컴마가 찍힌 문자열
 * 5. 사 용 예 : cfn_setFldToCurrencyFld("fldTest1");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_setFldToCurrencyFld(strId) {
    Event.observe(strId, "keydown", function() {
        $(strId).value = toCurrency($F(strId).removeComma());
    });
    Event.observe(strId, "blur", function() {
        $(strId).value = toCurrency($F(strId).removeComma());
    });
    Event.observe(strId, "keyup", function() {
        $(strId).value = toCurrency($F(strId).removeComma());
    });
}
// 수 필드
function cfn_setFldToNumberFld(strId) {
    Event.observe(strId, "keydown", function() {
        $(strId).value = $F(strId).getNumber();
    });
    Event.observe(strId, "keyup", function() {
        $(strId).value = $F(strId).getNumber();
    });
    Event.observe(strId, "blur", function() {
        $(strId).value = $F(strId).getNumber();
    });
}

// 숫자필드
function cfn_setFldToFigureFld(strId) {
    Event.observe(strId, "keydown", function() {
        $(strId).value = $F(strId).getFigure();
    });
    Event.observe(strId, "blur", function() {
        $(strId).value = $F(strId).getFigure();
    });
}

// return T/F에 따라서..
// ex) <input type="text" style="ime-mode:disabled;" onKeyPress="return numbersonly(event, false)">
function cfn_numbersonly(e, decimal) {
    var key;
    var keychar;

    if (window.event) {
        key = window.event.keyCode;
    } else if (e) {
        key = e.which;
    } else {
        return true;
    }
    keychar = String.fromCharCode(key);

    if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13)
            || (key == 27)) {
        return true;
    } else if ((("0123456789").indexOf(keychar) > -1)) {
        return true;
    } else if (decimal && (keychar == ".")) {
        return true;
    } else
        return false;
}


/*
 * 1. 이    름 : cfn_setTelFld
 * 2. 설    명 : 전화번호 포메팅, 해당 입력란에 키 입력이 있을 때마다 전화번호 포메팅
 * 3. 인    자 : String - 입력란의 ID
 * 4. 반 환 값 : String - 모든 문자열 strForm이 strTo으로 변경된 문자열
 * 5. 사 용 예 : cfn_setTelFld("fldTest2");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 01. 19    이민재     최초 등록
 */
function cfn_setTelFld(strId) {
    Event.observe(strId, "keyup", function(ev) {

        var value = ev.target.value;
        value = value.getFigure();

        if(value.length == 11) {        // 11 자리일때
            $(strId).value = value.substring(0, 3) + "-" + value.substring(3, 7) + "-" + value.substring(7, 11);
        } else if(value.length == 10) { // 10 자리일때
            if(value.substring(0, 2) == "02") {
                $(strId).value = value.substring(0, 2) + "-" + value.substring(2, 6) + "-" + value.substring(6, 10);
            } else {
                $(strId).value = value.substring(0, 3) + "-" + value.substring(3, 6) + "-" + value.substring(6, 10);
            }
        } else if(value.length == 9) {  // 9 자리 일때
            $(strId).value = value.substring(0, 2) + "-" + value.substring(2, 5) + "-" + value.substring(5, 9);
        } else if(value.length > 6) {
            $(strId).value = value.substring(0, 2) + "-" + value.substring(2, 5) + "-" + value.substring(5, value.length);
        } else if(value.length > 3) {
            $(strId).value = value.substring(0, 2) + "-" + value.substring(2, value.length);
        } else {
            $(strId).value = value;
        }
    });
}

/*
 * 1. 이    름 : cfn_setAllCheckbox
 * 2. 설    명 : 전체 체크 체크박스를 클릭시 모든 대상 체크박스의 값을 변경한다.
 * 3. 인    자 : String tot - 전체 체크박스 ID
 *               Stirng chk - 대상 체크박스의 name
 * 4. 반 환 값 :
 * 5. 사 용 예 : <input type="checkbox" onclick="cfn_setAllCheckbox('totChk', 'chk')">
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 03. 11    이민재     최초 등록
 */
function cfn_setAllCheckbox(tot, chk) {
	var f = $(tot).checked;
    var c = document.getElementsByName(chk);
    for(var i = 0; i < c.length; i++) {
        c[i].checked = f;
    }
}

/*
 * 1. 이    름 : cfn_isOnlyOneChecked
 * 2. 설    명 : 대상 체크박스 중 하나의 값만 체크 되었는지 체크한다.
 * 3. 인    자 : String - 대상 체크박스의 name
 * 4. 반 환 값 : boolean
 * 5. 사 용 예 : setFldToNumberFld("fldTest2");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 03. 11    이민재     최초 등록
 */
function cfn_isOnlyOneChecked(name) {
    var c = document.getElementsByName(name);
    var cnt = 0;
    for(var i = 0; i < c.length; i++) {
        if(c[i].checked) {
            cnt++;
        }
    }

    if(cnt < 1) {
        alert("체크된 항목 없음");
        return false;
    } else if( cnt > 1) {
        alert("하나만 선택하시오");
        return false;
    }
    return true;
}

/*
 * 1. 이    름 : cfn_clearForm
 * 2. 설    명 : 대상 폼 내의 입력값들을 초기화한다.
 * 3. 인    자 : String - 대상 폼 name
 * 4. 반 환 값 :
 * 5. 사 용 예 : cfn_clearForm("frmCommCode");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 03. 11    이민재     최초 등록
 */
function cfn_clearForm(frmName) {
    var eleArr = Form.getElements(frmName);

    for(var i = 0; i < eleArr.length; i++) {
        if(eleArr[i].type == "text" || eleArr[i].type == "textarea" || eleArr[i].type == "hidden") {
            eleArr[i].value = "";
        } else if(eleArr[i].type == "check") {
            eleArr[i].checked = false;
        } else if(eleArr[i].tagName.toLowerCase() == "select") {
            eleArr[i].selectedIndex = 0;
        }
    }
}

/*
 * 1. 이    름 : cfn_openModal
 * 2. 설    명 : MODAL 팝업을 연다.
 *               현재 IE 5 이상, 파폭 3 이상, 크롬 지원 확인
 * 3. 인    자 : String url - 팝업 URL
 *               Object - 모달창으로 넘길 인자
 *               Number width - 모달창의 넓이
 *               Number height - 모달창의 높이
 * 4. 반 환 값 :
 * 5. 사 용 예 : cfn_openModal("/popInstitution.action");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 03. 23    이민재     최초 등록
 */
function cfn_openModal(url, args, width, height) {
	if(cfn_isUndefined(args)) {
		args = "";
	}
	if(cfn_isUndefined(width)) {
		width = 600;
	}
	if(cfn_isUndefined(height)) {
		height = 400;
	}

	var x = screen.availWidth;
	var y = screen.availHeight

	x = (x - width) / 2;
	y = (y - height) / 2;

	var status = "center:1;dialogHeight="+height+"px;dialogWidth="+width+"px;dialogLeft="+x+"px;dialogTop="+y+"px;";
	return window.showModalDialog(url, args, status);

}

/*
 * 1. 이    름 : cfn_openPop
 * 2. 설    명 : 팝업을 연다.
 *               (팝업창에 hdnTarget ID를 가진 객체가 존재해야 target값을 넘길 수 있다.)
 * 3. 인    자 : String url - 팝업 URL
 *               String target - 반환값의 타겟 혹은 구분자(글로벌물류사업에선 폼값을 넘기므로 target값은 필요 없음. 빈값 넘김)
 *               Number width - 팝업창의 넓이
 *               Number height - 팝업창의 높이
 * 4. 반 환 값 :
 * 5. 사 용 예 : cfn_openPop("/popInstitution.action");
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 03. 23    이민재     최초 등록
 */
function cfn_openPop(url, target, width, height) {
	if(cfn_isUndefined(width)) {
		width = 600;
	}
	if(cfn_isUndefined(height)) {
		height = 400;
	}

	var x = screen.availWidth;
	var y = screen.availHeight

	x = (x - width) / 2;
	y = (y - height) / 2;

	var status = "height="+height+",width="+width+",left="+x+",top="+y+",scrollbars=1";
	var p = window.open(url, "_blank", status);
	var tid;
	if(!cfn_isNull(target)) {
		//cfn_openPop1(p, "111111111", tid);
	}
}
function cfn_openPop1(p, target, tid) {
	var h;
	setInterval(function() {
		try {
			h = p.document.getElementById('fldSrchOrganName');
		} catch (err) {
			h = null;
		}
		if(!cfn_isUndefined(h) && !cfn_isNull(h)) {
			h.value = target;
			clearInterval(tid);
		}
	}, 500);
}

/*
 * 1. 이    름 : cfn_markRow
 * 2. 설    명 : 테이블에선 선택한 row의 스타일을 변경한다.
 * 3. 인    자 : Object o - 테이블의 tr 객체
 * 4. 반 환 값 :
 * 5. 사 용 예 : <tr id="trList$!cnt" ondblclick="fn_viewDetail('$!row.sno');cfn_markRow(this)">
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 03. 23    이민재     최초 등록
 */
function cfn_markRow(o) {
	var tr = document.getElementsByTagName("tr");
	if(!cfn_isNull(tr)) {
		for(var i = 0; i < tr.length; i++) {
			tr[i].className = "defaultState";
		}
	}
	o.className = "selectedState";
	//alert(o.rowIndex+"+"+tr.rowIndex);
//	$("hdnSrchRownum").value = o.rowIndex;
//	$("hdnDetailRownum").value = o.rowIndex;
}

/*
 * 1. 이    름 : cfn_fckModeSwitch
 * 2. 설    명 : FCK 에디터의 편집 모드를 변경한다. 소스 <->WYSWYG
 * 3. 인    자 : String name - 에디터 ID명
 *               Sgring key - 소스 편집모드인지 에디터 편집 모드인지.(소스 0, WYSWYG 1)
 * 4. 반 환 값 :
 * 5. 사 용 예 : cfn_fckModeSwitch('cd_nm', 0)
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 03. 31    이민재     최초 등록
 */
function cfn_fckModeSwitch(name, key) {
	var oEditor = FCKeditorAPI.GetInstance(name) ;
	if(oEditor.EditMode == FCK_EDITMODE_SOURCE == key) {
		oEditor.SwitchEditMode(false) ;
	}
}

/*
 * 1. 이    름 : cfn_startLoading
 * 2. 설    명 : 화면에 진행중임을 나타내는 레이어를 띄운다.
 * 3. 인    자 :
 * 4. 반 환 값 :
 * 5. 사 용 예 : cfn_startLoading();
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 04. 01    이민재     최초 등록
 */
function cfn_startLoading() {
	var width = document.body.clientWidth;		// 문서의 넓이
	var height = document.body.clientHeight;	// 문서의 높이
	var opacity = 0.8;			// 투명 정도
	var e = $("divLoading");

	if(cfn_isUndefined(e) || cfn_isNull(e)) {
		e = document.createElement("div");	// 레이어 엘리먼트 생성
		e.id = "divLoading";
		e.style.width = width;				// 레이어의 넓이를 문서와 같게
		e.style.height = height;			// 레이어의 높이를 문서와 같게
		e.style.position = "absolute";
		e.style.left = 0;
		e.style.top = 0;
		e.style.backgroundColor = "#ffffff";	// 레이어의 배경색(흰색)
		e.style.display = 'inline';
		e.style.zIndex = 0;						// 레이어의 z-index

		// 브라우저별 투명도 지정
		try{e.style.opacity = opacity;} catch(e) {}
		try{e.style.MozOpacity = opacity;} catch(e) {}
		try{e.style.filter = 'alpha(opacity='+Math.round(opacity * 100)+')';} catch(e) {}

		var t = document.createElement("img");	// 처리중임을 나타내는 이미지 생성
		t.src = '/img/progress.gif';			// 이미지 URL
		t.style.position = "absolute";
		t.style.left = (width - 100)/2;			// 이미지의 가로 위치 중앙
		t.style.top = (height - 100)/2;			// 이미지의 세로 위치 중앙
		t.style.display = 'inline';
		t.style.zIndex = 0;
		e.appendChild(t);
	}

	document.body.appendChild(e);
}

/*
 * 1. 이    름 : cfn_endLoading
 * 2. 설    명 : 화면에서 진행중임을 나타내는 레이어를 지운다.
 * 3. 인    자 :
 * 4. 반 환 값 :
 * 5. 사 용 예 : cfn_endLoading();
 * 6. 변경사항 :
 *      변경일          변경자     변경내용
 *    --------------------------------------------------------------------------
 *      2009. 04. 01    이민재     최초 등록
 */
function cfn_endLoading() {
	var e = $("divLoading");

	if(!cfn_isUndefined(e) && !cfn_isNull(e)) {
		document.body.removeChild(e);
	}
}

/*
 * 1. 이    름 : cfn_popReturn
 * 2. 설    명 : 신고하기 팝업을 연다.
 * 3. 인    자 : 	String contGubun - 컨텐츠 구분
 *              String passiveId - 피신고자 ID
 *              String keyNm1 - Link를 걸기위한 파라미터 이름
 *              String keyVal1 - Link를 걸기위한 파라미터 값
 * 4. 반 환 값 :
 * 5. 사 용 예 : cfn_popReturn("popIcs.action","test3","cont_seq","1");
 * 6. 변경사항 :
 *      변경일          변경자    		변경내용
 *    --------------------------------------------------------------------------
 *      2009. 04. 07    Hippie     	최초 등록
 */
function cfn_popReturn(contGubun, passiveId, keyNm1, keyVal1) {

	var url = "popReturn.action" + "?contGubun=" + contGubun + "&passiveId=" + passiveId + "&keyNm1=" + keyNm1 + "&keyVal1=" + keyVal1;

	width = 500;
	height = 400;

	var x = screen.availWidth;
	var y = screen.availHeight;

	x = (x - width) / 2;
	y = (y - height) / 2;

	var status = "height="+height+",width="+width+",left="+x+",top="+y+",scrollbars=0";

	window.open(url, "_blank", status);

}

/*
 * 1. 이    름 : cfn_avoidBackspaceInReadonly
 * 2. 설    명 : 읽기전용인 입력란에서 백스페이스를 눌렀을때 히스토리백 방지.
 *               body 로드시에 readOnly 값이 참인 element에 대해 일괄적용됨.
 *               문서의 스크립트 부분에 사용예의 코드가 들어가 있어야 함.
 * 3. 인    자 : Event evt - 이벤트객체
 * 4. 반 환 값 :
 * 5. 사 용 예 : Event.observe(document.body, 'keydown', cfn_avoidBackspaceInReadonly, false);
 * 6. 변경사항 :
 *      변경일          변경자    		변경내용
 *    --------------------------------------------------------------------------
 *      2009. 04. 07    이민재     	최초 등록
 */
function cfn_avoidBackspaceInReadonly(evt){
	var child = Event.element(evt);
	//alert('Element id=' + child.id + ' was keydown. readonly = '+ child.readOnly + ", keycode " +evt.keyCode);
	//Event.stop(evt); //avoid another call related to 'parent_node' itself
	if(child.readOnly && evt.keyCode == Event.KEY_BACKSPACE) {
		Event.stop(evt);
		return false;
	}
}

/*
 * 1. 이    름 : cfn_search
 * 2. 설    명 : 엔터 입력시 조회하는 함수(cfn_search)를 호출한다.
 *               단, 조회하는 함수는 fn_search 이고 조회조건 DIV 의 ID가 srchBox여야 한다.
 *               사용예의 코드를 body가 onload된 뒤에 실행시킨다.
 * 3. 인    자 : Event evt - 이벤트객체
 * 4. 반 환 값 :
 * 5. 사 용 예 : Event.observe('srchBox', 'keydown', cfn_search, false);
 * 6. 변경사항 :
 *      변경일          변경자    		변경내용
 *    --------------------------------------------------------------------------
 *      2009. 04. 07    이민재     	최초 등록
 */
function cfn_search(evt) {
		if(evt.keyCode == Event.KEY_RETURN) {
			fn_search();
			Event.stop(evt);
			return false;
		}
	}
////////////////////////////////////////////////////////////////////////////////////
//작성자 : indigo
//작성일 : 2007.01.21
//함수명 : cfn_checkString(검색어 특수문자방지)
//설  명 : 불필요한 html태그공격에 대비하기위해서...급조
////////////////////////////////////////////////////////////////////////////////////

function _CheckType(s,spc) {
	var i;
	for(i=0; i<s.length; i++) {
		if (spc.indexOf( s.substring(i, i+1)) > 0) {
			return false;
		}
	}
	return true;
}
function cfn_checkString(sInput) {
    var _NoInputTag = "~!#$%&*+@^`'{|}><[];:,\\"+"\n"+"\r";
	var sInput;
	if (!_CheckType(sInput,_NoInputTag)){
		alert("HTML태그나 부적절한 특수문자는 지원하지 않습니다.올바른 검색어를 입력해주세요.");
		return false;
	}
	return true;
}

////////////////////////////////////////////////////////////////////////////////////
//작성자 : indigo
//작성일 : 2009.04.14
//함수명 : cfn_clearTagString(검색어 특수문자제거)
//설  명 : 불필요한 html태그공격에 대비하기위해서...급조
////////////////////////////////////////////////////////////////////////////////////
function cfn_clearTagString(sInput) {
   var limit_char = /[~!@\#$%%^&*\\=+|:;?"<,.>']/;
   var tmp_str="";
   if(sInput.length > 0){
	   for(var i=0; i<sInput.length; i++) {
	    var data = sInput.charAt(i);
	    tmp_str+=data.replace(limit_char,"");
	   }
   }
   return tmp_str;
}



////////////////////////////////////////////////////////////////////////////////////
//작성자 : indigo
//작성일 :
//함수명 : cfn_isValidDate(날짜 형식 체크 함수)
//설  명 : 입력날짜에 대한 포멧을 체크한다.월일 또는 년월일, 년2자리,숫자또는 숫자하이픈조합 모두 체크 허용
////////////////////////////////////////////////////////////////////////////////////
 function cfn_isValidDate(obj) {
/**
0303 4자리
03-03 5자리
090303 6자리
09-0303 7자리1
0903-03 7자리2
09-03-03 8자리1
20090303 8자리2
200903-03 9자리1
2009-0303 9자리2
2009-03-04 10자리
 */
	var pt4 = /^\d{2}\d{2}$/;//4자리
	var pt5 = /^\d{2}-\d{2}$/;//5자리
	var pt6 = /^\d{2}\d{2}\d{2}$/;//6자리
	var pt71 = /^\d{2}-\d{2}\d{2}$/;//7자리
	var pt72 = /^\d{2}\d{2}-\d{2}$/;//7자리
	var pt81 = /^\d{2}-\d{2}-\d{2}$/;//8자리
	var pt82 = /^\d{4}\d{2}\d{2}$/;//8자리
	var pt91 = /^\d{4}\d{2}-\d{2}$/;//9자리
	var pt92 = /^\d{4}-\d{2}\d{2}$/;//9자리
	var pt10 = /^\d{4}-\d{2}-\d{2}$/;//10자리

	var now = new Date();
	var year = now.getFullYear();
    var month = now.getMonth() + 1;
    var day = now.getDate();

    if (("" + month).length == 1) {
        month = "0" + month;
    }
    if (("" + day).length == 1) {
        day = "0" + day;
    }
    var y,m,d;
    if(obj.value.length >= 4 && obj.value.length <= 10) {

         switch(obj.value.length) {

         case 4://0303 4자리
               if (!pt4.exec(obj.value)) {
	        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n월일 입력예)"+month+""+day);
	        		obj.value ="";
	        		obj.focus();
	        		return false;
	        	}
	     		y = year;
	     		m = parseInt(obj.value.substr(0,2), 10) - 1;
	     		d = parseInt(obj.value.substr(2,2), 10);
         		break;
         case 5://03-03 5자리
                if (!pt5.exec(obj.value)) {
	        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n월일 입력예)"+month+"-"+day);
	        		obj.value ="";
	        		obj.focus();
	        		return false;
	        	}
	        	y = year;
	     		m = parseInt(obj.value.substr(0,2), 10) - 1;
	     		d = parseInt(obj.value.substr(3,2), 10);
         		break;
         case 6://090303 6자리
                if (!pt6.exec(obj.value)) {
	        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year.substr(0,2)+""+month+""+day);
	        		obj.value ="";
	        		obj.focus();
	        		return false;
	        	}
	        	y = parseInt(obj.value.substr(0,2), 10);
	     		m = parseInt(obj.value.substr(2,2), 10) - 1;
	     		d = parseInt(obj.value.substr(4,2), 10);
         		break;
         case 7:

               if((obj.value).substr(2,1) =='-'){//09-0303 7자리1
	                if (!pt71.exec(obj.value)) {
		        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year.substr(0,2)+"-"+month+""+day);
		        		obj.value ="";
		        		obj.focus();
		        		return false;
		        	}
	     		m = parseInt(obj.value.substr(3,2), 10) - 1;

               }else if((obj.value).substr(4,1) =='-'){//0903-03 7자리2
	                if (!pt72.exec(obj.value)) {
		        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year.substr(0,2)+""+month+"-"+day);
		        		obj.value ="";
		        		obj.focus();
		        		return false;
		        	}
		        m = parseInt(obj.value.substr(2,2), 10) - 1;
               }else{
                   alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.정확한 날짜를 입력해주세요.\n예)"+year+"-"+month+"-"+day+" 또는)"+year+""+month+""+day);
		        		obj.value ="";
		        		obj.focus();
		        		return false;
               }
                y = parseInt(obj.value.substr(0,2), 10);
                d = parseInt(obj.value.substr(5,2), 10);

         		break;
         case 8:
               if((obj.value).substr(2,1) =='-' && (obj.value).substr(5,1) =='-'){//09-03-03 8자리1
	                if (!pt81.exec(obj.value)) {
		        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year.substr(0,2)+"-"+month+""+day);
		        		obj.value ="";
		        		obj.focus();
		        		return false;
		        	}
	     		y = parseInt(obj.value.substr(0,2), 10);
	     		m = parseInt(obj.value.substr(3,2), 10) - 1;

               }else{//20090303 8자리2
	                if (!pt82.exec(obj.value)) {
		        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year+""+month+""+day);
		        		obj.value ="";
		        		obj.focus();
		        		return false;
		        	}
		        y = parseInt(obj.value.substr(0,4), 10);
		        m = parseInt(obj.value.substr(4,2), 10) - 1;
		        }
		        d = parseInt(obj.value.substr(6,2), 10);
         		break;
         case 9:
               if((obj.value).substr(6,1) =='-'){//200903-03 9자리1
	                if (!pt91.exec(obj.value)) {
		        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year+""+month+"-"+day);
		        		obj.value ="";
		        		obj.focus();
		        		return false;
		        	}

	     		m = parseInt(obj.value.substr(4,2), 10) - 1;

               }else if((obj.value).substr(4,1) =='-'){//2009-0303 9자리2
	                if (!pt92.exec(obj.value)) {
		        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year+"-"+month+""+day);
		        		obj.value ="";
		        		obj.focus();
		        		return false;
		        	}
		        m = parseInt(obj.value.substr(5,2), 10) - 1;
               }else{
                   alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year+"-"+month+"-"+day+" 또는)"+year+""+month+""+day);
		        		obj.value ="";
		        		obj.focus();
		        		return false;
               }
	        	y = parseInt(obj.value.substr(0,4), 10);
	     		d = parseInt(obj.value.substr(7,2), 10);
         		break;
         case 10:
               if (!pt10.exec(obj.value)) {
	        		alert("[" + obj.value + "]" + " 날짜형식에 맞지않습니다.\n년월일 입력예)"+year+"-"+month+"-"+day+" 또는)"+year+""+month+""+day);
	        		obj.value ="";
	        		obj.focus();
	        		return false;
	        	}
	        	y = parseInt(obj.value.substr(0,4), 10);
	     		m = parseInt(obj.value.substr(5,2), 10) - 1;
	     		d = parseInt(obj.value.substr(8,2), 10);
         		break;
         }
	   var dt = new Date(y, m, d);
	   if (dt.getYear() == y && dt.getMonth() == m && dt.getDate() == d) {

	       m = parseInt(m,10)+1;

	       if (("" + y).length == 1) {
            y = "200" + y;
	       }else if(("" + y).length==2){
	        y = "20"+y;
	       }
	       if(("" + m).length==1){
	        m = "0"+m;
	       }
	       if(("" + d).length==1){
	        d = "0"+d;
	       }
	        obj.value = y+"-"+m+"-"+d;
	        return true;

	    } else {
	        alert("적합하지않은 날짜가 입력되었습니다. 해당년월에 마지막 일자를 확인하세요.\n년월일 입력예)"+year+"-"+month+"-"+day+" 또는)"+year+""+month+""+day);
	        //obj.value = year+"-"+month+"-"+day;
	         obj.value ="";
	         obj.focus();
	        return false;
	    }

    }else{
           if(obj.value.length > 0) {
	        alert("날짜 표시는 년월일 또는 월일을 숫자로 입력합니다.구분단위로 -(하이픈허용).\n년월일 입력예)"+year+"-"+month+"-"+day+" 또는)"+year+""+month+""+day);
	        //obj.value = year+"-"+month+"-"+day;
	         obj.value ="";
	         obj.focus();
	        return false;
	       }
    }
}

////////////////////////////////////////////////////////////////////////////////////
//작성자 : hippie
//작성일 :
//함수명 : cfn_setIcsCodeFormat(ics코드)
//설   명 :
////////////////////////////////////////////////////////////////////////////////////
    function cfn_setIcsCodeFormat(aStr) {
    	var rtn ="";
    	if(aStr.length >2){
    		rtn = aStr.substring(0,2) + "." + aStr.substring(2,5);
    		if(aStr.length >5){
    			rtn += "." + aStr.substring(5,7);
    		}
    	} else {
    		rtn = aStr;
    	}
    	return rtn;
    }
////////////////////////////////////////////////////////////////////////////////////
//작성자 : indigo
//작성일 : 2009-04-15
//함수명 : cfn_maxLen(object,maxValue)
//설   명 : onKey이벤트시 체크    = 오브젝트와 제한길이(byte값)을 넣어준다.
//예) onkeypress='cfn_maxLen(this,100);'
////////////////////////////////////////////////////////////////////////////////////
	function cfn_maxLen(obj,lens) {
	var t;
	var msglen,defaultlen;
	defaultlen = lens; //byte수
	msglen = defaultlen;
	fix = obj.value.length;
	tmpstr = ""
		for(k=0;k<fix;k++) {
		    t = obj.value.charAt(k);
			if (escape(t).length > 4){
				msglen -= 2;
			}else{
				msglen--;
			}
			if(msglen < 0) {
				alert(defaultlen+"자 미만 입력할 수 있습니다.");
				obj.value = tmpstr;
				msglen =0;
				break;
			}else{
				tmpstr += t;
				obj.focus();
			}
		}
	}

/*
 * 1. 이    름 : cfn_popPost
 * 2. 설    명 : 팝업 (POST 전송을 위한 팝업)
 * 3. 인    자 : 	String url - URL 정보
 *              String winName - target을 걸기위한  Window ID
 *              String width - 팝업의 가로크기
 *              String height - 팝업의 세로크기
 * 4. 반 환 값 :
 * 5. 사 용 예 : cfn_popPost("popIcs.action","test3","400","500");
 * 6. 변경사항 :
 *      변경일          		변경자    		변경내용
 *    --------------------------------------------------------------------------
 *      2009. 04. 24    Hippie     	최초 등록
 */
function cfn_popPost(url, winName, width, height) {

	var x = screen.availWidth;
	var y = screen.availHeight;

	x = (x - width) / 2;
	y = (y - height) / 2;

	var status = "height="+height+",width="+width+",left="+x+",top="+y+",scrollbars=1";

	var aa = window.open(url, winName, status);

}

/*
 * 1. 이    름 : cfn_showLayer
 * 2. 설    명 : ID에 해당하는 레이어를 보여준다.
 * 3. 인    자 : String layer - 레이어 아이디
 *               String c - 레이어 클래스
 * 4. 반 환 값 :
 * 5. 사 용 예 :
 * 6. 변경사항 :
 *      변경일          변경자    		변경내용
 *    --------------------------------------------------------------------------
 *      2009. 04. 24    이민재		최초 등록
 */
function cfn_showLayer(layer, x, y, v, h) {
	var l = $(layer);
	var source = Event.element(event);
	if(cfn_isUndefined(h) || cfn_isNull(h))
		h = 23;
	if(cfn_isUndefined(x) || cfn_isNull(x))
		x = 150;
	if(l.style.display != "inline") {
		cfn_hideLayer(2);
		l.style.position = "absolute";
		l.style.left = x;
		l.style.top = y + ((v-1)*h) - (l.clientHeight/2);
		l.style.display = "inline";
		l.style.zIndex = 0;						// 레이어의 z-index
	}
	l.style.top = y + ((v-1)*h) - (l.clientHeight/2);
	cfn_log(y +"+"+ ((v-1)*h) +"-"+ (l.clientHeight/2) + "=" +(y + ((v-1)*h) - (l.clientHeight/2)));
}

/*
 * 1. 이    름 : cfn_hideLayer
 * 2. 설    명 : ID에 해당하는 레이어를 숨긴다.
 * 3. 인    자 : String C - 레이어 클래스
 * 4. 반 환 값 :
 * 5. 사 용 예 :
 * 6. 변경사항 :
 *      변경일          변경자    		변경내용
 *    --------------------------------------------------------------------------
 *      2009. 04. 24    이민재		최초 등록
 */
function cfn_hideLayer(c) {
	var l = document.getElementsByTagName("div");
	var temp;
	if(c != 2 && Element.hasClassName(Event.element(event), "leyerMenuLeft"))
		return;

	for(var i = 0; i < l.length; i++){
		temp = l[i];
		if(temp.className == "leyerMenuLeftSub")
			temp.style.display = "none";
	}
}

//  디버깅용 콘솔창을 만든다.
function cfn_debugBox() {
	var t = document.createElement("div");	// 처리중임을 나타내는 이미지 생성
	t.id = 'debugConsole';
	t.style.position = "absolute";
	t.style.overflow = "auto";
	t.style.width = 500;
	t.style.height = 100;
	t.style.left = 0;
	t.style.top = document.body.scrollTop;
	t.style.border = "1px solid silver";
	t.style.background = "white";
	document.body.appendChild(t);
}
// 디버깅용 콘솔창 출력
function cfn_log(str) {
	if(cfn_isNull($("debugConsole")))
		return;
	var t = $("debugConsole").innerText;
	if(t.length > 500) {
		t = t.substring(t.length - 500, t.length);
	}
	$("debugConsole").innerText = t+"\n" + new Date().getFormattedString("HH:mm:ss") + " " + str ;
	$("debugConsole").scrollTop = $("debugConsole").scrollHeight;
}
