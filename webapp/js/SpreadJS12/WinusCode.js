//***************
//콤보용 글로벌변수
var MC_DLV_TYP = "017";                 // 출고구분
var MC_CAR_OWN_TYP = "016";             // 운송구분
var MC_CAR_OWN_TYP_KLP = "016001";      // KLP운송
var MC_CAR_OWN_TYP_USR = "016002";      // USER운송
var MC_CAR_OWN_TYP_KCP = "016003";      // KCP운송

var MC_CAR_WGT_TYP = "014";             // 톤수구분
var MC_DEP_PAY_TYP = "026";		        // 물류센타지급구분
var MC_REQ_TYP = "015";		            // 요청구분
var MC_PRD_MNG_TYP = "225";		        // 유형관리구분
var MC_DSP_POS_TYP = "256";		        // 매각 파손부위
var MC_MRO_REQ_TYP = "246";		        // (판매)업체처리구분
var MC_MRO_UNIT_TYP = "247";		    // (판매)판매단위
var MC_MRO_MAJOR_TYP = "248";		    // (판매)접수배송상태구분

// 로그인 업체등급(관리등급)
var MC_LOGIN_MNG_GRD = "116";
var MC_LOGIN_MNG_GRD_KCP_MNG = "116001";    // 관리
var MC_LOGIN_MNG_GRD_KCP_GRP = "116002";    // KCP그룹
var MC_LOGIN_MNG_GRD_KCP_GEN = "116003";    // KCP일반
var MC_LOGIN_MNG_GRD_CST_GRP = "116004";    // 업체 그룹
var MC_LOGIN_MNG_GRD_CST_GEN = "116005";    // 업체 일반

// 업체구분
var MC_CST_TYP = "020";
var MC_CST_TYP_MAK = "020001";    // 제작처
var MC_CST_TYP_DEP = "020002";    // 물류센터
var MC_CST_TYP_TRN = "020003";    // 운송사
var MC_CST_TYP_CTR = "020004";    // 계약처
var MC_CST_TYP_ADM = "020005";    // 실수요처

// 제품구분
var MC_PRD_TYP = "002";
var MC_PRD_TYP_CTN = "002001";            // KCP컨테이너
var MC_PRD_TYP_RPR = "002005";            // 보수재
var MC_PRD_TYP_PLT = "002014";            // 파렛트
var MC_PRD_TYP_ETC = "002099";            // 기타(PBOX)

// 재질구분
var MC_PRD_MAT_TYP = "004";

// 제품등급
var MC_PRD_GRD_TYP = "027";

var MC_DEP_RCP_TYP = "029";

// 사용여부
var MC_USE_TYP = "S01";
var MC_USE_TYP_Y = "S01N";    // 미사용
var MC_USE_TYP_N = "S01Y";    // 사용

// 업체 컨트롤 객체명 변수
var MC_OBJ_CST_TYP = "";
var MC_OBJ_CST_COD = "";
var MC_OBJ_CST_NAM = "";


// 청구서형식
var MC_PRD_DMD_SUB_TYP = "205";

// 웹이동 자료구분
var MC_WEB_DATA_TYP = "057";
var MC_WEB_DATA_TYP_NEW = "057001";     // 신규
var MC_WEB_DATA_TYP_MOD = "057002";     // 수정
var MC_WEB_DATA_TYP_DEL = "057003";     // 삭제


// 웹이동 생성구분
var MC_WEB_CREATE_TYP = "224";
var MC_WEB_CREATE_TYP_DLV = "224001";       // 웹출하통보
var MC_WEB_CREATE_TYP_RCP = "224002";       // 웹입고등록
var MC_WEB_CREATE_TYP_POOL = "224003";      // POOL21
var MC_WEB_CREATE_TYP_KLNET = "224004";     // KL_NET

// 웹이동구분(출하,반납,입고,이동,이적)
var MC_WEB_MOV_TYP = "109";
var MC_WEB_MOV_TYP_DLV = "109001";          // 출하
var MC_WEB_MOV_TYP_RTN = "109002";          // 반납
var MC_WEB_MOV_TYP_RCP = "109003";          // 입고
var MC_WEB_MOV_TYP_MOV = "109004";          // 이동
var MC_WEB_MOV_TYP_TMV = "109005";          // 이적
var MC_WEB_MOV_TYP_REP = "109006";          // 보수재
var MC_WEB_MOV_TYP_DIR = "109007";          // 보수재
//var MC_WEB_MOV_TYP_CONFM = "10900A";         // 출하통보확인용

//***************
//글자색
var FKC_BL = 'blue';        //파랑
var FKC_RD = 'red';         //빨랑
var FKC_GR = 'green';       //녹색
var FKC_OG = 'orange';      //오렌지
var FKC_VL = 'violet';      //보라
var FKC_BK = 'black';       //검정
var FKC_WT = 'white';       //흰색
//배경색
var BKC_BL = '#E1F0FF';     //소계
var BKC_RD = '#FFE1F0';     //합계
var BKC_GR = '#D2F0D2';
var BKC_OG = '#FFFFE4';
var BKC_VL = '#F5E6FF';
var BKC_BK = '#000000';
var BKC_WT = '#FFFFFF';
var BKC_SK = '#EBF7FF';     //Edit활성화

//기능에의한 글자색구분
var FKC_NOR = FKC_BK;
var FKC_STT = FKC_BL;
var FKC_GTT = FKC_RD;
var FKC_NTT = FKC_GR;
var FKC_OTT = FKC_OG;

//기능에의한 배경색구분
var BKC_NOR = FKC_WT;       //흰색
var BKC_BTN = '#E6ECF2';    //버튼색d8e1ed
var BKC_LCK = '#E0E0E0';    //Lock #E9E9E9
var BKC_FRC = '#FFEBC8';    //필수입력
var BKC_STT = BKC_BL;       //소계
var BKC_GTT = BKC_RD;       //합계
var BKC_NTT = BKC_GR;
var BKC_OTT = BKC_OG;

var BKC_DEF = '#D2D6DE';    //라인-default
var BKC_PRI = '#3C8DBC';    //라인-Primary
var BKC_INF = '#00C0EF';    //라인-info
var BKC_SUC = '#00A65A';    //라인-sucess
var BKC_ALT = '#DD4B39';    //라인-alert
var BKC_WAR = '#F39C12';    //라인-warning

//Line Default
var DFC_LINE = '#D4D4D4';

//SPDREAD Height Default
var MC_SPD_HEIGHT_TITLE = '22';
var MC_SPD_HEIGHT = '25';

//SPDREAD HEADER DEFINE
var arrMC_SPD_HEADER_ABC = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];

//SPDREAD CHECK FUNCTION DEFINE
var arrMC_SPD_CHK_SET = [];

//GRID사이즈 컨트롤 객체
var PUB_SW_DEF = 60;
var PUB_SW_SELECT = 30;
var PUB_SW_BTN = 25;
var PUB_SW_SEQ = 40;
var PUB_SW_COMBO60 = 60;    //예아니오,사용미사용
var PUB_SW_COMBO80 = 80;    //일반 TBL_CODE

var PUB_SW_QTY = 60;
var PUB_SW_SUM = 60;
var PUB_SW_COD = 50;        //업체코드,차량번호,유형
var PUB_SW_NAM = 130;
var PUB_SW_TEL = 90;
var PUB_SW_CHIT = 80;
var PUB_SW_REMARK = 150;    //비고

var PUB_SW_DAT08 = 70;      //20161231
var PUB_SW_DAT10 = 90;      //2016-12-31
var PUB_SW_YM06 = 50;       //201612
var PUB_SW_YM07 = 55;       //2016-12

//PDF View 영역 사이즈 컨트롤 변수
var PUB_PDFDESC_POSSIZE = "257";
var PUB_PDFVIEW_WIDTH = "992";

//택배관련
var PUB_HOST_NAME = "POOLATHOME";