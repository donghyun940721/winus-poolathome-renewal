var PUBLIC_SPD_HEIGHT_TITLE = "22";
var PUBLIC_SPD_HEIGHT = "25";
var DFC_LINE = '#D4D4D4';

//***************
//스토리지 init
function pf_savLogin_Infor() {
    jQuery.ajax({type:"POST",url:"/_mod/SAV_LOGIN.asp",
        data:{"COMP_TYPE":pf_getUserCOMP_TYPE()
             ,"LOGIN_ID":pf_getUserLOGIN_ID()
             ,"SCR_W":screen.width
             ,"SCR_H":screen.height
             ,"BRW_W":window.outerWidth
             ,"BRW_H":window.outerHeight
             ,"BRW_T":pf_getBrowserInfo()
             ,"OS_T":pf_getOsInfo()
            },
        dataType:"json",success:function() {console.log("success");},error:function() {}
    });
}

//***************
//사용자 운영체제 정보 가져오기
function pf_getOsInfo() {
    var parser = new UAParser();
    var result = parser.getResult();
    var rtnOSInfo = result.os.name+"-"+result.os.version;
    return rtnOSInfo;
}

//***************
//사용자 브라우저 정보 가져오기
function pf_getBrowserInfo() {
    var parser = new UAParser();
    var result = parser.getResult();
    var rtnBRInfo = result.browser.name+"-"+result.browser.version;
    return rtnBRInfo;
}

//***************
//사용자 위치 정보 가져오기
function pf_getLocation() {if (navigator.geolocation) {navigator.geolocation.getCurrentPosition(showPosition);} else {jQuery("#X_LAT").val("");jQuery("#Y_LON").val("");}}
function showPosition(position) {jQuery("#X_LAT").val(position.coords.latitude);jQuery("#Y_LON").val(position.coords.longitude);}

//***************
//ooooooooooooooooo
//스토리지 init
function pf_initUser() {
    //웹스토리지확인, 초기화
    if(typeof(Storage) !== "undefined") {
        //Code sessionStorage
        window.sessionStorage["LOGIN_ID"] = "";
        window.sessionStorage["LOGIN_NAM"] = "";
        window.sessionStorage["MNG_GRD"] = "";
        window.sessionStorage["COMP_TYPE"] = "";
        window.sessionStorage["CST_COD"] = "";
        window.sessionStorage["CST_NAM"] = "";
        window.sessionStorage["BIZ_REG_NUM"] = "";
        window.sessionStorage["BIZ_TYP_NAM"] = "";
        window.sessionStorage["BIZ_ITM_NAM"] = "";
        window.sessionStorage["CHIEF_NAM"] = "";
        window.sessionStorage["CST_ADDR_HD"] = "";
        window.sessionStorage["CST_ADDR_DTL"] = "";
        window.sessionStorage["CST_TEL_NUM"] = "";
        window.sessionStorage["CST_FAX_NUM"] = "";
        window.sessionStorage["DPT_COD"] = "";
        window.sessionStorage["DPT_NAM"] = "";
        window.sessionStorage["DPT_TEL_NUM"] = "";
        window.sessionStorage["EMP_COD"] = "";
        window.sessionStorage["EMP_NAM"] = "";
        window.sessionStorage["EMP_TEL"] = "";
        window.sessionStorage["EMP_EMAIL"] = "";
        window.sessionStorage["EMP_HAND_PHONE_NUM"] = "";
        window.sessionStorage["CST_TYP"] = "";
        window.sessionStorage["USE_FLG"] = "";
        window.sessionStorage["WEB_MENU"] = "";
        window.sessionStorage["CST_ARV"] = "";// 간편입고용
        window.sessionStorage["CST_PRD"] = "";// 간편입고용
        window.sessionStorage["SND_CST_COD"] = "";// 간편입고용
        window.sessionStorage["REQ_TEMPLATE"] = "";         // 간편입고 양식(그룹
        window.sessionStorage["REQ_TEMPLATE_DETAIL"] = "";  // 간편입고 양식(상세)
        window.sessionStorage["GRP_CST_LIST"] = "";// 그룹재고용
        window.sessionStorage["API_KEY"] = "";
        // 브라우저 IE 여부 값 세팅
        if ( (navigator.appName == "Netscape" && navigator.userAgent.search("Trident") != -1) || (agent.indexOf("msie") != -1) ) {
            window.sessionStorage["IE_FLG"] = "Y";  // IE 브라우저
        } else {
            window.sessionStorage["IE_FLG"] = "N";  // 기타 브라우저
        }
    } else {
        alert("현재 사용중인 브라우져는\n" +
              "프로그램을 실행하는데, 적합하지 않습니다.\n\n" +
              "크롬4.0, IE10이상 브라우져를 권장합니다.");
    };
}
//스토리지 set
function pf_setUser(varLOGIN_ID, varLOGIN_NAM , varMNG_GRD, varCOMP_TYPE
        , varCST_COD, varCST_NAM, varBIZ_REG_NUM, varBIZ_TYP_NAM
        , varBIZ_ITM_NAM, varCHIEF_NAM, varCST_ADDR_HD, varCST_ADDR_DTL
        , varCST_TEL_NUM, varCST_FAX_NUM, varDPT_COD, varDPT_NAM, varDPT_TEL_NUM, varDPT_FAX_NUM
        , varEMP_COD, varEMP_NAM, varEMP_TEL, varEMP_EMAIL
        , varCST_TYP, varUSE_FLG, varREQ_FLG, varLOGIN_DATE, varAPI_KEY, varCOMP_COD) {//, varEMP_HAND_PHONE_NUM
    window.localStorage.setItem("COMP_TYPE", gf_Null(varCOMP_TYPE,""));
    window.localStorage.setItem("LOGIN_ID", gf_Null(varLOGIN_ID,""));
    window.sessionStorage.setItem("LOGIN_ID", gf_Null(varLOGIN_ID,""));
    window.sessionStorage.setItem("LOGIN_NAM", gf_Null(varLOGIN_NAM,""));
    window.sessionStorage.setItem("MNG_GRD", gf_Null(varMNG_GRD,""));
    window.sessionStorage.setItem("COMP_TYPE", gf_Null(varCOMP_TYPE,""));
    window.sessionStorage.setItem("CST_COD", gf_Null(varCST_COD,""));
    window.sessionStorage.setItem("CST_NAM", gf_Null(varCST_NAM,""));
    window.sessionStorage.setItem("BIZ_REG_NUM", gf_Null(varBIZ_REG_NUM,""));
    window.sessionStorage.setItem("BIZ_TYP_NAM", gf_Null(varBIZ_TYP_NAM,""));
    window.sessionStorage.setItem("BIZ_ITM_NAM", gf_Null(varBIZ_ITM_NAM,""));
    window.sessionStorage.setItem("CHIEF_NAM", gf_Null(varCHIEF_NAM,""));
    window.sessionStorage.setItem("CST_ADDR_HD", gf_Null(varCST_ADDR_HD,""));
    window.sessionStorage.setItem("CST_ADDR_DTL", gf_Null(varCST_ADDR_DTL,""));
    window.sessionStorage.setItem("CST_TEL_NUM", gf_Null(varCST_TEL_NUM,""));
    window.sessionStorage.setItem("CST_FAX_NUM", gf_Null(varCST_FAX_NUM,""));
    window.sessionStorage.setItem("DPT_COD", gf_Null(varDPT_COD,""));
    window.sessionStorage.setItem("DPT_NAM", gf_Null(varDPT_NAM,""));
    window.sessionStorage.setItem("DPT_TEL_NUM", gf_Null(varDPT_TEL_NUM,""));
    window.sessionStorage.setItem("DPT_FAX_NUM", gf_Null(varDPT_FAX_NUM,""));
    window.sessionStorage.setItem("EMP_COD", gf_Null(varEMP_COD,""));
    window.sessionStorage.setItem("EMP_NAM", gf_Null(varEMP_NAM,""));
    window.sessionStorage.setItem("EMP_TEL", gf_Null(varEMP_TEL,""));
    window.sessionStorage.setItem("EMP_EMAIL", gf_Null(varEMP_EMAIL,""));
    window.sessionStorage.setItem("CST_TYP", gf_Null(varCST_TYP,""));
    window.sessionStorage.setItem("USE_FLG", gf_Null(varUSE_FLG,""));
    window.sessionStorage.setItem("REQ_FLG", gf_Null(varREQ_FLG,""));
	window.sessionStorage.setItem("LOGIN_DATE", gf_Null(varLOGIN_DATE,""));
	window.sessionStorage.setItem("API_KEY", gf_Null(varAPI_KEY,""));
	window.sessionStorage.setItem("COMP_COD", gf_Null(varCOMP_COD,""));
//    window.sessionStorage.setItem("EMP_HAND_PHONE_NUM", gf_Null(varEMP_HAND_PHONE_NUM,""));
//    window.sessionStorage.setItem("WEB_MENU", gf_Null(varWEB_MENU,""));
//    window.sessionStorage.setItem("CST_ARV", gf_Null(varCST_ARV,""));
//    window.sessionStorage.setItem("IE_FLG", gf_Null(varIE_FLG,""));
}

//ooooooooooooooooo
//스토리지 call
function pf_getUserLOGIN_ID() {return gf_Null(window.sessionStorage["LOGIN_ID"],"");}
function pf_getUserLOGIN_NAM() {return gf_Null(window.sessionStorage["LOGIN_NAM"],"");}
function pf_getUserMNG_GRD() {return gf_Null(window.sessionStorage["MNG_GRD"],"");}
function pf_getUserCOMP_TYPE() {return gf_Null(window.sessionStorage["COMP_TYPE"],"");}
function pf_getUserCST_COD() {return gf_Null(window.sessionStorage["CST_COD"],"");}
function pf_getUserCST_NAM() {return gf_Null(window.sessionStorage["CST_NAM"],"");}
function pf_getUserBIZ_REG_NUM() {return gf_Null(window.sessionStorage["BIZ_REG_NUM"],"");}
function pf_getUserBIZ_TYP_NAM() {return gf_Null(window.sessionStorage["BIZ_TYP_NAM"],"");}
function pf_getUserBIZ_ITM_NAM() {return gf_Null(window.sessionStorage["BIZ_ITM_NAM"],"");}
function pf_getUserCHIEF_NAM() {return gf_Null(window.sessionStorage["CHIEF_NAM"],"");}
function pf_getUserCST_ADDR_HD() {return gf_Null(window.sessionStorage["CST_ADDR_HD"],"");}
function pf_getUserCST_ADDR_DTL() {return gf_Null(window.sessionStorage["CST_ADDR_DTL"],"");}
function pf_getUserCST_TEL_NUM() {return gf_Null(window.sessionStorage["CST_TEL_NUM"],"");}
function pf_getUserCST_FAX_NUM() {return gf_Null(window.sessionStorage["CST_FAX_NUM"],"");}
function pf_getUserDPT_COD() {return gf_Null(window.sessionStorage["DPT_COD"],"");}
function pf_getUserDPT_NAM() {return gf_Null(window.sessionStorage["DPT_NAM"],"");}
function pf_getUserDPT_TEL_NUM() {return gf_Null(window.sessionStorage["DPT_TEL_NUM"],"");}
function pf_getUserDPT_FAX_NUM() {return gf_Null(window.sessionStorage["DPT_FAX_NUM"],"");}
function pf_getUserEMP_COD() {return gf_Null(window.sessionStorage["EMP_COD"],"");}
function pf_getUserEMP_NAM() {return gf_Null(window.sessionStorage["EMP_NAM"],"");}
function pf_getUserEMP_TEL() {return gf_Null(window.sessionStorage["EMP_TEL"],"");}
function pf_getUserEMP_EMAIL() {return gf_Null(window.sessionStorage["EMP_EMAIL"],"");}
//function pf_getUserEMP_HAND_PHONE_NUM() {return gf_Null(window.sessionStorage["EMP_HAND_PHONE_NUM"],"");}
function pf_getUserCST_TYP() {return gf_Null(window.sessionStorage["CST_TYP"],"");}
function pf_getUserUSE_FLG() {return gf_Null(window.sessionStorage["USE_FLG"],"");}
function pf_getUserREQ_FLG() {return gf_Null(window.sessionStorage["REQ_FLG"],"");}
function pf_getUserLOGIN_DATE() {return gf_Null(window.sessionStorage["LOGIN_DATE"],"");}
function pf_getUserWEB_MENU() {return gf_Null(window.sessionStorage["WEB_MENU"],"");}
function pf_getUserCST_ARV() {return gf_Null(window.sessionStorage["CST_ARV"],"");}
function pf_getUserCST_PRD() {return gf_Null(window.sessionStorage["CST_PRD"],"");}
function pf_getUserSND_CST_COD() {return gf_Null(window.sessionStorage["SND_CST_COD"],"");}
function pf_getUserIE_FLG() {return gf_Null(window.sessionStorage["IE_FLG"],"");}
function pf_getUserAPI_KEY() {return gf_Null(window.sessionStorage["API_KEY"],"");}
function pf_getUserCOMP_COD() {return gf_Null(window.sessionStorage["COMP_COD"],"");}
function pf_getTemplateGroup() {return gf_Null(window.sessionStorage["REQ_TEMPLATE"],"");}
function pf_getTemplateGroupDetail() {return gf_Null(window.sessionStorage["REQ_TEMPLATE_DETAIL"],"");}
function pf_getGrpCstList() {return gf_Null(window.sessionStorage["GRP_CST_LIST"],"");}

//oooooooooooooooooo
//스토리지 user chk
function pf_chkUser() {
    // 스프레드JS key 입력
//    GC.Spread.Sheets.LicenseKey = "E186645884313253#A0G4nTMiojIklkI1pjIEJCLi4TP7FDWoVVYp3iSrN5bNBTaXx6bDJXWEhzVMJUZzgVRNNUMytkVzQXUMdXQrg4VstCaLJEZqRkUspnSTNWaKlGTxtmarkFT5xENOZXVQ3kY0hTVZhmYxInI0IyUiwyN7ITNzEDN8ETM0IicfJye&Qf35VfiEzRwEkI0IyQiwiIwEjL6ByUKBCZhVmcwNlI0IiTis7W0ICZyBlIsICO5MDNxADIyAjMxYTMwIjI0ICdyNkIsISMwEDM7EDMyIiOiAHeFJCLiQVRONVSH3ETVJiOiEmTDJCLlVnc4pjIsZXRiwiIzUjMzEzM4gDq3oj";
    if (pf_getUserCOMP_TYPE()=="" || pf_getUserLOGIN_ID()=="" || pf_getUserWEB_MENU()=="") {
        alert("사용자정보가 부정확 합니다.\n로그인 후 사용 바랍니다.");
        parent.location="http://wcps.logisall.com";
    };
}

//oooooooooooooooooo
// 간편Link 프로그램 ACCESS chk
function pf_chkProgramAccess(pstrPROGRAM) {
    if ((pf_getUserCST_TYP() == MC_CST_TYP_CTR)) {jQuery("#idCTR41003WF").show();jQuery("#idCTR42001WF").show();jQuery("#idCTR42003WF").show();};
    if ((pf_getUserMNG_GRD() == MC_LOGIN_MNG_GRD_KCP_MNG) || (pf_getUserMNG_GRD() == MC_LOGIN_MNG_GRD_KCP_GRP) || (pf_getUserMNG_GRD() == MC_LOGIN_MNG_GRD_KCP_GEN)) {jQuery("#idCTR41003WF").show();jQuery("#idCTR42001WF").show();jQuery("#idCTR42003WF").show();};
    //pstrPROGRAM에 의한 확인사항
}

//oooooooooooooooooo
//스토리지 user chk popup전용 (window.opener 참조)
function pf_chkUser_POPUP() {
    // 스프레드JS key 입력
//    GC.Spread.Sheets.LicenseKey = "E186645884313253#A0G4nTMiojIklkI1pjIEJCLi4TP7FDWoVVYp3iSrN5bNBTaXx6bDJXWEhzVMJUZzgVRNNUMytkVzQXUMdXQrg4VstCaLJEZqRkUspnSTNWaKlGTxtmarkFT5xENOZXVQ3kY0hTVZhmYxInI0IyUiwyN7ITNzEDN8ETM0IicfJye&Qf35VfiEzRwEkI0IyQiwiIwEjL6ByUKBCZhVmcwNlI0IiTis7W0ICZyBlIsICO5MDNxADIyAjMxYTMwIjI0ICdyNkIsISMwEDM7EDMyIiOiAHeFJCLiQVRONVSH3ETVJiOiEmTDJCLlVnc4pjIsZXRiwiIzUjMzEzM4gDq3oj";
    if (gf_Null(window.opener.sessionStorage["COMP_TYPE"],"")=="" || gf_Null(window.opener.sessionStorage["LOGIN_ID"],"")=="" || gf_Null(window.opener.sessionStorage["WEB_MENU"],"")=="") {
        alert("사용자정보가 부정확 합니다.\n로그인 후 사용 바랍니다.[popup chk]");
        parent.location="http://wcps.logisall.com";
    };
}
//oooooooooooooooooo
//로그인 스토리지 함수
function pf_SetLOGIN(ds) {
    pf_setUser(ds[0].LOGIN_ID, ds[0].LOGIN_NAM, ds[0].MNG_GRD
            , ds[0].COMP_TYPE, ds[0].CST_COD, ds[0].CST_NAM
            , ds[0].BIZ_REG_NUM, ds[0].BIZ_TYP_NAM, ds[0].BIZ_ITM_NAM, ds[0].CHIEF_NAM
            , ds[0].CST_ADDR_HD, ds[0].CST_ADDR_DTL, ds[0].CST_TEL_NUM, ds[0].CST_FAX_NUM
            , ds[0].DPT_COD, ds[0].DPT_NAM, ds[0].DPT_TEL_NUM, ds[0].DPT_FAX_NUM
            , ds[0].EMP_COD, ds[0].EMP_NAM, ds[0].EMP_TEL, ds[0].EMP_EMAIL
            , ds[0].CST_TYP, ds[0].USE_FLG, ds[0].REQ_FLG, ds[0].LOGIN_DATE, ds[0].API_KEY, ds[0].COMP_COD); // 입고요청 가능 여부
}
//oooooooooooooooooo
//조회처리함수
//차후 FRAME-MAIN 페이지로 이동
function pf_GetWcpsMenu(ds) {
    var getParameter = {"USER_LOGIN_ID":ds[0].LOGIN_ID,"USER_API_KEY":ds[0].API_KEY,"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":ds[0].CST_COD,"CST_TYP":ds[0].CST_TYP};
    jQuery.ajax({type : "POST", url : "MENU_QRY.asp",
        datatype : "json", contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data : getParameter,
        success: function (data) {window.sessionStorage.setItem("WEB_MENU",data);window.location="/_MAIN";},
        error: function (ex) {alert("Exception:" + ex);}
    });
}
//oooooooooooooooooo
//초기 조회처리함수 실착지
//차후 FRAME-MAIN 페이지로 이동
function pf_GetCstArv(ds) {
    //처리조건 chk
    //실제파라메터
    var getParameter = {"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":ds[0].CST_COD};
    //개발자파라메터
    //var getParameter = {"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":"290799"};
    jQuery.ajax({type : "POST", url : "/_POPUP/_SRCP_REQ/CST_ARV_QRY.asp",
        datatype : "json", contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data : getParameter,
        success: function (data) {window.sessionStorage.setItem("CST_ARV",data);},
        error: function (ex) {alert("Exception:" + ex);}
    });
}
//oooooooooooooooooo
//초기 조회처리함수 유형
//차후 FRAME-MAIN 페이지로 이동
function pf_GetCstPrd(ds) {
    //처리조건 chk
    //실제파라메터
    var getParameter = {"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":ds[0].CST_COD};
    //개발자파라메터
    //var getParameter = {"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":"290799"};
    jQuery.ajax({type : "POST", url : "/_POPUP/_SRCP_REQ/CST_PRD_QRY.asp",
        datatype : "json", contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data : getParameter,
        success: function (data) {window.sessionStorage.setItem("CST_PRD",data);},
        error: function (ex) {alert("Exception:" + ex);}
    });
}
//oooooooooooooooooo
//초기 조회처리함수 물류센터(SND_CST_COD)
//차후 FRAME-MAIN 페이지로 이동
function pf_GetSndCst(ds) {
    //처리조건 chk
    //실제파라메터
    var getParameter = {"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":ds[0].CST_COD};
    //개발자파라메터
    //var getParameter = {"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":"293790"};
    jQuery.ajax({type : "POST", url : "/_POPUP/_SRCP_REQ/SND_CST_QRY.asp",
        datatype : "json", contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data : getParameter,
        success: function (data) {window.sessionStorage.setItem("SND_CST_COD",data);},
        error: function (ex) {alert("Exception:" + ex);}
    });
}
//oooooooooooooooooo
//입고요청 양식 그룹핑
function pf_GetReqTemplate() {
    //처리조건 chk
    //실제파라메터
    var getParameter = {"COMP_TYPE":pf_getUserCOMP_TYPE(),"CST_COD":pf_getUserCST_COD()};
    //개발자파라메터
    jQuery.ajax({type : "POST", url : "/_POPUP/_SRCP_REQ/REQ_TEMPLATE_QRY.asp",
        datatype : "json", contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data : getParameter,
        success: function (data) {window.sessionStorage.setItem("REQ_TEMPLATE",data);},
        error: function (ex) {alert("Exception:" + ex);}
    });
}
//oooooooooooooooooo
//입고요청 양식 상세
function pf_GetReqTemplateDetail() {
    //처리조건 chk
    //실제파라메터
    var getParameter = {"COMP_TYPE":pf_getUserCOMP_TYPE(),"CST_COD":pf_getUserCST_COD()};
    //개발자파라메터
    jQuery.ajax({type : "POST", url : "/_POPUP/_SRCP_REQ/REQ_TEMPLATE_DETAIL_QRY.asp",
        datatype : "json", contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data : getParameter,
        success: function (data) {window.sessionStorage.setItem("REQ_TEMPLATE_DETAIL",data);},
        error: function (ex) {alert("Exception:" + ex);}
    });
}
//oooooooooooooooooo
//초기 조회처리함수 재고그룹 하위 업체들 조회용 (CST_COD)
//차후 FRAME-MAIN 페이지로 이동
function pf_GetGrpCst(ds) {
    //처리조건 chk
    //실제파라메터
    var getParameter = {"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":ds[0].CST_COD};
    //개발자파라메터
    //var getParameter = {"COMP_TYPE":ds[0].COMP_TYPE,"CST_COD":"293790"};
    jQuery.ajax({type : "POST", url : "/_POPUP/_CST/GRP_CST_QRY.asp",
        datatype : "json", contentType: "application/x-www-form-urlencoded;charset=utf-8",
        data : getParameter,
        success: function (data) {window.sessionStorage.setItem("GRP_CST_LIST",data);},
        error: function (ex) {alert("Exception:" + ex);}
    });
}

//oooooooooooooooooo
//GRP_CST_LIST 콤보 활성화 공란인 경우 크기 2 - '[]'
function pf_OnOffGrpCstList() {
    if (pf_getGrpCstList().length > 2){
        jQuery('#TBL_GRP_CST_LIST').show();
    } else {
        jQuery('#TBL_GRP_CST_LIST').hide();
    }
}

//***************
//null check함수
function gf_isNULL(obj) {return (typeof obj != "undefined" && obj != null && obj != "") ? false : true;}
function gf_Null(vartmp,varnew) {
    if (vartmp == "undefined") {vartmp = varnew;}
    if (vartmp == undefined) {vartmp = varnew;}
    if (vartmp == "null") {vartmp = varnew;}
    if (vartmp == null) {vartmp = varnew;}
    if (vartmp == "") {vartmp = varnew;}
    return vartmp;
}
//***************
//숫자단위 변환함수
function pf_getComma(vartmp) {
  var reg = /(^[+-]?\d+)(\d{3})/; // 정규식
  vartmp += ""; // 숫자를 문자열로 변환
  while (reg.test(vartmp))
    vartmp = vartmp.replace(reg, "jQuery1" + "," + "jQuery2");
  return vartmp;
}
//////////***************
//////////html-BG변수
////////function gf_RowBgColor(row) {
////////	var temp_row = 0;
////////	var temp_html = "";
////////	temp_row = row % 2;
////////	if (temp_row == 0) {temp_html = " style='background-color:#E9E9E9' ";}
////////	return temp_html;
////////}
//***************
//요일가져오기 (vardate=20150420)형식
function pf_getWEEK(vardate){
    var yy = parseInt(vardate.substr(0, 4));
    var mm = parseInt(vardate.substr(4, 2));
    var dd = parseInt(vardate.substr(6, 2));
    var varday = new Date(yy,mm - 1, dd);
    var varweek = new Array('일', '월', '화', '수', '목', '금', '토');
    return varweek[varday.getDay()];
}
//***************
//LostFocus
function pf_getNAM_CST(rtnid_code,rtnid_name) {
rtnid_code="#"+rtnid_code;
rtnid_name="#"+rtnid_name;
if((jQuery(rtnid_code).val()).length != 6) {jQuery(rtnid_name).val("");return;};
jQuery.ajax({type:"POST",url:"/_mod/INF_CST.asp",
    data:{"CODE":jQuery(rtnid_code).val()},dataType:"json",
    success:function(ds) {jQuery(rtnid_name).val('');jQuery.each(ds, function(i, entry){jQuery(rtnid_name).val(ds[i].NAME)})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
//***************
//LostFocus
function pf_getNAM_ITEM(varcst,rtnid_code,rtnid_name) {
rtnid_code="#"+rtnid_code;
rtnid_name="#"+rtnid_name;
if((jQuery(rtnid_code).val()).length != 6) {jQuery(rtnid_name).val("");return;};
jQuery.ajax({type:"POST",url:"/_mod/INF_ITEM.asp",
    data:{"CST":varcst,"CODE":jQuery(rtnid_code).val()},dataType:"json",
    success:function(ds) {jQuery(rtnid_name).val('');jQuery.each(ds, function(i, entry){jQuery(rtnid_name).val(ds[i].NAME)})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
//***************
//LostFocus
function pf_getNAM_PRD(rtnid_code,rtnid_name) {
rtnid_code="#"+rtnid_code;
rtnid_name="#"+rtnid_name;
if((jQuery(rtnid_code).val()).length != 5) {jQuery(rtnid_name).val("");return;};
jQuery.ajax({type:"POST",url:"/_mod/INF_PRD.asp",
    data:{"CODE":jQuery(rtnid_code).val(),"NAME":jQuery(rtnid_name).val()},dataType:"json",
    success:function(ds) {jQuery(rtnid_name).val('');jQuery.each(ds, function(i, entry){jQuery(rtnid_name).val(ds[i].NAME)})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
//***************
//LostFocus 2016-11-18 박종민 주석처리
//function pf_getNAM_CST_SPJS(row, col) {
//var activeSheet = spread.getActiveSheet(0);
//var cstcod = activeSheet.getValue(row, col)+'';
//
//if(cstcod.length != 6) {alert("f"+cstcod);activeSheet.setValue(row, col,"");return;};
//alert("S");
//jQuery.ajax({
//    type:"POST",
//    url:"/_mod/INF_CST.asp",
//    data:{"CODE":cstcod},
//    dataType:"json",
//    success:function(ds) {activeSheet.setValue(row, col+2,"");jQuery.each(ds, function(i, entry){activeSheet.setValue(row, col+2,ds[i].NAME);})},
//    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
//});
//}
//***************
//    pf_GetDAT('P','WRK_DAT','WEEKDAY');
//    pf_GetDAT('M','WRK_DAT','WEEKDAY');
//rtn_code : 리턴받는 코드객체명
//rtn_name : 리턴받는 이름객체명
function pf_GetDAT(varmode,rtnid_date,rtnid_week) {
var tempdate = "";
rtnid_date="#"+rtnid_date;
rtnid_week="#"+rtnid_week;
if((jQuery(rtnid_date).val()).length != 8) {jQuery(rtnid_date).val("");jQuery(rtnid_week).val("");return;};
jQuery.ajax({type:"POST",url:"/_mod/INF_DAT.asp",
    data:{"DATE":jQuery(rtnid_date).val(),"MODE":varmode},dataType:"json",
    success:function(ds) {jQuery.each(ds, function(i, entry){jQuery(rtnid_date).val(ds[i].RTN_DATE);jQuery(rtnid_week).val(ds[i].RTN_WEEK);tempdate= new Date(ds[i].RTN_DATE.substr(0,4)+'/'+ds[i].RTN_DATE.substr(4,2)+'/'+ds[i].RTN_DATE.substr(6,2));jQuery(rtnid_date).datepicker('setDate', tempdate);})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
//***************
//    pf_GetYM('P','WRK_DAT');
//    pf_GetYM('M','WRK_DAT');
//rtn_ym : 리턴받는 코드객체명
function pf_GetYM(varmode,rtn_ym) {
var tempym = "";
rtn_ym="#"+rtn_ym;
if((jQuery(rtn_ym).val()).length != 6) {jQuery(rtn_ym).val("");return;};
jQuery.ajax({type:"POST",url:"/_mod/INF_YM.asp",
    data:{"DATE":jQuery(rtn_ym).val(),"MODE":varmode},dataType:"json",
    success:function(ds) {jQuery.each(ds, function(i, entry){jQuery(rtn_ym).val(ds[i].RTN_YM);tempym= new Date(ds[i].RTN_YM.substr(0,4)+'/'+ds[i].RTN_YM.substr(4,2)+'/01');jQuery(rtn_ym).datepicker({dateFormat:'yyyy/mm'});jQuery(rtn_ym).datepicker('setDate', tempym);})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
//***************
//    pf_GetYEAR('P','WRK_DAT');
//    pf_GetYEAR('M','WRK_DAT');
//rtn_ym : 리턴받는 코드객체명
function pf_GetYEAR(varmode,rtn_ym) {
var tempym = "";
rtn_ym="#"+rtn_ym;
if((jQuery(rtn_ym).val()).length != 4) {jQuery(rtn_ym).val("");return;};
jQuery.ajax({type:"POST",url:"/_mod/INF_YEAR.asp",
    data:{"DATE":jQuery(rtn_ym).val(),"MODE":varmode},dataType:"json",
    success:function(ds) {jQuery.each(ds, function(i, entry){jQuery(rtn_ym).val(ds[i].RTN_YM);tempym= new Date(ds[i].RTN_YM+'/01/01');jQuery(rtn_ym).datepicker({dateFormat:'yyyy'});jQuery(rtn_ym).datepicker('setDate', tempym);})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
//////////***************
//////////가맹점의 본사,물류센터 정보가져오기
////////function pf_GetCSTSUB(varmode,valcode,rtn_cod,rtn_nam) {
////////rtn_cod="#"+rtn_cod;
////////rtn_nam="#"+rtn_nam;
////////if((valcode).length != 6) {jQuery(rtn_cod).val("");jQuery(rtn_nam).val("");return;};
////////jQuery.ajax({type:"POST",url:"/_mod/INF_CST_SUB.asp",
////////    data:{"MODE":varmode,"CODE":valcode},dataType:"json",
////////    success:function(ds) {jQuery.each(ds, function(i, entry){jQuery(rtn_cod).val(ds[i].RTN_CODE);jQuery(rtn_nam).val(ds[i].RTN_NAME);})},
////////    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
////////});
////////}
//***************
//상태바에 메세지전송
function ps_setStatusBar(varColor, varMSG) {vartmp = "<span style='color:"+varColor+";'><strong><small>"+varMSG+"</small></strong></span>";jQuery('#divPanel_Result').html(vartmp);}
//***************
//작업변수선언
var mlngQRY_CNT=0;
var mlngNEW_CNT=0;
var mlngSAV_CNT=0;
var mlngDEL_CNT=0;
//작업(SAV,DEL)처리 진행여부
var mblnProcessING = false;




//***************
//날짜값처리리 빈값처리함수
function fillZeros(n, digits) {
    var zero = '';
    n = n.toString();
    if (n.length < digits) {
        for (i = 0; i < digits - n.length; i++)
            zero += '0';
    }
    return zero + n;
}
//***************
//오늘날짜값 YYYY-MM-DD 23:59:29 형식
function getNowTimeStamp() {
    var d = new Date();
    var s = fillZeros(d.getFullYear(),4)+'-'+fillZeros(d.getMonth()+1,2)+'-'+fillZeros(d.getDate(),2)+' '+fillZeros(d.getHours(),2)+':'+fillZeros(d.getMinutes(),2)+':'+fillZeros(d.getSeconds(),2);
    return s;
}
//현재시각235929
function getNowTimeHHMMSS() {
    var d = new Date();
    var s = fillZeros(d.getHours(),2)+fillZeros(d.getMinutes(),2)+fillZeros(d.getSeconds(),2);
    return s;
}
//***************
//오늘날짜값 YYYYMMDD
function getNowDate() {
    var d = new Date();
    var s = fillZeros(d.getFullYear(),4) +fillZeros(d.getMonth()+1,2)+fillZeros(d.getDate(),2);
    return s;
}
//***************

//***************
//현재년월 YYYYMM
function getNowYYYYMM() {
    var d = new Date();
    var s = fillZeros(d.getFullYear(),4) +fillZeros(d.getMonth()+1,2);
    return s;
}


//+D YYYYMMDD
function getTomorrowDate() {
    var d = new Date();
    d = d.addDays(1);
    var s = fillZeros(d.getFullYear(),4) +fillZeros(d.getMonth()+1,2)+fillZeros(d.getDate(),2);
    return s;
}
Date.prototype.addDays = function(days) {
  var dat = new Date(this.valueOf());
  dat.setDate(dat.getDate() + days);
  return dat;
}
//***************
//오늘날짜값 YYYY-MM-DD
function getNowDateDash() {
    var d = new Date();
    var s = fillZeros(d.getFullYear(),4)+'-'+fillZeros(d.getMonth()+1,2)+'-'+fillZeros(d.getDate(),2);
    return s;
}
//***************
//입력값 대쉬 추가 YYYY-MM-DD
function getDateDash(dtYYYYMMDD) {
    var s = dtYYYYMMDD.slice(0, 4) + '-' + dtYYYYMMDD.slice(4,6) + '-' + dtYYYYMMDD.slice(-2);
    return s;
}
//***************
//오늘날짜값 YYYYMMDD
function getNow() {
    var d = new Date();
    var s = fillZeros(d.getFullYear(),4)+fillZeros(d.getMonth() +1,2)+fillZeros(d.getDate(),2)+fillZeros(d.getHours(),2)+fillZeros(d.getMinutes(),2)+fillZeros(d.getSeconds(),2);
    return s;
}
//***************
//오늘날짜값 YYYY-MM-DD 23:59:29 형식
//yyyymmdd 형식의 날짜값을 입력받아서 유효한 날짜인지 체크한다.
//ex) gf_isDate("20070415");
function gf_isDate(pstrDate) {
    var pstr_Date = pstrDate+"";
    if((pstr_Date).length != 8 ) {return false;}
    var limit_day;
    switch (pstr_Date.substring(4, 6)) {
        case "01": case "03": case "05": case "07": case "08": case "10": case "12": limit_day = 31; break;
        case "02":
            if ((Number(pstr_Date.substring(0, 4)) - 2008) % 4 == 0) limit_day = 29; else limit_day = 28;
            break;
        case "04": case "06": case "09": case "11": limit_day = 30; break;
        default: return false; break;
    }
    if (Number(pstr_Date.substring(6)) > limit_day) { return false }
    if (Number(pstr_Date.substring(6)) < 1) { return false }
    return true;
}
//***************
//replaceAll 함수 (str, 구분자, 대체자)
function replaceAll(str, ori, rep) {
    return str.split(ori).join(rep);
}
//***************
//MAJOR에 맞는 코드 명칭(값) 추출해, arraypstrPC_LIST에 입력
function pf_setCombo_List(pstrMC_MAJOR,callback) {
	var getParameter = {'MAJOR': pstrMC_MAJOR};
	var arrBind = [];
//    callback = null;
	jQuery.ajax({type : 'POST', url : '/_mod/INF_CODE2.asp',
		datatype : 'json',contentType: 'application/x-www-form-urlencoded;charset=utf-8',async : false,
		data : getParameter,
		success: function (data) {arrBind = JSON.parse(data);console.log(JSON.stringify(arrBind));var temp = jQuery.map(arrBind,function(v,i){return keysToLowerCase(v);});callback(temp);},
		error: function (ex) {alert('Exception:' + ex);}
	});
}
//***************
//MAJOR에 원하는 TBL_CODE 필드값) 추출해, arraypstrPC_LIST에 입력
function pf_setCodeArray_List(pstrMC_MAJOR, pstrMC_COL,callback) {
	var getParameter = {'MAJOR': pstrMC_MAJOR, 'COLUMN': pstrMC_COL};
	var arrBind = [];
//    callback = null;
	jQuery.ajax({type : 'POST', url : '/_mod/INF_CODE3.asp',
		datatype : 'json',contentType: 'application/x-www-form-urlencoded;charset=utf-8',async : false,
		data : getParameter,
		success: function (data) {arrBind = JSON.parse(data);var temp = jQuery.map(arrBind,function(v,i){return  v;});callback(temp);},
		error: function (ex) {alert('Exception:' + ex);}
	});
}
//***************
function pf_setCombo_ListManual(pstrMAJOR) {
	var tmpData = '';	// 2개 배열 생성
	var tmpValue = '';
	var tmpCode = '';
   	var arrBind = [];
    arrBind = JSON.parse(pstrMAJOR);
    var temp = jQuery.map(arrBind,function(v,i){return keysToLowerCase(v);});
    return temp;
}
// JSON 키값 소문자 변환
function keysToLowerCase(obj) {
    if (!typeof(obj) === "object" || typeof(obj) === "string" || typeof(obj) === "number" || typeof(obj) === "boolean") {return obj;}
    var keys = Object.keys(obj);
    var n = keys.length;
    var lowKey;
    while (n--) {
        var key = keys[n];
        if (key === (lowKey = key.toLowerCase()))
            continue;
        obj[lowKey] = keysToLowerCase(obj[key]);
        delete obj[key];
    }
    return (obj);
}
//***************
//GC_SPD_PRD_TYP
function pf_setCombo_ppt(pjsonGC_LIST, pstrIdCOD, pstrALLFLG) {
	var ComboHtml = '';
    if(pstrALLFLG == "Y") {ComboHtml += '<option value=\''+'\' selected>(전체)</option>';}
    jQuery.each(pjsonGC_LIST, function(i){
        ComboHtml += '<option value='+pjsonGC_LIST[i].value+'>'+pjsonGC_LIST[i].text+'</option>';
    });
    jQuery("#"+ pstrIdCOD).html(ComboHtml);
}
//***************
function pf_setCombo(pstrMAJOR, pstrIdCOD, pstrALLFLG) {
	var getParameter = {'MAJOR': pstrMAJOR};
	var arrBind = [];
	var ComboHtml = '';
	jQuery.ajax({type : 'POST',url : '/_mod/INF_CODE.asp',
		datatype : 'json',contentType: 'application/x-www-form-urlencoded;charset=utf-8',
		data : getParameter,
		success: function (data) {
				arrBind = JSON.parse(data);
				if(pstrALLFLG == "Y") {ComboHtml += '<option value=\'\' selected>(전체)</option>';}
				jQuery.each(arrBind, function(i){ComboHtml += '<option value='+arrBind[i].KEYCODE+'>'+arrBind[i].CODE_DESC+'</option>';});
				jQuery("#"+ pstrIdCOD).html(ComboHtml);
		},
		error: function (ex) {alert('Exception:' + ex);}
	});
}
//***************
function pf_setComboManual(pstrMAJOR, pstrIdCOD, pstrALLFLG) {
	var tmpData = '';	// 2개 배열 생성
	var tmpValue = '';
	var tmpCode = '';
	var ComboHtml = '';
	tmpData = pstrMAJOR.split('||');
	tmpValue = tmpData[0].split(',');
	tmpCode = tmpData[1].split(',');
	if(pstrALLFLG == "Y") {ComboHtml += '<option value=\'\' selected>(전체)</option>';}
	for (var i=0; i<tmpValue.length; i++) {ComboHtml += '<option value='+tmpValue[i]+'>'+tmpCode[i]+'</option>';}
	jQuery("#"+ pstrIdCOD).html(ComboHtml);
}

//***************
// * Module for displaying "Waiting for..." dialog using Bootstrap
// * @author Eugene Maslovich <ehpc@em42.ru>
//***************
var waitingDialog = waitingDialog || (function (jQuery) {
    'use strict';
	// Creating modal dialog's DOM
	var jQuerydialog = jQuery('<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
                    '<div class="modal-dialog modal-m"><div class="modal-content"><div class="modal-header"><h3 style="margin:0;"></h3></div><div class="modal-body"><div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
                    '</div></div></div></div>');
	return {
		/**
		 * Opens our dialog
		 * @param message Custom message
		 * @param options Custom options:
		 * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
		 * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
		 */
		show: function (message, options) {
			// Assigning defaults
			if (typeof options === 'undefined') {options = {};}
			if (typeof message === 'undefined') {message = 'Loading';}
			var settings = jQuery.extend({
				dialogSize: 'm',
				progressType: '',
				onHide: null // This callback runs after the dialog was hidden
			}, options);
			// Configuring dialog
			jQuerydialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
			jQuerydialog.find('.progress-bar').attr('class', 'progress-bar');
			if (settings.progressType) {jQuerydialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);}
			jQuerydialog.find('h3').text(message);
			// Adding callbacks
			if (typeof settings.onHide === 'function') {jQuerydialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {settings.onHide.call(jQuerydialog);});}
			// Opening dialog
			jQuerydialog.modal();
		},
		// Closes dialog
		hide: function () {jQuerydialog.modal('hide');}
	};
})(jQuery);
////***************
////UTF8, ANSI변환
//UTF8 = {
//    encode: function(s){
//        for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
//            s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
//        );
//        return s.join("");
//    },
//    decode: function(s){
//        for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
//            ((a = s[i][c](0)) & 0x80) &&
//            (s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
//            o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
//        );
//        return s.join("");
//    }
//};
function reposition() {
    var modal = jQuery(this),
    dialog = modal.find('.modal-dialog');
    modal.css('display', 'block');
    // Dividing by two centers the modal exactly, but dividing by three
    // or four works better for larger screens.
    dialog.css("margin-top", Math.max(0, (jQuery(window).height() - dialog.height()) / 2));
}
//***************
//2016-10-18
//SHEET 정의
function ps_setSpreadSheet_init(pobjSheet, blnTabsNameView) {
	
	console.log("Hear!!!!!");
	
	//SHEET 속성
    pobjSheet.options.allowContextMenu     = false;
    pobjSheet.options.allowSheetReorder    = false;
    pobjSheet.options.allowUserDragFill    = false;
    pobjSheet.options.allowUserDragDrop    = false;
    pobjSheet.options.allowUserZoom        = false;
    pobjSheet.options.canUserDragDrop      = false;
    pobjSheet.options.canUserDragFill      = false;
    pobjSheet.options.newTabVisible        = false;
    pobjSheet.options.scrollbarMaxAlign    = true;
    pobjSheet.options.scrollbarShowMax     = true;
    pobjSheet.options.tabEditable          = false;
    pobjSheet.options.tabNavigationVisible = false;
    pobjSheet.options.tabStripVisible      = blnTabsNameView;
    pobjSheet.options.scrollIgnoreHidden   = true;
    // DEL키 입력방지
    pobjSheet.commandManager().register('mydelete',function(spread1){
    	pobjSheet.suspendEvent();
    	pobjSheet.resumeEvent();
	});
    pobjSheet.commandManager().setShortcutKey(null,GC.Spread.Commands.Key.del, false, false, false, false);
    pobjSheet.commandManager().setShortcutKey('mydelete',GC.Spread.Commands.Key.del, false, false, false, false);

    // 셀값 변경 이벤트 (에디트 수정 시에만)
    pobjSheet.getActiveSheet().bind(GC.Spread.Sheets.Events.ValueChanged, function (sender, args) {

		//** 2017-03-08
		//** 1) 공통함수 spdList 체크박스 클릭 시 이벤트
		//**    체크on 일 경우 체크 된 row 수 만큼 arrMC_SPD_CHK_SET[] 저장
		//**    활용 : arrMC_SPD_CHK_SET[] 이 0일 경우 체크 된 row 없음 alert
		var sheet = args.sheet;
		if (args.col === 0 && args.newValue != args.oldValue) {
			// 체크박스 일 경우. (col_No == 0)
			switch(args.newValue){
				case true: //chk on
					ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
					break;
				case false: //chk off
					ps_spdListChkSetup(args.row, "out"); //arrMC_SPD_CHK_SET seq삭제
					break;
			}
		} else if (args.col != 0 && args.newValue != args.oldValue) {
			// 체크박스가 아닐 경우. (col_No > 0)
			sheet.setValue(args.row, 0, 1, GC.Spread.Sheets.SheetArea.viewport);
			ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
		}
    });

	// 셀변경 이벤트
	pobjSheet.getActiveSheet().bind(GC.Spread.Sheets.Events.CellChanged, function (sender, args) {
		//** 1) 신규버튼 클릭 시 arrMC_SPD_CHK_SET[] 에 카운트 추가
		var sheet = args.sheet;
		if(args.col === 0 && sheet.getValue(args.row, args.col) === true && args.propertyName === "value"){
			//체크박스가 0,0에 있고 체크박스가 ture 값일 경우!!
			switch(args.newValue){
				case true: //chk on
					ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
					break;
				case false: //chk off
					ps_spdListChkSetup(args.row, "out"); //arrMC_SPD_CHK_SET seq삭제
					break;
			}
		}
		//** 2) 화면 단(ui) user함수 정의 call
        if (args.propertyName === "value") {
            ps_setSpreadEvents_CellChanged(args);
        }
    });

};
//***************
//2016-10-18
//spdList 정의
function ps_setSpreadList_init(pobjspdList, plngTitleRowCnt, plngTitleColCnt, plngFirstShowRowCnt, plngFrozenColumn) {
    
	console.log("ps_setSpreadList_init");
	
	//*****ver12
    pobjspdList.suspendPaint();
    //pobjspdList 타이틀 행수
    pobjspdList.setRowCount(plngTitleRowCnt, GC.Spread.Sheets.SheetArea.colHeader);
    //pobjspdList 타이틀 컬럼수
    pobjspdList.setColumnCount(plngTitleColCnt, GC.Spread.Sheets.SheetArea.viewport);
    //pobjspdList 최초 보이는 행수
    pobjspdList.setRowCount(plngFirstShowRowCnt, GC.Spread.Sheets.SheetArea.viewport);

    //*****ver12
    //pobjspdList 전체보호모드로 적용하겠다.(true)
    //pobjspdList.options.setIsProtected = false;
    pobjspdList.options.isProtected = true;
    //pobjspdList 행,컬럼 사이즈 사용자 변경
    pobjspdList.options.protectionOptions.allowResizeRows  = false;
    pobjspdList.options.protectionOptions.allowResizeColumns  = true;
    pobjspdList.options.allowCellOverflow  = false;
    pobjspdList.options.protectionOptions.allowFilter = true;
	pobjspdList.options.protectionOptions.allowSort = true;

    //pobjspdList 고정틀_컬럼
    if (plngFrozenColumn != null){pobjspdList.frozenColumnCount(plngFrozenColumn+1);};

    //기본높이
    pobjspdList.defaults.colHeaderRowHeight = PUBLIC_SPD_HEIGHT_TITLE;
    pobjspdList.defaults.rowHeight = PUBLIC_SPD_HEIGHT;

    //*****vver12
    //pobjspdList 화면의 속성을 더이상 수정하지 않겠다.(false)
    pobjspdList.resumePaint();

    //sheet style setting 시트 폰트 세팅
    var defaultStyle = new GC.Spread.Sheets.Style();
    defaultStyle.font = "13px Arial";
    //defaultStyle.
    defaultStyle.hAlign = GC.Spread.Sheets.HorizontalAlign.center;

    pobjspdList.setDefaultStyle(defaultStyle, GC.Spread.Sheets.SheetArea.viewport);
    pobjspdList.setDefaultStyle(defaultStyle, GC.Spread.Sheets.SheetArea.colHeader);
    pobjspdList.setDefaultStyle(defaultStyle, GC.Spread.Sheets.SheetArea.rowHeader);

};
//***************
//2022-01-21
//spdList 정의
//폰트 사이즈 변경
function ps_setSpreadList_init_custom1(pobjspdList, plngTitleRowCnt, plngTitleColCnt, plngFirstShowRowCnt, plngFrozenColumn) {
	
	console.log("ps_setSpreadList_init");
	
	//*****ver12
	pobjspdList.suspendPaint();
	//pobjspdList 타이틀 행수
	pobjspdList.setRowCount(plngTitleRowCnt, GC.Spread.Sheets.SheetArea.colHeader);
	//pobjspdList 타이틀 컬럼수
	pobjspdList.setColumnCount(plngTitleColCnt, GC.Spread.Sheets.SheetArea.viewport);
	//pobjspdList 최초 보이는 행수
	pobjspdList.setRowCount(plngFirstShowRowCnt, GC.Spread.Sheets.SheetArea.viewport);
	
	//*****ver12
	//pobjspdList 전체보호모드로 적용하겠다.(true)
	//pobjspdList.options.setIsProtected = false;
	pobjspdList.options.isProtected = true;
	//pobjspdList 행,컬럼 사이즈 사용자 변경
	pobjspdList.options.protectionOptions.allowResizeRows  = false;
	pobjspdList.options.protectionOptions.allowResizeColumns  = true;
	pobjspdList.options.allowCellOverflow  = false;
	pobjspdList.options.protectionOptions.allowFilter = true;
	pobjspdList.options.protectionOptions.allowSort = true;
	
	//pobjspdList 고정틀_컬럼
	if (plngFrozenColumn != null){pobjspdList.frozenColumnCount(plngFrozenColumn+1);};
	
	//기본높이
	pobjspdList.defaults.colHeaderRowHeight = PUBLIC_SPD_HEIGHT_TITLE;
	pobjspdList.defaults.rowHeight = PUBLIC_SPD_HEIGHT;
	
	//*****vver12
	//pobjspdList 화면의 속성을 더이상 수정하지 않겠다.(false)
	pobjspdList.resumePaint();
	
	//sheet style setting 시트 폰트 세팅
	var defaultStyle = new GC.Spread.Sheets.Style();
	defaultStyle.font = "12px Arial";
	//defaultStyle.
	defaultStyle.hAlign = GC.Spread.Sheets.HorizontalAlign.center;
	
	pobjspdList.setDefaultStyle(defaultStyle, GC.Spread.Sheets.SheetArea.viewport);
	pobjspdList.setDefaultStyle(defaultStyle, GC.Spread.Sheets.SheetArea.colHeader);
	pobjspdList.setDefaultStyle(defaultStyle, GC.Spread.Sheets.SheetArea.rowHeader);
	
};
//***************
//2016-10-18
//Events Bind
function ps_setSpreadEvents_Bind(pobjspdList) {
    //Events : ButtonCliecked
    spdSheet.bind(GC.Spread.Sheets.Events.ButtonClicked, function (sender, args) {
        ps_setSpreadEvents_ButtonClicked(args);
    });
    //Events : ValueChanged
    pobjspdList.bind(GC.Spread.Sheets.Events.ValueChanged, function (sender, args) {
        ps_setSpreadEvents_ValueChanged(args);
    });
    //Events : CellChanged
    pobjspdList.bind(GC.Spread.Sheets.Events.CellChanged, function (sender, args) {
        ps_setSpreadEvents_CellChanged(args);
    });
    //Events : CellClick
    pobjspdList.bind(GC.Spread.Sheets.Events.CellClick, function (sender, args) {
        ps_setSpreadEvents_CellClick(args);
    });
//    //Events : CellChanged(EditEnded)
//    pobjspdList.bind(GcSpread.Sheets.Events.EditEnded, function (sender, args) {
//        ps_setSpreadEvents_CellChanged(args.row, args.col);
//    });
    //Events : CellDoubleClick
    pobjspdList.bind(GC.Spread.Sheets.Events.CellDoubleClick, function (sender, args) {
        ps_setSpreadEvents_CellDoubleClick(args);
    });
    //Events : EditEnded
    pobjspdList.bind(GC.Spread.Sheets.Events.EditEnded, function (sender, args) {
        ps_setSpreadEvents_LostFocus(args);
    });
    //Events : LeaveCell
    pobjspdList.bind(GC.Spread.Sheets.Events.LeaveCell, function (sender, args) {
        ps_setSpreadEvents_LostFocus(args);
    });
    //Events : ClipboardPasted
    pobjspdList.bind(GC.Spread.Sheets.Events.ClipboardPasted, function (sender, args) {
        ps_setSpreadEvents_Pasted(args);
    });
//************
//error사용안함
//    //Events : CellChanged(EditEnded)
//    pobjspdList.bind(GcSpread.Sheets.Events.EditChange, function (sender, args) {
//        ps_setSpreadEvents_CellEdit(args.row, args.col);
//    });
}
//***************
//2016-10-18
//HEADER 타이틀정의
function ps_setSpreadTitle_Named(pobjspdList, plngSPDROW, plngSPDCOL, pstrTITLE_NAM, plgColWidth, pblnHide) {
	if(plngSPDCOL == 0){
		pobjspdList.setRowHeight(0, 35.0,GC.Spread.Sheets.SheetArea.colHeader);
		var colRange = pobjspdList.getRange(0, -1, 1, -1, GC.Spread.Sheets.SheetArea.colHeader);
		colRange.backColor("#DCDCDC");
	}
    if (pstrTITLE_NAM == "선택") {
        pobjspdList.setCellType(0,0, new HeaderCheckBoxCellType(), GC.Spread.Sheets.SheetArea.colHeader);
        pobjspdList.setValue(0,0,0, GC.Spread.Sheets.SheetArea.colHeader);

        pobjspdList.setColumnWidth(plngSPDCOL, plgColWidth,GC.Spread.Sheets.SheetArea.viewport);
        pobjspdList.setColumnVisible(plngSPDCOL,pblnHide,GC.Spread.Sheets.SheetArea.viewport);
		
		//colRange.foreColor("Black");
		//pobjspdList.getCell(1, -1, GC.Spread.Sheets.SheetArea.rowHeader).backColor("Yellow");
    } else {
        pobjspdList.setValue(plngSPDROW, plngSPDCOL, pstrTITLE_NAM, GC.Spread.Sheets.SheetArea.colHeader);
        pobjspdList.setColumnWidth(plngSPDCOL, plgColWidth,GC.Spread.Sheets.SheetArea.viewport);
        pobjspdList.setColumnVisible(plngSPDCOL,pblnHide,GC.Spread.Sheets.SheetArea.viewport);
  }
}
//***************
//2019-10-23
//HEADER 타이틀 히든
function ps_setSpreadTitle_Hide(pobjspdList, plngStartCol, plngEndCol, plngRowCount) {
    for (var lngCol=plngStartCol; lngCol<plngEndCol; lngCol++) {
        pobjspdList.setColumnVisible(lngCol,false,GC.Spread.Sheets.SheetArea.viewport);
        //ps_setSpreadTitle_Named(spdList, 0, lngCol, '', 80, false);
        //ps_setSpreadCol_TEXT_Property(spdList, lngCol, 'CENTER', 'LEFT', 8, true, true, "");
    }
//    console.log(plngStartCol+":"+plngEndCol);
//    var colRange = pobjspdList.getRange(0, plngStartCol, plngRowCount, plngEndCol - plngStartCol, GC.Spread.Sheets.SheetArea.colHeader);
//    //var colRange = new GC.Spread.Sheet.CellRange(0, plngStartCol, plngRowCount, plngEndCol - plngStartCol, GC.Spread.Sheets.SheetArea.colHeader);
//    colRange.backColor("Red");
//    pobjspdList.showColumnOutline(false);
}

///////////////////////
// Define checkbox cell type
function HeaderCheckBoxCellType() {
    var cellType = new GC.Spread.Sheets.CellTypes.CheckBox();
    cellType.isThreeState(false);
    return cellType;
}
////////////////////////
//***************
//***************
//2016-10-18
//FreezeRowCount
//FreezeColumnCount
//FreezeLine Color

//***************
//2016-10-18
//컬럼 속성 : 초기화(빈칸으로 표기됨)
function ps_setSpread_Init(pobjspdList, plngSPDROW, plngSPDCOL) {
    pobjspdList.setCellType(plngSPDROW, plngSPDCOL, '');
}
//***************
//2016-10-18
//컬럼 속성 : TEXT
function ps_setSpreadCol_TEXT_Property(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngFormatter, pblnwordWrap, pblnLock, pstrBgColor) {
    var lngtmpValign = 1;       //default : Center
    var lngtmpHalign = 0;       //default : Left
    if (pstrValign == 'TOP') {lngtmpValign = 0;};
    if (pstrValign == 'CENTER') {lngtmpValign = 1;};
    if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
    if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
    if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
    if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
    //pobjspdList 컬럼 수직,수평 Align 설정
    var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);

    cell.vAlign(lngtmpValign);
    cell.hAlign(lngtmpHalign);

    if (pstrBgColor != "") {
    	cell.backColor(pstrBgColor);
    }
    cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    //최대길이 plngMaxLength
    //대소문자
    cell.wordWrap(pblnwordWrap);
    cell.locked(pblnLock);

    //if (pblnLock){
  	//  cell.backColor(BKC_LCK);
    //}

    var g = (function(f){return (f!=undefined)?f:"@";});
    cell.formatter(g(plngFormatter));
    pobjspdList.options.isProtected = true;
}

//***************
//2018-11-29
//컬럼 속성 : star custom cell
function ps_setSpreadCol_STAR_Property(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngMaxLength, pblnwordWrap, pblnLock, pstrBgColor) {
    var lngtmpValign = 1;       //default : Center
    var lngtmpHalign = 0;       //default : Left
    if (pstrValign == 'TOP') {lngtmpValign = 0;};
    if (pstrValign == 'CENTER') {lngtmpValign = 1;};
    if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
    if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
    if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
    if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};

    var CellType_Star = new FivePointedStarCellType();

    var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
    cell.vAlign(lngtmpValign);
    cell.hAlign(lngtmpHalign);

    //pobjspdList 컬럼 수직,수평 Align 설정
//    pobjspdList.getColumn(plngSPDCOL).vAlign(lngtmpValign);
//    pobjspdList.getColumn(plngSPDCOL).hAlign(lngtmpHalign);
    pobjspdList.getRange(-1, plngSPDCOL, -1, 1).vAlign(lngtmpValign);
    pobjspdList.getRange(-1, plngSPDCOL, -1, 1).hAlign(lngtmpHalign);

    if (pstrBgColor != "") {
    	cell.backColor(pstrBgColor);
    	cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	    cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	    cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	    cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    };

    pobjspdList.setCellType(-1, plngSPDCOL, CellType_Star, GC.Spread.Sheets.SheetArea.viewport);

}


//***************
//2016-10-18
//컬럼 속성 : TEXT
//function ps_setSpreadCol_TEXT_Property2(pobjspdList, plngSPDCOL, plgColWidth, pstrValign, pstrHalign, plngMaxLength, pblnwordWrap, pblnLock, pstrBgColor, pblnHide) {
//    var lngtmpValign = 1;       //default : Center
//    var lngtmpHalign = 0;       //default : Left
//    if (pstrValign == 'TOP') {lngtmpValign = 0;};
//    if (pstrValign == 'CENTER') {lngtmpValign = 1;};
//    if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
//    if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
//    if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
//    if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
//    //pobjspdList 컬럼 수직,수평 Align 설정
//    pobjspdList.getColumn(plngSPDCOL).vAlign(lngtmpValign);
//    pobjspdList.getColumn(plngSPDCOL).hAlign(lngtmpHalign);
//    if (pstrBgColor != "") {
//    pobjspdList.getColumn(plngSPDCOL).backColor(pstrBgColor);
//    pobjspdList.getColumn(plngSPDCOL).borderRight(new GcSpread.Sheets.LineBorder(DFC_LINE,GcSpread.Sheets.LineStyle.thin));
//    pobjspdList.getColumn(plngSPDCOL).borderLeft(new GcSpread.Sheets.LineBorder(DFC_LINE,GcSpread.Sheets.LineStyle.thin));
//    pobjspdList.getColumn(plngSPDCOL).borderTop(new GcSpread.Sheets.LineBorder(DFC_LINE,GcSpread.Sheets.LineStyle.thin));
//    pobjspdList.getColumn(plngSPDCOL).borderBottom(new GcSpread.Sheets.LineBorder(DFC_LINE,GcSpread.Sheets.LineStyle.thin));
//    };
//    //최대길이 plngMaxLength
//    //대소문자
//    pobjspdList.getColumn(plngSPDCOL).wordWrap(pblnwordWrap);
//	pobjspdList.getColumn(plngSPDCOL).locked(pblnLock);
//	pobjspdList.getColumn(plngSPDCOL).width(plgColWidth);
//	pobjspdList.getColumn(plngSPDCOL).visible(pblnHide);
//    pobjspdList.getColumn(plngSPDCOL).formatter("@");
//}
//***************
//2016-10-18
//컬럼 속성 : NUMBER
function ps_setSpreadCol_NUMBER_Property(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngFormatter, pblnwordWrap, pblnLock, pstrBgColor) {
    var lngtmpValign = 1;       //default : Center
    var lngtmpHalign = 0;       //default : Left
    if (pstrValign == 'TOP') {lngtmpValign = 0;};
    if (pstrValign == 'CENTER') {lngtmpValign = 1;};
    if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
    if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
    if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
    if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
    //pobjspdList 컬럼 수직,수평 Align 설정
    var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
    cell.vAlign(lngtmpValign);
    
    if (pstrBgColor != "") {
    	cell.backColor(pstrBgColor);
    	cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    };
    cell.hAlign(GC.Spread.Sheets.HorizontalAlign.right);    //fix : 오른쪽정렬
    cell.wordWrap(pblnwordWrap);
    cell.locked(pblnLock);

    //if (pblnLock){
  	//  cell.backColor(BKC_LCK);
    //}

    if (plngFormatter.toString().length < 3)
    {
        plngFormatter = "###,##0";
    }

    //숫자형태정의
    //자리수 유지.000,000.00 (1234.5 > 001,234.50)
    //자동 변동.###,###.## (1234.5 > 1,234.5)
    //pobjspdList.getRange(-1,plngSPDCOL,-1,1).formatter(new GC.Spread.Sheets.GeneralFormatter("###,###.##", GC.Spread.Sheets.FormatMode.StandardNumericMode));

    var g = (function(f){return (f!=undefined)?f:"###,###";});
    cell.formatter(g(plngFormatter));
    pobjspdList.options.isProtected = true;
}




//2021-07-28
//컬럼 속성 : NUMBER  yhku
function ps_setSpreadCol_NUMBER_Property2(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngFormatter, pblnwordWrap, pblnLock, pstrBgColor) {
	  var lngtmpValign = 1;       //default : Center
	  var lngtmpHalign = 0;       //default : Left
	  if (pstrValign == 'TOP') {lngtmpValign = 0;};
	  if (pstrValign == 'CENTER') {lngtmpValign = 1;};
	  if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
	  if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
	  if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
	  if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
	  //pobjspdList 컬럼 수직,수평 Align 설정
	  var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
	  cell.vAlign(lngtmpValign);
	  if (pstrBgColor != "") {
	  	cell.backColor(pstrBgColor);
	  	cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	  	cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	  	cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	  	cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	  };
	  cell.hAlign(GC.Spread.Sheets.HorizontalAlign.right);    //fix : 오른쪽정렬
	  cell.wordWrap(pblnwordWrap);
	  cell.locked(pblnLock);

	  //if (pblnLock){
		//  cell.backColor(BKC_LCK);
	  //}

	/*  if (plngFormatter.toString().length < 3)
	  {
	      plngFormatter = "###,##0";
	  }

	  //숫자형태정의
	  //자리수 유지.000,000.00 (1234.5 > 001,234.50)
	  //자동 변동.###,###.## (1234.5 > 1,234.5)
	  //pobjspdList.getRange(-1,plngSPDCOL,-1,1).formatter(new GC.Spread.Sheets.GeneralFormatter("###,###.##", GC.Spread.Sheets.FormatMode.StandardNumericMode));

	  var g = (function(f){return (f!=undefined)?f:"###,###";});
	  cell.formatter(g(plngFormatter));
	  pobjspdList.options.isProtected = true;*/
	  
	  /*추가*/
 	  var customFormatterTest = {};
 	  customFormatterTest.prototype = GC.Spread.Formatter.FormatterBase;
 	  customFormatterTest.format = function (obj, formatterData) {
	  
 		 return number_format(obj, 2, ".", ",");
 		  
 	  };
 	  customFormatterTest.parse = function (str) {
 	      if (!str) {
 	          return "";
 	      }else{
 	    	 if(isNaN(str) ){//문자
 	    		 return "";
 	    	 }else{//숫자
 	    	 }
 	      }
 	      return str;
 	  };
  
 	  cell.formatter(customFormatterTest);
 	  pobjspdList.options.isProtected = true;
	  
}


//2021-07-28
//컬럼 속성 : NUMBER plngFormatter 사용 x, 천단위, 소수점 사용, hAlign 적용(non-fix)
function ps_setSpreadCol_NUMBER_Property4(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngFormatter, pblnwordWrap, pblnLock, pstrBgColor, decPoint) {
    var lngtmpValign = 1;       //default : Center
    var lngtmpHalign = 0;       //default : Left
    if (pstrValign == 'TOP') {lngtmpValign = 0;};
    if (pstrValign == 'CENTER') {lngtmpValign = 1;};
    if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
    if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
    if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
    if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
    //pobjspdList 컬럼 수직,수평 Align 설정
    var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
    cell.vAlign(lngtmpValign);
    cell.hAlign(lngtmpHalign);
    if (pstrBgColor != "") {
        cell.backColor(pstrBgColor);
        cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
        cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
        cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
        cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    };
    cell.wordWrap(pblnwordWrap);
    cell.locked(pblnLock);

  //if (pblnLock){
      //  cell.backColor(BKC_LCK);
    //}

  /*  if (plngFormatter.toString().length < 3)
    {
        plngFormatter = "###,##0";
    }

    //숫자형태정의
    //자리수 유지.000,000.00 (1234.5 > 001,234.50)
    //자동 변동.###,###.## (1234.5 > 1,234.5)
    //pobjspdList.getRange(-1,plngSPDCOL,-1,1).formatter(new GC.Spread.Sheets.GeneralFormatter("###,###.##", GC.Spread.Sheets.FormatMode.StandardNumericMode));

    var g = (function(f){return (f!=undefined)?f:"###,###";});
    cell.formatter(g(plngFormatter));
    pobjspdList.options.isProtected = true;*/
    
    /*추가*/
    var customFormatterTest = {};
    customFormatterTest.prototype = GC.Spread.Formatter.FormatterBase;
    customFormatterTest.format = function (obj, formatterData) {
      if(typeof decPoint != 'undefined' && decPoint != null && decPoint != ''){
          return number_format(obj, decPoint, ".", ",");
      }
      return number_format(obj, 2, ".", ",");
        
    };
    customFormatterTest.parse = function (str) {
        if (!str) {
            return "";
        }else{
           if(isNaN(str) ){//문자
               return "";
           }else{//숫자
           }
        }
        return Number(str);
    };

    cell.formatter(customFormatterTest);
    pobjspdList.options.isProtected = true;
    
}

function number_format(number, decimals, dec_point, thousands_sep, isRoundUp) {
    /*
         * Parameter Description: 
         * Number: the number to be formatted
         * Decimals: to retain several decimal places
         * Dec_point: decimal notation
         * Thousands_sep: thousands separator symbol
         * IsRoundUp: whether rounding
    * */
    number = (number + '').replace(/[^0-9+-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
                         // whether rounding
            if(isRoundUp){
                return '' + Math.round(n * k) / k;
            }
                         // floor realized not only carry rounding logic
            return '' + Math.floor(n * k) / k;
        };
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    var re = /(-?\d+)(\d{3})/;
    while (re.test(s[0])) {
        s[0] = s[0].replace(re, "$1" + sep + "$2");
    }
 
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
    }
         // When the fractional part is empty, no decimal point
    if(s[1] === ""){
        return s.join("");
    }
    return s.join(dec);
}
//***************
//2016-10-18
//컬럼 속성 : BUTTON
function ps_setSpreadCol_BUTTON_Property(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngMaxLength, pblnwordWrap, pblnLock, pstrBgColor, pstrMARK) {
    var lngtmpValign = 1;       //default : Center
    var lngtmpHalign = 0;       //default : Left
    if (pstrValign == 'TOP') {lngtmpValign = 0;};
    if (pstrValign == 'CENTER') {lngtmpValign = 1;};
    if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
    if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
    if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
    if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
    //BUTTON
    var CellType_BUTTON = new GC.Spread.Sheets.CellTypes.Button();
    CellType_BUTTON.text('▲');
    if (gf_Null(pstrMARK,"") != "") {CellType_BUTTON.text(pstrMARK);}
    CellType_BUTTON.marginLeft(0);
    CellType_BUTTON.marginTop(0);
    CellType_BUTTON.marginRight(0);
    CellType_BUTTON.marginBottom(0);
//    CellType_BUTTON.buttonBackColor('#F1F1F1');
    //pobjspdList 컬럼 수직,수평 Align 설정

    var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
    cell.vAlign(lngtmpValign);
    cell.hAlign(lngtmpHalign);
    if (pstrBgColor != "") {
    	cell.backColor(pstrBgColor);
    	cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	    cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	    cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
	    cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    };
    cell.wordWrap(pblnwordWrap);
    cell.locked(pblnLock);
    //celltype 속성정의
    pobjspdList.setCellType(-1, plngSPDCOL, CellType_BUTTON, GC.Spread.Sheets.SheetArea.viewport);
    //pobjspdList.options.isProtected = true;
}
//***************
//2016-10-18
//컬럼 속성 : CHECKBOX
function ps_setSpreadCol_CHECKBOX_Property(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngMaxLength, pblnwordWrap, pblnLock, pstrBgColor) {
    var lngtmpValign = 1;       //default : Center
    var lngtmpHalign = 0;       //default : Left
    if (pstrValign == 'TOP') {lngtmpValign = 0;};
    if (pstrValign == 'CENTER') {lngtmpValign = 1;};
    if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
    if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
    if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
    if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
    //CHECKBOX
    var CellType_CHECKBOX = new GC.Spread.Sheets.CellTypes.CheckBox();


    var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
    //pobjspdList 컬럼 수직,수평 Align 설정
    cell.vAlign(lngtmpValign);
    cell.hAlign(lngtmpHalign);
    if (pstrBgColor != "") {
    	cell.backColor(pstrBgColor);
    	cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    };
    cell.wordWrap(pblnwordWrap);
    cell.locked(pblnLock);
    //celltype 속성정의
    pobjspdList.setCellType(-1, plngSPDCOL, CellType_CHECKBOX, GC.Spread.Sheets.SheetArea.viewport);
    pobjspdList.options.isProtected = true;
}
//***************
//2016-10-18
//컬럼 속성 : COMBO
function ps_setSpreadCol_COMBO_Property(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, pobjCOMBO_TYP, pblnLock, pstrBgColor) {
    var lngtmpValign = 1;       //default : Center
    var lngtmpHalign = 0;       //default : Left
    if (pstrValign == 'TOP') {lngtmpValign = 0;};
    if (pstrValign == 'CENTER') {lngtmpValign = 1;};
    if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
    if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
    if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
    if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
    //CHECKBOX
    var CellType_COMBO = new GC.Spread.Sheets.CellTypes.ComboBox();
    var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
    //pobjspdList 컬럼 수직,수평 Align 설정
    cell.vAlign(lngtmpValign);
    cell.hAlign(lngtmpHalign);
    if (pstrBgColor != "") {
    	cell.backColor(pstrBgColor);
    	cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    	cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
    };
    cell.locked(pblnLock);
    //celltype 속성정의
    cell.cellType(CellType_COMBO);
	//CellType_COMBO.items(pobjCOMBO_TYP).editorValueType(GC.Spread.Sheets.EditorValueType.Value);
    CellType_COMBO.items(pobjCOMBO_TYP);//.editorValueType(GC.Spread.Sheets.CellTypes.EditorValueType.value);
    pobjspdList.options.isProtected = true;
}
//////////***************
//////////행높이정의
//////////사용안함
////////function ps_setSpreadRowHeight(pobjspdList) {
////////    for (i=0; i<pobjspdList.getRowCount(); i++) {
////////        pobjspdList.setRowHeight(i, MC_SPD_HEIGHT);     //기본값MC_SPD_HEIGHT:25
////////    };
////////}
//////////***************
//////////2016-10-19
//////////SPREAD-TOP BORDER COLOR
////////function ps_setSpreadTopLine(pobjspdList, plngSPDROW, pdefLineColor) {
////////    pobjspdList.getRow(plngSPDROW).borderTop(pdefLineColor);
////////}
//***************
//2016-10-19
//COLUMN기준값을 비교해서, 값이 다르면, 라인표시
function ps_setSpreadBorderLineDraw_COLUMN(pobjspdList, plngSPDCOL, pdefLineColor) {
for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") != gf_Null(pobjspdList.getValue(lngRow+1, plngSPDCOL),"")) {
        pobjspdList.getRange(lngRow, -1, 1, -1).borderBottom(new GC.Spread.Sheets.LineBorder(pdefLineColor, GC.Spread.Sheets.LineStyle.thin));
    };
};
}
//////////***************
//////////2016-10-19
//////////Value값을 비교해서, 같으면, 라인표시
////////function ps_setSpreadBorderLineDraw_VALUE(pobjspdList, plngSPDCOL, pstrValue, pdefLineColor) {
////////for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
////////    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue) {
////////        pobjspdList.getRow(lngRow).borderTop(pdefLineColor);
////////    };
////////};
////////}
//////////***************
//////////2017-02-20
//////////2개 컬럼 Value값을 비교해서, 다르면, 폰트색상 표시
////////function ps_setSpreadFontColor_VALUE(pobjspdList, plngSPDCOL1, plngSPDCOL2, pdefColor) {
////////for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
////////    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL1), "") != gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL2), "")) {
////////        ps_setSpreadForeColor_Cell(pobjspdList, lngRow, plngSPDCOL1, pdefColor);
////////        ps_setSpreadForeColor_Cell(pobjspdList, lngRow, plngSPDCOL2, pdefColor);
////////    };
////////};
////////}
//***************
//***************
//2016-10-19
//Value값을 비교해서, 같으면, 배경색
//아래함수 명칭으로 변경(ps_setSpreadBackColor_VALUE)
////////function ps_setSpreadBorderBGColor_VALUE(pobjspdList, plngSPDCOL, pstrValue, pdefLineColor) {
//////////사용안함
////////for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
////////    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue) {
////////        pobjspdList.getRow(lngRow).backColor(pdefLineColor);
////////    };
////////};
////////}
////////function ps_setSpreadBackColor_VALUE(pobjspdList, plngSPDCOL, pstrValue, pstrBGColor) {
////////for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
////////    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue) {
////////        ps_setSpreadBackColor_Cell(pobjspdList, lngRow, plngSPDCOL, pstrBGColor);
////////    };
////////};
////////}
//Value값을 비교해서, 같으면, 글자색
function ps_setSpreadForeColor_VALUE(pobjspdList, plngSPDCOL, pstrValue1, pstrBGColor1, pstrValue2, pstrBGColor2, pstrValue3, pstrBGColor3, pstrValue4, pstrBGColor4, pstrValue5, pstrBGColor5) {
for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue1) {
        ps_setSpreadForeColor_Cell(pobjspdList, lngRow, plngSPDCOL, pstrBGColor1);
    };
    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue2) {
        ps_setSpreadForeColor_Cell(pobjspdList, lngRow, plngSPDCOL, pstrBGColor2);
    };
    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue3) {
        ps_setSpreadForeColor_Cell(pobjspdList, lngRow, plngSPDCOL, pstrBGColor3);
    };
    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue4) {
        ps_setSpreadForeColor_Cell(pobjspdList, lngRow, plngSPDCOL, pstrBGColor4);
    };
    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue5) {
        ps_setSpreadForeColor_Cell(pobjspdList, lngRow, plngSPDCOL, pstrBGColor5);
    };
};
}
//***************
//***************
//2016-11-18
//해당값이면, (행)락 true, false
//사용금지, 아래 ps_setSpreadChkVal_RowLock 를 사용할것
////////function ps_setSpreadChkVal_Lock(pobjspdList, plngSPDCOL, pstrValue, pblnLock) {
////////for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
////////    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue) {
////////	    pobjspdList.getRow(lngRow).locked(pblnLock);
////////    };
////////};
////////}
////////function ps_setSpreadChkVal_RowLock(pobjspdList, plngSPDCOL, pstrValue, pblnLock) {
////////for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
////////    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue) {
////////	    pobjspdList.getRow(lngRow).locked(pblnLock);
////////    };
////////};
////////}
//////////해당값이면, (컬럼)락 true, false
////////function ps_setSpreadChkVal_ColLock(pobjspdList, plngSPDCOL, pstrValue, pblnLock) {
////////for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
////////    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue) {
////////	    pobjspdList.getColumn(lngRow).locked(pblnLock);
////////    };
////////};
////////}
//////////해당값이면, (Cell)락 true, false
////////function ps_setSpreadChkVal_CellLock(pobjspdList, plngSPDCOL, pstrValue, pblnLock) {
////////for (lngRow = 0; lngRow < pobjspdList.getRowCount() ; lngRow++) {
////////    if (gf_Null(pobjspdList.getValue(lngRow, plngSPDCOL), "") == pstrValue) {
////////	    pobjspdList.getCell(lngRow,plngSPDCOL).locked(pblnLock);
////////    };
////////};
////////}
//***************
//***************
//2016-10-19
//COLUMN (전체) 글자색
function ps_setSpreadForeColor_Col(pobjspdList, plngSPDCOL, pstrColor) {
	pobjspdList.getRange(-1, plngSPDCOL, -1, 1).foreColor(pstrColor);
}
//COLUMN (전체) 배경색
function ps_setSpreadBackColor_Col(pobjspdList, plngSPDCOL, pstrColor) {
	pobjspdList.getRange(-1, plngSPDCOL, -1, 1).backColor(pstrColor);
    pobjspdList.getRange(-1, plngSPDCOL, -1, 1).borderTop(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    pobjspdList.getRange(-1, plngSPDCOL, -1, 1).borderBottom(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    pobjspdList.getRange(-1, plngSPDCOL, -1, 1).borderRight(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    pobjspdList.getRange(-1, plngSPDCOL, -1, 1).borderLeft(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
}
//***************
//***************
//2016-10-19
//ROW (전체) 글자색
function ps_setSpreadForeColor_Row(pobjspdList, plngSPDROW, pstrColor) {
	pobjspdList.getRow(plngSPDROW).foreColor(pstrColor);
}
//ROW (전체) 배경색
function ps_setSpreadBackColor_Row(pobjspdList, plngSPDROW, pstrColor) {
    //pobjspdList.getRow(plngSPDROW).backColor(pstrColor);
    //pobjspdList.getRow(plngSPDROW).borderTop(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    //pobjspdList.getRow(plngSPDROW).borderBottom(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    //pobjspdList.getRow(plngSPDROW).borderRight(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    //pobjspdList.getRow(plngSPDROW).borderLeft(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));

    pobjspdList.getRange(plngSPDROW, -1, 1, -1).backColor(pstrColor);
    pobjspdList.getRange(plngSPDROW, -1, 1, -1).borderTop(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    pobjspdList.getRange(plngSPDROW, -1, 1, -1).borderBottom(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    pobjspdList.getRange(plngSPDROW, -1, 1, -1).borderRight(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    pobjspdList.getRange(plngSPDROW, -1, 1, -1).borderLeft(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));


}
//***************
//***************
//2016-10-19
//CELL 글자색
function ps_setSpreadForeColor_Cell(pobjspdList, plngSPDROW, plngSPDCOL, pstrColor) {
    var cell = pobjspdList.getCell(plngSPDROW, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
    cell.foreColor(pstrColor);
	//sheet.getCell(args.row, curCol, GC.Spread.Sheets.SheetArea.viewport).borderRight(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
//    cell.borderTop(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
//    cell.borderBottom(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
//    cell.borderRight(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
//    cell.borderLeft(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
}
//CELL 배경색
function ps_setSpreadBackColor_Cell(pobjspdList, plngSPDROW, plngSPDCOL, pstrColor) {
    var cell = pobjspdList.getCell(plngSPDROW, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);
    cell.backColor(pstrColor);
    //pobjspdList.getRow(plngSPDROW).borderTop(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    //pobjspdList.getRow(plngSPDROW).borderBottom(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    //pobjspdList.getRow(plngSPDROW).borderRight(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
    //pobjspdList.getRow(plngSPDROW).borderLeft(new GC.Spread.Sheets.LineBorder(BKC_DEF, GC.Spread.Sheets.LineStyle.thin));
}
//***************
//***************
//2016-10-19
//라인정의
function ps_setSpreadLineColor_Row(pobjspdList, plngSPDROW, pstrColor) {
//    pobjspdList.getRow(plngSPDROW).borderTop(new GC.Spread.Sheets.LineBorder(pstrColor, GC.Spread.Sheets.LineStyle.thin));
    pobjspdList.getRange(plngSPDROW, -1, 1, -1).borderBottom(new GC.Spread.Sheets.LineBorder(pstrColor, GC.Spread.Sheets.LineStyle.thin));
    //pobjspdList.getRow(plngSPDROW).borderBottom(new GC.Spread.Sheets.LineBorder(pstrColor, GC.Spread.Sheets.LineStyle.thin));
}
//***************
//***************
//2016-10-19
//COL 락 LOCK
function ps_setSpreadLock_Col(pobjspdList, plngSPDCOL, pblnLock) {
	pobjspdList.getColumn(plngSPDCOL).locked(pblnLock);
}
//ROW 락 LOCK
function ps_setSpreadLock_Row(pobjspdList, plngSPDROW, pblnLock) {
	pobjspdList.getRange(plngSPDROW, -1, 1, -1).locked(pblnLock);
}
//CELL 락 LOCK
function ps_setSpreadLock_Cell(pobjspdList, plngSPDROW, plngSPDCOL, pblnLock) {
	pobjspdList.getCell(plngSPDROW,plngSPDCOL).locked(pblnLock);
}
//***************
//2016-11-18
//특정값(이후***)로 Row 색상을 제어한다.
//정렬된행에서 사용가능
function ps_setSpreadRowChkVal_Style(pobjspdList, plngSPDCOL, pstrChkValue, pstrForeColor, pstrBackColor) {
    var searchCondition = new GC.Spread.Sheets.Search.SearchCondition();
    searchCondition.searchString = pstrChkValue;
    searchCondition.startSheetIndex = 0;
    searchCondition.endSheetIndex = 0;
    searchCondition.columnStart = plngSPDCOL;
    searchCondition.columnEnd = plngSPDCOL;
    searchCondition.searchOrder = GC.Spread.Sheets.Search.SearchOrder.nOrder;
    searchCondition.searchTarget = GC.Spread.Sheets.Search.SearchFoundFlags.cellText;
    searchCondition.searchFlags = GC.Spread.Sheets.Search.SearchFlags.ignoreCase| GC.Spread.Sheets.Search.SearchFlags.useWildCards;
    var searchresult = pobjspdList.search(searchCondition);
    //COUNTIF 문자열 만들기
    var strFindStr = "";
    strFindStr = "COUNTIF("+arrMC_SPD_HEADER_ABC[plngSPDCOL]+":"+arrMC_SPD_HEADER_ABC[plngSPDCOL]+",\""+pstrChkValue+"\")";
    //문자열 카운트 셀에 저장
    pobjspdList.getCell(0, 0).formula(strFindStr);
    var rowCnt = Number(pobjspdList.getValue(0, 0));
    var rowIndex = Number(searchresult.foundRowIndex);
	for (var lngRow = 0; lngRow < rowCnt ; lngRow++) {
        if (pstrForeColor != null) {ps_setSpreadForeColor_Row(pobjspdList, rowIndex+lngRow, pstrForeColor);}
        if (pstrBackColor != null) {ps_setSpreadBackColor_Row(pobjspdList, rowIndex+lngRow, pstrBackColor);}
	};
}
//***************
//2016-11-18
//특정값(이후***)로 Row 색상을 제어한다.
//정렬된행에서 사용가능
function ps_setSpreadRowChkDup(pobjspdList, plngSPDCOL, pstrChkValue) {
    var returnCnt = 0;
    var searchCondition = new GC.Spread.Sheets.Search.SearchCondition();
    searchCondition.searchString = pstrChkValue;
    searchCondition.startSheetIndex = 0;
    searchCondition.endSheetIndex = 0;
    searchCondition.columnStart = plngSPDCOL;
    searchCondition.columnEnd = plngSPDCOL;
    searchCondition.searchOrder = GC.Spread.Sheets.Search.SearchOrder.nOrder;
    searchCondition.searchTarget = GC.Spread.Sheets.Search.SearchFoundFlags.cellText;
    searchCondition.searchFlags = GC.Spread.Sheets.Search.SearchFlags.ignoreCase| GC.Spread.Sheets.Search.SearchFlags.useWildCards;
    var searchresult = pobjspdList.search(searchCondition);
    //COUNTIF 문자열 만들기
    var strFindStr = "";
    strFindStr = "=COUNTIF("+arrMC_SPD_HEADER_ABC[plngSPDCOL]+":"+arrMC_SPD_HEADER_ABC[plngSPDCOL]+",\""+pstrChkValue+"\")";
    //문자열 카운트 셀에 저장 nRow : 임의 setData addRow
	var nRow = pobjspdList.getRowCount();
	pobjspdList.addRows(nRow, 1);    //임의 데이터를 저장 할 row 생성
	pobjspdList.getCell(nRow, 0).formula(strFindStr);
    var rowCnt = Number(pobjspdList.getValue(nRow, 0)) - 1;
	var rowIndex = Number(searchresult.foundRowIndex) + 1;
	pobjspdList.deleteRows(nRow,1); //임의 데이터를 처리 후 row 제거

	if(rowCnt > 0){
		pobjspdList.setActiveCell(rowIndex - 1, plngSPDCOL);
		return rowIndex + "::" + pstrChkValue;
	}else{
		return 0 + "::";
	}
}
//***************
//2016-11-18
//특정값의 Row (1)라인을 제어한다.
//정렬된행에서 사용가능
function ps_setSpreadRowChkVal_Line(pobjspdList, plngSPDCOL, pstrChkValue, pstrLineColor) {
    var searchCondition = new GC.Spread.Sheets.SearchCondition();
    searchCondition.searchString = pstrChkValue;
    searchCondition.startSheetIndex = 0;
    searchCondition.endSheetIndex = 0;
    searchCondition.columnStart = plngSPDCOL;
    searchCondition.columnEnd = plngSPDCOL;
    searchCondition.searchOrder = GC.Spread.Sheets.SearchOrder.NOrder;
    searchCondition.searchTarget = GC.Spread.Sheets.SearchFoundFlags.CellText;
    searchCondition.searchFlags = GC.Spread.Sheets.SearchFlags.Ignorecase| GC.Spread.Sheets.SearchFlags.UseWildCards;
    var searchresult = pobjspdList.search(searchCondition);
//    //COUNTIF 문자열 만들기
//    var strFindStr = "";
//    strFindStr = "COUNTIF("+arrMC_SPD_HEADER_ABC[plngSPDCOL]+":"+arrMC_SPD_HEADER_ABC[plngSPDCOL]+",\""+pstrChkValue+"\")";
//    //문자열 카운트 셀에 저장
//    pobjspdList.getCell(0, 0).formula(strFindStr);
//    var rowCnt = Number(pobjspdList.getValue(0, 0));
//    var rowIndex = Number(searchresult.foundRowIndex);
    ps_setSpreadLineColor_Row(pobjspdList, Number(searchresult.foundRowIndex), pstrLineColor);
}
//***************
//2016-11-18
//특정값으로 Cell색상 한개를 제어한다.
function ps_setSpreadColChkVal_Style(pobjspdList, plngSPDCOL, pstrChkValue, pstrForeColor, pstrBackColor) {
    //특정cell값(pstrChkValue)으로 속성을 정의한다.
    var style = new GC.Spread.Sheets.Style();
    if (pstrForeColor != null) {style.foreColor = pstrForeColor;};
    if (pstrBackColor != null) {style.backColor = pstrBackColor;};
//    if (pblnLock != null) {style.locked = pblnLock;};     //***사용하지말것(styel속성이라, 값이 맞으면, Lock이 걸림)
//    style.borderTop = new GC.Spread.Sheets.LineBorder("blue",GC.Spread.Sheets.LineStyle.thin);
    var rule = new GC.Spread.Sheets.SpecificTextRule(GC.Spread.Sheets.TextComparisonOperator.Contains, pstrChkValue, style);
    rule.ranges = [new GC.Spread.Sheets.Range(0, plngSPDCOL, pobjspdList.getRowCount(), plngSPDCOL)];
    pobjspdList.getConditionalFormats().addRule(rule);
}
//***************
//2016-10-26
//스프레드 컬럼 합계 "arrBind", "RCP_QTY,DLV_QTY,STC_QTY"
function ps_setSpdColSum(ArrBindName, ArrColNam) {
	// 1. 컬럼명 나누어 변수 처리
	var arrColNam;
	arrColNam = ArrColNam.split(',');
	for (j=0;j<arrColNam.length;j++) {
		eval('cSum'+arrColNam[j]+'=0');
    }


	// 2. 컬럼별 합계 처리
	for(i = 0; i < eval(ArrBindName+'.length'); i++) {
		for (j=0;j<arrColNam.length;j++) {
			eval("cSum"+arrColNam[j]+"+=Number("+ArrBindName+"["+i+"]['"+arrColNam[j]+"'])");
		}
	}
}

//2016-10-26
//스프레드 컬럼 합계 "arrBind", "RCP_QTY,DLV_QTY,STC_QTY"
function ps_setSpdRowSum(RowNum, ArrBindName, ArrColNam) {
	// 1. 컬럼명 나누어 변수 처리
	var arrColNam;
	var rowSum = 0;
	arrColNam = ArrColNam.split(',');
	// 2. 해당 컬럼 합계 리턴
	for (j=0;j<arrColNam.length;j++) {
		eval("rowSum"+"+=Number("+ArrBindName+"["+RowNum+"]['"+arrColNam[j]+"'])");
	}
	return rowSum;
}
function ps_setSpdChkALL(pobjspdList, ArrBindName) {
	// 선택 대상 있는지 여부
	if (eval(ArrBindName+'.length') == 0){
		alert('선택할 대상이 없습니다.');
		return false;
	}
	// 버튼 텍스트 비교 후 선택/취소 처리
	if( jQuery('#btnProcessCHK').html() == '선택' ) {
		for (i=0; i < eval(ArrBindName+'.length'); i++) {
			eval(pobjspdList+'.setValue('+i+','+0+','+true+')');
		}
	    jQuery('#btnProcessCHK').html('해제');
	} else {
		for (i=0; i < eval(ArrBindName+'.length'); i++) {
			eval(pobjspdList+'.setValue('+i+','+0+','+false+')');
		}
		jQuery('#btnProcessCHK').html('선택');
	}
}

//***************
//2016-10-31
//CSV 스트링
function ps_setSpreadCSV() {
	alert(window.CSVString = spdSheet.getSheet(0).getCsv(0, 0, 16, 7, "\r", ","));
}
//***************
//2016-10-31
//스프레드 프린트
function ps_setSpreadPRINT() {
	spdSheet.print(0);
}
//***************
//2016-11-01
//컬럼 헤더 스팬
function ps_setSpreadTitle_Span(pobjspdList, plngStartRow, plngStartCol, plngRowCnt, plngEndColCnt) {
    pobjspdList.addSpan(plngStartRow,plngStartCol,plngRowCnt,plngEndColCnt,GC.Spread.Sheets.SheetArea.colHeader);
}
//***************
//2016-11-01
//컬럼 스팬
function ps_setSpreadCell_Span(pobjspdList, plngStartRow, plngStartCol, plngRowCnt, plngEndColCnt) {
    pobjspdList.addSpan(plngStartRow,plngStartCol,plngRowCnt,plngEndColCnt,GC.Spread.Sheets.SheetArea.viewport);
}
//***************
//2016-11-02
// popup 업체 구분 셀렉트 박스 세팅, true false로 셀렉트 박스 가/불가 지정
function ps_setPop_CST_TYP(pstrPOPUP_MODE, pboolLock){
	var SearchCST = [];
	pf_setCombo_List('020',function(data){ SearchCST =  data; });
	pf_setCombo_ppt(SearchCST, 'SearchCST_TYP', 'N');
	if ((pf_getUserMNG_GRD()==MC_LOGIN_MNG_GRD_CST_GEN)||(pf_getUserMNG_GRD()==MC_LOGIN_MNG_GRD_CST_GRP)) {
		if (pboolLock == true) { jQuery('#SearchCST_TYP').attr('disabled','disabed'); } else { jQuery('#SearchCST_TYP').removeAttr('disabled'); }
		jQuery('#SearchCST_TYP').val(pstrPOPUP_MODE);
	} else {
		jQuery('#SearchCST_TYP').removeAttr('disabled');
		jQuery('#SearchCST_TYP').val(pstrPOPUP_MODE);
	}
}
//***************
//2016-11-15
// 스프레드 상 PRD_COD 입력 시 기준 열로 PRD_NAM 세팅
function ps_setSpPrdCodName(pobjspdList, pstrRow, pstrCol, pstrRtnCol){
    var tempPrdCod = "";
    tempPrdCod = gf_Null(pobjspdList.getValue(pstrRow,pstrCol),"")+'';
    if (tempPrdCod.length == 5){
        pobjspdList.setValue(pstrRow,pstrCol,tempPrdCod);
        // DB 조회 후 값 가져오기
        pf_getNAM_PRD_SPJS(pobjspdList, pstrRow, pstrCol, pstrRtnCol);
    } else if (tempPrdCod.length != 5){
        pobjspdList.setValue(pstrRow,pstrCol,"");
        pobjspdList.setValue(pstrRow,pstrRtnCol,"");
    }
}
//***************
//2016-11-18
// 스프레드 상 CST_COD 입력 시 기준 열로 CST_NAM 세팅
function ps_setSpCstCodName(pobjspdList, pstrRow, pstrCol, pstrRtnCol){
    var tempCstCod = "";
    tempCstCod = gf_Null(pobjspdList.getValue(pstrRow,pstrCol),"")+'';
    if (tempCstCod.length == 6){
        pobjspdList.setValue(pstrRow,pstrCol,tempCstCod);
        // DB 조회 후 값 가져오기
        pf_getNAM_CST_SPJS(pobjspdList, pstrRow, pstrCol, pstrRtnCol);
    } else if (tempCstCod.length != 6){
        pobjspdList.setValue(pstrRow,pstrCol,"");
        pobjspdList.setValue(pstrRow,pstrRtnCol,"");
    }
}
//***************
//2017-04-13
// 스프레드 상 CST_COD 입력 시 기준 열로 CST_NAM 세팅(CST_TYP 추가)
// 스프레드 업체 정보 지정(csttyp LogisCode.js // 업체구분 기준
function ps_setSpCstCodNameType(pobjspdList, pstrRow, pstrCol, pstrRtnCol, pstrCstTyp){
    var tempCstCod = "";
    tempCstCod = gf_Null(pobjspdList.getValue(pstrRow,pstrCol),"")+'';
    if (tempCstCod.length == 6){
        pobjspdList.setValue(pstrRow,pstrCol,tempCstCod);
        // DB 조회 후 값 가져오기
        pf_getNAM_CST_SPJS_TYP(pobjspdList, pstrRow, pstrCol, pstrRtnCol,pstrCstTyp);
    } else if (tempCstCod.length != 6){
        pobjspdList.setValue(pstrRow,pstrCol,"");
        pobjspdList.setValue(pstrRow,pstrRtnCol,"");
    }
}
//***************
//LostFocus PRD_COD 2016-11-15
function pf_getNAM_PRD_SPJS(pobjspdList, pstrRow, pstrCol, rtnCol) {
var prdcod = pobjspdList.getText(pstrRow, pstrCol)+'';
prdcod=prdcod.toUpperCase();
if(prdcod.length != 5) {pobjspdList.setValue(pstrRow, pstrCol,"");return;};
jQuery.ajax({
    type:"POST",url:"/_mod/INF_PRD.asp",
    data:{"CODE":prdcod},dataType:"json",
    success:function(ds) {pobjspdList.setText(pstrRow, pstrCol,"");pobjspdList.setText(pstrRow, rtnCol,"");jQuery.each(ds, function(i, entry){pobjspdList.setText(pstrRow,rtnCol,ds[i].NAME);pobjspdList.setText(pstrRow,pstrCol,ds[i].CODE);})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
//***************
//LostFocus CST_COD 2016-11-18
function pf_getNAM_CST_SPJS(pobjspdList, pstrRow, pstrCol, rtnCol) {
var cstcod = pobjspdList.getText(pstrRow, pstrCol)+'';
if(cstcod.length != 6) {pobjspdList.setValue(pstrRow, pstrCol,"");return;};
jQuery.ajax({
    type:"POST",url:"/_mod/INF_CST.asp",
    data:{"CODE":cstcod},dataType:"json",
    success:function(ds) {pobjspdList.setText(pstrRow, pstrCol,"");pobjspdList.setText(pstrRow, rtnCol,"");jQuery.each(ds, function(i, entry){pobjspdList.setText(pstrRow,rtnCol,ds[i].NAME);pobjspdList.setValue(pstrRow,pstrCol,ds[i].CODE);})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
//***************
//LostFocus CST_COD 2016-11-18
// 스프레드 업체 정보 지정(csttyp LogisCode.js // 업체구분 기준
function pf_getNAM_CST_SPJS_TYP(pobjspdList, pstrRow, pstrCol, rtnCol, pstrCstTyp) {
var cstcod = pobjspdList.getText(pstrRow, pstrCol)+'';
if(cstcod.length != 6) {pobjspdList.setValue(pstrRow, pstrCol,"");return;};
jQuery.ajax({
    type:"POST",url:"/_mod/INF_CST.asp",
    data:{"CODE":cstcod,"TYPE":pstrCstTyp},dataType:"json",
    success:function(ds) {pobjspdList.setText(pstrRow, pstrCol,"");pobjspdList.setText(pstrRow, rtnCol,"");jQuery.each(ds, function(i, entry){pobjspdList.setText(pstrRow,rtnCol,ds[i].NAME);pobjspdList.setValue(pstrRow,pstrCol,ds[i].CODE);})},
    error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
});
}
// 엑셀 임포트 익스포트 JS
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Export CSV
//function exportFileWithIFrame(serverUrl, jQueryfileElement, options) {
//////////////////////////////////////////////////////////////////////////////////////////////////////////////


//***************
//2016-11-18
//window.resize
function onResizeControl() {
try{
    var MC_DOCUMENT_WIDTH = (self.innerWidth);
    var MC_DOCUMENT_HEIGHT = (self.innerHeight);
    if (MC_DOCUMENT_WIDTH <= 500) {
        jQuery("#divPanel_SpdList").css("height", 500);
        jQuery("#spdList").css("height", 500);
    } else {
        var intdivPanelSearch_Height = parseInt(jQuery("#divPanel_Search").css("height"));
        var intdivPanelTitle_Height  = parseInt(jQuery(".content-header").css("height"));
        var intdivPanelHeader_Height = parseInt(jQuery(".main-header").css("height"));
        var intdivPanelSpdList_Height = MC_DOCUMENT_HEIGHT - intdivPanelTitle_Height;
        intdivPanelSpdList_Height = intdivPanelSpdList_Height - intdivPanelHeader_Height;
        intdivPanelSpdList_Height = intdivPanelSpdList_Height - intdivPanelSearch_Height;
        intdivPanelSpdList_Height = intdivPanelSpdList_Height - 70;         //하단간격***
        jQuery("#divPanel_SpdList").css("height", intdivPanelSpdList_Height);
        jQuery("#spdList").css("height", parseInt(jQuery("#divPanel_SpdList").css("height")));
    };
	//화면 resize 공통함수 작업 시 width height 모듈화 필요
	var setPdfViewW = jQuery("#pdfView").parent().width() - PUB_PDFDESC_POSSIZE;
	jQuery("#pdfView").attr("style", "float:right; position:absolute; left:"+PUB_PDFDESC_POSSIZE+"px; width:"+setPdfViewW+"px; height:100%;");
} catch(e) {};
}
//***************
//2016-11-21
//문자열 값 서칭 기능 : 서치 여부 및 해당 셀 값 리턴
function ps_setSpreadSearchStringCore(pobjspdList, plngRowStart, plngRowEnd, plngColStart, plngColEnd, pstrChkValue) {
    var searchCondition = new GC.Spread.Sheets.SearchCondition();
    searchCondition.searchString = pstrChkValue;
    searchCondition.startSheetIndex = 0;
    searchCondition.endSheetIndex = 0;
    searchCondition.rowStart = plngRowStart;
    searchCondition.rowEnd = plngRowEnd;
    searchCondition.columnStart = plngColStart;
    searchCondition.columnEnd = plngColEnd;
    searchCondition.searchOrder = GC.Spread.Sheets.SearchOrder.NOrder;
    searchCondition.searchTarget = GC.Spread.Sheets.SearchFoundFlags.CellText;
    searchCondition.searchFlags = GC.Spread.Sheets.SearchFlags.Ignorecase| GC.Spread.Sheets.SearchFlags.UseWildCards;
    var searchresult = pobjspdList.search(searchCondition);
    return searchresult;
}
//***************
//행단위 작업처리시 적용
//2016-11-21
//문자열 값 서칭 기능 : 서치 여부 및 해당 셀 값 리턴
//pstrLockMode : ROW / CELL
//pstrForeMode : ROW / CELL
//pstrBackMode : ROW / CELL
function ps_setSpreadSearchValue_Style(pobjspdList, plngSPDCOL, pstrChkValue, pblnLock, pstrLockMode, pstrForeColor, pstrForeMode, pstrBackColor, pstrBackMode, pstrLineColor, pblnLineRepeat) {
    var rtnFlag = 1;
    var sr;
    var RowIdx = 0;
    var ColIdx = 0;
    sr = ps_setSpreadSearchStringCore(pobjspdList, RowIdx, pobjspdList.getRowCount(), plngSPDCOL, plngSPDCOL, pstrChkValue);
    RowIdx = sr.foundRowIndex;
    ColIdx = sr.foundColumnIndex;
    rtnFlag = sr.searchFoundFlag;
    if (pstrForeColor != null) {if (pstrForeMode == "ROW"){ps_setSpreadForeColor_Row(pobjspdList, Number(RowIdx), pstrForeColor);}
                                else {ps_setSpreadForeColor_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pstrForeColor);}}
    if (pstrBackColor != null) {if (pstrBackMode == "ROW"){ps_setSpreadBackColor_Row(pobjspdList, Number(RowIdx), pstrBackColor);}
                                else {ps_setSpreadBackColor_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pstrBackColor);}}
    if (pblnLock != null) {if (pstrLockMode == "ROW"){ps_setSpreadLock_Row(pobjspdList, Number(RowIdx), pblnLock);}
                           else {ps_setSpreadLock_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pblnLock);}}
    if (pstrLineColor != null) {ps_setSpreadLineColor_Row(pobjspdList, Number(RowIdx), pstrLineColor);}
    while (rtnFlag == 1) {
        sr = ps_setSpreadSearchStringCore(pobjspdList, Number(RowIdx) + 1, pobjspdList.getRowCount(), plngSPDCOL, plngSPDCOL, pstrChkValue);
        RowIdx = sr.foundRowIndex;
        ColIdx = sr.foundColumnIndex;
        rtnFlag = sr.searchFoundFlag;
        if (pstrForeColor != null) {if (pstrForeMode == "ROW"){ps_setSpreadForeColor_Row(pobjspdList, Number(RowIdx), pstrForeColor);}
                                    else {ps_setSpreadForeColor_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pstrForeColor);}}
        if (pstrBackColor != null) {if (pstrBackMode == "ROW"){ps_setSpreadBackColor_Row(pobjspdList, Number(RowIdx), pstrBackColor);}
                                    else {ps_setSpreadBackColor_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pstrBackColor);}}
        if (pblnLock != null) {if (pstrLockMode == "ROW"){ps_setSpreadLock_Row(pobjspdList, Number(RowIdx), pblnLock);}
                               else {ps_setSpreadLock_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pblnLock);}}
        if (pblnLineRepeat != null){if (pblnLineRepeat == true){if (pstrLineColor != null) {ps_setSpreadLineColor_Row(pobjspdList, Number(RowIdx), pstrLineColor);}}}
        if (sr.searchFoundFlag < 1) {break;}
    }
}
//2017-06-09
//문자열 값 서칭 기능 : 서치 여부 및 해당 셀 값 리턴
//pstrLockMode : ROW / CELL
//pstrForeMode : ROW / CELL
//pstrBackMode : ROW / CELL
//pstrSelFlg : 체크박스 락 선택
function ps_setSpreadSearchValue_LockStyle(pobjspdList, plngSPDCOL, pstrChkValue, pblnLock, pstrLockMode, pstrForeColor, pstrForeMode, pstrBackColor, pstrBackMode, pstrLineColor, pblnLineRepeat, pstrSelFlg) {
    var rtnFlag = 1;
    var sr;
    var RowIdx = 0;
    var ColIdx = 0;
    sr = ps_setSpreadSearchStringCore(pobjspdList, RowIdx, pobjspdList.getRowCount(), plngSPDCOL, plngSPDCOL, pstrChkValue);
    RowIdx = sr.foundRowIndex;
    ColIdx = sr.foundColumnIndex;
    rtnFlag = sr.searchFoundFlag;
    if (pstrForeColor != null) {if (pstrForeMode == "ROW"){ps_setSpreadForeColor_Row(pobjspdList, Number(RowIdx), pstrForeColor);}
                                else {ps_setSpreadForeColor_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pstrForeColor);}}
    if (pstrBackColor != null) {if (pstrBackMode == "ROW"){ps_setSpreadBackColor_Row(pobjspdList, Number(RowIdx), pstrBackColor);}
                                else {ps_setSpreadBackColor_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pstrBackColor);}}
    if (pblnLock != null) {if (pstrLockMode == "ROW"){ps_setSpreadLock_Row(pobjspdList, Number(RowIdx), pblnLock);ps_setSpreadLock_Cell(pobjspdList, Number(RowIdx),0,pstrSelFlg);}
                           else {ps_setSpreadLock_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pblnLock);ps_setSpreadLock_Cell(pobjspdList, Number(RowIdx),0,pstrSelFlg);}}
    if (pstrLineColor != null) {ps_setSpreadLineColor_Row(pobjspdList, Number(RowIdx), pstrLineColor);}
    while (rtnFlag == 1) {
        sr = ps_setSpreadSearchStringCore(pobjspdList, Number(RowIdx) + 1, pobjspdList.getRowCount(), plngSPDCOL, plngSPDCOL, pstrChkValue);
        RowIdx = sr.foundRowIndex;
        ColIdx = sr.foundColumnIndex;
        rtnFlag = sr.searchFoundFlag;
        if (pstrForeColor != null) {if (pstrForeMode == "ROW"){ps_setSpreadForeColor_Row(pobjspdList, Number(RowIdx), pstrForeColor);}
                                    else {ps_setSpreadForeColor_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pstrForeColor);}}
        if (pstrBackColor != null) {if (pstrBackMode == "ROW"){ps_setSpreadBackColor_Row(pobjspdList, Number(RowIdx), pstrBackColor);}
                                    else {ps_setSpreadBackColor_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pstrBackColor);}}
        if (pblnLock != null) {if (pstrLockMode == "ROW"){ps_setSpreadLock_Row(pobjspdList, Number(RowIdx), pblnLock);ps_setSpreadLock_Cell(pobjspdList, Number(RowIdx),0,pstrSelFlg);}
                               else {ps_setSpreadLock_Cell(pobjspdList, Number(RowIdx), plngSPDCOL, pblnLock);ps_setSpreadLock_Cell(pobjspdList, Number(RowIdx),0,pstrSelFlg);}}
        if (pblnLineRepeat != null){if (pblnLineRepeat == true){if (pstrLineColor != null) {ps_setSpreadLineColor_Row(pobjspdList, Number(RowIdx), pstrLineColor);}}}
        if (sr.searchFoundFlag < 1) {break;}
    }
}
//임시AJAX
function fetchJSONFile(path, callback) {
    var httpRequest = new XMLHttpRequest();
    httpRequest.onreadystatechange = function() {
        if (httpRequest.readyState === 4) {
            if (httpRequest.status === 200) {
                var data = JSON.parse(httpRequest.responseText);
                if (callback) callback(data);
            } else {
                console.log(path+"리퀘스트 상태값:::::"+httpRequest.status);
            }
        }
    };
    httpRequest.open('GET', path);
    httpRequest.send();
}


//***************
//2016-12-15
//컬럼 헤더 전체선택
function ps_setSelectALL(pobjCOL, args){
    var spd = args.sheet, sheetArea = args.sheetArea, row = args.row, col = args.col;
    var selectCbx = spd.getValue(0, pobjCOL, GC.Spread.Sheets.SheetArea.colHeader);
    spd.suspendPaint();
    if ((selectCbx == null) || (selectCbx == 0)) {
        spd.setValue(0, pobjCOL, 1, GC.Spread.Sheets.SheetArea.colHeader);
        for(var i = 0; i < spd.getRowCount(); i++){
            if (!spd.getCell(i, 0, GC.Spread.Sheets.SheetArea.viewport).locked()) {
                spd.setValue(i, 0, true, GC.Spread.Sheets.SheetArea.viewport);
            }
        }
    }  else {
        spd.setValue(0, pobjCOL, 0, GC.Spread.Sheets.SheetArea.colHeader);
        for(var i = 0; i < spd.getRowCount(); i++){
            if (!spd.getCell(i, 0, GC.Spread.Sheets.SheetArea.viewport).locked()) {
                spd.setValue(i, 0, false, GC.Spread.Sheets.SheetArea.viewport);
            }
        }
    }
    spd.resumePaint();
}

//////////***************
//////////2017-03-08
//////////컬럼 선택 상태 변경
////////function ps_setSelect(args, type){
////////    var sheet = args.sheet;
////////	var chkVal = sheet.getValue(args.row, 0, GcSpread.Sheets.SheetArea.viewport);
////////
////////	sheet.isPaintSuspended(true);
////////	if (args.col === 0 && args.propertyName === "value") {
////////		// 체크박스 일 경우. (col_No == 0)
////////		switch(chkVal){
////////			case true: //chk on
////////				ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
////////				break;
////////			case false: //chk off
////////				ps_spdListChkSetup(args.row, "out"); //arrMC_SPD_CHK_SET seq삭제
////////				break;
////////		}
////////	} else if (args.col != 0 && args.propertyName === "value") {
////////		// 체크박스가 아닐 경우. (col_No > 0)
////////		sheet.setValue(args.row, 0, 1, GcSpread.Sheets.SheetArea.viewport);
////////		ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
////////	}
////////	sheet.resumePaint();
////////}
//////////***************
//////////2017-03-07
//////////컬럼 선택 col별 색상 변경
////////function ps_setSelect(args, type){
////////    var sheet = args.sheet;
////////	var nCol = sheet.getColumnCount();
////////	var chkVal = sheet.getValue(args.row, 0, GcSpread.Sheets.SheetArea.viewport);
////////
////////	// 체크박스 일 경우. (col_No == 0)
////////	if (args.col === 0 && args.propertyName === "value" && type === true) {
////////		sheet.isPaintSuspended(true);
////////		for (var curCol = 1; curCol < nCol ; curCol++) {
////////			if (sheet.getColumn(curCol).locked() === false){
////////				switch(chkVal){
////////					case true: //chk on
////////						ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).backColor(BKC_SK);
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderTop(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderBottom(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderRight(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderLeft(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////						break;
////////					case false: //chk off
////////						ps_spdListChkSetup(args.row, "out"); //arrMC_SPD_CHK_SET seq삭제
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).backColor(BKC_WT);
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderTop(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderBottom(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderRight(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////						sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderLeft(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////						break;
////////				}
////////			}
////////		}
////////		sheet.resumePaint();
////////	} else if (args.col === 0 && args.propertyName === "value" && type === false) {
////////		switch(chkVal){
////////			case true: //chk on
////////				ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
////////				break;
////////			case false: //chk off
////////				ps_spdListChkSetup(args.row, "out"); //arrMC_SPD_CHK_SET seq삭제
////////				break;
////////		}
////////	}
////////
////////	// 체크박스가 아닐 경우. (col_No > 0)
////////	if (args.col != 0 && args.propertyName === "value" && type === true) {
////////		sheet.setValue(args.row, 0, 1, GcSpread.Sheets.SheetArea.viewport);
////////		ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
////////		sheet.isPaintSuspended(true);
////////		for (var curCol = 1; curCol < nCol ; curCol++) {
////////			if (sheet.getColumn(curCol).locked() === false){
////////				sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).backColor(BKC_SK);
////////				sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderTop(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////				sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderBottom(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////				sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderRight(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////				sheet.getCell(args.row, curCol, GcSpread.Sheets.SheetArea.viewport).borderLeft(new GcSpread.Sheets.LineBorder(BKC_DEF, GcSpread.Sheets.LineStyle.thin));
////////			}
////////		}
////////		sheet.resumePaint();
////////	} else if (args.col != 0 && args.propertyName === "value" && type === false) {
////////		sheet.setValue(args.row, 0, true, GcSpread.Sheets.SheetArea.viewport);
////////		ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
////////	}
////////}

//공통함수 spdList 체크상태 확인
function ps_spdListChkSetup(nRow, type){
	switch(type){
		case 'in': //arry in
			if (arrMC_SPD_CHK_SET.indexOf(nRow) != -1) {
			} else {
				arrMC_SPD_CHK_SET.push(nRow); //arrMC_SPD_CHK_SET seq추가
			}
			break;
		case 'out': //arry out
			if (arrMC_SPD_CHK_SET.indexOf(nRow) != -1) {
				arrMC_SPD_CHK_SET.splice(arrMC_SPD_CHK_SET.indexOf(nRow),1); //arrMC_SPD_CHK_SET seq삭제
			} else {
			}
			break;
	}
}
//////////pf_callPopup으로 통합할것
//////////***************
//////////2017-01-11
//////////조건부분 PRD 팝업
////////function pf_callPOP_PRD(pobjOBJ_TYP, pobjPRD_COD, pobjPRD_NAM, pobjSPD){
////////    var innerHtml = "";
////////
////////    if (pobjOBJ_TYP == "TXT") {
////////        //조건부분 PRD 팝업용
////////        innerHtml += "<input type='hidden' id='HOBJ_TYP' value='"+pobjOBJ_TYP+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_ROW_COD' value='"+pobjPRD_COD+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_COL_NAM' value='"+pobjPRD_NAM+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_SPDLIST' value="+pobjSPD+">";
////////    } else if (pobjOBJ_TYP == "ONE"){
////////        //스프레드 부분 PRD 팝업용 ONE 단일 값
////////        innerHtml += "<input type='hidden' id='HOBJ_TYP' value='"+pobjOBJ_TYP+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_ROW_COD' value='"+pobjPRD_COD+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_COL_NAM' value='"+pobjPRD_NAM+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_SPDLIST' value="+pobjSPD+">";
////////    } else {
////////        innerHtml += "<input type='hidden' id='HOBJ_TYP' value='"+pobjOBJ_TYP+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_ROW_COD' value='"+pobjPRD_COD+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_COL_NAM' value='"+pobjPRD_NAM+"'>";
////////        innerHtml += "<input type='hidden' id='HPRD_SPDLIST' value="+pobjSPD+">";
////////    }
////////    jQuery("#divHidden_Zone").html(innerHtml);
////////    window.open('/_POPUP/POP_PRDWF/POP_PRD.asp', 'POP_PRD', 'toolbar=no,scrollbars=no,resizable=no,top=200,left=150,width=1000,height=600');
////////}
//***************
//2017-01-11
// PRD 팝업 필요사항
// pstrPopTyp : 업체선택창 "CST", 유형선택창 : "PRD"
// pobjOBJ_TYP : 객체 타입 "TXT": 조회조건, "SPD":스프레드
// pobjOBJ_CNT : "ONE_COD" 코드 단일, "ONE_NAM" 명칭 단일, "TWO_ALL" 코드/명칭
// pobjROW_COD : "HPRD_ROW_COD" 리턴로우 넘버 / COD 객체명
// pobjCOL_NAM : "HPRD_ROW_NAM" 리턴컬럼 넘버 / NAM 객체명
// pobjSPD :  "HCST_SPDLIST": 스프레드 번호
// pstrCAT_TYP :  "HCAT_TYP": 분류 카테고리 값(CST의 경우 업체구분, PRD의 경우 PRD_TYP)
// pstrPRG_ID :  프로그램 ID
function pf_callPopup(pstrPopTyp, pobjOBJ_TYP, pobjOBJ_CNT, pobjROW_COD, pobjCOL_NAM, pobjSPD, pstrCAT_TYP, pstrPRG_ID){

//pf_callPopup("CST_ARV", "", "", pobjROW_COD, pobjCOL_NAM, pobjSPD, "", pstrPRG_ID)
    var innerHtml = "";
    var tmpCOL_TYP = Number(pobjCOL_NAM) + 1;

    if (pstrPopTyp == "ZIP") {
        innerHtml += "<input type='hidden' id='HZIP_ROW_NUM' value='"+pobjROW_COD+"'>";
        innerHtml += "<input type='hidden' id='HZIP_COL_NUM' value='"+pobjCOL_NAM+"'>";
        innerHtml += "<input type='hidden' id='HZIP_SPDLIST' value="+pobjSPD+">";
    } else {
        if (pstrPopTyp == "CST_ARV") {
            innerHtml += "<input type='hidden' id='HARV_ZIP_ROW_NUM' value='"+pobjROW_COD+"'>";
            innerHtml += "<input type='hidden' id='HARV_ZIP_COL_NUM' value='"+pobjCOL_NAM+"'>";
            innerHtml += "<input type='hidden' id='HARV_ZIP_SPDLIST' value="+pobjSPD+">";
            innerHtml += "<input type='hidden' id='HARV_PRG_ID' value="+pstrPRG_ID+">";
        }else if (pstrPopTyp == "CST_CAR_MNG") {
            innerHtml += "<input type='hidden' id='HARV_ROW_NUM' value='"+pobjROW_COD+"'>";
            innerHtml += "<input type='hidden' id='HARV_COL_NUM' value='"+pobjCOL_NAM+"'>";
            innerHtml += "<input type='hidden' id='HARV_ZIP_SPDLIST' value="+pobjSPD+">";
            innerHtml += "<input type='hidden' id='HARV_PRG_ID' value="+pstrPRG_ID+">";
        } else {
            innerHtml += "<input type='hidden' id='HPOP_TYP' value="+pstrPopTyp+">";
            innerHtml += "<input type='hidden' id='HOBJ_TYP' value='"+pobjOBJ_TYP+"'>";
            innerHtml += "<input type='hidden' id='HOBJ_CNT' value='"+pobjOBJ_CNT+"'>";
            innerHtml += "<input type='hidden' id='HROW_COD' value='"+pobjROW_COD+"'>";
            innerHtml += "<input type='hidden' id='HCOL_NAM' value='"+pobjCOL_NAM+"'>";
            innerHtml += "<input type='hidden' id='HCOL_TYP' value='"+tmpCOL_TYP+"'>";
            innerHtml += "<input type='hidden' id='HSPDLIST' value="+pobjSPD+">";
            innerHtml += "<input type='hidden' id='HCAT_TYP' value="+pstrCAT_TYP+">";
            innerHtml += "<input type='hidden' id='HPRG_ID' value="+pstrPRG_ID+">";
        }
    }
    jQuery("#divHidden_Zone").html(innerHtml);
    //POPUP
    if (pstrPopTyp == "ZIP") {pf_windowopen('POP_ZIP');}
    if (pstrPopTyp == "PRD") {pf_windowopen('POP_PRD');}
    if (pstrPopTyp == "CST") {pf_windowopen('POP_CST');}
    if (pstrPopTyp == "CST_ARV") {pf_windowopen('POP_CST_ARV');}
    if (pstrPopTyp == "MRO_CST_ARV") {pf_windowopen('POP_MRO_CST_ARV');}
    if (pstrPopTyp == "MRO_CST_ITEM") {pf_windowopen('POP_MRO_CST_ITEM');}
    if (pstrPopTyp == "MRO_MAD_CST") {pf_windowopen('POP_MRO_MAD_CST');}
    if (pstrPopTyp == "MRO_MAD_ITEM") {pf_windowopen('POP_MRO_MAD_ITEM');}
    if (pstrPopTyp == "CST_CAR_MNG") {pf_windowopen('POP_CST_CAR_MNG');}
}

//***************
//2017-01-11
// PRD 팝업 필요사항
// pstrPopTyp : 업체선택창 "CST", 유형선택창 : "PRD"
// pobjOBJ_TYP : 객체 타입 "TXT": 조회조건, "SPD":스프레드
// pobjOBJ_CNT : "ONE_COD" 코드 단일, "ONE_NAM" 명칭 단일, "TWO_ALL" 코드/명칭
// pobjROW_COD : "HPRD_ROW_COD" 리턴로우 넘버 / COD 객체명
// pobjCOL_NAM : "HPRD_ROW_NAM" 리턴컬럼 넘버 / NAM 객체명
// pobjSPD :  "HCST_SPDLIST": 스프레드 번호
// pstrCAT_TYP :  "HCAT_TYP": 분류 카테고리 값(CST의 경우 업체구분, PRD의 경우 PRD_TYP)
// pstrPRG_ID :  프로그램 ID
function pf_callPopup_42004(pstrPopTyp, pobjOBJ_TYP, pobjOBJ_CNT, pobjROW_COD, pobjCOL_NAM, pobjSPD, pstrCAT_TYP, pstrPRG_ID, pstrCOMP_TYPE){
    var innerHtml = "";
    var tmpCOL_TYP = Number(pobjCOL_NAM) + 1;
    if (pstrPopTyp == "ZIP") {
        innerHtml += "<input type='hidden' id='HZIP_ROW_NUM' value='"+pobjROW_COD+"'>";
        innerHtml += "<input type='hidden' id='HZIP_COL_NUM' value='"+pobjCOL_NAM+"'>";
        innerHtml += "<input type='hidden' id='HZIP_SPDLIST' value="+pobjSPD+">";
    } else {
        innerHtml += "<input type='hidden' id='HOBJ_TYP' value='"+pobjOBJ_TYP+"'>";
        innerHtml += "<input type='hidden' id='HOBJ_CNT' value='"+pobjOBJ_CNT+"'>";
        innerHtml += "<input type='hidden' id='HROW_COD' value='"+pobjROW_COD+"'>";
        innerHtml += "<input type='hidden' id='HCOL_NAM' value='"+pobjCOL_NAM+"'>";
        innerHtml += "<input type='hidden' id='HCOL_TYP' value='"+tmpCOL_TYP+"'>";
        innerHtml += "<input type='hidden' id='HSPDLIST' value="+pobjSPD+">";
        innerHtml += "<input type='hidden' id='HCAT_TYP' value="+pstrCAT_TYP+">";
        innerHtml += "<input type='hidden' id='HPRG_ID' value="+pstrPRG_ID+">";
		innerHtml += "<input type='hidden' id='HPOP_TYP' value="+pstrPopTyp+">";
		innerHtml += "<input type='hidden' id='HCOMP_TYPE' value="+pstrCOMP_TYPE+">";
    }
    jQuery("#divHidden_Zone").html(innerHtml);
	//POPUP
    if (pstrPopTyp == "42004_PRD") {pf_windowopen("POP_42004_PRD");}
    if (pstrPopTyp == "42004_CST") {pf_windowopen("POP_42004_CST");}
}
//        pf_callPopup_SCM("SPD", "TWO_ALL",args.row, SPDCOL_REQ_CST_COD, "0","SCM_REQ_CST","spdList");
function pf_callPopup_SCM(pobjOBJ_TYP, pobjOBJ_CNT, pobjROW_COD, pobjCOL_NAM, pobjSPD_NUM, pstrCAT_TYP, pstrSPD_ID){
    var innerHtml = "";
    var tmpCOL_TYP = Number(pobjCOL_NAM) + 1;
    innerHtml += "<input type='hidden' id='HOBJ_TYP' value='"+pobjOBJ_TYP+"'>";
    innerHtml += "<input type='hidden' id='HOBJ_CNT' value='"+pobjOBJ_CNT+"'>";
    innerHtml += "<input type='hidden' id='HROW_COD' value='"+pobjROW_COD+"'>";
    innerHtml += "<input type='hidden' id='HCOL_NAM' value='"+pobjCOL_NAM+"'>";
    innerHtml += "<input type='hidden' id='HCOL_TYP' value='"+tmpCOL_TYP+"'>";
    innerHtml += "<input type='hidden' id='HSPD_NUM' value="+pobjSPD_NUM+">";
    innerHtml += "<input type='hidden' id='HSPD_ID' value="+pstrSPD_ID+">";
    innerHtml += "<input type='hidden' id='HCAT_TYP' value="+pstrCAT_TYP+">";
    jQuery("#divHidden_Zone").html(innerHtml);
	//POPUP
    pf_windowopen("POP_SCM_CST");
}
//주소 팝업 창 호출
function pf_callPopup_ADDR(pobjROW, pobjSTART_COL, pobjGPS_X, pobjGPS_Y, pobjGPS_Z, pstrFULL_ADDR){
    var innerHtml = "";
    //var tmpCOL_TYP = Number(pobjCOL_NAM) + 1;
    innerHtml += "<input type='hidden' id='HADDR_ROW' value='"+pobjROW+"'>";
    innerHtml += "<input type='hidden' id='HADDR_S_COL' value='"+pobjSTART_COL+"'>";
    innerHtml += "<input type='hidden' id='HGPS_X' value='"+pobjGPS_X+"'>";
    innerHtml += "<input type='hidden' id='HGPS_Y' value='"+pobjGPS_Y+"'>";
    innerHtml += "<input type='hidden' id='HGPS_Z' value='"+pobjGPS_Z+"'>";
    innerHtml += "<input type='hidden' id='HADDR_FULL' value="+pstrFULL_ADDR+">";
    jQuery("#divHidden_Zone").html(innerHtml);

    var winPOPUP = window.open("", "ADDR_MAP", "top=150, left=150, width=1000, height=750, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
    winPOPUP.location.href = "/_POPUP/_ADDR_MAP/ADDR_KAKAO.asp?SEARCH_ADDR="+pstrFULL_ADDR+"&GPS_X="+pobjGPS_X+"&GPS_Y="+pobjGPS_Y+"&GPS_Z="+pobjGPS_Z;
    //winPOPUP.location.href = "http://ucps.logisall.co.kr/CPS101WF_RoadZip/v2/?SEARCH_ADDR="+pstrFULL_ADDR+"&GPS_X="+pobjGPS_X+"&GPS_Y="+pobjGPS_Y+"&GPS_Z="+pobjGPS_Z;
}
//////////////////pf_callPopup으로 통합할것
//////////***************
//////////2017-06-15
////////// CST_ARV_ZIP 실 도착지 선택 창
////////// 스프레드에서만 사용 됨
////////// pobjROW_COD, pobjCOL_COD
////////// pobjPRG_ID 업체와 관련 쿼리를 위해
////////// pobjSPD스프레드 탭순서
////////function pf_callPOPUP_CSTARV(pobjROW_COD, pobjCOL_COD, pobjSPD, pstrPRG_ID){
////////    var innerHtml = "";
////////    innerHtml += "<input type='hidden' id='HARV_ZIP_ROW_NUM' value='"+pobjROW_COD+"'>";
////////    innerHtml += "<input type='hidden' id='HARV_ZIP_COL_NUM' value='"+pobjCOL_COD+"'>";
////////    innerHtml += "<input type='hidden' id='HARV_ZIP_SPDLIST' value="+pobjSPD+">";
////////    innerHtml += "<input type='hidden' id='HARV_PRG_ID' value="+pstrPRG_ID+">";
////////    jQuery("#divHidden_ARV_CST").html(innerHtml);
////////    window.open('/_POPUP/_CST_ARV/', 'POP_CST_ARV', 'toolbar=no,scrollbars=no,resizable=no,top=200,left=150,width=1000,height=600');
////////}




// 부모창의 pf_FromPopup(pstrCOD, pstrNAM, pstrTYP)이 받은 값을 히든 정보를 활용해서 객체에 전달
function pf_setPopup(pstrCOD, pstrNAM, pstrTYP){
	var OBJ_TYP = jQuery("#HOBJ_TYP").val();
    var OBJ_CNT = jQuery("#HOBJ_CNT").val();

    // 조회조건용
    if (OBJ_TYP == "TXT" && OBJ_CNT == "ONE_COD") {
        var tmpROW_COD = jQuery("#HROW_COD").val();
        jQuery("#"+tmpROW_COD).val(pstrCOD);
    } else if (OBJ_TYP == "TXT" && OBJ_CNT == "ONE_NAM") {
        var tmpCOL_NAM = jQuery("#HCOL_NAM").val();
        jQuery("#"+tmpCOL_NAM).val(pstrNAM);
    } else if (OBJ_TYP == "TXT" && OBJ_CNT == "TWO_ALL") {
        var tmpROW_COD = jQuery("#HROW_COD").val();
        var tmpCOL_NAM = jQuery("#HCOL_NAM").val();
        jQuery("#"+tmpROW_COD).val(pstrCOD);
        jQuery("#"+tmpCOL_NAM).val(pstrNAM);
    }

    // spread 용
    if (OBJ_TYP == "SPD" && OBJ_CNT == "ONE_COD") {
        var tmpSPDLIST = jQuery('#HSPDLIST').val();
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "ONE_NAM") {
        var tmpSPDLIST = jQuery('#HSPDLIST').val();
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrNAM);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "TWO_ALL") {
        var tmpSPDLIST = Number(jQuery('#HSPDLIST').val());
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+2, pstrNAM);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "THR_ALL") {
        var tmpSPDLIST = Number(jQuery('#HSPDLIST').val());
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        var tmpCOL_TYP = Number(jQuery("#HCOL_TYP").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+2, pstrNAM);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+3, pstrTYP);
    }
}

// 부모창의 pf_setPopup_42004(pstrCOD, pstrNAM, pstrTYP)이 받은 값을 히든 정보를 활용해서 객체에 전달
function pf_setPopup_42004(pstrCOD, pstrNAM, pstrTYP, pstrUsrNam){
	var OBJ_TYP = jQuery("#HOBJ_TYP").val();
    var OBJ_CNT = jQuery("#HOBJ_CNT").val();

    // 조회조건용
    if (OBJ_TYP == "TXT" && OBJ_CNT == "ONE_COD") {
        var tmpROW_COD = jQuery("#HROW_COD").val();
        jQuery("#"+tmpROW_COD).val(pstrCOD);
    } else if (OBJ_TYP == "TXT" && OBJ_CNT == "USR_ALL") {
        var tmpCOL_NAM = jQuery("#HCOL_NAM").val();
        jQuery("#"+tmpCOL_NAM).val(pstrNAM);
    } else if (OBJ_TYP == "TXT" && OBJ_CNT == "TWO_ALL") {
        var tmpROW_COD = jQuery("#HROW_COD").val();
        var tmpCOL_NAM = jQuery("#HCOL_NAM").val();
        jQuery("#"+tmpROW_COD).val(pstrCOD);
        jQuery("#"+tmpCOL_NAM).val(pstrNAM);
    }

    // spread 용
    if (OBJ_TYP == "SPD" && OBJ_CNT == "ONE") {
        var tmpSPDLIST = jQuery('#HSPDLIST').val();
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "USR") {
        var tmpSPDLIST = jQuery('#HSPDLIST').val();
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "USR_TWO") {
        var tmpSPDLIST = Number(jQuery('#HSPDLIST').val());
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        var tmpCOL_TYP = Number(jQuery("#HCOL_TYP").val());
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+2, pstrNAM);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "USR_ALL") {
        var tmpSPDLIST = Number(jQuery('#HSPDLIST').val());
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        var tmpCOL_TYP = Number(jQuery("#HCOL_TYP").val());
    //alert(pstrCOD+"::"+pstrNAM+"::"+pstrTYP+"::"+pstrUsrNam+"::"+pstrCOD+"::"+pstrNAM);
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+2, pstrNAM);
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+3, pstrTYP);
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+4, pstrUsrNam);
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+5, pstrCOD);
        spdSheet_SUB.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+7, pstrNAM);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "LST") {
        var tmpSPDLIST = jQuery('#HSPDLIST').val();
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "LST_TWO") {
        var tmpSPDLIST = Number(jQuery('#HSPDLIST').val());
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        var tmpCOL_TYP = Number(jQuery("#HCOL_TYP").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+2, pstrNAM);
    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "LST_ALL") {
    //alert(pstrCOD+"::"+pstrNAM+"::"+pstrTYP+"::"+pstrUsrNam+"::"+pstrCOD+"::"+pstrNAM);
        var tmpSPDLIST = Number(jQuery('#HSPDLIST').val());
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        var tmpCOL_TYP = Number(jQuery("#HCOL_TYP").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+1, pstrTYP); //usr_cod
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+2, pstrNAM);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+5, pstrCOD);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+7, pstrNAM);
        //spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+48, pstrUsrNam); //usr_nam

    } else if (OBJ_TYP == "SPD" && OBJ_CNT == "THR_ALL") {
        var tmpSPDLIST = Number(jQuery('#HSPDLIST').val());
        var tmpROW_COD = Number(jQuery("#HROW_COD").val());
        var tmpCOL_NAM = Number(jQuery("#HCOL_NAM").val());
        var tmpCOL_TYP = Number(jQuery("#HCOL_TYP").val());
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM, pstrCOD);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+2, pstrNAM);
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+3, pstrTYP);
        if (tmpCOL_NAM == 5 || tmpCOL_NAM == 7) {
            spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+5, pstrCOD);
            spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+7, pstrNAM);
            spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_NAM+8, pstrTYP);
        }
    }
}

//***************
//2017-02-23
// POP_ZIP 팝업 필요사항
// pobjROW_NUM : "HZIP_ROW_NUM" 리턴로우 넘버
// pobjCOL_NUM : "HZIP_COL_NUM" 리턴컬럼 시작 번호
// pobjSPD :  "HZIP_SPDLIST": 스프레드 번호
// 부모창의 pf_FromPopZip(args)이 받은 값을 히든 정보를 활용해서 객체에 전달
function pf_setPopZip(zip1, zip2, zips, addr1, addr2){
    var tmpSPDLIST = jQuery('#HZIP_SPDLIST').val();
    var tmpROW_COD = Number(jQuery("#HZIP_ROW_NUM").val());
    var tmpCOL_COD = Number(jQuery("#HZIP_COL_NUM").val());
    var zipLength = 0;
    zipLength = zip1.length + zip2.length;

    spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_COD, zip1);               //ZIP1_NUM
    spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 1), zip2);   //ZIP2_NUM
    spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 2), zips);   //ZIP_SEQ
    if (zipLength == 5) {
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 4), addr1);   //ADDR1
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 5), addr2);   //ADDR2
    } else {
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 4), addr1+" "+addr2);   //ADDR1
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 5), "");   //ADDR2
    }
}


//***************
//2017-06-15
// CST_ARV_POP_ZIP 팝업 필요사항
// pobjROW_NUM : "HZIP_ROW_NUM" 리턴로우 넘버
// pobjCOL_NUM : "HZIP_COL_NUM" 리턴컬럼 시작 번호
// pobjSPD :  "HZIP_SPDLIST": 스프레드 번호
// 부모창의 pf_FromPopZip(args)이 받은 값을 히든 정보를 활용해서 객체에 전달
function pf_setCstArvPopZip(zip1, zip2, zips, addr1, addr2, cst_arv_seq) {
    var tmpSPDLIST = jQuery('#HARV_ZIP_SPDLIST').val();
    var tmpROW_COD = Number(jQuery("#HARV_ZIP_ROW_NUM").val());
    var tmpCOL_COD = Number(jQuery("#HARV_ZIP_COL_NUM").val());
    var zipLength = 0;
    zipLength = zip1.length + zip2.length;

    spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, tmpCOL_COD, zip1);                      //ZIP1_NUM
    spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 1), zip2);   //ZIP2_NUM
    spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 2), zips);   //ZIP_SEQ
    spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 3), cst_arv_seq);   //CST_ARV_SEQ

    if (zipLength == 5) {
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 4), addr1);   //ADDR1
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 5), addr2);   //ADDR2
    } else {
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 4), addr1);   //ADDR1
        spdSheet.getSheet(tmpSPDLIST).setValue(tmpROW_COD, Number(tmpCOL_COD + 5), addr2);   //ADDR2
    }
}


// PDF 뷰어 실행 프로세스 new
function pf_setPDFViwer(pdfFileName, rptProjectName, rptProgramName, param, urlType){
	var userId = pf_getUserLOGIN_ID();
	var actionUrl   = "http://1.209.94.143:8091/ReportLoader/execution.go?projectName="+rptProjectName+"&programName="+rptProgramName+"&pdfFileName="+pdfFileName+"&urlType="+urlType;
	var urlVal  = "http://"+urlType+".logisall.co.kr/OZReport/"+pdfFileName+".asp?ExportPath=D:/Report/"+rptProjectName+"/"+userId+"/"+param;
    var urlRep  = "/../../../Report/WCPS/"+userId+"/"+pdfFileName+".pdf";
    //var urlRep  = "/_setup/Report_download.asp?id="+userId+"&fname="+pdfFileName+".pdf";
    document.getElementById("pdfView").innerHTML=''; //div pdfView 영역 초기화
	jQuery("#LOADING").modal('show'); //LOADING On

	jQuery.ajax({url:actionUrl
            ,dataType:"jsonp"
            ,data:{"urlVal":encodeURI(urlVal), "userId":userId}
            ,type:'POST'
        ,error: function(rslt){
			jQuery("#LOADING").modal('hide'); //LOADING Off
			alert("X : " + rslt.data.success);
		},
		success: function(rslt){
			//alert("O : " + rslt.data.success);
			if(rslt.data.success == "fail"){
				alert("런타임오류: OZReport 생성 되지 않습니다.(30sec)");
				return false;
			}
			//report 뷰어 import
			var tax_url = "http://1.209.94.143:8092/Report/"+rptProjectName+"/"+userId+"/"+pdfFileName+".pdf?timeTemp="+((new Date()).getTime())+"#zoom=100";
			document.getElementById("pdfView").innerHTML='<object data='+tax_url+' type="application/pdf" style="width:100%;height:100%;"></object>';
			jQuery("#pdfDescText").val("PDF Viewer Text !!");
            jQuery('#btnREPORT_DOWN').css('display','block');
            //jQuery('#btnREPORT_DOWN').attr('target','_blank');
            jQuery('#btnREPORT_DOWN').attr('href',''+urlRep+'');
            jQuery("#LOADING").modal('hide'); //LOADING Off

////////			//report 뷰어 import IE & Chrome 분기
////////			if ( pf_getUserIE_FLG() == "Y")
////////            {
////////                window.open(tax_url, 'POP_PDF', 'toolbar=no,scrollbars=no,resizable=no,top=290,left=240,width=1000,height=600');
////////            } else {
////////                document.getElementById("pdfView").innerHTML='<object data='+tax_url+' type="application/pdf" style="width:100%;height:100%;"></object>';
////////            }

			//div zIndex 설정
			//document.getElementById("ERROR_BOX").style.zIndex="100";
			//document.getElementById("pdfView").style.zIndex="200";
		}
	});
}



// PDF 뷰어 실행 프로세스 new
function pf_setPDFViwerSSL(pdfFileName, rptProjectName, rptProgramName, param, urlType){
	var userId = pf_getUserLOGIN_ID();
	var actionUrl   = "https://print.logisall.com/ReportLoader/execution.go?projectName="+rptProjectName+"&programName="+rptProgramName+"&pdfFileName="+pdfFileName+"&urlType="+urlType;
	var urlVal  = "http://"+urlType+".logisall.co.kr/OZReport/"+pdfFileName+".asp?ExportPath=D:/Report/"+rptProjectName+"/"+userId+"/"+param;
    var urlRep  = "/../../../Report/"+userId+"/"+pdfFileName+".pdf";

    document.getElementById("pdfView").innerHTML=''; //div pdfView 영역 초기화
	jQuery("#LOADING").modal('show'); //LOADING On

	jQuery.ajax({url:actionUrl
            ,dataType:"jsonp"
            ,data:{"urlVal":encodeURI(urlVal), "userId":userId}
            ,type:"POST"
            ,error: function(rslt){
			    jQuery("#LOADING").modal("hide");    //LOADING Off
			    alert("ERROR : " + rslt.data.success);
		    }
            ,success: function(rslt){
                //alert("O : " + rslt.data.success);
                if(rslt.data.success == "fail"){
                    alert("런타임오류: OZReport 생성 되지 않습니다.(30sec)");
                    return false;
                }
                //report 뷰어 import
                var tax_url = "/Report/"+userId+"/"+pdfFileName+".pdf?timeTemp="+((new Date()).getTime())+"#zoom=100";
                document.getElementById("pdfView").innerHTML='<object data='+tax_url+' type="application/pdf" style="width:100%;height:100%;"></object>';
                jQuery("#pdfDescText").val("PDF Viewer Text !!");
                jQuery('#btnREPORT_DOWN').css('display','block');
                //jQuery('#btnREPORT_DOWN').attr('target','_blank');
                jQuery('#btnREPORT_DOWN').attr('href',''+urlRep+'');
                console.log("hide");
                jQuery("#LOADING").modal('hide');    //LOADING Off

////////			//report 뷰어 import IE & Chrome 분기
////////			if ( pf_getUserIE_FLG() == "Y")
////////            {
////////                window.open(tax_url, 'POP_PDF', 'toolbar=no,scrollbars=no,resizable=no,top=290,left=240,width=1000,height=600');
////////            } else {
////////                document.getElementById("pdfView").innerHTML='<object data='+tax_url+' type="application/pdf" style="width:100%;height:100%;"></object>';
////////            }

                //div zIndex 설정
                //document.getElementById("ERROR_BOX").style.zIndex="100";
                //document.getElementById("pdfView").style.zIndex="200";
            }
	});
}


////////// PDF 뷰어 실행 프로세스 old
////////function pf_setPDFViwer(pdfFileName, rptProjectName, rptProgramName, param){
//////////    alert("!!!");
////////	var rptProcessRunUrl = "http://ucps.logisall.co.kr/OZReport/"+ pdfFileName +".asp?ExportPath=D:/Report/"+ rptProjectName +"/"+ rptProgramName +"/"+ param;
////////    jQuery("#projectName").val(rptProjectName);
////////	jQuery("#programName").val(rptProgramName);
////////	jQuery("#pdfFileName").val(pdfFileName);
////////	jQuery("#urlVal").val(rptProcessRunUrl);
////////
////////	//load창
////////	var openW = "300";
////////	var openH = "100";
////////    setW=screen.availWidth;
////////    setH=screen.availHeight;
////////
////////	var px=(setW-openW)/2;
////////    var py=(setH-openH)/2;
////////	window.open('','newWin','width='+openW+' height='+openH+' status=no location=no top='+py+' left='+px);
////////    rptProcessRun.target = "newWin";
////////	rptProcessRun.submit();
////////
////////	//report 뷰어 import
////////	var tax_url = "http://1.209.94.143:8092/Report/"+rptProjectName+"/"+rptProgramName+"/"+pdfFileName+".pdf?timeTemp="+((new Date()).getTime())+"#zoom=100";
////////	document.getElementById("pdfView").innerHTML='<object data='+tax_url+' type="application/pdf" style="width:100%;height:100%;"></object>';
////////	jQuery("#pdfDescText").val("PDF Viewer Text !!");
////////}


// PDF 뷰어 실행 프로세스 popup new
function pf_setPDFViwerPop(pdfFileName, rptProjectName, rptProgramName, param, urlType){
	var userId = pf_getUserLOGIN_ID();
	var actionUrl = "http://1.209.94.143:8091/ReportLoader/execution.go?projectName="+rptProjectName+"&programName="+rptProgramName+"&pdfFileName="+pdfFileName+"&urlType="+urlType;
	var urlVal = "http://"+urlType+".logisall.co.kr/OZReport/"+pdfFileName+".asp?ExportPath=D:/Report/"+rptProjectName+"/"+userId+"/"+param;
	jQuery("#LOADING").modal('show'); //LOADING On

    jQuery.ajax({url:actionUrl,dataType:"jsonp",data:{"urlVal":encodeURI(urlVal), "userId":userId},type:'POST',
		error: function(rslt){alert("ERROR : " + rslt.data.success);},
		success: function(rslt){
			if(rslt.data.success == "fail"){alert("런타임오류: OZReport 생성 되지 않습니다.(30sec)");return false;}
			//report 뷰어 import
			var tax_url = "http://1.209.94.143:8092/Report/"+rptProjectName+"/"+userId+"/"+pdfFileName+".pdf?timeTemp="+((new Date()).getTime())+"#zoom=100";
			window.open(tax_url, 'POP_PDF', 'toolbar=no,scrollbars=no,resizable=no,top=100,left=50,width=1000,height=700');

            jQuery("#LOADING").modal('hide'); //LOADING Off
		}
	});
}

// PDF 뷰어 실행 프로세스 popup new SSL
function pf_setPDFViwerPopSSL(pdfFileName, rptProjectName, rptProgramName, param, urlType){
	var userId = pf_getUserLOGIN_ID();
	var actionUrl = "https://print.logisall.com/ReportLoader/execution.go?projectName="+rptProjectName+"&programName="+rptProgramName+"&pdfFileName="+pdfFileName+"&urlType="+urlType;
	var urlVal = "http://"+urlType+".logisall.co.kr/OZReport/"+pdfFileName+".asp?ExportPath=D:/Report/"+rptProjectName+"/"+userId+"/"+param;
    var urlRep  = "/../../../Report/"+userId+"/"+pdfFileName+".pdf";
	jQuery("#LOADING").modal('show'); //LOADING On

    jQuery.ajax({url:actionUrl,dataType:"jsonp",data:{"urlVal":encodeURI(urlVal), "userId":userId},type:'POST',
		error: function(rslt){alert("ERROR : " + rslt.data.success);},
		success: function(rslt){
			if(rslt.data.success == "fail"){alert("런타임오류: OZReport 생성 되지 않습니다.(30sec)");return false;}
			//report 뷰어 import
			var tax_url = "http://1.209.94.143:8092/Report/"+rptProjectName+"/"+userId+"/"+pdfFileName+".pdf?timeTemp="+((new Date()).getTime())+"#zoom=100";
			window.open(urlRep, 'POP_PDF', 'toolbar=no,scrollbars=no,resizable=no,top=100,left=50,width=1000,height=700');

            jQuery("#LOADING").modal('hide'); //LOADING Off
		}
	});
}

////////위에 pf_setPDFViwerPop 함수와 동일해서 주석처리
////////2017-08-24
////////양정선
////////// PDF 뷰어 실행 프로세스 popup new
////////function pf_setPDFViwerPopChit(pdfFileName, rptProjectName, rptProgramName, param, urlType){
////////	var userId = pf_getUserLOGIN_ID();
////////	var actionUrl = "http://1.209.94.143:8091/ReportLoader/execution.go?projectName="+rptProjectName+"&programName="+rptProgramName+"&pdfFileName="+pdfFileName+"&urlType="+urlType;
////////	var urlVal = "http://"+urlType+".logisall.co.kr/OZReport/"+pdfFileName+".asp?ExportPath=D:/Report/"+rptProjectName+"/"+userId+"/"+param;
////////	jQuery("#LOADING").modal('show'); //LOADING On
////////
////////    jQuery.ajax({url:actionUrl,dataType:"jsonp",data:{"urlVal":encodeURI(urlVal), "userId":userId},type:'POST',
////////		error: function(rslt){alert("ERROR : " + rslt.data.success);},
////////		success: function(rslt){
////////			if(rslt.data.success == "fail"){alert("런타임오류: OZReport 생성 되지 않습니다.(30sec)");return false;}
////////			//report 뷰어 import
////////			var tax_url = "http://1.209.94.143:8092/Report/"+rptProjectName+"/"+userId+"/"+pdfFileName+".pdf?timeTemp="+((new Date()).getTime())+"#zoom=100";
////////			window.open(tax_url, 'POP_PDF', 'toolbar=no,scrollbars=no,resizable=no,top=100,left=50,width=1000,height=700');
////////
////////            jQuery("#LOADING").modal('hide'); //LOADING Off
////////		}
////////	});
////////}


////////
////////// PDF 뷰어 실행 프로세스 popup old
////////function pf_setPDFViwerPop(pdfFileName, rptProjectName, rptProgramName, param){
//////////    alert("!!!");
////////	var rptProcessRunUrl = "http://ucps.logisall.co.kr/OZReport/"+ pdfFileName +".asp?ExportPath=D:/Report/"+ rptProjectName +"/"+ rptProgramName +"/"+ param;
////////    jQuery("#projectName").val(rptProjectName);
////////	jQuery("#programName").val(rptProgramName);
////////	jQuery("#urlVal").val(rptProcessRunUrl);
////////
////////	//load창
////////	var openW = "300";
////////	var openH = "100";
////////    setW=screen.availWidth;
////////    setH=screen.availHeight;
////////
////////	var px=(setW-openW)/2;
////////    var py=(setH-openH)/2;
////////	window.open('','newWin','width='+openW+' height='+openH+' status=no location=no top='+py+' left='+px);
////////    rptProcessRun.target = "newWin";
////////	rptProcessRun.submit();
////////
////////	//report 뷰어 import
////////	var tax_url = "http://1.209.94.143:8092/Report/"+rptProjectName+"/"+rptProgramName+"/"+pdfFileName+".pdf?timeTemp="+((new Date()).getTime())+"#zoom=100";
//////////	document.getElementById("pdfView").innerHTML='<object data='+tax_url+' type="application/pdf" style="width:100%;height:100%;"></object>';
//////////	jQuery("#pdfDescText").val("PDF Viewer Text !!");
////////    window.open(tax_url, 'POP_PRD', 'toolbar=no,scrollbars=no,resizable=no,top=200,left=150,width=1000,height=600');
////////}


//PDF View page 초기화 삭제 프로세스
//report 삭제 리스너
function pf_initPDFViwePage(rptProjectName, rptProgramName, pdfFileName){
    // 20191016 박종민 http 블럭 처리로 인한 오류예외 처리
//	var userId = pf_getUserLOGIN_ID();
//	jQuery.ajax({url:"http://1.209.94.143:8091/ReportLoader/initdelete.go?projectName="+rptProjectName+"&programName="+rptProgramName+"&pdfFileName="+pdfFileName+"&userId="+userId,
//		dataType:"jsonp",type:'GET',
//		success: function(rslt){},
//		error: function(rslt){}
//	});
}

// DatePicker show for Click event
function pf_showSpdDatepicker(row, col) {

    var dateString="";

    jQuery("#spdRow").val(row);
    jQuery("#spdCol").val(col);

    var cellRect = spdSheet.getActiveSheet().getCellRect(row, col);
    var hearRowCnt = spdSheet.getActiveSheet().getRowCount(GC.Spread.Sheets.SheetArea.colHeader);

    if (spdSheet.getActiveSheet().getValue(row, col-1) != null)
        dateString  = replaceAll(spdSheet.getActiveSheet().getValue(row, col-1), "-", "");
    else
        dateString  = getNowDate();

    var year        = dateString.substring(0,4);
    var month       = dateString.substring(4,6);
    var day         = dateString.substring(6,8);

    jQuery('#spdDatePannel').datepicker('setDate', new Date(year, month-1, day));
    jQuery("#spdDatePannel").css("display", "inline");

    var rectDefault = cellRect.y - ((hearRowCnt-1) * 31);
    if (cellRect.y > 200)
    {jQuery("#spdDatePannel").css("top",rectDefault-10);}else{jQuery("#spdDatePannel").css("top",rectDefault+214);}

    jQuery("#spdDatePannel").css("left",cellRect.x+35);
    jQuery("#spdRow").val(row);
    jQuery("#spdCol").val(col);
}

// DatePicker show for Click event
function pf_showSpdMonthPicker(row, col) {

    var dateString="";

    jQuery("#spdRow").val(row);
    jQuery("#spdCol").val(col);

    var cellRect = spdSheet.getActiveSheet().getCellRect(row, col);
    var hearRowCnt = spdSheet.getActiveSheet().getRowCount(GC.Spread.Sheets.SheetArea.colHeader);

    if (spdSheet.getActiveSheet().getValue(row, col-1) != null)
        dateString  = replaceAll(spdSheet.getActiveSheet().getValue(row, col-1), "-", "");
    else
        dateString  = getNowYYYYMM();

    var year        = dateString.substring(0,4);
    var month       = dateString.substring(4,6);
    var day         = dateString.substring(6,8);

    jQuery('#spdDatePannel').datepicker('setDate', new Date(year, month, day));
    jQuery("#spdDatePannel").css("display", "inline");

    var rectDefault = cellRect.y - ((hearRowCnt-1) * 31);
    if (cellRect.y > 200)
    {jQuery("#spdDatePannel").css("top",rectDefault-10);}else{jQuery("#spdDatePannel").css("top",rectDefault+214);}

    jQuery("#spdDatePannel").css("left",cellRect.x+35);
    jQuery("#spdRow").val(row);
    jQuery("#spdCol").val(col);
}

// DatePicker show for Click event
function pf_showSpdDatepicker_DTL(row, col) {
    jQuery("#spdRow").val(row);
    jQuery("#spdCol").val(col);

    var cellRect = spdSheetDtl.getActiveSheet().getCellRect(row, col);
    var hearRowCnt = spdSheetDtl.getActiveSheet().getRowCount(GC.Spread.Sheets.SheetArea.colHeader);
    var dateString  = replaceAll(spdSheetDtl.getActiveSheet().getValue(row, col-1), "-", "");
    var year        = dateString.substring(0,4);
    var month       = dateString.substring(4,6);
    var day         = dateString.substring(6,8);
    jQuery('#spdDatePannel').datepicker('setDate', new Date(year, month-1, day));
    jQuery("#spdDatePannel").css("display", "inline");

    var rectDefault = cellRect.y - ((hearRowCnt-1) * 31);
    if (cellRect.y > 200)
    {jQuery("#spdDatePannel").css("top",rectDefault-10);}else{jQuery("#spdDatePannel").css("top",rectDefault+490);}

    jQuery("#spdDatePannel").css("left",cellRect.x+35);

}

// DatePicker show for Click event
function pf_showSpdDatepicker_SUB(row, col) {

    jQuery("#spdRow_SUB").val(row);
    jQuery("#spdCol_SUB").val(col);

    var dateString  = replaceAll(spdSheet_SUB.getActiveSheet().getValue(row, col-1), "-", "");
    var year        = dateString.substring(0,4);
    var month       = dateString.substring(4,6);
    var day         = dateString.substring(6,8);

    jQuery('#spdDatePannel_SUB').datepicker('setDate', new Date(year, month-1, day));

    var hearRowCnt = spdSheet_SUB.getActiveSheet().getRowCount(GC.Spread.Sheets.SheetArea.colHeader);
    var cellRect = spdSheet_SUB.getActiveSheet().getCellRect(row, col);
    var rectDefault = cellRect.y - ((hearRowCnt-1) * 31);
    var spdList_top = jQuery("#spdList2");
    var topPosition = spdList_top.position();

    jQuery("#spdDatePannel_SUB").css("display", "inline");
    jQuery("#spdDatePannel_SUB").css("top",topPosition.top+rectDefault+344);
    jQuery("#spdDatePannel_SUB").css("left",cellRect.x+35);

}

// Left Side Menu User Emblem Image Setup
function pf_userEmImageSet() {
    var cstType = pf_getUserCST_TYP();
	var rtnPngVal = "user_EMP.png"; // 기본

	if(pf_getUserLOGIN_ID() == "999999") {
		rtnPngVal = "user_999.png";
	} else {
		switch(cstType){
		case MC_CST_TYP_MAK:
			rtnPngVal = "user_MAK.png";
			break;
		case MC_CST_TYP_DEP:
			rtnPngVal = "user_DEP.png";
			break;
		case MC_CST_TYP_TRN:
			rtnPngVal = "user_TRN.png";
			break;
		case MC_CST_TYP_CTR:
			rtnPngVal = "user_CTR.png";
			break;
		case MC_CST_TYP_ADM:
			rtnPngVal = "user_ADM.png";
			break;
		}
	}
	return rtnPngVal;
}
//***************
//***************
// 숫자(양수)만 입력 해야하는 셀의 로스트 포커스 이벤트 숫자 아닌 문자, "" 값, null값 을 "0"으로 대체
function getNumber(args) {
    var sheet = args.sheet, row = args.row, col = args.col;
    var strTmp = "";
    var regNumber = /^[0-9]*jQuery/;     // 숫자만인지 체크하는 정규식
    if (sheet.getValue(row, col) == null) {sheet.setValue(row, col, "0");}
    else {
        strTmp = sheet.getValue(row, col);
        if (gf_Null(strTmp,"") == "") {
            sheet.setValue(row, col, "0");
        };
        if(!regNumber.test(strTmp)) {
            sheet.setValue(row, col, "0");
        }
    }
}

//***************
//***************
// 모든 숫자 입력 해야하는 셀의 로스트 포커스 이벤트 숫자 아닌 문자, "" 값, null값 을 "0"으로 대체
function getNumberALL(args) {
    var sheet = args.sheet, row = args.row, col = args.col;
    var strTmp = "";
    var regNumber = /^[-]?\d*jQuery/g;
    if (sheet.getValue(row, col) == null) {sheet.setValue(row, col, "0");}
    else {
        strTmp = sheet.getValue(row, col);
        if (gf_Null(strTmp,"") == "") {
            sheet.setValue(row, col, "0");
        };
        if(!regNumber.test(strTmp)) {
            sheet.setValue(row, col, "0");
        }
    }
}

function getNumberPasted(sheet, row, col) {
    var strTmp = "";
    var regNumber = /^[0-9]*jQuery/;     // 숫자만인지 체크하는 정규식
    if (sheet.getValue(row, col) == null) {sheet.setValue(row, col, "0");}
    else {
        strTmp = sheet.getValue(row, col);
        if (gf_Null(strTmp,"") == "") {
            sheet.setValue(row, col, "0");
        };
        if(!regNumber.test(strTmp)) {
            sheet.setValue(row, col, "0");
        }
    }
}
// 입력 값 "" 값, null값 을 "0"으로 대체
function getNumber_valueOnly(val) {
    var strTmp = "";
    var regNumber = /^[0-9]*jQuery/;     // 숫자만인지 체크하는 정규식
    if (val == null) {return 0;}
    else {strTmp = val;if (gf_Null(strTmp,"") == "") {return 0;}; if(!regNumber.test(strTmp)) {return 0;}};
}
// 입력 값 "" 값, null값 을 "0"으로 대체
function getNumber_ALL(val) {
    var strTmp = "";
    var regNumber = /^[-]?\d*jQuery/g;     // 숫자만인지 체크하는 정규식
    if (val == null) {return "0";}
    else {
        strTmp = val;
        if (gf_Null(strTmp,"") == "") {
            return "0";
        };
        if(!regNumber.test(strTmp)) {
            return "0";
        }
        return strTmp;
    };
}

// 해당 셀 전화번호 체크
function getTelNumber(args) {
    var sheet = args.sheet, row = args.row, col = args.col;
    var intTmp = sheet.getValue(row, col);
    alert(intTmp);
    var strTmp = "";
    if(!intTmp.match(/^\d[\d\s]+\djQuery/)){
        sheet.setValue(row, col, "0");
    } else {
        strTmp = "0" + intTmp.toString();
        alert(strTmp);
        sheet.setValue(row, col, "0"+strTmp);
    }
}


//***************
//2017-04-10
// 합계라인 TOTROWPOP_ZIP 팝업 필요사항
// pobjSpdList : 그리드
// pobjROW_NUM : 합계 넘버
// pobjSPAN_COL : 스팬 컬럼 갯수
// pstrTitle : 컬럼 타이틀
// pstrBgColor : 배경색
function ps_setSpreadTOT_ROW(pstrSpdList, pstrSPAN_COL, pstrTitle, pstrBgColor) {
    var nRow = pstrSpdList.getRowCount();
	pstrSpdList.addRows(nRow + 1 ,1);

    pstrSpdList.setCellType(nRow, 0, null);
    pstrSpdList.setValue(nRow, 1,pstrTitle);
    ps_setSpreadForeColor_Cell(pstrSpdList, nRow, 0, pstrBgColor);
    ps_setSpreadCell_Span(pstrSpdList, nRow, 1, 1, pstrSPAN_COL);
    ps_setSpreadBackColor_Row(pstrSpdList, nRow, pstrBgColor);

    pstrSpdList.frozenTrailingRowCount(1);
}

//***************
//2017-04-10
//스프레드 컬럼 합계 화면 변경용
function ps_setSpdRtnColSum(pstrSpdList, pstrCol, pstrTotFlg) {
	var tmpColSum = 0;
    var loopCnt = 0;
    if (pstrTotFlg = "Y") {loopCnt = pstrSpdList.getRowCount() - 1;} else {loopCnt = pstrSpdList.getRowCount();};
    for(i = 0; i < loopCnt; i++) {tmpColSum = tmpColSum + Number(pstrSpdList.getValue(i, pstrCol));}
    return tmpColSum;
}

//***************
//2017-04-19
//그리드 컬럼 cell의 값이 copy & setValue 일 경우 (edit keyIn이 아닐 경우)
//그리드 값이 변경 될 경우 체크박스 true 셋팅 로직
function ps_cellChanged_setChkBox(args, nCol) {
	var sheet = args.sheet;
	if(args.col === nCol && sheet.getValue(args.row, args.col) != true && args.propertyName === "value"){
		sheet.setValue(args.row, 0, 1, GC.Spread.Sheets.SheetArea.viewport);
		ps_spdListChkSetup(args.row, "in"); //arrMC_SPD_CHK_SET seq추가
	}
}

//***************
//2018-02-22
//스프레드 컬럼 합계
//ps_setSpreadTOT_ROW 처리후, 합계처리컬럼을 정의한다.
function ps_setSpreadCOL_SUM(pobjspdList, pstrColumn, pstrTotFlg) {
    pobjspdList.setValue((pobjspdList.getRowCount() - 1), pstrColumn, ps_setSpdRtnColSum(pobjspdList, pstrColumn, pstrTotFlg));
}
//***************
//2018-02-22
//스프레드 컬럼 합계를 ps_setSpreadCOL_FormulaSUM처리한다.
function ps_setSpreadCOL_FormulaSUM(pobjspdList, pstrColumn, pstrFormulaParameter) {
    //합계'=SUM(A1:A5)','=SUM(A:A)'
    pobjspdList.setFormula((pobjspdList.getRowCount() - 1), pstrColumn, pstrFormulaParameter);
}















//***************
// 2017-06-12
// 양정선
// * Return a CSV string from an array of json object
// *
// * @method JSONtoCSV
// * @param {Object} jsonArray an array of json object
// * @param {String} [delimiter=;] delimiter
// * @param {String} [dateFormat=ISO] dateFormat if a date is detected
// * @return {String} Returns the CSV string
//
// 사용법(Display)
//      //Create Object
//      var items = [
//        { name: "Item 1", color: "Green1", size: "X-Large" },
//        { name: "Item 2", color: "Green2", size: "X-Large" },
//        { name: "Item 3", color: "Green3", size: "X-Large" }];
//      //Convert Object to JSON
//      var jsonObject = JSON.stringify(items);
//      //DISPLAY
//      jQuery('#json').text(jsonObject);
// 추가사항 이것을 CSV처리한다.
//***************
function JSONtoCSV(jsonArray, delimiter, dateFormat){
	dateFormat = dateFormat || 'YYYY-MM-DDTHH:mm:ss Z'; // ISO
	delimiter = delimiter || ';' ;

	var body = '';
	// Entete
	var keys = _.map(jsonArray[0], function(num, key){ return key; });
	body += keys.join(delimiter) + '\r\n';
	// Data
	for(var i=0; i<jsonArray.length; i++){
		var item = jsonArray[i];
		for(var j=0; j<keys.length; j++){
			var obj = item[keys[j]] ;
			if (_.isDate(obj)) {body += moment(obj).format(dateFormat) ;} else {body += obj;}
			if (j < keys.length-1) {body += delimiter;}
		}
		body += '\r\n';
	}
	return body;
}

//***************
//Login check
function pf_Login_Limit_Check(objArr) {
    if (objArr.length > 0) {
        if (objArr[0].STATUS == 'ERROR') {
            jQuery('#LOADING').modal('hide');
            jQuery('#ERROR_BOX_HEAD').html('<strong>접속에러 : '+objArr[0].STATUS+'</strong>');
            jQuery('#ERROR_BOX_BODY').html('<strong> ' + objArr[0].MSG +'</strong>');
            jQuery('#ERROR_BOX').modal('show');
            return false;
        }
    }
    return true;
}



//////***************
//////filedownload_att
////function pf_FileDownLoad_Att(pstrMode, pstrSub, pstrFileName) {
////	window.open('./_setup/File_DownLoad.asp?mode='+pstrMode+'&sub='+pstrSub+'&fname='+pstrFileName);
////}

//***************
//fnMakeTableForEXCEL()
//활성화된 스프레드의 값을 읽어 _MAIN.asp의 'FOOTER' frames에 정의 된 spdListTalbe에 테이블을 생성
function pf_MakeTableForEXCEL(pobjspdList) {
    var nHRow = pobjspdList.getRowCount(GC.Spread.Sheets.SheetArea.colHeader);
    var nCol = pobjspdList.getColumnCount();
    var nRow = pobjspdList.getRowCount();
    var htmlText = "";

    htmlText += "<table>";
    for (var curRow = 0; curRow < nHRow ; curRow++) {
        htmlText += "<tr>";
        for (var curCol = 0; curCol < nCol; curCol++) {
            if (pobjspdList.getRange(-1, curCol, -1, 1).visible()==true) {
                htmlText += "<td bgcolor='#E9E9E9'>"+gf_Null(pobjspdList.getText(curRow, curCol,GC.Spread.Sheets.SheetArea.colHeader),'')+"</td>";
            }
        }
        htmlText += "</tr>";
    };
    for (var curRow = 0; curRow < nRow ; curRow++) {
        htmlText += "<tr>";
        for (var curCol = 0; curCol < nCol; curCol++) {
            if (pobjspdList.getRange(-1, curCol, -1, 1).visible()==true) {
                htmlText += "<td>"+pobjspdList.getText(curRow, curCol)+"</td>";
            }
        }
        htmlText += "</tr>";
    };
    htmlText += "</table>";
    //jQuery("#spdListTable",parent.frames['FOOTER'].document).html(htmlText);
	jQuery("#spdListTable").html(htmlText);
}

function pf_ExcelReportExport(name, pobjSheet){
	var excelIo = new GC.Spread.Excel.IO();
	var fileName = name+".xlsx";
	var serializationOption = {
			includeBindingSource : true,
			ignoreStyle : false,
			ignoreFormula : true,
			rowHeadersAsFrozenColumns : false,
			columnHeadersAsFrozenRows : true,
			includeAutoMergedCells : false
	}
	
	var tempJson = pobjSheet.toJSON(serializationOption);
	delete tempJson.sheets.Sheet1.spans;
	
	var columnDataArray = tempJson.sheets.Sheet1.data.columnDataArray;
	var rowDataArray = tempJson.sheets.Sheet1.data.rowDataArray;
	var dataArray = tempJson.sheets.Sheet1.data.dataTable;
	var defaultDataNode = tempJson.sheets.Sheet1.data.defaultDataNode;
	
    tempJson.sheets.Sheet1.isProtected = false;    
	
	// dataArray style = rowDataArray + columnDataArray + dataArray style 
	for(var i = 0 ; i < rowDataArray.length ; i++){
		if(rowDataArray[i] != null && rowDataArray[i].style != null){
			for(var j = 0 ; j < columnDataArray.length ; j++){
				if(dataArray[i][j] != null){
					if(dataArray[i][j].style != null){
						if(columnDataArray[j] != null && columnDataArray[j].style != null){
							Object.assign(dataArray[i][j].style, columnDataArray[j].style);
						}
						Object.assign(dataArray[i][j].style, rowDataArray[i].style);
					}
					else{
						dataArray[i][j].style = {};
						Object.assign(dataArray[i][j].style,defaultDataNode.style);
						if(columnDataArray[j] != null && columnDataArray[j].style != null){
							Object.assign(dataArray[i][j].style, columnDataArray[j].style);
						}
						Object.assign(dataArray[i][j].style, rowDataArray[i].style);
					}
				}	
			}
		}
	}
	var json = JSON.stringify(tempJson);
	excelIo.save(json,function(blob){
		saveAs(blob,fileName);
	}, function(e){
		console.log(e);
	});
}


function pf_ExcelReportExportCallback(name, pobjSheet, callback){
	var excelIo = new GC.Spread.Excel.IO();
	var fileName = name+".xlsx";
	var serializationOption = {
			includeBindingSource : true,
			ignoreStyle : false,
			ignoreFormula : true,
			rowHeadersAsFrozenColumns : false,
			columnHeadersAsFrozenRows : true,
			includeAutoMergedCells : false
	}
	
	var tempJson = pobjSheet.toJSON(serializationOption);
	delete tempJson.sheets.Sheet1.spans;
	
	var columnDataArray = tempJson.sheets.Sheet1.data.columnDataArray;
	var rowDataArray = tempJson.sheets.Sheet1.data.rowDataArray;
	var dataArray = tempJson.sheets.Sheet1.data.dataTable;
	var defaultDataNode = tempJson.sheets.Sheet1.data.defaultDataNode;
	
	// dataArray style = rowDataArray + columnDataArray + dataArray style 
	for(var i = 0 ; i < rowDataArray.length ; i++){
		if(rowDataArray[i] != null && rowDataArray[i].style != null){
			for(var j = 0 ; j < columnDataArray.length ; j++){
				if(dataArray[i][j] != null){
					if(dataArray[i][j].style != null){
						if(columnDataArray[j] != null && columnDataArray[j].style != null){
							Object.assign(dataArray[i][j].style, columnDataArray[j].style);
						}
						Object.assign(dataArray[i][j].style, rowDataArray[i].style);
					}
					else{
						dataArray[i][j].style = {};
						Object.assign(dataArray[i][j].style,defaultDataNode.style);
						if(columnDataArray[j] != null && columnDataArray[j].style != null){
							Object.assign(dataArray[i][j].style, columnDataArray[j].style);
						}
						Object.assign(dataArray[i][j].style, rowDataArray[i].style);
					}
				}	
			}
		}
	}
	var json = JSON.stringify(tempJson);
	excelIo.save(json,function(blob){
		saveAs(blob,fileName);
		
		callback();
		
	}, function(e){
		console.log(e);
	});
}



function pf_ExcelReport(name){
	var urls = '/Excel/downLoadExcelReport.action';
	
	var tempForm = document.createElement('form');
	tempForm.setAttribute("method","post");
	tempForm.setAttribute("action",urls);
	tempForm.setAttribute("target",'_self');
	
	var table = jQuery("#spdListTable").clone();
	console.log(jQuery("#spdListTable"));
	var list =  pf_getMap(table);
	
	var head = list["Head"];
	var body = list["Body"];
	
	
	var jsonData = {
		"Title" : name,
		"Head" : head,
		"Body" : body
	}
	
	var tempData = document.createElement('input');
	tempData.setAttribute("type","hidden");
	tempData.setAttribute("name","JsonData");
	tempData.setAttribute("value",JSON.stringify(jsonData));
	tempForm.appendChild(tempData);
	var isHeaderSpace = document.createElement('input');
	isHeaderSpace.setAttribute("type","hidden");
	isHeaderSpace.setAttribute("name","isHeaderSpace");
	isHeaderSpace.setAttribute("value",true);
	tempForm.appendChild(isHeaderSpace);
	
	document.body.appendChild(tempForm);
	tempForm.submit();
}

function pf_getMap(table){
	var list = {};
	var head = [];
	var body = [];
	
	var headcount = 1;
	var headc = 0;
	var tempHead = [];
	var tempc = 0;
	table.find("tr").each(function (i,a){
		if(i == 0){
			jQuery(this).find("td").each(function (j,a){
				if(jQuery(this).attr('colspan') > 1){
					for (var i = 0 ; i < jQuery(this).attr('colspan') ; i++){
						tempHead[tempc++] = headc;
						head[headc++] = "";
					}
				}
				else{
					head[headc++] = jQuery(this).text();
				}
				if (jQuery(this).attr('rowspan') > 1){
					if(headcount < jQuery(this).attr('rowspan')){
						headcount = jQuery(this).attr('rowspan');
					}
				}
			});
		}
		else if(i < headcount){
			jQuery(this).find("td").each(function (j,a){
				head[tempHead[j]] = jQuery(this).text();
			});
		}
		else{
			var tempBody = [];
			jQuery(this).find("td").each(function (j,a){
				tempBody[j] = jQuery(this).text();
			});
			body[i-headcount] = tempBody;
		}
	});
	
	list["Head"] = head;
	list["Body"] = body;
	
	return list;
}

//***************
//fnExcelReport(파일명)
//_MAIN.asp의 'FOOTER' frames에 정의 된 spdListTalbe의 HTML값을 읽어 엑셀파일 생성

function pf_ExcelReport_old(name) {
    var tab_text = '';
    tab_text = tab_text + '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
    tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
    tab_text = tab_text + '<x:Name>Sheet1</x:Name>';
    tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
    tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
    tab_text = tab_text + "<table border='1px'>";
    //var exportTable = jQuery("#spdListTable",parent.frames['FOOTER'].document).clone();
	var exportTable = jQuery("#spdListTable").clone();
    exportTable.find('input').each(function (index, elem) { jQuery(elem).remove(); });
    tab_text = tab_text + exportTable.html();
    tab_text = tab_text + '</table></body></html>';
    var data_type = 'data:application/vnd.ms-excel';
    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    var fileName = name+ '.xls';
    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
        if (window.navigator.msSaveBlob) {
            var blob = new Blob(["\ufeff"+tab_text], {
                type: "application/csv;charset=utf-8;"
            });
            navigator.msSaveBlob(blob, fileName);
        }
    } else {
        var blob2 = new Blob(["\ufeff"+tab_text], {
            type: "application/csv;charset=utf-8;"
        });
        var filename = fileName;
        var elem = window.document.createElement('a');
        elem.href = window.URL.createObjectURL(blob2);
        elem.download = filename;
        document.body.appendChild(elem);
        elem.click();
        document.body.removeChild(elem);
    }
}


//*****************
//2017-08-24
//window.open
//팝업창통합
//*****************
function pf_windowopen(pstrmode, pobjfrm) {

    if (pstrmode == "ITEM_INF") {
        var winPOPUP = window.open("", "ITEM_INF", "top=200, left=150, width=1000, height=700, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_MRO_ITEM_INF/";
//        void(0);
    };
    if (pstrmode == "PRD_INF") {
        var winPOPUP = window.open("", "PRD_INF", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_PRD_INF/";
//        void(0);
    };
    if (pstrmode == "ezHELP") {
        window.open("https://939.co.kr/ezHelpClient.exe","","");
    };
    if (pstrmode == "POPUP_MAP") {
        window.open("","POPUP_MAP","top=200, left=150, width=800, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=yes");
        pobjfrm.action = "/_POPUP/_MAP/";
        pobjfrm.method = "POST";
        pobjfrm.target = "POPUP_MAP";
        pobjfrm.submit();
    };
    if (pstrmode == "CTR_CHIT_NUM") {
        window.open("","CTR_CHIT_NUM","top=200, left=150, width=850, height=600, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        pobjfrm.method = "POST";
        pobjfrm.target = "CTR_CHIT_NUM";
        pobjfrm.action = "http://npps.logisall.co.kr/Files/IMG180SWU/";
        pobjfrm.submit();
    };
    if (pstrmode == "TRN_CHIT_NUM") {
        window.open("","TRN_CHIT_NUM","top=200, left=150, width=850, height=600, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        pobjfrm.method = "POST";
        pobjfrm.target = "TRN_CHIT_NUM";
        pobjfrm.action = "http://npps.logisall.co.kr/Files/IMG160SWU2/";
        pobjfrm.submit();
    };
    if (pstrmode == "POP_ZIP") {
        var winPOPUP = window.open("", "POP_ZIP", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_ZIP/";
    };
    if (pstrmode == "POP_PRD") {
        var winPOPUP = window.open("", "POP_PRD", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_PRD/";
    };
    if (pstrmode == "POP_CST") {
        var winPOPUP = window.open("", "POP_CST", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_CST/";
    };
    if (pstrmode == "POP_CST_ARV") {
        var winPOPUP = window.open("", "POP_CST_ARV", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_CST_ARV/";
    };
    if (pstrmode == "POP_MRO_CST_ARV") {
        var winPOPUP = window.open("", "POP_MRO_CST_ARV", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_MRO_CST_ARV/";
    };
    if (pstrmode == "POP_MRO_CST_ITEM") {
        var winPOPUP = window.open("", "POP_MRO_CST_ITEM", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_MRO_CST_ITEM/";
    };
    if (pstrmode == "POP_MRO_MAD_CST") {
        var winPOPUP = window.open("", "POP_MRO_MAD_CST", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_MRO_MAD_CST/";
    };
    if (pstrmode == "POP_MRO_MAD_ITEM") {
        var winPOPUP = window.open("", "POP_MRO_MAD_ITEM", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_MRO_MAD_ITEM/";
    };
    if (pstrmode == "POP_42004_PRD") {
        var winPOPUP = window.open("", "POP_42004_PRD", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/POP_THIS_ONLYWF/POP_42004_PRD.asp";
    };
    if (pstrmode == "POP_42004_CST") {
        var winPOPUP = window.open("", "POP_42004_CST", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/POP_THIS_ONLYWF/POP_42004_CST.asp";
    };
    if (pstrmode == "POP_CST_CAR_MNG") {
        var winPOPUP = window.open("", "POP_CST_CAR_MNG", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_CST_CAR_MNG/";
    };
    if (pstrmode == "/_pfMEMO/MEMO71001WF/") {
        window.location.href = "/_pfMEMO/MEMO71001WF/";     //쪽지함 바로가기
    };

    if (pstrmode == "POP_SCM_CST") {
        var winPOPUP = window.open("", "POP_SCM_CST", "top=200, left=150, width=1000, height=600, location=no, toolbar=no, menubar=no, scrollbars=no, resizable=no");
        winPOPUP.location.href = "/_POPUP/_SCM_CST/SCM_CST.asp";
    };
    //(간편)입고요청
    if (pstrmode == "/_pfCTR/CTR41003WF/") {
        if (pf_getUserCST_TYP() == "020004") {

            // 입고/반납 요청양식
            var arrREQ_TEMPLATE = JSON.parse(pf_getTemplateGroup());
            var ComboHtml = '<option value=\'0\'>입고/반납요청 양식</option>';
            jQuery.each(arrREQ_TEMPLATE, function(i, value) {
               ComboHtml += '<option value='+arrREQ_TEMPLATE[i].TEMPLATE_SEQ+'>'+arrREQ_TEMPLATE[i].TITLE+'</option>';
            });
            jQuery('#STEMPLATE_TYP').html(ComboHtml);


            // 유형수량화면
            var arrCST_PRD = JSON.parse(pf_getUserCST_PRD());
            var ListHtml = '';
            ListHtml += '<tbody><tr>';
            ListHtml += '  <th style=\'text-align: center;\' width=\'20%\'>유형</th>';
            ListHtml += '  <th style=\'text-align: center;\' width=\'20%\'>1PLT</th>';
            ListHtml += '  <th style=\'text-align: center;\' width=\'30%\'>세척수량</th>';
            ListHtml += '  <th style=\'text-align: center;\' width=\'30%\'>일반수량</th>';
            ListHtml += '</tr>';

            jQuery.each(arrCST_PRD, function(i, value) {
            ListHtml += '<tr>';
            ListHtml += '  <td align=\'center\'>';
            ListHtml += '      <input name=\'SPRD_COD\' class=\'form-control input-sm text-uppercase\' type=\'text\' maxlength=\'6\' style=\'width:100px;text-align:center;\' value='+arrCST_PRD[i].PRD_COD+' disabled>';
            ListHtml += '  </td>';
            ListHtml += '  <td align=\'center\'>';
            ListHtml += '      <input name=\'SPLT_QTY\' class=\'form-control input-sm text-uppercase\' type=\'text\' maxlength=\'6\' style=\'width:100px;text-align:center;\' value='+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+' disabled>';
            ListHtml += '  </td>';

            ListHtml += '  <td align=\'center\'>';
            ListHtml += '       <table cellspacing=\'0\' cellpadding=\'0\' border=0 style=\'width:100%;\'>';
            ListHtml += '           <tr style=\'width:100%;\'>';
            ListHtml += '           <td style=\'width:20%;\' align=\'right\'>';
            ListHtml += '               <button class=\'btn btn-primary btn-sm\' style=\'width:95%;padding-left:0px;padding-right:0px;\' onclick=\'SubQtyPLT('+ i +', '+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+',"S")\' name=\'btnSUBS_QTY_PLT\'>━</button>';
            ListHtml += '           </td>';
            ListHtml += '           <td align=\'center\' style=\'width:60%;\'>';
            ListHtml += '               <input name=\'SSEL_QTY\' class=\'form-control input-sm text-uppercase\' type=\'number\' min=\'0\' onChange=\'javascript:CountPLT();\' style=\'width:100%;text-align:right;\'>';
            ListHtml += '           </td>';
            ListHtml += '           <td style=\'width:20%;\'>';
            ListHtml += '               <button class=\'btn btn-primary btn-sm\' style=\'width:95%;padding-left:0px;padding-right:0px;margin-right:5px;\' onclick=\'AddQtyPLT('+ i +', '+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+',"S")\' name=\'btnADDS_QTY_PLT\'>╋</button>';
            ListHtml += '           </td>';
            ListHtml += '           </tr>';
            ListHtml += '       </table>';
            ListHtml += '  </td>';


            ListHtml += '  <td align=\'center\'>';
            ListHtml += '       <table cellspacing=\'0\' cellpadding=\'0\' border=0 style=\'width:100%;\'>';
            ListHtml += '           <tr style=\'width:100%;\'>';
            ListHtml += '           <td style=\'width:20%;\' align=\'right\'>';
            ListHtml += '               <button class=\'btn btn-primary btn-sm\' style=\'width:95%;padding-left:0px;padding-right:0px;\' onclick=\'SubQtyPLT('+ i +', '+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+',"G")\' name=\'btnSUBG_QTY_PLT\'>━</button>';
            ListHtml += '           </td>';
            ListHtml += '           <td align=\'center\' style=\'width:60%;\'>';
            ListHtml += '               <input name=\'SGEN_QTY\' class=\'form-control input-sm text-uppercase\' type=\'number\' min=\'0\' onChange=\'javascript:CountPLT();\' style=\'width:100%;text-align:right;\'>';
            ListHtml += '           </td>';
            ListHtml += '           <td style=\'width:20%;\'>';
            ListHtml += '               <button class=\'btn btn-primary btn-sm\' style=\'width:95%;padding-left:0px;padding-right:0px;margin-right:5px;\' onclick=\'AddQtyPLT('+ i +', '+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+',"G")\' name=\'btnADDG_QTY_PLT\'>╋</button>';
            ListHtml += '           </td>';
            ListHtml += '           </tr>';
            ListHtml += '       </table>';
            ListHtml += '  </td>';

//            ListHtml += '  <td align=\'center\'>';
//            ListHtml += '      <input name=\'SSEL_QTY\' class=\'form-control input-sm text-uppercase\' type=\'number\' min=\'0\' onChange=\'javascript:CountPLT();\' style=\'width:100px;text-align:right;\'>';
//            ListHtml += '  </td>';
//            ListHtml += '  <td align=\'left\'>';
//            ListHtml += '      <input name=\'SGEN_QTY\' class=\'form-control input-sm text-uppercase\' type=\'number\' min=\'0\' onChange=\'javascript:CountPLT();\' style=\'width:100px;text-align:right;\'>';
//            ListHtml += '  </td>';
            ListHtml += '</tr>';
            });
            // 비고란
            ListHtml += '<tr>';
            ListHtml += '  <td align=\'center\'><b>비 고</b></td>';
            ListHtml += '  <td align=\'left\' colspan=2>';
            ListHtml += '      <input id=\'SETC_DESC\' class=\'form-control input-sm\' type=\'text\'>';
            ListHtml += '  </td>';
            ListHtml += '</tr>';
            ListHtml += '</tbody>';

            jQuery('#SPRD_LIST').html(ListHtml);



            setTimeout(function() {
                jQuery('#SIMPLE_REQ').modal('show');
            }, 100);

        } else {

            // 현업직원 입고요청 창
//            var arrREQ_TEMPLATE = JSON.parse(pf_getTemplateGroup());
//            var ComboHtml = '<option value=\'0\'>입고/반납요청 양식</option>';
//            jQuery.each(arrREQ_TEMPLATE, function(i, value) {
//               ComboHtml += '<option value='+arrREQ_TEMPLATE[i].TEMPLATE_SEQ+'>'+arrREQ_TEMPLATE[i].TITLE+'</option>';
//            });
//            jQuery('#STEMPLATE_TYP').html(ComboHtml);
//            var ListHtml = '';

//            // 유형수량화면
//            var arrCST_PRD = JSON.parse(pf_getUserCST_PRD());
//
//            ListHtml += '<tbody><tr>';
//            ListHtml += '  <th style=\'text-align: center;\' width=\'20%\'>유형</th>';
//            ListHtml += '  <th style=\'text-align: center;\' width=\'20%\'>1PLT</th>';
//            ListHtml += '  <th style=\'text-align: center;\' width=\'30%\'>세척수량</th>';
//            ListHtml += '  <th style=\'text-align: center;\' width=\'30%\'>일반수량</th>';
//            ListHtml += '</tr>';
//
//            jQuery.each(arrCST_PRD, function(i, value) {
//            ListHtml += '<tr>';
//            ListHtml += '  <td align=\'center\'>';
//            ListHtml += '      <input name=\'SPRD_COD\' class=\'form-control input-sm text-uppercase\' type=\'text\' maxlength=\'6\' style=\'width:100px;text-align:center;\' value='+arrCST_PRD[i].PRD_COD+' disabled>';
//            ListHtml += '  </td>';
//            ListHtml += '  <td align=\'center\'>';
//            ListHtml += '      <input name=\'SPLT_QTY\' class=\'form-control input-sm text-uppercase\' type=\'text\' maxlength=\'6\' style=\'width:100px;text-align:center;\' value='+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+' disabled>';
//            ListHtml += '  </td>';
//
//            ListHtml += '  <td align=\'center\'>';
//            ListHtml += '       <table cellspacing=\'0\' cellpadding=\'0\' border=0 style=\'width:100%;\'>';
//            ListHtml += '           <tr style=\'width:100%;\'>';
//            ListHtml += '           <td style=\'width:20%;\' align=\'right\'>';
//            ListHtml += '               <button class=\'btn btn-primary btn-sm\' style=\'width:95%;padding-left:0px;padding-right:0px;\' onclick=\'SubQtyPLT('+ i +', '+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+',"S")\' name=\'btnSUBS_QTY_PLT\'>━</button>';
//            ListHtml += '           </td>';
//            ListHtml += '           <td align=\'center\' style=\'width:60%;\'>';
//            ListHtml += '               <input name=\'SSEL_QTY\' class=\'form-control input-sm text-uppercase\' type=\'number\' min=\'0\' onChange=\'javascript:CountPLT();\' style=\'width:100%;text-align:right;\'>';
//            ListHtml += '           </td>';
//            ListHtml += '           <td style=\'width:20%;\'>';
//            ListHtml += '               <button class=\'btn btn-primary btn-sm\' style=\'width:95%;padding-left:0px;padding-right:0px;margin-right:5px;\' onclick=\'AddQtyPLT('+ i +', '+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+',"S")\' name=\'btnADDS_QTY_PLT\'>╋</button>';
//            ListHtml += '           </td>';
//            ListHtml += '           </tr>';
//            ListHtml += '       </table>';
//            ListHtml += '  </td>';
//
//
//            ListHtml += '  <td align=\'center\'>';
//            ListHtml += '       <table cellspacing=\'0\' cellpadding=\'0\' border=0 style=\'width:100%;\'>';
//            ListHtml += '           <tr style=\'width:100%;\'>';
//            ListHtml += '           <td style=\'width:20%;\' align=\'right\'>';
//            ListHtml += '               <button class=\'btn btn-primary btn-sm\' style=\'width:95%;padding-left:0px;padding-right:0px;\' onclick=\'SubQtyPLT('+ i +', '+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+',"G")\' name=\'btnSUBG_QTY_PLT\'>━</button>';
//            ListHtml += '           </td>';
//            ListHtml += '           <td align=\'center\' style=\'width:60%;\'>';
//            ListHtml += '               <input name=\'SGEN_QTY\' class=\'form-control input-sm text-uppercase\' type=\'number\' min=\'0\' onChange=\'javascript:CountPLT();\' style=\'width:100%;text-align:right;\'>';
//            ListHtml += '           </td>';
//            ListHtml += '           <td style=\'width:20%;\'>';
//            ListHtml += '               <button class=\'btn btn-primary btn-sm\' style=\'width:95%;padding-left:0px;padding-right:0px;margin-right:5px;\' onclick=\'AddQtyPLT('+ i +', '+gf_Null(arrCST_PRD[i].PT_UNIT_QTY,'0')+',"G")\' name=\'btnADDG_QTY_PLT\'>╋</button>';
//            ListHtml += '           </td>';
//            ListHtml += '           </tr>';
//            ListHtml += '       </table>';
//            ListHtml += '  </td>';
//            ListHtml += '</tr>';
//            });
//            // 비고란
//            ListHtml += '<tr>';
//            ListHtml += '  <td align=\'center\'><b>비 고</b></td>';
//            ListHtml += '  <td align=\'left\' colspan=2>';
//            ListHtml += '      <input id=\'SETC_DESC\' class=\'form-control input-sm\' type=\'text\'>';
//            ListHtml += '  </td>';
//            ListHtml += '</tr>';
//            ListHtml += '</tbody>';
//
//            jQuery('#SPRD_LIST').html(ListHtml);



            setTimeout(function() {
                jQuery('#SIMPLE_EMP').modal('show');
            }, 100);



            //window.location.href = "/_pfCTR/CTR41003WF/";
        }
    };
};
//************
//바닥PLT 버튼입력
function AddQtyPLT(row, basQty, prdTyp) {
    var getQty;
    var setQty;
    if (prdTyp=="G") {
        getQty = parseInt(gf_Null(jQuery('input[name=SGEN_QTY]').eq(row).val(),'0'));
        setQty = getQty + parseInt(basQty);
        jQuery('input[name=SGEN_QTY]').eq(row).val(setQty);
    } else {
        getQty = parseInt(gf_Null(jQuery('input[name=SSEL_QTY]').eq(row).val(),'0'));
        setQty = getQty + parseInt(basQty);
        jQuery('input[name=SSEL_QTY]').eq(row).val(setQty);
    }
    CountPLT();
};

function SubQtyPLT(row, basQty, prdTyp) {
    var getQty;
    var setQty;
    if (prdTyp=="G") {
        getQty = parseInt(gf_Null(jQuery('input[name=SGEN_QTY]').eq(row).val(),'0'));
        if (getQty < parseInt(basQty)) {
            setQty = 0;
        } else {
            setQty = getQty - parseInt(basQty);
        }
        jQuery('input[name=SGEN_QTY]').eq(row).val(setQty);
    } else {
        getQty = parseInt(gf_Null(jQuery('input[name=SSEL_QTY]').eq(row).val(),'0'));
        if (getQty < parseInt(basQty)) {
            setQty = 0;
        } else {
            setQty = getQty - parseInt(basQty);
        }
        jQuery('input[name=SSEL_QTY]').eq(row).val(setQty);
    }
    CountPLT();
};
//바닥PLT 버튼입력 종료
//************
//담당자 바닥PLT 버튼입력
function EAddQtyPLT(row, basQty, prdTyp) {
    var getQty;
    var setQty;
    if (prdTyp=="G") {
        getQty = parseInt(gf_Null(jQuery('input[name=EGEN_QTY]').eq(row).val(),'0'));
        setQty = getQty + parseInt(basQty);
        jQuery('input[name=EGEN_QTY]').eq(row).val(setQty);
    } else {
        getQty = parseInt(gf_Null(jQuery('input[name=ESEL_QTY]').eq(row).val(),'0'));
        setQty = getQty + parseInt(basQty);
        jQuery('input[name=ESEL_QTY]').eq(row).val(setQty);
    }
    EmpCountPLT();
};

function ESubQtyPLT(row, basQty, prdTyp) {
    var getQty;
    var setQty;
    if (prdTyp=="G") {
        getQty = parseInt(gf_Null(jQuery('input[name=EGEN_QTY]').eq(row).val(),'0'));
        if (getQty < parseInt(basQty)) {
            setQty = 0;
        } else {
            setQty = getQty - parseInt(basQty);
        }
        jQuery('input[name=EGEN_QTY]').eq(row).val(setQty);
    } else {
        getQty = parseInt(gf_Null(jQuery('input[name=ESEL_QTY]').eq(row).val(),'0'));
        if (getQty < parseInt(basQty)) {
            setQty = 0;
        } else {
            setQty = getQty - parseInt(basQty);
        }
        jQuery('input[name=ESEL_QTY]').eq(row).val(setQty);
    }
    EmpCountPLT();
};
//담당자 바닥PLT 버튼입력 종료
//************
//***************
//파일fileLink
//***************
function pf_FileLink(pstrGUBN) {
    if (pstrGUBN == "MANUAL") {
        //새탭에 해당파일을 호출한다.
        var file_url = "/_file/Manual/wcps_manual_20181023.pdf";
        window.open(file_url, "popup");
    }
    if (pstrGUBN == "CTR") {
        //새탭에 해당파일을 호출한다.
        var file_url = "/_file/Manual/wcps_ctr_20181023.pdf";
        window.open(file_url, "popup");
    }
    if (pstrGUBN == "WINUS") {
        //새탭에 해당파일을 호출한다.
        var file_url = "/_file/Manual/winus_solution(20180810).pdf";
        window.open(file_url, "popup");
    }
    if (pstrGUBN == "EASY") {
        //새탭에 해당파일을 호출한다.
        var file_url = "/_file/Manual/wcps_easy.pdf";
        window.open(file_url, "popup");
    }
    return false;
}

//***************
//SRCP_COL_REQ 간편입고/회수 등록
function pf_setSRCP_PRD_LIST() {
	var InnerHtml = '';

    jQuery("#SREG_PRD_LIST").empty();

    InnerHtml += '<tbody><tr>';
    InnerHtml += '  <th style=\'text-align: center;\' width=\'50%\'>유형</th>';
    InnerHtml += '  <th style=\'text-align: center;\' width=\'50%\'>수량</th>';
    InnerHtml += '</tr>';

    jQuery.ajax({type:"POST",url:"/_pfDEP/DEP33004WF/SRCP_PRD_QRY.asp",
        data:{"COMP_TYPE":pf_getUserCOMP_TYPE(),"CST_COD":jQuery("#SREG_CST_COD").val()}
        ,async : false
        ,dataType:"json",
        success:function(ds) {jQuery.each(ds, function(i, entry){
        InnerHtml += '<tr>';
        InnerHtml += '  <td align=\'center\'>';
        InnerHtml += '      <input name=\'SRCP_PRD_COD\' class=\'form-control input-sm text-uppercase\' type=\'text\' maxlength=\'6\' style=\'width:100px;text-align:center;\' value='+ds[i].PRD_COD+' disabled>';
        InnerHtml += '  </td>';
        InnerHtml += '  <td align=\'center\'>';
        InnerHtml += '      <input name=\'SRCP_QTY\' class=\'form-control input-sm text-uppercase\' type=\'number\' min=\'0\' onChange=\'javascript:CountBPS();\' style=\'width:100px;text-align:right;\'>';
        InnerHtml += '  </td>';
        InnerHtml += '</tr>';
        //jQuery(rtnid_name).val(ds[i].NAME)
            });
        },
        error:function(request, textStatus, errorThrown) {alert("error:"+textStatus);}
    });
    jQuery("#SREG_PRD_LIST").html(InnerHtml);
}

function fnSaveExcelR(objspread, objList,  title){
	var d = new Date();
	var filename = title+"_"+d.getTime() + ".xlsx";

	var nHRow = objList.getRowCount(GC.Spread.Sheets.SheetArea.colHeader);
    var nCol = objList.getColumnCount();
    var nRow = objList.getRowCount();
    var htmlText = "";

    htmlText += "<table>";
    for (var curRow = 0; curRow < nHRow ; curRow++) {
        htmlText += "<tr>";
        for (var curCol = 0; curCol < nCol; curCol++) {
            htmlText += "<td bgcolor='#E9E9E9'>"+gf_Null(objList.getText(curRow, curCol,GC.Spread.Sheets.SheetArea.colHeader),'')+"</td>";
        }
        htmlText += "</tr>";
    };
    for (var curRow = 0; curRow < nRow ; curRow++) {
        htmlText += "<tr>";
        for (var curCol = 0; curCol < nCol; curCol++) {
        	htmlText += "<td>"+objList.getText(curRow, curCol)+"</td>";
        }
        htmlText += "</tr>";
    };
    htmlText += "</table>";
   console.log(htmlText);

   var tab_text = '';
   tab_text = tab_text + '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
   tab_text = tab_text + '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
   tab_text = tab_text + '<x:Name>Sheet1</x:Name>';
   tab_text = tab_text + '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions></x:ExcelWorksheet>';
   tab_text = tab_text + '</x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
   tab_text = tab_text + "<table border='1px'>";
   tab_text = tab_text + htmlText;
   tab_text = tab_text + '</table></body></html>';
   var data_type = 'data:application/vnd.ms-excel';
   var ua = window.navigator.userAgent;
   var msie = ua.indexOf("MSIE ");

   if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
       if (window.navigator.msSaveBlob) {
           var blob = new Blob([tab_text], {
               type: "application/csv;charset=utf-8;"
           });
           console.log(blob);
           navigator.msSaveBlob(blob, fileName);
       }
   } else {
       var blob2 = new Blob([tab_text], {
           type: "application/csv;charset=utf-8;"
       });
       console.log(blob);
       var elem = window.document.createElement('a');
       elem.href = window.URL.createObjectURL(blob2);
       elem.download = filename;
       document.body.appendChild(elem);
       elem.click();
       document.body.removeChild(elem);
   }


}


//***************
//2020-08-26
//Events Bind
function ps_setSpreadEvents_Bind_Multi(pobjspdList, pdbjspdSheet) {
  //Events : ButtonCliecked
  pdbjspdSheet.bind(GC.Spread.Sheets.Events.ButtonClicked, function (sender, args) {
      ps_setSpreadEvents_ButtonClicked(args);
  });
  //Events : ValueChanged
  pobjspdList.bind(GC.Spread.Sheets.Events.ValueChanged, function (sender, args) {
      ps_setSpreadEvents_ValueChanged(args);
  });
  //Events : CellChanged
  pobjspdList.bind(GC.Spread.Sheets.Events.CellChanged, function (sender, args) {
      ps_setSpreadEvents_CellChanged(args);
  });
  //Events : CellClick
  pobjspdList.bind(GC.Spread.Sheets.Events.CellClick, function (sender, args) {
      ps_setSpreadEvents_CellClick(args);
  });
  //Events : CellChanged(EditEnded)
  //pobjspdList.bind(GcSpread.Sheets.Events.EditEnded, function (sender, args) {
  //    ps_setSpreadEvents_CellChanged(args.row, args.col);
  //});
  //Events : CellDoubleClick
  pobjspdList.bind(GC.Spread.Sheets.Events.CellDoubleClick, function (sender, args) {
      ps_setSpreadEvents_CellDoubleClick(args);
  });
  //Events : EditEnded
  pobjspdList.bind(GC.Spread.Sheets.Events.EditEnded, function (sender, args) {
      ps_setSpreadEvents_LostFocus(args);
  });
  //Events : LeaveCell
  pobjspdList.bind(GC.Spread.Sheets.Events.LeaveCell, function (sender, args) {
      ps_setSpreadEvents_LostFocus(args);
  });
  //Events : ClipboardPasted
  pobjspdList.bind(GC.Spread.Sheets.Events.ClipboardPasted, function (sender, args) {
      ps_setSpreadEvents_Pasted(args);
  });
  
  //Events : RowChanged
  /*pobjspdList.bind(GC.Spread.Sheets.Events.RowChanged, function (sender, args) {
	  ps_setSpreadEvents_RowChanged(args);
  });*/
  
}


//***************
//2022-03-18
//Events Bind New(funcObject 추가)
/*
funcObject = {
	'onButonClicked' : function(args){}
    , 'onValueChanged' : function(args){}
    , 'onCellChanged' : function(args){}
    , 'onCellClick' : function(args){}
    , 'onCellDoubleClick' : function(args){}
    , 'onEditEnded' : function(args){}
    , 'onLeaveCell' : function(args){}
    , 'onPasted' : function(args){}
}
*/
function ps_setSpreadEvents_Bind_MultiV2(pobjspdList, pdbjspdSheet, funcObject) {
    //Events : ButtonCliecked
    pdbjspdSheet.bind(GC.Spread.Sheets.Events.ButtonClicked, function (sender, args) {
        funcObject.onButonClicked(args);
    });
    //Events : ValueChanged
    pobjspdList.bind(GC.Spread.Sheets.Events.ValueChanged, function (sender, args) {
        funcObject.onValueChanged(args);
    });
    //Events : CellChanged
    pobjspdList.bind(GC.Spread.Sheets.Events.CellChanged, function (sender, args) {
        funcObject.onCellChanged(args);
    });
    //Events : CellClick
    pobjspdList.bind(GC.Spread.Sheets.Events.CellClick, function (sender, args) {
        funcObject.onCellClick(args);
    });
    //Events : CellDoubleClick
    pobjspdList.bind(GC.Spread.Sheets.Events.CellDoubleClick, function (sender, args) {
        funcObject.onCellDoubleClick(args);
    });
    //Events : EditEnded
    pobjspdList.bind(GC.Spread.Sheets.Events.EditEnded, function (sender, args) {
        funcObject.onEditEnded(args);
    });
    //Events : LeaveCell
    pobjspdList.bind(GC.Spread.Sheets.Events.LeaveCell, function (sender, args) {
        funcObject.onLeaveCell(args);
    });
    //Events : ClipboardPasted
    pobjspdList.bind(GC.Spread.Sheets.Events.ClipboardPasted, function (sender, args) {
        funcObject.onPasted(args);
    });
  }

//DatePicker show for Click event
function showSpdDatepicker(row, col, pdbjspdList) {
//jQuery('#spdDatePannel').datepicker('hide');
jQuery('#spdDatePannel').datepicker('destroy');

if(pdbjspdList.getCell(row, col).locked()) {
	 return;
}

var dateString="";


var cellRect = document.activeElement; //pdbjspdList.getCellRect(row, col);
var hearRowCnt = pdbjspdList.getRowCount(GC.Spread.Sheets.SheetArea.colHeader);

if (!cfn_isEmpty(pdbjspdList.getValue(row, col)))
   dateString  = replaceAll(pdbjspdList.getValue(row, col), "-", "");
else
   dateString  = getNowDate();

var year        = dateString.substring(0,4);
var month       = dateString.substring(4,6);
var day         = dateString.substring(6,8);

var dateFull = year + "-" + month + "-" + day;

var top;
var left;
var rectDefault = jQuery(cellRect).offset().top; //- ((hearRowCnt-1) * 31);
//if (cellRect.y > 200){
if(false){
	top = rectDefault - 10;
	 //jQuery("#spdDatePannel").css("top",rectDefault-10);
}else{
	 top = rectDefault - 200;
	 //jQuery("#spdDatePannel").css("top",rectDefault+214);
}
left = jQuery(cellRect).offset().left - 200;

//jQuery("#spdDatePannel").css("left",cellRect.x+35);

jQuery('#spdDatePannel').datepicker({
	changeMonth : true,
	changeYear : true,
	dateFormat : "yy-mm-dd",
	//showOn : "button",
	//buttonImage : "/img/exam/btn/calendar.gif",
	buttonImageOnly : true,
	//showOn: 'both',
	onSelect: function(dateText) {
		pdbjspdList.setValue(row, col, dateText);
	},
	beforeShow: function (event, ui) {
      setTimeout(function () {
          ui.dpDiv.css({ left: left, top: top });      
      }, 0);               
  }
	
});
jQuery('#spdDatePannel').datepicker("setDate", dateFull);
jQuery('#spdDatePannel').datepicker('show');

}

function ps_setSpreadSheet_init_new(pobjSheet, blnTabsNameView, contextOn) {
	
	console.log("Hear!!!!!");
	
	//SHEET 속성
  if(contextOn == null || contextOn == undefined){
	  contextOn = false;
  }
  pobjSheet.options.allowContextMenu     = contextOn;
  pobjSheet.options.allowSheetReorder    = false;
  pobjSheet.options.allowUserDragFill    = false;
  pobjSheet.options.allowUserDragDrop    = false;
  pobjSheet.options.allowUserZoom        = false;
  pobjSheet.options.canUserDragDrop      = false;
  pobjSheet.options.canUserDragFill      = false;
  pobjSheet.options.newTabVisible        = false;
  pobjSheet.options.scrollbarMaxAlign    = true;
  pobjSheet.options.scrollbarShowMax     = true;
  pobjSheet.options.tabEditable          = false;
  pobjSheet.options.tabNavigationVisible = false;
  pobjSheet.options.tabStripVisible      = blnTabsNameView;
  pobjSheet.options.scrollIgnoreHidden  = true;
  // DEL키 입력방지
  pobjSheet.commandManager().register('mydelete',function(spread1){
  	pobjSheet.suspendEvent();
  	pobjSheet.resumeEvent();
	});
  pobjSheet.commandManager().setShortcutKey(null,GC.Spread.Commands.Key.del, false, false, false, false);
  pobjSheet.commandManager().setShortcutKey('mydelete',GC.Spread.Commands.Key.del, false, false, false, false);

	

};



function ps_setSpreadCol_TEXT_Property2(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngMaxLength, pblnwordWrap, pblnLock, pstrBgColor, pstrMARKY, pstrMARKN) {
	var lngtmpValign = 1;       //default : Center
  var lngtmpHalign = 0;       //default : Left
  if (pstrValign == 'TOP') {lngtmpValign = 0;};
  if (pstrValign == 'CENTER') {lngtmpValign = 1;};
  if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
  if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
  if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
  if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
  //pobjspdList 컬럼 수직,수평 Align 설정
  var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);

  cell.vAlign(lngtmpValign);
  cell.hAlign(lngtmpHalign);

  if (pstrBgColor != "") {
  	cell.backColor(pstrBgColor);
  }
  cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  //최대길이 plngMaxLength
  //대소문자
  cell.wordWrap(pblnwordWrap);
  cell.locked(pblnLock);

  //if (pblnLock){
	//  cell.backColor(BKC_LCK);
  //}
  
  var customFormatterTest = {};
  customFormatterTest.prototype = GC.Spread.Formatter.FormatterBase;
  customFormatterTest.format = function (obj, formatterData) {
	  
      var result;
      if(obj.toString() === 'Y'){
    	  formatterData.conditionalForeColor = "green";
      	result = pstrMARKY;
      }else{
    	  formatterData.conditionalForeColor = "grey";
      	result = pstrMARKN;        	
      }
      
      return result;
  };
  customFormatterTest.parse = function (str) {
      if (!str) {
          return "";
      }
      return str;
  }
  
  cell.formatter(customFormatterTest);
  pobjspdList.options.isProtected = true;
	
}


function ps_setSpreadCol_YN_Property(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngMaxLength, pblnwordWrap, pblnLock, pstrBgColor, pstrMARKY, pstrMARKN) {
	var lngtmpValign = 1;       //default : Center
  var lngtmpHalign = 0;       //default : Left
  if (pstrValign == 'TOP') {lngtmpValign = 0;};
  if (pstrValign == 'CENTER') {lngtmpValign = 1;};
  if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
  if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
  if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
  if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
  //pobjspdList 컬럼 수직,수평 Align 설정
  var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);

  cell.vAlign(lngtmpValign);
  cell.hAlign(lngtmpHalign);

  if (pstrBgColor != "") {
  	cell.backColor(pstrBgColor);
  }
  cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  //최대길이 plngMaxLength
  //대소문자
  cell.wordWrap(pblnwordWrap);
  cell.locked(pblnLock);

  cell.font('bold 25px arial');
  
  var customFormatterTest = {};
  customFormatterTest.prototype = GC.Spread.Formatter.FormatterBase;
  customFormatterTest.format = function (obj, formatterData) {
	  
      var result;
      if(obj.toString() === 'Y'){
    	  formatterData.conditionalForeColor = "green";
      	result = pstrMARKY;
      }else{
    	  formatterData.conditionalForeColor = "red";
      	result = pstrMARKN;        	
      }
      
      return result;
  };
  customFormatterTest.parse = function (str) {
      if (!str) {
          return "";
      }
      return str;
  }
  
  cell.formatter(customFormatterTest);
  pobjspdList.options.isProtected = true;
	
}


function ps_setSpreadCol_CHECK_YN_Property(pobjspdList, plngSPDCOL, pstrValign, pstrHalign, plngMaxLength, pblnwordWrap, pblnLock, pstrBgColor) {
  var lngtmpValign = 1;       //default : Center
  var lngtmpHalign = 0;       //default : Left
  if (pstrValign == 'TOP') {lngtmpValign = 0;};
  if (pstrValign == 'CENTER') {lngtmpValign = 1;};
  if (pstrValign == 'BOTTOM') {lngtmpValign = 2;};
  if (pstrHalign == 'LEFT') {lngtmpHalign = 0;};
  if (pstrHalign == 'CENTER') {lngtmpHalign = 1;};
  if (pstrHalign == 'RIGHT') {lngtmpHalign = 2;};
  //pobjspdList 컬럼 수직,수평 Align 설정
  var cell = pobjspdList.getCell(-1, plngSPDCOL, GC.Spread.Sheets.SheetArea.viewport);

  cell.vAlign(lngtmpValign);
  cell.hAlign(lngtmpHalign);
  
  
  
  cell.borderRight(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderLeft(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderTop(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  cell.borderBottom(new GC.Spread.Sheets.LineBorder(DFC_LINE,GC.Spread.Sheets.LineStyle.thin));
  //최대길이 plngMaxLength
  //대소문자
  cell.wordWrap(pblnwordWrap);
  cell.locked(pblnLock);

  //if (pblnLock){
	//  cell.backColor(BKC_LCK);
  //}
  
  var customFormatterTest = {};
  customFormatterTest.prototype = GC.Spread.Formatter.FormatterBase;
  customFormatterTest.format = function (obj, formatterData) {
      
      if(obj.toString() === 'Y'){
    	  if (pstrBgColor != "") {
    		  pobjspdList.getRow().backColor(pstrBgColor);
    	  }
      }else{
    	     	
      }
      
      return obj;
  };
  customFormatterTest.parse = function (str) {
      if (!str) {
          return "";
      }
      return str;
  }
  
  cell.formatter(customFormatterTest);
  pobjspdList.options.isProtected = true;
	
}

function ps_setSpreadLineColor_Column(pobjspdList, plngSPDCOL, pstrColor) {
  //pobjspdList.getRange(plngSPDROW, -1, 1, -1).borderBottom(new GC.Spread.Sheets.LineBorder(pstrColor, GC.Spread.Sheets.LineStyle.thin));
  pobjspdList.getRange(-1, plngSPDCOL, -1, 1).borderRight(new GC.Spread.Sheets.LineBorder(pstrColor, GC.Spread.Sheets.LineStyle.thin));
}

// Footer Test
function fn_pager_search_event_bind(footer,funSearch){
	fn_setPageOnChangeListener(footer,funSearch);
	fn_setFooterSelectorOnChangeListener(footer,funSearch);
	fn_setBtnFirstOnClickListener(footer,funSearch);
	fn_setBtnPrevOnClickListener(footer,funSearch);
	fn_setBtnNextOnClickListener(footer,funSearch);
	fn_setBtnEndOnClickListener(footer,funSearch);
}

function fn_pager_init(footer, rowCountArray){
	var footStr = "";
	var maxpage = 1;
	var totalCount = 0;
	
	footStr += "<div class='ui-state-default ui-jqgrid-pager ui-corner-bottom' dir='ltr'>"
	footStr += "<div class='ui-pager-control' role='group'>"
	footStr += "<table cellspacing='0' cellpadding='0' border='0' class='ui-pg-table' style='width:100%; table-layout:fixed; height:100%;' role='row'>"
	footStr += "<tbody>"
	footStr += "<tr>"
	footStr += "<td align='left'> </td>"
	footStr += "<td align='center' style = 'white-space: pre; width:269px;'>"
	footStr += "<table cellspacing='0' cellpadding='0' border='0' class='ui-pg-table' style='table-layout: auto;'>"
	footStr += "<tbody>"
	footStr += "<tr>"
	footStr += "<td class='ui-pg-button ui-corner-all ui-state-disabled' style='cursor: default;'>"
	footStr += "<span class='ui-icon ui-icon-seek-first'></span>"
	footStr += "</td>"
	footStr += "<td class='ui-pg-button ui-corner-all ui-state-disabled' style='cursor: default;'>"
	footStr += "<span class='ui-icon ui-icon-seek-prev'></span>"
	footStr += "</td>"
	footStr += "<td class='ui-pg-button ui-state-disabled' style='width: 4px;'>"
	footStr += "<span class='ui-separator'></span>"
	footStr += "</td>"
	footStr += "<td dir='ltr'> Page "
	footStr += "<input class='ui-pg-input' type='text' size='2' maxlength='7' value='1' role='textbox'> of "
	footStr += "<span id='sp_1_pjmap1'>"+ maxpage +"</span>"
	footStr += "</td>"
	footStr += "<td class='ui-pg-button ui-state-disabled' style='width: 4px;'>"
	footStr += "<span class='ui-separator'></span>"
	footStr += "</td>"
	footStr += "<td class='ui-pg-button ui-corner-all ui-state-disabled' style='cursor: default;'>"
	footStr += "<span class='ui-icon ui-icon-seek-next'></span>"
	footStr += "</td>"
	footStr += "<td class='ui-pg-button ui-corner-all ui-state-disabled' style='cursor: default;'>"
	footStr += "<span class='ui-icon ui-icon-seek-end'></span>"
	footStr += "</td>"
	footStr += "<td dir='ltr'>"
	
	footStr += "<select class='ui-pg-selbox' role='listbox'>"
	for(var i = 0 ; i < rowCountArray.length; i++){
		if(i == 0){
			footStr += "<option role='option' value='"+rowCountArray[i]+"' selected='selected'>" + rowCountArray[i] + "</option>" 
		}
		else{
			footStr += "<option role='option' value='"+rowCountArray[i]+"'>" + rowCountArray[i] + "</option>"
		}
	}
	footStr += "</td>"
	footStr += "</tr>"
	footStr += "</tbody>"
	footStr += "</table>"
	footStr += "</td>"
	footStr += "<td align='right'>"
	footStr += "<div dir='ltr' style='text-align:right' class='ui-paging-info'>Total : "+totalCount+"</div>"
    footStr += "<div style='display:none' class='ui-paging-info-hidden'>"+totalCount+"</div>"
	footStr += "</td>"
	footStr += "</tr>"
	footStr += "</tbody>"
	footStr += "</table>"
	footStr += "</div>"
	footStr += "</div>"

	footer.append(footStr);
	fn_setPageBtn(footer);
	fn_setPageBtnOnHover(footer);
}

function fn_resetCurrentPage(footer){
	footer.find("input").val(1);
}


function fn_getFooterData(footer){
	var rowCount = footer.find("select").val();
	var currentPage = footer.find("input").val();
	var totalRowCount = footer.find("div[class='ui-paging-info-hidden']").text();
	var totalPage = footer.find("#sp_1_pjmap1").text();
	var res = {
		CurrentPage : currentPage,
		RowCount : rowCount,
		TotalRowCount : totalRowCount,
		TotalPage : totalPage,
	}
	return res;
}


function fn_setFooterSelectorOnChangeListener(footer,fn){
	footer.find("select").on("change",function(){
		fn_setTotalPage(footer);
		footer.find("input").val(1);
		var value = fn_getFooterData(footer)
		fn(value);
	});
}

function fn_setPageOnChangeListener(footer,fn){
	var footerInput = footer.find("input");
	footerInput.on("change",function(){
		var value = fn_getFooterData(footer)
		if(value.TotalPage==0){ footerInput.val(0); return; }
		if(!(Number(value.CurrentPage) <= Number(value.TotalPage) && Number(value.CurrentPage) >= 1)){ footerInput.val(1); }
		fn_setPageBtn(footer);
		value = fn_getFooterData(footer)
		fn(value);
	});
}

function fn_setBtnFirstOnClickListener(footer,fn){
	var first = footer.find("span[class='ui-icon ui-icon-seek-first']");
	first.on("click",function(){
		if(!first.parent().hasClass('ui-state-disabled')){
			var currentPage = footer.find("input").val();
			footer.find("input").val(1);
			var value = fn_getFooterData(footer);
			fn(value)
			fn_setPageBtn(footer);
		}
	});
}

function fn_setBtnPrevOnClickListener(footer,fn){
	var prev = footer.find("span[class='ui-icon ui-icon-seek-prev']");
	prev.on("click",function(){
		if(!prev.parent().hasClass('ui-state-disabled')){
			var currentPage = footer.find("input").val();
			footer.find("input").val(Number(currentPage)-1);
			var value = fn_getFooterData(footer);
			fn(value)
			fn_setPageBtn(footer);
		}
	});
}

function fn_setBtnNextOnClickListener(footer,fn){
	var next = footer.find("span[class='ui-icon ui-icon-seek-next']");
	next.on("click",function(){
		if(!next.parent().hasClass('ui-state-disabled')){
			var currentPage = footer.find("input").val();
			footer.find("input").val(Number(currentPage)+1);
			var value = fn_getFooterData(footer);
			fn(value)
			fn_setPageBtn(footer);
		}
	});
}

function fn_setBtnEndOnClickListener(footer,fn){
	var end = footer.find("span[class='ui-icon ui-icon-seek-end']");
	end.on("click",function(){
		if(!end.parent().hasClass('ui-state-disabled')){
			var totalPage = footer.find("#sp_1_pjmap1").text();
			footer.find("input").val(totalPage);
			var value = fn_getFooterData(footer);
			fn(value)
			fn_setPageBtn(footer);
		}
	});
}

function fn_setTotalRowCount(footer,value){
    var tempValue = value;
	var text = "Total : " + tempValue.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
	footer.find("div[class='ui-paging-info']").text(text);
    footer.find("div[class='ui-paging-info-hidden']").text(value);
	fn_setTotalPage(footer);
}
function fn_setTotalPage(footer){
	var totalRowCount = footer.find("div[class='ui-paging-info-hidden']").text();
	var maxRowCount = footer.find("select").val();
	var totalPage = Math.ceil(totalRowCount/maxRowCount);
	footer.find("#sp_1_pjmap1").text(totalPage);
	var currentPage = footer.find("input");
	if(totalPage == 0){
		currentPage.val(1);
		footer.find("#sp_1_pjmap1").text(1);
	}
	fn_setPageBtn(footer);
}

function fn_setPageBtnOnHover(footer){
	var first = footer.find("span[class='ui-icon ui-icon-seek-first']");
	var prev = footer.find("span[class='ui-icon ui-icon-seek-prev']");
	var next = footer.find("span[class='ui-icon ui-icon-seek-next']");
	var end = footer.find("span[class='ui-icon ui-icon-seek-end']");
	
	first.hover(function(){
		var currentPage = Number(footer.find("input").val());
		var totalPage = Number(footer.find("#sp_1_pjmap1").text());
		if(currentPage <= 1 || totalPage == 0){
			return;
		}
		first.parent().css("cursor","pointer");
		first.parent().addClass('ui-state-hover');
	},function(){
		first.parent().css("cursor","default");
		first.parent().removeClass('ui-state-hover');
	});
	prev.hover(function(){
		var currentPage = Number(footer.find("input").val());
		var totalPage = Number(footer.find("#sp_1_pjmap1").text());
		if(currentPage <= 1 || totalPage == 0){
			return;
		}
		prev.parent().css("cursor","pointer");
		prev.parent().addClass('ui-state-hover');
	},function(){
		prev.parent().css("cursor","default");
		prev.parent().removeClass('ui-state-hover');
	});
	next.hover(function(){
		var currentPage = Number(footer.find("input").val());
		var totalPage = Number(footer.find("#sp_1_pjmap1").text());
		if(currentPage < 1 || currentPage >= totalPage || totalPage == 0){
			return;
		}
		next.parent().css("cursor","pointer");
		next.parent().addClass('ui-state-hover');
	},function(){
		next.parent().css("cursor","default");
		next.parent().removeClass('ui-state-hover');
	});
	end.hover(function(){
		var currentPage = Number(footer.find("input").val());
		var totalPage = Number(footer.find("#sp_1_pjmap1").text());
		if(currentPage < 1 || currentPage >= totalPage || totalPage == 0){
			return;
		}
		end.parent().css("cursor","pointer");
		end.parent().addClass('ui-state-hover');
	},function(){
		end.parent().css("cursor","default");
		end.parent().removeClass('ui-state-hover');
	});
}

function fn_setPageBtn(footer){
	var first = footer.find("span[class='ui-icon ui-icon-seek-first']");
	var prev = footer.find("span[class='ui-icon ui-icon-seek-prev']");
	var next = footer.find("span[class='ui-icon ui-icon-seek-next']");
	var end = footer.find("span[class='ui-icon ui-icon-seek-end']");
	
	var currentPage = Number(footer.find("input").val());
	var totalPage = Number(footer.find("#sp_1_pjmap1").text());
	
	if(currentPage == 0 || totalPage == 0 || totalPage == 1){
		first.parent().addClass('ui-state-disabled');
		prev.parent().addClass('ui-state-disabled');
		next.parent().addClass('ui-state-disabled');
		end.parent().addClass('ui-state-disabled');
	}
	else{
		if(currentPage >= totalPage){
			first.parent().removeClass('ui-state-disabled');
			prev.parent().removeClass('ui-state-disabled');
			next.parent().addClass('ui-state-disabled');
			end.parent().addClass('ui-state-disabled');
		}
		else if(currentPage <= 1){
			first.parent().addClass('ui-state-disabled');
			prev.parent().addClass('ui-state-disabled');
			next.parent().removeClass('ui-state-disabled');
			end.parent().removeClass('ui-state-disabled');
		}
		else{
			first.parent().removeClass('ui-state-disabled');
			prev.parent().removeClass('ui-state-disabled');
			next.parent().removeClass('ui-state-disabled');
			end.parent().removeClass('ui-state-disabled');
		}
	}
}

function pf_checkedExcelReportExport(name, pobjSheet, chkList){
	var excelIo = new GC.Spread.Excel.IO();
	var fileName = name+".xlsx";
	var serializationOption = {
			includeBindingSource : true,
			ignoreStyle : false,
			ignoreFormula : true,
			rowHeadersAsFrozenColumns : false,
			columnHeadersAsFrozenRows : true,
			includeAutoMergedCells : false
	}
	
	var tempJson = pobjSheet.toJSON(serializationOption);
	delete tempJson.sheets.Sheet1.spans;
	
	var columnDataArray = tempJson.sheets.Sheet1.data.columnDataArray;
	var rowDataArray = tempJson.sheets.Sheet1.data.rowDataArray;
	var dataArray = tempJson.sheets.Sheet1.data.dataTable;
	var defaultDataNode = tempJson.sheets.Sheet1.data.defaultDataNode;

    var rowHeaderCount = pobjSheet.getActiveSheet(0).getRowCount(GC.Spread.Sheets.SheetArea.colHeader);

	var dataIdx = [];

    for(var i = 0; i < rowHeaderCount ; i++){
        dataIdx.push(i);
    }

	for(var i = 0; i < chkList.length; ++i){
		dataIdx.push(chkList[i]+rowHeaderCount);
	}

	var dataList = {};
	for(var i = 0; i < dataIdx.length; ++i){
		// Object.assign(dataList, dataArray[dataIdx[i]]);
		dataList[i] = dataArray[dataIdx[i]];
	}
	
	tempJson.sheets.Sheet1.data.dataTable = dataList;

	// dataArray style = rowDataArray + columnDataArray + dataArray style 
	for(var i = 0 ; i < rowDataArray.length ; i++){
        for(var j = 0 ; j < columnDataArray.length ; j++){
            //dataArray[i][j].style = dataArray[i][j]!= null && dataArray[i][j].style != null ? {}:null;
            // init DataArray Style
            if(dataArray[i][j] == null) {dataArray[i][j] = {};}
            dataArray[i][j].style = {};
            if(rowDataArray[i] != null && rowDataArray[i].style != null){
                if(dataArray[i][j].style != null){
                    if(columnDataArray[j] != null && columnDataArray[j].style != null){
                        Object.assign(dataArray[i][j].style, columnDataArray[j].style);
                    }
                    Object.assign(dataArray[i][j].style, rowDataArray[i].style);
                }
                else{
                    Object.assign(dataArray[i][j].style,defaultDataNode.style);
                    if(columnDataArray[j] != null && columnDataArray[j].style != null){
                        Object.assign(dataArray[i][j].style, columnDataArray[j].style);
                    }
                    Object.assign(dataArray[i][j].style, rowDataArray[i].style);
                }
			}
		}
	}
    
    //Erase RowDataArray/ColDataArray Style
    for(var i = 0 ; i < rowDataArray.length ; i++){
        if(rowDataArray[i] != null){rowDataArray[i].style = {};}
    }
    for(var j = 0 ; j < columnDataArray.length ; j++){
        if(columnDataArray[j] != null){columnDataArray[j].style = {};}
    }


	var json = JSON.stringify(tempJson);
	excelIo.save(json,function(blob){
		saveAs(blob,fileName);
	}, function(e){
		console.log(e);
	});
}
