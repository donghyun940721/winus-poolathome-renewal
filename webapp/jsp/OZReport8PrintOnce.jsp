<%@page import="java.net.URLEncoder"%>
<%@page import="java.net.URLDecoder"%>
<%@ page language ="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<% request.setCharacterEncoding("UTF-8"); %>
<!DOCTYPE html>
<html style="height:100%">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<script src="http://1.209.94.130:8088/oz80/ozhviewer/jquery-2.0.3.min.js"></script>
<link rel="stylesheet" href="http://1.209.94.130:8088/oz80/ozhviewer/jquery-ui.css" type="text/css"/>
<script src="http://1.209.94.130:8088/oz80/ozhviewer/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://1.209.94.130:8088/oz80/ozhviewer/ui.dynatree.css" type="text/css"/>
<script type="text/javascript" src="http://1.209.94.130:8088/oz80/ozhviewer/jquery.dynatree.js" charset="utf-8"></script>
<script type="text/javascript" src="http://1.209.94.130:8088/oz80/ozhviewer/OZJSViewer.js" charset="utf-8"></script>

</head>
<body style="width:100%;height:100%;margin:0px;">
<div id="OZViewer" style="width:100%;height:100%;"></div>

<script type="text/javascript" >
	
	<%
	String[] viewerMode = (request.getParameter("viewerMode")==null?"":(String)request.getParameter("viewerMode")).split(",");
	Boolean PrintFlag = false;
	Boolean ExportFlag = false;
		
	if(viewerMode.length != 0){
		for(int i = 0 ; i < viewerMode.length ; i++){
			if(viewerMode[i].equals("print")){
				PrintFlag = true;
			}else if(viewerMode[i].equals("export")){
				ExportFlag = true;
			}
		}
	}
	%>
	//step - Step of the report creating(0 : Setting Viewer Options, 1 : Download the report file, 2 : Create a report template, 3 : Download the data, 4 : Report binding)
	//state - State of the report creating(1 : Start, 2 : Finish)
	function OZProgressCommand_OZViewer(step, state, reportname) {
		if (step == 4 && state == 2){
			<%if(PrintFlag){%>
			OZViewer.Script("print");
			<%}%>
			<%if(ExportFlag){%>
			OZViewer.Script("save");
			<%}%>
		}
	}
	
	<%
		String[] key = ((String)request.getParameter("arrKey")).split(",");
		int argLen = key.length;
	%>
	var fileName = "<%=request.getParameter("fileName")%>";
	var reportName = "<%=request.getParameter("reportName")%>";
	var odiName = "<%=request.getParameter("odiName")%>";
	var zoom = "<%=request.getParameter("zoom")!=null ? request.getParameter("zoom"):"100"%>";
	var viewerMode = "<%=request.getParameter("viewerMode")!=null ? request.getParameter("viewerMode"):"print"%>";
	// ì¤ì¦ë·°ì´ html5 ì¤í ë¶ë¶
	function SetOZParamters_OZViewer(){
		var oz;

		oz = document.getElementById("OZViewer");
		oz.sendToActionScript("viewer.zoom",zoom);
		oz.sendToActionScript("viewer.mode","preview");
		oz.sendToActionScript("export.mode","silent");
		oz.sendToActionScript("print.mode","silent");
		oz.sendToActionScript("print.once","true");
		oz.sendToActionScript("export.format","pdf");
		oz.sendToActionScript("export.filename", fileName);
		oz.sendToActionScript("pdf.fontembedding","true");

		oz.sendToActionScript("connection.servlet", "http://1.209.94.130:8088/oz80/server");
		oz.sendToActionScript("connection.reportname","WINUS/"+reportName);

		oz.sendToActionScript("odi.odinames",odiName);
		oz.sendToActionScript("information.debug","true");

		oz.sendToActionScript("connection.pcount","1");
		var date = new Date();
		oz.sendToActionScript("connection.args1","NOW_DATE="+date);
	
		oz.sendToActionScript("odi."+odiName+".pcount","<%=argLen%>");
		<% for(int i = 0; i< argLen; i++){%>
			oz.sendToActionScript("odi."+odiName+".args<%=i+1%>","<%=key[i]%>=<%=request.getParameter(key[i])%>");
		<%}%>
		oz.sendToActionScript("viewer.progresscommand", "true");
		return true;
	}

	start_ozjs("OZViewer","http://1.209.94.130:8088/oz80/ozhviewer/");
</script>

</body>
</html>