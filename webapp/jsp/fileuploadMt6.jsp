<%@page import="com.logisall.winus.frm.common.util.ConstantIF"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script language="javascript">

//파일 경로 값 이동
function _fn_addfileprocess(parm) {	
	var idxTemp = jdfwForm.rowIdx.value;
	if (idxTemp != '') {
		parent._fn_addfilelineready(idxTemp, parm);
	}
	
}
	
function uploaderCallback(arg){	
	var idxTemp = jdfwForm.rowIdx.value;
	var filetag = jdfwForm.txtFile;
	var parent_filetag = filetag.parentNode;
	var new_filetag = filetag.cloneNode(true);
	parent_filetag.removeChild(filetag);
	parent_filetag.appendChild(new_filetag);
	
	var splits=arg.split("^");
	var status=splits[0];
	var statuss=status.split(":");
	var result=statuss[1]; // 경로
	
	if ( splits.length > 1 ) {
		for( var i = 1; i < splits.length; i++) {
			var fileinfo = splits[i];
			if(result == "-1") {
				alert(fileinfo);
			}
			
			if ( fileinfo != '') {
				var fileinfos = fileinfo.split("||");
				var realfilepath = fileinfos[0];
				var filename = fileinfos[1];
				var filesize = Number(fileinfos[2]);
				var fileIdx = fileinfos[3];
				var lastIdx = false;
				if ( fileinfos.length > 4) {
					lastIdx = true;
				}
				// console.log('realfilepath :' + realfilepath + ', filesize :' + filesize );				
				parent._fn_fileprocess(result, realfilepath, filename, filesize, fileIdx, lastIdx);				
			}			
		}
	}
	
	jdfwForm.formname.value = "";
	jdfwForm.rowIdx.value = "";
	jdfwForm.txtFile.value = "";
	jdfwForm.txtFile0.value = "";
	jdfwForm.txtFile1.value = "";
	jdfwForm.txtFile2.value = "";
	jdfwForm.txtFile3.value = "";
	jdfwForm.txtFile4.value = "";
	jdfwForm.txtFile5.value = "";
	jdfwForm.txtFile6.value = "";
	jdfwForm.txtFile7.value = "";
	jdfwForm.txtFile8.value = "";
	jdfwForm.txtFile9.value = "";
	jdfwForm.txtFile10.value = "";
	jdfwForm.txtFile11.value = "";
}
</script>
</head>
<body>

	<div id="file_part" >
	
		<iframe id="uploadFrameID" name="uploadFrame" height="0" width="0" frameborder="0" scrolling="no"></iframe>
		  
		<form id="jdfwForm" name="jdfwForm" enctype="multipart/form-data" method="post" action="/JDFW5XUploader6" target="uploadFrame">
			<input type="text" name="formname" id="formname" />
			<input type="text" name="rowIdx" id="rowIdx" />
			<div id="fileuppart">
				<input type="file" name="txtFile" id="txtFile" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile0" id="txtFile0" onchange="_fn_addfileprocess(this.value);return false;" /> 
				<input type="file" name="txtFile1" id="txtFile1" onchange="_fn_addfileprocess(this.value);return false;" /> 
				<input type="file" name="txtFile2" id="txtFile2" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile3" id="txtFile3" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile4" id="txtFile4" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile5" id="txtFile5" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile6" id="txtFile6" onchange="_fn_addfileprocess(this.value);return false;" />  
				<input type="file" name="txtFile7" id="txtFile7" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile8" id="txtFile8" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile9" id="txtFile9" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile10" id="txtFile10" onchange="_fn_addfileprocess(this.value);return false;" />
				<input type="file" name="txtFile11" id="txtFile11" onchange="_fn_addfileprocess(this.value);return false;" />
			</div>			
			<input type="text" name="filePath" 		id="filePath" 		style="width:500px;" value=""/>
			<input type="text" name="fileLimit" 	id="fileLimit" 		style="width:500px;" value="20"/>
			<input type="text" name="filePostion" 	id="filePostion" 	style="width:500px;"/>
			<input type="text" name="fileIdx" 		id="fileIdx" 		style="width:500px;"/>
			<input type="submit" id="submitID" name="submitID" value="파일전송"/>		
		</form>
      
	</div>
	
</body>
</html>