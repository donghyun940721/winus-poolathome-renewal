<%@page import="com.logisall.winus.frm.common.util.ConstantIF"%>
<%@page import="com.logisall.winus.frm.common.util.DeleteDir"%>
<%@page import="java.util.Timer"%>
<%@page import="java.util.TimerTask"%>
<%@ page language ="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% request.setCharacterEncoding("UTF-8"); %>
<%@ page import="java.io.*, com.logisall.winus.frm.common.util.CommonUtil" %>

<%
    String fileName = request.getParameter("file_name");
    String newFileName = java.net.URLEncoder.encode(fileName, "UTF-8");
    String fileType = newFileName.substring(newFileName.lastIndexOf(".")+1);
	
    //파일경로와 파일명을 넘겨받아 파일을 생성한다.
    String savePath = request.getParameter("file_path");
    String baseUploadDir = ConstantIF.FILE_ATTACH_PATH + "ATCH_FILE";
    File file = new File( baseUploadDir + savePath + "/" + fileName );
  
    String deletePath   = request.getParameter("file_delPath");
    String userBasePath = request.getParameter("file_userBasePath");
    
    if(file.exists()) {
      	FileInputStream fileIn = null;
      	BufferedOutputStream bos = null;
      	
	   try {
		   newFileName = CommonUtil.replace(newFileName, "+", "%20");

		   //response.reset();
		   response.setContentType((fileType == null)? "application/octet-stream" : fileType);
	       response.setHeader("Content-Disposition", "attachment;filename=" + newFileName + ";");

		   int fileLength = (int) file.length();
           response.setContentLength(fileLength);
           
           // writer 객체 중복사용에 의한 에러방지를 위해 한번 클리어 해준다.
           out.clear();
           out = pageContext.pushBody();

		   //파일 객체를 스트림으로 불러온다.
           fileIn = new FileInputStream(file);

           byte[] b = new byte[1024];

           bos = new BufferedOutputStream(response.getOutputStream());
           int bytesRead = -1;
           
           try {
                  while ((bytesRead = fileIn.read(b)) != -1) {
						bos.write(b, 0, bytesRead);
                  }
                  
                  bos.flush();
                  
                  //파일삭제
                  File fileD    = new File(deletePath);
                  File fileBase = new File(userBasePath);
       		   	  DeleteDir.deleteDirectory(fileD);
       		  	  DeleteDir.deleteZipFile(fileBase, fileD);
              	
           } catch (Exception e) {
        	   throw e;
           }
 	   }
	   catch(Exception e1) {
		   e1.printStackTrace();
%>

<script language="javascript">
<!--
alert("<spring:message code='파일다운로드에실패했습니다' />");
// history.go(-1);
//-->
</script>

<%
        } finally {
            try {
            	if (bos != null) {
                    bos.close();
                }
                if (fileIn != null) {
                    fileIn.close();
                }
            } catch (IOException e2) {
                 throw new Exception();
            }
        }
	} else {
%>

<script language="javascript">
<!--
alert("<spring:message code='첨부된파일이없습니다' />");
// history.go(-1);
//-->
</script>

<% } %>