// 결품율 게이지
// Themes begin
am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance

// Let's cut a hole in our Pie chart the size of 40% the radius
var chart = am4core.create("chartdiv06", am4charts.PieChart);
chart.innerRadius = "40%";
chart.radius = "90%";



// Add and configure Series
var pieSeries = chart.series.push(new am4charts.PieSeries());
pieSeries.dataFields.value = "value";
pieSeries.dataFields.category = "category";
pieSeries.slices.template.stroke = 0;
pieSeries.innerRadius = 10;
pieSeries.slices.template.fill = "#316fd3";
pieSeries.slices.template.fillOpacity = 0.1;

pieSeries.slices.template.propertyFields.disabled = "labelDisabled";
pieSeries.labels.template.propertyFields.disabled = "labelDisabled";
pieSeries.ticks.template.propertyFields.disabled = "labelDisabled";
pieSeries.labels.template.disabled = true;
pieSeries.ticks.template.disabled = true;

// Add data
pieSeries.data = [
  {
    "category": "장기재고율",
    "value": 10
  }, {
    "category": "Unused",
    "value": 90,
    "labelDisabled":true
  }
];

// Disable sliding out of slices
pieSeries.slices.template.states.getKey("hover").properties.shiftRadius = 0;
pieSeries.slices.template.states.getKey("hover").properties.scale = 1;
pieSeries.slices.template.states.getKey("active").properties.shiftRadius = 0;

// Add second series
var pieSeries2 = chart.series.push(new am4charts.PieSeries());
pieSeries2.dataFields.value = "value";
pieSeries2.dataFields.category = "category";
pieSeries2.slices.template.stroke = 0;
pieSeries2.slices.template.states.getKey("hover").properties.shiftRadius = 0;
pieSeries2.slices.template.states.getKey("hover").properties.scale = 1;
pieSeries2.slices.template.states.getKey("active").properties.shiftRadius = 0;
pieSeries2.slices.template.propertyFields.fill = "fill";
pieSeries2.labels.template.disabled = true;
pieSeries2.ticks.template.disabled = true;
pieSeries2.slices.template.tooltipText = "";

// Add data
pieSeries2.data = [
  {
    "category": "First",
    "value": 10,
    "fill": "#316fd3"
  },
  {
    "category": "Remaining",
    "value": 90,
    "fill":"#eeeeee",
  }
];


pieSeries.adapter.add("innerRadius", function(innerRadius, target){
  return am4core.percent(40);
})

pieSeries2.adapter.add("innerRadius", function(innerRadius, target){
  return am4core.percent(60);
})

pieSeries.adapter.add("radius", function(innerRadius, target){
  return am4core.percent(100);
})

pieSeries2.adapter.add("radius", function(innerRadius, target){
  return am4core.percent(80);
})
