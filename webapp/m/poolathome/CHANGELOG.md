***See migration guides in the docs***

---

### 1.5.2 (patch)

  *  Updated: rgba-to-hex SCSS function. The second parameter isn't required anymore.

**HTML5, .Net Core, .Net Core MVC, .Net MVC, .Net MVC + Webpack, Laravel, Rails, Rails + Turbolinks**

  *  Fixed: select2 styling

**Vue.js, Laravel + Vue.js**

  *  Fixed: plyr version

### 1.5.1 (patch)

**HTML5, .Net Core, .Net Core MVC, .Net MVC, .Net MVC + Webpack, Laravel, Rails, Rails + Turbolinks**

  *  Fixed: flatpickr styling

**Vue.js, Laravel + Vue.js**

  *  Fixed: vue-flatpickr-component styling

**React**

  *  Fixed: react-flatpickr styling

### 1.5.0

  *  Upgraded: bootstrap to 4.4.*
  *  Upgraded: @fortawesome/fontawesome-free to 5.13.*
  *  **New:** Dark style
  *  **New:** Added custom variables that allow to include/exclude features, such as RTL direction support; light, material and dark styles
  *  **New:** ThemeSettings.setStyle(), ThemeSettings.isLightStyle(), ThemeSettings.isMaterialStyle(), ThemeSettings.isDarkStyle() methods
  *  **New:** .callout notification elements
  *  **New:** Added graphic logos support to .app-brand element
  *  **Breaking:** default-style renamed to light-style
  *  **Breaking:** ThemeSettings.setMaterial() method removed
  *  **Breaking:** ThemeSettings' available controls are changed, see the docs
  *  **Changed:** Yarn package manager is no longer required, you can use pure NPM now
  *  **Removed:** 1.2.2 → 1.3.0 migration stylesheets

**HTML5, .Net Core, .Net Core MVC, .Net MVC, .Net MVC + Webpack, Laravel, Rails, Rails + Turbolinks**

  *  Upgraded: nouislider to 14.2.*
  *  Upgraded: swiper to 5.3.*
  *  Upgraded: shepherd.js to 7.1.*
  *  Upgraded: perfect-scrollbar to 1.5.*
  *  Upgraded: sweetalert2 to 9.10.*
  *  Upgraded: blueimp-gallery to 2.38.*
  *  Upgraded: bootbox to 5.4.*
  *  Upgraded: timepicker to 1.13.*
  *  Upgraded: @flowjs/flow.js to 2.14.*
  *  Upgraded: chart.js to 2.9.*
  *  Upgraded: flot to 4.2.*
  *  Upgraded: plyr to 3.5.*
  *  Upgraded: fullcalendar to 4.4.*
  *  Upgraded: bootstrap-table to 1.16.*
  *  Upgraded: dropzone to 5.7.*
  *  Upgraded: smartwizard to 4.4.*
  *  Upgraded: spinkit to 2.0.*. Markup is changed, see https://github.com/tobiasahlin/SpinKit
  *  Upgraded: core-js to 3.6.*
  *  Upgraded: development dependencies

**Vue.js, Laravel + Vue.js**

  *  **Breaking**: "isRTL" global property is renamed to "isRtlMode" because of name collisions
  *  **New:** Form tags, see https://bootstrap-vue.js.org/docs/components/form-tags
  *  Upgraded: bootstrap-vue to 2.11.*
  *  Upgraded: nouislider to 14.2.*
  *  Upgraded: perfect-scrollbar to 1.5.*
  *  Upgraded: vue-simple-calendar to 4.3.*
  *  Upgraded: vue-slider-component to 3.1.*
  *  Upgraded: vue-awesome-swiper to 4.1.*. Changes: "swiper" package is a peer dependency now; swiper and swiperSlide components are renamed to Swiper and SwiperSlide. See: https://github.com/surmon-china/vue-awesome-swiper/blob/master/CHANGELOG.md (starting from 4.0.0-rc.0)
  *  Upgraded: vue2-dropzone to 3.6.*
  *  Upgraded: vue-cropper to 0.5.x
  *  Upgraded: vue-password-strength-meter to 1.7.*
  *  Upgraded: vue-plyr to 6.0.*
  *  Upgraded: vue-chartjs to 3.5.*
  *  Upgraded: chart.js to 2.9.*
  *  Upgraded: echarts to 4.7.*
  *  Upgraded: spinkit to 2.0.*. Markup is changed, see https://github.com/tobiasahlin/SpinKit
  *  Replaced: `plyr` with `vue-plyr`

**Laravel, Laravel + Vue.js**

  *  Upgraded: laravel to 7.0.*. See https://laravel.com/docs/7.x/upgrade
  *  Upgraded: development dependencies

**Angular**

  *  Replaced: ng2-img-cropper package with ngx-image-cropper
  *  Upgraded: angular to 9.1.x. **Note:** Ivy is disabled by default because a lot of third-party packages aren't ready yet
  *  Upgraded: ng-bootstrap to 6.0.*. Changelog: https://github.com/ng-bootstrap/ng-bootstrap/blob/master/CHANGELOG.md
  *  Upgraded: @angular/cdk to 9.2.*
  *  Upgraded: @agm/core to 1.1.*
  *  Upgraded: ng-select to 4.0.*
  *  Upgraded: ngx-contextmenu to 5.4.*
  *  Upgraded: ngx-markdown-editor to 3.0.*
  *  Upgraded: marked to 0.8.*
  *  Upgraded: angular-calendar to 0.28.*. See https://github.com/mattlewis92/angular-calendar/blob/master/CHANGELOG.md#-breaking-changes
  *  Upgraded: ngx-sweetalert2 to 8.1.*
  *  Upgraded: date-fns to 2.9.*
  *  Upgraded: ngx-charts to 13.0.*
  *  Upgraded: angular2-ladda to 3.1.*
  *  Upgraded: ngx-clipboard to 13.0.*
  *  Upgraded: ngx-color-picker to 9.0.*
  *  Upgraded: ngx-swiper-wrapper to 9.0.*
  *  Upgraded: ngx-perfect-scrollbar to 9.0.*
  *  Upgraded: ngx-dropzone-wrapper to 9.0.*
  *  Upgraded: ng2-file-upload to 1.4.*
  *  Upgraded: ng2-smart-table to 1.6.*. The component requires @akveo/ng2-completer package installed
  *  Upgraded: ngx-toastr to 12.0.*
  *  Upgraded: ngx-trend to 5.0.*
  *  Upgraded: ngx-image-gallery to 2.0.*
  *  Upgraded: ng2-completer to 9.0.*
  *  Upgraded: shepherd.js to 7.1.*
  *  Upgraded: nouislider to 14.2.*
  *  Upgraded: sweetalert2 to 9.10.*
  *  Upgraded: chart.js to 2.9.*
  *  Upgraded: spinkit to 2.0.*. Markup is changed, see https://github.com/tobiasahlin/SpinKit
  *  Replaced: `plyr` with `ngx-plyr`
  *  Upgraded: plyr to 3.5.*

**Vue**

  *  Upgraded: development dependencies

**React**

  *  Replaced: react-plyr with original plyr package
  *  Upgraded: react to 16.13.*
  *  Upgraded: react-dom to 16.13.*
  *  Upgraded: react-redux to 7.2.*
  *  Upgraded: @loadable/component to 5.12.*
  *  Upgraded: react-select to 3.1.*
  *  Upgraded: react-beautiful-dnd to 13.0.*
  *  Upgraded: react-big-calendar to 0.24.*
  *  Upgraded: react-bootstrap-table-next to 3.3.*
  *  Upgraded: react-bootstrap-table2-filter to 1.3.*
  *  Upgraded: react-chartjs-2 to 2.9.*
  *  Upgraded: react-color to 2.18.*
  *  Upgraded: react-contextmenu to 2.13.*
  *  Upgraded: react-datepicker to 2.14.*
  *  Upgraded: react-dropzone to 10.2.*
  *  Upgraded: react-flatpickr to 3.10.*
  *  Upgraded: react-id-swiper to 3.0.*
  *  Upgraded: react-sortable-tree to 2.7.*
  *  Upgraded: react-toastify to 5.5.*
  *  Upgraded: react-mde to 8.2.*
  *  Upgraded: react-images to 1.1.*
  *  Upgraded: react-table to 7.0.*. The component has breaking changes, see https://github.com/tannerlinsley/react-table
  *  Upgraded: react-bootstrap-table-next to 4.0.*
  *  Upgraded: sweetalert2-react-content to 3.0.*
  *  Upgraded: styled-components to 5.1.*
  *  Upgraded: swiper to 5.3.*
  *  Upgraded: sweetalert2 to 9.10.*
  *  Upgraded: perfect-scrollbar to 1.5.*
  *  Upgraded: chart.js to 2.9.*
  *  Upgraded: spinkit to 2.0.*. Markup is changed, see https://github.com/tobiasahlin/SpinKit
  *  Added: react-plyr package
  *  Upgraded: development dependencies

**Rails**

  *  Upgraded: development dependencies

**.Net Core, .Net Core MVC **

  *  Upgraded: .Net Core SDK to 3.1.*

**.Net MVC, .Net MVC + Webpack**

  *  Upgraded: .Net Framework to 4.8

### 1.4.0

  *  **New:** React version
  *  **New:** .Net Core MVC version
  *  **New:** .Net MVC + Webpack version
  *  **New:** Laravel + Vue.js Demo project
  *  **New:** Rails + jQuery (no Turbolinks) Starter project
  *  Upgraded: ionicons to 4.6.*
  *  Upgraded: @fortawesome/fontawesome-free to 5.11.*. See https://github.com/FortAwesome/Font-Awesome/blob/master/UPGRADING.md
  *  Fixed: .layout-sidenav height in static vertical mode
  *  Fixed: small CSS bugs
  *  Fixed: SideNav dividers and headers within submenus and dropdowns
  *  Updated: SideNav→onOpen and SideNav→onClose callbacks are preventable now

**HTML5, .Net Core, .Net MVC, Laravel, Rails**

  *  Removed: bootstrap-sweetalert package, use original sweetalert2 package
  *  Removed: bootstrap-multiselect package
  *  Upgraded: nouislider to 14.0.*
  *  Upgraded: flatpickr to 4.6.*
  *  Upgraded: swiper to 5.1.*
  *  Upgraded: fullcalendar to 4.3.*
  *  Upgraded: sweetalert2 to 8.18.*
  *  Upgraded: bootstrap-datepicker to 1.9.*
  *  Upgraded: shepherd.js to 5.0.*. See https://github.com/shipshapecode/shepherd/blob/master/CHANGELOG.md and demo examples
  *  Upgraded: sortablejs to 1.10.*
  *  Upgraded: blueimp-gallery to 2.35.*
  *  Upgraded: bootbox to 5.3.*
  *  Upgraded: bootstrap-table to 1.15.*. Note: Some extensions were removed, see https://github.com/wenzhixin/bootstrap-table/blob/develop/CHANGELOG.md
  *  Upgraded: popper.js to 1.16.*
  *  Upgraded: c3 to 0.7.*
  *  Upgraded: flot to 3.2.*
  *  Upgraded: raphael to 2.3.*
  *  Upgraded: cropper to 4.1.*
  *  Upgraded: development dependencies
  *  Added: focus state for Quill editor

**Vue.js, Laravel + Vue.js**

  *  Upgraded: bootstrap-vue to 2.0.*. See https://bootstrap-vue.js.org/docs/misc/changelog#breaking-changes-and-deprecated-features-removal-v200
  *  Upgraded: vue-router to 3.1.*
  *  Upgraded: vue-meta to 2.3.*
  *  Upgraded: axios to 0.19.*
  *  Upgraded: nouislider to 14.0.*
  *  Upgraded: vuejs-datepicker to 1.6.*
  *  Upgraded: vue-number-input-spinner to 2.2.*
  *  Upgraded: vuedraggable to 2.23.*
  *  Upgraded: vue-password-strength-meter to 1.5.*
  *  Upgraded: vue-simplemde to 1.0.*
  *  Upgraded: c3 to 0.7.*
  *  Added: focus state for vue-quill-editor component
  *  Added: default 404 Not Found page

**Laravel, Laravel + Vue.js**

  *  Upgraded: laravel to 6.0.*. See https://laravel.com/docs/6.x/upgrade
  *  Upgraded: laravel-mix to 5.0.*
  *  Upgraded: development dependencies
  *  Improved: full integration with Laravel Mix, that lets use original Javascript sources

**Vue.js**

  *  Upgraded: Vue CLI to 4.*
  *  Upgraded: development dependencies

**Angular**

  *  Removed: unnecessary `@angular/http` package
  *  Replaced: `ngx-tour-core` and `ngx-tour-ng-bootstrap` packages with `shepherd.js`
  *  Replaced: deprecated `angular-sortablejs` package with `ngx-sortablejs`
  *  Upgraded: angular to 8.2.*. See https://blog.ninja-squad.com/2019/05/29/what-is-new-angular-8.0/
  *  Upgraded: typescript to 3.5.*
  *  Upgraded: @ng-bootstrap/ng-bootstrap to 5.1.*
  *  Upgraded: zone.js to 0.10.*
  *  Upgraded: sortablejs to 1.10.*
  *  Upgraded: nouislider to 14.0.*
  *  Upgraded: ng2-nouislider to 1.8.*
  *  Upgraded: ngx-swiper-wrapper to 8.0.*
  *  Upgraded: ngx-perfect-scrollbar to 8.0.*
  *  Upgraded: ngx-clipboard to 12.2.*
  *  Upgraded: @swimlane/ngx-datatable to 16.0.*
  *  Upgraded: @ng-select/ng-select to 3.3.*
  *  Upgraded: ng2-smart-table to 1.5.*
  *  Upgraded: ngx-dropzone-wrapper to 8.0.*
  *  Upgraded: ngx-toastr to 11.1.*
  *  Upgraded: @sweetalert2/ngx-sweetalert2 to 6.0.*. See https://github.com/sweetalert2/ngx-sweetalert2/releases/tag/v6.0.0
  *  Upgraded: sweetalert2 to 8.18.*
  *  Upgraded: ngx-color-picker to 8.2.*
  *  Upgraded: ngx-contextmenu to 5.2.*
  *  Upgraded: ngx-markdown-editor to 2.1.*
  *  Upgraded: marked to 0.7.*
  *  Upgraded: @swimlane/ngx-charts to 12.0.*
  *  Upgraded: ng2-charts to 2.3.*
  *  Upgraded: ngx-trend to 4.0.*
  *  Added: focus state for quill component
  *  Modified: layout-sidenav now uses default `ChangeDetectionStrategy`
  *  Upgraded: development dependencies
  *  Added: default 404 Not Found page

**Rails**

  *  Renamed: rails-starter to rails-turbolinks-starter
  *  Upgraded: Ruby on Rails to 6.*
  *  Improved: asset management with Webpacker, that lets use original Javascript sources

**.Net Core**

  *  Upgraded: .Net Core to 3.*
  *  Improved: asset management with Gulp and Webpack, that lets use original Javascript sources

**.Net MVC**

  *  Upgraded: .Net Framework to 4.7.2
  *  Upgraded: NuGet packages
  *  Modified: simplified project config. For now, the entire content of Content/, Scripts/ and Vendor/ directories is included in the project

### 1.3.1

**HTML5, .Net Core, .Net MVC, Laravel, Rails**

  *  Changed: all dependencies that rely on GitHub repository were removed from package.json and copied directly to the "libs" directory

### 1.3.0

  *  Upgraded: bootstrap to 4.3.*
  *  Upgraded: node-sass to 4.11.*
  *  Upgraded: ionicons to 4.5.*
  *  Upgraded: @fortawesome/fontawesome-free to 5.8.*. See https://github.com/FortAwesome/Font-Awesome/blob/master/UPGRADING.md
  *  Fixed: form feedbacks
  *  Added: feedbacks support for switchers
  *  Added: .overflow-visible, .cursor-move, .font-weight-thin, .font-sans-serif, .font-serif, .font-monospace, .bg-none and .cell-fit helper classes
  *  Added: Multi language support for ThemeSettings plugin
  *  Added: Bootstrap 4 custom range inputs
  *  Added: Bootstrap 4 toasts
  *  Added: Bootstrap 4 spinners
  *  Added: Timeline element to the Social UI Kit
  *  Added: Sketch files
  *  Modified: Control layout sidenav hover state using Javascript instead of pure CSS. This change gives more control over the sidenav state and fixes "freezed" sidenav on tablets
  *  Refactored: Core Appwork CSS
  *  Note: Added support for Nodejs v10.15 LTS

**HTML5, .Net Core, .Net MVC, Laravel, Rails**

  *  Replaced: bootstrap-tour with Shepherd.js. See https://shipshapecode.github.io/shepherd/
  *  Upgraded: bootstrap-table to 1.14.*. Note: Some extensions were removed
  *  Upgraded: bootbox to 5.0.*
  *  Upgraded: chart.js to 2.8.*
  *  Upgraded: jquery-validation to 1.19.*
  *  Upgraded: moment to 2.24.*
  *  Upgraded: nouislider to 13.1.*
  *  Upgraded: sortablejs to 1.8.*
  *  Upgraded: swiper to 4.5.*
  *  Upgraded: sweetalert2 to 8.7.*
  *  Upgraded: bootstrap-slider to 10.6.*
  *  Upgraded: flot to 3.0.*. See https://github.com/flot/flot/blob/master/CHANGELOG.md
  *  Upgraded: fullcalendar to 4.0.*. The plugin API changed - see https://fullcalendar.io/docs and demo examples
  *  Upgraded: development dependencies
  *  Added: flatpickr plugin
  *  Note: `bootstrap-sweetalert` package has been deprecated since no development activity in the plugin repository. Use original SweetAlert2 plugin instead.

**Vue.js, Laravel + Vue.js**

  *  **Breaking**: "baseUrl" global property is renamed to "publicUrl" because of name collisions
  *  Upgraded: bootstrap-vue to 2.0.0-rc.16
  *  Upgraded: vue to 2.6.*
  *  Upgraded: nouislider to 13.1.*
  *  Upgraded: vue-clipboard2 to 0.3.*
  *  Upgraded: moment to 2.24.*
  *  Upgraded: chart.js to 2.8.*
  *  Upgraded: vue-echarts to 4.0.*
  *  Upgraded: vue-flatpickr-component to 8.1.*
  *  Upgraded: vue-gallery to 2.0.*. Breaking: the component doesn't support IE10 anymore, see usage examples
  *  Upgraded: vue-input-tag to 2.0.*. See https://github.com/matiastucci/vue-input-tag
  *  Upgraded: vue-number-input-spinner to 2.1.*
  *  Upgraded: vue-password-strength-meter to 1.4.*
  *  Upgraded: vuedraggable to 2.20.*. See https://github.com/SortableJS/Vue.Draggable/blob/master/documentation/migrate.md. Breaking: component import changed from "vuedraggable" to "vuedraggable/src/vuedraggable"
  *  Upgraded: vue-slider-component to 3.0.*. See https://nightcatsama.github.io/vue-slider-component/#/api/props
  *  Upgraded: vue-simple-calendar to 4.2.*
  *  Upgraded: vue-simplemde to 0.5.*
  *  Upgraded: vue-meta to 1.6.*
  *  Upgraded: development dependencies
  *  Added: echarts package

**Angular**

  *  Replaced: ng-autosize with ngx-textarea-autosize due low activity in plugin's repository. See https://github.com/evseevdev/ngx-textarea-autosize
  *  Replaced: @toverux/ngx-sweetalert2 package with new @sweetalert2/ngx-sweetalert2
  *  Upgraded: @angular to 7.2.*
  *  Upgraded: @angular/cdk to 7.3.*
  *  Upgraded: @angular/cli to 7.3.*
  *  Upgraded: @ng-bootstrap/ng-bootstrap to 4.1.*
  *  Upgraded: ng-select to 2.16.*
  *  Upgraded: ngx-charts to 10.1.*
  *  Upgraded: ngx-datatable to 14.0.*
  *  Upgraded: angular-2-dropdown-multiselect to 1.9.*
  *  Upgraded: angular-sortablejs to 2.7.*
  *  Upgraded: sortablejs to 1.8.*
  *  Upgraded: nouislider to 13.1.*
  *  Upgraded: ngx-trend to 3.4.*
  *  Upgraded: ng-block-ui to 2.1.*
  *  Upgraded: ngx-chips to 2.1.*
  *  Upgraded: ngx-contextmenu to 5.1.*
  *  Upgraded: ng2-charts to 2.2.*. Breaking change: Replace "ng2-charts/ng2-charts" imports with "ng2-charts"
  *  Upgraded: ng-chartist to 4.1.*
  *  Upgraded: ng2-dragula to 2.1.*. API changed, see https://github.com/valor-software/ng2-dragula
  *  Upgraded: ngx-clipboard to 12.0.*
  *  Upgraded: ngx-tour-core to 4.0.*
  *  Upgraded: ngx-tour-ng-bootstrap to 4.0.*
  *  Upgraded: ngx-perfect-scrollbar to 7.2.*
  *  Upgraded: ngx-color-picker to 7.4.*
  *  Upgraded: ng2-smart-table to 1.4.*
  *  Upgraded: ngx-toastr to 10.0.*. See https://github.com/scttcper/ngx-toastr/releases/tag/v10.0.0
  *  Upgraded: ngx-swiper-wrapper to 7.2.*
  *  Upgraded: ngx-dropzone-wrapper to 7.2.*
  *  Upgraded: ngx-markdown-editor to 1.2.*
  *  Upgraded: angular-calendar to 0.27.*
  *  Upgraded: highlight.js to 9.15.*
  *  Upgraded: marked to 0.6.*
  *  Upgraded: core-js to 2.6.*
  *  Upgraded: moment to 2.24.*
  *  Upgraded: rxjs to 6.5.*
  *  Upgraded: rxjs-compat to 6.4.*
  *  Upgraded: sweetalert2 to 8.7.*
  *  Upgraded: development dependencies

**Vue.js**

  *  Removed: deprecated VueCLI 2 projects

**Laravel, Laravel + Vue.js**

  *  Upgraded: laravel to 5.8.*. See https://laravel.com/docs/5.8/upgrade

**Laravel + Vue.js**

  *  Fixed: Hot Module Replacement bug in laravel-mix

**Rails**

  *  Upgraded: rails to 5.2.3. See https://github.com/rails/rails/releases/tag/v5.2.2 and https://github.com/rails/rails/releases/tag/v5.2.3

**.Net Core**

  *  Upgraded: .Net Core version to 2.2.*. See https://docs.microsoft.com/en-us/aspnet/core/migration/21-to-22
  *  Added: "build:vendor:copy" task that copying custom vendor assets from node_modules. See Gulpfile.js
  *  Added: support for unobtrusive validation out-of-the-box.
  *  Modified: Split build task on development and production mode to decrease compile time while developing.

**.Net MVC**

  *  Upgraded: NuGet packages

### 1.2.2

**Vue, Laravel + Vue.js**

  *  Upgraded: vue to 2.6.*
  *  Fixed: Inconsistency between vue and vue-template-compiler versions

### 1.2.1

**Angular**

  *  Fixed: ngx-markdown-editor examples

**Laravel, Laravel + Vue.js**

  *  Fixed: Complete upgrade to laravel 5.7.*. See https://laravel.com/docs/5.7/upgrade#upgrade-5.7.0

### 1.2.0

  *  Upgraded: ionicons to 4.4.*
  *  Upgraded: replace deprecated @fortawesome/fontawesome-free-webfonts package with @fortawesome/fontawesome-free
  *  Fixed: breadcrumb dividers in RTL mode
  *  Fixed: modal-dialog border color
  *  Fixed: form state feedback support for .form-control-file
  *  Fixed: switchers in RTL mode
  *  Fixed: custom radio indicator bg
  *  Added: indeterminate checkboxes

**HTML5, .Net Core, .Net MVC, Laravel, Rails**

  *  Upgraded: animate.css to 3.7.*
  *  Upgraded: bootstrap-slider to 10.2.*
  *  Upgraded: dropzone to 5.5.*
  *  Upgraded: sweetalert2 to 7.28.*
  *  Upgraded: text-mask-addons to 3.8.*
  *  Upgraded: jquery-validation to 1.18.*
  *  Upgraded: jquery-minicolors to 2.3.*
  *  Upgraded: nouislider to 12.0.*
  *  Upgraded: pwstrength-bootstrap to 3.0.*
  *  Upgraded: swiper to 4.4.*
  *  Upgraded: tableexport.jquery.plugin to 1.10.*
  *  Upgraded: development dependencies
  *  Updated: toastr icons
  *  Removed: unnecessary jquery.flot.tooltip package

**Vue.js, Laravel + Vue.js**

  *  Upgraded: text-mask-addons to 3.8.*
  *  Upgraded: vue-c3 to 1.2.*
  *  Upgraded: vue-chartjs to 3.4.*
  *  Upgraded: vue-clipboard2 to 0.2.*
  *  Upgraded: vue-color to 2.7.*
  *  Upgraded: vue-cropper to 0.4.*
  *  Upgraded: vue-password-strength-meter to 1.3.*
  *  Upgraded: vue2-google-maps to 0.10.*
  *  Upgraded: vuejs-datepicker to 1.5.*
  *  Upgraded: nouislider to 12.0.*
  *  Upgraded: vue-echarts to 3.1.*
  *  Upgraded: vue-flatpickr-component to 8.0.*
  *  Upgraded: vue-simple-calendar to 4.1.*. See https://github.com/richardtallent/vue-simple-calendar/blob/master/CHANGELOG.md#breaking-changes
  *  Upgraded: vue2-dropzone to 3.5.*
  *  Upgraded: development dependencies
  *  Added: c3 package (vue-c3 dependency)
  *  Updated: vue-toasted styling

**Angular**

  *  Replaced: ng-select with @ng-select/ng-select due low activity in plugin's repository. See https://github.com/ng-select/ng-select
  *  Upgraded: angular to 7.0.*
  *  Upgraded: rxjs to 6.3.*
  *  Upgraded: rxjs-compat to 6.3.*
  *  Upgraded: angular to 6.1.*
  *  Upgraded: angular-cli to 6.1.*
  *  Upgraded: @angular/cdk to 7.0.*
  *  Upgraded: ng-bootstrap to 3.3.*
  *  Upgraded: ngx-charts to 9.0.*
  *  Upgraded: ngx-datatable to 13.1.*
  *  Upgraded: angular-confirmation-popover to 4.2.*
  *  Upgraded: angular-sortablejs to 2.6.*
  *  Upgraded: sweetalert2 to 7.28.*
  *  Upgraded: text-mask-addons to 3.8.*
  *  Upgraded: ngx-color-picker to 6.7.*
  *  Upgraded: ngx-swiper-wrapper to 6.4.*
  *  Upgraded: ngx-perfect-scrollbar to 6.3.*
  *  Upgraded: ngx-dropzone-wrapper to 6.2.*
  *  Upgraded: ngx-toastr to 9.1.*
  *  Upgraded: ng2-smart-table to 1.3.*
  *  Upgraded: marked to 0.5.0
  *  Upgraded: angular-calendar to 0.26.*. See https://github.com/mattlewis92/angular-calendar/blob/master/CHANGELOG.md#breaking-changes
  *  Upgraded: nouislider to 12.0.*
  *  Upgraded: development dependencies
  *  Added: ng2-completer package (ng2-smart-table dependency)
  *  Added: "destroy" event emitter to sidenav component
  *  Updated: ngx-datatable styling
  *  Updated: ngx-toastr icons
  *  Updated: Quill component
  *  Updated: Move IE11 MouseEvent fix to polyfills.ts

**Vue.js**

  * Vue projects upgraded to VueCLI 3. **VueCLI 2 generated projects are deprecated and will be removed on the next release**


**.Net Core**

  *  Added: "default" task in Gulpfile.js
  *  Upgraded: .Net Core dependency to 2.1.5
  *  Updated: gulp build hooks (Gulpfile.js and *.csproj changed)

**.Net MVC**

  *  Fixed: issues related to deploying to IIS
  *  Added: CssRewriteUrlTransformWrapper to fix asset paths when the project is deployed on non-root url (IIS)
  *  Upgraded: NuGet dependencies
  *  Changed: Disable minification for vendor javascripts

**Laravel**

  *  Upgraded: laravel to 5.7.*

**Laravel + Vue.js**

  *  Upgraded: laravel to 5.7.*
  *  Updated: Add polyfills to entry-point.js
  *  Updated: Laravel Mix config
  *  Updated: package.json config

**Known issues**

  *  **Laravel + Vue.js** and deprecated **Vue.js CLI2** projects don't support `vue-cropper` and `vue-c3` packages

### 1.1.0

  *  Upgraded: bootstrap to 4.1.*
  *  Updated: utilities
  *  Updated: icons
  *  Added: fixed footers
  *  Added: footer position switcher in ThemeSettings panel
  *  Added: footer background switcher in ThemeSettings panel
  *  Added: `.app-brand` component
  *  Added: laravel-vue-starter project
  *  Added: aspnet-core-demo project
  *  Added: aspnet-mvc-demo project
  *  Added: laravel-demo project
  *  Added: rails-demo project
  *  Fixed: `.layout-sidenav-horizontal` z-index
  *  Fixed: namespace in aspnet-core-starter
  *  Fixed: `.container-p-y` and `.container-m--y` utilities for non-RTL styles

**HTML5, .Net Core, .Net MVC, Laravel, Rails**

  *  Upgraded: bootstrap-datepicker to 1.8.*
  *  Upgraded: bootstrap-daterangepicker to 3.0.*
  *  Upgraded: moment to 2.22.*
  *  Upgraded: swiper to 4.3.*
  *  Upgraded: blueimp-gallery to 2.33.*
  *  Upgraded: nouislider to 11.1.*
  *  Upgraded: sweetalert2 to 7.22.*
  *  Upgraded: ladda to 2.0.*
  *  Upgraded: vanilla-text-mask to 5.1.*
  *  Upgraded: bootstrap-select to 1.13.*
  *  Upgraded: c3 to 0.6.*
  *  Upgraded: perfect-scrollbar to 1.4.*
  *  Updated: bootstrap-datepicker styling
  *  Updated: bootstrap-daterangepicker styling
  *  Added: fullcalendar plugin

**Angular**

  *  Upgraded: angular to 6.0.*
  *  Upgraded: angular-cli to 6.0.*
  *  Upgraded: ng-bootstrap to 2.1.*
  *  Upgraded: cdk to 6.2.*
  *  Upgraded: ngx-chips to 1.9.*
  *  Upgraded: ngx-charts to 8.0.*
  *  Upgraded: ngx-datatable to 13.0.*
  *  Upgraded: angular2-text-mask to 9.0.*
  *  Upgraded: ng-block-ui to 1.0.*
  *  Upgraded: ngx-clipboard to 11.1.*
  *  Upgraded: ngx-color-picker to 6.3.*
  *  Upgraded: ngx-contextmenu to 5.0.*
  *  Upgraded: ngx-dropzone-wrapper to 6.1.*
  *  Upgraded: ngx-perfect-scrollbar to 6.2.*
  *  Upgraded: ngx-swiper-wrapper to 6.2.*
  *  Upgraded: ngx-tour-ng-bootstrap to 3.0.*
  *  Upgraded: sweetalert2 to 7.22.*
  *  Upgraded: ngx-trend to 3.3.*
  *  Upgraded: ngx-toastr to 8.8.*
  *  Upgraded: angular2-ladda to 2.0.*
  *  Upgraded: nouislider to 11.1.*
  *  Upgraded: moment to 2.22.*
  *  Upgraded: ngx-sweetalert2 to 4.0.*
  *  Upgraded: rxjs to 6.2.*
  *  Upgraded: angular-2-dropdown-multiselect to 1.8.*
  *  Upgraded: marked to 0.4.*
  *  Updated: starter project
  *  Updated: docs
  *  Added: scroll to top of the page on route change
  *  Added: ThemeSettings service
  *  Added: angular-calendar component
  *  Fixed: linting errors

**Vue.js, Laravel + Vue.js**

  *  Upgraded: bootstrap-vue to 2.0.0-rc.11
  *  Upgraded: vue-meta to 1.5.*
  *  Upgraded: vuejs-datepicker to 1.3.*
  *  Upgraded: vue-chartjs to 3.3.*
  *  Upgraded: vue-clipboard2 to 0.1.*
  *  Upgraded: vue-jstree to 2.1.*
  *  Upgraded: vue-masonry to 0.11.*
  *  Upgraded: vue-multiselect to 2.1.*
  *  Upgraded: vue-password-strength-meter to 1.2.*
  *  Upgraded: vue-text-mask to 6.1.*
  *  Upgraded: vue-slider-component to 2.7.*
  *  Upgraded: vue2-dropzone to 3.2.*
  *  Upgraded: vue-tables-2 to 1.4.*
  *  Upgraded: vue2-google-maps to 0.9.*
  *  Upgraded: ladda to 2.0.*
  *  Upgraded: nouislider to 11.1.*
  *  Upgraded: moment to 2.22.*
  *  Upgraded: perfect-scrollbar to 1.4.*
  *  Upgraded: vue-gallery to 1.4.*
  *  Updated: vue-masonry import
  *  Added: scroll to top of the page on route change
  *  Added: ThemeSettings helpers
  *  Added: vue-simple-calendar component

### 1.0.0

  *  Initial release
