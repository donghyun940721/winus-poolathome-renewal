function sortResultJson(obj, prop, asc) {
	obj.sort(function(a, b) {
		if(asc.toUpperCase() == "ASC"){
			return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
		}else{
			return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
		}
	});
}