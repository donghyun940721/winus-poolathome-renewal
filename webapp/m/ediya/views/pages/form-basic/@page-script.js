jQuery(function($) {

	// disable/enable the `readonly` input
	$('#id-checkbox-disable-it')
		.prop('checked', false)
		.on('click', function() {
			var inp = document.getElementById('id-disable-me')
			inp.disabled = this.checked
			if (this.checked) {
				inp.removeAttribute('readonly')
				inp.value = 'This text field is disabled!'
			}
			else {
				inp.setAttribute('readonly', '')
				inp.value = 'This text field is readonly!'
			}
		})


	$('#toggle-password')
		.on('click', function(e) {
			e.preventDefault()
			$(this).toggleClass('active')

			var inp = document.getElementById('id-form-field-password-toggle')
			inp.type = $(this).hasClass('active') ? 'text' : 'password'
			inp.focus()
		})



	// input with tooltip and popover example
	$('#tooltip-1').tooltip({
		container: 'body',
		template: '<div class="tooltip" role="tooltip"><div class="arrow brc-success-m1"></div><div class="bgc-success-m1 tooltip-inner text-dark text-600 text-110 px-2 pb-2"></div></div>'
	})

	$('#popover-1').popover({
		container: 'body',
		trigger: 'click',
		placement: 'auto',
		template: '<div class="popover brc-primary-m2" role="tooltip"><div class="arrow arrow2 brc-white"></div><div class="arrow brc-primary-m1"></div><h3 class="popover-header bgc-primary-l2 border-0 text-115 text-dark-tp3"></h3><div class="popover-body text-grey-d2 text-105"></div></div>'
	})



	// textarea limiter
	$('#id-textarea-limit1').maxlength({
		alwaysShow: true,
		allowOverMax: false,

		warningClass: "badge badge-dark",
		limitReachedClass: "badge badge-warning",
		placement: 'bottom-right-inside'
	})

	$('#id-textarea-limit2').maxlength({

		alwaysShow: true,
		allowOverMax: false,
		appendToParent: true,

		warningClass: "w-100 popover bs-popover-bottom  brc-primary-m2 border-2",
		limitReachedClass: "w-100 popover bs-popover-bottom brc-danger-m2 border-2",
		placement: 'bottom',

		message: function(currentText, maxLength) {
			return '<div class="arrow ' + (maxLength == currentText.length ? 'brc-danger' : 'brc-primary') + '" style="left:calc(50% - 0.5rem)"></div>\
              <div class="popover-body">'+ (Math.max(0, maxLength - currentText.length)) + ' characters remaining ...\
              <div class="h-1"></div> max allowed: '+ maxLength + '!</div>'
		}
	})



	//autosize for textarea
	autosize($('#id-textarea-autosize'))


	///////////////////////////////////////////////
	//masked input
	try {//not working in IE11
		$("#form-field-mask-1").inputmask("99/99/9999")
		$("#form-field-mask-2").inputmask("(999) 999-9999")
		$("#form-field-mask-3").inputmask("a*-999-a999")
		$("#form-field-mask-4").inputmask("~9.99 ~9.99 999")
		//$("").inputmask("9-a{1,3}9{1,3}")
	} catch (e) { }



	// custom ace file input

	// simple
	$('#ace-file-input1').aceFileInput({
		/**
		btnChooseClass: 'bgc-grey-l2 pt-15 px-2 my-1px mr-1px',
		btnChooseText: 'SELECT FILE',
		placeholderText: 'NO FILE YET',
		placeholderIcon: '<i class="fa fa-file bgc-warning-m1 text-white w-4 py-2 text-center"></i>'
		*/
	})

	// multiple with image preview
	$('#ace-file-input2').aceFileInput({
		style: 'drop',
		droppable: true,

		container: 'border-1 border-dashed brc-grey-l1 brc-h-info-m2 shadow-sm',

		placeholderClass: 'text-125 text-600 text-grey-l1 my-2',
		placeholderText: 'Drop images here or click to choose',
		placeholderIcon: '<i class="fa fa-cloud-upload-alt fa-3x text-info-m2 my-2"></i>',

		//previewSize: 64,
		thumbnail: 'large',

		//allowExt: 'gif|jpg|jpeg|png|webp|svg',
		//allowMime: 'image/png|image/jpeg',
		allowMime: 'image/*',

		//maxSize: 100000,
	})
		.on('change', function() {
			// get dropped/selected files

			// console.log( $(this).data('ace_input_files') )
			// console.log( $(this).data('ace_input_method') )

			// or
			// console.log( $(this).aceFileInput('files') )
			// console.log( $(this).aceFileInput('method') )
		})
		.on('invalid.ace.file', function(e, errors) {
			// console.log(errors)
		})
		.on('preview_failed.ace.file', function(e, error) {
			// console.log(error)
			// if(error.code == 2) alert(error.filename + ' is not an image!')
		})
		.on('reset.ace.file', function(e) {
			// e.preventDefault()
		})

	// some available methods

	// $('#ace-file-input2').aceFileInput('disable')
	// $('#ace-file-input2').aceFileInput('startLoading')

	// $('#ace-file-input2').aceFileInput('showFileList', [{name: 'avatar3.jpg', type: 'image', path: 'assets/image/avatar/avatar3.jpg'} , {name: 'avatar2.jpg', type: 'image', path: 'assets/image/avatar/avatar2.jpg'}])
	// $('#ace-file-input1').aceFileInput('showFileList', [{name: '2.txt', type: 'document'}])
	// $('#ace-file-input1').aceFileInput('resetInput');



	//////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////


	// datepicker
	var TinyDatePicker = DateRangePicker.TinyDatePicker;
	TinyDatePicker('#from_date', {
		mode: 'dp-modal',
	})
		.on('statechange', function(ev) {

		})
		
	TinyDatePicker('#to_date', {
		mode: 'dp-modal',
	})
		.on('statechange', function(ev) {

		})
		
	TinyDatePicker('#valDlvDt', {
		mode: 'dp-modal',
	})
		.on('statechange', function(ev) {

		})
		
	// modal one
	TinyDatePicker('#id-date-2', {
		mode: 'dp-modal',
	}).on('statechange', function(ev) {
		// console.log(ev);
	})


	// third one
	// on mobile devices show native datepicker
	if ('ontouchstart' in window && window.matchMedia('(max-width: 600px)')) {
		document.querySelector('#id-date-3').setAttribute('type', 'date')
	}
	else {
		TinyDatePicker('#id-date-3', {
			mode: 'dp-modal',
		})
	}

	//////
	// date range picker example
	var daterange_container = document.querySelector('#id-daterange-container')
	// Inject DateRangePicker into our container
	DateRangePicker.DateRangePicker(daterange_container, {
		mode: 'dp-modal'
	})
		.on('statechange', function(_, rp) {
			// Update the inputs when the state changes
			var range = rp.state
			$('#id-daterange-from').val(range.start ? range.start.toDateString() : '')
			$('#id-daterange-to').val(range.end ? range.end.toDateString() : '')
		})

	$('#id-daterange-from, #id-daterange-to').on('focus', function() {
		daterange_container.classList.add('visible')
	})

	var daterange_wrapper = document.querySelector('#id-daterange-wrapper')
	var previousTimeout = null;
	$(daterange_wrapper).on('focusout', function() {
		if (previousTimeout) clearTimeout(previousTimeout)
		previousTimeout = setTimeout(function() {
			if (!daterange_wrapper.contains(document.activeElement)) {
				daterange_container.classList.remove('visible')
			}
		}, 10)
	})



	////////////////////////
	// Datetimepicker plugin
	$('#id-timepicker').datetimepicker({
		icons: {
			time: 'far fa-clock text-green-d1 text-120',
			date: 'far fa-calendar text-blue-d1 text-120',

			up: 'fa fa-chevron-up text-secondary',
			down: 'fa fa-chevron-down text-secondary',
			previous: 'fa fa-chevron-left text-secondary',
			next: 'fa fa-chevron-right text-secondary',

			today: 'far fa-calendar-check text-purple-d1 text-120',
			clear: 'fa fa-trash-alt text-orange-d2 text-120',
			close: 'fa fa-times text-danger text-120'
		},

		// sideBySide: true,

		toolbarPlacement: "top",

		allowInputToggle: true,
		// showClose: true,
		// showClear: true,
		showTodayButton: true,

		//"format": "HH:mm:ss"
	})

	//***** NOTE *******//
	// the above `date/time` picker plugin was designed for BS3.
	// To make it work with BS4, the following piece of code is required
	$('#id-timepicker')
		.on('dp.show', function() {
			$(this).find('.collapse.in').addClass('show')
			$(this).find('.table-condensed').addClass('table table-borderless')

			$(this).find('[data-action][title]').tooltip() // enable tooltip
		})

	// now listen to the `.collapse` events inside this datetimepicker accordion (one `.collapse` is for timepicker, the other one is for datepicker)
	// then add or remove the old `in` BS3 class so the plugin works correctly
	$(document)
		.on('show.bs.collapse', '.bootstrap-datetimepicker-widget .collapse', function() {
			$(this).addClass('in')
		}).on('hide.bs.collapse', '.bootstrap-datetimepicker-widget .collapse', function() {
			$(this).removeClass('in')
		})



	//////////////////////////////////
	// the small time picker inside popover

	$('#id-popover-timepicker')
		.popover({
			title: 'Choose Time',
			sanitize: false,
			content: function() {
				return $('#id-popover-timepicker-html').html()
			},
			html: true,
			placement: 'auto',
			trigger: 'manual',
			container: 'body',

			template: '<div class="popover popover-timepicker brc-primary-m3 border-1 radius-1 shadow-sm" role="tooltip"><div class="arrow brc-primary"></div><h3 class="popover-header bgc-primary-l3 brc-primary-l1 text-dark-tp4 text-600 text-center pb-25"></h3><div class="popover-body text-grey-d2 p-4"></div></div>'
		})
		.on('click', function() {
			// show popover when button is clicked
			$(this).popover('toggle')
			$(this).toggleClass('active')

			// get a reference to it
			var popover = $(document.body).find('.popover-timepicker').last()

			// hide popover if clicked outside of it
			if (popover.length > 0 && popover.hasClass('show')) {
				var This = this
				$(document).on('click.popover-time', function(ev) {
					if (popover.get(0).contains(ev.target) || ev.target == document.getElementById('id-popover-timepicker')) return
					$(This).popover('hide')
					$(This).removeClass('active')

					$(document).off('.popover-time')
				})
			}
		})



	////////////////////////////////////
	// color picker

	try {
		$('#id-color-picker-1').colorpicker({
			container: true,
			extensions: [
				{
					name: 'swatches',
					options: {
						colors: {
							'tetrad1': '#f00',
							'tetrad2': '#00f',
							'tetrad3': '#eee',
							'tetrad4': '#ddd'
						},
						namesAsValues: false
					}
				}
			]
		})
			.on('colorpickerChange', function(event) {
				$('#id-color-picker-1-update').css('background-color', event.color.toString())
			})
	}
	catch (err) {
		$('#id-color-picker-1').attr('placeholder', 'Plugin not loaded...')
	}


	////////////////////////////////////
	// the other colorpicker
	var colorPicker = new iro.ColorPicker("#color-picker-container", {
		// Set the size of the color picker
		width: 160,
		// Set the initial color to pure red
		color: "#f00"
	})
	colorPicker
		.on('color:change', function(color, changes) {
			//console.log(color.hexString);
		})



	////////////////////////////////////////
	// Knob input

	$('.knob').knob()

})